!-------------------------------------------------------------------------------
!  module gk_eigen.f90
!  This module provides the utilities necessary to compute phase velocity,
!  field, and distribution function solutions for an arbitrary number of ion
!  and electron species, based on the derivation of the collisionless
!  gyrokinetic dispersion relation presented in Howes, Cowley, Dorland, Hammet,
!  Quataert, and Scheckochihin, _Ap. J._ *651*, 590 (2006).
!  Further information about this module can be found in the gk_eigen Companion
!  which should be distributed with this module.
!  KDN, November 2010
!-------------------------------------------------------------------------------
!  VERSION 2: Modification by Greg Howes 2011 JAN 21
!  STRATEGY:
!           1) Solve for omega, X, Y, Z for each Fourier mode (kx,ky,kz) 
!           2) Determine apar, bpar, phi, and g_s for each Fourier mode (kx,ky,kz)
!           3) Convert from Fourier mode (kx,ky,kz) to (kx,ky,z)

module gk_eigen
  use constants, only:zi,pi,twopi
  use file_utils, only: error_unit
  use run_parameters, only:beta

  implicit none

!List of subroutines and functions
! [] = multiple mention
! () = optional (might never be called)
! - * # @ =   Nesting levels
!
! - subroutine gk_eigen_ginit
!   * subroutine gkeig_init_gk_eigen
!     # subroutine gkeig_read_parameters
!   * subroutine gkeig_kgrid_info
!   * subroutine gkeig_vgrid_info
!   * subroutine gkeig_solve_eigenvalues
!     # subroutine get_corrected_indices
!     # subroutine set_kp2
!     # function gkeig_solve_omega
!       @ function gkeig_disp
!         = subroutine get_kp2
!       @ [subroutine gkeig_set_auxillary]
!     # subroutine gkeig_set_auxillary
!   * subroutine gkeig_set_fields
!     # [subroutine get_corrected_indices]
!   * subroutine gkeig_populate_g
!     # [subroutine get_corrected_indices]
!     # subroutine elgrids_to_vgrids
!   * (subroutine gkeig_write_eigen)
!   * [(subroutine gk_eigfields)]
!   * subroutine gkeig_cleanup
! - (subroutine gk_eigfields)
! - subroutine close_gk_eigfields
!SELF: List goes here

  public :: gk_eigen_ginit,&
            gk_eigfields,&
            close_gk_eigfields,&
            gk_eigen_inited

  private


  logical                                :: debug=.false.

  !Knobs and status reporting
  logical                                :: no_eigen_knobs, no_eigen_modes,&
                                            initialized, gk_write_g,&
                                            gk_write_disp,&
                                            eigfields_gnuplot_spacing,&
                                            eigfields_immediate_write
                                            
  logical, save                          :: gk_eigen_inited

  ! Hack needed to provide rtsec with functions of a single argument
  real                                   :: global_kp2

  ! Temporary storage for auxillary gyrokinetic coefficients A,B,C,D, and E
  complex                                :: gkca,gkcb,gkcc,gkcd,gkce

!-INPUTS------------------------------------------------------------------------
  integer                                :: nk_eigen,nwrite_eigfields

  integer, dimension(:), allocatable     :: kx_eigen,ky_eigen,kz_eigen
  ! (1:nk_eigen)
  
  real, dimension(:), allocatable        :: amp_eigen,phase_eigen, &
                                            prec_eigen,tol_eigen
  ! (1:nk_eigen)

  complex,dimension(:), allocatable      :: om_est_eigen
  ! (1:nk_eigen)
  
  logical, dimension(:), allocatable     :: localized_eigen
  ! (1:nk_eigen)

  real, dimension(:), allocatable         :: z0_eigen     !Window center
  ! (1:nk_eigen)

  real, dimension(:), allocatable         :: deltz_eigen  !Window width
  ! (1:nk_eigen)

  real, dimension(:), allocatable         :: power_eigen  !Window exponential
  ! (1:nk_eigen)

!-OUTPUTS-----------------------------------------------------------------------
  complex, dimension(:), allocatable     :: gka_eigen,gkb_eigen,gkc_eigen, &
                                            gkd_eigen,gke_eigen
  ! (1:nk_eigen)

  complex, dimension(:), allocatable     :: apar_eigen,bpar_eigen,phi_eigen, &
                                            om_eig_eigen
  ! (1:nk_eigen)

  complex, dimension(:,:), allocatable   :: apar_spatial
  ! (-ntgrid:ntgrid,1:nk_eigen)

  ! For keeping track of the individual contributions of each species to the
  ! spectral distribution function
  complex, dimension(:,:,:,:), allocatable :: g_kz_by_mode
  ! (1:nk_eigen,1:nspec,1:negrid,1:2*nlambda)
  ! for isgn=1, (1:nlambda)
  ! for isgn=2, (nlambda+1:2*nlambda), in reverse
  !  (i.e., il=nlambda corresponds to nlambda+1, il=1 corresponds to 2*nlambda)

  ! For tracking velocity information in the same format as g_kz_by_mode, above.
  ! Same convention for lambda (il) as above
  real, dimension(:,:,:), allocatable    :: g_kz_vperp,g_kz_vpar
  ! (1:nspec,1:negrid,1:2*nlambda)
  
  ! For gk_eigfields
  integer, dimension(:), allocatable, save :: unit_list
  logical, dimension(:), allocatable, save :: mode_init
  logical, save                            :: eigfields_inited

  ! GGH modifications===========================================================
  integer, parameter :: meth=1             !0=KDN, 1=GGH
  real, parameter    :: ftnorm =1.         !Normalization factor in Fourier
                                           !coefficients
  complex :: gkx,gky,gkz                   !X,Y,Z eigenfunctions solutions 
  complex, dimension(:), allocatable     :: gkx_eigen,gky_eigen,gkz_eigen
  ! (1:nk_eigen)
  complex, dimension(:), allocatable     :: cnorm_eigen !GGH: Normalization for
                                                        !Amplitude/phase

  contains

!-------------------------------------------------------------------------------

  subroutine gk_eigen_ginit
    !<doc> INFO (KN):
    ! subroutine gk_eigen_ginit
    ! This is the main subroutine called by init_g that initializes fields and
    ! distribution functions for any number of modes specified in the AstroGK
    ! input file, "<input>.in".
    use mp, only: proc0,barrier,iproc
    use le_grids, only: init_le_grids
    use kgrids, only: init_kgrids
    use dist_fn, only: init_dist_fn

    character(12) :: lname
    integer :: igk

    if (debug) write(*,*) "gk_eigen_ginit: Calling gkeig_init_gk_eigen on &
                       &   proc ",iproc
    call gkeig_init_gk_eigen
    if (debug) write(*,*) "gk_eigen_ginit: Finished call to &
                       &   gkeig_init_gk_eigen on proc ",iproc
    if ( proc0 .and. gk_write_g ) then
      call gkeig_kgrid_info
      call gkeig_vgrid_info
    end if

    ! Solve the eigenvalue problem for "frequency" and auxillary coefficients
    if (debug) write(*,*) "gk_eigen_ginit: Calling gkeig_solve_eigenvalues."
    call gkeig_solve_eigenvalues

    ! Solve for electromagnetic fields
    if (debug) write(*,*) "gk_eigen_ginit: Calling gkeig_set_fields."
    !GGH
    if (meth .eq. 0) then
       call gkeig_set_fields
    else
       call gkeig_set_fields2
    endif

    ! Solve for distribution functions
    if (debug .and. proc0) write(*,*) "gk_eigen_ginit: Calling gkeig_populate_g"
    !GGH
    if (meth .eq. 0) then
       call gkeig_populate_g
    else
       call gkeig_populate_g2
    endif
    
    !GGH: Correct so that ky=0 modes have correct complex conjugate
!    call correct_ky0_complex

    ! Make sure all distribution function information is computed before
    ! attmepting to write data.
    if (debug .and. proc0) write(*,*) "gk_eigen_ginit: gkeig_populate_g done."
    call barrier

!    if (debug .and. proc0) write(*,*) "PAST barrier"

    ! Proc0 handles writing any gk_eigen related data
    if (proc0) then
      if (gk_write_disp) then
        if (debug) write(*,*) "gk_eigen_init: Calling gkeig_write_eigen."
        call gkeig_write_eigen
      endif
      if (eigfields_immediate_write) then
        lname="gk_ginit"
        call gk_eigfields(header=lname)
      endif
    end if

    call gkeig_cleanup

  if (debug) write (*,*) "gk_eigen_init: proc",iproc," finished."
!    if (debug .and. proc0) write(*,*) "PAST finish"
  end subroutine gk_eigen_ginit

!-------------------------------------------------------------------------------

  subroutine gkeig_init_gk_eigen
    !<doc> INFO (KN):
    ! subroutine gkeig_init_gk_eigen
    ! Initializes the gk_eigen module.  Called by gk_eigen_ginit.
    ! Executes gkeig_read_parameters on proc0, and allocates all needed arrays
    ! on all processors.  Also handles broadcasting input data from proc0 to
    ! other processors.
    !</doc>
    use agk_mem, only: alloc8
    use mp, only: proc0, iproc, broadcast, barrier
    use theta_grid, only: ntgrid

    logical, save :: initialized = .false.

    if (initialized) return
    if (debug) write(*,*) "gkeig_init_gk_eigen: Entering initialization step &
                        &   on proc",iproc,". This message should appear only &
                        &   once per processor."
    initialized = .true.
    eigfields_inited = .false.

    if(proc0) then
      call gkeig_read_parameters
      if (no_eigen_knobs) then
        write(error_unit(),*) "Module gk_eigen called without gk_eigen_knobs &
                         &      namelist. Exiting gk_eigen initialization."
      endif
    end if
    ! Pass the message to all processors
    call broadcast (no_eigen_knobs)
    ! and exit on all processors
    if (no_eigen_knobs) then
      return
    end if
    
    if (proc0) then
      if (no_eigen_modes) then
        write(error_unit(),*) "Module gk_eigen called with nk_eigen<1.  Must &
                          &     specify finitely many eigenmodes to initialize. &
                          &     If flat fields and distribution functions are  &
                          &     desired, use ginit_opt = default or g_init_opt= &
                          &     zero. Exiting gk_eigen initialization."
      end if
    end if
    call broadcast (no_eigen_modes)
    if(no_eigen_modes) then
      return
    end if
    ! Change this public logical to true to let external routines know that the
    ! initialization step has been performed (no other use in gk_eigen)
    gk_eigen_inited = .true.

    if (proc0) then
      if (gk_write_g .and. (.not. gk_write_disp)) then
        write(error_unit(),*) "Logical gk_write_g set .true. without setting &
                           &   logical gk_write_disp to .true.  This probably &
                           &   does not make sense, as none of the information &
                           &   computed about g will be written to the file &
                           &   <input>.eigen."
      end if
    end if
    ! Don't broadcast gk_write_g and gk_write_disp, since only proc0 will handle
    ! these functions.
    
    call broadcast(nk_eigen)
    ! Make sure all processors have received nk_eigen first.
    call barrier
    
    if(proc0) then
      ! Allocate memory for arrays that have not already been
      ! allocated by gkeig_read_parameters

      allocate (om_eig_eigen(nk_eigen))
      call alloc8(c1=om_eig_eigen,v="om_eig_eigen")

      allocate (apar_eigen(nk_eigen)); call alloc8(c1=apar_eigen,v="apar_eigen")
      allocate (bpar_eigen(nk_eigen)); call alloc8(c1=bpar_eigen,v="bpar_eigen")
      allocate (phi_eigen(nk_eigen)); call alloc8(c1=phi_eigen,v="phi_eigen")

      allocate (gka_eigen(nk_eigen)); call alloc8(c1=gka_eigen,v="gka_eigen")
      allocate (gkb_eigen(nk_eigen)); call alloc8(c1=gkb_eigen,v="gkb_eigen")
      allocate (gkc_eigen(nk_eigen)); call alloc8(c1=gkc_eigen,v="gkc_eigen")
      allocate (gkd_eigen(nk_eigen)); call alloc8(c1=gkd_eigen,v="gkd_eigen")
      allocate (gke_eigen(nk_eigen)); call alloc8(c1=gke_eigen,v="gke_eigen")
      
      !GGH
      allocate (gkx_eigen(nk_eigen)); call alloc8(c1=gkx_eigen,v="gkx_eigen")
      allocate (gky_eigen(nk_eigen)); call alloc8(c1=gky_eigen,v="gky_eigen")
      allocate (gkz_eigen(nk_eigen)); call alloc8(c1=gkz_eigen,v="gkz_eigen")
      allocate (cnorm_eigen(nk_eigen))
      call alloc8(c1=cnorm_eigen,v="cnorm_eigen")
      
      allocate (apar_spatial(-ntgrid:ntgrid,nk_eigen))
      call alloc8(c2=apar_spatial,v="apar_spatial")

    ! For other processors that did not run gkeig_read_parameters, initialize
    ! memory for all arrays
    else
      ! Arrays to store input values of each mode driven
      allocate (kx_eigen(nk_eigen)); call alloc8(i1=kx_eigen,v="kx_eigen")
      allocate (ky_eigen(nk_eigen)); call alloc8(i1=ky_eigen,v="ky_eigen")
      allocate (kz_eigen(nk_eigen)); call alloc8(i1=kz_eigen,v="kz_eigen")
      allocate (amp_eigen(nk_eigen)); call alloc8(r1=amp_eigen,v="amp_eigen")
      allocate (phase_eigen(nk_eigen))
      call alloc8(r1=phase_eigen,v="phase_eigen")
      allocate (prec_eigen(nk_eigen)); call alloc8(r1=prec_eigen,v="prec_eigen")
      allocate (tol_eigen(nk_eigen)); call alloc8(r1=tol_eigen,v="tol_eigen")
      allocate (om_est_eigen(nk_eigen))
      call alloc8(c1=om_est_eigen,v="om_est_eigen")
      allocate (localized_eigen(nk_eigen)) !No accounting for logical arrays
      allocate (z0_eigen(nk_eigen)); call alloc8(r1=z0_eigen,v="z0_eigen")
      allocate (deltz_eigen(nk_eigen)); call alloc8(r1=deltz_eigen,v="deltz_eigen")
      allocate (power_eigen(nk_eigen)); call alloc8(r1=power_eigen,v="power_eigen")
      
      ! Arrays to store output values from GKDR solutions for each mode driven
      allocate (om_eig_eigen(nk_eigen))
      call alloc8(c1=om_eig_eigen,v="om_eig_eigen")
      
      allocate (apar_eigen(nk_eigen)); call alloc8(c1=apar_eigen,v="apar_eigen")
      allocate (bpar_eigen(nk_eigen)); call alloc8(c1=bpar_eigen,v="bpar_eigen")
      allocate (phi_eigen(nk_eigen)); call alloc8(c1=phi_eigen,v="phi_eigen")

      allocate (gka_eigen(nk_eigen)); call alloc8(c1=gka_eigen,v="gka_eigen")
      allocate (gkb_eigen(nk_eigen)); call alloc8(c1=gkb_eigen,v="gkb_eigen")
      allocate (gkc_eigen(nk_eigen)); call alloc8(c1=gkc_eigen,v="gkc_eigen")
      allocate (gkd_eigen(nk_eigen)); call alloc8(c1=gkd_eigen,v="gkd_eigen")
      allocate (gke_eigen(nk_eigen)); call alloc8(c1=gke_eigen,v="gke_eigen")
      
      !GGH
      allocate (gkx_eigen(nk_eigen)); call alloc8(c1=gkx_eigen,v="gkx_eigen")
      allocate (gky_eigen(nk_eigen)); call alloc8(c1=gky_eigen,v="gky_eigen")
      allocate (gkz_eigen(nk_eigen)); call alloc8(c1=gkz_eigen,v="gkz_eigen")
      allocate (cnorm_eigen(nk_eigen))
      call alloc8(c1=cnorm_eigen,v="cnorm_eigen")
      
      allocate (apar_spatial(-ntgrid:ntgrid,nk_eigen))
      call alloc8(c2=apar_spatial,v="apar_spatial")

   end if

    ! With memory allocated, share remaining input parameters from proc0 with
    ! all processors
    call broadcast (gk_write_g)
    call broadcast (kx_eigen(:))
    call broadcast (ky_eigen(:))
    call broadcast (kz_eigen(:))
    call broadcast (amp_eigen(:))
    call broadcast (phase_eigen(:))
    call broadcast (om_est_eigen(:))
    call broadcast (tol_eigen(:))
    call broadcast (prec_eigen(:))
    call broadcast (localized_eigen(:))
    call broadcast (z0_eigen(:))
    call broadcast (deltz_eigen(:))
    call broadcast (power_eigen(:))
  end subroutine gkeig_init_gk_eigen

!-------------------------------------------------------------------------------

  subroutine gkeig_read_parameters
    !<doc>INFO (KN):
    ! subroutine gkeig_read_parameters
    ! Read parameters from the AstroGK input file, "<input>.in".
    !
    ! gk_eigen_knobs
    !
    !  nk_eigen                     Number of eigenmodes to be initialized
    !                               Default of 0 will cause errors.
    !
    !  prec                         Precision of initial frequency guess
    !                               Default is 0.01.
    !
    !  tol                          Tolerance required of GKDR solution
    !                               Default is 1.e-13.
    !
    !  gk_write_disp                Whether or not to write dispersion data
    !                               in "<input>.eigen".  Default is .true.
    !
    !  gk_write_g                   Whether to write spectral distribution
    !                               function data to "<input>.eigen". Requires
    !                               gk_write_disp=.true.  Default is .false.
    !
    !  nwrite_eigfields             If eigfields is called with parameter istep,
    !                               output only when istep is a multiple of
    !                               nwrite_eigfields. Default is 100.
    !
    !  eigfields_gnuplot_spacing    Whether to put one (.false.) or two (.true.)
    !                               blank lines between field outputs made by
    !                               gk_eigfields.  Default is .false.
    !                               Normal function of pm3d requires false,
    !                               while indexing all times requires true.
    !
    !  eigfields_immediate_write    Whether to run gk_eigfields once before
    !                               fields are recomputed from the distribution
    !                               functions by get_init_field in module
    !                               dist_fn.  Default is .false.
    !
    ! gk_eigen_#
    !
    !  kx                           x-space wavenumber (kx=1 is 1/x0)
    !
    !  ky                           y-space wavenumber (ky=1 is 1/y0)
    !
    !  kz                           z-space wavenumber (kz=1 is 1/boxlength)
    !
    !  amp                          Real amplitude of A_parallel.  This is half
    !                               the peak-to-trough amplitude if ftnorm is
    !                               set to 1, and is the *spatial*, not spectral
    !                               amplitude.
    !
    !  phase                        Real phase to be added to the complex
    !                               argument of A_parallel
    !
    !  om_est                       Initial guess for frequency to use in root
    !                               search for GKDR solutions. (Strictly
    !                               speaking, the phase velocity.) Normalized to
    !                               the *reference* species' Alfven speed.
    !                               Default is (1.0,0.01)
    !
    !  prec                         Overrides prec for only this mode (whether
    !                               set by default or gk_eigen_knobs).
    !
    !  tol                          Overrides tol for only this mode (whether
    !                               set by default or gk_eigen_knobs).
    !
    !  localized                    T= Sinusoidal signal is windowed in z
    !                               Window = exp(- 4((theta(ig)-z0)/deltz)**power ) 
    !                               Default is false
    !
    !  z0                           Localization window center
    !
    !  deltz                        Localization window width
    !
    !  power                        Exponential power in window
    !
    !</doc>
    use agk_mem, only: alloc8
    use file_utils, only:input_unit_exist,get_indexed_namelist_unit
    use kgrids, only: box
    
    integer :: kx, ky, kz, igk, unit, in_file, ierr
    real :: amp, phase, prec, precl, tol, toll
    complex :: om_est
    logical :: exist
    logical :: localized
    real    :: z0,deltz,power        !Localized window center, width, and power
    
    namelist /gk_eigen_knobs/ nk_eigen,prec,tol,gk_write_disp,gk_write_g,&
                              nwrite_eigfields,eigfields_gnuplot_spacing, &
                              eigfields_immediate_write
    namelist /gk_eigen/ kx,ky,kz,amp,phase,om_est,prec,tol,localized,z0,deltz,power
    
    no_eigen_knobs = .false.
    no_eigen_modes = .false.
    gk_write_disp = .true.
    gk_write_g = .false.
    eigfields_gnuplot_spacing = .false.
    eigfields_immediate_write = .false.
    
    !Putting prec in both gk_eigen_knobs and gk_eigen namelists allows for
    !a global override of the default precision of 0.01 as well as overriding
    !mode by mode.
    nk_eigen = 0               !Number of eigenmodes to be initialized
    prec = 0.01                !Root finding precision (brackets initial guess)
    tol = 1.e-13               !Root finding tolerance (solution quality)
    nwrite_eigfields = 100     !Call gkeig_write_eigfields every
                               !nwrite_eigfields timesteps
    
    in_file=input_unit_exist("gk_eigen_knobs",exist)
    ! Catch the case that gk_eigen_ginit has been called, but no gk_eigen_knobs
    ! have been specified
    if (.not. exist) then
      no_eigen_knobs = .true.
      return
    else
      read (unit=in_file, nml=gk_eigen_knobs)
      ! If we are running kgrids in 'single' mode instead of 'box',
      ! set nk_eigen = 1
      if ( .not. box ) nk_eigen = 1
    end if
    ! Catch the case that gk_eigen_ginit has been called and gk_eigen_knobs is
    ! present, but nk_eigen has not been set to at least 1 (and we are not
    ! driving a single mode).
    if ( nk_eigen .lt. 1 ) then
      no_eigen_modes = .true.
      return
    else
      !These calls to alloc8 (module agk_mem.fpp) document allocation rather
      !than blindly allocating without oversight
      allocate (kx_eigen(nk_eigen)); call alloc8(i1=kx_eigen,v="kx_eigen")
      allocate (ky_eigen(nk_eigen)); call alloc8(i1=ky_eigen,v="ky_eigen")
      allocate (kz_eigen(nk_eigen)); call alloc8(i1=kz_eigen,v="kz_eigen")
      allocate (amp_eigen(nk_eigen)); call alloc8(r1=amp_eigen,v="amp_eigen")
      allocate (phase_eigen(nk_eigen))
      call alloc8(r1=phase_eigen,v="phase_eigen")
      allocate (prec_eigen(nk_eigen)); call alloc8(r1=prec_eigen,v="prec_eigen")
      allocate (tol_eigen(nk_eigen)); call alloc8(r1=tol_eigen,v="tol_eigen")
      allocate (om_est_eigen(nk_eigen))
      call alloc8(c1=om_est_eigen,v="om_est_eigen")
      allocate (localized_eigen(nk_eigen)) !No accounting for logical arrays
      allocate (z0_eigen(nk_eigen)); call alloc8(r1=z0_eigen,v="z0_eigen")
      allocate (deltz_eigen(nk_eigen)); call alloc8(r1=deltz_eigen,v="deltz_eigen")
      allocate (power_eigen(nk_eigen)); call alloc8(r1=power_eigen,v="power_eigen")
     

      precl=prec  !Local copies of global settings to allow restoration
      toll=tol    !in the event that gk_eigen_# overrides prec and/or tol.
      do igk=1,nk_eigen
        call get_indexed_namelist_unit (in_file, "gk_eigen", igk)
        !At the moment, there is no error checking if igk > number of gk_eigen_#
        !namelists specified, nor does get_indexed_namlist_unit appear to return
        !an error, simply exiting the attempt to allocate and perhaps throwing
        !an error (and likely crashing) on a subsequent read attempt.
        
        !Default values if not specified.  Values really SHOULD be specified.
        kx=1
        ky=1
        kz=1
        ! Arbitrary complex amplitude represented with real amplitude
        ! and real phase, rather than complex amplitude.
        amp=1.0
        phase=0.0
        ! Initial guess for complex frequency
        om_est=(1.0,0.01) ! Reference species Alfven speed normalization
        ! Defaults for localized wave simulations
        localized=.false.
        z0=0.
        deltz=3.14159/4.
        power=2.

        read (unit=in_file, nml=gk_eigen)
        close(unit=in_file)
        kx_eigen(igk) = kx
        ky_eigen(igk) = ky
        kz_eigen(igk) = kz
        if ( .not. box ) then
          kx_eigen(igk) = 0
          ky_eigen(igk) = 1
        end if
        amp_eigen(igk) = amp
        phase_eigen(igk) = phase
        om_est_eigen(igk) = om_est
        prec_eigen(igk) = prec
        tol_eigen(igk) = tol
        tol=toll    ! Reset tol and prec to their global values to prevent a
        prec=precl  ! single mode's adjustment from affecting all subsequent
                    ! modes.
        localized_eigen(igk)=localized
        z0_eigen(igk)=z0
        deltz_eigen(igk)=deltz
        power_eigen(igk)=power
        
      end do

    end if

  end subroutine gkeig_read_parameters

!-------------------------------------------------------------------------------

  subroutine gkeig_solve_eigenvalues
    !<doc> INFO (KN):
    ! subroutine gkeig_solve_eigenvalues
    ! For each mode, solves for omegabar_0 and auxillary gyrokinetic coefficents
    ! from input parameters, and stores the solutions in nk_eigen-length arrays.
    !</doc>
    use kgrids, only:kperp2

    integer :: igk,it,ik
    real    :: kp2
    complex :: om_eig
    real :: alphai
    logical :: conj ! unused in this subroutine
    
    do igk = 1, nk_eigen
    
      call get_corrected_indices(kx_eigen(igk),ky_eigen(igk),it,ik,conj)
      kp2 = kperp2(it,ik)
      
      ! Determine omegabar_0
      call set_kp2(kp2) ! Sets kp2_global so gkeig_disp(om) can see it
      om_eig=gkeig_solve_omega(om_est_eigen(igk),prec_eigen(igk),tol_eigen(igk))

      ! Determine AGCs from omegabar_0
      call gkeig_set_auxillary(kp2,om_eig)

      ! Store the solutions for each driven mode
      om_eig_eigen(igk) = om_eig
      !GGH
      if (meth .eq. 0) then
         gka_eigen(igk) = gkca
         gkb_eigen(igk) = gkcb
         gkc_eigen(igk) = gkcc
         gkd_eigen(igk) = gkcd
         gke_eigen(igk) = gkce
      else
         !Calculate eigenfunctions X, Y, Z
         alphai=0.5*kp2   !NOTE: THIS SHOULD BE GENERALIZED TO BE SURE IT IS ION/REFERENCE SPECIES
         gky=cmplx(1.,0.)
         gkx=(gkcc*( gkca*alphai/om_eig**2. - gkca*gkcb + gkcb*gkcb)  &
              - gkcb*(gkca*gkce + gkcb*gkcc))/(gkca * (gkca*gkce + gkcb*gkcc)) *gky 
         gkz= - ( gkca*alphai/om_eig**2. - gkca*gkcb + gkcb*gkcb)/(gkca*gkce + gkcb*gkcc) *gky 
         gkx_eigen(igk) = gkx
         gky_eigen(igk) = gky
         gkz_eigen(igk) = gkz
      endif
    end do

  end subroutine gkeig_solve_eigenvalues

!-------------------------------------------------------------------------------

  complex function gkeig_solve_omega(om_est,prec,tol)
    !<doc>INFO (KN):
    ! complex function gkeig_solve_omega(om_est,prec,tol)
    ! Given an estimate for omega (om_est), find the value of omega that
    ! satisfies the gyrokinetic dispersion relation to within a specified
    ! tolerance tol by bracketing the initial guess by a factor of specified
    ! parameter prec.  The associated dispersion function gkeig_disp expects 
    ! complex frequency om_est to be normalized to the reference species'
    ! Alfven speed.
    !</doc>
    use gk_complex_root, only:rtsec
    complex, intent(in)            :: om_est
    complex                        :: om1,om2
    real                           :: prec,tol
    integer                        :: iflag

    om1=om_est*(1.-prec)
    om2=om_est*(1.+prec)

    gkeig_solve_omega=rtsec(gkeig_disp,om1,om2,tol,iflag)

    return

  end function gkeig_solve_omega

!-------------------------------------------------------------------------------

  complex function gkeig_disp(om)
    !<doc>INFO (KN):
    ! complex function gkeig_disp(om,kp2)
    ! Return complex-valued result of dispersion relation for input values
    ! kperp^2 = kp2 and omegabar_0 = om. (Species temperatures and masses do not
    ! vary between modes and thus do not need to be passed to the function.)
    ! The value of kp2 must be passed indirectly through the subroutines set_kp2
    ! and get_kp2, as the root solving routine rtsec requires a univariate
    ! function to be passed.
    ! This function will return the discrepancy in the GKDR equality for a
    ! choice of om, with smaller discrepancies corresponding to more precise
    ! solutions. (It returns the LHS-RHS of equation (13) in the gk_eigen
    ! Companion, or equation (C16) of Howes et al., _Ap. J._, (2006).)
    !</doc>
  
    complex, intent(in) :: om
    real                :: kp2

    !
    call get_kp2(kp2)
    call gkeig_set_auxillary(kp2,om)
    
    gkeig_disp = ( ( (kp2*gkca)/(2*om*om) ) - gkca*gkcb + gkcb*gkcb )&
                 * ( ( 2*gkca / beta ) - gkca*gkcd + gkcc*gkcc )&
                 - ( gkca*gkce + gkcb*gkcc )**2
    return
    
  end function gkeig_disp

!-------------------------------------------------------------------------------

  subroutine set_kp2(kp2)

    real, intent(in) :: kp2

    global_kp2 = kp2
    return

  end subroutine set_kp2

!-------------------------------------------------------------------------------

  subroutine get_kp2(kp2)

    real, intent(out) :: kp2

    kp2 = global_kp2
    return

  end subroutine get_kp2

!-------------------------------------------------------------------------------

  subroutine gkeig_set_auxillary(kp2,om)
    !<doc> INFO (KN):
    ! subroutine gkeig_set_auxillary(kp2,om)
    ! This subroutine computes the values of the auxillary gyrokinetic
    ! coefficients, as defined in the gk_eigen Companion (equations 6-10).
    ! This routine takes a specified value of kp2 [(kperp*rho0)**2] and om
    ! [omega/(|kpar|v_A0)]. Note that om does not have to satisfy the GKDR in
    ! order for this routine to return its coefficients.
    !</doc>
    use species, only: spec, nspec, tracer_species
    
    complex, intent(in) :: om
    real, intent(in)    :: kp2
    complex :: xi,z
    real    :: gkcmu,gkcnu,gkctau,gkctheta,alpha,gam0,gam1,c1,c2,c3
    integer :: igk,is
    
    ! Zero-out all coefficients
    gkca = 0.
    gkcb = 0.
    gkcc = 0.
    gkcd = 0.
    gkce = 0.
    
    do is = 1, nspec
      if ( spec(is)%type .eq. tracer_species) then
        ! Do nothing (Don't add contributions from tracer species)
      else
        ! Sum contributions from each non-tracer species
        gkcmu    = spec(is)%mass
        gkcnu    = spec(is)%dens
        gkctau   = spec(is)%temp
        gkctheta = spec(is)%z

        c1 = gkctheta * gkctheta * gkcnu / gkctau
        c2 = gkctheta * gkcnu
        c3 = gkcnu*gkctau
        
        alpha = 0.5 * ( ( gkctau * gkcmu ) / gkctheta**2. ) * kp2
        gam0 = bessi0e(alpha)
        gam1 = gam0 - bessi1e(alpha)
        xi = om *sqrt( beta * gkcmu / gkctau )
        z = plasma_disp(xi)

        gkca = gkca + c1 * ( 1 + gam0 * xi * z )
        gkcb = gkcb + c1 * ( 1 - gam0 )
        gkcc = gkcc + c2 * ( gam1 * xi * z )
        gkcd = gkcd + c3 * ( 2. * gam1 * xi * z )
        gkce = gkce + c2 * ( gam1 )
          
      end if
    end do

  end subroutine gkeig_set_auxillary
  
!-------------------------------------------------------------------------------

  subroutine gkeig_set_fields
    !<doc> INFO (KN):
    ! subroutine gkeig_set_fields
    ! Solves first for spectral fields, and subsequently for positional fields
    ! (the inverse fourier transform of the spectral fields).
    ! A normalization convention must be chosen to perform the DFT from spectral
    ! z to positional z.  The setting "ftnorm = 1" assumes that amp specifies
    ! the amplitude of A_parallel in *position* space.  This has the notable
    ! effect that in spectral space, the amplitude is a factor of sqrt(twopi)
    ! larger if using the symmetric convention for the Fourier transform, or a
    ! factor of twopi larger if using the asymmetric convention.  In either
    ! case, it is assumed that position space is derived from spectral space
    ! through the *inverse* Fourier transform (and thus the sign of the argument
    ! is positive in this section).  Again, for emphasis, it is not the spectral
    ! amplitude that is being set, but rather the spatial amplitude (half the
    ! peak-to-trough amplitude).
    !</doc>
    use theta_grid, only: ntgrid,delthet
    use fields_arrays, only: apar,bpar,phi,aparnew,bparnew,phinew
    use kgrids, only: kperp2

    integer :: kx,ky,kz,igk,ig,it,ik
    real    :: ftarg   !Argument to exponential in Fourier coefficients
    real    :: amp,phase,kpar,kparsign,kp2
    complex :: fcoeff  !Fourier coefficients at each point in z space
    complex :: om_eig
    complex :: aparc,bparc,phic
    logical :: conj

    ! ftnorm = 1 means amp defines spatial amplitude.
    ! (Other choices are possible:
    !  ftnorm = 2*pi means amp prescribes spectral amplitude for assymetric
    !                Fourier transform,
    !  ftnorm = sqrt(2*pi) means amp presecribes spectral amplitude for
    !                      symmetric Fourier transform.)
!    ftnorm = 1.

    !For each mode i that we will be driving:
    do igk = 1, nk_eigen

      !Get wavenumbers of mode
      ! Indices it and ik are used rather than kx and ky in order to be
      ! consistent with other modules' format
      call get_corrected_indices(kx_eigen(igk),ky_eigen(igk),it,ik,conj)
      kpar = real(kz_eigen(igk))
      kparsign = sign(1.,kpar)
      kp2 = kperp2(it,ik)
      
      !Get mode parameters
      amp = amp_eigen(igk)
      phase = phase_eigen(igk)
      om_eig = om_eig_eigen(igk)
      !Get precomputed auxillary gyrokinetic coefficients
      gkca = gka_eigen(igk)
      gkcb = gkb_eigen(igk)
      gkcc = gkc_eigen(igk)
      gkcd = gkd_eigen(igk)
      gkce = gke_eigen(igk)
      
      !Compute spectral fields
      aparc = amp * ( cos(phase)+zi*sin(phase) )
      phic  = ( - kparsign * ( om_eig / sqrt(beta) ) * aparc ) * &
              ( ( gkcb - gkca ) * gkce - ( kp2*gkcc ) / ( 2 * om_eig**2 ) ) &
              / ( gkca*gkce + gkcb*gkcc )
      bparc = ( - kparsign * ( om_eig / sqrt(beta) ) * aparc ) * &
              ( kp2 * gkca / ( 2 * om_eig**2 ) - gkca*gkcb + gkcb*gkcb ) &
              / ( gkca*gkce + gkcb*gkcc )


      ! If we need to conjugate because of ky < 0, do that now
      if ( conj ) then
        aparc = conjg(aparc)
        bparc = conjg(bparc)
        phic  = conjg(phic)
      end if

      ! Store solutions by mode
        apar_eigen(igk) = aparc
        bpar_eigen(igk) = bparc
        phi_eigen(igk)  = phic


      !Compute spatial field amplitudes from spectral field amplitudes
      do ig = -ntgrid, ntgrid
        ! fcoeff = exp( zi * ftarg )
        ! zi is sqrt(-1) from module constants
        ! Note positive zi, for inverse FT
        ftarg = kpar*delthet*real(ig)
        fcoeff =  ftnorm * ( cos(ftarg) + zi * sin(ftarg) )
        ! Also note that we do not conjugate this term, since it would be
        ! exp(i*kz*z) -> e((-i)*(-kz)*z) = e(i*kz*z), and we do not wish to
        ! modify the value of kz (kpar) where the dispersion relation and field
        ! solutions are concerned.
        
        ! Store A_parallel independently for each mode, for later use in the
        ! computation of distribution functions.
        apar_spatial(ig,igk) = aparc*fcoeff
        !The aparc, bparc, and phic values are the coefficients in kz (spectral
        ! space), and must be multiplied by fourier coefficents to get their
        ! positional values {i.e., apar(z)=FT[apar(kz)]
        ! where apar(kz)=(aparc/fnorm)*delta(kz-kz0) }

        apar(ig,it,ik) = apar(ig,it,ik) + aparc * fcoeff
        bpar(ig,it,ik) = bpar(ig,it,ik) + bparc * fcoeff
        phi(ig,it,ik) = phi(ig,it,ik) + phic * fcoeff

      end do
    end do
    
    aparnew=apar
    bparnew=bpar
    phinew=phi
  
  end subroutine gkeig_set_fields

!-------------------------------------------------------------------------------

  subroutine gkeig_set_fields2
    !<doc> INFO (GGH):
    ! subroutine gkeig_set_fields
    ! Solve for spectral fields Fourier coefficients phic, aparc, bparc
    ! Next, compute fields along z
    !</doc>
    use theta_grid, only: ntgrid,delthet,theta
    use fields_arrays, only: apar,bpar,phi,aparnew,bparnew,phinew
    use kgrids, only: kperp2,nakx
    use mp, only: proc0
    !    use nonlinear_terms, only: nonlin
    integer :: kx,ky,kz,igk,ig,it,ik
    integer :: itc     !Conjgate value of it for ky=0 modes
    real    :: ftarg   !Argument to exponential in Fourier coefficients
    real    :: amp,phase,kpar,kparsign,kp2
    complex :: fcoeff  !Fourier coefficients at each point in z space
    complex :: om_eig
    complex :: cnorm
    complex :: aparc,bparc,phic
    logical :: conj    !Necessary only for get_corrected_indices call, but not used
    real    :: a0=1.0         !Window function in z for initialized wave amplitude
    real    :: z0p,z0m     !Periodic points of z0 (to maintain periodicity)
        
    !For each mode i that we will be driving:
    do igk = 1, nk_eigen

      !Get wavenumbers of mode
      ! Indices it and ik are used rather than kx and ky in order to be
      ! consistent with other modules' format
      call get_corrected_indices(kx_eigen(igk),ky_eigen(igk),it,ik,conj)
      kpar = real(kz_eigen(igk))
      kparsign = sign(1.,kpar)
      kp2 = kperp2(it,ik)
      
      !Get mode parameters
      amp = amp_eigen(igk)
      phase = phase_eigen(igk)
      om_eig = om_eig_eigen(igk)
      !Get X, Y, and Z 
      gkx = gkx_eigen(igk)
      gky = gky_eigen(igk)
      gkz = gkz_eigen(igk)
      
      !Compute relative spectral Fourier coefficients for fields
      aparc = sqrt(beta )/om_eig*gky
      phic  = gkx+gky
      bparc= gkz

      !Adjust fields to desired amplitude and phase
      cnorm = amp * ( cos(phase)+zi*sin(phase) )/aparc
      aparc = aparc * cnorm
      phic  = phic  * cnorm
      bparc = bparc * cnorm

!      if (proc0) write(*,*)'GGH DEBUG: ',aparc
      
      ! Store solutions by mode
      cnorm_eigen(igk) = cnorm
      apar_eigen(igk)  = aparc
      bpar_eigen(igk)  = bparc
      phi_eigen(igk)   = phic

      !Compute spatial field amplitudes from spectral field amplitudes 
      !  (and add to existing solution)
      !NOTE: 27 OCT 2011 GGH: This seems never to be called for single mode!
      if( ik .eq. 1  .and. nakx .gt. 1) then
         !Add half amplitude and also assign conjugate value if ky=0 mode
         itc=it_conjug_ky0(it)
!         if (proc0) write(*,*)'GGH DEBUG: ',it,itc
         do ig = -ntgrid, ntgrid

            !Set amplitude for localization in z
            if (localized_eigen(igk)) then
               !Compute points that are perioidic in next domains
               z0p=z0_eigen(igk)+(theta(ntgrid)-theta(-ntgrid))
               z0m=z0_eigen(igk)-(theta(ntgrid)-theta(-ntgrid))
               !Compute amplitude including z0p & z0m to ensure periodicity
               a0=   exp(-4.*( (theta(ig)-z0_eigen(igk))/deltz_eigen(igk))**power_eigen(igk)) &
                   + exp(-4.*( (theta(ig)-z0p)/deltz_eigen(igk))**power_eigen(igk)) &
                   + exp(-4.*( (theta(ig)-z0m)/deltz_eigen(igk))**power_eigen(igk))
            else
               a0=1.0
            endif

            apar(ig,it,ik) =apar(ig,it,ik) + 0.5*ftnorm* aparc*exp(zi*kpar*theta(ig))    *a0
            phi(ig,it,ik)  =phi(ig,it,ik)  + 0.5*ftnorm* phic* exp(zi*kpar*theta(ig))    *a0
            bpar(ig,it,ik) =bpar(ig,it,ik) + 0.5*ftnorm* bparc*exp(zi*kpar*theta(ig))    *a0
            !Conjugates for ky=0 modes
            apar(ig,itc,ik) =apar(ig,itc,ik) + 0.5*ftnorm* conjg(aparc*exp(zi*kpar*theta(ig)))    *a0
            phi(ig,itc,ik)  =phi(ig,itc,ik)  + 0.5*ftnorm* conjg(phic* exp(zi*kpar*theta(ig)))    *a0
            bpar(ig,itc,ik) =bpar(ig,itc,ik) + 0.5*ftnorm* conjg(bparc*exp(zi*kpar*theta(ig)))    *a0 

!            if (proc0) write(*,'(a,f8.3,4f9.4,f6.2)')'GGH DEBUG: ',theta(ig),apar(ig,it,ik),aparc,kpar
            
         end do
         !Ensure that the initial conditions are exactly periodic
         apar(ntgrid,it,ik)=apar(-ntgrid,it,ik)
         phi(ntgrid,it,ik)=phi(-ntgrid,it,ik)
         bpar(ntgrid,it,ik)=bpar(-ntgrid,it,ik)
         apar(ntgrid,itc,ik)=apar(-ntgrid,itc,ik)
         phi(ntgrid,itc,ik)=phi(-ntgrid,itc,ik)
         bpar(ntgrid,itc,ik)=bpar(-ntgrid,itc,ik)
      else
         do ig = -ntgrid, ntgrid

            !Set amplitude for localization in z
            if (localized_eigen(igk)) then
               !Compute points that are perioidic in next domains
               z0p=z0_eigen(igk)+(theta(ntgrid)-theta(-ntgrid))
               z0m=z0_eigen(igk)-(theta(ntgrid)-theta(-ntgrid))
               !Compute amplitude including z0p & z0m to ensure periodicity
               a0=   exp(-4.*( (theta(ig)-z0_eigen(igk))/deltz_eigen(igk))**power_eigen(igk)) &
                   + exp(-4.*( (theta(ig)-z0p)/deltz_eigen(igk))**power_eigen(igk)) &
                   + exp(-4.*( (theta(ig)-z0m)/deltz_eigen(igk))**power_eigen(igk))
            else
               a0=1.0
            endif

            apar(ig,it,ik) =apar(ig,it,ik) + ftnorm* aparc*exp(zi*kpar*theta(ig))    *a0
            phi(ig,it,ik)  =phi(ig,it,ik)  + ftnorm* phic* exp(zi*kpar*theta(ig))    *a0
            bpar(ig,it,ik) =bpar(ig,it,ik) + ftnorm* bparc*exp(zi*kpar*theta(ig))    *a0
!            if (proc0) write(*,'(a,f8.3,4f9.4,f6.2)')'GGH DEBUG: ',theta(ig),apar(ig,it,ik),aparc,kpar
            
         end do
         !Ensure that the initial conditions are exactly periodic
         apar(ntgrid,it,ik)=apar(-ntgrid,it,ik)
         phi(ntgrid,it,ik)=phi(-ntgrid,it,ik)
         bpar(ntgrid,it,ik)=bpar(-ntgrid,it,ik)
      endif

    end do
    
    !Set new field values to the same values
    aparnew=apar
    bparnew=bpar
    phinew=phi
  
  end subroutine gkeig_set_fields2
!-------------------------------------------------------------------------------
  subroutine gkeig_populate_g2
    !<doc> INFO (GGH):
    ! subroutine gkeig_populate_g
    ! Primary subroutine of gk_eigen. Populates the species
    ! distribution functions using omega, X, & Z from GK eigenfunction solution
    !</doc>
    use agk_layouts, only: g_lo,ik_idx,it_idx,is_idx,ie_idx,il_idx,idx_local,idx
    use kgrids, only: nakx
    use dist_fn_arrays, only: g,gnew,vpa,vperp2
    use fields_arrays, only:phi,apar,bpar
    use kgrids, only: akx,aky,ikx,iky,kperp2
    use le_grids, only: negrid,nlambda,e,al
    use species, only: specie,spec,nspec
    use spfunc, only: j0,j1
    use mp, only: iproc,proc0,sum_reduce
    use agk_mem, only:alloc8
    use theta_grid, only:ntgrid, theta

    integer :: iglo,isgn,igk,it,ik,kz,is,ie,il,ill,ig
    integer :: itc     !Conjgate value of it for ky=0 modes
    real    :: gkvpar,gkvperp,kpar,kparsign,kp2,kperp
    real    :: gkarg,gkj0,gkj1
    real    :: gkq,gkm,gkt,gksmt
    complex :: om_eig,aparc, cnorm
    complex :: g_gk_without_A
    complex, dimension(:), allocatable   :: g_kz_1d
    complex :: gsc
    logical :: conj
    type(specie) :: gkspec
    real    :: a0=1.0         !Window function in z for initialized wave amplitude
    real    :: z0p,z0m     !Periodic points of z0 (to maintain periodicity)

    ! If distribution function data is to be written:
    if (gk_write_g) then
      allocate(g_kz_by_mode(nk_eigen,nspec,negrid,2*nlambda))
      call alloc8(c4=g_kz_by_mode,v="g_kz_by_mode")
      g_kz_by_mode=0.
      allocate(g_kz_1d(nk_eigen*nspec*negrid*2*nlambda))
      call alloc8(c1=g_kz_1d,v="g_kz_1d")
      g_kz_1d=0.
      ! Create a table of velocity vs. e, lambda for output by proc 0
      if (proc0) then
        call gkeig_elgrids_to_vgrids
      end if
    end if
    if (debug) write(*,*) "gkeig_populate_g: proc ",iproc,", g_lo%llim_proc=",&
                          g_lo%llim_proc,"g_lo%ulim_proc=",g_lo%ulim_proc

    !For each mode to be driven:
    do igk=1,nk_eigen
      !Get wavenumber and frequency of mode
      call get_corrected_indices(kx_eigen(igk),ky_eigen(igk),it,ik,conj)
      if (ik .eq. 1 .and. nakx .gt. 1) itc=it_conjug_ky0(it) !Get Conjugate of it (itc) if ky=0 mode
      kp2 = kperp2(it,ik)
      kperp = sqrt(kp2)
      kpar = real(kz_eigen(igk))
      kparsign = sign(1.,kpar)
      !Get oimega, X, and Z for this driven mode
      om_eig = om_eig_eigen(igk)
      gkx = gkx_eigen(igk)
      gkz = gkz_eigen(igk)
      cnorm = cnorm_eigen(igk)
      
      !For each species (including tracer species)
      do is=1,nspec
        ! Identify the local species and its properties
        gkspec = spec(is)
        gkq = gkspec%z
        gkm = gkspec%mass
        gkt = gkspec%temp
        gksmt = sqrt(gkm/gkt)
        
        !For each energy locus
        do ie=1,negrid
           !For each pitch angle locus
           do il=1,nlambda
              !If this grid point is handled by this processor:
              if (idx_local (g_lo, ik, it, il, ie, is)) then
                 ! Determine the index corresponding to this grid point
                 iglo=idx(g_lo,ik,it,il,ie,is)
                 
                 gkvperp = sqrt(al(il)*e(ie,is)) ! Compute vperp since init_dist_fn
                 ! hasn't been called at this point
                 ! and vperp(iglo) isn't defined
                 
                 ! Evaluate argument to Bessel functions
                 gkarg=kperp*gkvperp*sqrt(gkt*gkm)/gkq  ! |B| = 1 assumed
                 ! Evaluate Bessel functions, noting that j1(x) is BesselJ1(x)/x
                 gkj0=j0(gkarg)
                 gkj1=j1(gkarg)
                 
                 ! For both upfield and downfield parallel velocities:
                 do isgn=1,2
                    !Compute vparallel
                    gkvpar = sqrt(e(ie,is)*max(0.0, 1.0 - al(il)))*(3-2*isgn)
                    ! (3-2*isgn) is +1 for isgn=1, -1 for isgn=2
                    ! Same as gkvpar = vpa(isgn,iglo), but explicitly since
                    ! init_dist_fn has not yet been run and vpa is undefined
                    
                    !Calculate complex Fourier Coefficient for the distribution func
                    gsc=gkvpar/(kparsign*om_eig*gksmt/sqrt(beta) - gkvpar) * &
                         ( gkj0 * gkq/gkt * gkx +  gkj1 * 2.*gkvperp**2. * gkz)
                    
                    !Adjust gsc to desired amplitude and phase
                    gsc=gsc*cnorm
                    
                    !Compute spatial dist func amplitudes from spectral dist func amplitudes 
                    !  (and add to existing solution)
                    !NOTE: Do these need to go from -ntgrid+1, ntgrid for isgn=1 and -ntgrid,ntgrid-1 for isgn=2?
                    if  (nakx .gt. 1 .and. ik .eq. 1) then !If ky=0 mode, add only half amplitude
                       do ig = -ntgrid, ntgrid

                          !Set amplitude for localization in z
                          if (localized_eigen(igk)) then
                             !Compute points that are perioidic in next domains
                             z0p=z0_eigen(igk)+(theta(ntgrid)-theta(-ntgrid))
                             z0m=z0_eigen(igk)-(theta(ntgrid)-theta(-ntgrid))
                             !Compute amplitude including z0p & z0m to ensure periodicity
                             a0=   exp(-4.*( (theta(ig)-z0_eigen(igk))/deltz_eigen(igk))**power_eigen(igk)) &
                                  + exp(-4.*( (theta(ig)-z0p)/deltz_eigen(igk))**power_eigen(igk)) &
                                  + exp(-4.*( (theta(ig)-z0m)/deltz_eigen(igk))**power_eigen(igk))
                          else
                             a0=1.0
                          endif

                          g(ig,isgn,iglo) =g(ig,isgn,iglo) + 0.5*ftnorm* gsc*exp(zi*kpar*theta(ig))    *a0
                       end do
                       !Ensure that the initial conditions are exactly periodic
                       g(ntgrid,isgn,iglo) =g(-ntgrid,isgn,iglo)
                    else !Otherwise, just do as normal
                       do ig = -ntgrid, ntgrid

                          !Set amplitude for localization in z
                          if (localized_eigen(igk)) then
                             !Compute points that are perioidic in next domains
                             z0p=z0_eigen(igk)+(theta(ntgrid)-theta(-ntgrid))
                             z0m=z0_eigen(igk)-(theta(ntgrid)-theta(-ntgrid))
                             !Compute amplitude including z0p & z0m to ensure periodicity
                             a0=   exp(-4.*( (theta(ig)-z0_eigen(igk))/deltz_eigen(igk))**power_eigen(igk)) &
                                  + exp(-4.*( (theta(ig)-z0p)/deltz_eigen(igk))**power_eigen(igk)) &
                                  + exp(-4.*( (theta(ig)-z0m)/deltz_eigen(igk))**power_eigen(igk))
                          else
                             a0=1.0
                          endif

                          g(ig,isgn,iglo) =g(ig,isgn,iglo) + ftnorm* gsc*exp(zi*kpar*theta(ig))    *a0
                       end do
                       !Ensure that the initial conditions are exactly periodic
                       g(ntgrid,isgn,iglo) =g(-ntgrid,isgn,iglo)
                        
                    endif
                    ! If we will be writing spectral distribution functions:
                    if(gk_write_g) then
                       g_kz_by_mode(igk,is,ie,il+(2*nlambda+1-2*il)*(isgn-1)) = gsc
                       ! This convoluted term means that as
                       ! il advances, when isgn=1, the index
                       ! steps up 1,2,3,4,5... and when
                       ! isgn=2, the index steps down
                       ! (2*nlambda), (2*nlambda-1),... etc.
                       ! This is important because nlambda and
                       ! nlambda+1 are 'adjacent', and this
                       ! continuity allows for correct
                       ! rendering of pm3d surfaces in
                       ! gnuplot.        
                    end if
                    
                 end do
                 
              end if
              if (nakx .gt. 1 .and. ik .eq. 1) then
                 if (idx_local (g_lo, ik, itc, il, ie, is) )then
                    ! Determine the index corresponding to this grid point
                    iglo=idx(g_lo,ik,itc,il,ie,is)
                    
                    gkvperp = sqrt(al(il)*e(ie,is)) ! Compute vperp since init_dist_fn
                    ! hasn't been called at this point
                    ! and vperp(iglo) isn't defined
                    
                    ! Evaluate argument to Bessel functions
                    gkarg=kperp*gkvperp*sqrt(gkt*gkm)/gkq  ! |B| = 1 assumed
                    ! Evaluate Bessel functions, noting that j1(x) is BesselJ1(x)/x
                    gkj0=j0(gkarg)
                    gkj1=j1(gkarg)
                    
                    ! For both upfield and downfield parallel velocities:
                    do isgn=1,2
                       !Compute vparallel
                       gkvpar = sqrt(e(ie,is)*max(0.0, 1.0 - al(il)))*(3-2*isgn)
                       ! (3-2*isgn) is +1 for isgn=1, -1 for isgn=2
                       ! Same as gkvpar = vpa(isgn,iglo), but explicitly since
                       ! init_dist_fn has not yet been run and vpa is undefined
                       
                       !Calculate complex Fourier Coefficient for the distribution func
                       gsc=gkvpar/(kparsign*om_eig*gksmt/sqrt(beta) - gkvpar) * &
                            ( gkj0 * gkq/gkt * gkx +  gkj1 * 2.*gkvperp**2. * gkz)
                       
                       !Adjust gsc to desired amplitude and phase
                       gsc=gsc*cnorm
                       
                       !Compute spatial dist func amplitudes from spectral dist func amplitudes 
                       !  (and add to existing solution)
                       !NOTE: Do these need to go from -ntgrid+1, ntgrid for isgn=1 
                       !      and -ntgrid,ntgrid-1 for isgn=2?
                       !This is a ky=0 mode, so add only half amplitude of conjugate
                       do ig = -ntgrid, ntgrid

                          !Set amplitude for localization in z
                          if (localized_eigen(igk)) then
                             !Compute points that are perioidic in next domains
                             z0p=z0_eigen(igk)+(theta(ntgrid)-theta(-ntgrid))
                             z0m=z0_eigen(igk)-(theta(ntgrid)-theta(-ntgrid))
                             !Compute amplitude including z0p & z0m to ensure periodicity
                             a0=   exp(-4.*( (theta(ig)-z0_eigen(igk))/deltz_eigen(igk))**power_eigen(igk)) &
                                  + exp(-4.*( (theta(ig)-z0p)/deltz_eigen(igk))**power_eigen(igk)) &
                                  + exp(-4.*( (theta(ig)-z0m)/deltz_eigen(igk))**power_eigen(igk))
                          else
                             a0=1.0
                          endif

                          g(ig,isgn,iglo) =g(ig,isgn,iglo) + 0.5*ftnorm* conjg(gsc*exp(zi*kpar*theta(ig)))    *a0
                       end do
                       !Ensure that the initial conditions are exactly periodic
                       g(ntgrid,isgn,iglo) =g(-ntgrid,isgn,iglo)
                    end do
                 endif
              endif
           end do
        end do
     end do
  end do
  
  ! If gkeig_write_eigen is to write distribution function information:
  if (gk_write_g) then
     !Sum_reduce to get all values of array to proc 0
     !Reshape to 1-D array for MPI
     g_kz_1d=reshape(g_kz_by_mode,(/ nk_eigen*nspec*negrid*2*nlambda /))
     !Sum flattened g_kz_by_mode across all processors
     call sum_reduce(g_kz_1d,0)
     !Reshape back to 4-D array
     g_kz_by_mode=reshape(g_kz_1d,(/ nk_eigen,nspec,negrid,2*nlambda /))
     ! Now we are done with g_kz_1d
     deallocate(g_kz_1d)
  end if
  
  gnew = g
  
end subroutine gkeig_populate_g2
!-------------------------------------------------------------------------------

  subroutine gkeig_populate_g
    !<doc> INFO (KN):
    ! subroutine gkeig_populate_g
    ! Primary subroutine of gk_eigen.  This is the only subroutine that is
    ! required to execute in order to initialize eigenmodes, although it
    ! depends on the successful execution of other subroutines.  All other
    ! subroutines (those that compute fields, etc.) are present simply to verify
    ! the proper function of this subroutine.
    ! Queries each processor for its range of grid-space responsibility and
    ! uses those attributes to compute the distribution function at those grid
    ! spaces.
    !</doc>
    use agk_layouts, only: g_lo, ik_idx, it_idx,is_idx,ie_idx,il_idx,idx_local,&
                           idx
    use dist_fn_arrays, only: g,gnew,vpa,vperp2
    use fields_arrays, only:phi,apar,bpar
    use kgrids, only: akx,aky,ikx,iky,kperp2
    use le_grids, only: negrid,nlambda,e,al
    use species, only: specie,spec,nspec
    use spfunc, only: j0,j1
    use mp, only: iproc,proc0,sum_reduce
    use agk_mem, only:alloc8
    use theta_grid, only:ntgrid

    integer :: iglo,isgn,igk,it,ik,kz,is,ie,il,ill
    real    :: gkvpar,gkvperp,kpar,kparsign,kp2,kperp
    real    :: gkarg,gkj0,gkj1
    real    :: gkq,gkm,gkt,gksmt
    complex :: om_eig,aparc, g_gk_without_A
    complex, dimension(:), allocatable   :: g_kz_1d
    logical :: conj
    type(specie) :: gkspec

    ! If distribution function data is to be written:
    if (gk_write_g) then
      allocate(g_kz_by_mode(nk_eigen,nspec,negrid,2*nlambda))
      call alloc8(c4=g_kz_by_mode,v="g_kz_by_mode")
      g_kz_by_mode=0.
      allocate(g_kz_1d(nk_eigen*nspec*negrid*2*nlambda))
      call alloc8(c1=g_kz_1d,v="g_kz_1d")
      g_kz_1d=0.
      ! Create a table of velocity vs. e, lambda for output by proc 0
      if (proc0) then
        call gkeig_elgrids_to_vgrids
      end if
    end if
    if (debug) write(*,*) "gkeig_populate_g: proc ",iproc,", g_lo%llim_proc=",&
                          g_lo%llim_proc,"g_lo%ulim_proc=",g_lo%ulim_proc

    !For each mode to be driven:
    do igk=1,nk_eigen
      !Get wavenumber and frequency of mode
      call get_corrected_indices(kx_eigen(igk),ky_eigen(igk),it,ik,conj)
      kp2 = kperp2(it,ik)
      kperp = sqrt(kp2)
      kpar = real(kz_eigen(igk))
      kparsign = sign(1.,kpar)
      om_eig = om_eig_eigen(igk)
      aparc = apar_eigen(igk)
      
      !Get precomputed auxillary gyrokinetic coefficients
      gkca = gka_eigen(igk)
      gkcb = gkb_eigen(igk)
      gkcc = gkc_eigen(igk)
      gkcd = gkd_eigen(igk)
      gkce = gke_eigen(igk)
      
      !For each species (including tracer species)
      do is=1,nspec
        ! Identify the local species and its properties
        gkspec = spec(is)
        gkq = gkspec%z
        gkm = gkspec%mass
        gkt = gkspec%temp
        gksmt = sqrt(gkm/gkt)
        
        !For each energy locus
        do ie=1,negrid
!         !For each pitch angle locus
          do il=1,nlambda
            !If this grid point is handled by this processor:
            if (idx_local (g_lo, ik, it, il, ie, is)) then
            ! Determine the index corresponding to this grid point
            iglo=idx(g_lo,ik,it,il,ie,is)
            
            gkvperp = sqrt(al(il)*e(ie,is)) ! Compute vperp since init_dist_fn
                                            ! hasn't been called at this point
                                            ! and vperp(iglo) isn't defined

            ! Evaluate argument to Bessel functions
            gkarg=kperp*gkvperp*sqrt(gkt*gkm)/gkq
            ! Evaluate Bessel functions, noting that j1(x) is BesselJ1(x)/x
            gkj0=j0(gkarg)
            gkj1=j1(gkarg)

              ! For both upfield and downfield parallel velocities:
              do isgn=1,2
                !Compute vparallel
                gkvpar = sqrt(e(ie,is)*max(0.0, 1.0 - al(il)))*(3-2*isgn)
                ! (3-2*isgn) is +1 for isgn=1, -1 for isgn=2
                ! Same as gkvpar = vpa(isgn,iglo), but explicitly since
                ! init_dist_fn has not yet been run and vpa is undefined
                
                !Compute the gyrokinetic distribution function, but without
                !the factor of A_parallel.  This allows both the spectral value
                !of the distribution function as well as the spatial array to be
                !computed by simple multiplication.
                g_gk_without_A= (gkq/gkt) * &
                         (gkvpar/(kparsign*om_eig*sqrt(gkm/(beta*gkT))-gkvpar))&
                         * ( 1 / ( gkca*gkce + gkcb*gkcc ) ) * &
                         ( - kparsign*om_eig/sqrt(beta) ) &
                         * ( &
                             gkj0*(gkcb*(gkcc+gkce) - kp2*gkcc/(2*om_eig**2) ) &
                             + 2*(gkt/gkq)*gkj1*gkvperp*gkvperp * &
                             (kp2*gkca/(2*om_eig**2) + gkcb*(gkcb-gkca) )&
                           )
                ! We can conjugate only this portion if ky < 0, since aparc and
                ! apar_spatial(:) were both conjugated in gkeig_set_fields
                if ( conj ) then
                  g_gk_without_A = conjg(g_gk_without_A)
                end if

                ! Add distribution function computed for this mode to the
                ! total distribution function at this grid point.
                g(:,isgn,iglo) = g(:,isgn,iglo) + &
                                 g_gk_without_A * apar_spatial(:,igk)
                ! This allows multiple modes to contribute to the same grid
                ! point, for example, k1=(1,0,1) and k2 = (1,0,-1).
                ! Since our definition for g depends only on the AGC and
                ! A_parallel contribution computed for each mode and not the
                ! total fields (which already represent the sum of each mode),
                ! this method correctly accounts for multiple modes with the
                ! same perpendicular wavenumber.

                ! If we will be writing spectral distribution functions:
                if(gk_write_g) then
                  g_kz_by_mode(igk,is,ie,il+(2*nlambda+1-2*il)*(isgn-1)) = &
                  g_gk_without_A * aparc ! This convoluted term means that as
                                         ! il advances, when isgn=1, the index
                                         ! steps up 1,2,3,4,5... and when
                                         ! isgn=2, the index steps down
                                         ! (2*nlambda), (2*nlambda-1),... etc.
                                         ! This is important because nlambda and
                                         ! nlambda+1 are 'adjacent', and this
                                         ! continuity allows for correct
                                         ! rendering of pm3d surfaces in
                                         ! gnuplot.        
                end if
                
              end do
            end if
          end do
        end do
      end do
    end do
    
    ! If gkeig_write_eigen is to write distribution function information:
    if (gk_write_g) then
      !Sum_reduce to get all values of array to proc 0
      !Reshape to 1-D array for MPI
      g_kz_1d=reshape(g_kz_by_mode,(/ nk_eigen*nspec*negrid*2*nlambda /))
      !Sum flattened g_kz_by_mode across all processors
      call sum_reduce(g_kz_1d,0)
      !Reshape back to 4-D array
      g_kz_by_mode=reshape(g_kz_1d,(/ nk_eigen,nspec,negrid,2*nlambda /))
      ! Now we are done with g_kz_1d
      deallocate(g_kz_1d)
    end if

    gnew = g

  end subroutine gkeig_populate_g

!-------------------------------------------------------------------------------
! Supporting subroutines
!-------------------------------------------------------------------------------

  integer function it_conjug_ky0(it)
    use kgrids, only: ikx, nakx
    !<doc> INFO (GGH):
    ! Determine index of conjugate value of it for ky=0 modes
    !</doc>
    integer, intent(in) :: it
    
    it_conjug_ky0=mod(-ikx(it)+nakx,nakx)+1
    
  end function it_conjug_ky0

  subroutine get_corrected_indices(kxin,kyin,kxout,kyout,conj)
    !<doc> INFO (KN):
    ! subroutine get_corrected_indices(kxin,kyin,kzin,kxout,kyout,kzout,conj)
    ! Maps "physical" kx,ky, and kz to their appropriate indices in AstroGK.
    ! Returns a boolean, "conj", that tracks whether spectral fields must be
    ! complex conjugated (which they must be if ky < 0).  Since this subroutine
    ! makes use of the reality condition A(k) = A*(-k) and kz -> -kz, the net
    ! effect on the z axis is no change.  Moreover, the value of kz should not
    ! be modified where the dispersion relation is concerned.  Thus,
    ! applications of "conj" do not modify fourier coefficients in z.
    !</doc>
    use kgrids, only: nakx,box
    
    integer, intent(out) :: kxout,kyout
    logical, intent(out) :: conj  ! If ky < 0, we will need to conjugate fields
    integer, intent(in)  :: kxin,kyin
    
    !If we are using gridopt_box (which in general, we should):
    if ( box ) then
      if (kyin .lt. 0) then
      ! Using f(k)=f*(-k) to remap ky < 0 modes to ky > 0.

        kyout = 1 + abs(kyin)
        kxout = 1 + mod(-kxin+nakx,nakx)
        conj  = .true.
      else
      ! Regular mapping, kx=0 is 1st kx element, kx=1 is 2nd kx element, etc.
      ! See kgrids.f90 for details
        kyout = 1 + kyin
        kxout = 1 + mod(kxin+nakx,nakx)
        conj  = .false.
      end if
    !We have used gridopt_single
    else
      kxout = 1
      kyout = 1
    end if

  end subroutine get_corrected_indices

!-------------------------------------------------------------------------------
  subroutine gkeig_elgrids_to_vgrids
    !<doc> INFO (KN):
    ! subroutine gkeig_elgrids_to_vgrids
    ! Subroutine for mapping energy/pitch-angle velocity grids to
    ! parallel/perpendicular velocity grids.
    ! This subroutine supports the gkeig_write_eigen subroutine by providing
    ! velocity information in the same array format as distribution function
    ! information is stored.
    ! Should only be called if gk_write_g is true, otherwise it has no purpose.
    !</doc>

    use mp, only: proc0
    use agk_mem, only: alloc8
    use le_grids, only: negrid,nlambda,al,e
    use species, only:nspec
    integer :: ie,il,is,illp,illm
    real    :: al1,e1,vperp,vpar
    
    if (proc0) then
      if (gk_write_g) then
      ! Set up velocity space information in the same style as g_kz.
        allocate (g_kz_vperp(nspec,negrid,2*nlambda))
        call alloc8(r3=g_kz_vperp,v="g_kz_vperp")
        allocate (g_kz_vpar(nspec,negrid,2*nlambda))
        call alloc8(r3=g_kz_vpar,v="g_kz_vpar")
        
        do is=1,nspec
          do ie=1,negrid
            do il=1,nlambda

              al1   = al(il)
              e1    =  e(ie,is)
              ! maps il as (1:nlambda;-nlambda:-1) to ensure that nlambda
              ! and nlambda+1 are "adjacent" in velocity space.
              illp  = il
              illm  = 2*nlambda+1-il
              ! Computed manually since init_dist_fn hasn't been run yet
              vpar  = sqrt(e1*(1-al1))
              vperp = sqrt(e1*al1)

              g_kz_vperp(is,ie,illp)=vperp
              g_kz_vperp(is,ie,illm)=vperp
              g_kz_vpar(is,ie,illp)=vpar
              g_kz_vpar(is,ie,illm)=-vpar

            end do
          end do
        end do

      end if
    end if

  end subroutine gkeig_elgrids_to_vgrids
  
!-------------------------------------------------------------------------------

  subroutine gkeig_write_eigen
    !<doc> INFO (KN):
    ! subroutine gkeig_write_eigen
    ! This subroutine is responsible for writing eigenvalue-related information
    ! to the file "<input>.eigen".  Will only be called if "gk_write_disp" is 
    ! true.  Will also write distribution function information for each species
    ! (including tracer species) if "gk_write_g" is true.
    ! Successive values of il are printed one after the other, while each block
    ! of il within a single ie are separated by one blank line.  This allows
    ! gnuplot's pm3d feature to successfully identify which values are 'x'
    ! values and which are 'y' values.  Each value of is (the species blocks)
    ! is separated by two blank lines, to allow gnuplot to select a single
    ! species to plot through use of gnuplot's index feature.
    !</doc>
    use file_utils, only: open_output_file,close_output_file
    use le_grids, only:negrid,nlambda
    use kgrids, only:kperp2
    use species, only:spec,nspec
    
    integer :: unit,igk,ie,il,is,it,ik
    real    :: om_eig_real,om_eig_im,vperp,vpar,g_kz_real,g_kz_im,kp2
    real    :: j0i,j1i
    complex :: g_kz
    complex :: om_eig
    logical :: conj

    call open_output_file(unit, ".eigen")
    
      do igk = 1,nk_eigen
        write(unit,'(a,i3.3)') "# gk_eigen_",igk
        write(unit,'(a)') "# INPUTS"
        write(unit,'(3(a,g14.6))') "# beta_0 = ",beta," amp: ",&
                                   amp_eigen(igk)," phase: ",phase_eigen(igk)
        call get_corrected_indices(kx_eigen(igk),ky_eigen(igk),it,ik,conj)
        kp2=kperp2(it,ik)
        write(unit,'(3(a6,i4),a6,g14.6)') &
                   "# kx= ",kx_eigen(igk),"  ky= ",ky_eigen(igk),&
                   "  kz= ",kz_eigen(igk)," kp2= ",kp2

                           ! sp in format code forces display of +/-
        write(unit,'(a,g16.8,sp,g16.8,a2)') "# Initial frequency guess: ",&
                                          real(om_est_eigen(igk)),&
                                          aimag(om_est_eigen(igk))," i"
        write(unit,'(a,g12.4)') "# Root finding precision: ",prec_eigen(igk)
                                          
        write(unit,'(a)') "# DISPERSION SOLUTION"
        om_eig = om_eig_eigen(igk)
        om_eig_real=real(om_eig)
        om_eig_im=aimag(om_eig)
        
        gkca=gka_eigen(igk)
        gkcb=gkb_eigen(igk)
        gkcc=gkc_eigen(igk)
        gkcd=gkd_eigen(igk)
        gkce=gke_eigen(igk)
        
        write(unit,'(2(a12,g20.12))') "# Re(om): ",om_eig_real,&
                             "  Im(om): ",om_eig_im
        write(unit,'(2(a12,g20.12))') "# Re(apar): ",real(apar_eigen(igk)),&
                             "  Im(apar): ",aimag(apar_eigen(igk))
        write(unit,'(2(a12,g20.12))') "# Re(bpar): ",real(bpar_eigen(igk)),&
                             "  Im(bpar): ",aimag(bpar_eigen(igk))
        write(unit,'(2(a12,g20.12))') "# Re(phi): ",real(phi_eigen(igk)),&
                             "  Im(phi): ",aimag(phi_eigen(igk))
        write(unit,'(2(a12,g20.12))') "# Re(gka): ",real(gkca),&
                                      "  Im(gka): ",aimag(gkca)
        write(unit,'(2(a12,g20.12))') "# Re(gkb): ",real(gkcb),&
                                      "  Im(gkb): ",aimag(gkcb)
        write(unit,'(2(a12,g20.12))') "# Re(gkc): ",real(gkcc),&
                                      "  Im(gkc): ",aimag(gkcc)
        write(unit,'(2(a12,g20.12))') "# Re(gkd): ",real(gkcd),&
                                      "  Im(gkd): ",aimag(gkcd)
        write(unit,'(2(a12,g20.12))') "# Re(gke): ",real(gkce),&
                                      "  Im(gke): ",aimag(gkce)

        if (gk_write_g) then
          do is=1,nspec
            write(unit,'(a,i2,a)') &
                       "# Distribution Function for species with index ",is,":"
            write(unit,'(4(a7,g12.4))') &
                       "# m_s= ",spec(is)%mass,"  q_s= ",spec(is)%z,&
                       "  n_s= ",spec(is)%dens,"  T_s= ",spec(is)%temp
            write(unit,'(2(a5),4(a16))') &
                       "#  ie","il","vperp","vpar","Re(g)","Im(g)"
            
            do ie=1,negrid
              do il=1,2*nlambda
                vperp=g_kz_vperp(is,ie,il)
                vpar=g_kz_vpar(is,ie,il)
                g_kz=g_kz_by_mode(igk,is,ie,il)
                g_kz_real=real(g_kz)
                g_kz_im=aimag(g_kz)
                write(unit,'(2(i5),4(g16.8))')ie,il,vperp,vpar,g_kz_real,g_kz_im
              end do
              write(unit,*) !Add one blank line for gnuplot's pm3d feature
            end do

            write(unit,*) !Add two blank lines for readability and use of
            write(unit,*) !gnuplot's index feature
            
          end do
        end if
      end do
    if (debug) write(*,*) "gkeig_write_eigen: Closing eigen output."
    call close_output_file(unit)
    if (debug) write(*,*) "gkeig_write_eigen: output closed."
    
  end subroutine gkeig_write_eigen

!-------------------------------------------------------------------------------

  subroutine gk_eigfields(istep,header)
    !<doc> INFO (KN):
    ! subroutine gk_eigfields(istep,header)
    ! Subroutine to write out field data at intervals of nwrite_eigfields steps.
    ! Writes real and imaginary phi, A_par, and B_par vs z and t. Opens a
    ! separate file for each eigenmode to be driven, "<input>.n.eigefields",
    ! where n is the index of the mode.
    ! Both istep and header are optional parameters.  Passing istep will require
    ! istep to be a multiple of nwrite_eigfields in order to output, useful
    ! within diagnostic loops.  Omitting istep will force gk_eigfields to run
    ! regardless of the present timestep.
    ! The parameter header allows an up-to-80 character array to be passed to
    ! gk_eigfields, which will be printed on a line immediately preceding the
    ! gk_eigfields field output.  This is useful for identifying a particular
    ! step (if gk_eigfields were to be called as a diagnostic after a particular
    ! operation, e.g. after g_adjust, header="Fields after g_adjust").
    !</doc>
    use file_utils, only: open_output_file
    use theta_grid, only: ntgrid,theta
    use kgrids, only: aky,akx,nakx,naky
    use fields_arrays, only:apar,bpar,phi
    use agk_time, only: time
    use mp, only: proc0
    use agk_mem, only: alloc8
    
    integer,optional,intent(in) :: istep
    integer :: ik,it,ig,igk,step
    character(30) :: modeidx,extension
    character (*),optional,intent(in) :: header
    character(80) :: lheader
    logical :: conj ! Only here to make get_corrected_indices behave

    if (proc0) then
    
      if ( .not. eigfields_inited) then
        eigfields_inited = .true.
        allocate(mode_init(nk_eigen)) !No alloc8 interface for logical variables
        allocate(unit_list(nk_eigen)); call alloc8(i1=unit_list,v="unit_list")
        mode_init=.false.
      end if
    
      if (present(istep)) then
        step=istep
        if ((mod(step,nwrite_eigfields) .gt. 0)) then
          return
        end if
      end if

      do igk=1,nk_eigen
        if ( .not. mode_init(igk) ) then
          mode_init(igk)=.true.
          write(modeidx,*) igk
          modeidx=adjustl(modeidx)
          extension="."//trim(modeidx)//".eigfields"
          call open_output_file (unit_list(igk), trim(extension))
          if (present(header)) then
            lheader=adjustl("# "//trim(header))
            write(unit_list(igk),*) lheader
          end if
          call get_corrected_indices(kx_eigen(igk),ky_eigen(igk),it,ik,conj)
          write(unit_list(igk),'(2(a7,e12.5),sp,2(a7,i3))')&
                               "# akx= ",akx(it),"  aky= ",aky(ik),&
                               " kx= ",kx_eigen(igk)," ky= ",ky_eigen(igk)
          write(unit_list(igk),'(a4,a12,a3,7(1x,a12))') "#   ","time","   ",&
                               "z (theta)","Re(phi)","Im(phi)","Re(apar)",&
                               "Im(apar)","Re(bpar)","Im(bpar)"
          if (debug) write(*,*) "eigfield file with igk=",igk," initialized."
        else
          call get_corrected_indices(kx_eigen(igk),ky_eigen(igk),it,ik,conj)
        end if
        do ig = -ntgrid, ntgrid
          write (unit_list(igk), '(a4,e12.5,a3,7(1x,e12.5))') " t= ",time,&
                " z=",theta(ig), phi(ig,it,ik), apar(ig,it,ik), bpar(ig,it,ik)
        end do
        write (unit_list(igk), "()")
        if (eigfields_gnuplot_spacing) then
          write (unit_list(igk), "()")
          ! Skip two lines to allow gnuplot's index feature to function.
          ! Each call to gk_eigfields will produce a separate index that may be
          ! addressed by gnuplot, allowing time ranges to be easily plotted.
          ! If gk_gnuplot_spacing is false, skip only one line.
        end if
      end do
    else
      write (error_unit(),*) "gkeigfields called by a processor other than &
                          &    proc0."
      return
    end if

  end subroutine gk_eigfields
  
!-------------------------------------------------------------------------------

  subroutine close_gk_eigfields
    !<doc> INFO (KN):
    ! Allows external subroutines to close files associated with gk_eigfields,
    ! necessary since gk_eigfields may be repeatedly called after the gk_eigen
    ! module is otherwise finished.  (Need to wait until agk_diagnostics is done
    ! looping before we close.)
    ! This subroutine will check to see that gk_eigfields has been initialized.
    ! If it has not, then this subroutine will do nothing.  Therefore, it should
    ! be able to be safely called at any point after gk_eigen is called without
    ! affecting the stability of the code, regardless of whether gk_eigfields
    ! has been used.
    !</doc>
    use file_utils, only: close_output_file
    integer :: igk

    if (eigfields_inited) then
      do igk=1,nk_eigen
        call close_output_file (unit_list(igk))
        if(debug) write(*,*) "eigfield file with igk=",igk," closed."
      end do
      ! Now that we are done with this information, deallocate these arrays
      deallocate(mode_init); deallocate(unit_list)
    end if

    !These two arrays are needed for gk_eigfields, so assign responsibility
    !for deallocating them to close_gk_eigfields.
    deallocate(kx_eigen); deallocate(ky_eigen)

  end subroutine close_gk_eigfields
  
!-------------------------------------------------------------------------------
! Extras
!-------------------------------------------------------------------------------

  subroutine gkeig_kgrid_info
    !<doc> INFO (KN):
    ! Provides information about kgrids in supplemental data file 
    ! "<input>.kgrids".  For each value of it (kx) and ik (ky), provides the
    ! "physical" index of the mode (ixk and iky), the dimensionless wavenumbers
    ! of the mode (akx and aky), and the dimensionless value of (kperp*rho0)**2
    ! (kp2).
    !</doc>
    use kgrids, only:nakx,naky,kperp2,ikx,iky,akx,aky
    use file_utils, only:open_output_file

    integer :: kunit,likx,liky,it,ik
    real :: kp2,lakx,laky
    
    call open_output_file(kunit,".kgrid")
    
    write(kunit,*) "nakx= ",nakx," naky= ",naky
    do it=1,nakx
      likx=ikx(it)
      lakx=akx(it)
      do ik=1,naky
        liky=iky(ik)
        laky=aky(ik)
        kp2 = kperp2(it,ik)
        write(kunit,'(4(a6,i3),3(a6,g14.6))') "  it= ",it,"  ik= ",ik, &
        " ixk= ",likx," iky= ",liky," akx= ",lakx," aky= ",laky," kp2= ",kp2
      end do
    end do
    close(kunit)
  end subroutine gkeig_kgrid_info
!-------------------------------------------------------------------------------

  subroutine gkeig_vgrid_info
    !<doc> INFO (KN):
    ! subroutine gkeig_vgrid_info
    ! Provides information about vgrids in supplemental data file
    ! "<input>.vgrid".  Provides velocity information for each species, although
    ! each species' output will look identical since their grids are identical
    ! with respect to their thermal velocity.
    !</doc>
    use le_grids, only: negrid,nlambda,e,al
    use dist_fn_arrays, only: vpa,vperp2
    use agk_layouts, only:idx,g_lo
    use file_utils, only:open_output_file
    use species, only:nspec,spec
    
    integer :: vunit,ie,il,is
    real :: gkvpar,gkvperp
    
    call open_output_file(vunit,".vgrid")
    
    do is=1,nspec
      write(vunit,'(a,i0,a,i0)')"#negrid= ",negrid," nlambda= ",nlambda
      write(vunit,'(a)')"#Species 1, type ",spec(is)%type," (1=i,2=e)"
      write(vunit,'(a80)')"#-------------------------------------------------&
                          &------------------------------"
      do ie=1,negrid
        do il=1,nlambda
          gkvpar = sqrt(e(ie,is)*max(0.0, 1.0 - al(il)))
          gkvperp = sqrt(al(il)*e(ie,is))

! The following two lines will cause a segfault when run on more than one
! processor, since there will be indices iglo that are not contained on a
! single processor.  This section was used for debugging to make sure that
! the velocity grids were being initialized correctly, but since they are,
! it suffices to compute the velocity values directly.
!
!          vparin=vpa(1,idx(g_lo,1,1,il,ie,1))
!          vperpin=sqrt(vperp2(idx(g_lo,1,1,il,ie,1)))

          write(vunit,'(2(a5,i3),2(a9,g16.8))') "  ie=",ie,"  il=",il,&
                             "   vpar= ",gkvpar," vperp= ",gkvperp
        end do
      end do
      write(vunit,*)
    end do
    close(vunit)
  end subroutine gkeig_vgrid_info

!-------------------------------------------------------------------------------

  subroutine gkeig_cleanup
    !<doc> INFO (KN):
    ! subroutine gk_eigen_cleanup
    ! Deallocate memory from all arrays that were used for creating eigenvalue
    ! solutions and populating distribution functions.
    !</doc>

    !NOTE: These are not accounted for in memory  using dealloc8 calls!

  use mp, only: proc0
  !kx_eigen and ky_eigen are deallocated by close_gk_eigfields
  deallocate(kz_eigen)
  deallocate (amp_eigen); deallocate(phase_eigen)
  deallocate (prec_eigen); deallocate(tol_eigen)
  deallocate (om_est_eigen)
  deallocate (localized_eigen);  deallocate (z0_eigen)
  deallocate (deltz_eigen); deallocate(power_eigen)

  
  deallocate(gka_eigen); deallocate(gkb_eigen); deallocate(gkc_eigen)
  deallocate(gkd_eigen); deallocate(gke_eigen)
  deallocate(gkx_eigen,gky_eigen,gkz_eigen)
  deallocate(om_eig_eigen)
  deallocate(apar_eigen); deallocate(bpar_eigen); deallocate(phi_eigen)
  deallocate(apar_spatial)
  
  if (gk_write_g) then
    deallocate (g_kz_by_mode)
    if (proc0) then
      deallocate (g_kz_vperp)
      deallocate (g_kz_vpar)
    end if
  end if

  end subroutine gkeig_cleanup

!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Numerical routines supporting eigenvalue solver
!-------------------------------------------------------------------------------

  function bessi0e(x)
!-------------------------------------------------------------------------------
!    <doc> INFO (KN):
!     function bessi0e(x)
!     Based on function besi0e(x) included in fn library
!     (http://netlib3.cs.utk.edu/fn/besi0e.f)
!     I have removed the Chebyshev coefficients from the original code for the
!     sake of compactness; they are the same as in the uncommented, implemented
!     code.
!
!     Have omitted the inits statements... these serve to identify how many
!     terms are necessary to reach machine precision, but this implementation
!     simply uses all terms.  These functions are presently called only by
!     gk_eigen, and shouldn't be called too frequently, so this inefficiency
!     should be forgivable.
!    </doc>
!-------------------------------------------------------------------------------
!<Original Code>
!        function besi0e (x)
!c july 1977 version.  w. fullerton, c3, los alamos scientific lab.
!      dimension bi0cs(12), ai0cs(21), ai02cs(22)
!      external csevl, exp, inits, r1mach, sqrt
!c
!c series for bi0        on the interval  0.          to  9.00000d+00
!c                                        with weighted error   2.46e-18
!c                                         log weighted error  17.61
!c                               significant figures required  17.90
!c                                    decimal places required  18.15
!c
!c
!c series for ai0        on the interval  1.25000d-01 to  3.33333d-01
!c                                        with weighted error   7.87e-17
!c                                         log weighted error  16.10
!c                               significant figures required  14.69
!c                                    decimal places required  16.76
!c
!c
!c series for ai02       on the interval  0.          to  1.25000d-01
!c                                        with weighted error   3.79e-17
!c                                         log weighted error  16.42
!c                               significant figures required  14.86
!c                                    decimal places required  17.09
!c
!c
!      data nti0, ntai0, ntai02, xsml / 3*0, 0. /
!c
!      if (nti0.ne.0) go to 10
!      nti0 = inits (bi0cs, 12, 0.1*r1mach(3))
!      ntai0 = inits (ai0cs, 21, 0.1*r1mach(3))
!      ntai02 = inits (ai02cs, 22, 0.1*r1mach(3))
!      xsml = sqrt (4.0*r1mach(3))
!c
! 10   y = abs(x)
!      if (y.gt.3.0) go to 20
!c
!      besi0e = 1.0
!      if (y.gt.xsml) besi0e = exp(-y) * ( 2.75 +
!     1  csevl (y*y/4.5-1.0, bi0cs, nti0) )
!      return
!c
! 20   if (y.le.8.) besi0e = (.375 + csevl ((48./y-11.)/5., ai0cs, ntai0)
!     1  ) / sqrt(y)
!      if (y.gt.8.) besi0e = (.375 + csevl (16./y-1., ai02cs, ntai02))
!     1  / sqrt(y)
!c
!      return
!      end
!</Original Code>
!-------------------------------------------------------------------------------

    implicit none
    real              :: bessi0e,xsml,y
    real, intent(in)  :: x
    
    real, parameter :: bi0cs(12) =  (/ &
         -.07660547252839144951e0,     &
         1.927337953993808270e0,       &
          .2282644586920301339e0,      &
          .01304891466707290428e0,     &
          .00043442709008164874e0,     &
          .00000942265768600193e0,     &
          .00000014340062895106e0,     &
          .00000000161384906966e0,     &
          .00000000001396650044e0,     &
          .00000000000009579451e0,     &
          .00000000000000053339e0,     &
          .00000000000000000245e0    /)
    
    real, parameter :: ai0cs(21) =  (/ &
          .07575994494023796e0,        &
          .00759138081082334e0,        &
          .00041531313389237e0,        &
          .00001070076463439e0,        &
         -.00000790117997921e0,        &
         -.00000078261435014e0,        &
          .00000027838499429e0,        &
          .00000000825247260e0,        &
         -.00000001204463945e0,        &
          .00000000155964859e0,        &
          .00000000022925563e0,        &
         -.00000000011916228e0,        &
          .00000000001757854e0,        &
          .00000000000112822e0,        &
         -.00000000000114684e0,        &
          .00000000000027155e0,        &
         -.00000000000002415e0,        &
         -.00000000000000608e0,        &
          .00000000000000314e0,        &
         -.00000000000000071e0,        &
          .00000000000000007e0       /)
      
    real, parameter :: ai02cs(22) = (/ &
          .05449041101410882e0,        &
          .00336911647825569e0,        &
          .00006889758346918e0,        &
          .00000289137052082e0,        &
          .00000020489185893e0,        &
          .00000002266668991e0,        &
          .00000000339623203e0,        &
          .00000000049406022e0,        &
          .00000000001188914e0,        &
         -.00000000003149915e0,        &
         -.00000000001321580e0,        &
         -.00000000000179419e0,        &
          .00000000000071801e0,        &
          .00000000000038529e0,        &
          .00000000000001539e0,        &
         -.00000000000004151e0,        &
         -.00000000000000954e0,        &
          .00000000000000382e0,        &
          .00000000000000176e0,        &
         -.00000000000000034e0,        &
         -.00000000000000027e0,        &
          .00000000000000003e0       /)
    
    ! epsilon(1.0) should be the same thing as r1mach(3)
    xsml = sqrt(4.0*epsilon(1.0))
    y = abs(x)
    
    if (y .gt. 3.) then
      if (y .gt. 8.) then
        bessi0e = (0.375 + csevl(16./y - 1., ai02cs, 22))/sqrt(y)
      else
        bessi0e = (0.375 + csevl((48./y - 11.)/5.,ai0cs,21))/sqrt(y)
      endif
    else if (y .gt. xsml) then
      bessi0e = exp(-y) * (2.75 + csevl(y*y/4.5 - 1.0, bi0cs, 12))
    else
      bessi0e=1.0
    endif
    return
  end function bessi0e

!-------------------------------------------------------------------------------

  function bessi1e(x)
!-------------------------------------------------------------------------------
!  <doc> INFO (KN):
!    function bessi1e(x)
!    Based on function besi1e(x) included in fn library
!    (http://netlib3.cs.utk.edu/fn/besi1e.f)
!    Original code below has had Chebyshev coefficients removed for
!    compactness; they are the same as in the uncommented code.
!
!    Have omitted the inits statements... these serve to identify how many
!    terms are necessary to reach machine precision, but this implementation
!    simply uses all terms.  These functions are presently called only by
!    gk_eigen, and shouldn't be called too frequently, so this inefficiency
!    should be forgivable.
!  </doc>
!-------------------------------------------------------------------------------
!<Original Code>
!      function besi1e (x)
!c oct 1983 version.  w. fullerton, c3, los alamos scientific lab.
!      dimension bi1cs(11), ai1cs(21), ai12cs(22)
!      external csevl, exp, inits, r1mach, sqrt
!c
!c series for bi1        on the interval  0.          to  9.00000d+00
!c                                        with weighted error   2.40e-17
!c                                         log weighted error  16.62
!c                               significant figures required  16.23
!c                                    decimal places required  17.14
!c

!c
!c series for ai1        on the interval  1.25000d-01 to  3.33333d-01
!c                                        with weighted error   6.98e-17
!c                                         log weighted error  16.16
!c                               significant figures required  14.53
!c                                    decimal places required  16.82
!c

!c
!c series for ai12       on the interval  0.          to  1.25000d-01
!c                                        with weighted error   3.55e-17
!c                                         log weighted error  16.45
!c                               significant figures required  14.69
!c                                    decimal places required  17.12
!c
!c
!      data nti1, ntai1, ntai12, xmin, xsml / 3*0, 2*0. /
!c
!      if (nti1.ne.0) go to 10
!      nti1 = inits (bi1cs, 11, 0.1*r1mach(3))
!      ntai1 = inits (ai1cs, 21, 0.1*r1mach(3))
!      ntai12 = inits (ai12cs, 22, 0.1*r1mach(3))
!c
!      xmin = 2.0*r1mach(1)
!      xsml = sqrt (8.0*r1mach(3))
!c
! 10   y = abs(x)
!      if (y.gt.3.0) go to 20
!c
!      besi1e = 0.0
!      if (y.eq.0.0) return
!c
!      if (y.le.xmin) call seteru (
!     1  37hbesi1e  abs(x) so small i1 underflows, 37, 1, 0)
!      besi1e = 0.0
!      if (y.gt.xmin) besi1e = 0.5*x
!      if (y.gt.xsml) besi1e = x * (.875 + csevl(y*y/4.5-1., bi1cs,nti1))
!      besi1e = exp(-y) * besi1e
!      return
!c
! 20   if (y.le.8.) besi1e = (.375 + csevl ((48./y-11.)/5., ai1cs, ntai1)
!     1  ) / sqrt(y)
!      if (y.gt.8.) besi1e = (.375 + csevl (16./y-1.0, ai12cs, ntai12))
!     1  / sqrt(y)
!      besi1e = sign (besi1e, x)
!c
!      return
!      end
!</Original Code>
!-------------------------------------------------------------------------------
    implicit none
    real              :: bessi1e,xsml,xmin,y
    real, intent(in)  :: x
    
    real, parameter :: bi1cs(11) =  (/ &
         -.001971713261099859e0,       &
          .40734887667546481e0,        &
          .034838994299959456e0,       &
          .001545394556300123e0,       &
          .000041888521098377e0,       &
          .000000764902676483e0,       &
          .000000010042493924e0,       &
          .000000000099322077e0,       &
          .000000000000766380e0,       &
          .000000000000004741e0,       &
          .000000000000000024e0      /)
    real, parameter :: ai1cs(21) =  (/ &  
         -.02846744181881479e0,        &
         -.01922953231443221e0,        &
         -.00061151858579437e0,        &
         -.00002069971253350e0,        &
          .00000858561914581e0,        &
          .00000104949824671e0,        &
         -.00000029183389184e0,        &
         -.00000001559378146e0,        &
          .00000001318012367e0,        &
         -.00000000144842341e0,        &
         -.00000000029085122e0,        &
          .00000000012663889e0,        &
         -.00000000001664947e0,        &
         -.00000000000166665e0,        &
          .00000000000124260e0,        &
         -.00000000000027315e0,        &
          .00000000000002023e0,        &
          .00000000000000730e0,        &
         -.00000000000000333e0,        &
          .00000000000000071e0,        &
         -.00000000000000006e0       /)
      
    real, parameter :: ai12cs(22) = (/ &
          .02857623501828014e0,        &
         -.00976109749136147e0,        &
         -.00011058893876263e0,        &
         -.00000388256480887e0,        &
         -.00000025122362377e0,        &
         -.00000002631468847e0,        &
         -.00000000383538039e0,        &
         -.00000000055897433e0,        &
         -.00000000001897495e0,        &
          .00000000003252602e0,        &
          .00000000001412580e0,        &
          .00000000000203564e0,        &
         -.00000000000071985e0,        &
         -.00000000000040836e0,        &
         -.00000000000002101e0,        &
          .00000000000004273e0,        &
          .00000000000001041e0,        &
         -.00000000000000382e0,        &
         -.00000000000000186e0,        &
          .00000000000000033e0,        &
          .00000000000000028e0,        &
         -.00000000000000003e0       /)
      
      !tiny(1.0) should be the same as r1mach(1)
      xmin = 2.0*tiny(1.0)
      !epsilon(1.0) should be the same as r1mach(3)
      xsml = sqrt(8.0*epsilon(1.0))
      
      y = abs(x)
      if (y .gt. 3.0 ) then
        if (y .gt. 8.0) then
          bessi1e = (0.375 + csevl(16./y - 1.0,ai12cs,22))/sqrt(y)
        else
          bessi1e=(0.375 + csevl((48./y - 11.)/5.,ai1cs,21))/sqrt(y)
        endif
        bessi1e = sign(bessi1e, x)
      else if (y .gt. xsml ) then
        bessi1e = x * (0.875 + csevl(y*y/4.5 - 1., bi1cs, 11))
        bessi1e = bessi1e * exp(-y)
      else if (y .gt. xmin) then 
        bessi1e = 0.5 * x
        bessi1e = bessi1e * exp(-y)
      else
        !Does not distinguish between 0.0 and small arguments that cause
        !underflow, since underflow should never be an issue in the context
        !of AstroGK (arguments to bessi1 are alpha_s).
        bessi1e = 0.0
      endif
      
  end function bessi1e

!-------------------------------------------------------------------------------

  function csevl(x,cs,n)
!-------------------------------------------------------------------------------
!    <doc> INFO (KN):
!     function csevl(x,cs,n)
!     Based on function csevl(x,cs,n) included in fn library
!     (http://netlib3.cs.utk.edu/fn/csevl.f)
!     Not sure why only half the first coefficient is summed, or if this is the
!     right thing to do.  It is the function on which bessi0e and bessi0i, which
!     are also taken from the fn library, are based.
!    </doc>
!-------------------------------------------------------------------------------
!<Original Code>
!      function csevl (x, cs, n)
!c april 1977 version.  w. fullerton, c3, los alamos scientific lab.
!c
!c evaluate the n-term chebyshev series cs at x.  adapted from
!c r. broucke, algorithm 446, c.a.c.m., 16, 254 (1973).  also see fox
!c and parker, chebyshev polys in numerical analysis, oxford press, p.56.
!c
!c             input arguments --
!c x      value at which the series is to be evaluated.
!c cs     array of n terms of a chebyshev series.  in eval-
!c        uating cs, only half the first coef is summed.
!c n      number of terms in array cs.
!c
!      dimension cs(1)
!c
!      if (n.lt.1) call seteru (28hcsevl   number of terms le 0, 28, 2,2)
!      if (n.gt.1000) call seteru (31hcsevl   number of terms gt 1000,
!     1  31, 3, 2)
!      if (x.lt.(-1.1) .or. x.gt.1.1) call seteru (
!     1  25hcsevl   x outside (-1,+1), 25, 1, 1)
!c
!      b1 = 0.
!      b0 = 0.
!      twox = 2.*x
!      do 10 i=1,n
!        b2 = b1
!        b1 = b0
!        ni = n + 1 - i
!        b0 = twox*b1 - b2 + cs(ni)
! 10   continue
!c
!      csevl = 0.5 * (b0-b2)
!c
!      return
!      end
!</Original Code>
!-------------------------------------------------------------------------------
    implicit none

    integer, intent(in)            :: n
    real, intent(in)               :: x
    real, dimension(:), intent(in) :: cs
    real                           :: csevl,b0,b1,b2,twox
    integer                        :: i,ni
    
    b1 = 0.0
    b0 = 0.0
    twox = 2.*x
    do i=1,n
      b2 = b1
      b1 = b0
      ni = n + 1 - i
      b0 = twox*b1 - b2 + cs(ni)
    end do
    
    csevl = 0.5 * (b0-b2)
    return
    
  end function csevl

!-------------------------------------------------------------------------------
  function plasma_disp(z)
!-------------------------------------------------------------------------------
!    <doc> INFO (KN):
!     function plasma_disp(z)
!     Based on subroutine wofz(xi,yi,u,v,flag) included in the toms archive
!     of netlib, from the ACM collected algorithms (CALGO).
!     (http://netlib3.cs.utk.edu/toms/680)
!     Using this function based on a note by Greg Hammett:
!     # The plasma dispersion function Z(z) is related to the w(z) function
!     # calculated by wofz.f by the simple relationship:
!     # 
!     #     Z(z) = i*sqrt(Pi) * w(z)
!     # 
!     # w(z) is sometimes called the Voight function or Faddeeva's function, and
!     # is related to the complementary error function by:
!     # 
!     #     w(z) = exp(-z**2)erfc(-iz)
!     # 
!     #     w(z) = exp(-z**2)(1 - erf(-iz))
!     # 
!     # The plasma dispersion function  Pdf(z) = Z(z) is usually defined as
!     # 
!     #     Z(z) = 1/SQRT(Pi) * Integral(-Inf,+Inf) exp(-t**2)/(t-z) dt ,
!     # 
!     # (this form is valid only for z in the upper half complex plane...)
!     (from http://www.pppl.gov/~hammett/comp/src/wofz_readme.html)
!     Rewritten as a complex function rather than a subroutine.
!     In the context of AstroGK and gyrokinetics, we should never come close
!     to overflowing, and so this error checking has been removed to facilitate
!     the conversion to a function rather than a subroutine.
!    </doc>
!-------------------------------------------------------------------------------
!C      ALGORITHM 680, COLLECTED ALGORITHMS FROM ACM.
!C      THIS WORK PUBLISHED IN TRANSACTIONS ON MATHEMATICAL SOFTWARE,
!C      VOL. 16, NO. 1, PP. 47.
!      SUBROUTINE WOFZ (XI, YI, U, V, FLAG)
!C
!C  GIVEN A COMPLEX NUMBER Z = (XI,YI), THIS SUBROUTINE COMPUTES
!C  THE VALUE OF THE FADDEEVA-FUNCTION W(Z) = EXP(-Z**2)*ERFC(-I*Z),
!C  WHERE ERFC IS THE COMPLEX COMPLEMENTARY ERROR-FUNCTION AND I
!C  MEANS SQRT(-1).
!C  THE ACCURACY OF THE ALGORITHM FOR Z IN THE 1ST AND 2ND QUADRANT
!C  IS 14 SIGNIFICANT DIGITS; IN THE 3RD AND 4TH IT IS 13 SIGNIFICANT
!C  DIGITS OUTSIDE A CIRCULAR REGION WITH RADIUS 0.126 AROUND A ZERO
!C  OF THE FUNCTION.
!C  ALL REAL VARIABLES IN THE PROGRAM ARE DOUBLE PRECISION.
!C
!C
!C  THE CODE CONTAINS A FEW COMPILER-DEPENDENT PARAMETERS :
!C     RMAXREAL = THE MAXIMUM VALUE OF RMAXREAL EQUALS THE ROOT OF
!C                RMAX = THE LARGEST NUMBER WHICH CAN STILL BE
!C                IMPLEMENTED ON THE COMPUTER IN DOUBLE PRECISION
!C                FLOATING-POINT ARITHMETIC
!C     RMAXEXP  = LN(RMAX) - LN(2)
!C     RMAXGONI = THE LARGEST POSSIBLE ARGUMENT OF A DOUBLE PRECISION
!C                GONIOMETRIC FUNCTION (DCOS, DSIN, ...)
!C  THE REASON WHY THESE PARAMETERS ARE NEEDED AS THEY ARE DEFINED WILL
!C  BE EXPLAINED IN THE CODE BY MEANS OF COMMENTS
!C
!C
!C  PARAMETER LIST
!C     XI     = REAL      PART OF Z
!C     YI     = IMAGINARY PART OF Z
!C     U      = REAL      PART OF W(Z)
!C     V      = IMAGINARY PART OF W(Z)
!C     FLAG   = AN ERROR FLAG INDICATING WHETHER OVERFLOW WILL
!C              OCCUR OR NOT; TYPE LOGICAL;
!C              THE VALUES OF THIS VARIABLE HAVE THE FOLLOWING
!C              MEANING :
!C              FLAG=.FALSE. : NO ERROR CONDITION
!C              FLAG=.TRUE.  : OVERFLOW WILL OCCUR, THE ROUTINE
!C                             BECOMES INACTIVE
!C  XI, YI      ARE THE INPUT-PARAMETERS
!C  U, V, FLAG  ARE THE OUTPUT-PARAMETERS
!C
!C  FURTHERMORE THE PARAMETER FACTOR EQUALS 2/SQRT(PI)
!C
!C  THE ROUTINE IS NOT UNDERFLOW-PROTECTED BUT ANY VARIABLE CAN BE
!C  PUT TO 0 UPON UNDERFLOW;
!C
!C  REFERENCE - GPM POPPE, CMJ WIJERS; MORE EFFICIENT COMPUTATION OF
!C  THE COMPLEX ERROR-FUNCTION, ACM TRANS. MATH. SOFTWARE.
!C
!*
!*
!*
!*
!      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
!*
!      LOGICAL A, B, FLAG
!      PARAMETER (FACTOR   = 1.12837916709551257388D0,
!     *           RMAXREAL = 0.5D+154,
!     *           RMAXEXP  = 708.503061461606D0,
!     *           RMAXGONI = 3.53711887601422D+15)
!*
!      FLAG = .FALSE.
!*
!      XABS = DABS(XI)
!      YABS = DABS(YI)
!      X    = XABS/6.3
!      Y    = YABS/4.4
!*
!C
!C     THE FOLLOWING IF-STATEMENT PROTECTS
!C     QRHO = (X**2 + Y**2) AGAINST OVERFLOW
!C
!      IF ((XABS.GT.RMAXREAL).OR.(YABS.GT.RMAXREAL)) GOTO 100
!*
!      QRHO = X**2 + Y**2
!*
!      XABSQ = XABS**2
!      XQUAD = XABSQ - YABS**2
!      YQUAD = 2*XABS*YABS
!*
!      A     = QRHO.LT.0.085264D0
!*
!      IF (A) THEN
!C
!C  IF (QRHO.LT.0.085264D0) THEN THE FADDEEVA-FUNCTION IS EVALUATED
!C  USING A POWER-SERIES (ABRAMOWITZ/STEGUN, EQUATION (7.1.5), P.297)
!C  N IS THE MINIMUM NUMBER OF TERMS NEEDED TO OBTAIN THE REQUIRED
!C  ACCURACY
!C
!        QRHO  = (1-0.85*Y)*DSQRT(QRHO)
!        N     = IDNINT(6 + 72*QRHO)
!        J     = 2*N+1
!        XSUM  = 1.0/J
!        YSUM  = 0.0D0
!        DO 10 I=N, 1, -1
!          J    = J - 2
!          XAUX = (XSUM*XQUAD - YSUM*YQUAD)/I
!          YSUM = (XSUM*YQUAD + YSUM*XQUAD)/I
!          XSUM = XAUX + 1.0/J
! 10     CONTINUE
!        U1   = -FACTOR*(XSUM*YABS + YSUM*XABS) + 1.0
!        V1   =  FACTOR*(XSUM*XABS - YSUM*YABS)
!        DAUX =  DEXP(-XQUAD)
!        U2   =  DAUX*DCOS(YQUAD)
!        V2   = -DAUX*DSIN(YQUAD)
!*
!        U    = U1*U2 - V1*V2
!        V    = U1*V2 + V1*U2
!*
!      ELSE
!C
!C  IF (QRHO.GT.1.O) THEN W(Z) IS EVALUATED USING THE LAPLACE
!C  CONTINUED FRACTION
!C  NU IS THE MINIMUM NUMBER OF TERMS NEEDED TO OBTAIN THE REQUIRED
!C  ACCURACY
!C
!C  IF ((QRHO.GT.0.085264D0).AND.(QRHO.LT.1.0)) THEN W(Z) IS EVALUATED
!C  BY A TRUNCATED TAYLOR EXPANSION, WHERE THE LAPLACE CONTINUED FRACTION
!C  IS USED TO CALCULATE THE DERIVATIVES OF W(Z)
!C  KAPN IS THE MINIMUM NUMBER OF TERMS IN THE TAYLOR EXPANSION NEEDED
!C  TO OBTAIN THE REQUIRED ACCURACY
!C  NU IS THE MINIMUM NUMBER OF TERMS OF THE CONTINUED FRACTION NEEDED
!C  TO CALCULATE THE DERIVATIVES WITH THE REQUIRED ACCURACY
!C
!*
!        IF (QRHO.GT.1.0) THEN
!          H    = 0.0D0
!          KAPN = 0
!          QRHO = DSQRT(QRHO)
!          NU   = IDINT(3 + (1442/(26*QRHO+77)))
!        ELSE
!          QRHO = (1-Y)*DSQRT(1-QRHO)
!          H    = 1.88*QRHO
!          H2   = 2*H
!          KAPN = IDNINT(7  + 34*QRHO)
!          NU   = IDNINT(16 + 26*QRHO)
!        ENDIF
!*
!        B = (H.GT.0.0)
!*
!        IF (B) QLAMBDA = H2**KAPN
!*
!        RX = 0.0
!        RY = 0.0
!        SX = 0.0
!        SY = 0.0
!*
!        DO 11 N=NU, 0, -1
!          NP1 = N + 1
!          TX  = YABS + H + NP1*RX
!          TY  = XABS - NP1*RY
!          C   = 0.5/(TX**2 + TY**2)
!          RX  = C*TX
!          RY  = C*TY
!          IF ((B).AND.(N.LE.KAPN)) THEN
!            TX = QLAMBDA + SX
!            SX = RX*TX - RY*SY
!            SY = RY*TX + RX*SY
!            QLAMBDA = QLAMBDA/H2
!          ENDIF
! 11     CONTINUE
!*
!        IF (H.EQ.0.0) THEN
!          U = FACTOR*RX
!          V = FACTOR*RY
!        ELSE
!          U = FACTOR*SX
!          V = FACTOR*SY
!        END IF
!*
!        IF (YABS.EQ.0.0) U = DEXP(-XABS**2)
!*
!      END IF
!*
!*
!C
!C  EVALUATION OF W(Z) IN THE OTHER QUADRANTS
!C
!*
!      IF (YI.LT.0.0) THEN
!*
!        IF (A) THEN
!          U2    = 2*U2
!          V2    = 2*V2
!        ELSE
!          XQUAD =  -XQUAD
!*
!C
!C         THE FOLLOWING IF-STATEMENT PROTECTS 2*EXP(-Z**2)
!C         AGAINST OVERFLOW
!C
!          IF ((YQUAD.GT.RMAXGONI).OR.
!     *        (XQUAD.GT.RMAXEXP)) GOTO 100
!*
!          W1 =  2*DEXP(XQUAD)
!          U2  =  W1*DCOS(YQUAD)
!          V2  = -W1*DSIN(YQUAD)
!        END IF
!*
!        U = U2 - U
!        V = V2 - V
!        IF (XI.GT.0.0) V = -V
!      ELSE
!        IF (XI.LT.0.0) V = -V
!      END IF
!*
!      RETURN
!*
!  100 FLAG = .TRUE.
!      RETURN
!*
!      END
!</Original Code>
!-------------------------------------------------------------------------------
  implicit none
  complex         :: plasma_disp,z
  real            :: qrho,xabs,yabs,x,y,xabsq,xquad,yquad,xi,yi
  real            :: xaux,xsum,ysum,u1,v1,u2,v2,daux,u,v
  real            :: qlambda,c,rx,ry,sx,sy,tx,ty,w1,h,h2
  integer         :: n,nu,kapn,i,j
  logical         :: qrholt0,hgt0
  real, parameter :: pi = 3.1415926535897932384626433832795028841971693993751058
  real :: factor 
!  No error checking is implemented, so these are not needed.
!  real, parameter :: rmaxreal = 0.5e+154,
!  real, parameter :: rmaxexp  = 708.503061461606
!  real, parameter :: rmaxgoni = 3.53711887601422e+15
  
  factor = 2./sqrt(pi)
  xi   = real(z)
  yi   = aimag(z)
  xabs = abs(xi)
  yabs = abs(yi)
  x = xabs/6.3
  y = yabs/4.4
  qrho = x**2 + y**2
  xabsq = xabs**2
  !Real part of z**2
  xquad = xabsq - yabs**2
  !Imaginary part of z**2
  yquad = 2*xabs*yabs
  
  qrholt0 = (qrho .lt. 0.085264)

! If qrho< 0.085264, then the faddeeva-function w(z) is evaluated using
! a power-series (Abramowitz & Stegun, eq. (7.1.5), p. 297).  n is the 
! minimum number of terms needed to obtain the required accuracy.
  if ( qrholt0 ) then
    qrho = (1 - 0.85*y)*sqrt(qrho)
    ! Rounded (up or down) to the nearest integer
    n = nint(6 + 72*qrho)
    j = 2*n+1
    xsum = 1.0/j
    ysum = 0.0
    do i = n, 1, -1
      j = j - 2
      xaux = (xsum*xquad - ysum*yquad)/i
      ysum = (xsum*yquad + ysum*xquad)/i
      xsum = xaux + 1.0/J
    end do
    u1   = -factor*(xsum*yabs + ysum*xabs) + 1.0
    v1   =  factor*(xsum*xabs - ysum*yabs)
    daux =  exp(-xquad)
    u2   =  daux*cos(yquad)
    v2   = -daux*sin(yquad)
    
    u    = u1*u2 - v1*v2
    v    = u1*v2 + v1*u2
  
! If qrho > 1.0, then the faddeeva-function w(z) is evaluated using the
! Laplace continued fraction.  nu is the minimum number of terms needed
! to obtain the required accuracy.
  else
    if (qrho .gt. 1.0 ) then
      h = 0.0
      kapn = 0
      qrho = sqrt(qrho)
      ! Rounded by truncating fractional part of magnitude (down for
      ! +ve quantities, up for -ve quantities).  Is this intentionally
      ! a call to int rather than nint?  Or is it a typo, and should be
      ! nint, like all the other calls?
      nu = int(3 + (1442/(26*qrho+77)))
  
! If 0.085264 < qrho < 1, then the faddeeva-function w(z) is evaluated
! by a truncated Taylor expansion, where the Laplace continued fraction
! is used to calculate the derivatives of w(z).  kapn is the minimum
! number of terms in the Taylor expansion needed to obtain the required
! accuracy.  nu is the minimum number of terms of the continued fraction
! needed to calculate the derivatives with the required accuracy.
    else
      qrho = (1 - y)*sqrt(1 - qrho)
      h    = 1.88*qrho
      h2   = 2*h
      ! Rounded (up or down) to the nearest integer.
      kapn = nint(7 + 34*qrho)
      nu   = nint(16 + 26*qrho)

    end if

    hgt0 = h .gt. 0.0
    if (hgt0) qlambda = h2**kapn

    rx = 0.0
    ry = 0.0
    sx = 0.0
    sy = 0.0

    do n = nu, 0, -1
      tx = yabs + h + (n+1)*rx
      ty = xabs - (n+1)*ry
      c  = 0.5/(tx**2 + ty**2)
      rx = c*tx
      ry = c*ty
      if( (hgt0) .and. (n.le.kapn) ) then
        tx = qlambda + sx
        sx = rx*tx - ry*sy
        sy = ry*tx + rx*sy
        qlambda = qlambda/h2
      end if
    end do

    if (h .eq. 0.0) then
      u = factor*rx
      v = factor*ry
    else
      u = factor*sx
      v = factor*sy
    end if
      
    if (yabs .eq. 0.0) u = exp(-xabs**2)

  ! Closes the else of the very first if statement,
  ! if (qrho .gt. 0.085264)... (that was a long while back)
  end if
  
  ! Result obtained from above is for z in quadrant I.
  ! Now, Adjust the result if the argument z is not in quadrant I.
  
  ! If in the lower half-plane:
  
  if (yi .lt. 0.0) then
    !If evaluating using Abramowitz and Stegun power-series:
    if (qrholt0) then
      u2    = 2*u2
      v2    = 2*v2
    !If evaluating using continued fraction and/or Taylor series:
    else
      xquad = -xquad
      w1    = 2*exp(xquad)
      u2    = w1*cos(yquad)
      v2    = -w1*sin(yquad)
    end if
    u = u2 - u
    v = v2 - v
    ! Stop here if z is in quadrant III.
    
    ! If z is in quadrant IV:
    if (xi .gt. 0.0) v = -v
  else
    ! If z is in quadrant II:
    if (xi .lt. 0.0) v = -v
  end if

  !plasma_disp(z) = i*sqrt(pi)*(u+iv)
  plasma_disp = sqrt(pi)*((0.0,1.0)*u - v)
  return
  end function plasma_disp
  
!-------------------------------------------------------------------------------
    
!-------------------------------------------------------------------------------
end module gk_eigen
