# include "define.inc"

module agk_layouts

  use layouts_type, only: g_layout_type, lz_layout_type, e_layout_type
  use layouts_type, only: le_layout_type

  implicit none
  private

  public :: pe_layout, layout, allow_accel
  public :: is_kx_local
  
  public :: init_dist_fn_layouts, init_agk_layouts
  public :: g_lo

  public :: init_fields_layouts
  public :: f_lo, f_layout_type

  public :: init_lorentz_layouts
  public :: lz_lo
  public :: gidx2lzidx

  public :: init_ediffuse_layouts
  public :: e_lo

  public :: init_le_layouts
  public :: le_lo

  public :: init_x_transform_layouts, init_y_transform_layouts
  public :: xxf_lo, xxf_layout_type, yxf_lo, yxf_layout_type
  public :: gidx2xxfidx, xxfidx2yxfidx, yxfidx2xxfidx, xxfidx2gidx
  public :: xxf_ky_is_zero

  public :: init_accel_transform_layouts
  public :: accel_lo, accel_layout_type, dealiasing
  public :: accelx_lo, accelx_layout_type

  public :: ig_idx, ik_idx, it_idx, il_idx, ie_idx, is_idx, isign_idx, if_idx
  public :: idx, proc_id, idx_local

!  logical :: accel_lxyes, lambda_local
  logical :: accel_lxyes, lambda_local = .false.  ! lambda_local must be initialized -- MAB
  character (len=5) :: layout
  logical :: allow_accel
!!! RNU>  2017/2/22: commented out flat_distribution as it's not used.
!!!       this prevents proc_id_g being elemental.
!!$  logical :: flat_distribution = .false.

  type ( g_layout_type) ::  g_lo
  type (lz_layout_type) :: lz_lo
  type ( e_layout_type) ::  e_lo
  type (le_layout_type) :: le_lo  ! new type

  type :: f_layout_type
     integer :: iproc
     integer :: nindex, naky, nakx
     integer :: llim_world, ulim_world, llim_proc, ulim_proc, ulim_alloc, blocksize
  end type f_layout_type

  type (f_layout_type) :: f_lo

  type :: xxf_layout_type
     integer :: iproc
     integer :: ntgrid, nsign, naky, nakx, nx, nadd, negrid, nlambda, nspec
     integer :: llim_world, ulim_world, llim_proc, ulim_proc, ulim_alloc, blocksize, gsize
     integer :: llim_group, ulim_group, igroup, ngroup, nprocset, iset, nset, groupblocksize
  end type xxf_layout_type

  type (xxf_layout_type) :: xxf_lo

  type :: yxf_layout_type
     integer :: iproc
     integer :: ntgrid, nsign, naky, ny, nakx, nx, negrid, nlambda, nspec
     integer :: llim_world, ulim_world, llim_proc, ulim_proc, ulim_alloc, blocksize, gsize
     integer :: llim_group, ulim_group, igroup, ngroup, nprocset, iset, nset, groupblocksize
  end type yxf_layout_type

  type (yxf_layout_type) :: yxf_lo

  type :: accel_layout_type
     integer :: iproc
     integer :: ntgrid, nsign, naky, ndky, ny, nakx
     integer :: nx, nxny, nxnky, negrid, nlambda, nspec, nia
     integer :: llim_world, ulim_world, llim_proc, ulim_proc, ulim_alloc, blocksize
  end type accel_layout_type

  type (accel_layout_type) :: accel_lo

  type :: accelx_layout_type
     integer :: iproc
     integer :: ntgrid, nsign, naky, ny, nakx, nx, nxny, negrid, nlambda, nspec
     integer :: llim_world, ulim_world, llim_proc, ulim_proc, ulim_alloc, blocksize
  end type accelx_layout_type

  type (accelx_layout_type) :: accelx_lo

  interface if_idx
     module procedure if_idx_f
  end interface

  interface ig_idx
     module procedure ig_idx_lz
     module procedure ig_idx_e
     module procedure ig_idx_le
     module procedure ig_idx_xxf
     module procedure ig_idx_yxf
  end interface

  interface ik_idx
     module procedure ik_idx_g
     module procedure ik_idx_f
     module procedure ik_idx_lz
     module procedure ik_idx_e
     module procedure ik_idx_le
     module procedure ik_idx_xxf
     module procedure ik_idx_accel
     module procedure ik_idx_accelx
  end interface

  interface it_idx
     module procedure it_idx_g
     module procedure it_idx_f
     module procedure it_idx_lz
     module procedure it_idx_e
     module procedure it_idx_le
     module procedure it_idx_yxf
     module procedure it_idx_accel
     module procedure it_idx_accelx
  end interface

  interface il_idx
     module procedure il_idx_g
     module procedure il_idx_e
     module procedure il_idx_xxf
     module procedure il_idx_yxf
     module procedure il_idx_accelx
  end interface

  interface ie_idx
     module procedure ie_idx_g
     module procedure ie_idx_lz
     module procedure ie_idx_xxf
     module procedure ie_idx_yxf
     module procedure ie_idx_accelx
  end interface

  interface is_idx
     module procedure is_idx_g
     module procedure is_idx_lz
     module procedure is_idx_e
     module procedure is_idx_le
     module procedure is_idx_xxf
     module procedure is_idx_yxf
     module procedure is_idx_accelx
  end interface

  interface isign_idx
     module procedure isign_idx_xxf
     module procedure isign_idx_yxf
  end interface

  interface proc_id
     module procedure proc_id_g
     module procedure proc_id_f
     module procedure proc_id_lz
     module procedure proc_id_e
     module procedure proc_id_le
     module procedure proc_id_xxf
     module procedure proc_id_yxf
  end interface

  interface idx
     module procedure idx_g
     module procedure idx_f
     module procedure idx_lz
     module procedure idx_e
     module procedure idx_le
     module procedure idx_xxf
     module procedure idx_yxf
  end interface

  interface idx_local
     module procedure idx_local_g,      ig_local_g
     module procedure idx_local_f,      ig_local_f
     module procedure idx_local_lz,     ig_local_lz
     module procedure idx_local_e,      ig_local_e
     module procedure idx_local_le,     ig_local_le
     module procedure idx_local_xxf,    ig_local_xxf
     module procedure idx_local_yxf,    ig_local_yxf
  end interface

contains

  subroutine init_agk_layouts
    
    use mp, only: proc0
    implicit none
    logical, save :: initialized = .false.

    if (initialized) return
    initialized = .true.
    
    if (proc0) call read_parameters
    call broadcast_results

  end subroutine init_agk_layouts

  subroutine read_parameters
    use file_utils, only: input_unit, error_unit, input_unit_exist, error_unit
    use mp, only: mp_abort
    implicit none
    integer :: in_file
    logical :: exist
    integer :: ireaderr=0
    logical :: abort_readerr=.true.

    namelist /layouts_knobs/ layout, allow_accel

    layout = 'lxyes'
    allow_accel = .false.
    in_file=input_unit_exist("layouts_knobs", exist)
    if (exist) read (unit=in_file, nml=layouts_knobs, iostat=ireaderr)

    if (abort_readerr) then
       if (ireaderr > 0) then
          call mp_abort ('Read error at layouts_knobs')
       else if (ireaderr < 0) then
          call mp_abort ('End of file/record occurred at layouts_knobs')
       endif
    endif

  end subroutine read_parameters
    
  subroutine broadcast_results
    use mp, only: broadcast
    implicit none

    call broadcast (layout)
    call broadcast (allow_accel)

  end subroutine broadcast_results

  subroutine check_accel (nakx, naky, nlambda, negrid, nspec, nblock)

    use mp, only: nproc
    use agk_mem, only: alloc8, dealloc8
    implicit none
    integer, intent (in) :: negrid, nspec, nlambda, naky, nakx
    integer, dimension(:,:), allocatable :: facs
    integer :: nsfacs, nefacs, nyfacs, nxfacs, nlfacs, nblock
    integer :: i

    if (.not. layout == 'lxyes') then
       accel_lxyes = .false.
       return
    end if

    accel_lxyes = .true.

    allocate (facs(max(nspec,negrid,naky,nakx,nlambda)/2+1,5)); call alloc8(i2=facs,v="facs")
    call factors (nspec,   nsfacs, facs(:,1))
    call factors (negrid,  nefacs, facs(:,2))
    call factors (naky,    nyfacs, facs(:,3))
    call factors (nakx,    nxfacs, facs(:,4))
    call factors (nlambda, nlfacs, facs(:,5))
    
!    do i=1,nsfacs-1
!       if (nproc == facs(i,1)) then
!          nblock = facs(i,1)
!          goto 100
!       end if
!    end do
!    
!    do i=1,nefacs-1
!       if (nproc == facs(i,2)*nspec) then
!          nblock = facs(i,2)*nspec
!          goto 100
!       end if
!    end do
    
    nblock = negrid*nspec
    do i=1,nyfacs-1
       if (nproc == facs(i,3)*negrid*nspec) goto 100
    end do
    
    do i=1,nxfacs-1
       if (nproc == facs(i,4)*naky*negrid*nspec) goto 100
    end do
    
    do i=1,nlfacs-1
       if (nproc == facs(i,5)*nakx*naky*negrid*nspec) goto 100
    end do
    
    accel_lxyes = .false.

!100 deallocate (facs)
100 call dealloc8(i2=facs,v="facs")

  end subroutine check_accel

  subroutine check_llocal (nakx, naky, nlambda, negrid, nspec)

    use mp, only: nproc
    use agk_mem, only: alloc8, dealloc8
    implicit none
    integer, intent (in) :: negrid, nspec, nlambda, naky, nakx
    integer, dimension(:,:), allocatable :: facs
    integer :: nsfacs, nefacs, nyfacs, nxfacs, nlfacs
    integer :: i

    lambda_local = .true.

    allocate (facs(max(nspec,negrid,naky,nakx,nlambda)/2+1,5)); call alloc8(i2=facs,v="facs2")

    select case (layout)

    case ('lxyes')

       call factors (nspec,   nsfacs, facs(:,1))
       call factors (negrid,  nefacs, facs(:,2))
       call factors (naky,    nyfacs, facs(:,3))
       call factors (nakx, nxfacs, facs(:,4))
       call factors (nlambda, nlfacs, facs(:,5))

       do i=1,nsfacs-1
          if (nproc == facs(i,1)) goto 100
       end do
       
       do i=1,nefacs-1
          if (nproc == facs(i,2)*nspec) goto 100
       end do

       do i=1,nyfacs-1
          if (nproc == facs(i,3)*negrid*nspec) goto 100
       end do
          
       do i=1,nxfacs-1
          if (nproc == facs(i,4)*naky*negrid*nspec) goto 100
       end do
          
       do i=1,nlfacs-1
          if (nproc == facs(i,5)*nakx*naky*negrid*nspec) goto 100
       end do
          
    case ('lyxes')

       call factors (nspec,   nsfacs, facs(:,1))
       call factors (negrid,  nefacs, facs(:,2))
       call factors (nakx, nxfacs, facs(:,3))
       call factors (naky,    nyfacs, facs(:,4))
       call factors (nlambda, nlfacs, facs(:,5))

       do i=1,nsfacs-1
          if (nproc == facs(i,1)) goto 100
       end do
       
       do i=1,nefacs-1
          if (nproc == facs(i,2)*nspec) goto 100
       end do

       do i=1,nxfacs-1
          if (nproc == facs(i,3)*negrid*nspec) goto 100
       end do
          
       do i=1,nyfacs-1
          if (nproc == facs(i,4)*nakx*negrid*nspec) goto 100
       end do
          
       do i=1,nlfacs-1
          if (nproc == facs(i,5)*naky*nakx*negrid*nspec) goto 100
       end do
          
    case ('lexys')

       call factors (nspec,   nsfacs, facs(:,1))
       call factors (naky,    nyfacs, facs(:,2))
       call factors (nakx, nxfacs, facs(:,3))
       call factors (negrid,  nefacs, facs(:,4))
       call factors (nlambda, nlfacs, facs(:,5))

       do i=1,nsfacs-1
          if (nproc == facs(i,1)) goto 100
       end do
       
       do i=1,nyfacs-1
          if (nproc == facs(i,2)*nspec) goto 100
       end do

       do i=1,nxfacs-1
          if (nproc == facs(i,3)*naky*nspec) goto 100
       end do
          
       do i=1,nefacs-1
          if (nproc == facs(i,4)*nakx*naky*nspec) goto 100
       end do
          
       do i=1,nlfacs-1
          if (nproc == facs(i,5)*negrid*nakx*naky*nspec) goto 100
       end do

    end select

    lambda_local = .false.    
                       
!100 deallocate (facs)
100 call dealloc8(i2=facs,v="facs")


  end subroutine check_llocal

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Distribution function layouts
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine init_dist_fn_layouts (ntgrid, naky, nakx, nlambda, negrid, nspec)
    use mp, only: iproc, nproc, proc0, mp_abort
    use file_utils, only: error_unit
    implicit none
    integer, intent (in) :: ntgrid, naky, nakx, nlambda, negrid, nspec
    logical, save :: initialized = .false.
# ifdef USE_C_INDEX
    integer :: ierr
# endif
# ifdef USE_C_INDEX
    interface
       function init_indices_glo_c (layout)
         integer :: init_indices_glo_c
         character(*) :: layout
       end function init_indices_glo_c
    end interface
# endif

    if (initialized) return
    initialized = .true.
    
    g_lo%iproc = iproc
    g_lo%naky = naky
    g_lo%nakx = nakx
    g_lo%nlambda = nlambda
    g_lo%negrid = negrid
    g_lo%nspec = nspec
    g_lo%llim_world = 0
    g_lo%ulim_world = naky*nakx*negrid*nlambda*nspec - 1

!!$    if (flat_distribution) then
!!$       ! <doc>
!!$       !  Data distribution to avoid zero work load of the last proc,
!!$       !  which happens if Np > l + m + 1 where Nw = Np * m + l with 
!!$       !  Np, Nw being the # of procs, and the # of grid points.
!!$       !  In this case, ulim_proc is always larger than llim_proc,
!!$       !  and ulim_alloc can be removed.
!!$       ! </doc>
!!$       m=(g_lo%ulim_world+1)/nproc
!!$       l=mod(g_lo%ulim_world+1,nproc)
!!$       if (iproc < l) then
!!$          g_lo%blocksize = m+1
!!$          g_lo%llim_proc = g_lo%blocksize*iproc
!!$          g_lo%ulim_proc = min(g_lo%ulim_world, g_lo%llim_proc + g_lo%blocksize -1)
!!$          g_lo%ulim_alloc = max(g_lo%llim_proc, g_lo%ulim_proc)
!!$       else
!!$          g_lo%blocksize = m
!!$          g_lo%llim_proc = g_lo%blocksize*iproc+l
!!$          g_lo%ulim_proc = min(g_lo%ulim_world, g_lo%llim_proc + g_lo%blocksize -1)
!!$          g_lo%ulim_alloc = max(g_lo%llim_proc, g_lo%ulim_proc)
!!$       end if
!!$    else
    g_lo%blocksize = g_lo%ulim_world/nproc + 1
    g_lo%llim_proc = g_lo%blocksize*iproc
    g_lo%ulim_proc = min(g_lo%ulim_world, g_lo%llim_proc + g_lo%blocksize - 1)
    g_lo%ulim_alloc = max(g_lo%llim_proc, g_lo%ulim_proc)
!!$    end if

    ! <doc> INFO (RN):
    !  In some special case, the last processor has no work load assigned.
    !  ulim_proc > llim_proc = ulim_world for that processor.
    !  This is not fatal at all. Just wasting one processor time.
    !  But, can be a potential bug source.
    ! </doc>

    if (g_lo%ulim_world < 0) then
       if (proc0) then 
          write (error_unit(),*) 'ERROR: negative ulim_world in g_lo'
          write (error_unit(),*) 'ERROR: possibly too many grid points'
       end if
       call mp_abort ('STOP in init_dist_fn_layouts')
    end if

# ifdef USE_C_INDEX
    ierr = init_indices_glo_c (layout)
    if (ierr /= 0) &
         & write (error_unit(),*) 'ERROR: layout not found: ', trim(layout)
# endif

  end subroutine init_dist_fn_layouts

  subroutine is_kx_local(negrid, nspec, nlambda, naky, nakx, kx_local)  

    use mp, only: nproc
    use agk_mem, only: alloc8, dealloc8
    implicit none
    integer, intent (in) :: negrid, nspec, nlambda, naky, nakx
    logical, intent (out) :: kx_local
    integer, dimension(:,:), allocatable :: facs
    integer :: nsfacs, nefacs, nyfacs, nlfacs
    integer :: i

    kx_local = .true.

    allocate (facs(max(nspec,negrid,naky,nlambda)/2+1,3)); call alloc8(i2=facs,v="facs")

    select case (layout)

    case ('yxels')

       call factors (nspec,   nsfacs, facs(:,1))
       call factors (nlambda, nlfacs, facs(:,2))
       call factors (negrid,  nefacs, facs(:,3))

       do i=1,nsfacs-1
          if (nproc == facs(i,1)) goto 100
       end do
       
       do i=1,nlfacs-1
          if (nproc == facs(i,2)*nspec) goto 100
       end do

       do i=1,nefacs-1
          if (nproc == facs(i,3)*nlambda*nspec) goto 100
       end do
          
    case ('yxles')

       call factors (nspec,   nsfacs, facs(:,1))
       call factors (negrid,  nefacs, facs(:,2))
       call factors (nlambda, nlfacs, facs(:,3))

       do i=1,nsfacs-1
          if (nproc == facs(i,1)) goto 100
       end do
       
       do i=1,nefacs-1
          if (nproc == facs(i,2)*nspec) goto 100
       end do

       do i=1,nlfacs-1
          if (nproc == facs(i,3)*negrid*nspec) goto 100
       end do
          
    case ('lxyes')

       call factors (nspec,   nsfacs, facs(:,1))
       call factors (negrid,  nefacs, facs(:,2))
       call factors (naky,    nyfacs, facs(:,3))

       do i=1,nsfacs-1
          if (nproc == facs(i,1)) goto 100
       end do
       
       do i=1,nefacs-1
          if (nproc == facs(i,2)*nspec) goto 100
       end do

       do i=1,nyfacs-1
          if (nproc == facs(i,3)*negrid*nspec) goto 100
       end do
          
    case ('lyxes')

       call factors (nspec,   nsfacs, facs(:,1))
       call factors (negrid,  nefacs, facs(:,2))

       do i=1,nsfacs-1
          if (nproc == facs(i,1)) goto 100
       end do
       
       do i=1,nefacs-1
          if (nproc == facs(i,2)*nspec) goto 100
       end do

    case ('lexys')

       call factors (nspec,   nsfacs, facs(:,1))
       call factors (naky,    nyfacs, facs(:,2))

       do i=1,nsfacs-1
          if (nproc == facs(i,1)) goto 100
       end do
       
       do i=1,nyfacs-1
          if (nproc == facs(i,2)*nspec) goto 100
       end do

    end select

    kx_local = .false.    
                       
!100 deallocate (facs)
100 call dealloc8(i2=facs,v="facs")

  end subroutine is_kx_local

# ifdef USE_C_INDEX
  function is_idx_g (lo, i)
# else
  elemental function is_idx_g (lo, i)
# endif
    implicit none
    integer :: is_idx_g
    type (g_layout_type), intent (in) :: lo
    integer, intent (in) :: i
# ifdef USE_C_INDEX
    interface
       function is_idx_g_c (lo,num)
         use layouts_type, only: g_layout_type
         integer :: is_idx_g_c
         type (g_layout_type) :: lo
         integer :: num
       end function is_idx_g_c
    end interface
    is_idx_g = is_idx_g_c (lo,i)
# else
    ! TT: the order of the division doesn't matter, so no need for branching
    is_idx_g = 1 + mod((i - lo%llim_world)/lo%naky/lo%nakx/lo%negrid/lo%nlambda, lo%nspec)
# endif

  end function is_idx_g

# ifdef USE_C_INDEX
  function il_idx_g (lo, i)
# else
  elemental function il_idx_g (lo, i)
# endif
    implicit none
    integer :: il_idx_g
    type (g_layout_type), intent (in) :: lo
    integer, intent (in) :: i
# ifdef USE_C_INDEX
    interface
       function il_idx_g_c (lo,num)
         use layouts_type, only: g_layout_type
         integer :: il_idx_g_c
         type (g_layout_type) :: lo
         integer :: num
       end function il_idx_g_c
    end interface
    il_idx_g = il_idx_g_c (lo,i)
# else
    select case (layout)
    case ('yxels')
       il_idx_g = 1 + mod((i - lo%llim_world)/lo%naky/lo%nakx/lo%negrid, lo%nlambda)
    case ('yxles')
       il_idx_g = 1 + mod((i - lo%llim_world)/lo%naky/lo%nakx, lo%nlambda)
    case ('lexys')
       il_idx_g = 1 + mod(i - lo%llim_world, lo%nlambda)
    case ('lxyes')
       il_idx_g = 1 + mod(i - lo%llim_world, lo%nlambda)
    case ('lyxes')
       il_idx_g = 1 + mod(i - lo%llim_world, lo%nlambda)
    end select
# endif

  end function il_idx_g

# ifdef USE_C_INDEX
  function ie_idx_g (lo, i)
# else
  elemental function ie_idx_g (lo, i)
# endif
    implicit none
    integer :: ie_idx_g
    type (g_layout_type), intent (in) :: lo
    integer, intent (in) :: i
# ifdef USE_C_INDEX
    interface
       function ie_idx_g_c (lo,num)
         use layouts_type, only: g_layout_type
         integer :: ie_idx_g_c
         type (g_layout_type) :: lo
         integer :: num
       end function ie_idx_g_c
    end interface
    ie_idx_g = ie_idx_g_c (lo,i)
# else
    select case (layout)
    case ('yxels')
       ie_idx_g = 1 + mod((i - lo%llim_world)/lo%naky/lo%nakx, lo%negrid)
    case ('yxles')
       ie_idx_g = 1 + mod((i - lo%llim_world)/lo%naky/lo%nakx/lo%nlambda, lo%negrid)
    case ('lexys')
       ie_idx_g = 1 + mod((i - lo%llim_world)/lo%nlambda, lo%negrid)
    case ('lxyes')
       ie_idx_g = 1 + mod((i - lo%llim_world)/lo%nlambda/lo%nakx/lo%naky, lo%negrid)
    case ('lyxes')
       ie_idx_g = 1 + mod((i - lo%llim_world)/lo%nlambda/lo%naky/lo%nakx, lo%negrid)
    end select
# endif

  end function ie_idx_g

# ifdef USE_C_INDEX
  function it_idx_g (lo, i)
# else
  elemental function it_idx_g (lo, i)
# endif
    implicit none
    integer :: it_idx_g
    type (g_layout_type), intent (in) :: lo
    integer, intent (in) :: i
# ifdef USE_C_INDEX
    interface
       function it_idx_g_c (lo,num)
         use layouts_type, only: g_layout_type
         integer :: it_idx_g_c
         type (g_layout_type) :: lo
         integer :: num
       end function it_idx_g_c
    end interface
    it_idx_g = it_idx_g_c (lo,i)
# else
    select case (layout)
    case ('yxels')
       it_idx_g = 1 + mod((i - lo%llim_world)/lo%naky, lo%nakx)
    case ('yxles')
       it_idx_g = 1 + mod((i - lo%llim_world)/lo%naky, lo%nakx)
    case ('lexys')
       it_idx_g = 1 + mod((i - lo%llim_world)/lo%nlambda/lo%negrid, lo%nakx)
    case ('lxyes')
       it_idx_g = 1 + mod((i - lo%llim_world)/lo%nlambda, lo%nakx)
    case ('lyxes')
       it_idx_g = 1 + mod((i - lo%llim_world)/lo%nlambda/lo%naky, lo%nakx)
    end select
# endif

  end function it_idx_g

# ifdef USE_C_INDEX
  function ik_idx_g (lo, i)
# else
  elemental function ik_idx_g (lo, i)
# endif
    implicit none
    integer :: ik_idx_g
    type (g_layout_type), intent (in) :: lo
    integer, intent (in) :: i
# ifdef USE_C_INDEX
    interface
       function ik_idx_g_c (lo,num)
         use layouts_type, only: g_layout_type
         integer :: ik_idx_g_c
         type (g_layout_type) :: lo
         integer :: num
       end function ik_idx_g_c
    end interface
    ik_idx_g = ik_idx_g_c (lo,i)
# else
    select case (layout)
    case ('yxels')
       ik_idx_g = 1 + mod(i - lo%llim_world, lo%naky)
    case ('yxles')
       ik_idx_g = 1 + mod(i - lo%llim_world, lo%naky)
    case ('lexys')
       ik_idx_g = 1 + mod((i - lo%llim_world)/lo%nlambda/lo%negrid/lo%nakx, lo%naky)
    case ('lxyes')
       ik_idx_g = 1 + mod((i - lo%llim_world)/lo%nlambda/lo%nakx, lo%naky)
    case ('lyxes')
       ik_idx_g = 1 + mod((i - lo%llim_world)/lo%nlambda, lo%naky)
    end select
# endif

  end function ik_idx_g

  elemental function proc_id_g (lo, i)
    implicit none
    integer :: proc_id_g
    type (g_layout_type), intent (in) :: lo
    integer, intent (in) :: i
!!$    if (flat_distribution) then
!!$       m=(lo%ulim_world+1)/nproc
!!$       l=mod(lo%ulim_world+1,nproc)
!!$       if (i < (m+1)*l) then
!!$          proc_id_g = i/(m+1)
!!$       else
!!$          proc_id_g = (i-(m+1)*l)/m+l
!!$       endif
!!$    else
    proc_id_g = i/lo%blocksize
!!$    end if
  end function proc_id_g

# ifdef USE_C_INDEX
  function idx_g (lo, ik, it, il, ie, is)
# else
  elemental function idx_g (lo, ik, it, il, ie, is)
# endif
    implicit none
    integer :: idx_g
    type (g_layout_type), intent (in) :: lo
    integer, intent (in) :: ik, it, il, ie, is
# ifdef USE_C_INDEX
    interface
       function idx_g_c (lo,ik,it,il,ie,is)
         use layouts_type, only: g_layout_type
         integer :: idx_g_c
         type (g_layout_type) :: lo
         integer :: ik,it,il,ie,is
       end function idx_g_c
    end interface
    idx_g = idx_g_c (lo, ik, it, il, ie, is)
# else
    select case (layout)
    case ('yxels')
       idx_g = ik-1 + lo%naky*(it-1 + lo%nakx*(ie-1 + lo%negrid*(il-1 + lo%nlambda*(is-1))))
    case ('yxles')
       idx_g = ik-1 + lo%naky*(it-1 + lo%nakx*(il-1 + lo%nlambda*(ie-1 + lo%negrid*(is-1))))
    case ('lexys')
       idx_g = il-1 + lo%nlambda*(ie-1 + lo%negrid*(it-1 + lo%nakx*(ik-1 + lo%naky*(is-1))))
    case ('lxyes')
       idx_g = il-1 + lo%nlambda*(it-1 + lo%nakx*(ik-1 + lo%naky*(ie-1 + lo%negrid*(is-1))))
    case ('lyxes')
       idx_g = il-1 + lo%nlambda*(ik-1 + lo%naky*(it-1 + lo%nakx*(ie-1 + lo%negrid*(is-1))))
    end select
# endif

  end function idx_g

# ifdef USE_C_INDEX
  function idx_local_g (lo, ik, it, il, ie, is)
# else
  elemental function idx_local_g (lo, ik, it, il, ie, is)
# endif
    implicit none
    logical :: idx_local_g
    type (g_layout_type), intent (in) :: lo
    integer, intent (in) :: ik, it, il, ie, is

    idx_local_g = idx_local(lo, idx(lo, ik, it, il, ie, is))
  end function idx_local_g

  elemental function ig_local_g (lo, ig)
    implicit none
    logical :: ig_local_g
    type (g_layout_type), intent (in) :: lo
    integer, intent (in) :: ig

    ig_local_g = lo%iproc == proc_id(lo, ig)
  end function ig_local_g

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Fields layouts
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine init_fields_layouts (nindex, naky, nakx)
    use mp, only: iproc, nproc
    implicit none
    integer, intent (in) :: nindex, naky, nakx
    logical, save :: initialized = .false.

    if (initialized) return
    initialized = .true.

    f_lo % iproc = iproc
    f_lo % nindex = nindex
    f_lo % naky = naky
    f_lo % nakx = nakx
    f_lo % llim_world = 0
    f_lo % ulim_world = nindex*naky*nakx - 1
    f_lo % blocksize = f_lo%ulim_world/nproc + 1
    f_lo % llim_proc = f_lo%blocksize*iproc
    f_lo % ulim_proc = min(f_lo%ulim_world, f_lo%llim_proc + f_lo%blocksize - 1)
    f_lo % ulim_alloc = max(f_lo%llim_proc, f_lo%ulim_proc)

  end subroutine init_fields_layouts

  elemental function if_idx_f (lo, i)
    implicit none
    integer :: if_idx_f
    type (f_layout_type), intent (in) :: lo
    integer, intent (in) :: i
    if_idx_f = 1 + mod(i - lo%llim_world, lo%nindex)
  end function if_idx_f

  elemental function it_idx_f (lo, i)
    implicit none
    integer :: it_idx_f
    type (f_layout_type), intent (in) :: lo
    integer, intent (in) :: i
    it_idx_f = 1 + mod((i - lo%llim_world)/lo%nindex, lo%nakx)
  end function it_idx_f

  elemental function ik_idx_f (lo, i)
    implicit none
    integer :: ik_idx_f
    type (f_layout_type), intent (in) :: lo
    integer, intent (in) :: i
    ik_idx_f = 1 + mod((i - lo%llim_world)/lo%nindex/lo%nakx, lo%naky)
  end function ik_idx_f

  elemental function proc_id_f (lo, i)
    implicit none
    integer :: proc_id_f
    type (f_layout_type), intent (in) :: lo
    integer, intent (in) :: i
    proc_id_f = i/lo%blocksize
  end function proc_id_f

  elemental function idx_f (lo, if, ik, it)
    implicit none
    integer :: idx_f
    type (f_layout_type), intent (in) :: lo
    integer, intent (in) :: if, ik, it
    idx_f = if-1 + lo%nindex*(it-1 + lo%nakx*(ik-1)) 
  end function idx_f  

  elemental function idx_local_f (lo, if, ik, it)
    implicit none
    logical :: idx_local_f
    type (f_layout_type), intent (in) :: lo
    integer, intent (in) :: if, ik, it
    idx_local_f = idx_local(lo, idx(lo, if, ik, it))
  end function idx_local_f

  elemental function ig_local_f (lo, ig)
    implicit none
    logical :: ig_local_f
    type (f_layout_type), intent (in) :: lo
    integer, intent (in) :: ig
    ig_local_f = lo%iproc == proc_id(lo, ig)
  end function ig_local_f

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Lorentz-Ediffuse layouts
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine init_le_layouts (ntgrid, naky, nakx, nspec)
    use mp, only: iproc, nproc, proc0, mp_abort
    use file_utils, only: error_unit
    implicit none
    integer, intent (in) :: ntgrid, naky, nakx, nspec
    logical, save :: initialized = .false.
# ifdef USE_C_INDEX
    integer :: ierr
# endif
# ifdef USE_C_INDEX
    interface
       function init_indices_lelo_c (layout)
         integer :: init_indices_lelo_c
         character(*) :: layout
       end function init_indices_lelo_c
    end interface
# endif

    if (initialized) return
    initialized = .true.
    
    le_lo%iproc = iproc
    le_lo%ntgrid = ntgrid
    le_lo%nakx = nakx
    le_lo%naky = naky
    le_lo%nspec = nspec
    le_lo%llim_world = 0
    le_lo%ulim_world = (2*ntgrid+1) * naky * nakx * nspec - 1
    le_lo%blocksize = le_lo%ulim_world / nproc + 1
    le_lo%llim_proc = le_lo%blocksize * iproc
    le_lo%ulim_proc &
         = min(le_lo%ulim_world, le_lo%llim_proc + le_lo%blocksize - 1)
    le_lo%ulim_alloc = max(le_lo%llim_proc, le_lo%ulim_proc)

    if (le_lo%ulim_world < 0) then
       if (proc0) then
          write (error_unit(),*) 'ERROR: negative ulim_world in le_lo'
          write (error_unit(),*) 'ERROR: possibly too many grid points'
       end if
       call mp_abort ('STOP in init_le_layouts')
    end if

# ifdef USE_C_INDEX
    ierr = init_indices_lelo_c (layout)
    if (ierr /= 0) &
         & write (error_unit(),*) 'ERROR: layout not found: ', trim(layout)
# endif
  end subroutine init_le_layouts

  elemental function is_idx_le (lo, i)
    implicit none
    integer :: is_idx_le
    type (le_layout_type), intent (in) :: lo
    integer, intent (in) :: i
    ! TT: the order of the division doesn't matter, so no need for branching
    is_idx_le = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%naky/lo%nakx, lo%nspec)
  end function is_idx_le

# ifdef USE_C_INDEX
  function it_idx_le (lo, i)
# else
  elemental function it_idx_le (lo, i)
# endif
    implicit none
    integer :: it_idx_le
    type (le_layout_type), intent (in) :: lo
    integer, intent (in) :: i
# ifdef USE_C_INDEX
    interface
       function it_idx_le_c (lo,num)
         use layouts_type, only: le_layout_type
         integer :: it_idx_le_c
         type (le_layout_type) :: lo
         integer :: num
       end function it_idx_le_c
    end interface
    it_idx_le = it_idx_le_c (lo,i)
# else
    select case (layout)
    case ('yxels')
       it_idx_le = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%naky, lo%nakx)
    case ('yxles')
       it_idx_le = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%naky, lo%nakx)
    case ('lexys')
       it_idx_le = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1), lo%nakx)
    case ('lxyes')
       it_idx_le = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1), lo%nakx)
    case ('lyxes')
       it_idx_le = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%naky, lo%nakx)
    end select
# endif
  end function it_idx_le

# ifdef USE_C_INDEX
  function ik_idx_le (lo, i)
# else
  elemental function ik_idx_le (lo, i)
# endif
    implicit none
    integer :: ik_idx_le
    type (le_layout_type), intent (in) :: lo
    integer, intent (in) :: i
# ifdef USE_C_INDEX
    interface
       function ik_idx_le_c (lo,num)
         use layouts_type, only: le_layout_type
         integer :: ik_idx_le_c
         type (le_layout_type) :: lo
         integer :: num
       end function ik_idx_le_c
    end interface
    ik_idx_le = ik_idx_le_c (lo,i)
# else
    select case (layout)
    case ('yxels')
       ik_idx_le = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1), lo%naky)
    case ('yxles')
       ik_idx_le = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1), lo%naky)
    case ('lexys')
       ik_idx_le = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%nakx, lo%naky)
    case ('lxyes')
       ik_idx_le = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%nakx, lo%naky)
    case ('lyxes')
       ik_idx_le = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1), lo%naky)
    end select
# endif
  end function ik_idx_le

  elemental function ig_idx_le (lo, i)
    implicit none
    integer :: ig_idx_le
    type (le_layout_type), intent (in) :: lo
    integer, intent (in) :: i
    ig_idx_le = -lo%ntgrid + mod(i - lo%llim_world, 2*lo%ntgrid + 1)
  end function ig_idx_le

# ifdef USE_C_INDEX
  function idx_le (lo, ig, ik, it, is)
# else
  elemental function idx_le (lo, ig, ik, it, is)
# endif
    implicit none
    integer :: idx_le
    type (le_layout_type), intent (in) :: lo
    integer, intent (in) :: ig, ik, it, is
# ifdef USE_C_INDEX
    interface
       function idx_le_c (lo, ig, ik, it, is)
         use layouts_type, only: le_layout_type
         integer :: idx_le_c
         type (le_layout_type) :: lo
         integer :: ig, ik, it, is
       end function idx_le_c
    end interface
    idx_le = idx_le_c (lo, ig, ik, it, is)
# else
    select case (layout)
    case ('yxels')
       idx_le = ig+lo%ntgrid + (2*lo%ntgrid+1)*(ik-1 &
            + lo%naky*(it-1 + lo%nakx*(is-1)))
    case ('yxles')
       idx_le = ig+lo%ntgrid + (2*lo%ntgrid+1)*(ik-1 &
            + lo%naky*(it-1 + lo%nakx*(is-1)))
    case ('lexys')
       idx_le = ig+lo%ntgrid + (2*lo%ntgrid+1)*(it-1 &
            + lo%nakx*(ik-1 + lo%naky*(is-1)))
    case ('lxyes')
       idx_le = ig+lo%ntgrid + (2*lo%ntgrid+1)*(it-1 &
            + lo%nakx*(ik-1 + lo%naky*(is-1)))
    case ('lyxes')
       idx_le = ig+lo%ntgrid + (2*lo%ntgrid+1)*(ik-1 &
            + lo%naky*(it-1 + lo%nakx*(is-1)))
    end select
# endif
  end function idx_le

  elemental function proc_id_le (lo, i)
    implicit none
    integer :: proc_id_le
    type (le_layout_type), intent (in) :: lo
    integer, intent (in) :: i

    proc_id_le = i / lo%blocksize

  end function proc_id_le

# ifdef USE_C_INDEX
  function idx_local_le (lo, ig, ik, it, is)
# else
  elemental function idx_local_le (lo, ig, ik, it, is)
# endif
    implicit none
    logical :: idx_local_le
    type (le_layout_type), intent (in) :: lo
    integer, intent (in) :: ig, ik, it, is

    idx_local_le = idx_local(lo, idx(lo, ig, ik, it, is))
  end function idx_local_le

  elemental function ig_local_le (lo, ig)
    implicit none
    logical :: ig_local_le
    type (le_layout_type), intent (in) :: lo
    integer, intent (in) :: ig

    ig_local_le = lo%iproc == proc_id(lo, ig)
  end function ig_local_le

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Energy scattering layouts
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine init_ediffuse_layouts (ntgrid, naky, nakx, nlambda, nspec)
    use mp, only: iproc, nproc, proc0, mp_abort
    use file_utils, only: error_unit
    implicit none
    integer, intent (in) :: ntgrid, naky, nakx, nlambda, nspec
    logical, save :: initialized = .false.
# ifdef USE_C_INDEX
    integer :: ierr
# endif
# ifdef USE_C_INDEX
    interface
       function init_indices_elo_c (layout)
         integer :: init_indices_elo_c
         character(*) :: layout
       end function init_indices_elo_c
    end interface
# endif

    if (initialized) return
    initialized = .true.

    e_lo%iproc = iproc
    e_lo%ntgrid = ntgrid
    e_lo%nsign = 2
    e_lo%nakx = nakx
    e_lo%naky = naky
    e_lo%nspec = nspec
    e_lo%nlambda = nlambda
    e_lo%llim_world = 0
    e_lo%ulim_world = (2*ntgrid+1)*naky*nakx*nlambda*nspec*2 - 1
    e_lo%blocksize = e_lo%ulim_world/nproc + 1
    e_lo%llim_proc = e_lo%blocksize*iproc
    e_lo%ulim_proc &
         = min(e_lo%ulim_world, e_lo%llim_proc + e_lo%blocksize - 1)
    e_lo%ulim_alloc = max(e_lo%llim_proc, e_lo%ulim_proc)

    if (e_lo%ulim_world < 0) then
       if (proc0) then
          write (error_unit(),*) 'ERROR: negative ulim_world in e_lo'
          write (error_unit(),*) 'ERROR: possively too many grid points'
       end if
       call mp_abort ('STOP in init_ediffuse_layouts')
    end if

# ifdef USE_C_INDEX
    ierr = init_indices_elo_c (layout)
    if (ierr /= 0) &
         & write (error_unit(),*) 'ERROR: layout not found: ', trim(layout)
# endif

  end subroutine init_ediffuse_layouts

# ifdef USE_C_INDEX
  function is_idx_e (lo, i)
# else
  elemental function is_idx_e (lo, i)
# endif
    implicit none
    integer :: is_idx_e
    type (e_layout_type), intent (in) :: lo
    integer, intent (in) :: i
# ifdef USE_C_INDEX
    interface
       function is_idx_e_c (lo,num)
         use layouts_type, only: e_layout_type
         integer :: is_idx_e_c
         type (e_layout_type) :: lo
         integer :: num
       end function is_idx_e_c
    end interface
    is_idx_e = is_idx_e_c (lo,i)
# else
    ! TT: the order of the division doesn't matter, so no need for branching
    is_idx_e = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%nsign/lo%naky/lo%nakx/lo%nlambda, lo%nspec)
# endif

  end function is_idx_e

# ifdef USE_C_INDEX
  function il_idx_e (lo, i)
# else
  elemental function il_idx_e (lo, i)
# endif
    implicit none
    integer :: il_idx_e
    type (e_layout_type), intent (in) :: lo
    integer, intent (in) :: i
# ifdef USE_C_INDEX
    interface
       function il_idx_e_c (lo,num)
         use layouts_type, only: e_layout_type
         integer :: il_idx_e_c
         type (e_layout_type) :: lo
         integer :: num
       end function il_idx_e_c
    end interface
    il_idx_e = il_idx_e_c (lo,i)
# else
    select case (layout)
    case ('yxels')
       il_idx_e = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%nsign/lo%naky/lo%nakx, lo%nlambda)
    case ('yxles')
       il_idx_e = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%nsign/lo%naky/lo%nakx, lo%nlambda)
    case ('lexys')
       il_idx_e = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%nsign, lo%nlambda)
    case ('lxyes')
       il_idx_e = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%nsign, lo%nlambda)
    case ('lyxes')
       il_idx_e = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%nsign, lo%nlambda)
    end select
# endif

  end function il_idx_e

# ifdef USE_C_INDEX
  function it_idx_e (lo, i)
# else
  elemental function it_idx_e (lo, i)
# endif
    implicit none
    integer :: it_idx_e
    type (e_layout_type), intent (in) :: lo
    integer, intent (in) :: i
# ifdef USE_C_INDEX
    interface
       function it_idx_e_c (lo,num)
         use layouts_type, only: e_layout_type
         integer :: it_idx_e_c
         type (e_layout_type) :: lo
         integer :: num
       end function it_idx_e_c
    end interface
    it_idx_e = it_idx_e_c (lo,i)
# else
    select case (layout)
    case ('yxels')
       it_idx_e = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%nsign/lo%naky, lo%nakx)
    case ('yxles')
       it_idx_e = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%nsign/lo%naky, lo%nakx)
    case ('lexys')
       it_idx_e = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%nsign/lo%nlambda, lo%nakx)
    case ('lxyes')
       it_idx_e = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%nsign/lo%nlambda, lo%nakx)
    case ('lyxes')
       it_idx_e = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%nsign/lo%nlambda/lo%naky, lo%nakx)
    end select
# endif

  end function it_idx_e

# ifdef USE_C_INDEX
  function ik_idx_e (lo, i)
# else
  elemental function ik_idx_e (lo, i)
# endif
    implicit none
    integer :: ik_idx_e
    type (e_layout_type), intent (in) :: lo
    integer, intent (in) :: i
# ifdef USE_C_INDEX
    interface
       function ik_idx_e_c (lo,num)
         use layouts_type, only: e_layout_type
         integer :: ik_idx_e_c
         type (e_layout_type) :: lo
         integer :: num
       end function ik_idx_e_c
    end interface
    ik_idx_e = ik_idx_e_c (lo,i)
# else
    select case (layout)
    case ('yxels')
       ik_idx_e = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%nsign, lo%naky)
    case ('yxles')
       ik_idx_e = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%nsign, lo%naky)
    case ('lexys')
       ik_idx_e = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%nsign/lo%nlambda/lo%nakx, lo%naky)
    case ('lxyes')
       ik_idx_e = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%nsign/lo%nlambda/lo%nakx, lo%naky)
    case ('lyxes')
       ik_idx_e = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%nsign/lo%nlambda, lo%naky)
    end select
# endif

  end function ik_idx_e

  elemental function ig_idx_e (lo, i)
    implicit none
    integer :: ig_idx_e
    type (e_layout_type), intent (in) :: lo
    integer, intent (in) :: i
    ! No need for branch
    ig_idx_e = -lo%ntgrid + mod(i - lo%llim_world, 2*lo%ntgrid + 1)
  end function ig_idx_e

# ifdef USE_C_INDEX
  function idx_e (lo, ig, isign, ik, it, il, is)
# else
  elemental function idx_e (lo, ig, isign, ik, it, il, is)
# endif
    implicit none
    integer :: idx_e
    type (e_layout_type), intent (in) :: lo
    integer, intent (in) :: ig, isign, ik, it, il, is
# ifdef USE_C_INDEX
    interface
       function idx_e_c (lo,ig,isign,ik,it,il,is)
         use layouts_type, only: e_layout_type
         integer :: idx_e_c
         type (e_layout_type) :: lo
         integer :: ig,isign,ik,it,il,is
       end function idx_e_c
    end interface
    idx_e = idx_e_c (lo, ig, isign, ik, it, il, is)
# else
    select case (layout)
    case ('yxels')
       idx_e = ig+lo%ntgrid + (2*lo%ntgrid+1)*(isign-1 + lo%nsign*(ik-1 &
            + lo%naky*(it-1 + lo%nakx*(il-1 + lo%nlambda*(is-1)))))
    case ('yxles')
       idx_e = ig+lo%ntgrid + (2*lo%ntgrid+1)*(isign-1 + lo%nsign*(ik-1 &
            + lo%naky*(it-1 + lo%nakx*(il-1 + lo%nlambda*(is-1)))))
    case ('lexys')
       idx_e = ig+lo%ntgrid + (2*lo%ntgrid+1)*(isign-1 + lo%nsign*(il-1 &
            + lo%nlambda*(it-1 + lo%nakx*(ik-1 + lo%naky*(is-1)))))
    case ('lxyes')
       idx_e = ig+lo%ntgrid + (2*lo%ntgrid+1)*(isign-1 + lo%nsign*(il-1 &
            + lo%nlambda*(it-1 + lo%nakx*(ik-1 + lo%naky*(is-1)))))
    case ('lyxes')
       idx_e = ig+lo%ntgrid + (2*lo%ntgrid+1)*(isign-1 + lo%nsign*(il-1 &
            + lo%nlambda*(ik-1 + lo%naky*(it-1 + lo%nakx*(is-1)))))
    end select
# endif

  end function idx_e

  elemental function proc_id_e (lo, i)
    implicit none
    integer :: proc_id_e
    type (e_layout_type), intent (in) :: lo
    integer, intent (in) :: i

    proc_id_e = i/lo%blocksize

  end function proc_id_e

# ifdef USE_C_INDEX
  function idx_local_e (lo, ig, isign, ik, it, il, is)
# else
  elemental function idx_local_e (lo, ig, isign, ik, it, il, is)
# endif
    implicit none
    logical :: idx_local_e
    type (e_layout_type), intent (in) :: lo
    integer, intent (in) :: ig, isign, ik, it, il, is

    idx_local_e = idx_local(lo, idx(lo, ig, isign, ik, it, il, is))
  end function idx_local_e

  elemental function ig_local_e (lo, ig)
    implicit none
    logical :: ig_local_e
    type (e_layout_type), intent (in) :: lo
    integer, intent (in) :: ig

    ig_local_e = lo%iproc == proc_id(lo, ig)
  end function ig_local_e

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Lorentz layouts
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine init_lorentz_layouts &
       (ntgrid, naky, nakx, nlambda, negrid, nspec)
    use mp, only: iproc, nproc, proc0, mp_abort
    use file_utils, only: error_unit
    implicit none
    integer, intent (in) :: ntgrid, naky, nakx, nlambda, negrid, nspec
    integer :: ngroup, nprocset
    logical, save :: initialized = .false.
# ifdef USE_C_INDEX
    integer :: ierr
# endif
# ifdef USE_C_INDEX
    interface
       function init_indices_lzlo_c (layout)
         integer :: init_indices_lzlo_c
         character(*) :: layout
       end function init_indices_lzlo_c
    end interface
# endif

    if (initialized) return
    initialized = .true.
    
    lz_lo%iproc = iproc
    lz_lo%ntgrid = ntgrid
    lz_lo%naky = naky
    lz_lo%nakx = nakx
    lz_lo%negrid = negrid
    lz_lo%nspec = nspec
    lz_lo%nlambda = nlambda
    lz_lo%llim_world = 0
    lz_lo%ulim_world = (2*ntgrid+1)*naky*nakx*negrid*nspec - 1

    if (lz_lo%ulim_world < 0) then
       if (proc0) then
          write (error_unit(),*) 'ERROR: negative ulim_world in lz_lo'
          write (error_unit(),*) 'ERROR: possibly too many grid points'
       end if
       call mp_abort ('STOP in init_lorentz_layouts')
    end if

    if (layout == 'lxyes' .or. layout == 'lexys' .or. layout == 'lyxes') &
         call check_llocal (nakx, naky, nlambda, negrid, nspec)

    if (lambda_local) then

       lz_lo%groupblocksize = (naky*nakx*negrid*nspec-1)/nproc + 1
       
       ngroup = min (nproc, naky*nakx*negrid*nspec)
       lz_lo%ngroup = ngroup
       
       nprocset = nproc / lz_lo%ngroup
       lz_lo%nprocset = nprocset

       lz_lo%iset   = mod (iproc, nprocset)
       lz_lo%igroup = mod (iproc/nprocset, ngroup)

       lz_lo%llim_group = 0
       lz_lo%ulim_group = (2*ntgrid+1)*lz_lo%groupblocksize - 1
       lz_lo%gsize      = (2*ntgrid+1)*lz_lo%groupblocksize 
       
       lz_lo%nset = lz_lo%ulim_group/lz_lo%nprocset + 1
       
       lz_lo%llim_proc = lz_lo%igroup*lz_lo%gsize + lz_lo%iset*lz_lo%nset
       lz_lo%ulim_proc = min(lz_lo%ulim_group+lz_lo%igroup*lz_lo%gsize, &
            lz_lo%llim_proc + lz_lo%nset - 1)
       lz_lo%ulim_alloc = max(lz_lo%llim_proc, lz_lo%ulim_proc)
    else

       lz_lo%blocksize = lz_lo%ulim_world/nproc + 1
       lz_lo%llim_proc = lz_lo%blocksize*iproc
       lz_lo%ulim_proc &
            = min(lz_lo%ulim_world, lz_lo%llim_proc + lz_lo%blocksize - 1)
       lz_lo%ulim_alloc = max(lz_lo%llim_proc, lz_lo%ulim_proc)
    end if

# ifdef USE_C_INDEX
    ierr = init_indices_lzlo_c (layout)
    if (ierr /= 0) &
         & write (error_unit(),*) 'ERROR: layout not found: ', trim(layout)
# endif

  end subroutine init_lorentz_layouts

# ifdef USE_C_INDEX
  function is_idx_lz (lo, i)
# else
  elemental function is_idx_lz (lo, i)
# endif
    implicit none
    integer :: is_idx_lz
    type (lz_layout_type), intent (in) :: lo
    integer, intent (in) :: i
# ifdef USE_C_INDEX
    interface
       function is_idx_lz_c (lo,num)
         use layouts_type, only: lz_layout_type
         integer :: is_idx_lz_c
         type (lz_layout_type) :: lo
         integer :: num
       end function is_idx_lz_c
    end interface
    is_idx_lz = is_idx_lz_c (lo, i)
# else
    ! TT: the order of the division doesn't matter, so no need for branching
    is_idx_lz = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%naky/lo%nakx/lo%negrid, lo%nspec)
# endif

  end function is_idx_lz

# ifdef USE_C_INDEX
  function ie_idx_lz (lo, i)
# else
  elemental function ie_idx_lz (lo, i)
# endif
    implicit none
    integer :: ie_idx_lz
    type (lz_layout_type), intent (in) :: lo
    integer, intent (in) :: i
# ifdef USE_C_INDEX
    interface
       function ie_idx_lz_c (lo,num)
         use layouts_type, only: lz_layout_type
         integer :: ie_idx_lz_c
         type (lz_layout_type) :: lo
         integer :: num
       end function ie_idx_lz_c
    end interface
    ie_idx_lz = ie_idx_lz_c (lo, i)
# else
    select case (layout)
    case ('yxels')
       ie_idx_lz = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%naky/lo%nakx, lo%negrid)
    case ('yxles')
       ie_idx_lz = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%naky/lo%nakx, lo%negrid)
    case ('lexys')
       ie_idx_lz = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1), lo%negrid)
    case ('lxyes')
       ie_idx_lz = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%nakx/lo%naky, lo%negrid)
    case ('lyxes')
       ie_idx_lz = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%naky/lo%nakx, lo%negrid)
    end select
# endif

  end function ie_idx_lz

# ifdef USE_C_INDEX
  function it_idx_lz (lo, i)
# else
  elemental function it_idx_lz (lo, i)
# endif
    implicit none
    integer :: it_idx_lz
    type (lz_layout_type), intent (in) :: lo
    integer, intent (in) :: i
# ifdef USE_C_INDEX
    interface
       function it_idx_lz_c (lo,num)
         use layouts_type, only: lz_layout_type
         integer :: it_idx_lz_c
         type (lz_layout_type) :: lo
         integer :: num
       end function it_idx_lz_c
    end interface
    it_idx_lz = it_idx_lz_c (lo, i)
# else
    select case (layout)
    case ('yxels')
       it_idx_lz = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%naky, lo%nakx)
    case ('yxles')
       it_idx_lz = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%naky, lo%nakx)
    case ('lexys')
       it_idx_lz = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%negrid, lo%nakx)
    case ('lxyes')
       it_idx_lz = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1), lo%nakx)
    case ('lyxes')
       it_idx_lz = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%naky, lo%nakx)
    end select
# endif

  end function it_idx_lz

# ifdef USE_C_INDEX
  function ik_idx_lz (lo, i)
# else
  elemental function ik_idx_lz (lo, i)
# endif
    implicit none
    integer :: ik_idx_lz
    type (lz_layout_type), intent (in) :: lo
    integer, intent (in) :: i
# ifdef USE_C_INDEX
    interface
       function ik_idx_lz_c (lo,num)
         use layouts_type, only: lz_layout_type
         integer :: ik_idx_lz_c
         type (lz_layout_type) :: lo
         integer :: num
       end function ik_idx_lz_c
    end interface
    ik_idx_lz = ik_idx_lz_c (lo, i)
# else
    select case (layout)
    case ('yxels')
       ik_idx_lz = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1), lo%naky)
    case ('yxles')
       ik_idx_lz = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1), lo%naky)
    case ('lexys')
       ik_idx_lz = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%negrid/lo%nakx, lo%naky)
    case ('lxyes')
       ik_idx_lz = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1)/lo%nakx, lo%naky)
    case ('lyxes')
       ik_idx_lz = 1 + mod((i - lo%llim_world)/(2*lo%ntgrid + 1), lo%naky)
    end select
# endif

  end function ik_idx_lz

  elemental function ig_idx_lz (lo, i)
    implicit none
    integer :: ig_idx_lz
    type (lz_layout_type), intent (in) :: lo
    integer, intent (in) :: i
    ! No need for branch
    ig_idx_lz = -lo%ntgrid + mod(i - lo%llim_world, 2*lo%ntgrid + 1)
  end function ig_idx_lz

# ifdef USE_C_INDEX
  function idx_lz (lo, ig, ik, it, ie, is)
# else
  elemental function idx_lz (lo, ig, ik, it, ie, is)
# endif
    implicit none
    integer :: idx_lz
    type (lz_layout_type), intent (in) :: lo
    integer, intent (in) :: ig, ik, it, ie, is
# ifdef USE_C_INDEX
    interface
       function idx_lz_c (lo,ig,ik,it,ie,is)
         use layouts_type, only: lz_layout_type
         integer :: idx_lz_c
         type (lz_layout_type) :: lo
         integer :: ig,ik,it,ie,is
       end function idx_lz_c
    end interface
    idx_lz = idx_lz_c (lo, ig, ik, it, ie, is)
# else
    select case (layout)
    case ('yxels')
       idx_lz = ig+lo%ntgrid + (2*lo%ntgrid+1)*(ik-1 + lo%naky*(it-1 &
            + lo%nakx*(ie-1 + lo%negrid*(is-1))))
    case ('yxles')
       idx_lz = ig+lo%ntgrid + (2*lo%ntgrid+1)*(ik-1 + lo%naky*(it-1 &
            + lo%nakx*(ie-1 + lo%negrid*(is-1))))
    case ('lexys')
       idx_lz = ig+lo%ntgrid + (2*lo%ntgrid+1)*(ie-1 + lo%negrid*(it-1 &
            + lo%nakx*(ik-1 + lo%naky*(is-1))))
    case ('lxyes')
       idx_lz = ig+lo%ntgrid + (2*lo%ntgrid+1)*(it-1 + lo%nakx*(ik-1 &
            + lo%naky*(ie-1 + lo%negrid*(is-1))))
    case ('lyxes')
       idx_lz = ig+lo%ntgrid + (2*lo%ntgrid+1)*(ik-1 + lo%naky*(it-1 &
            + lo%nakx*(ie-1 + lo%negrid*(is-1))))
    end select
# endif

  end function idx_lz

  elemental function proc_id_lz (lo, i)
    implicit none
    integer :: proc_id_lz
    type (lz_layout_type), intent (in) :: lo
    integer, intent (in) :: i

    if (lambda_local) then
       proc_id_lz = (i/lo%gsize)*lo%nprocset + mod(i, lo%gsize)/lo%nset
    else
       proc_id_lz = i/lo%blocksize
    end if

  end function proc_id_lz

# ifdef USE_C_INDEX
  function idx_local_lz (lo, ig, ik, it, ie, is)
# else
  elemental function idx_local_lz (lo, ig, ik, it, ie, is)
# endif
    implicit none
    logical :: idx_local_lz
    type (lz_layout_type), intent (in) :: lo
    integer, intent (in) :: ig, ik, it, ie, is

    idx_local_lz = idx_local(lo, idx(lo, ig, ik, it, ie, is))
  end function idx_local_lz

  elemental function ig_local_lz (lo, ig)
    implicit none
    logical :: ig_local_lz
    type (lz_layout_type), intent (in) :: lo
    integer, intent (in) :: ig

    ig_local_lz = lo%iproc == proc_id(lo, ig)
  end function ig_local_lz

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! X-space layouts
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine init_x_transform_layouts &
       (ntgrid, naky, nakx, nlambda, negrid, nspec, nx)
    use mp, only: iproc, nproc, proc0, mp_abort
    use file_utils, only: error_unit
    implicit none
    integer, intent (in) :: ntgrid, naky, nakx, nlambda, negrid, nspec, nx
    logical, save :: initialized = .false.
    integer :: nprocset, ngroup, nblock

    if (initialized) return
    initialized = .true.

    xxf_lo%iproc = iproc
    xxf_lo%ntgrid = ntgrid
    xxf_lo%nsign = 2
    xxf_lo%naky = naky
    xxf_lo%nakx = nakx
    if (nx > nakx) then
       xxf_lo%nx = nx
    else
       xxf_lo%nx = (3*nakx+1)/2
    end if
    xxf_lo%nadd = xxf_lo%nx - nakx
    xxf_lo%nlambda = nlambda
    xxf_lo%negrid = negrid
    xxf_lo%nspec = nspec
    xxf_lo%llim_world = 0
    xxf_lo%ulim_world = naky*(2*ntgrid+1)*2*nlambda*negrid*nspec - 1

    if (xxf_lo%ulim_world < 0) then
       if (proc0) then
          write (error_unit(),*) 'ERROR: negative ulim_world in xxf_lo'
          write (error_unit(),*) 'ERROR: possibly too many grid points'
       end if
       call mp_abort ('STOP in init_x_transform_layouts')
    end if

    call check_accel (nakx, naky, nlambda, negrid, nspec, nblock)
    if (accel_lxyes) then  

       xxf_lo%groupblocksize = (nblock-1)/nproc + 1

       ngroup = min (nproc, nblock)
       xxf_lo%ngroup = ngroup
       
       nprocset = nproc / xxf_lo%ngroup
       xxf_lo%nprocset = nprocset

       xxf_lo%iset   = mod (iproc, nprocset)
       xxf_lo%igroup = mod (iproc/nprocset, ngroup)

       xxf_lo%llim_group = 0
       xxf_lo%ulim_group = naky*(2*ntgrid+1)*2*nlambda*xxf_lo%groupblocksize - 1
       xxf_lo%gsize      = naky*(2*ntgrid+1)*2*nlambda*xxf_lo%groupblocksize 
       
       xxf_lo%nset = xxf_lo%ulim_group/xxf_lo%nprocset + 1
       
       xxf_lo%llim_proc = xxf_lo%igroup*xxf_lo%gsize + xxf_lo%iset*xxf_lo%nset
       xxf_lo%ulim_proc = min(xxf_lo%ulim_group+xxf_lo%igroup*xxf_lo%gsize, &
            xxf_lo%llim_proc + xxf_lo%nset - 1)
       xxf_lo%ulim_alloc = max(xxf_lo%llim_proc, xxf_lo%ulim_proc)

    else
       xxf_lo%blocksize = xxf_lo%ulim_world/nproc + 1
       xxf_lo%llim_proc = xxf_lo%blocksize*iproc
       xxf_lo%ulim_proc &
            = min(xxf_lo%ulim_world, xxf_lo%llim_proc + xxf_lo%blocksize - 1)
       xxf_lo%ulim_alloc = max(xxf_lo%llim_proc, xxf_lo%ulim_proc)
    end if

!    call barrier
!    do ip=0,nproc-1
!       if (ip == iproc) then
!          write (*,*) 'iproc= ',ip,' llim= ',xxf_lo%llim_proc,' ulim= ',xxf_lo%ulim_proc, &
!               & ' iset= ',xxf_lo%iset,' igroup= ',xxf_lo%igroup
!       end if
!    call barrier
!    end do

  end subroutine init_x_transform_layouts

  elemental function is_idx_xxf (lo, i)
    implicit none
    integer :: is_idx_xxf
    type (xxf_layout_type), intent (in) :: lo
    integer, intent (in) :: i

    ! TT: the order of the division doesn't matter, so no need for branching
    is_idx_xxf = 1 + mod((i - lo%llim_world)/lo%naky/(2*lo%ntgrid + 1)/lo%nsign/lo%negrid/lo%nlambda, lo%nspec)

  end function is_idx_xxf

  elemental function ie_idx_xxf (lo, i)
    implicit none
    integer :: ie_idx_xxf
    type (xxf_layout_type), intent (in) :: lo
    integer, intent (in) :: i

    select case (layout)
    case ('yxels')
       ie_idx_xxf = 1 + mod((i - lo%llim_world)/lo%naky/(2*lo%ntgrid + 1)/lo%nsign, lo%negrid)
    case ('yxles')
       ie_idx_xxf = 1 + mod((i - lo%llim_world)/lo%naky/(2*lo%ntgrid + 1)/lo%nsign/lo%nlambda, lo%negrid)
    case ('lexys')
       ie_idx_xxf = 1 + mod((i - lo%llim_world)/lo%naky/(2*lo%ntgrid + 1)/lo%nsign/lo%nlambda, lo%negrid)
    case ('lxyes')
       ie_idx_xxf = 1 + mod((i - lo%llim_world)/lo%naky/(2*lo%ntgrid + 1)/lo%nsign/lo%nlambda, lo%negrid)
    case ('lyxes')
       ie_idx_xxf = 1 + mod((i - lo%llim_world)/lo%naky/(2*lo%ntgrid + 1)/lo%nsign/lo%nlambda, lo%negrid)
    end select
  end function ie_idx_xxf

  elemental function il_idx_xxf (lo, i)
    implicit none
    integer :: il_idx_xxf
    type (xxf_layout_type), intent (in) :: lo
    integer, intent (in) :: i

    select case (layout)
    case ('yxels')
       il_idx_xxf = 1 + mod((i - lo%llim_world)/lo%naky/(2*lo%ntgrid + 1)/lo%nsign/lo%negrid, lo%nlambda)
    case ('yxles')
       il_idx_xxf = 1 + mod((i - lo%llim_world)/lo%naky/(2*lo%ntgrid + 1)/lo%nsign, lo%nlambda)
    case ('lexys')
       il_idx_xxf = 1 + mod((i - lo%llim_world)/lo%naky/(2*lo%ntgrid + 1)/lo%nsign, lo%nlambda)
    case ('lxyes')
       il_idx_xxf = 1 + mod((i - lo%llim_world)/lo%naky/(2*lo%ntgrid + 1)/lo%nsign, lo%nlambda)
    case ('lyxes')
       il_idx_xxf = 1 + mod((i - lo%llim_world)/lo%naky/(2*lo%ntgrid + 1)/lo%nsign, lo%nlambda)
    end select
  end function il_idx_xxf

  elemental function isign_idx_xxf (lo, i)
    implicit none
    integer :: isign_idx_xxf
    type (xxf_layout_type), intent (in) :: lo
    integer, intent (in) :: i

    select case (layout)
    case ('yxels')
       isign_idx_xxf = 1 + mod((i - lo%llim_world)/lo%naky/(2*lo%ntgrid + 1), lo%nsign)
    case ('yxles')
       isign_idx_xxf = 1 + mod((i - lo%llim_world)/lo%naky/(2*lo%ntgrid + 1), lo%nsign)
    case ('lexys')
       isign_idx_xxf = 1 + mod((i - lo%llim_world)/lo%naky/(2*lo%ntgrid + 1), lo%nsign)
    case ('lxyes')
       isign_idx_xxf = 1 + mod((i - lo%llim_world)/lo%naky/(2*lo%ntgrid + 1), lo%nsign)
    case ('lyxes')
       isign_idx_xxf = 1 + mod((i - lo%llim_world)/lo%naky/(2*lo%ntgrid + 1), lo%nsign)
    end select
  end function isign_idx_xxf

  elemental function ig_idx_xxf (lo, i)
    implicit none
    integer :: ig_idx_xxf
    type (xxf_layout_type), intent (in) :: lo
    integer, intent (in) :: i

    select case (layout)
    case ('yxels')
       ig_idx_xxf = -lo%ntgrid + mod((i - lo%llim_world)/lo%naky, 2*lo%ntgrid + 1)
    case ('yxles')
       ig_idx_xxf = -lo%ntgrid + mod((i - lo%llim_world)/lo%naky, 2*lo%ntgrid + 1)
    case ('lexys')
       ig_idx_xxf = -lo%ntgrid + mod((i - lo%llim_world)/lo%naky, 2*lo%ntgrid + 1)
    case ('lxyes')
       ig_idx_xxf = -lo%ntgrid + mod((i - lo%llim_world)/lo%naky, 2*lo%ntgrid + 1)
    case ('lyxes')
       ig_idx_xxf = -lo%ntgrid + mod((i - lo%llim_world)/lo%naky, 2*lo%ntgrid + 1)
    end select
  end function ig_idx_xxf

  elemental function ik_idx_xxf (lo, i)
    implicit none
    integer :: ik_idx_xxf
    type (xxf_layout_type), intent (in) :: lo
    integer, intent (in) :: i

    select case (layout)
    case ('yxels')
       ik_idx_xxf = 1 + mod(i - lo%llim_world, lo%naky)
    case ('yxles')
       ik_idx_xxf = 1 + mod(i - lo%llim_world, lo%naky)
    case ('lexys')
       ik_idx_xxf = 1 + mod(i - lo%llim_world, lo%naky)
    case ('lxyes')
       ik_idx_xxf = 1 + mod(i - lo%llim_world, lo%naky)
    case ('lyxes')
       ik_idx_xxf = 1 + mod(i - lo%llim_world, lo%naky)
    end select
  end function ik_idx_xxf

  elemental function idx_xxf (lo, ig, isign, ik, il, ie, is)
    implicit none
    integer :: idx_xxf
    type (xxf_layout_type), intent (in) :: lo
    integer, intent (in) :: ig, isign, ik, il, ie, is

    select case (layout)
    case ('yxels')
       idx_xxf = ik-1 + lo%naky*(ig+lo%ntgrid + (2*lo%ntgrid+1)*(isign-1 &
            + lo%nsign*(ie-1 + lo%negrid*(il-1 + lo%nlambda*(is-1)))))
    case ('yxles')
       idx_xxf = ik-1 + lo%naky*(ig+lo%ntgrid + (2*lo%ntgrid+1)*(isign-1 &
            + lo%nsign*(il-1 + lo%nlambda*(ie-1 + lo%negrid*(is-1)))))
    case ('lexys')
       idx_xxf = ik-1 + lo%naky*(ig+lo%ntgrid + (2*lo%ntgrid+1)*(isign-1 &
            + lo%nsign*(il-1 + lo%nlambda*(ie-1 + lo%negrid*(is-1)))))
    case ('lxyes')
       idx_xxf = ik-1 + lo%naky*(ig+lo%ntgrid + (2*lo%ntgrid+1)*(isign-1 &
            + lo%nsign*(il-1 + lo%nlambda*(ie-1 + lo%negrid*(is-1)))))
    case ('lyxes')
       idx_xxf = ik-1 + lo%naky*(ig+lo%ntgrid + (2*lo%ntgrid+1)*(isign-1 &
            + lo%nsign*(il-1 + lo%nlambda*(ie-1 + lo%negrid*(is-1)))))
    end select
  end function idx_xxf

  elemental function proc_id_xxf (lo, i)
    implicit none
    integer :: proc_id_xxf
    type (xxf_layout_type), intent (in) :: lo
    integer, intent (in) :: i

    if (accel_lxyes) then
       proc_id_xxf = (i/lo%gsize)*lo%nprocset + mod(i, lo%gsize)/lo%nset
    else
       proc_id_xxf = i/lo%blocksize
    end if

  end function proc_id_xxf

  elemental function idx_local_xxf (lo, ig, isign, ik, il, ie, is)
    implicit none
    logical :: idx_local_xxf
    type (xxf_layout_type), intent (in) :: lo
    integer, intent (in) :: ig, isign, ik, il, ie, is
    idx_local_xxf = idx_local (lo, idx(lo, ig, isign, ik, il, ie, is))
  end function idx_local_xxf

  elemental function ig_local_xxf (lo, i)
    implicit none
    logical ig_local_xxf
    type (xxf_layout_type), intent (in) :: lo
    integer, intent (in) :: i
    ig_local_xxf = lo%iproc == proc_id(lo, i)
  end function ig_local_xxf

  elemental function xxf_ky_is_zero (lo, i)
    implicit none
    logical xxf_ky_is_zero
    type (xxf_layout_type), intent (in) :: lo
    integer, intent (in) :: i

    xxf_ky_is_zero = 0 == mod(i, lo%naky)

  end function xxf_ky_is_zero

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Y-space layouts
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine init_y_transform_layouts &
       (ntgrid, naky, nakx, nlambda, negrid, nspec, nx, ny)
    use mp, only: iproc, nproc, proc0, mp_abort
    use file_utils, only: error_unit
    implicit none
    integer, intent (in) :: ntgrid, naky, nakx, nlambda, negrid, nspec
    integer, intent (in) :: nx, ny
    logical, save :: initialized = .false.
    integer :: nnx, nny, ngroup, nprocset, nblock

    if (initialized) return
    initialized = .true.

    if (nx > nakx) then
       nnx = nx
    else
       nnx = (3*nakx+1)/2
    end if
    if (ny > naky) then
       nny = ny
    else
       nny = 3*naky
    end if

    yxf_lo%iproc = iproc
    yxf_lo%ntgrid = ntgrid
    yxf_lo%nsign = 2
    yxf_lo%naky = naky
    yxf_lo%ny = nny
    yxf_lo%nakx = nakx
    yxf_lo%nx = nnx
    yxf_lo%nlambda = nlambda
    yxf_lo%negrid = negrid
    yxf_lo%nspec = nspec
    yxf_lo%llim_world = 0
    yxf_lo%ulim_world = nnx*(2*ntgrid+1)*2*nlambda*negrid*nspec - 1

    if (yxf_lo%ulim_world < 0) then
       if (proc0) then
          write (error_unit(),*) 'ERROR: negative ulim_world in yxf_lo'
          write (error_unit(),*) 'ERROR: possibly too many grid points'
       end if
       call mp_abort ('STOP in init_y_transform_layouts')
    end if

    call check_accel (nakx, naky, nlambda, negrid, nspec, nblock)

    if (accel_lxyes) then  

       yxf_lo%groupblocksize = (nblock-1)/nproc + 1

       ngroup = min (nproc, nblock)
       yxf_lo%ngroup = ngroup
       
       nprocset = nproc / yxf_lo%ngroup
       yxf_lo%nprocset = nprocset

       yxf_lo%iset   = mod (iproc, nprocset)
       yxf_lo%igroup = mod (iproc/nprocset, ngroup)

       yxf_lo%llim_group = 0
       yxf_lo%ulim_group = nnx*(2*ntgrid+1)*2*nlambda - 1
       yxf_lo%gsize      = nnx*(2*ntgrid+1)*2*nlambda*yxf_lo%groupblocksize
       
       yxf_lo%nset = yxf_lo%ulim_group/yxf_lo%nprocset + 1
       
       yxf_lo%llim_proc = yxf_lo%igroup*yxf_lo%gsize + yxf_lo%iset*yxf_lo%nset
       yxf_lo%ulim_proc = min(yxf_lo%ulim_group+yxf_lo%igroup*yxf_lo%gsize, &
            yxf_lo%llim_proc + yxf_lo%nset - 1)
       yxf_lo%ulim_alloc = max(yxf_lo%llim_proc, yxf_lo%ulim_proc)

    else
       yxf_lo%blocksize = yxf_lo%ulim_world/nproc + 1
       yxf_lo%llim_proc = yxf_lo%blocksize*iproc
       yxf_lo%ulim_proc &
            = min(yxf_lo%ulim_world, yxf_lo%llim_proc + yxf_lo%blocksize - 1)
       yxf_lo%ulim_alloc = max(yxf_lo%llim_proc, yxf_lo%ulim_proc)
    end if

  end subroutine init_y_transform_layouts

  elemental function is_idx_yxf (lo, i)
    implicit none
    integer :: is_idx_yxf
    type (yxf_layout_type), intent (in) :: lo
    integer, intent (in) :: i

    ! TT: the order of the division doesn't matter, so no need for branching
    is_idx_yxf = 1 + mod((i - lo%llim_world)/lo%nx/(2*lo%ntgrid + 1)/lo%nsign/lo%negrid/lo%nlambda, lo%nspec)

  end function is_idx_yxf

  elemental function ie_idx_yxf (lo, i)
    implicit none
    integer :: ie_idx_yxf
    type (yxf_layout_type), intent (in) :: lo
    integer, intent (in) :: i

    select case (layout)
    case ('yxels')
       ie_idx_yxf = 1 + mod((i - lo%llim_world)/lo%nx/(2*lo%ntgrid + 1)/lo%nsign, lo%negrid)
    case ('yxles')
       ie_idx_yxf = 1 + mod((i - lo%llim_world)/lo%nx/(2*lo%ntgrid + 1)/lo%nsign/lo%nlambda, lo%negrid)
    case ('lexys')
       ie_idx_yxf = 1 + mod((i - lo%llim_world)/lo%nx/(2*lo%ntgrid + 1)/lo%nsign/lo%nlambda, lo%negrid)
    case ('lxyes')
       ie_idx_yxf = 1 + mod((i - lo%llim_world)/lo%nx/(2*lo%ntgrid + 1)/lo%nsign/lo%nlambda, lo%negrid)
    case ('lyxes')
       ie_idx_yxf = 1 + mod((i - lo%llim_world)/lo%nx/(2*lo%ntgrid + 1)/lo%nsign/lo%nlambda, lo%negrid)
    end select
  end function ie_idx_yxf

  elemental function il_idx_yxf (lo, i)
    implicit none
    integer :: il_idx_yxf
    type (yxf_layout_type), intent (in) :: lo
    integer, intent (in) :: i

    select case (layout)
    case ('yxels')
       il_idx_yxf = 1 + mod((i - lo%llim_world)/lo%nx/(2*lo%ntgrid + 1)/lo%nsign/lo%negrid, lo%nlambda)
    case ('yxles')
       il_idx_yxf = 1 + mod((i - lo%llim_world)/lo%nx/(2*lo%ntgrid + 1)/lo%nsign, lo%nlambda)
    case ('lexys')
       il_idx_yxf = 1 + mod((i - lo%llim_world)/lo%nx/(2*lo%ntgrid + 1)/lo%nsign, lo%nlambda)
    case ('lxyes')
       il_idx_yxf = 1 + mod((i - lo%llim_world)/lo%nx/(2*lo%ntgrid + 1)/lo%nsign, lo%nlambda)
    case ('lyxes')
       il_idx_yxf = 1 + mod((i - lo%llim_world)/lo%nx/(2*lo%ntgrid + 1)/lo%nsign, lo%nlambda)
    end select
  end function il_idx_yxf

  elemental function isign_idx_yxf (lo, i)
    implicit none
    integer :: isign_idx_yxf
    type (yxf_layout_type), intent (in) :: lo
    integer, intent (in) :: i

    select case (layout)
    case ('yxels')
       isign_idx_yxf = 1 + mod((i - lo%llim_world)/lo%nx/(2*lo%ntgrid+1), lo%nsign)
    case ('yxles')
       isign_idx_yxf = 1 + mod((i - lo%llim_world)/lo%nx/(2*lo%ntgrid+1), lo%nsign)
    case ('lexys')
       isign_idx_yxf = 1 + mod((i - lo%llim_world)/lo%nx/(2*lo%ntgrid+1), lo%nsign)
    case ('lxyes')
       isign_idx_yxf = 1 + mod((i - lo%llim_world)/lo%nx/(2*lo%ntgrid+1), lo%nsign)
    case ('lyxes')
       isign_idx_yxf = 1 + mod((i - lo%llim_world)/lo%nx/(2*lo%ntgrid+1), lo%nsign)
    end select
  end function isign_idx_yxf

  elemental function ig_idx_yxf (lo, i)
    implicit none
    integer :: ig_idx_yxf
    type (yxf_layout_type), intent (in) :: lo
    integer, intent (in) :: i

    select case (layout)
    case ('yxels')
       ig_idx_yxf = -lo%ntgrid + mod((i - lo%llim_world)/lo%nx, 2*lo%ntgrid + 1)
    case ('yxles')
       ig_idx_yxf = -lo%ntgrid + mod((i - lo%llim_world)/lo%nx, 2*lo%ntgrid + 1)
    case ('lexys')
       ig_idx_yxf = -lo%ntgrid + mod((i - lo%llim_world)/lo%nx, 2*lo%ntgrid + 1)
    case ('lxyes')
       ig_idx_yxf = -lo%ntgrid + mod((i - lo%llim_world)/lo%nx, 2*lo%ntgrid + 1)
    case ('lyxes')
       ig_idx_yxf = -lo%ntgrid + mod((i - lo%llim_world)/lo%nx, 2*lo%ntgrid + 1)
    end select
  end function ig_idx_yxf

  elemental function it_idx_yxf (lo, i)
    implicit none
    integer :: it_idx_yxf
    type (yxf_layout_type), intent (in) :: lo
    integer, intent (in) :: i

    select case (layout)
    case ('yxels')
       it_idx_yxf = 1 + mod(i - lo%llim_world, lo%nx)
    case ('yxles')
       it_idx_yxf = 1 + mod(i - lo%llim_world, lo%nx)
    case ('lexys')
       it_idx_yxf = 1 + mod(i - lo%llim_world, lo%nx)
    case ('lxyes')
       it_idx_yxf = 1 + mod(i - lo%llim_world, lo%nx)
    case ('lyxes')
       it_idx_yxf = 1 + mod(i - lo%llim_world, lo%nx)
    end select
  end function it_idx_yxf

  elemental function idx_yxf (lo, ig, isign, it, il, ie, is)
    implicit none
    integer :: idx_yxf
    type (yxf_layout_type), intent (in) :: lo
    integer, intent (in) :: ig, isign, it, il, ie, is

    select case (layout)
    case ('yxels')
       idx_yxf = it-1 + lo%nx*(ig+lo%ntgrid + (2*lo%ntgrid+1)*(isign-1 &
            + lo%nsign*(ie-1 + lo%negrid*(il-1 + lo%nlambda*(is-1)))))
    case ('yxles')
       idx_yxf = it-1 + lo%nx*(ig+lo%ntgrid + (2*lo%ntgrid+1)*(isign-1 &
            + lo%nsign*(il-1 + lo%nlambda*(ie-1 + lo%negrid*(is-1)))))
    case ('lexys')
       idx_yxf = it-1 + lo%nx*(ig+lo%ntgrid + (2*lo%ntgrid+1)*(isign-1 &
            + lo%nsign*(il-1 + lo%nlambda*(ie-1 + lo%negrid*(is-1)))))
    case ('lxyes')
       idx_yxf = it-1 + lo%nx*(ig+lo%ntgrid + (2*lo%ntgrid+1)*(isign-1 &
            + lo%nsign*(il-1 + lo%nlambda*(ie-1 + lo%negrid*(is-1)))))
    case ('lyxes')
       idx_yxf = it-1 + lo%nx*(ig+lo%ntgrid + (2*lo%ntgrid+1)*(isign-1 &
            + lo%nsign*(il-1 + lo%nlambda*(ie-1 + lo%negrid*(is-1)))))
    end select
  end function idx_yxf

  elemental function proc_id_yxf (lo, i)
    implicit none
    integer :: proc_id_yxf
    type (yxf_layout_type), intent (in) :: lo
    integer, intent (in) :: i

    if (accel_lxyes) then
       proc_id_yxf = (i/lo%gsize)*lo%nprocset + mod(i, lo%gsize)/lo%nset
    else
       proc_id_yxf = i/lo%blocksize
    end if
  end function proc_id_yxf

  elemental function idx_local_yxf (lo, ig, isign, it, il, ie, is)
    implicit none
    logical :: idx_local_yxf
    type (yxf_layout_type), intent (in) :: lo
    integer, intent (in) :: ig, isign, it, il, ie, is
    idx_local_yxf = idx_local (lo, idx(lo, ig, isign, it, il, ie, is))
  end function idx_local_yxf

  elemental function ig_local_yxf (lo, i)
    implicit none
    logical ig_local_yxf
    type (yxf_layout_type), intent (in) :: lo
    integer, intent (in) :: i
    ig_local_yxf = lo%iproc == proc_id(lo, i)
  end function ig_local_yxf

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Accelerated FFT layouts
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine init_accel_transform_layouts &
       (ntgrid, naky, nakx, nlambda, negrid, nspec, nx, ny)
    use mp, only: iproc, nproc, proc0, mp_abort
    use file_utils, only: error_unit
    implicit none
    integer, intent (in) :: ntgrid, naky, nakx, nlambda, negrid, nspec
    integer, intent (in) :: nx, ny
    logical, save :: initialized = .false.
    integer :: nnx, nny

    if (initialized) return
    initialized = .true.

    if (nx > nakx) then
       nnx = nx
    else
       nnx = (3*nakx+1)/2
    end if
    if (ny > naky) then
       nny = ny
    else
       nny = 3*naky
    end if

    accelx_lo%iproc = iproc
    accelx_lo%ntgrid = ntgrid
    accelx_lo%nsign = 2
    accelx_lo%naky = naky
    accelx_lo%ny = nny
    accelx_lo%nakx = nakx
    accelx_lo%nx = nnx
    accelx_lo%nxny = nnx*nny
    accelx_lo%nlambda = nlambda
    accelx_lo%negrid = negrid
    accelx_lo%nspec = nspec
    accelx_lo%llim_world = 0
    accelx_lo%ulim_world = nnx*nny*nlambda*negrid*nspec - 1
    accelx_lo%blocksize = accelx_lo%ulim_world/nproc + 1
    accelx_lo%llim_proc = accelx_lo%blocksize*iproc
    accelx_lo%ulim_proc &
         = min(accelx_lo%ulim_world, accelx_lo%llim_proc + accelx_lo%blocksize - 1)
    accelx_lo%ulim_alloc = max(accelx_lo%llim_proc, accelx_lo%ulim_proc)

    if (accelx_lo%ulim_world < 0) then
       if (proc0) then
          write (error_unit(),*) 'ERROR: negative ulim_world in accelx_lo'
          write (error_unit(),*) 'ERROR: possibly too many grid points'
       end if
       call mp_abort ('STOP in init_accel_transform_layouts')
       ! This check is not needed for accel_lo as
       ! accel_lo%ulim_world ~ accelx_lo%ulim_world / 2
    end if

    accel_lo%iproc = iproc
    accel_lo%ntgrid = ntgrid
    accel_lo%nsign = 2
    accel_lo%naky = naky
    accel_lo%ndky = nny/2+1  ! Note: this is for dealiased k space quantities
    accel_lo%ny = nny
    accel_lo%nakx = nakx
    accel_lo%nx = nnx
    accel_lo%nxny = nnx*nny
    accel_lo%nxnky = nnx*(nny/2+1)
    accel_lo%nlambda = nlambda
    accel_lo%negrid = negrid
    accel_lo%nspec = nspec
    accel_lo%nia = negrid*nlambda*nspec/nproc
    accel_lo%llim_world = 0
    accel_lo%ulim_world = nnx*(nny/2+1)*nlambda*negrid*nspec - 1
    accel_lo%blocksize = accel_lo%ulim_world/nproc + 1
    accel_lo%llim_proc = accel_lo%blocksize*iproc
    accel_lo%ulim_proc &
         = min(accel_lo%ulim_world, accel_lo%llim_proc + accel_lo%blocksize - 1)
    accel_lo%ulim_alloc = max(accel_lo%llim_proc, accel_lo%ulim_proc)

  end subroutine init_accel_transform_layouts

  elemental function is_idx_accelx (lo, i)
    implicit none
    integer :: is_idx_accelx
    type (accelx_layout_type), intent (in) :: lo
    integer, intent (in) :: i
    is_idx_accelx = 1 + mod((i - lo%llim_world)/lo%nxny/lo%negrid/lo%nlambda, lo%nspec)
  end function is_idx_accelx

  elemental function ie_idx_accelx (lo, i)
    implicit none
    integer :: ie_idx_accelx
    type (accelx_layout_type), intent (in) :: lo
    integer, intent (in) :: i

    select case (layout)
    case ('yxels')
       ie_idx_accelx = 1 + mod((i - lo%llim_world)/lo%nxny, lo%negrid)
    case ('yxles')
       ie_idx_accelx = 1 + mod((i - lo%llim_world)/lo%nxny/lo%nlambda, lo%negrid)
    end select
  end function ie_idx_accelx

  elemental function il_idx_accelx (lo, i)
    implicit none
    integer :: il_idx_accelx
    type (accelx_layout_type), intent (in) :: lo
    integer, intent (in) :: i

    select case (layout)
    case ('yxels')
       il_idx_accelx = 1 + mod((i - lo%llim_world)/lo%nxny/lo%negrid, lo%nlambda)
    case ('yxles')
       il_idx_accelx = 1 + mod((i - lo%llim_world)/lo%nxny, lo%nlambda)
    end select
  end function il_idx_accelx

  elemental function it_idx_accelx (lo, i)
    implicit none
    integer :: it_idx_accelx
    type (accelx_layout_type), intent (in) :: lo
    integer, intent (in) :: i

    it_idx_accelx = 1 + mod((i - lo%llim_world)/lo%ny, lo%nx)
  end function it_idx_accelx

  elemental function ik_idx_accelx (lo, i)
    implicit none
    integer :: ik_idx_accelx
    type (accelx_layout_type), intent (in) :: lo
    integer, intent (in) :: i

    ik_idx_accelx = 1 + mod((i - lo%llim_world), lo%ny)
  end function ik_idx_accelx

  elemental function it_idx_accel (lo, i)
    implicit none
    integer :: it_idx_accel
    type (accel_layout_type), intent (in) :: lo
    integer, intent (in) :: i
    it_idx_accel = 1 + mod((i - lo%llim_world)/lo%ndky, lo%nx)
  end function it_idx_accel

  elemental function ik_idx_accel (lo, i)
    implicit none
    integer :: ik_idx_accel
    type (accel_layout_type), intent (in) :: lo
    integer, intent (in) :: i
    ik_idx_accel = 1 + mod(i - lo%llim_world, lo%ndky)
  end function ik_idx_accel

  elemental function dealiasing (lo, ia)
    implicit none
    logical dealiasing
    type (accel_layout_type), intent (in) :: lo
    integer, intent (in) :: ia
    
    dealiasing = .true.

    if (it_idx(accel_lo, ia) > lo%nakx/2+1 &
  .and. it_idx(accel_lo, ia) <= lo%nx-lo%nakx/2) return
    if (ik_idx(accel_lo, ia) > lo%naky) return

    dealiasing = .false.
  end function dealiasing


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Transformation subroutines
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


# ifdef USE_C_INDEX
  subroutine gidx2lzidx (ig, isign, g_lo, iglo, lz_lo, ntgrid, il, ilz)
# else
  pure subroutine gidx2lzidx (ig, isign, g_lo, iglo, lz_lo, ntgrid, il, ilz)
# endif

    implicit none
    integer, intent (in) :: ig, isign
    type (g_layout_type), intent (in) :: g_lo
    integer, intent (in) :: iglo
    type (lz_layout_type), intent (in) :: lz_lo
    integer, intent (in) :: ntgrid
    integer, intent (out) :: il, ilz

    il = il_idx(g_lo,iglo)

    if (isign == 2) then
       il = 2*g_lo%nlambda+1 - il
    end if

    ilz = idx(lz_lo, ig, ik_idx(g_lo,iglo), it_idx(g_lo,iglo), &
         ie_idx(g_lo,iglo), is_idx(g_lo,iglo))
  end subroutine gidx2lzidx

# ifdef USE_C_INDEX
  subroutine gidx2xxfidx (ig, isign, iglo, g_lo, xxf_lo, it, ixxf)
# else
  elemental subroutine gidx2xxfidx (ig, isign, iglo, g_lo, xxf_lo, it, ixxf)
# endif
    implicit none
    integer, intent (in) :: ig, isign, iglo
    type (g_layout_type), intent (in) :: g_lo
    type (xxf_layout_type), intent (in) :: xxf_lo
    integer, intent (out) :: it, ixxf

    it = it_idx(g_lo,iglo)
    if (it > (xxf_lo%nakx+1)/2) then
       it = it - xxf_lo%nakx + xxf_lo%nx
    end if

    ixxf = idx(xxf_lo, ig, isign, ik_idx(g_lo,iglo), il_idx(g_lo,iglo), &
         ie_idx(g_lo,iglo), is_idx(g_lo,iglo))
  end subroutine gidx2xxfidx

# ifdef USE_C_INDEX
  subroutine xxfidx2gidx (it, ixxf, xxf_lo, g_lo, ig, isign, iglo)
# else
  elemental subroutine xxfidx2gidx (it, ixxf, xxf_lo, g_lo, ig, isign, iglo)
# endif
    implicit none
    integer, intent (in) :: it, ixxf
    type (xxf_layout_type), intent (in) :: xxf_lo
    type (g_layout_type), intent (in) :: g_lo
    integer, intent (out) :: ig, isign, iglo
    integer :: it0

    it0 = it
    if (it0 > (xxf_lo%nakx+1)/2) then
       it0 = it0 + xxf_lo%nakx - xxf_lo%nx
       if (it0 <= (xxf_lo%nakx+1)/2) then
          ig = -999999
          isign = -999999
          iglo = -999999
          return
       end if
    end if

    ig = ig_idx(xxf_lo,ixxf)
    isign = isign_idx(xxf_lo,ixxf)
    iglo = idx(g_lo, ik_idx(xxf_lo,ixxf), it0, il_idx(xxf_lo,ixxf), &
         ie_idx(xxf_lo,ixxf), is_idx(xxf_lo,ixxf))
  end subroutine xxfidx2gidx

  elemental subroutine xxfidx2yxfidx (it, ixxf, xxf_lo, yxf_lo, ik, iyxf)
    implicit none
    integer, intent (in) :: it, ixxf
    type (xxf_layout_type), intent (in) :: xxf_lo
    type (yxf_layout_type), intent (in) :: yxf_lo
    integer, intent (out) :: ik, iyxf

    ik = ik_idx(xxf_lo,ixxf)
    iyxf = idx(yxf_lo, ig_idx(xxf_lo,ixxf), isign_idx(xxf_lo,ixxf), &
         it, il_idx(xxf_lo,ixxf), ie_idx(xxf_lo,ixxf), is_idx(xxf_lo,ixxf))
  end subroutine xxfidx2yxfidx

  elemental subroutine yxfidx2xxfidx (ik, iyxf, yxf_lo, xxf_lo, it, ixxf)
    implicit none
    integer, intent (in) :: ik, iyxf
    type (yxf_layout_type), intent (in) :: yxf_lo
    type (xxf_layout_type), intent (in) :: xxf_lo
    integer, intent (out) :: it, ixxf
    integer :: ik0

    ik0 = ik
    if (ik0 > xxf_lo%naky) then
       it = -999999
       ixxf = -999999
       return
    end if

    it = it_idx(yxf_lo,iyxf)
    ixxf = idx(xxf_lo, ig_idx(yxf_lo,iyxf), isign_idx(yxf_lo,iyxf), &
         ik0, il_idx(yxf_lo,iyxf), ie_idx(yxf_lo,iyxf), is_idx(yxf_lo,iyxf))
  end subroutine yxfidx2xxfidx

  subroutine pe_layout (char)

    character (1), intent (out) :: char

    select case (layout)
    case ('yxels')
       char = 'v'
    case ('yxles')
       char = 'v'
    case ('lexys')
       char = 'x'
    case ('lxyes')
       char = 'm'    ! mixed
    case ('lyxes')
       char = 'b'    ! big, since this layout makes sense mainly for big runs?
    end select

  end subroutine pe_layout

  subroutine factors (n, j, div)
    ! <doc>
    !  returns factors (div) and the number of factors (j) of n
    ! </doc>
    integer, intent (in) :: n
    integer, intent (out) :: j
    integer, dimension (:), intent (out) :: div
    integer :: i, imax

    do i=2,n
       if (mod(n,i)==0) exit
    end do
    imax = n/i
    j=1
    do i=1,imax
       if (mod(n,i)==0) then
          div(j) = i
          j=j+1
       end if
    end do
    div(j) = n
  end subroutine factors

end module agk_layouts
