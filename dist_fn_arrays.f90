module dist_fn_arrays

  implicit none
  public 

  ! dist fn
  complex, dimension (:,:,:), allocatable :: g, gnew
  ! (-ntgrid:ntgrid,2, -g-layout-)

  complex, dimension (:,:,:), allocatable :: g_eq
  ! (-ntgrid:ntgrid,2, -g-layout-)

  real, dimension(:), allocatable :: kx_shift
  ! (naky)

  real, dimension (:,:), allocatable :: vpa, vpac
  ! (2, -g-layout-)

  real, dimension (:), allocatable :: vperp2
  ! (-g-layout-)

  real, dimension (:,:), allocatable :: vpar
  ! (2, -g-layout-)

  real, dimension (:), allocatable :: aj0, aj1vp2, aj1
  ! (-g-layout-)

  ! fieldeq
  complex, dimension (:,:,:), allocatable :: apar_ext
  ! (-ntgrid:ntgrid,nakx,naky) replicated

  ! collisional diagnostic of heating rate
  complex, dimension (:,:,:,:,:), allocatable :: c_rate
  ! (-ntgrid:ntgrid,nakx,naky,nspecies,2) replicated

  ! Gabe's p
  real, dimension (:), allocatable :: gp

  complex, dimension (:,:,:,:), allocatable :: &
       & dens_eq, ux_eq, uy_eq, uz_eq, &
       & pxx_eq, pyy_eq, pzz_eq, pxy_eq, pyz_eq, pzx_eq

  logical, save :: eq_set = .false.

end module dist_fn_arrays
