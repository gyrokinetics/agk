# include "define.inc"

module agk_equil_io
  ! <doc>
  !  This module saves/restores equilibrium informations
  !  using HDF5 parallel io (and MPI-I/O) or NETCDF with parallel I/O support.
  ! </doc>

# if SAVEEQ == _HDF_
# if FCOMPILER == _GFORTRAN_
  use hdf5
# else
  use hdf5, only: HID_T, HSIZE_T
# endif
# elif SAVEEQ == _NETCDF_
  use netcdf, only: NF90_NOERR, NF90_NETCDF4, NF90_NOWRITE, NF90_MPIIO, NF90_COLLECTIVE
  use netcdf, only: nf90_var_par_access
  use netcdf, only: nf90_create, nf90_open, nf90_close
  use netcdf, only: nf90_inq_dimid, nf90_inq_varid
  use netcdf, only: nf90_inquire_dimension
  use netcdf, only: nf90_get_var, nf90_put_var
  use netcdf, only: nf90_def_dim, nf90_def_var, nf90_enddef
  use netcdf_utils, only: get_netcdf_code_precision
  use netcdf_utils, only: netcdf_real, kind_nf
  use netcdf_utils, only: netcdf_error
# endif

  implicit none

  public :: agk_save_eq, agk_restore_eq

  private

  character (len=300) :: equilibrium_file
  logical :: initialized = .false.
  logical :: debug = .false.

# if SAVEEQ == _HDF_
  integer (HID_T) :: dist_fspace_asis, dist_mspace_asis
  integer, parameter :: rank=4
  integer (HSIZE_T):: count_asis(rank)
# elif SAVEEQ == _NETCDF_
  integer (kind_nf) :: nceqid
  integer (kind_nf) :: riid, specid, thetaid, signid, gloid, kyid, kxid
  integer (kind_nf) :: geq_id
  integer (kind_nf) :: phi_eq_id, apar_eq_id, bpar_eq_id
  integer (kind_nf) :: dens_eq_id
  integer (kind_nf) :: ux_eq_id,uy_eq_id,uz_eq_id
  integer (kind_nf) :: pxx_eq_id,pyy_eq_id,pzz_eq_id
  integer (kind_nf) :: pxy_eq_id,pyz_eq_id,pzx_eq_id
# endif

contains

  subroutine agk_save_eq (g_eq, use_Phi, use_Apar, use_Bpar)
    use file_utils, only: error_unit, stdout_unit
    use mp, only: proc0
    use agk_layouts, only: g_lo
    use theta_grid, only: ntgrid
    use mp, only: mp_comm, mp_info
# if SAVEEQ == _HDF_
# if FCOMPILER != _GFORTRAN_
    use hdf5, only: HID_T, H5P_FILE_ACCESS_F, H5F_ACC_TRUNC_F
    use hdf5, only: h5fcreate_f, h5fclose_f
    use hdf5, only: h5pcreate_f, h5pset_fapl_mpio_f, h5pclose_f
    use hdf5, only: h5gcreate_f, h5gclose_f
# endif
    use hdf_wrapper, only: hdf_error, hdf_write
    use agk_mem, only: alloc8, dealloc8
    use kgrids, only: nakx, naky
    use theta_grid, only: ntheta
    use species, only: nspec
    use le_grids, only: nlambda, negrid
    use fields_arrays, only: phi_eq, apar_eq, bpar_eq
    use dist_fn_arrays, only: dens_eq, ux_eq, uy_eq, uz_eq
    use dist_fn_arrays, only: pxx_eq, pyy_eq, pzz_eq, pxy_eq, pyz_eq, pzx_eq
# elif SAVEEQ == _NETCDF_
    use constants, only: kind_rs, kind_rd, pi
    use fields_arrays, only: phi_eq, apar_eq, bpar_eq
    use dist_fn_arrays, only: dens_eq, ux_eq, uy_eq, uz_eq
    use dist_fn_arrays, only: pxx_eq, pyy_eq, pzz_eq, pxy_eq, pyz_eq, pzx_eq
    use kgrids, only: naky, nakx
    use species, only: nspec
    use agk_mem, only: alloc8, dealloc8
    use convert, only: c2r
# endif

    implicit none
    complex, intent (in) :: g_eq(-ntgrid:,:,g_lo%llim_proc:)
    logical, intent (in) :: use_Phi, use_Apar, use_Bpar
# if SAVEEQ == _HDF_
    integer :: istatus
    integer (HID_T) :: file_id, plist_id
    integer (HID_T) :: vgroup_id, vdgroup_id, vfgroup_id, vmgroup_id
    integer (HID_T) :: pgroup_id, pngroup_id

    if (debug.and.proc0) write(stdout_unit,*) 'Start agk_save_eq'

    call init_save_eq

    ! Setup file access property list with parallel I/O access
    !!! create a property list (plist_id) of a class class for file access.
    call h5pcreate_f(H5P_FILE_ACCESS_F, plist_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5pcreate_f error')
    !!! store mp_comm information in the file access property list.
    call h5pset_fapl_mpio_f(plist_id, mp_comm, mp_info, istatus)
    if (istatus /= 0) call hdf_error (message='h5pset_fapl_mpio_f error')

    ! Create the file collectively (must be called by all processors)
    !!! H5F_ACC_TRUNC_F truncates file.
    call h5fcreate_f(equilibrium_file, H5F_ACC_TRUNC_F, file_id, istatus, access_prp = plist_id)
    if (istatus /= 0) call hdf_error (message='h5fcreate_f error')

    call h5pclose_f(plist_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5pclose_f error')

    ! Create groups : create hierarchical data structure
    !
    ! / -+- Parameters --- Number_of_Grids-+- nakx
    !    |                                 +- naky
    !    |                                ...
    !    +- Variables -+- Dist_Func --- g
    !                  +- Fiels     -+- phi
    !                  |             +- Apar
    !                  |             +- Bpar
    !                  +- Moments   -+- dens
    !                                +- ux
    !                               ...
    !
    call h5gcreate_f(file_id, '/Parameters', pgroup_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5gcreate_f error')
    call h5gcreate_f(file_id, '/Parameters/Number_of_Grids', pngroup_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5gcreate_f error')
    call h5gcreate_f(file_id, '/Variables', vgroup_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5gcreate_f error')
    call h5gcreate_f(file_id, '/Variables/Dist_Func', vdgroup_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5gcreate_f error')
    call h5gcreate_f(file_id, '/Variables/Fields', vfgroup_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5gcreate_f error')
    call h5gcreate_f(file_id, '/Variables/Moments', vmgroup_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5gcreate_f error')

    ! Write number of grids
    call hdf_write (pngroup_id, 'nakx', nakx, istatus)
    if (istatus /= 0) call hdf_error (message='hdf_write error')
    call hdf_write (pngroup_id, 'naky', naky, istatus)
    if (istatus /= 0) call hdf_error (message='hdf_write error')
    call hdf_write (pngroup_id, 'ntheta', ntheta, istatus)
    if (istatus /= 0) call hdf_error (message='hdf_write error')
    call hdf_write (pngroup_id, 'nspec', nspec, istatus)
    if (istatus /= 0) call hdf_error (message='hdf_write error')
    call hdf_write (pngroup_id, 'nlambda', nlambda, istatus)
    if (istatus /= 0) call hdf_error (message='hdf_write error')
    call hdf_write (pngroup_id, 'negrid', negrid, istatus)
    if (istatus /= 0) call hdf_error (message='hdf_write error')

    ! Write fields
    if (use_Phi) then
       call hdf_write (vfgroup_id, 'phi_eq', phi_eq, istatus)
       if (istatus /= 0) call hdf_error (message='hdf_write error')
    end if
    if (use_Apar) then
       call hdf_write (vfgroup_id, 'apar_eq', apar_eq, istatus)
       if (istatus /= 0) call hdf_error (message='hdf_write error')
    end if
    if (use_Bpar) then
       call hdf_write (vfgroup_id, 'bpar_eq', bpar_eq, istatus)
       if (istatus /= 0) call hdf_error (message='hdf_write error')
    end if

    ! Write moments
    call hdf_write(vmgroup_id, 'dens_eq', dens_eq, istatus)
    if (istatus /= 0) call hdf_error (message='hdf_write error')    
    call hdf_write(vmgroup_id, 'ux_eq', ux_eq, istatus)
    if (istatus /= 0) call hdf_error (message='hdf_write error')    
    call hdf_write(vmgroup_id, 'uy_eq', uy_eq, istatus)
    if (istatus /= 0) call hdf_error (message='hdf_write error')    
    call hdf_write(vmgroup_id, 'uz_eq', uz_eq, istatus)
    if (istatus /= 0) call hdf_error (message='hdf_write error')    
    call hdf_write(vmgroup_id, 'pxx_eq', pxx_eq, istatus)
    if (istatus /= 0) call hdf_error (message='hdf_write error')    
    call hdf_write(vmgroup_id, 'pyy_eq', pyy_eq, istatus)
    if (istatus /= 0) call hdf_error (message='hdf_write error')    
    call hdf_write(vmgroup_id, 'pzz_eq', pzz_eq, istatus)
    if (istatus /= 0) call hdf_error (message='hdf_write error')    
    call hdf_write(vmgroup_id, 'pxy_eq', pxy_eq, istatus)
    if (istatus /= 0) call hdf_error (message='hdf_write error')    
    call hdf_write(vmgroup_id, 'pyz_eq', pyz_eq, istatus)
    if (istatus /= 0) call hdf_error (message='hdf_write error')    
    call hdf_write(vmgroup_id, 'pzx_eq', pzx_eq, istatus)
    if (istatus /= 0) call hdf_error (message='hdf_write error')    

    ! Write dist. func.
    call hdf_write_g_asis(vdgroup_id, 'g_eq', g_eq, istatus)
    if (istatus /= 0) call hdf_error (message='hdf_write_g_asis error')

    call h5gclose_f(pgroup_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5gclose_f error')
    call h5gclose_f(pngroup_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5gclose_f error')
    call h5gclose_f(vgroup_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5gclose_f error')
    call h5gclose_f(vdgroup_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5gclose_f error')
    call h5gclose_f(vfgroup_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5gclose_f error')
    call h5gclose_f(vmgroup_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5gclose_f error')
    call h5fclose_f(file_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5fclose_f error')

    call finish_save_eq

    if (debug.and.proc0) write(stdout_unit,*) 'End agk_save_eq'

# elif SAVEEQ == _NETCDF_

    integer :: istatus
    character (300) :: file_eq_proc
    integer :: n_elements, file_elements
    integer :: start(4), count(4)
    real, allocatable :: ftmp(:,:,:,:), gtmp(:,:,:,:), ftmp2(:,:,:,:,:)

    if (debug.and.proc0) write(stdout_unit,*) 'Start agk_save_eq'

    call init_save_eq

    n_elements = max(g_lo%ulim_proc-g_lo%llim_proc+1,0)
    file_elements=g_lo%ulim_world+1
    start = (/ 1,1,1,g_lo%llim_proc+1 /)
    count = (/ 2,2*ntgrid+1,2,n_elements /)

    file_eq_proc = trim(equilibrium_file)
    
    ! open file
    istatus = nf90_create (file_eq_proc, ior(NF90_NETCDF4, NF90_MPIIO), nceqid, comm=mp_comm, info=mp_info)
    if (istatus /= NF90_NOERR) then
       call netcdf_error (istatus, message="create eqfile")
       goto 1
    end if
    if (debug.and.proc0) write (stdout_unit,*) &
         & 'created netcdf file for eqfile', trim(file_eq_proc), &
         & ' with nceqid: ', nceqid, ' in agk_save_eq'

    ! write dimensions for fields and moments
    istatus = nf90_def_dim (nceqid, "ri", 2, riid)
    if (istatus /= NF90_NOERR) then
       call netcdf_error (istatus, dim="riid")
       goto 1
    end if

    istatus = nf90_def_dim (nceqid, "theta", 2*ntgrid+1, thetaid)
    if (istatus /= NF90_NOERR) then
       call netcdf_error (istatus, dim="thetaid")
       goto 1
    end if

    istatus = nf90_def_dim (nceqid, "aky", naky, kyid)
    if (istatus /= NF90_NOERR) then
       call netcdf_error (istatus, dim="kyid")
       goto 1
    end if

    istatus = nf90_def_dim (nceqid, "akx", nakx, kxid)
    if (istatus /= NF90_NOERR) then
       call netcdf_error (istatus, dim="kxid")
       goto 1
    end if

    istatus = nf90_def_dim (nceqid, "species", nspec, specid)
    if (istatus /= NF90_NOERR) then
       call netcdf_error (istatus, dim="specid")
       goto 1
    end if

    ! write dimensions for g
    if (n_elements > 0) then
       istatus = nf90_def_dim (nceqid, "sign", 2, signid)
       if (istatus /= NF90_NOERR) then
          call netcdf_error (istatus, dim="signid")
          goto 1
       end if

       istatus = nf90_def_dim (nceqid, "glo", file_elements, gloid)
       if (istatus /= NF90_NOERR) then
          call netcdf_error (istatus, dim="gloid")
          goto 1
       end if
    end if

    if (netcdf_real == 0) netcdf_real = get_netcdf_code_precision()

    ! define fields and moments
    if (use_Phi) then
       istatus = nf90_def_var (nceqid, "phi_eq", netcdf_real, &
            (/ riid, thetaid, kxid, kyid /), phi_eq_id)
       if (istatus /= NF90_NOERR) then
          call netcdf_error (istatus, var="phi_eq")
          goto 1
       end if
    end if

    if (use_Apar) then
       istatus = nf90_def_var (nceqid, "apar_eq", netcdf_real, &
            (/ riid, thetaid, kxid, kyid /), apar_eq_id)
       if (istatus /= NF90_NOERR) then
          call netcdf_error (istatus, var="apar_eq")
          goto 1
       end if
    end if

    if (use_Bpar) then
       istatus = nf90_def_var (nceqid, "bpar_eq", netcdf_real, &
            (/ riid, thetaid, kxid, kyid /), bpar_eq_id)
       if (istatus /= NF90_NOERR) then
          call netcdf_error (istatus, var="bpar_eq")
          goto 1
       end if
    end if

    ! moments
    istatus = nf90_def_var (nceqid, "dens_eq", netcdf_real, &
         (/ riid, thetaid, kxid, kyid, specid /), dens_eq_id)
    if (istatus /= NF90_NOERR) then
       call netcdf_error (istatus, var="dens_eq")
       goto 1
    end if

    istatus = nf90_def_var (nceqid, "ux_eq", netcdf_real, &
         (/ riid, thetaid, kxid, kyid, specid /), ux_eq_id)
    if (istatus /= NF90_NOERR) then
       call netcdf_error (istatus, var="ux_eq")
       goto 1
    end if
    istatus = nf90_def_var (nceqid, "uy_eq", netcdf_real, &
         (/ riid, thetaid, kxid, kyid, specid /), uy_eq_id)
    if (istatus /= NF90_NOERR) then
       call netcdf_error (istatus, var="uy_eq")
       goto 1
    end if
    istatus = nf90_def_var (nceqid, "uz_eq", netcdf_real, &
         (/ riid, thetaid, kxid, kyid, specid /), uz_eq_id)
    if (istatus /= NF90_NOERR) then
       call netcdf_error (istatus, var="uz_eq")
       goto 1
    end if

    istatus = nf90_def_var (nceqid, "pxx_eq", netcdf_real, &
         (/ riid, thetaid, kxid, kyid, specid /), pxx_eq_id)
    if (istatus /= NF90_NOERR) then
       call netcdf_error (istatus, var="pxx_eq")
       goto 1
    end if
    istatus = nf90_def_var (nceqid, "pyy_eq", netcdf_real, &
         (/ riid, thetaid, kxid, kyid, specid /), pyy_eq_id)
    if (istatus /= NF90_NOERR) then
       call netcdf_error (istatus, var="pyy_eq")
       goto 1
    end if
    istatus = nf90_def_var (nceqid, "pzz_eq", netcdf_real, &
         (/ riid, thetaid, kxid, kyid, specid /), pzz_eq_id)
    if (istatus /= NF90_NOERR) then
       call netcdf_error (istatus, var="pzz_eq")
       goto 1
    end if
    istatus = nf90_def_var (nceqid, "pxy_eq", netcdf_real, &
         (/ riid, thetaid, kxid, kyid, specid /), pxy_eq_id)
    if (istatus /= NF90_NOERR) then
       call netcdf_error (istatus, var="pxy_eq")
       goto 1
    end if
    istatus = nf90_def_var (nceqid, "pyz_eq", netcdf_real, &
         (/ riid, thetaid, kxid, kyid, specid /), pyz_eq_id)
    if (istatus /= NF90_NOERR) then
       call netcdf_error (istatus, var="pyz_eq")
       goto 1
    end if
    istatus = nf90_def_var (nceqid, "pzx_eq", netcdf_real, &
         (/ riid, thetaid, kxid, kyid, specid /), pzx_eq_id)
    if (istatus /= NF90_NOERR) then
       call netcdf_error (istatus, var="pzx_eq")
       goto 1
    end if
 
    ! define g
    if (n_elements > 0) then
       istatus = nf90_def_var (nceqid, "geq", netcdf_real, &
            (/ riid, thetaid, signid, gloid /), geq_id)
       if (istatus /= NF90_NOERR) then
          call netcdf_error (istatus, var="geq")
          goto 1
       end if
    end if

    ! end definition for fields and moments
    istatus = nf90_enddef (nceqid)
    if (istatus /= NF90_NOERR) then
       call netcdf_error (istatus, nceqid, message="enddef eqfile")
       goto 1
    end if

1   continue
    
    ! if error occured, close file for fields and moments and return
    if (istatus /= NF90_NOERR) then
       write (error_unit(),*) &
            & 'close file for eqfile and return from save_eq_for_restart'
       istatus = nf90_close (nceqid)
       if (istatus /= NF90_NOERR) &
            & call netcdf_error (istatus, message="close eqfile")
       return
    end if

    ! write fields and moments
    allocate (ftmp (2, 2*ntgrid+1, nakx, naky)) ; call alloc8 (r4=ftmp, v="ftmp")

    if (use_Phi) then
       call c2r(phi_eq, ftmp)
       istatus = nf90_put_var (nceqid, phi_eq_id, ftmp)
       if (istatus /= NF90_NOERR) &
            & call netcdf_error (istatus, nceqid, varid=phi_eq_id)
    end if
    
    if (use_Apar) then
       call c2r(apar_eq, ftmp)
       istatus = nf90_put_var (nceqid, apar_eq_id, ftmp)
       if (istatus /= NF90_NOERR) &
            & call netcdf_error (istatus, nceqid, varid=apar_eq_id)
    end if
    
    if (use_Bpar) then
       call c2r(bpar_eq, ftmp)
       istatus = nf90_put_var (nceqid, bpar_eq_id, ftmp)
       if (istatus /= NF90_NOERR) &
            & call netcdf_error (istatus, nceqid, varid=bpar_eq_id)
    end if
    
    call dealloc8 (r4=ftmp, v="ftmp") 
    
    allocate (ftmp2 (2, 2*ntgrid+1, nakx, naky, nspec)) ; call alloc8 (r5=ftmp2, v="ftmp2")
    
    call c2r(dens_eq, ftmp2)
    istatus = nf90_put_var (nceqid, dens_eq_id, ftmp2)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=dens_eq_id)
    
    call c2r(ux_eq, ftmp2)
    istatus = nf90_put_var (nceqid, ux_eq_id, ftmp2)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=ux_eq_id)
    call c2r(uy_eq, ftmp2)
    istatus = nf90_put_var (nceqid, uy_eq_id, ftmp2)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=uy_eq_id)
    call c2r(uz_eq, ftmp2)
    istatus = nf90_put_var (nceqid, uz_eq_id, ftmp2)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=uz_eq_id)
    
    call c2r(pxx_eq, ftmp2)
    istatus = nf90_put_var (nceqid, pxx_eq_id, ftmp2)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=pxx_eq_id)
    call c2r(pyy_eq, ftmp2)
    istatus = nf90_put_var (nceqid, pyy_eq_id, ftmp2)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=pyy_eq_id)
    call c2r(pzz_eq, ftmp2)
    istatus = nf90_put_var (nceqid, pzz_eq_id, ftmp2)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=pzz_eq_id)
    call c2r(pxy_eq, ftmp2)
    istatus = nf90_put_var (nceqid, pxy_eq_id, ftmp2)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=pxy_eq_id)
    call c2r(pyz_eq, ftmp2)
    istatus = nf90_put_var (nceqid, pyz_eq_id, ftmp2)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=pyz_eq_id)
    call c2r(pzx_eq, ftmp2)
    istatus = nf90_put_var (nceqid, pzx_eq_id, ftmp2)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=pzx_eq_id)
    
    call dealloc8 (r5=ftmp2, v="ftmp2") 
    
    ! write g
    if (n_elements > 0) then

       allocate (gtmp(2, 2*ntgrid+1, 2, g_lo%llim_proc:g_lo%ulim_alloc)) ; call alloc8 (r4=gtmp, v="gtmp")

       call c2r(g_eq, gtmp)
       istatus = nf90_var_par_access(nceqid, geq_id, NF90_COLLECTIVE)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, nceqid, geq_id)
       istatus = nf90_put_var (nceqid, geq_id, gtmp, start=start, count=count)
       if (istatus /= NF90_NOERR) &
            & call netcdf_error (istatus, nceqid, varid=geq_id)

       call dealloc8 (r4=gtmp, v="gtmp") 

    end if

    ! closing file
    istatus = nf90_close (nceqid)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, message="close eqfile")

    if (debug.and.proc0) write(stdout_unit,*) 'End agk_save_eq'

# else

    if (proc0) write (error_unit(),*) &
         'WARNING: agk_save_eq is called without hdf5 or netcdf library'

# endif

  end subroutine agk_save_eq

  subroutine agk_restore_eq (g_eq, use_Phi, use_Apar, use_Bpar)
    use file_utils, only: error_unit, stdout_unit
    use mp, only: proc0
    use agk_layouts, only: g_lo
    use theta_grid, only: ntgrid
    use mp, only: mp_comm, mp_info
# if SAVEEQ == _HDF_
# if FCOMPILER != _GFORTRAN_
    use hdf5, only: HID_T, H5P_FILE_ACCESS_F, H5F_ACC_RDONLY_F
    use hdf5, only: h5pcreate_f, h5pset_fapl_mpio_f, h5pclose_f
    use hdf5, only: h5fopen_f, h5fclose_f
    use hdf5, only: h5gopen_f, h5gclose_f
# endif
    use hdf_wrapper, only: hdf_error, hdf_read
    use agk_mem, only: alloc8, dealloc8
    use kgrids, only: nakx, naky
    use theta_grid, only: ntheta
    use species, only: nspec
    use le_grids, only: nlambda, negrid
    use fields_arrays, only: phi_eq, apar_eq, bpar_eq
    use dist_fn_arrays, only: dens_eq, ux_eq, uy_eq, uz_eq
    use dist_fn_arrays, only: pxx_eq, pyy_eq, pzz_eq, pxy_eq, pyz_eq, pzx_eq
    use dist_fn_arrays, only: eq_set
# elif SAVEEQ == _NETCDF_
    use mp, only: proc0, iproc
    use fields_arrays, only: phi_eq, apar_eq, bpar_eq
    use dist_fn_arrays, only: dens_eq, ux_eq, uy_eq, uz_eq
    use dist_fn_arrays, only: pxx_eq, pyy_eq, pzz_eq, pxy_eq, pyz_eq, pzx_eq
    use dist_fn_arrays, only: eq_set
    use kgrids, only: naky, nakx
    use species, only: nspec
    use agk_mem, only: alloc8, dealloc8
    use convert, only: r2c
# endif

    implicit none
    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (out) :: g_eq
    logical, intent (in) :: use_Phi, use_Apar, use_Bpar
# if SAVEEQ == _HDF_
    integer :: istatus
    integer (HID_T) :: file_id, plist_id
    integer (HID_T) :: vgroup_id, vdgroup_id, vfgroup_id, vmgroup_id
    integer (HID_T) :: pgroup_id, pngroup_id

    integer :: n

    if (debug.and.proc0) write(stdout_unit,*) 'Start agk_restore_eq'

    call init_save_eq

    ! Setup file access property list with parallel I/O access
    !!! create a property list (plist_id) of a class class for file access.
    call h5pcreate_f(H5P_FILE_ACCESS_F, plist_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5pcreate_f error')
    !!! store mp_comm information in the file access property list.
    call h5pset_fapl_mpio_f(plist_id, mp_comm, mp_info, istatus)
    if (istatus /= 0) call hdf_error (message='h5pset_fapl_mpio_f error')

    ! Open the file collectively (must be called by all processors)
    !!! H5F_ACC_TRUNC_F truncates file.
    call h5fopen_f(equilibrium_file, H5F_ACC_RDONLY_F, file_id, istatus, access_prp = plist_id)
    if (istatus /= 0) call hdf_error (message='h5fcreate_f error')

    call h5pclose_f(plist_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5pclose_f error')

    ! Open groups
    call h5gopen_f(file_id, '/Parameters', pgroup_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5gopen_f error')
    call h5gopen_f(file_id, '/Parameters/Number_of_Grids', pngroup_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5gopen_f error')
    call h5gopen_f(file_id, '/Variables', vgroup_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5gopen_f error')
    call h5gopen_f(file_id, '/Variables/Dist_Func', vdgroup_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5gopen_f error')
    call h5gopen_f(file_id, '/Variables/Fields', vfgroup_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5gopen_f error')
    call h5gopen_f(file_id, '/Variables/Moments', vmgroup_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5gopen_f error')

    ! Check number of grids
    call hdf_read(pngroup_id, 'nakx', n)
    if ( n /= nakx ) write(error_unit(),*) 'WARNING: mismatch in nakx', n, nakx
    call hdf_read(pngroup_id, 'naky', n)
    if ( n /= naky ) write(error_unit(),*) 'WARNING: mismatch in naky', n, naky
    call hdf_read(pngroup_id, 'ntheta', n)
    if ( n /= ntheta ) write(error_unit(),*) 'WARNING: mismatch in ntheta', n, ntheta
    call hdf_read(pngroup_id, 'nspec', n)
    if ( n /= nspec ) write(error_unit(),*) 'WARNING: mismatch in nspec', n, nspec
    call hdf_read(pngroup_id, 'nlambda', n)
    if ( n /= nlambda ) write(error_unit(),*) 'WARNING: mismatch in nlambda', n, nlambda
    call hdf_read(pngroup_id, 'negrid', n)
    if ( n /= negrid ) write(error_unit(),*) 'WARNING: mismatch in negrid', n, negrid

    ! Read fields
    if (use_Phi) call hdf_read(vfgroup_id, 'phi_eq', phi_eq)
    if (use_Apar) call hdf_read(vfgroup_id, 'apar_eq', apar_eq)
    if (use_Bpar) call hdf_read(vfgroup_id, 'bpar_eq', bpar_eq)

    ! Read moments
    call hdf_read(vmgroup_id, 'dens_eq', dens_eq)
    call hdf_read(vmgroup_id, 'ux_eq', ux_eq)
    call hdf_read(vmgroup_id, 'uy_eq', uy_eq)
    call hdf_read(vmgroup_id, 'uz_eq', uz_eq)
    call hdf_read(vmgroup_id, 'pxx_eq', pxx_eq)
    call hdf_read(vmgroup_id, 'pyy_eq', pyy_eq)
    call hdf_read(vmgroup_id, 'pzz_eq', pzz_eq)
    call hdf_read(vmgroup_id, 'pxy_eq', pxy_eq)
    call hdf_read(vmgroup_id, 'pyz_eq', pyz_eq)
    call hdf_read(vmgroup_id, 'pzx_eq', pzx_eq)

    ! Read dist. func.
    call hdf_read_g_asis (vdgroup_id, 'g_eq', g_eq, istatus)
    if (istatus /= 0) call hdf_error (message='hdf_read_g_asis error')

    ! Close everything
    call h5gclose_f(pgroup_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5gclose_f error')
    call h5gclose_f(pngroup_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5gclose_f error')
    call h5gclose_f(vgroup_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5gclose_f error')
    call h5gclose_f(vdgroup_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5gclose_f error')
    call h5gclose_f(vfgroup_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5gclose_f error')
    call h5gclose_f(vmgroup_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5gclose_f error')
    call h5fclose_f(file_id, istatus)
    if (istatus /= 0) call hdf_error (message='h5fclose_f error')

    call finish_save_eq

    eq_set = .true.

    if (debug.and.proc0) write(stdout_unit,*) 'End agk_restore_eq'

# elif SAVEEQ == _NETCDF_
    
    integer :: istatus
    character (300) :: file_eq_proc
    integer :: n_elements
    integer :: file_elements
    integer :: i
    integer :: start(4), count(4)
    real, allocatable :: ftmp(:,:,:,:), gtmp(:,:,:,:), ftmp2(:,:,:,:,:)

    if (debug.and.proc0) write(stdout_unit,*) 'Start agk_restore_eq'

    call init_save_eq

    n_elements = max(g_lo%ulim_proc-g_lo%llim_proc+1,0)
    file_elements=g_lo%ulim_world+1
    start = (/ 1,1,1,g_lo%llim_proc+1 /)
    count = (/ 2,2*ntgrid+1,2, n_elements /)

    file_eq_proc = trim(equilibrium_file)

    ! open file for fields and moments
    istatus = nf90_open (file_eq_proc, ior(NF90_NOWRITE,NF90_MPIIO), nceqid, comm=mp_comm, info=mp_info)
    if (istatus /= NF90_NOERR) &
         call netcdf_error (istatus, file=file_eq_proc, abort=.true.)
    if (debug.and.proc0) write (stdout_unit,*) &
         & 'opened netcdf file for eqfile', trim(file_eq_proc), &
         & ' with nceqid: ', nceqid, ' in agk_restore_eq'

!    if (netcdf_real == 0) netcdf_real = get_netcdf_code_precision()
!    call check_netcdf_file_precision (nceqid)

    ! inquire dimension id for fields and moments
    istatus = nf90_inq_dimid (nceqid, "ri", riid)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, dimid=riid)

    istatus = nf90_inq_dimid (nceqid, "theta", thetaid)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, dimid=thetaid)
    
    istatus = nf90_inq_dimid (nceqid, "aky", kyid)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, dimid=kyid)
    
    istatus = nf90_inq_dimid (nceqid, "akx", kxid)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, dimid=kxid)
    
    istatus = nf90_inq_dimid (nceqid, "species", specid)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, dimid=specid)

    ! inquire dimension id for g
    if (n_elements > 0) then
       istatus = nf90_inq_dimid (nceqid, "sign", signid)
       if (istatus /= NF90_NOERR) &
            & call netcdf_error (istatus, nceqid, dimid=signid)

       istatus = nf90_inq_dimid (nceqid, "glo", gloid)
       if (istatus /= NF90_NOERR) &
            & call netcdf_error (istatus, nceqid, dimid=gloid)
    end if

    ! inquire dimension size for fields and moments
    istatus = nf90_inquire_dimension (nceqid, riid, len=i)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, dim="riid")
    if (i /= 2) write(error_unit(),*) &
         & 'Restart error: ri=? ',i,' : ',2,' : ',iproc

    istatus = nf90_inquire_dimension (nceqid, thetaid, len=i)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, dim="thetaid")
    if (i /= 2*ntgrid + 1) write(error_unit(),*) &
         & 'Restart error: ntgrid=? ',i,' : ',ntgrid,' : ',iproc
    
    istatus = nf90_inquire_dimension (nceqid, kyid, len=i)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, dimid=kyid)
    if (i /= naky) write(error_unit(),*) &
         & 'Restart error: naky=? ',i,' : ',naky,' : ',iproc
    
    istatus = nf90_inquire_dimension (nceqid, kxid, len=i)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, dimid=kxid)
    if (i /= nakx) write(error_unit(),*) &
         & 'Restart error: nakx=? ',i,' : ',nakx,' : ',iproc
    
    istatus = nf90_inquire_dimension (nceqid, specid, len=i)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, dimid=specid)
    if (i /= nspec) write(error_unit(),*) &
         & 'Restart error: nspec=? ',i,' : ',nspec,' : ',iproc

    ! inquire dimension size for g
    if (n_elements > 0) then
       istatus = nf90_inquire_dimension (nceqid, riid, len=i)
       if (istatus /= NF90_NOERR) &
            & call netcdf_error (istatus, nceqid, dimid=riid)
       if (i /= 2) write(error_unit(),*) &
            & 'Restart error: ri=? ',i,' : ',2,' : ',iproc

       istatus = nf90_inquire_dimension (nceqid, thetaid, len=i)
       if (istatus /= NF90_NOERR) &
            & call netcdf_error (istatus, nceqid, dimid=thetaid)
       if (i /= 2*ntgrid + 1) write(error_unit(),*) &
            & 'Restart error: ntgrid=? ',i,' : ',ntgrid,' : ',iproc

       istatus = nf90_inquire_dimension (nceqid, signid, len=i)
       if (istatus /= NF90_NOERR) &
            & call netcdf_error (istatus, nceqid, dimid=signid)
       if (i /= 2) write(error_unit(),*) &
            & 'Restart error: sign=? ',i,' : ',iproc

       istatus = nf90_inquire_dimension (nceqid, gloid, len=i)
       if (istatus /= NF90_NOERR) &
            & call netcdf_error (istatus, nceqid, dimid=gloid)
       if (i /= file_elements) write(error_unit(),*) &
            & 'Restart error: glo=? ',i,' : ',iproc
    end if

    ! inquire variable id for fields and moments
    if (use_Phi) then
       istatus = nf90_inq_varid (nceqid, "phi_eq", phi_eq_id)
       if (istatus /= NF90_NOERR) &
            & call netcdf_error (istatus, nceqid, varid=phi_eq_id)
    end if

    if (use_Apar) then
       istatus = nf90_inq_varid (nceqid, "apar_eq", apar_eq_id)
       if (istatus /= NF90_NOERR) &
            & call netcdf_error (istatus, nceqid, varid=apar_eq_id)
    end if

    if (use_Bpar) then
       istatus = nf90_inq_varid (nceqid, "bpar_eq", bpar_eq_id)
       if (istatus /= NF90_NOERR) &
            & call netcdf_error (istatus, nceqid, varid=bpar_eq_id)
    end if
       
    ! moments
    istatus = nf90_inq_varid (nceqid, "dens_eq", dens_eq_id)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=dens_eq_id)
    
    istatus = nf90_inq_varid (nceqid, "ux_eq", ux_eq_id)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=ux_eq_id)
    istatus = nf90_inq_varid (nceqid, "uy_eq", uy_eq_id)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=uy_eq_id)
    istatus = nf90_inq_varid (nceqid, "uz_eq", uz_eq_id)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=uz_eq_id)
    
    istatus = nf90_inq_varid (nceqid, "pxx_eq", pxx_eq_id)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=pxx_eq_id)
    istatus = nf90_inq_varid (nceqid, "pyy_eq", pyy_eq_id)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=pyy_eq_id)
    istatus = nf90_inq_varid (nceqid, "pzz_eq", pzz_eq_id)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=pzz_eq_id)
    istatus = nf90_inq_varid (nceqid, "pxy_eq", pxy_eq_id)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=pxy_eq_id)
    istatus = nf90_inq_varid (nceqid, "pyz_eq", pyz_eq_id)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=pyz_eq_id)
    istatus = nf90_inq_varid (nceqid, "pzx_eq", pzx_eq_id)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=pzx_eq_id)

    ! inquire variable id for g
    if (n_elements > 0) then
       istatus = nf90_inq_varid (nceqid, "geq", geq_id)
       if (istatus /= NF90_NOERR) &
            & call netcdf_error (istatus, nceqid, varid=geq_id)
    end if

    allocate (ftmp (2, 2*ntgrid+1, nakx, naky)) ; call alloc8 (r4=ftmp, v="ftmp")

    if (use_Phi) then
       istatus = nf90_get_var (nceqid, phi_eq_id, ftmp)
       if (istatus /= NF90_NOERR) &
            & call netcdf_error (istatus, nceqid, varid=phi_eq_id)
       call r2c(phi_eq, ftmp)
    end if

    if (use_Apar) then
       istatus = nf90_get_var (nceqid, apar_eq_id, ftmp)
       if (istatus /= NF90_NOERR) &
            & call netcdf_error (istatus, nceqid, varid=apar_eq_id)
       call r2c(apar_eq, ftmp)
    end if

    if (use_Bpar) then
       istatus = nf90_get_var (nceqid, bpar_eq_id, ftmp)
       if (istatus /= NF90_NOERR) &
            & call netcdf_error (istatus, nceqid, varid=bpar_eq_id)
       call r2c(bpar_eq, ftmp)
    end if

    call dealloc8 (r4=ftmp, v="ftmp")

    allocate (ftmp2 (2, 2*ntgrid+1, nakx, naky, nspec)) ; call alloc8 (r5=ftmp2, v="ftmp2")
  
    istatus = nf90_get_var (nceqid, dens_eq_id, ftmp2)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=dens_eq_id)
    call r2c(dens_eq, ftmp2)

    istatus = nf90_get_var (nceqid, ux_eq_id, ftmp2)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=ux_eq_id)
    call r2c(ux_eq, ftmp2)
    istatus = nf90_get_var (nceqid, uy_eq_id, ftmp2)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=uy_eq_id)
    call r2c(uy_eq, ftmp2)
    istatus = nf90_get_var (nceqid, uz_eq_id, ftmp2)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=uz_eq_id)
    call r2c(uz_eq, ftmp2)
    
    istatus = nf90_get_var (nceqid, pxx_eq_id, ftmp2)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=pxx_eq_id)
    call r2c(pxx_eq, ftmp2)
    istatus = nf90_get_var (nceqid, pyy_eq_id, ftmp2)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=pyy_eq_id)
    call r2c(pyy_eq, ftmp2)
    istatus = nf90_get_var (nceqid, pzz_eq_id, ftmp2)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=pzz_eq_id)
    call r2c(pzz_eq, ftmp2)
    istatus = nf90_get_var (nceqid, pxy_eq_id, ftmp2)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=pxy_eq_id)
    call r2c(pxy_eq, ftmp2)
    istatus = nf90_get_var (nceqid, pyz_eq_id, ftmp2)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=pyz_eq_id)
    call r2c(pyz_eq, ftmp2)
    istatus = nf90_get_var (nceqid, pzx_eq_id, ftmp2)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, varid=pzx_eq_id)
    call r2c(pzx_eq, ftmp2)
    
    call dealloc8 (r5=ftmp2, v="ftmp2") 

    ! read variables for g
    if (n_elements > 0) then
       allocate (gtmp (2, 2*ntgrid+1, 2, g_lo%llim_proc:g_lo%ulim_alloc)) ; call alloc8 (r4=gtmp, v="gtmp")

       gtmp = 0.
       istatus = nf90_var_par_access(nceqid, geq_id, NF90_COLLECTIVE)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, nceqid, geq_id)
       istatus = nf90_get_var (nceqid, geq_id, gtmp, start=start, count=count)
       if (istatus /= NF90_NOERR) &
            & call netcdf_error (istatus, nceqid, varid=geq_id)
       call r2c(g_eq, gtmp)

       call dealloc8 (r4=gtmp, v="gtmp")
    end if

    ! close file for fields and moments
    istatus = nf90_close (nceqid)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, nceqid, message="close eqfile")

    eq_set = .true.

    if (debug.and.proc0) write(stdout_unit,*) 'End agk_restore_eq'

# else

    g_eq=0.
    if (proc0) write (error_unit(),*) &
         'WARNING: agk_restore_eq is called without hdf5 or netcdf library'

# endif
  end subroutine agk_restore_eq

  subroutine init_save_eq
    use file_utils, only: run_name, stdout_unit
    use mp, only: proc0
# if SAVEEQ == _HDF_
    use run_parameters, only: hdf5_stop, hdf5_dble
    use hdf_wrapper, only: hdf_init
# endif
    implicit none
# if SAVEEQ == _HDF_
    character (len=10) :: suffix = '.h5'
# elif SAVEEQ == _NETCDF_
    character (len=10) :: suffix = '.nc'
# endif

    if (debug.and.proc0) write(stdout_unit,*) 'Start init_save_eq'

    if (initialized) return

# if SAVEEQ == _HDF_ || SAVEEQ == _NETCDF_
    if (proc0) equilibrium_file = trim(run_name)//'.eq'//suffix
    call read_parameters
# endif

# if SAVEEQ == _HDF_
    call hdf_init (stop=hdf5_stop, dbl=hdf5_dble)
    call dist_hyperslab_asis
# endif

    initialized = .true.
    
    if (debug.and.proc0) write(stdout_unit,*) 'End init_save_eq'

  end subroutine init_save_eq

  subroutine finish_save_eq
    use file_utils, only: stdout_unit
    use mp, only: proc0
# if SAVEEQ == _HDF_
# if FCOMPILER != _GFORTRAN_
    use hdf5, only: h5sclose_f
# endif
    use hdf_wrapper, only: hdf_finish, hdf_error
    implicit none
    integer :: istatus
# endif

    if (debug.and.proc0) write(stdout_unit,*) 'Start finish_save_eq'

    if (.not.initialized) return

# if SAVEEQ == _HDF_
    call h5sclose_f(dist_fspace_asis, istatus)
    if (istatus /= 0) call hdf_error (message='h5sclose_f error')
    call h5sclose_f(dist_mspace_asis, istatus)
    if (istatus /= 0) call hdf_error (message='h5sclose_f error')

# endif

    initialized = .false.

    if (debug.and.proc0) write(stdout_unit,*) 'End finish_save_eq'

  end subroutine finish_save_eq

  subroutine read_parameters
    use file_utils, only: input_unit, error_unit, input_unit_exist
    use mp, only: proc0, broadcast, mp_abort
    integer :: in_file
    logical :: exist
    integer :: ireaderr=0
    logical :: abort_readerr=.true.

    namelist /equil_io/ equilibrium_file

    if (proc0) then
       in_file = input_unit_exist("equil_io", exist)
       if (exist) read (unit=in_file, nml=equil_io, iostat=ireaderr)
    endif

    call broadcast (ireaderr)
    if (abort_readerr) then
       if (ireaderr > 0) then
          call mp_abort ('Read error at equil_io')
       else if (ireaderr < 0) then
          call mp_abort ('End of file/record occurred at equil_io')
       endif
    endif

    call broadcast (equilibrium_file)

  end subroutine read_parameters

# if SAVEEQ == _HDF_
  subroutine hdf_write_g_asis (loc, name, g, ierr)
# if FCOMPILER != _GFORTRAN_
    use hdf5, only: HID_T, H5FD_MPIO_COLLECTIVE_F, H5P_DATASET_XFER_F
    use hdf5, only: h5dcreate_f, h5dwrite_f, h5dclose_f
    use hdf5, only: h5pcreate_f, h5pset_dxpl_mpio_f, h5pclose_f
# endif
    use hdf_wrapper, only: hdf_file_real, hdf_error
    use theta_grid, only: ntgrid
    use agk_layouts, only: g_lo
    use convert, only: c2r
    use agk_mem, only: alloc8, dealloc8
    implicit none
    integer (HID_T), intent(in) :: loc
    character (len=*), intent(in) :: name
    complex, intent(in) :: g(-ntgrid:ntgrid,2,g_lo%llim_proc:g_lo%ulim_alloc)
    integer, intent(out) :: ierr

    integer (HID_T) :: dist_dset_id, plist_id
    real, allocatable :: g_ri(:,:,:,:)

    call h5pcreate_f(H5P_DATASET_XFER_F, plist_id, ierr)
    if (ierr /= 0) then
       call hdf_error (message='h5pcreate_f error')
       goto 100
    endif

    call h5pset_dxpl_mpio_f(plist_id, H5FD_MPIO_COLLECTIVE_F, ierr)
    if (ierr /= 0) then
       call hdf_error (message='h5pset_dxpl_mpio_f error')
       goto 100
    endif

    call h5dcreate_f(loc, name, hdf_file_real, dist_fspace_asis, dist_dset_id, ierr)
    if (ierr /= 0) then
       call hdf_error (message='h5dcreate_f error')
       goto 100
    endif

    ! Write the dataset collectively
    allocate(g_ri(2,-ntgrid:ntgrid,2,g_lo%llim_proc:g_lo%ulim_alloc)) ; call alloc8(r4=g_ri, v='g_ri')
    g_ri=0.
    call c2r(g,g_ri)
    call h5dwrite_f(dist_dset_id, hdf_file_real, g_ri, count_asis, ierr, &
         xfer_prp=plist_id, file_space_id=dist_fspace_asis, &
         mem_space_id=dist_mspace_asis)
    if (ierr /= 0) then
       call hdf_error (message='h5dwrite_f error')
       goto 100
    end if

100 continue ! finalize

    if (plist_id /= 0) call h5pclose_f(plist_id, ierr)
    if (dist_dset_id /= 0) call h5dclose_f(dist_dset_id, ierr)
    if (allocated(g_ri)) call dealloc8(r4=g_ri,v='g_ri')

    return

  end subroutine hdf_write_g_asis
# endif

# if SAVEEQ == _HDF_
  subroutine hdf_read_g_asis (loc, name, g, ierr)
# if FCOMPILER != _GFORTRAN_
    use hdf5, only: HID_T, H5FD_MPIO_COLLECTIVE_F, H5P_DATASET_XFER_F
    use hdf5, only: h5pcreate_f, h5pset_dxpl_mpio_f, h5pclose_f
    use hdf5, only: h5dopen_f, h5dread_f, h5dclose_f
# endif
    use hdf_wrapper, only: hdf_file_real, hdf_error
    use theta_grid, only: ntgrid
    use agk_layouts, only: g_lo
    use convert, only: r2c
    use agk_mem, only: alloc8, dealloc8
    implicit none
    integer (HID_T), intent(in) :: loc
    character (len=*), intent(in) :: name
    complex, intent(out) :: g(-ntgrid:ntgrid,2,g_lo%llim_proc:g_lo%ulim_alloc)
    integer, intent(out) :: ierr

    integer (HID_T) :: dist_dset_id, plist_id
    real, allocatable :: g_ri(:,:,:,:)
    
    call h5pcreate_f(H5P_DATASET_XFER_F, plist_id, ierr)
    if (ierr /= 0) then
       call hdf_error (message='h5pcreate_f error')
       goto 100
    end if

    call h5pset_dxpl_mpio_f(plist_id, H5FD_MPIO_COLLECTIVE_F, ierr)
    if (ierr /= 0) then
       call hdf_error (message='h5pset_dxpl_mpio_f error')
       goto 100
    end if

    call h5dopen_f(loc, name, dist_dset_id, ierr)
    if (ierr /= 0) then
       call hdf_error (message='h5dopen_f error')
       goto 100
    end if

    ! Read the dataset collectively
    allocate(g_ri(2,-ntgrid:ntgrid,2,g_lo%llim_proc:g_lo%ulim_alloc)) ; call alloc8(r4=g_ri, v='g_ri')
    g_ri=0.
    call h5dread_f(dist_dset_id, hdf_file_real, g_ri, count_asis, ierr, &
         xfer_prp=plist_id, file_space_id=dist_fspace_asis, &
         mem_space_id=dist_mspace_asis)
    if (ierr /= 0) then
       call hdf_error (message='h5dread_f error')
       goto 100
    end if
    call r2c(g,g_ri)

100 continue ! finalize

    if (plist_id /= 0) call h5pclose_f(plist_id, ierr)
    if (dist_dset_id /= 0) call h5dclose_f(dist_dset_id, ierr)
    if (allocated(g_ri)) call dealloc8(r4=g_ri,v='g_ri')

    return

  end subroutine hdf_read_g_asis
# endif

# if SAVEEQ == _HDF_
  subroutine dist_hyperslab_asis
# if FCOMPILER != _GFORTRAN_
    use hdf5, only: HID_T, HSIZE_T, H5S_SELECT_SET_F
    use hdf5, only: h5screate_simple_f, h5sselect_hyperslab_f
    use hdf5, only: h5sselect_none_f
# endif
    use hdf_wrapper, only: hdf_error
    use agk_layouts, only: g_lo
    use theta_grid, only: ntgrid
    implicit none
    
    integer (HSIZE_T):: dims(rank), offset(rank)

    integer :: istatus
    integer :: n_elements

    n_elements = max(g_lo%ulim_proc-g_lo%llim_proc+1,0)
    dims=(/ 2, 2*ntgrid+1, 2, g_lo%ulim_world-g_lo%llim_world+1 /)
    offset=(/ 0, 0, 0, g_lo%llim_proc /)
    count_asis=(/ 2, 2*ntgrid+1, 2, n_elements /)

    ! n_elements = 0 is not allowed for h5screate_simple_f
    ! selection is reset afterwards (h5sselect_none_f below)
    if (n_elements == 0) count_asis=(/ 2, 2*ntgrid+1, 2, 1 /)
    
    call h5screate_simple_f(rank, dims, dist_fspace_asis, istatus)
    if (istatus /= 0) call hdf_error (message='h5screate_simple_f error')
    ! Select hyperslab in the file
    call h5sselect_hyperslab_f(dist_fspace_asis, H5S_SELECT_SET_F, offset, count_asis, istatus)
    if (istatus /= 0) call hdf_error (message='h5sselect_hyperslab_f error')

    offset=(/ 0,0,0,0 /)
    call h5screate_simple_f(rank, count_asis, dist_mspace_asis, istatus)
    if (istatus /= 0) call hdf_error (message='h5screate_simple_f error')
    call h5sselect_hyperslab_f(dist_mspace_asis, H5S_SELECT_SET_F, offset, count_asis, istatus)
    if (istatus /= 0) call hdf_error (message='h5sselect_hyperslab_f error')

    if (n_elements == 0) then
       ! no data to write
       call h5sselect_none_f(dist_fspace_asis, istatus)
       if (istatus /= 0) call hdf_error (message='h5sselect_none_f error')
       call h5sselect_none_f(dist_mspace_asis, istatus)
       if (istatus /= 0) call hdf_error (message='h5sselect_none_f error')
    end if

  end subroutine dist_hyperslab_asis
# endif

end module agk_equil_io
