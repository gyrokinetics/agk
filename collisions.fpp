# include "define.inc"

! TT: for variable step AB3 I made changes
!  dtime -> dtime(1)
! everywhere (3/28/10)

module collisions

  use redistribute, only: redist_type

  implicit none

  public :: init_collisions
  public :: solfp1
  public :: reset_init
  public :: update_vnewh          !Used for adapative hypercollisionality
  public :: get_dvperp
  public :: init_g2le_redistribute, redist_g2le  ! MAB

  private

  ! knobs
  logical :: conserve_momentum, const_v
  logical :: conserve_moments         ! should replace conserve_momentum -- MAB
  integer :: collision_model_switch
  integer :: lorentz_switch, ediff_switch
  logical :: adjust
  logical :: heating
  logical :: heating_exp, heating_imp
  integer :: heating_diag_switch
  logical :: hyper_colls
  logical :: conservative       ! MAB: 2/2/09
  logical :: resistivity
  logical :: resistivity_test
  logical :: ei_coll_only
  logical :: test

  integer, parameter :: collision_model_lorentz = 1
  integer, parameter :: collision_model_none = 3
  integer, parameter :: collision_model_lorentz_test = 5
  integer, parameter :: collision_model_full = 6
  integer, parameter :: collision_model_ediffuse = 7

  integer, parameter :: lorentz_scheme_conservative = 1
  integer, parameter :: lorentz_scheme_old = 2

  integer, parameter :: ediff_scheme_conservative = 1
  integer, parameter :: ediff_scheme_old = 2

  integer, parameter :: heating_diag_before_coll = 1
  integer, parameter :: heating_diag_after_coll = 2

  real, dimension (:,:), allocatable :: vpdiff
  ! (2,nlambda)

  real, dimension (:,:), allocatable :: vnew
  ! (negrid,nspec) replicated

  ! MAB: new collision frequencies needed when energy diffusion included 
  real, dimension (:,:), allocatable :: vnew_s, vnew_D, vnew_E, delvnew

  ! TT: Do we want to keep ig dependence for vnewh?
  ! TT: I've omitted ig dependence of vnewh (5/2/08).
  ! only for hyper-diffusive collisions
  real, dimension (:,:,:), allocatable :: vnewh
  ! (nakx,naky,nspec) replicated

  ! only for "new" momentum conservation (8.06)
  real, dimension (:,:), allocatable :: s0, w0, z0
# ifdef USE_LE_LAYOUT
  real, dimension (:,:,:), allocatable :: s0le, w0le, z0le
  real, dimension (:,:,:), allocatable :: aj0le, aj1vp2le
# elif USE_L2E_MAP
! TT: Lorentz layout conservation
  real, dimension (:,:), allocatable :: s0lz, w0lz, z0lz
  real, dimension (:,:), allocatable :: aj0lz, aj1vp2lz
# endif

  ! MAB>
  ! needed for momentum and energy conservation due to energy diffusion (3.08)
!  real, dimension (:), allocatable :: bs0
  real, dimension (:,:), allocatable :: bs0, bw0, bz0
# ifdef USE_LE_LAYOUT
  complex, dimension (:,:,:), allocatable :: bs0le, bw0le, bz0le
# endif
  ! <MAB

  ! MAB>
  ! only for energy diffusion
  real, dimension (:,:), allocatable :: ec1, ebetaa, eql
  real, dimension (:,:), allocatable :: ed1, eh1, ee1
  complex, dimension (:,:), allocatable :: ged, gedc
  complex, dimension (:,:), allocatable :: gedd
  ! <MAB

  ! only for lorentz
  real :: cfac
  real, dimension (:,:), allocatable :: c1, betaa, ql, d1, h1, e1
  complex, dimension (:,:), allocatable :: glz, glzc
  complex, dimension (:,:), allocatable :: glzd
  ! ( (2*nlambda+1), -*- lz_layout -*-)

  ! le_layout
  real, dimension (:,:,:), allocatable :: ec1le, ebetaale, eqle
  real, dimension (:,:,:), allocatable :: ed1le, eh1le, ee1le
  real, dimension (:,:,:), allocatable :: c1le, betaale, qle, d1le, h1le, e1le
  complex, dimension (:,:,:), allocatable :: gle, glec
  complex, dimension (:,:,:), allocatable :: gled

  type (redist_type), save :: lorentz_map
  type (redist_type), save :: ediffuse_map  ! MAB
  type (redist_type), save :: g2le
  type (redist_type), save :: l2e_map

  logical :: hypermult
  logical :: initialized = .false.
  logical :: accelerated_x = .false.
  logical :: accelerated_v = .false.

contains

  subroutine init_collisions

    use species, only: init_species, nspec, spec
    use theta_grid, only: init_theta_grid, ntgrid
    use kgrids, only: init_kgrids, naky, nakx
    use le_grids, only: init_le_grids, nlambda, negrid 
    use run_parameters, only: init_run_parameters
    use agk_layouts, only: init_dist_fn_layouts, init_agk_layouts

    implicit none
    if (initialized) return
    initialized = .true.

    call init_agk_layouts
    call init_species

    hyper_colls = .false.
    if (any(spec%nu_h > epsilon(0.0))) hyper_colls = .true.

    call init_theta_grid
    call init_kgrids
    call init_le_grids (accelerated_x, accelerated_v)
    call init_run_parameters
    call init_dist_fn_layouts (ntgrid, naky, nakx, nlambda, negrid, nspec)

    call read_parameters
    call init_map
    call init_arrays

  end subroutine init_collisions

  subroutine read_parameters
    use file_utils, only: input_unit, error_unit, input_unit_exist
    use text_options, only: text_option, get_option_value
    use mp, only: proc0, broadcast, mp_abort
    use species, only: nspec
    implicit none
    type (text_option), dimension (7), parameter :: modelopts = &
         (/ text_option('default', collision_model_full), &
            text_option('full', collision_model_full), &
            text_option('lorentz', collision_model_lorentz), &
            text_option('lorentz-test', collision_model_lorentz_test), &
            text_option('ediffuse', collision_model_ediffuse), &  ! MAB
            text_option('none', collision_model_none), &
            text_option('collisionless', collision_model_none) /)
    type (text_option), dimension (3), parameter :: schemeopts = &
         (/ text_option('default', lorentz_scheme_conservative), &
            text_option('conservative', lorentz_scheme_conservative), &
            text_option('old', lorentz_scheme_old) /)
    type (text_option), dimension (3), parameter :: eschemeopts = &
         (/ text_option('default', ediff_scheme_conservative), &
            text_option('conservative', ediff_scheme_conservative), &
            text_option('old', ediff_scheme_old) /)
    type (text_option), dimension (3), parameter :: heatdiagopts = &
         (/ text_option('default', heating_diag_before_coll), &
            text_option('before', heating_diag_before_coll), &
            text_option('after', heating_diag_after_coll) /)
    character(20) :: collision_model, lorentz_scheme, ediff_scheme
    character(20) :: heating_diag_type
    namelist /collisions_knobs/ collision_model, conserve_momentum, heating, heating_exp, heating_imp, heating_diag_type, &
         adjust, const_v, cfac, hypermult, conserve_moments, lorentz_scheme, &
         ediff_scheme, test, resistivity, resistivity_test, ei_coll_only, & ! TT: Last added on 8/5/08
         conservative ! MAB: added 2/1/09
    integer :: ierr, in_file
    logical :: exist
    logical :: not_warned = .true.
    integer :: ireaderr=0
    logical :: abort_readerr=.true.

    if (proc0) then
       hypermult = .false.
       cfac = 1.   ! DEFAULT CHANGED TO INCLUDE CLASSICAL DIFFUSION: APRIL 18, 2006
!!!RN       adjust = .true.
       collision_model = 'default' ! DEFAULT USES PITCH-ANGLE + ENERGY DIFFUSION
       lorentz_scheme = 'default'
       ediff_scheme = 'default'
!       conserve_momentum = .true. ! DEFAULT CHANGED TO REFLECT IMPROVED MOMENTUM CONSERVATION, 8/06
       conserve_momentum = .false. ! DEFAULT CHANGED TO REFLECT ENERGY DIFFUSION OPERATOR, 4/08
       conserve_moments = .true.   ! MAB
       resistivity = .false.
       conservative = .false.   ! MAB: added 2/1/09
       const_v = .false.
       heating = .false.
       heating_exp = .true.
       heating_imp = .true.
       heating_diag_type = 'after'
       test = .false.
       resistivity_test = .false.
       ei_coll_only = .false.
       in_file = input_unit_exist ("collisions_knobs", exist)
       if (exist) read (unit=in_file, nml=collisions_knobs, iostat=ireaderr)

       ierr = error_unit()
       call get_option_value &
            (collision_model, modelopts, collision_model_switch, &
            ierr, "collision_model in collisions_knobs")

       call get_option_value &
            (lorentz_scheme, schemeopts, lorentz_switch, &
            ierr, "lorentz_scheme in collisions_knobs")

       call get_option_value &
            (ediff_scheme, eschemeopts, ediff_switch, &
            ierr, "ediff_scheme in collisions_knobs")

       call get_option_value &
            (heating_diag_type, heatdiagopts, heating_diag_switch, &
            ierr, "heating_diag_type in collisions_knobs")
    end if

    call broadcast (ireaderr)
    if (abort_readerr) then
       if (ireaderr > 0) then
          call mp_abort ('Read error at collisions_knobs')
       else if (ireaderr < 0) then
          call mp_abort ('End of file/record occurred at collisions_knobs')
       endif
    endif

    if (conserve_momentum .and. &
         collision_model_switch /= collision_model_none) then
       if (not_warned) then
          ierr = error_unit()
          write (ierr,*) 'WARNING: conserve_momentum flag is obsolete!'
          write (ierr,*) 'WARNING: Now that energy diffusion operator is implemented (4/1/08),'
          write (ierr,*) 'WARNING: we should use conserve_moments option instead of conserve_momentum only!'
          not_warned = .false.
       end if
       conserve_moments = conserve_momentum
    end if

    if (resistivity) then
       if(nspec /= 2) then
          write(error_unit(),*) 'Forcing resistivity = FALSE'
          write(error_unit(),*) 'because you have chosen nspec /= 2'
          resistivity = .false.
       endif
    endif

    ! if heating is set to T, _exp and _imp are both T by default.
    !  _exp/_imp can be turned off by explicitly specifying _exp/_imp
    ! if heating is set to F, _exp/_imp are forced to F
    if (.not.heating) then
       heating_exp=.false.
       heating_imp=.false.
    end if
      
    call broadcast (hypermult)
    call broadcast (cfac)
    call broadcast (conserve_momentum)
    call broadcast (conserve_moments)    ! MAB
    call broadcast (resistivity)
    call broadcast (conservative)  ! MAB: added 2/1/09
    call broadcast (const_v)
    call broadcast (collision_model_switch)
    call broadcast (lorentz_switch)
    call broadcast (ediff_switch)
    call broadcast (heating)
    call broadcast (heating_exp)
    call broadcast (heating_imp)
    call broadcast (heating_diag_switch)
!!!RN    call broadcast (adjust)
    call broadcast (test)
    call broadcast (resistivity_test)
    call broadcast (ei_coll_only)
  end subroutine read_parameters

  subroutine init_map

    use mp, only: finish_mp, proc0
    use redistribute, only: report_map_property

# ifdef USE_LE_LAYOUT
    call init_g2le_redistribute ! new map
    if (test) call check_g2le
# else
    select case (collision_model_switch)
    case (collision_model_full)
       ! init_lorentz_layout is called in redistribute
       call init_lorentz_redistribute
       if (test) then
          if (proc0) print *, '*** Lorentz map property ***'
          call report_map_property (lorentz_map)
       end if
       ! init_ediffuse_layout is called in redistribute
       call init_ediffuse_redistribute
       if (test) then
          if (proc0) print *, '*** Ediffuse map property ***'
          call report_map_property (ediffuse_map)
       end if
# ifdef USE_L2E_MAP
       call init_lz2ed_redistribute ! new map
       if (test) call check_l2e_map
# endif
    case (collision_model_lorentz,collision_model_lorentz_test)
       ! init_lorentz_layout is called in redistribute
       call init_lorentz_redistribute
    case (collision_model_ediffuse)
       ! init_ediffuse_layout is called in redistribute
       call init_ediffuse_redistribute
    end select
# endif

    if (test) then
       if (proc0) print *, 'init_map done'
!!$    call finish_mp
!!$    stop
    end if

  end subroutine init_map

  subroutine init_arrays
    use species, only: nspec
    use le_grids, only: negrid
    use kgrids, only: naky, nakx
    use theta_grid, only: ntgrid
    use dist_fn_arrays, only: c_rate
    use agk_mem, only: alloc8
    implicit none
    real, dimension (negrid,nspec) :: hee

    logical :: first_time = .true.

    if (first_time) then
       if (heating_exp) then
          allocate (c_rate (-ntgrid:ntgrid, nakx, naky, nspec, 3)) ; call alloc8 (c5=c_rate, v="c_rate")
       else
          allocate (c_rate (-ntgrid:ntgrid, nakx, naky, nspec, 3:3)) ; call alloc8 (c5=c_rate, v="c_rate")
       end if
       c_rate = 0.
! MAB>
! moved first_time below to avoid recalculation of coll. frequencies
! during reinitializations
!       first_time = .false. 
    end if

!    if (collision_model_switch == collision_model_none) return
    if (collision_model_switch == collision_model_none) then
       first_time = .false.
       return
    end if

! no need to recalculate collision frequencies for reinitializations
    if (first_time) then
       call init_vnew (hee)
       first_time = .false.
    end if
!    call init_vnew (hee)
! <MAB
    if (all(abs(vnew(1,:)) <= 2.0*epsilon(0.0))) then
       collision_model_switch = collision_model_none
       return
    end if

    select case (collision_model_switch)

    case (collision_model_full)

       call init_lorentz
       call init_ediffuse
       if (conserve_moments) then
          call init_lorentz_conserve
          call init_diffuse_conserve
       end if

    case (collision_model_lorentz,collision_model_lorentz_test)
       call init_lorentz
       if (conserve_moments) call init_lorentz_conserve    ! MAB

    case (collision_model_ediffuse)
       call init_ediffuse
       if (conserve_moments) call init_diffuse_conserve

    end select

  end subroutine init_arrays

  subroutine init_lorentz_conserve   ! MAB

    ! Precompute three quantities needed for momentum conservation:
    ! s0, w0, z0

    use agk_layouts, only: g_lo, ie_idx, is_idx, ik_idx, it_idx, il_idx
    use species, only: nspec, spec, electron_species
    use constants, only: pi
    use kgrids, only: naky, nakx, kperp2
    use theta_grid, only: ntgrid
    use le_grids, only: e, integrate_moment, negrid
    use agk_time, only: dtime
    use dist_fn_arrays, only: aj0, aj1vp2, vpa
    use agk_mem, only: alloc8, dealloc8 
# ifdef USE_LE_LAYOUT
    use le_grids, only: nlambda, negrid
    use agk_layouts, only: le_lo
    use redistribute, only: gather
# elif USE_L2E_MAP
    use le_grids, only: nlambda
    use agk_layouts, only: lz_lo
    use redistribute, only: gather
# endif

    implicit none

    logical, save :: first = .true.
!    complex, dimension (1,1,1) :: dum1 = 0., dum2 = 0.
    real, dimension (:,:), allocatable :: gtmp
!    real, dimension (:,:,:), allocatable :: v0z0, v2s0   ! MAB: no longer needed, 2/3/09
!    real, dimension (:), allocatable :: duinv
    real, dimension (:,:,:), allocatable :: dtmp, duinv   ! MAB: see below
    real, dimension (:,:), allocatable :: vns
    complex, dimension (:,:,:), allocatable :: z_big
    integer :: ie, ik, is, isgn, iglo, all, it
    integer :: is_ion, il

# ifdef USE_LE_LAYOUT
    integer :: nxi
    complex, dimension (:,:,:), allocatable :: ctmp
# elif USE_L2E_MAP
    integer :: nxi
    complex, dimension (:,:), allocatable :: ctmp
# endif

    if (first) then
       allocate (z0 (2, g_lo%llim_proc:g_lo%ulim_alloc)) ; call alloc8 (r2=z0, v="z0")
       allocate (w0 (2, g_lo%llim_proc:g_lo%ulim_alloc)) ; call alloc8 (r2=w0, v="w0")
       allocate (s0 (2, g_lo%llim_proc:g_lo%ulim_alloc)) ; call alloc8 (r2=s0, v="s0")
       first = .false.
    end if

! Enu should be replaced with du -- MAB
! First, get Enu and then 1/Enu == Enuinv

    allocate (gtmp (2, g_lo%llim_proc:g_lo%ulim_alloc)) ; call alloc8 (r2=gtmp, v="gtmp")
! MAB> 
    allocate (dtmp (nakx, naky, nspec)) ; call alloc8 (r3=dtmp, v="dtmp")

!    call alloc8 (duinv, 'duinv', treal, 1, nspec)
! hack to deal with fact that in conservative formulation, duinv is result of 
! vspace integration which is currently not setup to return something with only 
! 'is' index
    allocate (duinv (nakx, naky, nspec)) ; call alloc8 (r3=duinv, v="duinv")
    duinv = 0.
! <MAB
    allocate (vns (negrid, nspec)) ; call alloc8 (r2=vns, v="vns")
    vns = 0.0

    if (resistivity) then
       do is = 1, nspec
          if (spec(is)%type /= electron_species) cycle
          vns(:,is) = spec(is)%nu / e(:,is)**1.5
          if (resistivity_test) vns(:,is) = spec(is)%nu
       end do
    end if

! Enu -> du, nu -> nu_D
!
! Enu == int (E nu f_0);  Enu = Enu(z, kx, ky, s)
! Enuinv = 1/Enu
    ! duinv is integral du (which has v^2 nu_s F0 as the integrand)
! MAB>
    ! evaluate integral numerically to allow for exact cancellations
    ! with other quantities evaluated numerically later
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       il = il_idx(g_lo,iglo)
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)

       if (conservative) then
          gtmp(:,iglo)  = vnew_D(ie,is)*vpa(:,iglo) &
               * vpdiff(:,il)*sqrt(e(ie,is))
       else
          gtmp(:,iglo)  = vnew_D(ie,is)*vpa(:,iglo)**2
       end if

    end do

    all = 1
    call integrate_moment (gtmp, duinv, all)  ! not 1/du yet
! <MAB

    where (abs(duinv) > epsilon(0.0))  ! necessary b/c some species may have nu=0
                                       ! duinv=0 iff vnew=0 so ok to keep duinv=0.
       duinv = 1./duinv  ! now it is 1/du
    end where

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Now get s0, w0 & z0 (first form)

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       it = it_idx(g_lo,iglo)
       ik = ik_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       if (nspec == 1) then
          is_ion = 1
       else
          is_ion=3-is ! (e=1 i=2) or (e=2 i=1)
       end if
! conservative branching added by MAB, 2/2/09
       if (conservative) then
          ! MAB + RN: need to test
!          z0(:,iglo) = - spec(is)%z*spec(is)%dens/(spec(is_ion)%z*spec(is_ion)%dens) &
          z0(:,iglo) = spec(is)%z*spec(is)%dens/(spec(is_ion)%z*spec(is_ion)%dens) &
               * 2.0 * vns(ie,is) * vpdiff(:,il)*sqrt(e(ie,is)) * aj0(iglo) * dtime(1)
!          z0(:,iglo) = - 2.0 * vns(ie,is) * vpdiff(:,il)*sqrt(e(ie,is)) * aj0(iglo) * dtime
          ! s0 = -nu_D * vpa * J0 * dt / du
          s0(:,iglo) = - vnew_D(ie,is) * vpdiff(:,il) * sqrt(e(ie,is)) &
               * aj0(iglo) * dtime(1) * duinv(it,ik,is)
       else
          ! MAB + RN: need to test
!          z0(:,iglo) = - spec(is)%z*spec(is)%dens/(spec(is_ion)%z*spec(is_ion)%dens) &
          z0(:,iglo) = spec(is)%z*spec(is)%dens/(spec(is_ion)%z*spec(is_ion)%dens) &
               * 2.0 * vns(ie,is) * vpa(:,iglo) * aj0(iglo) * dtime(1)
!          z0(:,iglo) = - 2.0 * vns(ie,is) * vpa(:,iglo) * aj0(iglo) * dtime
          s0(:,iglo) = - vnew_D(ie,is) * vpa(:,iglo) &
               * aj0(iglo) * dtime(1) * duinv(it,ik,is)
       end if

       w0(:,iglo) = - 0.5 * vnew_D(ie,is) * aj1vp2(iglo) &
            * dtime(1) * spec(is)%smz**2 * kperp2(it,ik) * duinv(it,ik,is)
    end do

    call dealloc8 (r3=duinv, v="duinv")

    ! TT: hack for the use of the general solver
    allocate (z_big(-ntgrid:ntgrid, 2, g_lo%llim_proc:g_lo%ulim_alloc)) ; call alloc8 (c3=z_big, v="z_big")
    z_big = 0.0
    z_big(1,:,:) = cmplx(z0, 0.0)
    z_big(0,:,:) = cmplx(s0, w0)
!    call solfp_lorentz (z_big, dum1, dum2, init=.true.)
    call solfp_lorentz (z_big, init=.true.)
    z0 = real(z_big(1,:,:))                 ! z0 is redefined below
    s0 = real(z_big(0,:,:))                 ! s0 is redefined below
    w0 = aimag(z_big(0,:,:))                ! w0 is redefined below
    call dealloc8 (c3=z_big, v="z_big") 

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Now get v0z0

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ! v0 = vpa J0 f0
       gtmp(:,iglo) = vpa(:,iglo)*aj0(iglo)*z0(:,iglo)
    end do
    
    call integrate_moment (gtmp, dtmp, all) ! v0z0

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Redefine z0 = z0 / (1 + v0z0)

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       it = it_idx(g_lo,iglo)
       ik = ik_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       z0(:,iglo) = z0(:,iglo) / (1.0 + dtmp(it,ik,is))
    end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Now get v0s0

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ! v0 = vpa J0 f0
       gtmp(:,iglo) = vpa(:,iglo)*aj0(iglo)*s0(:,iglo)
    end do

    call integrate_moment (gtmp, dtmp, all)    ! v0s0

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Redefine s0 = s0 - v0s0 * z0 / (1 + v0z0)

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       s0(:,iglo) = s0(:,iglo) - dtmp(it,ik,is)*z0(:,iglo)
    end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Now get v1s0

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ie = ie_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       ! v1 = nu_D vpa J0
       if (conservative) then
          gtmp(:,iglo) = vnew_D(ie,is)*sqrt(e(ie,is))*vpdiff(:,il) &
               * aj0(iglo)*s0(:,iglo)
       else
          gtmp(:,iglo) = vnew_D(ie,is)*vpa(:,iglo)*aj0(iglo)*s0(:,iglo)
       end if
    end do

    call integrate_moment (gtmp, dtmp, all)    ! v1s0

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Redefine s0 = s0 / (1 + v0s0)

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       s0(:,iglo) = s0(:,iglo) / (1.0 + dtmp(it,ik,is))
    end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Now get v0w0

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ! v0 = vpa J0 f0
       gtmp(:,iglo) = vpa(:,iglo)*aj0(iglo)*w0(:,iglo)
    end do

    call integrate_moment (gtmp, dtmp, all)    ! v0w0

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Redefine w0 = w0 - v0w0 * z0 / (1 + v0z0) (this is w1 from MAB notes)

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       w0(:,iglo) = w0(:,iglo) - z0(:,iglo)*dtmp(it,ik,is)
    end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Get v1w1

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ie = ie_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       ! v1 = nud vpa J0 f0
       if (conservative) then
          gtmp(:,iglo) = vnew_D(ie,is)*sqrt(e(ie,is))*vpdiff(:,il) &
               * aj0(iglo)*w0(:,iglo)
       else
          gtmp(:,iglo) = vnew_D(ie,is)*vpa(:,iglo)*aj0(iglo)*w0(:,iglo)
       end if
    end do

    call integrate_moment (gtmp, dtmp, all)    ! v1w1

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Redefine w0 = w1 - v1w1 * s1 / (1 + v1s1) (this is w2 from MAB notes)

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       w0(:,iglo) = w0(:,iglo) - s0(:,iglo)*dtmp(it,ik,is)
    end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Get v2w2

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       ! v2 = nud vperp J1 f0 
       gtmp(:,iglo) = 0.5*vnew_D(ie,is)*aj1vp2(iglo)*w0(:,iglo)
    end do
    
    call integrate_moment (gtmp, dtmp, all)   ! v2w2

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Redefine w0 = w2 / (1 + v2w2)

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       w0(:,iglo) = w0(:,iglo) / (1.0 + dtmp(it,ik,is))
    end do

    call dealloc8 (r2=gtmp, v="gtmp") 
    call dealloc8 (r3=dtmp, v='dtmp') 
    call dealloc8 (r2=vns, v='vns')   

    if (test) then
       print *, 'writing out z0 & z1 in init_lorentz_conserve'
       print *, 'isign, iglo, z0, z1'
       do isgn = 1, 2
          do iglo = g_lo%llim_proc, g_lo%ulim_proc
!             print *, isgn, iglo, z0(iglo), z1(isgn,iglo)
          end do
       end do
!!$       call finish_mp
!!$       stop
    end if

# ifdef USE_LE_LAYOUT

    nxi = nlambda * 2
    allocate (ctmp(nxi+1, negrid+1, le_lo%llim_proc:le_lo%ulim_alloc)) ; call alloc8 (c3=ctmp, v="ctmp")
    allocate (z_big(-ntgrid:ntgrid, 2, g_lo%llim_proc:g_lo%ulim_alloc)); call alloc8 (c3=z_big, v="z_big")

    ! first set s0le, w0le & z0le
    z_big = 0.0
    z_big = spread(cmplx(s0, w0), 1, ntgrid*2+1)
    call gather (g2le, z_big, ctmp)
!    deallocate (z0, z1) ! TT: let's not deallocate them for reinit
    if (.not. allocated(z0le)) then
       allocate (s0le (nxi+1, negrid+1, le_lo%llim_proc:le_lo%ulim_alloc)) ; call alloc8 (r3=s0le, v="s0le")
       allocate (w0le (nxi+1, negrid+1, le_lo%llim_proc:le_lo%ulim_alloc)) ; call alloc8 (r3=w0le, v="w0le")
       allocate (z0le (nxi+1, negrid+1, le_lo%llim_proc:le_lo%ulim_alloc)) ; call alloc8 (r3=z0le, v="z0le")
    end if
    s0le = real(ctmp)
    w0le = aimag(ctmp)
    ! set z0le
    z_big = spread(cmplx(z0, 0.0), 1, ntgrid*2+1)
    call gather (g2le, z_big, ctmp)
    z0le = real(ctmp)
    
    ! next set aj0le & aj1vp2le
    z_big = spread(spread(cmplx(aj0,aj1vp2),1,2), 1, ntgrid*2+1)
    call gather (g2le, z_big, ctmp)
    if (.not. allocated(aj0le)) then
       allocate (aj0le (nxi+1, negrid+1, le_lo%llim_proc:le_lo%ulim_alloc)); call alloc8 (r3=aj0le, v="aj0le")
       allocate (aj1vp2le (nxi+1, negrid+1, le_lo%llim_proc:le_lo%ulim_alloc)); call alloc8 (r3=aj1vp2le, v="aj1vp2le")
    end if
    aj0le = real(ctmp)
    aj1vp2le = aimag(ctmp)

    call dealloc8 (c3=ctmp, v="ctmp") 
    call dealloc8 (c3=z_big, v="z_big") 

# elif USE_L2E_MAP

    nxi = nlambda * 2

    ! calculate conservation in Lorentz layout
    allocate (ctmp(nxi+1, lz_lo%llim_proc:lz_lo%ulim_alloc))  ; call alloc8 (c2=ctmp, v="ctmp")
    allocate (z_big(-ntgrid:ntgrid, 2, g_lo%llim_proc:g_lo%ulim_alloc)) ; call alloc8 (c3=z_big, v="z_big")

    ! first set s0lz, w0lz & z0lz
    z_big = spread(cmplx(s0, w0), 1, ntgrid*2+1)
    call gather (lorentz_map, z_big, ctmp)
!    deallocate (z0, z1) ! TT: let's not deallocate them for reinit
    if (.not.allocated(z0lz)) then
       allocate (s0lz (nxi+1, lz_lo%llim_proc:lz_lo%ulim_alloc)) ; call alloc8 (r2=s0lz, v="s0lz")
       allocate (w0lz (nxi+1, lz_lo%llim_proc:lz_lo%ulim_alloc)) ; call alloc8 (r2=w0lz, v="w0lz")
       allocate (z0lz (nxi+1, lz_lo%llim_proc:lz_lo%ulim_alloc)) ; call alloc8 (r2=z0lz, v="z0lz")
    end if
    s0lz = real(ctmp)
    w0lz = aimag(ctmp)
    z_big = spread(cmplx(z0, 0.0), 1, ntgrid*2+1)
    call gather (lorentz_map, z_big, ctmp)
    z0lz = real(ctmp)

    ! next set aj0lz & aj1vp2lz
    z_big = spread(spread(cmplx(aj0,aj1vp2),1,2), 1, ntgrid*2+1)
    call gather (lorentz_map, z_big, ctmp)
    if (.not.allocated(aj0lz)) then
       allocate (aj0lz (nxi+1, lz_lo%llim_proc:lz_lo%ulim_alloc)) ; call alloc8 (r2=aj0lz, v="aj0lz")
       allocate (aj1vp2lz (nxi+1, lz_lo%llim_proc:lz_lo%ulim_alloc)) ; call alloc8 (r2=aj1vp2lz, v="aj1vp2lz")
    end if
    aj0lz = real(ctmp)
    aj1vp2lz = aimag(ctmp)
    
    call dealloc8 (c2=ctmp, v="ctmp") 
    call dealloc8 (c3=z_big, v="z_big") 

# endif

  end subroutine init_lorentz_conserve  ! MAB

! MAB>
  subroutine init_diffuse_conserve
!
! Precompute four quantities needed for momentum and energy conservation:
! bz0, bw0, bs0
!
    use constants, only: pi
    use agk_layouts, only: g_lo, ie_idx, is_idx, ik_idx, it_idx
    use species, only: nspec, spec
    use kgrids, only: naky, nakx, kperp2
    use theta_grid, only: ntgrid
    use le_grids, only: e, integrate_moment
    use agk_time, only: dtime
    use dist_fn_arrays, only: aj0, aj1vp2, vpa
    use agk_mem, only: alloc8, dealloc8
# ifdef USE_LE_LAYOUT
    use le_grids, only: nlambda, negrid
    use agk_layouts, only: le_lo
    use redistribute, only: gather
# endif

    logical, save :: first = .true.
    real, dimension (:,:), allocatable :: gtmp
!    real, dimension (:,:,:), allocatable :: v0z0, v2s0   ! MAB, 2/7/09
    complex, dimension (:,:,:), allocatable :: bz_big
!    real, dimension (:), allocatable :: duinv
    real, dimension (:,:,:), allocatable :: duinv, dtmp   ! MAB: added 2/7/09
!    real, dimension (:,:), allocatable :: vns
    integer :: ie, ik, is, iglo, all, it
# if USE_LE_LAYOUT
    integer :: nxi
    complex, dimension (:,:,:), allocatable :: ctmp
# endif

    if (first) then
       allocate (bz0(2,g_lo%llim_proc:g_lo%ulim_alloc)); call alloc8(r2=bz0,v="bz0")
       allocate (bw0(2,g_lo%llim_proc:g_lo%ulim_alloc)); call alloc8(r2=bw0,v="bw0")
       allocate (bs0(2,g_lo%llim_proc:g_lo%ulim_alloc)); call alloc8(r2=bs0,v="bs0")
!       allocate (bs0(g_lo%llim_proc:g_lo%ulim_alloc))
       first = .false.
    end if

! First, get du and then 1/du == duinv

    allocate (gtmp(2,g_lo%llim_proc:g_lo%ulim_alloc)); call alloc8(r2=gtmp,v="gtmp")
!    allocate (duinv(nspec))
    allocate (duinv(nakx,naky,nspec)); call alloc8(r3=duinv,v="duinv")  ! MAB: added 2/7/09 for conservative treatment
    allocate (dtmp(nakx,naky,nspec)); call alloc8(r3=dtmp,v="dtmp")  ! MAB: added 2/7/09 for conservative treatment
!    allocate (vns(negrid,nspec))

!    vns = vnew_s - vnew_D

! du == int (E nu_s f_0) == int (E^2 nu_E f_0);  du = du(s)
! duinv = 1/du
! MAB> changed 2/7/09 for conservative treatment
!    duinv(:) = spec(:)%nu * sqrt(2./pi)

    if (conservative) then
       do iglo = g_lo%llim_proc, g_lo%ulim_proc
          ie = ie_idx(g_lo,iglo)
          is = is_idx(g_lo,iglo)
          ! MAB: remember that vnew_E = nu_E * e
          gtmp(:,iglo) = e(ie,is)*vnew_E(ie,is)
       end do
       
       all = 1
       call integrate_moment (gtmp, duinv, all)  ! not 1/du yet
    else
       do is = 1, nspec
          duinv(:,:,is) = spec(is)%nu * sqrt(2./pi)
       end do
    end if

    where (abs(duinv) > epsilon(0.0))  ! necessary b/c some species may have nu=0
                                   ! duinv=0 iff vnew=0 so ok to keep duinv=0.
       duinv = 1./duinv  ! now it is 1/du
    end where

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Now get z0 (first form)

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       it = it_idx(g_lo,iglo)
       ik = ik_idx(g_lo,iglo)
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       ! u0 = -nu_E E dt J0 f_0 / du
       bz0(:,iglo) = - vnew_E(ie,is) *aj0(iglo) &
            * dtime(1) * duinv(it,ik,is)
    end do
    
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! redefine dq = du (for momentum-conserving terms)
    ! du == int (E nu_s f_0);  du = du(z, kx, ky, s) -- only for non-conservative scheme
    ! du == int (vpa^2 delnu f_0);  du = du(z, kx, ky, s) -- only for conservative scheme
    ! duinv = 1/du
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       gtmp(:,iglo)  = delvnew(ie,is)*vpa(:,iglo)**2
    end do
       
    all = 1
    call integrate_moment (gtmp, duinv, all)  ! not 1/du yet

    where (abs(duinv) > epsilon(0.0))  ! necessary b/c some species may have nu=0
                                        ! duinv=0 iff nu=0 so ok to keep duinv=0.
       duinv = 1./duinv  ! now it is 1/du
    end where

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Now get s0 (first form)

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       it = it_idx(g_lo,iglo)
       ik = ik_idx(g_lo,iglo)
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       ! u1 = -3 nu_s vpa dt J0 f_0 / du
       bs0(:,iglo) = - delvnew(ie,is)*vpa(:,iglo)*aj0(iglo)*dtime(1)*duinv(it,ik,is)
    end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Now get w0

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       it = it_idx(g_lo,iglo)
       ik = ik_idx(g_lo,iglo)
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       ! u0 = -3 dt J1 vperp vus a f0 / du
       bw0(:,iglo) = -0.5*delvnew(ie,is)*aj1vp2(iglo) &
            * dtime(1)*spec(is)%smz**2*kperp2(it,ik)*duinv(it,ik,is)
    end do

    ! TT: hack for the use of the general solver
    allocate (bz_big(-ntgrid:ntgrid,2,g_lo%llim_proc:g_lo%ulim_alloc)) ; call alloc8 (c3=bz_big, v="bz_big")
    bz_big = 0.0
!    bz_big(0,:,:) = cmplx( spread(bs0,1,2), bz0 )
    bz_big(0,:,:) = cmplx( bs0, bz0 )
    bz_big(1,:,:) = bw0
    call solfp_ediffuse (bz_big, init=.true.)
!    bs0 = real(bz_big(0,1,:))
    bs0 = real(bz_big(0,:,:))
    bw0 = real(bz_big(1,:,:))
    bz0 = aimag(bz_big(0,:,:))
    call dealloc8 (c3=bz_big, v="bz_big") 

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Now get v0z0

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       ! v0 = nu_E E J0 f_0
       gtmp(:,iglo) = vnew_E(ie,is)*aj0(iglo)*bz0(:,iglo)
    end do

    call integrate_moment (gtmp, dtmp, all) ! v0z0

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Redefine z0 = z0 / (1 + v0z0)

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       it = it_idx(g_lo,iglo)
       ik = ik_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       bz0(:,iglo) = bz0(:,iglo) / (1.0 + dtmp(it,ik,is))
    end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Now get v0s0

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       ! v0 = nu_E E J0
       gtmp(:,iglo) = vnew_E(ie,is)*aj0(iglo)*bs0(:,iglo)
    end do

    call integrate_moment (gtmp, dtmp, all)    ! v0s0

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Redefine s0 = s0 - v0s0 * z0 / (1 + v0z0)

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       bs0(:,iglo) = bs0(:,iglo) - dtmp(it,ik,is)*bz0(:,iglo)
    end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Now get v1s0

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       ! v1 = (nu_s - nu_D) vpa J0
       gtmp(:,iglo) = delvnew(ie,is)*vpa(:,iglo)*aj0(iglo)*bs0(:,iglo)
    end do

    call integrate_moment (gtmp, dtmp, all)    ! v1s0

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Redefine s0 = s0 / (1 + v0s0)

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       bs0(:,iglo) = bs0(:,iglo) / (1.0 + dtmp(it,ik,is))
    end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Now get v0w0

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       ! v0 = nu_E E J0
       gtmp(:,iglo) = vnew_E(ie,is)*aj0(iglo)*bw0(:,iglo)
    end do

    call integrate_moment (gtmp, dtmp, all)    ! v0w0

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Redefine w0 = w0 - v0w0 * z0 / (1 + v0z0) (this is w1 from MAB notes)

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       bw0(:,iglo) = bw0(:,iglo) - bz0(:,iglo)*dtmp(it,ik,is)
    end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Get v1w1

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       ! v1 = (nus-nud) vpa J0 f0
       gtmp(:,iglo) = delvnew(ie,is)*vpa(:,iglo)*aj0(iglo)*bw0(:,iglo)
    end do

    call integrate_moment (gtmp, dtmp, all)    ! v1w1

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Redefine w0 = w1 - v1w1 * s1 / (1 + v1s1) (this is w2 from MAB notes)

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       bw0(:,iglo) = bw0(:,iglo) - bs0(:,iglo)*dtmp(it,ik,is)
    end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Get v2w2

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       ! v2 = (nus-nud) vperp J1 f0 
       gtmp(:,iglo) = 0.5*delvnew(ie,is)*aj1vp2(iglo)*bw0(:,iglo)
    end do

    call integrate_moment (gtmp, dtmp, all)   ! v2w2

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Redefine w0 = w2 / (1 + v2w2)

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       bw0(:,iglo) = bw0(:,iglo) / (1.0 + dtmp(it,ik,is))
    end do

    call dealloc8 (r2=gtmp, v="gtmp") 
    call dealloc8 (r3=duinv, v="duinv") 
    call dealloc8 (r3=dtmp, v="dtmp") 

# ifdef USE_LE_LAYOUT

    nxi = nlambda * 2

    allocate (bz_big(-ntgrid:ntgrid, 2, g_lo%llim_proc:g_lo%ulim_alloc)) ; call alloc8 (c3=bz_big, v="bz_big")
    ! set up bs0le
!    bz_big = spread(spread(bs0,1,2), 1, ntgrid*2+1)
    bz_big = spread(bs0, 1, ntgrid*2+1)
    if (.not. allocated(bs0le)) then
       allocate (bs0le (nxi+1, negrid+1, le_lo%llim_proc:le_lo%ulim_alloc)) ; call alloc8 (c3=bs0le, v="bs0le")
    end if
    call gather (g2le, bz_big, bs0le)
!    deallocate (bs0) ! TT: let's not deallocate it for reinit
    ! set up bz0le
    bz_big = spread(bz0, 1, ntgrid*2+1)
    if (.not. allocated(bz0le)) then
       allocate (bz0le (nxi+1, negrid+1, le_lo%llim_proc:le_lo%ulim_alloc)) ; call alloc8 (c3=bz0le, v="bz0le")
    end if
    call gather (g2le, bz_big, bz0le)
!    deallocate (bz0) ! TT: let's not deallocate it for reinit
    ! set up bw0le
    bz_big = spread(bw0, 1, ntgrid*2+1)
    if (.not. allocated(bw0le)) then
       allocate (bw0le (nxi+1, negrid+1, le_lo%llim_proc:le_lo%ulim_alloc)) ; call alloc8 (c3=bw0le, v="bw0le")
    end if
    call gather (g2le, bz_big, bw0le)

    ! next set aj0le & aj1vp2le
    ! duplicated: needed for ediffuse only case
    allocate (ctmp(nxi+1, negrid+1, le_lo%llim_proc:le_lo%ulim_alloc)) ; call alloc8 (c3=ctmp, v="ctmp")
    bz_big = spread(spread(cmplx(aj0,aj1vp2),1,2), 1, ntgrid*2+1)
    call gather (g2le, bz_big, ctmp)
    if (.not. allocated(aj0le)) then
       allocate (aj0le (nxi+1, negrid+1, le_lo%llim_proc:le_lo%ulim_alloc)); call alloc8 (r3=aj0le, v="aj0le")
       allocate (aj1vp2le (nxi+1, negrid+1, le_lo%llim_proc:le_lo%ulim_alloc)); call alloc8 (r3=aj1vp2le, v="aj1vp2le")
    end if
    aj0le = real(ctmp)
    aj1vp2le = aimag(ctmp)
    call dealloc8 (c3=ctmp, v='ctmp')

!    deallocate (bw0) ! TT: let's not deallocate it for reinit
    call dealloc8 (c3=bz_big, v="bz_big")
# endif

  end subroutine init_diffuse_conserve

  subroutine init_vnew (hee)
    use species, only: nspec, spec, electron_species
    use le_grids, only: negrid, e, w
    use kgrids, only: naky, nakx, kperp2
    use run_parameters, only: zeff
    use constants
    use agk_mem, only: alloc8
    use spfunc, only: erf=>erf_ext
    implicit none
    real, dimension (:,:), intent (out) :: hee
    real,dimension (negrid,nspec) :: heevth, hsg, hsgvth  ! MAB
    integer :: ik, ie, is, it

    real :: v, k_nexp_h_max 
    real :: vl, vr, dv2l, dv2r    ! MAB: added 2/3/09

    do is = 1, nspec
       do ie = 1, negrid
          v = sqrt(e(ie,is))
          !            1                           1
          ! hee = ------------ e^{-v^2} + ( 1 - ------- ) erf(v)
          !        sqrt(pi) v                    2 v^2
          hee(ie,is) = 1.0/sqrt(pi)/v*exp(-e(ie,is)) &
               + (1.0 - 0.5/e(ie,is)) * erf(v)
!               *(1.0 - 1.0/(1.0          + v &
!               *(0.0705230784 + v &
!               *(0.0422820123 + v &
!               *(0.0092705272 + v &
!               *(0.0001520143 + v &
!               *(0.0002765672 + v &
!               *(0.0000430638)))))))**16)

! MAB>
! hsg is the G of Hirshman and Sigmar
! added to allow for momentum conservation with energy diffusion
          !          1                  1
          ! hsg = ------- erf(v) - ------------ e^{-v^2}
          !        2 v^2            sqrt(pi) v
! MAB: 2/3/09
          hsg(ie,is) = hsg_func(v)

!          hsg(ie,is) = -1.0/sqrt(pi)/v*exp(-e(ie,is)) &
!               + 0.5/e(ie,is) * erf(v)

!               *(1.0 - 1.0/(1.0 + v &  ! this line on is erf(v)
!               *(0.0705230784 + v &
!               *(0.0422820123 + v &
!               *(0.0092705272 + v &
!               *(0.0001520143 + v &
!               *(0.0002765672 + v &
!               *(0.0000430638)))))))**16)
! <MAB
       end do
    end do

!heevth is hee but using only thermal velocity (energy independent)

    do is = 1, nspec
       do ie = 1, negrid
          v = 1.          
          heevth(ie,is) = 1.0/sqrt(pi)/v*exp(-v**2) &
               + (1.0 - 0.5/v**2) * erf(v)
!               *(1.0 - 1.0/(1.0          + v &
!               *(0.0705230784 + v &
!               *(0.0422820123 + v &
!               *(0.0092705272 + v &
!               *(0.0001520143 + v &
!               *(0.0002765672 + v &
!               *(0.0000430638)))))))**16)

! MAB>
! hsg is the G of Helander and Sigmar
! added to allow for momentum conservation with energy diffusion
          hsgvth(ie,is) = hsg_func(1.0)

!          hsgvth(ie,is) = -1.0/sqrt(pi)/v*exp(-v**2) &
!               + 0.5/v**2 * erf(v)

!               *(1.0 - 1.0/(1.0 + v &
!               *(0.0705230784 + v &
!               *(0.0422820123 + v &
!               *(0.0092705272 + v &
!               *(0.0001520143 + v &
!               *(0.0002765672 + v &
!               *(0.0000430638)))))))**16)
! <MAB
       end do
    end do                                                                  

    if (.not.allocated(vnew)) then
       allocate (vnew(negrid,nspec)); call alloc8(r2=vnew,v="vnew")
       allocate (vnew_s(negrid,nspec)); call alloc8(r2=vnew_s,v="vnew_s")
       allocate (vnew_D(negrid,nspec)); call alloc8(r2=vnew_D,v="vnew_D")
       allocate (vnew_E(negrid,nspec)); call alloc8(r2=vnew_E,v="vnew_E")
       allocate (delvnew(negrid,nspec)); call alloc8(r2=delvnew,v="delvnew") ! MAB: added 2/3/09
    end if
    if(.not.allocated(vnewh)) then
       allocate (vnewh (nakx, naky, nspec)) ; call alloc8 (r3=vnewh, v="vnewh")
    end if

    do is = 1, nspec
       if (spec(is)%type == electron_species) then
          do ie = 1, negrid
             if (const_v) then
                vnew(ie,is) = spec(is)%nu * (zeff + heevth(ie,is)) * 0.5
                vnew_s(ie,is) = spec(is)%nu * hsgvth(ie,is) * 4.0
                vnew_D(ie,is) = spec(is)%nu * heevth(ie,is)
                ! MAB: added 2/3/09
                if (.not. conservative) then
                   vnew_E(ie,is) = vnew_s(ie,is) * 1.5 - 2.0 * vnew_D(ie,is)
                   delvnew(ie,is) = vnew_s(ie,is) - vnew_D(ie,is)
                end if
             else
                vnew(ie,is) = spec(is)%nu / e(ie,is)**1.5 &
                     *(zeff + hee(ie,is))*0.5        ! w/ ee
!                        * zeff * 0.5                    ! no ee
                vnew_s(ie,is) = spec(is)%nu / sqrt(e(ie,is)) * hsg(ie,is) * 4.0
                vnew_D(ie,is) = spec(is)%nu / e(ie,is)**1.5 * hee(ie,is)
                if (.not. conservative) then
                   ! MAB: added multiplication by e(ie,is) since nu_E always appears
                   ! multiplied by e(ie,is)
                   vnew_E(ie,is) = e(ie,is) * (vnew_s(ie,is) * (2.0 - 0.5 / e(ie,is)) &
                        - 2.0 * vnew_D(ie,is))
                   delvnew(ie,is) = vnew_s(ie,is)-vnew_D(ie,is)
                end if
             end if
             ! velocity independent collision freq for resistivity
             if (resistivity_test) then
                vnew(ie,is) = spec(is)%nu * (zeff + &
                     hee(ie,is)/e(ie,is)**1.5 )*0.5        ! w/ ee
                vnew_s(ie,is) = spec(is)%nu / sqrt(e(ie,is)) * hsg(ie,is) * 4.0
                vnew_D(ie,is) = spec(is)%nu / e(ie,is)**1.5 * hee(ie,is)
                if (.not. conservative) then
                   vnew_E(ie,is) = e(ie,is) * (vnew_s(ie,is) * (2.0 - 0.5 / e(ie,is)) &
                        - 2.0 * vnew_D(ie,is))
                   delvnew(ie,is) = vnew_s(ie,is) - vnew_D(ie,is)
                end if
             endif
          end do
          if(ei_coll_only) then
             vnew(:,is) = spec(is)%nu / e(:,is)**1.5*zeff*0.5
             vnew_s(:,is)=0.
             vnew_D(:,is)=0.
             vnew_E(:,is)=0.
             delvnew(:,is) = 0.
          endif
       else
          do ie = 1, negrid
             if (const_v) then
                vnew(ie,is) = spec(is)%nu * heevth(ie,is) * 0.5
             else
                !          nu
                ! vnew = ------- hee
                !         2 v^2
                vnew(ie,is) = spec(is)%nu / e(ie,is)**1.5 &
                     * hee(ie,is) * 0.5
             end if
             !           2 nu                2 v
             ! vnew_s = ------ [ erf(v) - ---------- e^{-v^2} ]
             !            v^3              sqrt(pi)
             vnew_s(ie,is) = spec(is)%nu / sqrt(e(ie,is)) * hsg(ie,is) * 4.0
             vnew_D(ie,is) = 2.0 * vnew(ie,is)
             if (.not. conservative) then
                vnew_E(ie,is) = e(ie,is) * (vnew_s(ie,is) * (2.0 - 0.5 / e(ie,is)) &
                     - 2.0 * vnew_D(ie,is))
                delvnew(ie,is) = vnew_s(ie,is) - vnew_D(ie,is)
             end if
          end do
          if(ei_coll_only) then
             vnew(:,is) = 0.
             vnew_s(:,is)=0.
             vnew_D(:,is)=0.
             vnew_E(:,is)=0.
             delvnew(:,is)=0.
          endif
       end if

       if (conservative) then

          do ie = 2, negrid-1
             vr = 0.5*(sqrt(e(ie+1,is)) + sqrt(e(ie,is)))
             vl = 0.5*(sqrt(e(ie,is)) + sqrt(e(ie-1,is)))
             dv2r = (e(ie+1,is) - e(ie,is)) / (sqrt(e(ie+1,is)) - sqrt(e(ie,is)))
             dv2l = (e(ie,is) - e(ie-1,is)) / (sqrt(e(ie,is)) - sqrt(e(ie-1,is)))
          
             ! MAB: actually nu_E * v^2
             vnew_E(ie,is) = spec(is)%nu*(vl*exp(-vl**2)*dv2l*hsg_func(vl) &
                  - vr*exp(-vr**2)*dv2r*hsg_func(vr)) / (sqrt(pi)*w(ie,is))
             delvnew(ie,is) = spec(is)%nu*(vl*exp(-vl**2)*hsg_func(vl) &
                     - vr*exp(-vr**2)*hsg_func(vr)) / (sqrt(pi*e(ie,is))*w(ie,is))
          end do

          ! boundary at v = 0
          vr = 0.5*(sqrt(e(2,is)) + sqrt(e(1,is)))
          dv2r = (e(2,is) - e(1,is)) / (sqrt(e(2,is)) - sqrt(e(1,is)))

          vnew_E(1,is) = -spec(is)%nu*vr*exp(-vr**2)*hsg_func(vr)*dv2r &
               / (sqrt(pi)*w(1,is))
          delvnew(1,is) = -spec(is)%nu*vr*exp(-vr**2)*hsg_func(vr) &
               / (sqrt(pi*e(1,is))*w(1,is))

          ! boundary at v -> infinity
          vl = 0.5*(sqrt(e(negrid,is)) + sqrt(e(negrid-1,is)))
          dv2l = (e(negrid,is) - e(negrid-1,is)) / (sqrt(e(negrid,is)) - sqrt(e(negrid-1,is)))

          vnew_E(negrid,is) = spec(is)%nu*vl*exp(-vl**2)*hsg_func(vl)*dv2l &
               / (sqrt(pi)*w(negrid,is))
          delvnew(negrid,is) = spec(is)%nu*vl*exp(-vl**2)*hsg_func(vl) &
               / (sqrt(pi*e(negrid,is))*w(negrid,is))

       end if

       ! add hyper-terms inside collision operator
!BD: Warning!
!BD: Also: there is no "grid_norm" option here and the exponent is fixed to 4 for now
!GGH: 07SEP07- Fixed so that exponent is given by spec(is)%nexp_h (default=4.0)
       if (hyper_colls) then
          k_nexp_h_max = (maxval(kperp2))**(spec(is)%nexp_h/2.)
          do ik = 1, naky
             do it = 1, nakx
                vnewh(it,ik,is) = spec(is)%nu_h &
                     * kperp2(it,ik)**(spec(is)%nexp_h/2.) / k_nexp_h_max
             end do
          end do
       else
          vnewh = 0.
       end if
    end do

  end subroutine init_vnew

  function hsg_func (vel)

    use constants, only: pi
    use spfunc, only: erf => erf_ext

    implicit none

    real, intent (in) :: vel
    real :: hsg_func

    hsg_func = 0.5*erf(vel)/vel**2-exp(-vel**2)/(sqrt(pi)*vel)

  end function hsg_func

!-----------------------------------------------------------------------------
! Update vnewh if adaptive hypercollisions has changed nu_h
! GGH: 2007 JUN 5
!-----------------------------------------------------------------------------
  subroutine update_vnewh
    use species, only: nspec, spec, specie
    use kgrids, only: naky, nakx, kperp2
    implicit none
    integer :: ik, is, it
    real :: k_nexp_h_max            !kperp_max normalization of hypercollisionality

    !Recalculate vnewh based on updated value of nu_h
    do is = 1, nspec
       if (hyper_colls) then
          k_nexp_h_max = (maxval(kperp2))**(spec(is)%nexp_h/2.) 
          do ik = 1, naky
             do it = 1, nakx
                vnewh(it,ik,is) = spec(is)%nu_h &
                     * kperp2(it,ik)**(spec(is)%nexp_h/2.) / k_nexp_h_max
             end do
          end do
       else
          vnewh = 0.
       end if
    enddo
    
    !Recalculate arrays for collision operator
    select case (collision_model_switch)
    case (collision_model_lorentz,collision_model_lorentz_test,collision_model_full)
       call init_lorentz
       if (conserve_moments) call init_lorentz_conserve   ! MAB
    end select

  end subroutine update_vnewh
!-----------------------------------------------------------------------------

  subroutine init_ediffuse

    use le_grids, only: negrid, ecut, vgrid
    use egrid, only: zeroes, energy
    use agk_layouts, only: ik_idx, it_idx, is_idx
    use agk_mem, only: alloc8, dealloc8
# ifdef USE_LE_LAYOUT
    use le_grids, only: nlambda
    use agk_layouts, only: le_lo
# else
    use agk_layouts, only: e_lo, il_idx
# endif

    implicit none

    integer :: ik, it, il, ie, is, ierr
    real, dimension (:), allocatable :: aa, bb, cc
    real, dimension (:), allocatable :: dd, hh, ee
    real, dimension (negrid-1) :: em
# ifdef USE_LE_LAYOUT
    integer :: ile, ixi, nxi
# else
    integer :: ielo
# endif

    allocate (aa(negrid)); call alloc8 (r1=aa,v="aa")
    allocate (bb(negrid)); call alloc8 (r1=bb,v="bb")
    allocate (cc(negrid)); call alloc8 (r1=cc,v="cc")
    allocate (dd(negrid)); call alloc8 (r1=dd,v="dd")
    allocate (hh(negrid)); call alloc8 (r1=hh,v="hh")
    allocate (ee(negrid)); call alloc8 (r1=ee,v="ee")

    if (.not.vgrid) then
       do ie=1, negrid-1
          em(ie) = energy ((zeroes(ie)+zeroes(ie+1))/2., ecut, ierr)
       end do
    end if

# ifdef USE_LE_LAYOUT

    nxi = nlambda*2
    if (.not.allocated(gle)) then
       allocate (gle(nxi+1, negrid+1, le_lo%llim_proc:le_lo%ulim_alloc)); call alloc8 (c3=gle, v="gle")
       gle = 0.0
       if (heating_exp) then
          allocate (glec(nxi+1, negrid+1, le_lo%llim_proc:le_lo%ulim_alloc)); call alloc8 (c3=glec, v="glec")
          allocate (gled(nxi+1, negrid+1, le_lo%llim_proc:le_lo%ulim_alloc)); call alloc8 (c3=gled, v="gled")
          glec = 0.0 ; gled = 0.0
       end if
    end if
    if (.not.allocated(ec1le)) then
       allocate (ec1le   (nxi, negrid, le_lo%llim_proc:le_lo%ulim_alloc)) ; call alloc8 (r3=ec1le, v="ec1le")
       allocate (ebetaale(nxi, negrid, le_lo%llim_proc:le_lo%ulim_alloc)) ; call alloc8 (r3=ebetaale, v="ebetaale")
       allocate (eqle    (nxi, negrid, le_lo%llim_proc:le_lo%ulim_alloc)) ; call alloc8 (r3=eqle, v="eqle")
       ec1le = 0.0 ; ebetaale = 0.0 ; eqle = 0.0
       if (heating_exp) then
          allocate (ed1le    (nxi, negrid, le_lo%llim_proc:le_lo%ulim_alloc)) ; call alloc8 (r3=ed1le, v="ed1le")
          allocate (eh1le    (nxi, negrid, le_lo%llim_proc:le_lo%ulim_alloc)) ; call alloc8 (r3=eh1le, v="eh1le")
          allocate (ee1le    (nxi, negrid, le_lo%llim_proc:le_lo%ulim_alloc)) ; call alloc8 (r3=ee1le, v="ee1le")
          ed1le = 0.0 ; eh1le = 0.0 ; ee1le = 0.0
       end if
    end if

    do ile = le_lo%llim_proc, le_lo%ulim_proc
       ik = ik_idx(le_lo, ile)
       it = it_idx(le_lo, ile)
       is = is_idx(le_lo, ile)
       do ixi=1, nxi
          il = min(ixi, nxi+1-ixi)
          call get_ediffuse_matrix (aa, bb, cc, dd, hh, ee, ik, it, il, is, em)
          ec1le(ixi,:,ile) = cc
          ebetaale(ixi,1,ile) = 1.0 / bb(1)
          do ie = 1, negrid-1
             eqle(ixi,ie+1,ile) = aa(ie+1) * ebetaale(ixi,ie,ile)
             ebetaale(ixi,ie+1,ile) = 1.0 / ( bb(ie+1) - eqle(ixi,ie+1,ile) &
                  * ec1le(ixi,ie,ile) )
          end do
          if (heating_exp) then
             ed1le(ixi,:,ile) = dd
             eh1le(ixi,:,ile) = hh
             ee1le(ixi,:,ile) = ee
          end if
       end do
    end do

# else

    if (.not.allocated(ged)) then
       allocate (ged(negrid+1, e_lo%llim_proc:e_lo%ulim_alloc)) ; call alloc8 (c2=ged, v="ged")
       ged = 0.0
       if (heating_exp) then
          allocate (gedc(negrid+1, e_lo%llim_proc:e_lo%ulim_alloc)) ; call alloc8 (c2=gedc, v="gedc")
          allocate (gedd(negrid+1, e_lo%llim_proc:e_lo%ulim_alloc)) ; call alloc8 (c2=gedd, v="gede")
          gedc = 0.0 ; gedd = 0.0
       end if
    end if
    if (.not.allocated(ec1)) then
       allocate (ec1   (negrid, e_lo%llim_proc:e_lo%ulim_alloc)) ; call alloc8 (r2=ec1, v="ec1")
       allocate (ebetaa(negrid, e_lo%llim_proc:e_lo%ulim_alloc)) ; call alloc8 (r2=ebetaa, v="ebetaa")
       allocate (eql   (negrid, e_lo%llim_proc:e_lo%ulim_alloc)) ; call alloc8 (r2=eql, v="eql")
       ec1 = 0.0 ; ebetaa = 0.0 ; eql = 0.0
       if (heating_exp) then
          allocate (ed1   (negrid, e_lo%llim_proc:e_lo%ulim_alloc)) ; call alloc8 (r2=ed1, v="ed1")
          allocate (eh1   (negrid, e_lo%llim_proc:e_lo%ulim_alloc)) ; call alloc8 (r2=eh1, v="eh1")
          allocate (ee1   (negrid, e_lo%llim_proc:e_lo%ulim_alloc)) ; call alloc8 (r2=ee1, v="ee1")
          ed1 = 0.0 ; eh1 = 0.0; ee1 = 0.0
       end if
    end if

    do ielo = e_lo%llim_proc, e_lo%ulim_proc
       is = is_idx(e_lo, ielo)
       ik = ik_idx(e_lo, ielo)
       il = il_idx(e_lo, ielo)
       it = it_idx(e_lo, ielo)
       call get_ediffuse_matrix (aa, bb, cc, dd, hh, ee, ik, it, il, is, em)
       ec1(:,ielo) = cc
       ebetaa(1,ielo) = 1.0 / bb(1)
       do ie = 1, negrid-1
          eql(ie+1,ielo) = aa(ie+1) * ebetaa(ie,ielo)
          ebetaa(ie+1,ielo) = 1.0 / ( bb(ie+1) - eql(ie+1,ielo)*ec1(ie,ielo) )
       end do
       if (heating_exp) then
          ed1(:,ielo) = dd
          eh1(:,ielo) = hh
          ee1(:,ielo) = ee
       end if
    end do
    
# endif

!    deallocate (aa, bb, cc)
    call dealloc8(r1=aa,v="aa")
    call dealloc8(r1=bb,v="bb")
    call dealloc8(r1=cc,v="cc")
    call dealloc8(r1=dd,v="dd")
    call dealloc8(r1=hh,v="hh")
    call dealloc8(r1=ee,v="ee")

  end subroutine init_ediffuse

  subroutine get_ediffuse_matrix (aa, bb, cc, dd, hh, ee, ik, it, il, is, em)

    use constants, only: pi
    use species, only: spec
    use le_grids, only: negrid, al
    use le_grids, only: vgrid
    use spfunc, only: erf => erf_ext
    use agk_time, only: dtime
    use egrid, only: zeroes
    use le_grids, only: w
    use kgrids, only: kperp2
    real, dimension (:), intent (out) :: aa, bb, cc
    real, dimension (:), intent (out) :: dd, hh, ee
    integer, intent (in) :: ik, it, il, is
    real, dimension (:), intent (in) :: em

    integer :: ie
    real :: vn, xe0, xe1, xe2, xer, xel, er, el
    real :: capgl, capgr, xi1, eefac

    vn = spec(is)%nu

    xi1 = sqrt(abs(1.0 - al(il)))     ! xi_j

    do ie = 2, negrid-1

       xe0 = zeroes(ie-1)
       xe1 = zeroes(ie)
       xe2 = zeroes(ie+1)

       xel = (xe0 + xe1)*0.5
       xer = (xe1 + xe2)*0.5

       !       m T kperp^2
       ! ee = ------------- (1 - xi^2) nu_s
       !          8 Z^2
       eefac = 0.125 * (1.0-xi1**2) * vnew_s(ie,is) &
            / spec(is)%zstm**2 * kperp2(it,ik) * cfac
       dd(ie) = vn * 0.5 *(erf(xe1)-2.*xe1*exp(-xe1**2)/sqrt(pi)) &
            & / (xe1**3 * (xe2-xe1)**2)
       hh(ie) = 0.0

       if (ediff_switch == ediff_scheme_conservative) then

          if (vgrid) then
             !          2 e^{-v_+^2}                2 v_+
             ! capgr = -------------- [erf(v_+) - ---------- e^{-v_+^2}]
             !          sqrt(pi) v_+               sqrt(pi)
             !             1       nu_s+ v_+^2 e^{-v_+^2}
             !       = ---------- ------------------------
             !          sqrt(pi)            nu
             ! where v_+ means v_{i+1/2} etc.
             capgr = 2.0*exp(-xer**2)*(erf(xer)-2.*xer*exp(-xer**2)/sqrt(pi))/xer/sqrt(pi)
             capgl = 2.0*exp(-xel**2)*(erf(xel)-2.*xel*exp(-xel**2)/sqrt(pi))/xel/sqrt(pi)
          else
             er = em(ie)
             el = em(ie-1)
             capgr = 8.0 * xer * sqrt(er) * exp(-2.0*er) / pi
             capgl = 8.0 * xel * sqrt(el) * exp(-2.0*el) / pi
          end if

          cc(ie) = -0.25 * vn * dtime(1) * capgr / (w(ie,is)*(xe2 - xe1))
          aa(ie) = -0.25 * vn * dtime(1) * capgl / (w(ie,is)*(xe1 - xe0))
       else

          if (vgrid) then
             capgr = 0.5 * exp(xe1**2-xer**2) / xe1**2 &
                  * ( erf(xer) - 2.*xer*exp(-xer**2)/sqrt(pi) ) / xer
             capgl = 0.5 * exp(xe1**2-xel**2) / xe1**2 &
                  * ( erf(xel) - 2.*xel*exp(-xel**2)/sqrt(pi) ) / xel
          else
             capgr = 8.0 * xer * sqrt(em(ie)) * exp(-2.0*em(ie)) / pi
             capgl = 8.0 * xel * sqrt(em(ie-1)) * exp(-2.0*em(ie-1)) / pi
          end if

          ! coefficients for tridiagonal matrix:
          cc(ie) = -vn * dtime(1) * capgr / ((xer-xel) * (xe2 - xe1))
          aa(ie) = -vn * dtime(1) * capgl / ((xer-xel) * (xe1 - xe0))
       end if

       ee(ie) = eefac
       bb(ie) = 1.0 - (aa(ie) + cc(ie)) + ee(ie)*dtime(1)

    end do

    !! boundary at xe = 0 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    xe1 = zeroes(1)
    xe2 = zeroes(2)

    xer = (xe2 + xe1) * 0.5

    er = em(1)

    eefac = 0.125 * (1.0-xi1**2) * vnew_s(1,is) &
         / spec(is)%zstm**2 * kperp2(it,ik) * cfac

    aa(1) = 0.0
    dd(1) = vn * 0.5 *(erf(xe1)-2.*xe1*exp(-xe1**2)/sqrt(pi)) &
         & / (xe1**3 * (xe2-xe1)**2)
    hh(1) = 0.0
    if (ediff_switch == ediff_scheme_conservative) then
       if (vgrid) then
          capgr = 2.0*exp(-xer**2)*(erf(xer)-2.*xer*exp(-xer**2)/sqrt(pi))/xer/sqrt(pi)
       else
          capgr = 8.0 * xer * sqrt(er) * exp(-2.0*er) / pi
       end if
       cc(1) = -0.25 * vn * dtime(1) * capgr / (w(1,is)*(xe2 - xe1))
    else
       if (vgrid) then
          capgr = 0.5*exp(xe1**2-xer**2)/xe1**2*(erf(xer)-2.*xer*exp(-xer**2)/sqrt(pi))/xer
       else
          capgr = 8.0*xer*sqrt(er)*exp(-2.0*er)/pi
       end if
       cc(1) = -vn * dtime(1) * capgr / (xer*(xe2 - xe1))
    end if

    ee(1) = eefac
    bb(1) = 1.0 - cc(1) + ee(1)*dtime(1)

    !! boundary at xe = 1 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    xe0 = zeroes(negrid-1)
    xe1 = zeroes(negrid)

    xel = (xe1 + xe0) * 0.5

    el = em(negrid-1)

    eefac = 0.125 * (1.0-xi1**2) * vnew_s(negrid,is) &
         / spec(is)%zstm**2 * kperp2(it,ik) * cfac

    cc(negrid) = 0.0
    dd(negrid) = 0.0
    hh(negrid) = 0.0
    if (ediff_switch == ediff_scheme_conservative) then
       if (vgrid) then
          capgl = 2.0*exp(-xel**2)*(erf(xel)-2.*xel*exp(-xel**2)/sqrt(pi))/xel/sqrt(pi)
       else
          capgl = 8.0 * xel * sqrt(el) * exp(-2.0*el) / pi
       end if
       aa(negrid) = -0.25 * vn * dtime(1) * capgl / (w(negrid,is)*(xe1 - xe0))
    else
       if (vgrid) then
          capgl = 0.5*exp(xe1**2-xel**2)/xe1**2*(erf(xel)-2.*xel*exp(-xel**2)/sqrt(pi))/xel
       else
          capgl = 8.0*xel*sqrt(em(negrid-1))*exp(-2.0*em(negrid-1))/pi
       end if
       aa(negrid) = -vn * dtime(1) * capgl / ((1.0-xel) * (xe1 - xe0))
    end if

    ee(negrid) = eefac
    bb(negrid) = 1.0 - aa(negrid) + ee(negrid)*dtime(1)

  end subroutine get_ediffuse_matrix

  subroutine init_ediffuse_redistribute
    use mp, only: nproc
    use species, only: nspec
    use theta_grid, only: ntgrid
    use kgrids, only: naky, nakx
    use le_grids, only: nlambda, negrid
    use agk_layouts, only: init_ediffuse_layouts
    use agk_layouts, only: g_lo, e_lo, ie_idx, ik_idx, it_idx, il_idx, is_idx
    use agk_layouts, only: idx_local, proc_id, idx
    use redistribute, only: index_list_type, init_redist, delete_list
    implicit none
    type (index_list_type), dimension(0:nproc-1) :: to_list, from_list
    integer, dimension(0:nproc-1) :: nn_to, nn_from
    integer, dimension(3) :: from_low, from_high
    integer, dimension(2) :: to_high
    integer :: to_low
    integer :: ig, isign, iglo, ik, it, il, ie, is, ielo
    integer :: n, ip
    logical :: done = .false.

    if (done) return

    call init_ediffuse_layouts (ntgrid, naky, nakx, nlambda, nspec)

    ! count number of elements to be redistributed to/from each processor
    nn_to = 0
    nn_from = 0
    do iglo = g_lo%llim_world, g_lo%ulim_world
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       do isign = 1, 2
          do ig = -ntgrid, ntgrid
             ielo = idx(e_lo,ig,isign,ik,it,il,is)
             if (idx_local(g_lo,iglo)) &
                  nn_from(proc_id(e_lo,ielo)) = nn_from(proc_id(e_lo,ielo)) + 1
             if (idx_local(e_lo,ielo)) &
                  nn_to(proc_id(g_lo,iglo)) = nn_to(proc_id(g_lo,iglo)) + 1
          end do
       end do
    end do

    do ip = 0, nproc-1
       if (nn_from(ip) > 0) then
          allocate (from_list(ip)%first(nn_from(ip))) !!! RN> unable to account memory for strucure variables
          allocate (from_list(ip)%second(nn_from(ip))) !!! RN> unable to account memory for strucure variables
          allocate (from_list(ip)%third(nn_from(ip))) !!! RN> unable to account memory for strucure variables
       end if
       if (nn_to(ip) > 0) then
          allocate (to_list(ip)%first(nn_to(ip))) !!! RN> unable to account memory for strucure variables
          allocate (to_list(ip)%second(nn_to(ip))) !!! RN> unable to account memory for strucure variables
       end if
    end do

    ! get local indices of elements distributed to/from other processors
    nn_to = 0
    nn_from = 0
    do iglo = g_lo%llim_world, g_lo%ulim_world
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       ie = ie_idx(g_lo,iglo)
       do isign = 1, 2
          do ig = -ntgrid, ntgrid
             ielo = idx(e_lo,ig,isign,ik,it,il,is)
             if (idx_local(g_lo,iglo)) then
                ip = proc_id(e_lo,ielo)
                n = nn_from(ip) + 1
                nn_from(ip) = n
                from_list(ip)%first(n) = ig
                from_list(ip)%second(n) = isign
                from_list(ip)%third(n) = iglo
             end if
             if (idx_local(e_lo,ielo)) then
                ip = proc_id(g_lo,iglo)
                n = nn_to(ip) + 1
                nn_to(ip) = n
                to_list(ip)%first(n) = ie
                to_list(ip)%second(n) = ielo
             end if
          end do
       end do
    end do

    from_low (1) = -ntgrid
    from_low (2) = 1
    from_low (3) = g_lo%llim_proc

    from_high(1) = ntgrid
    from_high(2) = 2
    from_high(3) = g_lo%ulim_alloc

    to_low = e_lo%llim_proc

    to_high(1) = negrid+1        ! TT: ?? +1 ?? for avoiding bank conflict ??
    to_high(2) = e_lo%ulim_alloc

    call init_redist (ediffuse_map, 'c', to_low, to_high, to_list, &
         from_low, from_high, from_list)

    call delete_list (to_list)
    call delete_list (from_list)

    done = .true.

  end subroutine init_ediffuse_redistribute

  subroutine init_lorentz
    use le_grids, only: nlambda
# ifdef USE_LE_LAYOUT
    use agk_layouts, only: le_lo
    use le_grids, only: negrid
# else
    use agk_layouts, only: lz_lo, ie_idx
# endif
    use agk_layouts, only: ik_idx, it_idx, is_idx
    use agk_mem, only: alloc8
    implicit none
    integer :: ik, it, il, ie, is, nxi

! TT> for debug
!    integer :: ixi
! <TT
    real, dimension (2*nlambda) :: aa, bb, cc, dd, hh, ee
# ifdef USE_LE_LAYOUT
    integer :: ile
# else
    integer :: ilz
# endif

    nxi = 2*nlambda

    call init_vpdiff

# ifdef USE_LE_LAYOUT

    if (.not.allocated(gle)) then
       allocate (gle(nxi+1, negrid+1, le_lo%llim_proc:le_lo%ulim_alloc)) ; call alloc8 (c3=gle, v="gle")
       gle = 0.0
       if (heating_exp) then
          allocate (glec(nxi+1, negrid+1, le_lo%llim_proc:le_lo%ulim_alloc)) ; call alloc8 (c3=glec, v="glec")
          allocate (gled(nxi+1, negrid+1, le_lo%llim_proc:le_lo%ulim_alloc)) ; call alloc8 (c3=gled, v="gled")
          glec = 0.0 ; gled = 0.0
       end if
    end if
    if (.not.allocated(c1le)) then
       allocate (c1le    (nxi+1, negrid, le_lo%llim_proc:le_lo%ulim_alloc)) ; call alloc8 (r3=c1le, v="c1le")
       allocate (betaale (nxi+1, negrid, le_lo%llim_proc:le_lo%ulim_alloc)) ; call alloc8 (r3=betaale, v="betaale")
       allocate (qle     (nxi+1, negrid, le_lo%llim_proc:le_lo%ulim_alloc)) ; call alloc8 (r3=qle, v="qle")
       c1le = 0.0 ; betaale = 0.0 ; qle = 0.0
       if (heating_exp) then
          allocate (d1le    (nxi+1, negrid, le_lo%llim_proc:le_lo%ulim_alloc)) ; call alloc8 (r3=d1le, v="d1le")
          allocate (h1le    (nxi+1, negrid, le_lo%llim_proc:le_lo%ulim_alloc)) ; call alloc8 (r3=h1le, v="h1le")
          allocate (e1le    (nxi+1, negrid, le_lo%llim_proc:le_lo%ulim_alloc)) ; call alloc8 (r3=e1le, v="e1le")
          d1le = 0.0 ; h1le = 0.0 ; e1le = 0.0
       end if
    endif

    do ile = le_lo%llim_proc, le_lo%ulim_proc
       ik = ik_idx(le_lo,ile)
       it = it_idx(le_lo,ile)
       is = is_idx(le_lo,ile)
       do ie = 1, negrid
          call get_lorentz_matrix (aa, bb, cc, dd, hh, ee, ik, it, ie, is)
          c1le(:nxi,ie,ile) = cc
          c1le(nxi+1,ie,ile) = 0.0

          if (heating_exp) then
             d1le(:nxi,ie,ile) = dd
             h1le(:nxi,ie,ile) = hh
             d1le(nxi+1,ie,ile) = 0.0
             h1le(nxi+1,ie,ile) = 0.0
             e1le(:nxi,ie,ile) = ee
             e1le(nxi+1,ie,ile) = 0.0
          end if

          qle(1,ie,ile) = 0.0
          betaale(1,ie,ile) = 1.0/bb(1)
          do il = 1, nxi-1
             qle(il+1,ie,ile) = aa(il+1) * betaale(il,ie,ile)
             betaale(il+1,ie,ile) = 1.0 / ( bb(il+1) - qle(il+1,ie,ile) &
                  * c1le(il,ie,ile) )
          end do
          qle(nxi+1,ie,ile) = 0.0
          betaale(nxi+1,ie,ile) = 0.0

       end do
    end do

# else

    if (.not.allocated(glz)) then
       allocate (glz (nxi+1, lz_lo%llim_proc:lz_lo%ulim_alloc)) ; call alloc8 (c2=glz, v="glz")
       glz = 0.0
       if (heating_exp) then
          allocate (glzc (nxi+1, lz_lo%llim_proc:lz_lo%ulim_alloc)) ; call alloc8 (c2=glzc, v="glzc")
          allocate (glzd (nxi+1, lz_lo%llim_proc:lz_lo%ulim_alloc)) ; call alloc8 (c2=glzd, v="glzd")
          glzc = 0. ; glzd = 0.
       end if
    end if
    if (.not.allocated(c1)) then
       allocate (c1 (nxi+1, lz_lo%llim_proc:lz_lo%ulim_alloc)) ; call alloc8 (r2=c1, v="c1")
       allocate (betaa (nxi+1, lz_lo%llim_proc:lz_lo%ulim_alloc)) ; call alloc8 (r2=betaa, v="betaa")
       allocate (ql (nxi+1, lz_lo%llim_proc:lz_lo%ulim_alloc)) ; call alloc8 (r2=ql, v="ql")
       c1 = 0.0 ; betaa = 0.0 ; ql = 0.0
       if (heating_exp) then
          allocate (d1 (nxi+1, lz_lo%llim_proc:lz_lo%ulim_alloc)) ; call alloc8 (r2=d1, v="d1")
          allocate (h1 (nxi+1, lz_lo%llim_proc:lz_lo%ulim_alloc)) ; call alloc8 (r2=h1, v="h1")
          allocate (e1 (nxi+1, lz_lo%llim_proc:lz_lo%ulim_alloc)) ; call alloc8 (r2=e1, v="e1")
          d1 = 0.0 ; h1 = 0.0 ; e1 = 0.0
       end if
    endif

    do ilz = lz_lo%llim_proc, lz_lo%ulim_proc
       ik = ik_idx(lz_lo,ilz)
       it = it_idx(lz_lo,ilz)
       ie = ie_idx(lz_lo,ilz)
       is = is_idx(lz_lo,ilz)
       call get_lorentz_matrix (aa, bb, cc, dd, hh, ee, ik, it, ie, is)

       c1(:nxi,ilz) = cc
       c1(nxi+1,ilz) = 0.0

       if (heating_exp) then
          d1(:nxi,ilz) = dd
          h1(:nxi,ilz) = hh
          d1(nxi+1,ilz) = 0.0
          h1(nxi+1,ilz) = 0.0
          e1(:nxi,ilz) = ee
          e1(nxi+1,ilz) = 0.
       end if

       ql(1,ilz) = 0.0
       betaa(1,ilz) = 1.0/bb(1)
       do il = 1, nxi-1
          ql(il+1,ilz) = aa(il+1) * betaa(il,ilz)
          betaa(il+1,ilz) = 1.0 / (bb(il+1)-ql(il+1,ilz)*c1(il,ilz))
       end do
       ql(nxi+1,ilz) = 0.0
       betaa(nxi+1,ilz) = 0.0
    end do

! TT> check matrix: some precision loss...
!  should we use double precision?
!!$    if (test) then
!!$       do ilz = lz_lo%llim_proc, lz_lo%ulim_proc
!!$          do ixi = 1, nlambda*2+1
!!$             print '("ql(",i3,", ",i5,") =",es25.11)', ixi, ilz, ql(ixi,ilz)
!!$             print '("c1(",i3,", ",i5,") =",es25.11)', ixi, ilz, c1(ixi,ilz)
!!$             print '("d1(",i3,", ",i5,") =",es25.11)', ixi, ilz, d1(ixi,ilz)
!!$             print '("h1(",i3,", ",i5,") =",es25.11)', ixi, ilz, h1(ixi,ilz)
!!$             print '("betaa(",i3,", ",i5,") =",es25.11)', &
!!$                  ixi, ilz, betaa(ixi,ilz)
!!$          end do
!!$       end do
!!$       stop
!!$    end if
! <TT

# endif

  end subroutine init_lorentz

  subroutine get_lorentz_matrix (a1, b1, c1, d1, h1, e1, ik, it, ie, is)

    use species, only: spec
    use le_grids, only: nlambda, al, e
    use le_grids, only: wl
    use agk_time, only: dtime
    use kgrids, only: kperp2
    use agk_mem, only: alloc8, dealloc8
    real, dimension (:), intent (out) :: a1, b1, c1, d1, h1, e1
    integer, intent (in) :: ik, it, ie, is
    integer :: il
    real, dimension (nlambda) :: aa, bb, cc, dd, ee, hh
    real :: xi0, xi1, xi2, xil, xir, vn, vnh, vnc
! MAB>
    real, dimension (:), allocatable :: xi
    real, dimension (:), allocatable, save :: aafac, bbfac, ccfac, ddfac, eefac
    logical :: init_xi = .true.
! <MAB

    if (collision_model_switch == collision_model_lorentz_test) then
       vn = abs(spec(is)%nu) / 2.0
       vnc = 0.
       vnh = 0.
    else
       if (hypermult) then
          vn = vnew(ie,is)*(1.+vnewh(it,ik,is))
          vnc = vnew(ie,is)
          vnh = vnewh(it,ik,is)*vnc
       else
          vn = vnew(ie,is)+vnewh(it,ik,is)
          vnc = vnew(ie,is)
          vnh = vnewh(it,ik,is)
       end if
    end if

    ! init solely lambda-dependent factors only once at beginning of simulation
    ! and store for later re-initializations
    if (init_xi) then
       allocate (xi(nlambda)); call alloc8(r1=xi,v="xi")
       allocate (aafac(nlambda)); call alloc8(r1=aafac,v="aafac")
       allocate (bbfac(nlambda)); call alloc8(r1=bbfac,v="bbfac")
       allocate (ccfac(nlambda)); call alloc8(r1=ccfac,v="ccfac")
       allocate (ddfac(nlambda)); call alloc8(r1=ddfac,v="ddfac")
       allocate (eefac(nlambda)); call alloc8(r1=eefac,v="eefac")

       ! xi = v_par/v
       xi = sqrt(abs(1.0 - al))
       do il = 2, nlambda-1
          ! xi = v_par/v
          xi0 = xi(il-1)  ! xi_{j-1}
          xi1 = xi(il)    ! xi_j
          xi2 = xi(il+1)  ! xi_{j+1}

          xil = (xi1 + xi0)*0.5  ! xi(j-1/2)
          xir = (xi1 + xi2)*0.5  ! xi(j+1/2)

          eefac(il) = 0.5 * (1.0 + xi1**2) * cfac
          ! coefficients for entropy heating calculation
          ddfac(il) = (1.0-xi1**2) / (xi2-xi1)**2
          if (lorentz_switch == lorentz_scheme_conservative) then
             ccfac(il) = 2.0 * (1.0 - xir**2) / (wl(il)*(xi2 - xi1))
             aafac(il) = 2.0 * (1.0 - xil**2) / (wl(il)*(xi1 - xi0))
             ! xi2 < xi1 < xi0 so sign flips
# ifdef HEATING_DIAG_OLD
             ddfac(il) = -2.0 * (1.0-xir*xir) / (wl(il)*(xi2-xi1))
# endif

          else
             ! coefficients for tridiagonal matrix:
             ccfac(il) = -(1.0 - xir*xir) / ((xir - xil) * (xi2 - xi1))
             aafac(il) = -(1.0 - xil*xil) / ((xir - xil) * (xi1 - xi0))

# ifdef HEATING_DIAG_OLD
             ddfac(il) = (1.0-xir*xir) / ((xir-xil) * (xi2-xi1))
# endif

          end if
          
       end do

! boundary at xi = 1
       xi0 = 1.0
       xi1 = xi(1)
       xi2 = xi(2)

       xil = (xi1 + xi0)/2.0
       xir = (xi1 + xi2)/2.0

       eefac(1) = 0.5 * (1.0 + xi1**2) * cfac
       ddfac(1) = (1.0-xi1**2) / (xi2-xi1)**2
       if (lorentz_switch == lorentz_scheme_conservative) then
          ccfac(1) = 2.0 * (1.0 - xir**2) / (wl(1)*(xi2 - xi1))

# ifdef HEATING_DIAG_OLD
          ddfac(1) = -2.0 * (1.0-xir**2) / (wl(1)*(xi2-xi1))
# endif

       else
          ccfac(1) = - (-1.0 - xir) / (xi2-xi1)

# ifdef HEATING_DIAG_OLD
          ddfac(1) = (1.0-xir*xir) / ((xir-xil) * (xi2-xi1))
# endif

       end if
       aafac(1) = 0.0

! boundary at xi = 0
       xi0 = xi(nlambda-1)
       xi1 = xi(nlambda)
       xi2 = -xi1

       xil = (xi1 + xi0)*0.5
       xir = 0.0

       eefac(nlambda) = 0.5 * (1.0 + xi1**2) * cfac
       ddfac(nlambda) = (1.0-xi1**2) / (xi2-xi1)**2
       if (lorentz_switch == lorentz_scheme_conservative) then
          ccfac(nlambda) = 2.0 * (1.0 - xir**2) / (wl(nlambda)*(xi2 - xi1))
          aafac(nlambda) = 2.0 * (1.0 - xil**2) / (wl(nlambda)*(xi1 - xi0))

# ifdef HEATING_DIAG_OLD
          ddfac(nlambda) = -2.0*(1.0-xir**2) / (wl(nlambda)*(xi2-xi1))
# endif

       else
          ccfac(nlambda) = - (1.0 - xir*xir) / ((xir - xil)*(xi2 - xi1))
          aafac(nlambda) = - (1.0 - xil*xil) / ((xir - xil)*(xi1 - xi0))

# ifdef HEATING_DIAG_OLD
          ddfac(nlambda) = (1.0-xir*xir) / ((xir-xil) * (xi2-xi1))
# endif

       end if

!       deallocate (xi)
       call dealloc8(r1=xi,v="xi")

       init_xi = .false.
    end if

    ee = e(ie,is) / spec(is)%zstm**2 * kperp2(it,ik) * eefac

    cc = vn * dtime(1) * ccfac
    aa = vn * dtime(1) * aafac

    ! xi2 < xi1 < xi0 so sign flips
    dd = vnc * ddfac
    hh = vnh * ddfac
    ee = vnc * ee

    bb = 1.0 - (aa + cc) + ee*dtime(1)

# ifdef HEATING_DIAG_OLD
    ! This is just for test old results.
    ! Old result is not correct. Bug was fixed at r2972
    ee = e(ie,is) / spec(is)%zstm**2 * kperp2(it,ik) * eefac * vnc * dtime(1)
    dd = vnc * (ddfac + ee)
    bb = 1.0 - (aa + cc) + ee
# endif

! old implementation -- MAB
!     do il = 2, nlambda-1
!        ! xi = v_par/v
!        xi0 = sqrt(abs(1.0 - al(il-1)))   ! xi_{j-1}
!        xi1 = sqrt(abs(1.0 - al(il)))     ! xi_j
!        xi2 = sqrt(abs(1.0 - al(il+1)))   ! xi_{j+1}

!        xil = (xi1 + xi0)/2.0  ! xi(j-1/2)
!        xir = (xi1 + xi2)/2.0  ! xi(j+1/2)

!        ee = 0.5*e(ie,is)*(1+xi1**2) / spec(is)%zstm**2 * kperp2(it,ik)*cfac

!        if (lorentz_switch == lorentz_scheme_conservative) then
!           cc(il) = 2.0 * vn * dtime * (1.0 - xir**2) / (wl(il)*(xi2 - xi1))
!           aa(il) = 2.0 * vn * dtime * (1.0 - xil**2) / (wl(il)*(xi1 - xi0))
!           ! xi2 < xi1 < xi0 so sign flips
!           dd(il) = vnc * (-2.0 * (1.0-xir*xir) / (wl(il)*(xi2-xi1)) + ee)
!           hh(il) = vnh * (-2.0 * (1.0-xir*xir) / (wl(il)*(xi2-xi1)))
!        else
!           ! coefficients for tridiagonal matrix:
!           cc(il) = -vn * dtime * (1.0 - xir*xir) / (xir - xil) / (xi2 - xi1)
!           aa(il) = -vn * dtime * (1.0 - xil*xil) / (xir - xil) / (xi1 - xi0)
!           ! coefficients for entropy heating calculation
!           dd(il) = vnc * ((1.0-xir*xir) / (xir-xil) / (xi2-xi1) + ee)
!           hh(il) = vnh * ((1.0-xir*xir) / (xir-xil) / (xi2-xi1))
!        end if
!        bb(il) = 1.0 - (aa(il) + cc(il)) + ee * vnc * dtime

!     end do

! ! boundary at xi = 1
!     xi0 = 1.0
!     xi1 = sqrt(abs(1.0-al(1)))
!     xi2 = sqrt(abs(1.0-al(2)))

!     xil = (xi1 + xi0)/2.0
!     xir = (xi1 + xi2)/2.0

!     ee = 0.5*e(ie,is) * (1.0+xi1**2) / spec(is)%zstm**2 * kperp2(it,ik)*cfac

!     if (lorentz_switch == lorentz_scheme_conservative) then
!        cc(1) = 2.0 * vn * dtime * (1.0 - xir**2) / (wl(1)*(xi2 - xi1))
!        dd(1) = vnc * (-2.0 * (1.0-xir**2) / (wl(1)*(xi2-xi1)) + ee)
!        hh(1) = vnh * (-2.0 * (1.0-xir**2) / (wl(1)*(xi2-xi1)))
!     else
!        cc(1) = -vn * dtime * (-1.0 - xir) / (xi2-xi1)
!        dd(1) = vnc * ((1.0-xir*xir) / (xir-xil) / (xi2-xi1) + ee)
!        hh(1) = vnh * ((1.0-xir*xir) / (xir-xil) / (xi2-xi1))
!     end if
!     aa(1) = 0.0
!     bb(1) = 1.0 - (aa(1) + cc(1)) + ee*vnc*dtime

! ! boundary at xi = 0
!     xi0 = sqrt(abs(1.0 - al(nlambda-1)))
!     xi1 = sqrt(abs(1.0 - al(nlambda)))
!     xi2 = -xi1

!     xil = (xi1 + xi0)/2.0
!     xir = (xi1 + xi2)/2.0  ! This is zero

!     ee = 0.5*e(ie,is) * (1.0+xi1**2) / spec(is)%zstm**2 * kperp2(it,ik)*cfac

!     if (lorentz_switch == lorentz_scheme_conservative) then
!        cc(nlambda) = 2.0*vn*dtime * (1.0 - xir**2) / (wl(nlambda)*(xi2 - xi1))
!        aa(nlambda) = 2.0*vn*dtime * (1.0 - xil**2) / (wl(nlambda)*(xi1 - xi0))
!        dd(nlambda) = vnc * (-2.0*(1.0-xir**2) / (wl(nlambda)*(xi2-xi1)) + ee)
!        hh(nlambda) = vnh * (-2.0*(1.0-xir**2) / (wl(nlambda)*(xi2-xi1)))
!     else
!        cc(nlambda) = -vn * dtime * (1.0 - xir*xir) / (xir - xil)/(xi2 - xi1)
!        aa(nlambda) = -vn * dtime * (1.0 - xil*xil) / (xir - xil)/(xi1 - xi0)
!        dd(nlambda) = vnc * ((1.0-xir*xir) / (xir-xil) / (xi2-xi1) + ee)
!        hh(nlambda) = vnh * ((1.0-xir*xir) / (xir-xil) / (xi2-xi1))
!     end if

!     bb(nlambda) = 1.0 - (aa(nlambda) + cc(nlambda)) + ee*vnc*dtime

! fill in the arrays for the tridiagonal
    a1(:nlambda) = aa
    b1(:nlambda) = bb
    c1(:nlambda) = cc
    d1(:nlambda) = dd
    h1(:nlambda) = hh
    e1(:nlambda) = ee

    a1(nlambda+1:2*nlambda) = cc(nlambda:1:-1)
    b1(nlambda+1:2*nlambda) = bb(nlambda:1:-1)
    c1(nlambda+1:2*nlambda) = aa(nlambda:1:-1)
    d1(nlambda+1:2*nlambda) = dd(nlambda:1:-1)
    h1(nlambda+1:2*nlambda) = hh(nlambda:1:-1)
    e1(nlambda+1:2*nlambda) = ee(nlambda:1:-1)

  end subroutine get_lorentz_matrix

  subroutine init_lorentz_redistribute
    use mp, only: nproc
    use species, only: nspec
    use theta_grid, only: ntgrid
    use kgrids, only: naky, nakx
    use le_grids, only: nlambda, negrid
    use agk_layouts, only: init_lorentz_layouts
    use agk_layouts, only: g_lo, lz_lo
    use agk_layouts, only: idx_local, proc_id
    use agk_layouts, only: ik_idx, it_idx, ie_idx, is_idx, il_idx, idx
    use redistribute, only: index_list_type, init_redist, delete_list
    implicit none
    type (index_list_type), dimension(0:nproc-1) :: to_list, from_list
    integer, dimension(0:nproc-1) :: nn_to, nn_from
    integer, dimension(3) :: from_low, from_high
    integer, dimension(2) :: to_high
    integer :: to_low
    integer :: ig, isign, iglo, il, ilz
    integer :: ik, it, ie, is
    integer :: n, ip
    logical :: done = .false.

    if (done) return

    call init_lorentz_layouts (ntgrid, naky, nakx, nlambda, negrid, nspec)

    ! count number of elements to be redistributed to/from each processor
    nn_to = 0
    nn_from = 0
    do iglo = g_lo%llim_world, g_lo%ulim_world
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       do ig = -ntgrid, ntgrid
          ilz = idx (lz_lo, ig, ik, it, ie, is)
          if (idx_local(g_lo,iglo)) &
! TT: add 2 instead of 1 since both signs move in the same way
               nn_from(proc_id(lz_lo,ilz)) = nn_from(proc_id(lz_lo,ilz)) + 2
          if (idx_local(lz_lo,ilz)) &
               nn_to(proc_id(g_lo,iglo)) = nn_to(proc_id(g_lo,iglo)) + 2
       end do
    end do

    do ip = 0, nproc-1
       if (nn_from(ip) > 0) then
          allocate (from_list(ip)%first(nn_from(ip))) !!! RN> unable to account memory for structure variables
          allocate (from_list(ip)%second(nn_from(ip))) !!! RN> unable to account memory for structure variables
          allocate (from_list(ip)%third(nn_from(ip))) !!! RN> unable to account memory for structure variables
       end if
       if (nn_to(ip) > 0) then
          allocate (to_list(ip)%first(nn_to(ip))) !!! RN> unable to account memory for structure variables
          allocate (to_list(ip)%second(nn_to(ip))) !!! RN> unable to account memory for structure variables
       end if
    end do

    ! get local indices of elements distributed to/from other processors
    nn_to = 0
    nn_from = 0
    do iglo = g_lo%llim_world, g_lo%ulim_world
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       do isign = 1, 2
          if (isign==2) il = 2*g_lo%nlambda+1 - il
          do ig = -ntgrid, ntgrid
             ilz = idx (lz_lo, ig, ik, it, ie, is)
             if (idx_local(g_lo,iglo)) then
                ip = proc_id(lz_lo,ilz)
                n = nn_from(ip) + 1
                nn_from(ip) = n
                from_list(ip)%first(n) = ig
                from_list(ip)%second(n) = isign
                from_list(ip)%third(n) = iglo
             end if
             if (idx_local(lz_lo,ilz)) then
                ip = proc_id(g_lo,iglo)
                n = nn_to(ip) + 1
                nn_to(ip) = n
                to_list(ip)%first(n) = il
                to_list(ip)%second(n) = ilz
             end if
          end do
       end do
    end do

    from_low (1) = -ntgrid
    from_low (2) = 1
    from_low (3) = g_lo%llim_proc

    to_low = lz_lo%llim_proc

    to_high(1) = 2*nlambda+1
    to_high(2) = lz_lo%ulim_alloc

    from_high(1) = ntgrid
    from_high(2) = 2
    from_high(3) = g_lo%ulim_alloc

    call init_redist (lorentz_map, 'c', to_low, to_high, to_list, &
         from_low, from_high, from_list)

    call delete_list (to_list)
    call delete_list (from_list)

    done = .true.

  end subroutine init_lorentz_redistribute

  subroutine init_g2le_redistribute
    use mp, only: nproc
    use species, only: nspec
    use theta_grid, only: ntgrid
    use kgrids, only: naky, nakx
    use le_grids, only: nlambda, negrid
    use agk_layouts, only: init_le_layouts
    use agk_layouts, only: g_lo, le_lo
    use agk_layouts, only: idx_local, proc_id
    use agk_layouts, only: ik_idx, it_idx, ie_idx, is_idx, il_idx, idx
    use redistribute, only: index_list_type, init_redist, delete_list
    implicit none
    type (index_list_type), dimension(0:nproc-1) :: to_list, from_list
    integer, dimension (0:nproc-1) :: nn_to, nn_from
    integer, dimension (3) :: from_low, from_high
    integer, dimension (3) :: to_high
    integer :: to_low
    integer :: ig, isign, iglo, il, ile
    integer :: ik, it, ie, is
    integer :: n, ip
    logical :: done = .false.

    if (done) return

    call init_le_layouts (ntgrid, naky, nakx, nspec)

    ! count number of elements to be redistributed to/from each processor
    nn_to = 0
    nn_from = 0
    do iglo = g_lo%llim_world, g_lo%ulim_world
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       do ig = -ntgrid, ntgrid
          ile = idx (le_lo, ig, ik, it, is)
          if (idx_local(g_lo,iglo)) &
! TT: add 2 instead of 1 since both signs move in the same way
               nn_from(proc_id(le_lo,ile)) = nn_from(proc_id(le_lo,ile)) + 2
          if (idx_local(le_lo,ile)) &
               nn_to(proc_id(g_lo,iglo)) = nn_to(proc_id(g_lo,iglo)) + 2
       end do
    end do

    do ip = 0, nproc-1
       if (nn_from(ip) > 0) then
          allocate (from_list(ip)%first (nn_from(ip))) !!! RN> unable to account memory for structure variables
          allocate (from_list(ip)%second(nn_from(ip))) !!! RN> unable to account memory for structure variables
          allocate (from_list(ip)%third (nn_from(ip))) !!! RN> unable to account memory for structure variables
       end if
       if (nn_to(ip) > 0) then
          allocate (to_list(ip)%first (nn_to(ip))) !!! RN> unable to account memory for structure variables
          allocate (to_list(ip)%second(nn_to(ip))) !!! RN> unable to account memory for structure variables
          allocate (to_list(ip)%third (nn_to(ip))) !!! RN> unable to account memory for structure variables
       end if
    end do

    ! get local indices of elements distributed to/from other processors
    nn_to = 0
    nn_from = 0
    do iglo = g_lo%llim_world, g_lo%ulim_world
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       do isign = 1, 2
          if (isign==2) il = 2*g_lo%nlambda+1 - il
          do ig = -ntgrid, ntgrid
             ile = idx (le_lo, ig, ik, it, is)
             if (idx_local(g_lo,iglo)) then
                ip = proc_id(le_lo,ile)
                n = nn_from(ip) + 1
                nn_from(ip) = n
                from_list(ip)%first(n)  = ig
                from_list(ip)%second(n) = isign
                from_list(ip)%third(n)  = iglo
             end if
             if (idx_local(le_lo,ile)) then
                ip = proc_id(g_lo,iglo)
                n = nn_to(ip) + 1
                nn_to(ip) = n
                to_list(ip)%first(n)  = il
                to_list(ip)%second(n) = ie
                to_list(ip)%third(n)  = ile
             end if
          end do
       end do
    end do

    from_low (1) = -ntgrid
    from_low (2) = 1
    from_low (3) = g_lo%llim_proc

    from_high(1) = ntgrid
    from_high(2) = 2
    from_high(3) = g_lo%ulim_alloc

    to_low = le_lo%llim_proc

    to_high(1) = 2*nlambda+1
    to_high(2) = negrid + 1  ! TT: just followed convention with +1.
    ! TT: It may be good to avoid bank conflict.
    to_high(3) = le_lo%ulim_alloc

    call init_redist (g2le, 'c', to_low, to_high, to_list, &
         from_low, from_high, from_list)

    call delete_list (to_list)
    call delete_list (from_list)

    done = .true.

  end subroutine init_g2le_redistribute

  subroutine init_lz2ed_redistribute

    use mp, only: nproc
    use species, only: nspec
    use theta_grid, only: ntgrid
    use kgrids, only: naky, nakx
    use le_grids, only: nlambda, negrid
    use agk_layouts, only: init_lorentz_layouts, init_ediffuse_layouts
    use agk_layouts, only: lz_lo, e_lo
    use agk_layouts, only: idx_local, proc_id
    use agk_layouts, only: ig_idx, ik_idx, it_idx, ie_idx, is_idx, idx
    use redistribute, only: index_list_type, init_redist, delete_list
    implicit none
    type (index_list_type), dimension (0:nproc-1) :: to_list, from_list
    integer, dimension (0:nproc-1) :: nn_to, nn_from
    integer, dimension (2) :: from_low, from_high
    integer, dimension (2) :: to_high
    integer :: to_low
    integer :: ig, isign, ilz, il, ielo, ixi, nxi
    integer :: ik, it, ie, is
    integer :: n, ip
    logical :: done = .false.

    if (done) return

    call init_lorentz_layouts (ntgrid, naky, nakx, nlambda, negrid, nspec)
    call init_ediffuse_layouts (ntgrid, naky, nakx, nlambda, nspec)

    nxi = nlambda*2
    ! count number of elements to be redistributed to/from each processor
    nn_to = 0
    nn_from = 0
    do ilz = lz_lo%llim_world, lz_lo%ulim_world
       ig = ig_idx(lz_lo,ilz)
       ik = ik_idx(lz_lo,ilz)
       it = it_idx(lz_lo,ilz)
       is = is_idx(lz_lo,ilz)
       do ixi = 1, nxi
          il = min(ixi, nxi+1-ixi)
          isign = (ixi - 1) / nlambda + 1
          ielo = idx (e_lo, ig, isign, ik, it, il, is)
          if (idx_local(lz_lo,ilz)) &
               nn_from(proc_id(e_lo,ielo)) = nn_from(proc_id(e_lo,ielo)) + 1
          if (idx_local(e_lo,ielo)) &
               nn_to(proc_id(lz_lo,ilz)) = nn_to(proc_id(lz_lo,ilz)) + 1
       end do
    end do

    do ip = 0, nproc-1
       if (nn_from(ip) > 0) then
          allocate (from_list(ip)%first (nn_from(ip))) !!! RN> unable to account memory for structure variables
          allocate (from_list(ip)%second(nn_from(ip))) !!! RN> unable to account memory for structure variables
       end if
       if (nn_to(ip) > 0) then
          allocate (to_list(ip)%first (nn_to(ip))) !!! RN> unable to account memory for structure variables
          allocate (to_list(ip)%second(nn_to(ip))) !!! RN> unable to account memory for structure variables
       end if
    end do

    ! get local indices of elements distributed to/from other processors
    nn_to = 0
    nn_from = 0
    do ilz = lz_lo%llim_world, lz_lo%ulim_world
       ig = ig_idx(lz_lo,ilz)
       ik = ik_idx(lz_lo,ilz)
       it = it_idx(lz_lo,ilz)
       ie = ie_idx(lz_lo,ilz)
       is = is_idx(lz_lo,ilz)
       do ixi = 1, nxi
          il = min(ixi, nxi+1-ixi)
          isign = (ixi - 1) / nlambda + 1
          ielo = idx (e_lo, ig, isign, ik, it, il, is)
          if (idx_local(lz_lo,ilz)) then
             ip = proc_id(e_lo,ielo)
             n = nn_from(ip) + 1
             nn_from(ip) = n
             from_list(ip)%first(n)  = ixi
             from_list(ip)%second(n) = ilz
          end if
          if (idx_local(e_lo,ielo)) then
             ip = proc_id(lz_lo,ilz)
             n = nn_to(ip) + 1
             nn_to(ip) = n
             to_list(ip)%first(n)  = ie
             to_list(ip)%second(n) = ielo
          end if
       end do
    end do

    from_low (1) = 1
    from_low (2) = lz_lo%llim_proc

    from_high(1) = nxi+1
    from_high(2) = lz_lo%ulim_alloc

    to_low = e_lo%llim_proc

    to_high(1) = negrid + 1  ! TT: just followed convention with +1.
    ! TT: It may be good to avoid bank conflict.
    to_high(2) = e_lo%ulim_alloc

    call init_redist (l2e_map, 'c', to_low, to_high, to_list, &
         from_low, from_high, from_list)

    call delete_list (to_list)
    call delete_list (from_list)

    done = .true.



  end subroutine init_lz2ed_redistribute

  subroutine solfp1 &
       (g, gold, g1, phi, apar, bpar, phinew, aparnew, bparnew, diagnostics)
    use agk_layouts, only: g_lo
    use theta_grid, only: ntgrid
    use run_parameters, only: beta
    use le_grids, only: integrate_moment
    use agk_time, only: dtime
    use species, only: nspec, spec, electron_species
    use dist_fn_arrays, only: c_rate, vpac
    use kgrids, only: kperp2
    use agk_layouts, only: g_lo, it_idx, ik_idx, ie_idx, is_idx
    use constants
    use agk_mem, only: alloc8, dealloc8 
    implicit none
    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (in out) :: g, gold, g1 
    complex, dimension (-ntgrid:,:,:), intent (in) :: phi, apar, bpar
    complex, dimension (-ntgrid:,:,:), intent (in) :: phinew, aparnew, bparnew
    logical, intent (in) :: diagnostics
    logical :: heating_flag_e=.false., heating_flag_i=.false.

    integer :: ig, it, ik, ie, is, iglo
    complex, dimension (:,:,:), allocatable :: gc1, gc2, gc3, gc4, gc5

    if (collision_model_switch == collision_model_none) return

    heating_flag_e = heating_exp .and. diagnostics
    heating_flag_i = heating_imp .and. diagnostics

    if (heating_flag_e) then
       allocate (gc1 (-ntgrid:ntgrid, 2, g_lo%llim_proc:g_lo%ulim_alloc)) ; call alloc8 (c3=gc1, v="gc1")
       allocate (gc2 (-ntgrid:ntgrid, 2, g_lo%llim_proc:g_lo%ulim_alloc)) ; call alloc8 (c3=gc2, v="gc2")
       allocate (gc4 (-ntgrid:ntgrid, 2, g_lo%llim_proc:g_lo%ulim_alloc)) ; call alloc8 (c3=gc4, v="gc4")
       allocate (gc5 (-ntgrid:ntgrid, 2, g_lo%llim_proc:g_lo%ulim_alloc)) ; call alloc8 (c3=gc5, v="gc5")
       gc1 = 0. ; gc2 = 0. ; gc4 = 0. ; gc5 = 0.
    end if
    if (heating_flag_i) then
       allocate (gc3 (-ntgrid:ntgrid, 2, g_lo%llim_proc:g_lo%ulim_alloc)) ; call alloc8 (c3=gc3, v="gc3")
       gc3 = 0.
    end if
!    else
!       allocate (gc1(1,1,1)); call alloc8(c3=gc1,v="gc1"); gc1 = 0.
!       allocate (gc2(1,1,1)); call alloc8(c3=gc2,v="gc2"); gc2 = 0.
!       allocate (gc3(1,1,1)); call alloc8(c3=gc3,v="gc3"); gc3 = 0.
!    end if

!!!RN>
    ! changed 28/07/08 -- MAB
!    if (adjust) call g_adjust (g, phi, bpar, fphi, fbpar)
    ! TT: I wonder if above might be correct since g on the right hand side
    ! is still at the old time step? :TT
!!!    if (adjust) call g_adjust (g, phinew, bparnew, fphi, fbpar)
!!!    if (adjust) call g_adjust (gold, phi, bpar, fphi, fbpar)
!!!<RN

    if (heating_flag_i) gc3 = g

    select case (collision_model_switch)

    case (collision_model_full)

       if (resistivity .and. beta > epsilon(0.0) .and. conserve_moments) &
            call ion_drag_Apar

       if (heating_flag_e) then
!          gc3 = g
          call solfp_lorentz (g, gc1, gc2)
       else
!          call solfp_lorentz (g, gc1, gc2)
          call solfp_lorentz (g)
       end if
       if (conserve_moments) call conserve_lorentz (g)

       if (heating_flag_e) then
          call solfp_ediffuse (g, gc4, gc5)
       else
          call solfp_ediffuse (g)
       end if
       if (conserve_moments) call conserve_diffuse (g)

    case (collision_model_lorentz, collision_model_lorentz_test)

       if (resistivity .and. beta > epsilon(0.0) .and. conserve_moments) &
            call ion_drag_Apar

       if (heating_flag_e) then
!          gc3 = g
          call solfp_lorentz (g, gc1, gc2)
       else
!          call solfp_lorentz (g, gc1, gc2)
          call solfp_lorentz (g)
       end if

       if (conserve_moments) call conserve_lorentz (g)

    case (collision_model_ediffuse)

       ! added 5.16.08 -- MAB
       if (heating_flag_e) then
          call solfp_ediffuse (g, gc4, gc5)
       else
          call solfp_ediffuse (g)
       end if
       if (conserve_moments) call conserve_diffuse (g)

    end select

    if (heating_flag_e) then
       gc1=gc1+gc4
       call integrate_moment (gc1, c_rate(:,:,:,:,1))

       gc2=gc2+gc5
       if (hyper_colls) call integrate_moment (gc2, c_rate(:,:,:,:,2))
    end if

    if (heating_flag_i) then
       ! form (h_i+1 + h_i)/2 * C(h_i+1) and integrate.
       gc3 = 0.5*conjg(g+gold)*(g-gc3)/dtime(1)

       call integrate_moment (gc3, c_rate(:,:,:,:,3))
    end if

    if (heating_flag_e) then
       call dealloc8 (c3=gc1, v="gc1") 
       call dealloc8 (c3=gc2, v="gc2") 
       call dealloc8 (c3=gc4, v="gc4") 
       call dealloc8 (c3=gc5, v="gc5")
    end if
    if (heating_flag_i) then   
       call dealloc8 (c3=gc3, v="gc3") 
    endif

!!!RN>
    ! changed 28/07/08 -- MAB
!    if (adjust) call g_adjust (g, phi, bpar, -fphi, -fbpar)
    ! TT: and now g is at the new time step so we should definitely use below
!!!    if (adjust) call g_adjust (g, phinew, bparnew, -fphi, -fbpar)
!!!    if (adjust) call g_adjust (gold, phi, bpar, -fphi, -fbpar)
!!!<RN

  contains

    subroutine ion_drag_Apar
      use run_parameters, only: ieqzip
      use dist_fn_arrays, only: aj0
      use le_grids, only: e, negrid
      integer :: is_ion
      real, allocatable :: vns(:,:)
      allocate(vns(negrid,nspec)); call alloc8(r2=vns,v="vns")
      vns(:,:)=0.
      do is=1,nspec
         if(resistivity_test) then
            vns(:,is)=spec(is)%nu
         else
            vns(:,is)=spec(is)%nu / e(:,is)**1.5
         endif
      end do

      do iglo = g_lo%llim_proc, g_lo%ulim_proc
         is = is_idx(g_lo,iglo)
         if (spec(is)%type /= electron_species) cycle
         !! this assumes two species
         is_ion=3-is ! (e=1 i=2) or (e=2 i=1)
         it = it_idx(g_lo,iglo)
         ik = ik_idx(g_lo,iglo)
         ie = ie_idx(g_lo,iglo)
         do ig = -ntgrid, ntgrid
            g(ig,:,iglo) = g(ig,:,iglo) + ieqzip(it,ik) * ( &
                 vpac(:,iglo) * vns(ie,is) &
                 * dtime(1) * aj0(iglo) * kperp2(it,ik) * aparnew(ig,it,ik) &
                 / (beta * spec(is_ion)%z * spec(is)%stm * spec(is_ion)%dens) &
                 )
         end do
      end do
!      deallocate(vns)
      call dealloc8(r2=vns,v="vns")
    end subroutine ion_drag_Apar

  end subroutine solfp1

  subroutine conserve_lorentz (g)
    use run_parameters, only: ieqzip
    use theta_grid, only: ntgrid
    use species, only: nspec
    use kgrids, only: naky, nakx
    use agk_layouts, only: g_lo, ik_idx, it_idx, ie_idx, is_idx, il_idx
    use le_grids, only: integrate_moment, e
    use agk_mem, only: alloc8, dealloc8
# ifdef USE_LE_LAYOUT
    use le_grids, only: nlambda, negrid, e, al
    use agk_layouts, only: le_lo, ig_idx
    use redistribute, only: measure_scatter
# elif USE_L2E_MAP
    use le_grids, only: nlambda, e, al
    use agk_layouts, only: lz_lo, ig_idx
    use redistribute, only: measure_gather, measure_scatter
# else
    use dist_fn_arrays, only: aj0, aj1vp2, vpa
# endif

    implicit none
    
    ! MAB: should be able to remove g1 now, 2/3/09
    ! RNU g1 removed 6/22/16
    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (in out) :: g
    complex, dimension (:,:,:,:), allocatable :: v0y0, v1y1, v2y2

# ifdef USE_LE_LAYOUT
    complex, dimension (:,:,:), allocatable :: gtmp
    real, dimension (:,:,:), allocatable :: vpanud
# elif USE_L2E_MAP
    complex, dimension (:,:), allocatable :: gtmp
# else
    complex, dimension (:,:,:), allocatable :: gtmp
# endif

    integer :: isgn, ik, ie, il, is, it
# ifdef USE_LE_LAYOUT
    integer :: ig, ile, ixi, nxi
# elif USE_L2E_MAP
    integer :: ig, ilz, ixi, nxi
    integer :: all = 1
# else
    integer :: iglo
    integer :: all = 1
# endif

! MAB> changes made 2/3/09

    allocate (v0y0 (-ntgrid:ntgrid, nakx, naky, nspec)) ; call alloc8 (c4=v0y0, v="v0y0")
    allocate (v1y1 (-ntgrid:ntgrid, nakx, naky, nspec)) ; call alloc8 (c4=v1y1, v="v1y1")
    allocate (v2y2 (-ntgrid:ntgrid, nakx, naky, nspec)) ; call alloc8 (c4=v2y2, v="v2y2")

# ifdef USE_LE_LAYOUT

    nxi = nlambda*2
    ! Let's work on gle directly instead of g for the moment
    allocate (gtmp(nxi+1, negrid+1, le_lo%llim_proc:le_lo%ulim_alloc)) ; call alloc8 (c3=gtmp, v="gtmp")
    allocate (vpanud(nxi+1, negrid+1, nspec)); call alloc8(r3=vpanud,v="vpanud")

    if (resistivity) then

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! First get v0y0

       isgn = 1
       do is = 1, nspec
          do ie = 1, negrid
             do ixi = 1, nxi
                il = min(ixi, nxi+1-ixi)
                vpanud(ixi,ie,is) = sign(isgn, nlambda-ixi)*sqrt((1.0-al(il))*e(ie,is))
             end do
          end do
       end do
       
       do ile = le_lo%llim_proc, le_lo%ulim_proc
          is = is_idx(le_lo,ile)
          gtmp(:,:,ile) = vpanud(:,:,is) * aj0le(:,:,ile) * gle(:,:,ile)
!          do ie=1, negrid
!             do ixi=1, nxi
!                il = min(ixi, nxi+1-ixi)
!                gtmp(ixi,ie,ile) = (-1)**((ixi-1)/nlambda) &
!                     * sqrt((1.0-al(il))*e(ie,1)) * aj0le(ixi,ie,ile) &
!                     * gle(ixi,ie,ile)
!             end do
!          end do
       end do
       call integrate_moment (le_lo, gtmp, v0y0)    ! v0y0

       ! add part of ion-drag term
       do ile = le_lo%llim_proc, le_lo%ulim_proc
          ig = ig_idx(le_lo,ile)
          ik = ik_idx(le_lo,ile)
          it = it_idx(le_lo,ile)
          is = is_idx(le_lo,ile)
          gle(:,:,ile) = gle(:,:,ile) - ieqzip(it,ik) * &
               z0le(:,:,ile) * v0y0(ig,it,ik,is)
       end do

    end if

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Now get v1y1

!     if (conservative) then
!        do ile = le_lo%llim_proc, le_lo%ulim_proc
!           is = is_idx(le_lo,ile)
!           do ie=1, negrid
!              do ixi=1, nxi
!                 il = min(ixi, nxi+1-ixi)
!                 isgn = 2 - il/ixi  ! isgn = 1 for il = ixi and isgn = 2 for il < ixi
!                 gtmp(ixi,ie,ile) = vnew_D(ie,is) * (-1)**((ixi-1)/nlambda) &
!                      * vpdiff(isgn,il) * sqrt(e(ie,is)) * aj0le(ixi,ie,ile) &
!                      * gle(ixi,ie,ile)
!              end do
!           end do
!        end do
!     else
!        do ile = le_lo%llim_proc, le_lo%ulim_proc
!           is = is_idx(le_lo,ile)
!           do ie=1, negrid
!              do ixi=1, nxi
!                 il = min(ixi, nxi+1-ixi)
!                 gtmp(ixi,ie,ile) = vnew_D(ie,is) * (-1)**((ixi-1)/nlambda) &
!                      * sqrt((1.0-al(il))*e(ie,is)) * aj0le(ixi,ie,ile) &
!                      * gle(ixi,ie,ile)
!              end do
!           end do
!        end do
!     end if

    if (conservative) then
       do is = 1, nspec
          do ie = 1, negrid
             do ixi = 1, nxi
                il = min(ixi, nxi+1-ixi)
                isgn = 2 - il/ixi
                vpanud(ixi,ie,is) = vpdiff(isgn,il) * sqrt(e(ie,is)) * vnew_D(ie,is)
             end do
          end do
       end do
    else
       isgn = 1
       do is = 1, nspec
          do ie = 1, negrid
             do ixi = 1, nxi
                il = min(ixi, nxi+1-ixi)
                vpanud(ixi,ie,is) = sign(isgn, nlambda-ixi)*sqrt((1.0-al(il))*e(ie,is))*vnew_D(ie,is)
             end do
          end do
       end do
    end if

    do ile = le_lo%llim_proc, le_lo%ulim_proc
       is = is_idx(le_lo,ile)
       gtmp(:,:,ile) = vpanud(:,:,is) * aj0le(:,:,ile) &
            * gle(:,:,ile)
    end do
    call integrate_moment (le_lo, gtmp, v1y1)    ! v1y1

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Get y2 = y1 - v1y1 * s1 / (1 + v1s1)

    do ile = le_lo%llim_proc, le_lo%ulim_proc
       it = it_idx(le_lo,ile)
       ik = ik_idx(le_lo,ile)
       ig = ig_idx(le_lo,ile)
       is = is_idx(le_lo,ile)
       gle(:,:,ile) = gle(:,:,ile) - ieqzip(it,ik) &
            * s0le(:,:,ile) * v1y1(ig,it,ik,is)
    end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Now get v2y2

    do ile = le_lo%llim_proc, le_lo%ulim_proc
       is = is_idx(le_lo,ile)
       do ie=1, negrid
          ! aj1vp2 = 2 * J1(arg)/arg * vperp^2
          gtmp(:,ie,ile) = 0.5 * vnew_D(ie,is) * aj1vp2le(:,ie,ile) &
               * gle(:,ie,ile)
       end do
    end do
    call integrate_moment (le_lo, gtmp, v2y2)    ! v2y2

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Finally get x = y2 - v2y2 * w2 / (1 + v2w2)

    do ile = le_lo%llim_proc, le_lo%ulim_proc
       it = it_idx(le_lo,ile)
       ik = ik_idx(le_lo,ile)
       ig = ig_idx(le_lo,ile)
       is = is_idx(le_lo,ile)
       gle(:,:,ile) = gle(:,:,ile) - ieqzip(it,ik) &
            * w0le(:,:,ile) * v2y2(ig,it,ik,is)
    end do

!    deallocate (vpanud)
    call dealloc8(r3=vpanud,v="vpanud")

# elif USE_L2E_MAP

    nxi = nlambda*2
    ! calculate in Lorentz layout
    allocate (gtmp(nxi+1, lz_lo%llim_proc:lz_lo%ulim_alloc)) ; call alloc8 (c2=gtmp, v="gtmp")

    if (resistivity) then

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! First get v0y0

       do ilz = lz_lo%llim_proc, lz_lo%ulim_proc
          ie = ie_idx(lz_lo,ilz)
          is = is_idx(lz_lo,ilz)
          do ixi=1, nxi
             il = min(ixi, nxi+1-ixi)
             gtmp(ixi,ilz) = (-1)**((ixi-1)/nlambda) &
                  * sqrt((1.0-al(il))*e(ie,is)) * aj0lz(ixi,ilz) &
                  * glz(ixi,ilz)
          end do
       end do
       call integrate_moment (lz_lo, gtmp, v0y0)    ! v0y0
       ! add part of ion-drag term
       do ilz = lz_lo%llim_proc, lz_lo%ulim_proc
          ig = ig_idx(lz_lo,ilz)
          ik = ik_idx(lz_lo,ilz)
          it = it_idx(lz_lo,ilz)
          is = is_idx(lz_lo,ilz)
          glz(:,ilz) = glz(:,ilz) - ieqzip(it,ik) * &
               z0lz(:,ilz) * v0y0(ig,it,ik,is)
       end do

    end if

    if (conservative) then
       do ilz = lz_lo%llim_proc, lz_lo%ulim_proc
          ie = ie_idx(lz_lo,ilz)
          is = is_idx(lz_lo,ilz)
          do ixi = 1, nxi
             il = min(ixi, nxi+1-ixi)
             isgn = 2 - il/ixi  ! isgn = 1 for il = ixi and isgn = 2 for il < ixi
!             gtmp(ixi,ilz) = vnew_D(ie,is) * (-1)**((ixi-1)/nlambda) &
             gtmp(ixi,ilz) = vnew_D(ie,is) &
                  * vpdiff(isgn,il) * sqrt(e(ie,is)) * aj0lz(ixi,ilz) * glz(ixi,ilz)
          end do
       end do
    else
       do ilz = lz_lo%llim_proc, lz_lo%ulim_proc
          ie = ie_idx(lz_lo,ilz)
          is = is_idx(lz_lo,ilz)
          do ixi = 1, nxi
             il = min(ixi, nxi+1-ixi)
             gtmp(ixi,ilz) = vnew_D(ie,is) * (-1)**((ixi-1)/nlambda) &
                  * sqrt((1.0-al(il))*e(ie,is)) * aj0lz(ixi,ilz) * glz(ixi,ilz)
          end do
       end do
    end if

    call integrate_moment (lz_lo, gtmp, v1y1, all)    ! v1y1

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Get y2 = y1 - v1y1 * s1 / (1 + v1s1)

    do ilz = lz_lo%llim_proc, lz_lo%ulim_proc
       it = it_idx(lz_lo,ilz)
       ik = ik_idx(lz_lo,ilz)
       ig = ig_idx(lz_lo,ilz)
       is = is_idx(lz_lo,ilz)
       glz(:,ilz) = glz(:,ilz) - ieqzip(it,ik) &
            * s0lz(:,ilz) * v1y1(ig,it,ik,is)
    end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Now get v2y2

    do ilz = lz_lo%llim_proc, lz_lo%ulim_proc
       ie = ie_idx(lz_lo,ilz)
       is = is_idx(lz_lo,ilz)
       ! aj1vp2 = 2 * J1(arg)/arg * vperp^2
       gtmp(:,ilz) = 0.5 * vnew_D(ie,is) * aj1vp2lz(:,ilz) * glz(:,ilz)
    end do
    call integrate_moment (lz_lo, gtmp, v2y2, all)    ! v2y2

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Finally get x = y2 - v2y2 * w2 / (1 + v2w2)

    do ilz = lz_lo%llim_proc, lz_lo%ulim_proc
       it = it_idx(lz_lo,ilz)
       ik = ik_idx(lz_lo,ilz)
       ig = ig_idx(lz_lo,ilz)
       is = is_idx(lz_lo,ilz)
       glz(:,ilz) = glz(:,ilz) - ieqzip(it,ik) &
            * w0lz(:,ilz) * v2y2(ig,it,ik,is)
    end do

# else

    allocate (gtmp(-ntgrid:ntgrid, 2, g_lo%llim_proc:g_lo%ulim_alloc)) ; call alloc8 (c3=gtmp, v="gtmp")

    if (resistivity) then

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! First get v0y0

       do iglo = g_lo%llim_proc, g_lo%ulim_proc
          do isgn = 1, 2
             ! v0 = vpa J0 f_0
             ! No factor of sqrt(T/m) here on purpose (see derivation) 
             gtmp(:,isgn,iglo) = vpa(isgn,iglo) * aj0(iglo) * g(:,isgn,iglo)
          end do
       end do
       call integrate_moment (gtmp, v0y0, all)    ! v0y0

       ! add part of ion-drag term
       ! y1 = y0 - v0y0 * z0 / (1 + v0z0)       
       do iglo = g_lo%llim_proc, g_lo%ulim_proc
          ik = ik_idx(g_lo,iglo)
          it = it_idx(g_lo,iglo)
          is = is_idx(g_lo,iglo)
          do isgn=1,2
             g(:,isgn,iglo) = g(:,isgn,iglo) - ieqzip(it,ik) * &
                  z0(isgn,iglo) * v0y0(:,it,ik,is)
          end do
       end do

    end if

    if (conservative) then
       do iglo = g_lo%llim_proc, g_lo%ulim_proc
          ie = ie_idx(g_lo,iglo)
          is = is_idx(g_lo,iglo)
          il = il_idx(g_lo,iglo)
          do isgn = 1, 2
             gtmp(:,isgn,iglo) = vnew_D(ie,is) * vpdiff(isgn,il)*sqrt(e(ie,is)) &
                  * aj0(iglo) * g(:,isgn,iglo)
          end do
       end do
    else
       do iglo = g_lo%llim_proc, g_lo%ulim_proc
          ie = ie_idx(g_lo,iglo)
          is = is_idx(g_lo,iglo)
          do isgn = 1, 2
             gtmp(:,isgn,iglo) = vnew_D(ie,is) * vpa(isgn,iglo) * aj0(iglo) &
                  * g(:,isgn,iglo)
          end do
       end do
    end if

    call integrate_moment (gtmp, v1y1, all)    ! v1y1

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Get y2 = y1 - v1y1 * s1 / (1 + v1s1)

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       do isgn=1,2
          g(:,isgn,iglo) = g(:,isgn,iglo) - ieqzip(it,ik) &
               * s0(isgn,iglo)*v1y1(:,it,ik,is)
       end do
    end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Now get v2y2

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       ! aj1vp2 = 2 * J1(arg)/arg * vperp^2
       gtmp(:,:,iglo) = 0.5 * vnew_D(ie,is) * aj1vp2(iglo) * g(:,:,iglo)
    end do

    call integrate_moment (gtmp, v2y2, all)    ! v2y2

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       do isgn=1,2
          g(:,isgn,iglo) = g(:,isgn,iglo) - ieqzip(it,ik) &
               * w0(isgn,iglo)*v2y2(:,it,ik,is)
       end do
    end do

# endif

! <MAB
    
    call dealloc8 (c4=v0y0, v="v0y0") 
    call dealloc8 (c4=v1y1, v="v1y1") 
    call dealloc8 (c4=v2y2, v="v2y2") 
# ifdef USE_L2E_MAP
    call dealloc8 (c2=gtmp, v="gtmp") 
# else
    call dealloc8 (c3=gtmp, v="gtmp") 
# endif

# ifdef USE_LE_LAYOUT
    if (collision_model_switch == collision_model_lorentz) &
       call measure_scatter (g2le, gle, g)
# elif USE_L2E_MAP
    if (collision_model_switch == collision_model_lorentz) &
         call measure_scatter (lorentz_map, glz, g)
# endif

  end subroutine conserve_lorentz

  subroutine conserve_diffuse (g)
    use run_parameters, only: ieqzip
    use theta_grid, only: ntgrid
    use species, only: nspec
    use kgrids, only: naky, nakx
    use agk_layouts, only: g_lo, ik_idx, it_idx, ie_idx, is_idx
    use le_grids, only: integrate_moment
    use agk_mem, only: alloc8, dealloc8
# ifdef USE_LE_LAYOUT
    use le_grids, only: al, e
    use le_grids, only: nlambda, negrid
    use agk_layouts, only: le_lo, ig_idx
    use redistribute, only: measure_scatter
# else
    use dist_fn_arrays, only: aj0, aj1vp2, vpa
# endif
    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (in out) :: g
!    complex, dimension (:,:,:,:), allocatable :: v0y0, v1y0, v2y0
    complex, dimension (:,:,:,:), allocatable :: v0y0, v1y1, v2y2    ! MAB: 2/7/09

    integer :: isgn, ik, ie, is, it
    complex, dimension (:,:,:), allocatable :: gtmp                  ! MAB: 2/7/09
# ifdef USE_LE_LAYOUT
    integer :: ig, ile, ixi, nxi, il
    real, dimension (:,:,:), allocatable :: vpadelnu
!    complex, dimension (:,:,:), allocatable :: gtmp
# else
    integer :: iglo
    integer :: all = 1
# endif

    allocate (v0y0(-ntgrid:ntgrid, nakx, naky, nspec)) ; call alloc8 (c4=v0y0, v="v0y0")
    allocate (v1y1(-ntgrid:ntgrid, nakx, naky, nspec)) ; call alloc8 (c4=v1y1, v="v1y1")
    allocate (v2y2(-ntgrid:ntgrid, nakx, naky, nspec)) ; call alloc8 (c4=v2y2, v="v2y2")

#ifdef USE_LE_LAYOUT
    nxi = nlambda*2
    allocate (gtmp(nxi+1, negrid+1, le_lo%llim_proc:le_lo%ulim_alloc)) ; call alloc8 (c3=gtmp, v="gtmp")
    allocate (vpadelnu(nxi+1, negrid+1, nspec)); call alloc8(r3=vpadelnu,v="vpadelnu")

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! First get v0y0

    do ile = le_lo%llim_proc, le_lo%ulim_proc
       is = is_idx(le_lo,ile)
       do ie=1, negrid
          gtmp(:,ie,ile) = vnew_E(ie,is)*aj0le(:,ie,ile)*gle(:,ie,ile)
       end do
    end do

    call integrate_moment (le_lo, gtmp, v0y0)    ! v0y0

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Get y1 = y0 - v0y0 * z0 / (1 + v0z0)

    do ile = le_lo%llim_proc, le_lo%ulim_proc
       ig = ig_idx(le_lo,ile)
       it = it_idx(le_lo,ile)
       ik = ik_idx(le_lo,ile)
       is = is_idx(le_lo,ile)
       gle(:,:,ile) = gle(:,:,ile) - ieqzip(it,ik) &
            * v0y0(ig,it,ik,is)*bz0le(:,:,ile)
    end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Now get v1y1

!     do ile = le_lo%llim_proc, le_lo%ulim_proc
!        is = is_idx(le_lo,ile)
!        do ie=1, negrid
!           do ixi=1, nxi
!              il = min(ixi, nxi+1-ixi)
!              gtmp (ixi,ie,ile) = delvnew(ie,is) &
!                   * (-1)**((ixi-1)/nlambda) * sqrt( (1.0-al(il))*e(ie,is) ) &
!                   * aj0le(ixi,ie,ile) * gle(ixi,ie,ile)
!           end do
!        end do
!     end do

    isgn = 1
    do is = 1, nspec
       do ie = 1, negrid
          do ixi = 1, nxi
             il = min(ixi, nxi+1-ixi)
             vpadelnu(ixi,ie,is) = sign(isgn, nlambda-ixi) * delvnew(ie,is) * sqrt( (1.0-al(il))*e(ie,is) )
          end do
       end do
    end do

    do ile = le_lo%llim_proc, le_lo%ulim_proc
       is = is_idx(le_lo,ile)
       gtmp (:,:,ile) = vpadelnu(:,:,is) * aj0le(:,:,ile) * gle(:,:,ile)
    end do
    
    call integrate_moment (le_lo, gtmp, v1y1)    ! v1y1

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Get y2 = y1 - v1y1 * s1 / (1 + v1s1)

    do ile = le_lo%llim_proc, le_lo%ulim_proc
       it = it_idx(le_lo,ile)
       ik = ik_idx(le_lo,ile)
       ig = ig_idx(le_lo,ile)
       is = is_idx(le_lo,ile)
       gle(:,:,ile) = gle(:,:,ile) - ieqzip(it,ik) &
            * bs0le(:,:,ile) * v1y1(ig,it,ik,is)
    end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Now get v2y2

    do ile = le_lo%llim_proc, le_lo%ulim_proc
       is = is_idx(le_lo,ile)
       do ie=1, negrid
          gtmp(:,ie,ile) = 0.5 * delvnew(ie,is) &
               * aj1vp2le(:,ie,ile) * gle(:,ie,ile)
       end do
    end do

    call integrate_moment (le_lo, gtmp, v2y2)    ! v2y2

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Finally get x = y2 - v2y2 * w2 / (1 + v2w2)

    do ile = le_lo%llim_proc, le_lo%ulim_proc
       it = it_idx(le_lo,ile)
       ik = ik_idx(le_lo,ile)
       ig = ig_idx(le_lo,ile)
       is = is_idx(le_lo,ile)
       gle(:,:,ile) = gle(:,:,ile) - ieqzip(it,ik) &
            * bw0le(:,:,ile) * v2y2(ig,it,ik,is)
    end do

     call measure_scatter (g2le, gle, g)

!    deallocate (vpadelnu)
     call dealloc8(r3=vpadelnu,v="vpadelnu")

#else

    allocate (gtmp(-ntgrid:ntgrid,2,g_lo%llim_proc:g_lo%ulim_alloc)) ; call alloc8 (c3=gtmp, v="gtmp")

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! First get v0y0

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       gtmp(:,:,iglo) = vnew_E(ie,is) * aj0(iglo) * g(:,:,iglo)
    end do

    call integrate_moment (gtmp, v0y0, all)    ! v0y0

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Get y1 = y0 - v0y0 * z0 / (1 + v0z0)

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       it = it_idx(g_lo,iglo)
       ik = ik_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       do isgn = 1, 2
          g(:,isgn,iglo) = g(:,isgn,iglo) - &
               & ieqzip(it,ik) * bz0(isgn,iglo)*v0y0(:,it,ik,is)
       end do
    end do
    
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Now get v1y1

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       gtmp (:,:,iglo) = delvnew(ie,is) &
            * spread(vpa(:,iglo), 1, ntgrid*2+1) * aj0(iglo) &
            * g(:,:,iglo)
    end do

    call integrate_moment (gtmp, v1y1, all)    ! v1y1

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Get y2 = y1 - v1y1 * s1 / (1 + v1s1)

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       it = it_idx(g_lo,iglo)
       ik = ik_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       do isgn = 1, 2
          g(:,isgn,iglo) = g(:,isgn,iglo) - &
               & ieqzip(it,ik) * bs0(isgn,iglo)*v1y1(:,it,ik,is)
       end do
    end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Now get v2y2

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       gtmp(:,:,iglo) = 0.5 * delvnew(ie,is) &
            * aj1vp2(iglo) * g(:,:,iglo)
    end do

    call integrate_moment (gtmp, v2y2, all)    ! v2y2

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Finally get x = y2 - v2y2 * w2 / (1 + v2w2)

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       it = it_idx(g_lo,iglo)
       ik = ik_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       do isgn = 1, 2
          g(:,isgn,iglo) = g(:,isgn,iglo) - & 
               & ieqzip(it,ik) * bw0(isgn,iglo)*v2y2(:,it,ik,is)
       end do
    end do

#endif

    call dealloc8 (c3=gtmp, v="gtmp") 
    call dealloc8 (c4=v0y0, v="v0y0") 
    call dealloc8 (c4=v1y1, v="v1y1") 
    call dealloc8 (c4=v2y2, v="v2y2") 

  end subroutine conserve_diffuse

  subroutine solfp_lorentz (g, gc, gh, init)
    use run_parameters, only: ieqzip
    use species, only: electron_species
    use theta_grid, only: ntgrid
    use le_grids, only: nlambda
    use agk_layouts, only: g_lo, is_idx, ik_idx, it_idx
    use redistribute, only: measure_gather, measure_scatter, gather, scatter
# ifdef USE_LE_LAYOUT
    use le_grids, only: negrid
    use agk_layouts, only: le_lo
# else
    use agk_layouts, only: lz_lo
# endif
    implicit none
    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (in out) :: g
    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (out), optional :: gc, gh
    logical :: heating_flag
    logical, optional, intent (in) :: init

    complex, dimension (2*nlambda+1) :: delta
    complex :: fac
    integer :: il, is, je

# ifdef USE_LE_LAYOUT
    integer :: ie, ile
# else
    integer :: ilz
# endif
    
    heating_flag=.false.
    if (heating_exp .and. (present(gc) .or. present(gh))) heating_flag = .true.

# ifdef USE_LE_LAYOUT
    if (present(init)) then
       call gather (g2le, g, gle)
    else
       call measure_gather (g2le, g, gle)
    end if
# else
    if (present(init)) then
       call gather (lorentz_map, g, glz)
    else
       call measure_gather (lorentz_map, g, glz)
    end if
# endif

    ! saves glz/gle before coll. op.
    if (heating_flag) &
# ifdef USE_LE_LAYOUT         
         gled=gle
# else
         glzd=glz
# endif
    
    ! solve for glz row by row
    je = 2*nlambda+1

# ifdef USE_LE_LAYOUT

    do ile = le_lo%llim_proc, le_lo%ulim_proc
       is = is_idx(le_lo,ile)
       if (abs(vnew(1,is)) < 2.0*epsilon(0.0)) cycle
       if (ieqzip(it_idx(le_lo,ile),ik_idx(le_lo,ile))==0) cycle

       do ie=1, negrid
          gle(je:,ie,ile) = 0.0
          ! right and left sweeps for tridiagonal solve:
          delta(1) = gle(1,ie,ile)
          do il = 1, je-1
             delta(il+1) = gle(il+1,ie,ile) - qle(il+1,ie,ile) * delta(il)
          end do
          gle(je,ie,ile) = delta(je)*betaale(je,ie,ile)
          do il = je-1, 1, -1
             gle(il,ie,ile) = (delta(il) - c1le(il,ie,ile)*gle(il+1,ie,ile)) &
                  * betaale(il,ie,ile)
          end do
       end do
    end do

    if (present(init)) then
       call scatter (g2le, gle, g)
    else if (collision_model_switch == collision_model_lorentz .and. &
         (.not.conserve_moments) ) then
       call measure_scatter (g2le, gle, g)
    end if

# else

    do ilz = lz_lo%llim_proc, lz_lo%ulim_proc
       is = is_idx(lz_lo,ilz)
       if (abs(vnew(1,is)) < 2.0*epsilon(0.0)) cycle
       if (ieqzip(it_idx(lz_lo,ilz),ik_idx(lz_lo,ilz))==0) cycle

       glz(je:,ilz) = 0.0
       ! right and left sweeps for tridiagonal solve:
       delta(1) = glz(1,ilz)
       do il = 1, je-1
          delta(il+1) = glz(il+1,ilz) - ql(il+1,ilz)*delta(il)
       end do
       glz(je,ilz) = delta(je)*betaa(je,ilz)
       do il = je-1, 1, -1
          glz(il,ilz) = (delta(il) - c1(il,ilz)*glz(il+1,ilz))*betaa(il,ilz)
       end do
    end do

    if (present(init)) then
       call scatter (lorentz_map, glz, g)
# ifdef USE_L2E_MAP
    else if ( (.not.conserve_moments) .and. &
         collision_model_switch == collision_model_lorentz ) then
# else
    else
# endif
       call measure_scatter (lorentz_map, glz, g)
    end if
# endif

    if (heating_flag) then
       je = 2*nlambda
# ifdef USE_LE_LAYOUT
       ! uses gle after coll. op.
       if (heating_diag_switch == heating_diag_after_coll) gled=gle

       do ile = le_lo%llim_proc, le_lo%ulim_proc
          ! makes symmetric w.r.t. xi=0, reduces xi=1 boundary effect
          do ie=1, negrid
             do il = 1, nlambda
                fac = gled(il+1,ie,ile)-gled(il,ie,ile)
                glec(il,ie,ile) = conjg(fac)*fac*d1le(il,ie,ile)
                fac = gled(2*nlambda+1-il,ie,ile)-gled(2*nlambda-il,ie,ile)
                glec(2*nlambda+1-il,ie,ile) = conjg(fac)*fac*d1le(2*nlambda+1-il,ie,ile)
             end do
          end do
       end do
       glec = glec + cabs(gled)**2*e1le

# ifdef HEATING_DIAG_OLD
       do ile = le_lo%llim_proc, le_lo%ulim_proc
          do ie=1, negrid
             do il = 1, je-1
                fac = gled(il+1,ie,ile)-gled(il,ie,ile)
                glec(il,ie,ile) = conjg(fac)*fac*d1le(il,ie,ile)  ! d1 accounts for hC(h) entropy
             end do
          end do
       end do
# endif

       call measure_scatter (g2le, glec, gc)
# else
       ! uses glz after coll. op.
       if (heating_diag_switch == heating_diag_after_coll) glzd=glz

       do ilz = lz_lo%llim_proc, lz_lo%ulim_proc
          ! makes symmetric w.r.t. xi=0, reduces xi=1 boundary effect
          do il = 1, nlambda
             fac = glzd(il+1,ilz)-glzd(il,ilz)
             glzc(il,ilz) = conjg(fac)*fac*d1(il,ilz)
             fac = glzd(2*nlambda+1-il,ilz)-glzd(2*nlambda-il,ilz)
             glzc(2*nlambda+1-il,ilz) = conjg(fac)*fac*d1(2*nlambda+1-il,ilz)
          end do
       end do
       glzc = glzc + cabs(glzd)**2*e1

# ifdef HEATING_DIAG_OLD
       do ilz = lz_lo%llim_proc, lz_lo%ulim_proc
          do il = 1, je-1
             fac = glzd(il+1,ilz)-glzd(il,ilz)
             glzc(il,ilz) = conjg(fac)*fac*d1(il,ilz)  ! d1 accounts for hC(h) entropy
          end do
       end do
# endif

       call measure_scatter (lorentz_map, glzc, gc)

# endif

       if (hyper_colls) then
# ifdef USE_LE_LAYOUT

          do ile = le_lo%llim_proc, le_lo%ulim_proc
             ! makes symmetric w.r.t. xi=0, reduces xi=1 boundary effect
             do ie=1, negrid
                do il = 1, nlambda
                   fac = gled(il+1,ie,ile)-gled(il,ie,ile)
                   glec(il,ie,ile) = conjg(fac)*fac*h1le(il,ie,ile)
                   fac = gled(2*nlambda+1-il,ie,ile)-gled(2*nlambda-il,ie,ile)
                   glec(2*nlambda+1-il,ie,ile) = conjg(fac)*fac*h1le(2*nlambda+1-il,ie,ile)
                end do
             end do
          end do

# ifdef HEATING_DIAG_OLD
          do ile = le_lo%llim_proc, le_lo%ulim_proc
             do ie=1, negrid
                do il = 1, je-1
                   fac = gled(il+1,ie,ile)-gled(il,ie,ile)
                   glec(il,ie,ile) = conjg(fac)*fac*h1le(il,ie,ile)
                end do
             end do
          end do
# endif

          call measure_scatter (g2le, glec, gh)
# else

          do ilz = lz_lo%llim_proc, lz_lo%ulim_proc
             ! makes symmetric w.r.t. xi=0, reduces xi=1 boundary effect
             do il = 1, nlambda
                fac = glzd(il+1,ilz)-glzd(il,ilz)
                glzc(il,ilz) = conjg(fac)*fac*h1(il,ilz)
                fac = glzd(2*nlambda+1-il,ilz)-glzd(2*nlambda-il,ilz)
                glzc(2*nlambda+1-il,ilz) = conjg(fac)*fac*h1(2*nlambda+1-il,ilz)
             end do
          end do

# ifdef HEATING_DIAG_OLD
          do ilz = lz_lo%llim_proc, lz_lo%ulim_proc
             do il = 1, je-1
                fac = glzd(il+1,ilz)-glzd(il,ilz)
                glzc(il,ilz) = conjg(fac)*fac*h1(il,ilz)  ! h1 accounts for hH(h) entropy
             end do
          end do
# endif

          call measure_scatter (lorentz_map, glzc, gh)
# endif

       end if
    end if

  end subroutine solfp_lorentz

  subroutine solfp_ediffuse (g, gc, gh, init)
    use run_parameters, only: ieqzip
    use species, only: spec
    use theta_grid, only: ntgrid
    use le_grids, only: negrid
    use redistribute, only: measure_gather, measure_scatter, gather, scatter
    use agk_layouts, only: is_idx, g_lo, it_idx, ik_idx
# ifdef USE_LE_LAYOUT
    use le_grids, only: nlambda
    use agk_layouts, only: le_lo
# else
    use agk_layouts, only: e_lo
# endif
    implicit none

    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (in out) :: g
    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (out), optional :: gc, gh
    logical :: heating_flag
    logical, optional, intent (in) :: init
    integer :: ie, is
# ifdef USE_LE_LAYOUT
    complex, dimension (nlambda*2,negrid) :: delta
    integer :: ile, nxi, il
# else
    complex, dimension (negrid) :: delta
    integer :: ielo
# endif
    complex :: fac

    heating_flag=.false.
    if (heating_exp .and. (present(gc) .or. present(gh))) heating_flag = .true.

# ifdef USE_LE_LAYOUT

    if (present(init)) then
       call gather (g2le, g, gle)
    else if (collision_model_switch == collision_model_ediffuse) then
       call measure_gather (g2le, g, gle)
    end if

    if (heating_flag) gled=gle

    nxi = nlambda*2
    do ile = le_lo%llim_proc, le_lo%ulim_proc
       is = is_idx(le_lo,ile)
       if (spec(is)%nu < 2.0*epsilon(0.0)) cycle
       if (ieqzip(it_idx(le_lo,ile),ik_idx(le_lo,ile))==0) cycle

       delta(:,1) = gle(:nxi,1,ile)
       do ie = 1, negrid-1
          delta(:,ie+1) = gle(:nxi,ie+1,ile) - eqle(:nxi,ie+1,ile) * delta(:,ie)
       end do
       gle(:,negrid+1,ile) = 0.0
       do ie = negrid, 1, -1
          gle(:nxi,ie,ile) = ( delta(:,ie) - ec1le(:nxi,ie,ile) &
               * gle(:nxi,ie+1,ile) ) * ebetaale(:nxi,ie,ile)
       end do
    end do

    if (present(init) .or. .not.conserve_moments) &
         call measure_scatter (g2le, gle, g)

    if (heating_flag) then

       if (heating_diag_switch == heating_diag_after_coll) gled=gle

       do ile = le_lo%llim_proc, le_lo%ulim_proc
          do ie=1, negrid
             do il=1, nxi
                fac = gled(il,ie+1,ile) - gled(il,ie,ile)
                glec(il,ie,ile) = cabs(fac)**2*ed1le(il,ie,ile) + cabs(gled(il,ie,ile))**2*ee1le(il,ie,ile)
             end do
          end do
       end do

       call measure_scatter (g2le, glec, gc)

       ! no hyper_colls at the moment
       if (hyper_colls) then
          gh=0.
       end if

    end if
# else

# ifdef USE_L2E_MAP
    if (present(init)) then
       call gather (ediffuse_map, g, ged)
    else if (collision_model_switch == collision_model_full) then
       call measure_gather (l2e_map, glz, ged)
    else
       call measure_gather (ediffuse_map, g, ged)
    end if
# else
    if (present(init)) then
       call gather (ediffuse_map, g, ged)
    else
       call measure_gather (ediffuse_map, g, ged)
    end if
# endif

    if (heating_flag) gedd=ged

    do ielo = e_lo%llim_proc, e_lo%ulim_proc
       is = is_idx(e_lo,ielo)
       if (spec(is)%nu < 2.0*epsilon(0.0)) cycle
       if (ieqzip(it_idx(e_lo,ielo),ik_idx(e_lo,ielo))==0) cycle

       delta(1) = ged(1,ielo)
       do ie = 1, negrid-1
          delta(ie+1) = ged(ie+1,ielo) - eql(ie+1,ielo)*delta(ie)
       end do
       ged(negrid+1,ielo) = 0.0
       do ie = negrid, 1, -1
          ged(ie,ielo) = ( delta(ie) - ec1(ie,ielo) * ged(ie+1,ielo) ) &
               * ebetaa(ie,ielo)
       end do
    end do

    if (present(init)) then
       call scatter (ediffuse_map, ged, g)
    else
       call measure_scatter (ediffuse_map, ged, g)
    end if

    if (heating_flag) then

       if (heating_diag_switch == heating_diag_after_coll) gedd=ged

       do ielo = e_lo%llim_proc, e_lo%ulim_proc
          do ie=1, negrid
             fac = gedd(ie+1,ielo) - gedd(ie,ielo)
             gedc(ie,ielo) = cabs(fac)**2*ed1(ie,ielo) + cabs(gedd(ie,ielo))**2*ee1(ie,ielo)
          end do
       end do

       call measure_scatter (ediffuse_map, gedc, gc)

       ! no hyper_colls at the moment
       if (hyper_colls) then
          gh=0.
       end if
    end if

# endif

  end subroutine solfp_ediffuse

  subroutine reset_init
!
! forces recalculation of coefficients in collision operator
! when timestep changes.
!    
    initialized = .false.  

  end subroutine reset_init

  subroutine check_g2le

    use file_utils, only: error_unit
    use mp, only: finish_mp, iproc, proc0
    use theta_grid, only: ntgrid
    use le_grids, only: nlambda, negrid
    use agk_layouts, only: g_lo, le_lo
    use agk_layouts, only: ig_idx, ik_idx, it_idx, il_idx, ie_idx, is_idx
    use redistribute, only: gather, scatter, report_map_property
    use agk_mem, only: alloc8, dealloc8
    integer :: iglo, ile, ig, ik, it, il, ie, is, ierr
    complex, dimension (:,:,:), allocatable :: gtmp, letmp

    if (proc0) then
       ierr = error_unit()
    else
       ierr = 6
    end if

    ! report the map property
    if (proc0) print *, '*** g2le map property ***'
    call report_map_property (g2le)

    allocate (gtmp(-ntgrid:ntgrid, 2, g_lo%llim_proc:g_lo%ulim_alloc)); call alloc8(c3=gtmp,v="gtmp")
    allocate (letmp(nlambda*2+1, negrid+1, le_lo%llim_proc:le_lo%ulim_alloc)); call alloc8(c3=letmp,v="letmp")

    ! gather check
    gtmp = 0.0
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       gtmp(0,1,iglo) = rule(ik,it,il,ie,is)
    end do
    call gather (g2le, gtmp, letmp)
    do ile = le_lo%llim_proc, le_lo%ulim_proc
       ig = ig_idx(le_lo,ile)
! MAB>
! following line changed to account for extra elements in gle layout
! (i.e. il=2*nlambda+1 and ie=negrid+1)
!       if (ig /= 0 .and. all(letmp(:,:,ile)==0.0)) cycle
       if (ig /= 0 .and. all(letmp(:2*nlambda,:negrid,ile)==0.0)) cycle
! <MAB
       ik = ik_idx(le_lo,ile)
       it = it_idx(le_lo,ile)
       is = is_idx(le_lo,ile)
       do il=1, nlambda
          do ie=1, negrid
             if (int(real(letmp(il,ie,ile))) /= rule(ik,it,il,ie,is)) &
                  write (ierr,'(a,8i6)') 'ERROR: gather by g2le broken!', iproc
          end do
       end do
    end do
    if (proc0) write (ierr,'(a)') 'g2le gather check done'

    ! scatter check
    gtmp = 0.0
    call scatter (g2le, letmp, gtmp)
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       if (gtmp(0,1,iglo) /= rule(ik,it,il,ie,is)) &
            write (ierr,'(a,i6)') 'ERROR: scatter by g2le broken!', iproc
    end do
    if (proc0) write (ierr,'(a)') 'g2le scatter check done'

!    deallocate (gtmp,letmp)
    call dealloc8(c3=gtmp,v="gtmp")
    call dealloc8(c3=letmp,v="letmp")

!!$    call finish_mp
!!$    stop

  contains

    function rule (ik, it, il, ie, is)
      integer, intent (in) :: ik, it, il, ie, is
      integer :: rule
      rule = ie + ik  ! make whatever you want
    end function rule

  end subroutine check_g2le

  subroutine check_l2e_map

    use file_utils, only: error_unit
    use mp, only: finish_mp, iproc, proc0
    use le_grids, only: nlambda, negrid
    use agk_layouts, only: lz_lo, e_lo
    use agk_layouts, only: ig_idx, ik_idx, it_idx, il_idx, ie_idx, is_idx
    use redistribute, only: gather, scatter, report_map_property
    use agk_mem, only: alloc8, dealloc8
    integer :: ilz, ielo, ig, ik, it, il, ie, is, ierr, nxi, ixi
    complex, dimension (:,:), allocatable :: ltmp, etmp

    if (proc0) then
       ierr = error_unit()
    else
       ierr = 6
    end if

    ! report the map property
    if (proc0) print *, '*** L2E map property ***'
    call report_map_property (l2e_map)

    nxi = nlambda * 2
    allocate (ltmp(nxi+1, lz_lo%llim_proc:lz_lo%ulim_alloc)); call alloc8(c2=ltmp,v="ltmp")
    allocate (etmp(negrid+1, e_lo%llim_proc:e_lo%ulim_alloc)); call alloc8(c2=etmp,v="etmp")

    ! gather check
    ltmp = 0.0
    do ilz = lz_lo%llim_proc, lz_lo%ulim_proc
       ig = ig_idx(lz_lo,ilz)
       if (ig /= 0) cycle
       ik = ik_idx(lz_lo,ilz)
       it = it_idx(lz_lo,ilz)
       ie = ie_idx(lz_lo,ilz)
       is = is_idx(lz_lo,ilz)
       do ixi=1, nxi
          il = min(ixi, nxi+1-ixi)
          ltmp(ixi,ilz) = rule(ig,ik,it,il,ie,is)
       end do
    end do
    call gather (l2e_map, ltmp, etmp)
    do ielo = e_lo%llim_proc, e_lo%ulim_proc
       ig = ig_idx(e_lo,ielo)
       ik = ik_idx(e_lo,ielo)
       it = it_idx(e_lo,ielo)
       il = il_idx(e_lo,ielo)
       is = is_idx(e_lo,ielo)
       do ie=1, negrid
          if (ig /= 0 .and. etmp(ie,ielo)==0.0) cycle
          if (int(real(etmp(ie,ielo))) /= rule(ig,ik,it,il,ie,is)) &
               write (ierr,'(a,i6)') 'ERROR: gather by l2e_map broken!', iproc
       end do
    end do
    if (proc0) write (ierr,'(a)') 'l2e_map gather check done'

    ! scatter check
    ltmp = 0.0
    call scatter (l2e_map, etmp, ltmp)
    do ilz = lz_lo%llim_proc, lz_lo%ulim_proc
       ig = ig_idx(lz_lo,ilz)
       if (ig /= 0) cycle
       ik = ik_idx(lz_lo,ilz)
       it = it_idx(lz_lo,ilz)
       ie = ie_idx(lz_lo,ilz)
       is = is_idx(lz_lo,ilz)
       do ixi=1, nxi
          il = min(ixi, nxi+1-ixi)
          if (int(real(ltmp(ixi,ilz))) /= rule(ig,ik,it,il,ie,is)) &
               write (ierr,'(a,i6)') 'ERROR: scatter by l2e_map broken!', iproc
       end do
    end do
    if (proc0) write (ierr,'(a)') 'l2e_map scatter check done'

!    deallocate (ltmp,etmp)
    call dealloc8(c2=ltmp,v="ltmp")
    call dealloc8(c2=etmp,v="etmp")

!!$    call finish_mp
!!$    stop

  contains

    function rule (ig, ik, it, il, ie, is)
      integer, intent (in) :: ig, ik, it, il, ie, is
      integer :: rule
      rule = ie + ik  ! make whatever you want
    end function rule

  end subroutine check_l2e_map

  ! dvperp(it,ik,is)
  subroutine get_dvperp (dvperp)

    use mp, only: proc0
    use agk_layouts, only: le_lo, g_lo, is_idx, ig_idx
    use redistribute, only: measure_gather, measure_scatter
    use le_grids, only: al, e, nlambda, negrid, integrate_moment
    use species, only: nspec
    use kgrids, only: nakx, naky
    use theta_grid, only: ntgrid
    use dist_fn_arrays, only: gnew
    use agk_mem, only: alloc8, dealloc8
    real, dimension (:,:,:), intent (out) :: dvperp
    integer :: nxi, ixi, ie, ile, ig, is, il
    real, dimension (nlambda*2) :: xi
    real, dimension (3,nlambda*2) :: dxi
    real, dimension (3,negrid,nspec) :: de
    real, dimension (nakx, naky, nspec) :: hkdvi, hki
    complex, dimension (:,:), allocatable :: ctmp1, ctmp2
    complex, dimension (:,:,:), allocatable :: hkdv

    nxi = nlambda*2
    call init_g2le_redistribute
    if (.not.allocated(gle)) then
       allocate (gle(nxi+1, negrid+1, le_lo%llim_proc:le_lo%ulim_alloc)); call alloc8 (c3=gle,v="gle")
    endif
       
    gle = 0.0

    ! define grid and dx
    xi(1:nlambda) = -sqrt(abs(1.0 - al))
    xi(nlambda+1:nxi) = -xi(nlambda:1:-1)
    call get_dx (xi, dxi)
    do is=1, nspec
       call get_dx (e(:,is), de(:,:,is))
    end do

    ! g -> h
!!!RN    call g_adjust (gnew, phinew, bparnew, fphi, fbpar)
    call measure_gather (g2le, gnew, gle)

    ! compute dhk/dvperp
    allocate (ctmp1(nlambda*2, negrid)); call alloc8(c2=ctmp1,v="ctmp1")
    allocate (ctmp2(nlambda*2, negrid)); call alloc8(c2=ctmp2,v="ctmp2")
    ctmp1 = 0.0 ; ctmp2 = 0.0
    do ile=le_lo%llim_proc, le_lo%ulim_proc
       ig = ig_idx(le_lo,ile)
       if (ig /= 0) cycle         ! chosen ig=0 to output
       is = is_idx(le_lo,ile)
       ! obtain dhk/dE
       do ixi=1, nxi
          call take_ddx (gle(ixi,:negrid,ile), de(:,:,is), ctmp1(ixi,:))
       end do
       ! obtain dhk/dxi
       do ie=1, negrid
          call take_ddx (gle(:nxi,ie,ile), dxi, ctmp2(:,ie))
       end do
       ! sum up to get dhk/dvperp
       do ixi=1, nxi
          il = min(ixi, nxi+1-ixi)
          do ie=1, negrid
             gle(ixi,ie,ile) = (ctmp1(ixi,ie) * 2.0 - xi(ixi)/e(ie,is) &
                  * ctmp2(ixi,ie)) * sqrt(al(il)*e(ie,is))
          end do
       end do
    end do
!    deallocate (ctmp1, ctmp2)
    call dealloc8(c2=ctmp1,v="ctmp1")
    call dealloc8(c2=ctmp2,v="ctmp2")
    
    allocate (hkdv(-ntgrid:ntgrid, 2, g_lo%llim_proc:g_lo%ulim_alloc)); call alloc8(c3=hkdv,v="hkdv")
    call measure_scatter (g2le, gle, hkdv)

    ! square and integrate: chosen ig=0 to output
    call integrate_moment (real(conjg(hkdv(0,:,:))*hkdv(0,:,:)), hkdvi)
!    deallocate (hkdv)
    call dealloc8(c3=hkdv,v="hkdv")
    call integrate_moment (real(conjg(gnew(0,:,:))*gnew(0,:,:)), hki)

    ! divide and get dvperp in (kx,ky)-plane for each species
    ! the weird factor for aky=0 does not need to be incorporated
    ! since it's both in numer and denom
    if (proc0) dvperp = sqrt ( hki / hkdvi )

    ! h -> g
!!!RN    call g_adjust (gnew, phinew, bparnew, -fphi, -fbpar)

  end subroutine get_dvperp

  subroutine redist_g2le (g, gle)

    use agk_layouts, only: g_lo, le_lo
    use theta_grid, only: ntgrid
    use redistribute, only: measure_gather

    implicit none

    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (in) :: g
    complex, dimension (:,:,le_lo%llim_proc:), intent (out) :: gle

    gle = 0.0

    call measure_gather (g2le, g, gle)    

  end subroutine redist_g2le

  ! take the second order finite difference of f from uneven grid points x
  ! df contains the result
  subroutine take_ddx (f, dx, df)

    complex, dimension (:), intent (in) :: f
    real, dimension (:,:), intent (in) :: dx
    complex, dimension (:), intent (out) :: df
    integer :: ix, n

    n = size(f)
    df(1) = dot_product(dx(1:3,1), f(1:3))
    do ix=2, n-1
       df(ix) = dot_product(dx(1:3,ix), f(ix-1:ix+1))
    end do
    df(n) = dot_product(dx(1:3,n), f(n-2:n))

  end subroutine take_ddx

  ! obtain finite difference factor of the second order dx
  ! from uneven grid points x
  ! dx must be an array with the size (3,size(x))
  subroutine get_dx (x, dx)

    real, dimension (:), intent (in) :: x
    real, dimension (:,:), intent (out) :: dx
    integer :: ix, n
    real :: dx1, dx2

    n = size(x)

    ! small x edge: forward difference
    dx1 = x(2) - x(1)
    dx2 = x(3) - x(2)
    dx(1,1) = - (2.0*dx1 + dx2) / dx1 / (dx1 + dx2)
    dx(2,1) = (dx1 + dx2) / dx1 / dx2
    dx(3,1) = - dx1 / dx2 / (dx1 + dx2)

    ! middle points: centered difference
    do ix=2, n-1
       dx1 = x(ix+1) - x(ix)
       dx2 = x(ix) - x(ix-1)
       dx(1,ix) = - dx1 / dx2 / (dx1 + dx2)
       dx(2,ix) = (dx1 - dx2) / dx1 / dx2
       dx(3,ix) = dx2 / dx1 / (dx1 + dx2)
    end do

    ! large x edge: backward difference
    dx1 = x(n) - x(n-1)
    dx2 = x(n-1) - x(n-2)
    dx(1,n) = dx1 / dx2 / (dx1 + dx2)
    dx(2,n) = - (dx1 + dx2) / dx1 / dx2
    dx(3,n) = (2.0*dx1 + dx2) / dx1 / (dx1 + dx2)

  end subroutine get_dx

! MAB>
  subroutine init_vpdiff

    use agk_mem, only: alloc8
    use le_grids, only: al, wl, nlambda

    implicit none

    integer :: il
    real :: slb0, slb1, slb2, slbl, slbr

    if (.not. allocated(vpdiff) .and. conservative) then
       allocate (vpdiff (2, nlambda)) ; call alloc8 (r2=vpdiff, v="vpdiff")
       
       vpdiff = 0.0
       
       ! boundary at xi = 1 (vperp = 0)
       slb1 = sqrt(abs(1.0-al(1)))
       slb2 = sqrt(abs(1.0-al(2)))
       slbr = 0.5*(slb1+slb2)
       vpdiff(1,1) = (1.0 - slbr**2)/wl(1)
       vpdiff(2,1) = -vpdiff(1,1)
       
       ! boundary at xi = 0 (vpa = 0)
       slb0 = sqrt(abs(1.0-al(nlambda-1)))
       slb1 = sqrt(abs(1.0-al(nlambda)))
       slb2 = -slb1
       slbl = 0.5*(slb0+slb1)
       slbr = 0.5*(slb1+slb2)
       vpdiff(1,nlambda) = (slbl**2 - slbr**2)/wl(nlambda)
       vpdiff(2,nlambda) = -vpdiff(1,nlambda)
       
       do il = 2, nlambda-1
          slb0 = sqrt(abs(1.0-al(il-1)))
          slb1 = sqrt(abs(1.0-al(il)))
          slb2 = sqrt(abs(1.0-al(il+1)))
          slbl = 0.5*(slb0+slb1)
          slbr = 0.5*(slb1+slb2)
          vpdiff(1,il) = (slbl**2 - slbr**2)/wl(il)
          vpdiff(2,il) = -vpdiff(1,il)
       end do
    end if
    
  end subroutine init_vpdiff

! <MAB

end module collisions
