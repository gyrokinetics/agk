program AstroGK
!$  use omp_lib
  use job_manage, only: checkstop, job_fork, checktime, time_message
  use mp, only: init_mp, finish_mp, proc0, nproc, broadcast,barrier
  use file_utils, only: init_file_utils, finish_file_utils, run_name
  use file_utils, only: stdout_unit
  use fields, only: init_fields
  use agk_diagnostics, only: init_agk_diagnostics, finish_agk_diagnostics
!  use agk_diagnostics, only: nsave
  use run_parameters, only: nstep
  use run_parameters, only: use_Phi, use_Apar, use_Bpar
  use run_parameters, only: avail_cpu_time, margin_cpu_time
  use fields, only: advance
  use dist_fn_arrays, only: gnew
  use agk_save, only: init_agk_save
  use agk_save, only: agk_save_for_restart, nsave
  use agk_diagnostics, only: loop_diagnostics
  use agk_reinit, only: reset_time_step, check_time_step
  use agk_reinit, only: time_reinit
  use agk_time, only: write_dt, init_tstart, update_time
  use agk_time, only: time, dtime
  use agk_mem, only: init_mem,finish_mem,highwater_memory
  use init_g, only: tstart
  use redistribute, only: time_redist
  use fft_work, only: finish_fft_work
  use fields_implicit, only: time_field, time_matmal
  use hdf_wrapper, only: hdf_finish

  implicit none
!  include 'pat_apif.h'
  real :: time_init(2) = 0., time_advance(2) = 0., time_finish(2) = 0.
  real :: time_diag(2) = 0.
  real :: time_total(2) = 0.
  integer :: istep, istep_end, istatus
  logical :: exit, reset, list
  character (2000), target :: cbuff
  
  logical :: debug=.false.

!  if(debug) write(*,*)'Begin AstroGK' !DEBUG-GGH-080324

! initialize message passing
  call init_mp
  call checktime(avail_cpu_time,exit) ! initialize

  if (debug.and.proc0) write(*,*)'MPI initialized' !DEBUG-GGH-080324

!  call PAT_tracing_state(PAT_STATE_OFF)

! report # of processors (and OpenMP threads) being used
  if (proc0) then
     write(*,'(1x,"Running on",1x,i0,1x,"processor")',advance='no') nproc
     if (nproc /= 1 ) write(*,'("s")',advance='no')
!$     write(6,'(1x,": with (max)",1x,i0,1x,"threads")',advance='no') omp_get_max_threads()
     write (*,'(/)',advance='no')
     write (*,*) 
! figure out run name or get list of jobs
     call init_file_utils (list, name="AstroGK")
     if (debug.and.proc0) write(*,*)'File_utils initialized' !DEBUG-GGH-080324
  end if
  call broadcast (list)

! if given a list of jobs, fork
  if (list) call job_fork

  if (proc0) call time_message(.false.,time_total,' Total')

  if (proc0) then
     call time_message(.false.,time_init,' Initialization')
     cbuff = trim(run_name)
  end if

  call broadcast (cbuff)
  if (.not. proc0) run_name => cbuff

  call init_mem !Initialize memory diagnostics
  if (debug.and.proc0) write(*,*)'Memory accounting initialized'   !DEBUG-GGH-080324
  call init_agk_save
  if (debug.and.proc0) write(*,*)'Save initialized'
  call init_fields
  if (debug.and.proc0) write(*,*)'Fields initialized'  !DEBUG-GGH-080324
  call init_agk_diagnostics (list, nstep)
  if (debug.and.proc0) write(*,*)'Diagnostics initialized'  !DEBUG-GGH-080324
  call init_tstart (tstart)   ! tstart is in user units 
  if (debug.and.proc0) write(*,*)'tstart initialized?' !DEBUG-KDN-110715
  if (proc0) call time_message(.false.,time_init,' Initialization')
  istep_end = nstep

!  call PAT_tracing_state(PAT_STATE_ON)
!  call PAT_region_begin(100,'Main advance loop')

  ! output initial state
  call loop_diagnostics (0, exit)
  if (debug.and.proc0) write(*,*) 'Initial loop_diagnostics' !DEBUG-KDN-110713

  do istep = 1, nstep
     if (proc0) call time_message(.false.,time_advance,' Advance time step')

     call advance (istep)
! TT: I think we should update time before save
     call update_time
! <TT
     if (nsave > 0 .and. mod(istep, nsave) == 0) then
        call agk_save_for_restart (gnew, time, dtime(1), istatus, use_Phi, use_Apar, use_Bpar)
     end if

! TT: moved up
!     call update_time
! <TT
     if (proc0) call time_message(.false.,time_diag,' Diagnostics')
     call loop_diagnostics (istep, exit)
     if (proc0) call time_message(.false.,time_diag,' Diagnostics')

     call check_time_step (istep, reset, exit)

     if (proc0) call time_message(.false.,time_advance,' Advance time step')
     if (reset) call reset_time_step (istep, exit)

     if (mod(istep,5) == 0) call checkstop(exit)

     call checktime(avail_cpu_time,exit,margin_cpu_time)

     if (exit) then
        istep_end = istep
        exit
     end if
  end do
  
!  call PAT_region_end(100)
!  call PAT_flush_buffer()
!  call PAT_tracing_state(PAT_STATE_OFF)

  if (proc0) call time_message(.false.,time_finish,' Finished run')

  if (proc0) call write_dt

  call finish_agk_diagnostics (istep_end)
  call finish_fft_work
  call finish_mem !Finish memory diagnostics
  if (proc0) call finish_file_utils

  if (proc0) call time_message(.false.,time_finish,' Finished run')

  if (proc0) call time_message(.false.,time_total,' Total')

  if (proc0) then

     print '(/,'' Initialization'',T25,0pf9.3,'' min'',T40,2pf5.1,'' %'',/, &
          &'' Advance steps'',T25,0pf9.3,'' min'',T40,2pf5.1,'' %'',/, &
          &''(redistribute'',T25,0pf9.3,'' min'',T40,2pf5.1,'' %)'',/, &
          &''(field solve'',T25,0pf9.3,'' min'',T40,2pf5.1,'' %)'',/, &
          &''(field matmal'',T25,0pf9.3,'' min'',T40,2pf5.1,'' %)'',/, &
          &''(diagnostics'',T25,0pf9.3,'' min'',T40,2pf5.1,'' %)'',/, &
          &'' Re-initialize'',T25,0pf9.3,'' min'',T40,2pf5.1,'' %'',/, &
          &'' Finishing'',T25,0pf9.3,'' min'',T40,2pf5.1,'' %'',/,  &
          &'' total from timer is:'',T24,0pf10.3,'' min'',/)', &
          time_init(1)/60.,time_init(1)/time_total(1), &
          time_advance(1)/60.,time_advance(1)/time_total(1), &
          time_redist(1)/60.,time_redist(1)/time_total(1), &
          time_field(1)/60.,time_field(1)/time_total(1), &
          time_matmal(1)/60.,time_matmal(1)/time_total(1), &
          time_diag(1)/60.,time_diag(1)/time_total(1), &
          time_reinit(1)/60.,time_reinit(1)/time_total(1), &
          time_finish(1)/60.,time_finish(1)/time_total(1),time_total(1)/60.

     write(stdout_unit,*) '# of steps advanced', istep_end

     call highwater_memory  !Output high-water memory
! TT> for debug
!     print *, 'Count gather: ', gather_count, ' scatter: ', scatter_count
! <TT

  end if
  
  call hdf_finish
  call finish_mp

end program AstroGK
