!
! This module is much simpler in AstroGK than GS2
! because of the lack of equilibrium magnetic shear. 
! Shear couples different modes together linearly
! through the twist and shift boundary conditions, 
! and leads to very large matrix inversions which 
! are parallelized in GS2.  
!
! The only routine which would benefit strongly from 
! parallelization in the AstroGK case is getfield.
! Getfield is called in the timestepping loop.  
! (Note that advance_implicit is already parallel.)
! Getfield calls a couple of array-packing and 
! unpacking routines, which could be made parallel
! if getfield were parallelized.  
!
! One obvious strategy for parallelizing these loops 
! would be to distribute the it and ik indices.  Since
! many large runs use layouts with these indices on 
! processor (to optimize the evaluation of the nonlinear
! terms), I am putting this off for another time.
!

module fields_implicit
  use fields_arrays, only: nidx
  implicit none

  public :: init_fields_implicit   ! initialize this module
  public :: advance_implicit       ! Do a full timestep using original algorithm
  public :: init_phi_implicit      ! Set up fields for first time step
  public :: nidx                   ! Do not recall why this isn't declared locally to fields_implicit module.
  public :: reset_init             ! Deallocates matrices for implicit fields solution, in prep for changing dt.
  public :: time_field, time_matmal

  private

  integer, save :: nfield          ! number of fields (1,2, or 3)
  logical :: initialized = .false.
  real, save :: time_field(2)=0., time_matmal(2)=0.

contains

  subroutine init_fields_implicit

    use antenna, only: init_antenna
    use theta_grid, only: init_theta_grid
    use kgrids, only: init_kgrids
    implicit none

    if (initialized) return
    initialized = .true.

    call init_theta_grid          ! Set up grid along the field line
    call init_kgrids              ! Set up grids perpendicular to the field line
    call init_response_matrix     ! Calculate matrices for implicit Maxwell solve
    call init_antenna             ! Set up external driving antenna

  end subroutine init_fields_implicit

  subroutine init_phi_implicit
    use fields_arrays, only: phi, apar, bpar, phinew, aparnew, bparnew
    use dist_fn, only: get_init_field
    use dist_fn_arrays, only: g, gnew
    implicit none

    call get_init_field (phinew, aparnew, bparnew)
    phi = phinew; apar = aparnew; bpar = bparnew; g = gnew

  end subroutine init_phi_implicit

  subroutine advance_implicit (istep)
! TT> temporary for check of variable step AB3
!      use agk_time, only: time
! <TT
    use fields_arrays, only: phi, apar, bpar, phinew, aparnew, bparnew
    use fields_arrays, only: apar_ext 
    use antenna, only: antenna_amplitudes
    use dist_fn, only: timeadv, exb_shear
    use dist_fn_arrays, only: g, gnew
    use run_parameters, only: aant
    use agk_time, only: dtime
    use init_g, only: single_kpar, force_single_kpar
    implicit none
    integer, intent (in) :: istep

    call antenna_amplitudes (apar_ext)                                          ! GGH NOTE: apar_ext is initialized in this call
    call exb_shear (gnew, phinew, aparnew, bparnew, istep)                             ! See Hammett & Loureiro, APS 2006
    if (single_kpar .and. force_single_kpar) call &
        ensure_single_parallel_mode(istep)  

    g = gnew  ;  phi = phinew  ;  apar = aparnew  ;  bpar = bparnew             ! Store previously advanced values as current values
    call timeadv (phi, apar, bpar, phinew, aparnew, bparnew, istep)             ! Advance f
    if (aant) then
       aparnew = aparnew + apar_ext                                                ! Add in antenna
    else
       phinew = phinew + apar_ext
    end if
    call getfield (phinew, aparnew, bparnew)                                    ! Find future part of fields
    phinew = phinew + phi ; aparnew = aparnew + apar ; bparnew = bparnew + bpar ! Update fields
    call timeadv (phi, apar, bpar, phinew, aparnew, bparnew, istep)             ! Complete f advance with updated fields

! TT: variable step AB3
!    if (proc0) print *, istep, ': at t= ', time, ' dtime= ', dtime(1:3)
!    print *, iproc, istep, ': at t= ', time, ' dtime= ', dtime(1:3)
    dtime(2:3) = dtime(1:2)
! <TT

  end subroutine advance_implicit

  !> Get rid of unwanted parallel modes which grow due to numerical
  !! errors when initialised with only a single parallel mode. EGH

  subroutine ensure_single_parallel_mode(istep)
    use fields_arrays, only: phinew
    use theta_grid, only: ntgrid
    use kgrids, only: nakx, naky
    use init_g, only: ikpar_init, single_kpar
    use mp, only: mp_abort
    use agk_transforms, only: init_z_transforms, fft_z_forward_field, fft_z_backward_field
    !use agk_transforms, only: fft_z_forward_complex_one, &
      !fft_z_backward_complex_one
    use run_parameters, only: fapar, fbpar


    complex, dimension(:,:,:), allocatable :: phi_trans
    !complex, dimension(-ntgrid:ntgrid) :: g_trans, g_temp
    real, dimension(:,:,:), allocatable :: phi2
    real :: phi2_mag_before, phi2_mag_after 
    integer, intent (in) :: istep
    integer :: ikpar_box_order, ig
    !logical, save :: initialized_transforms = .false.


    if (.not. single_kpar) then 
       call mp_abort("The function ensure_single_parallel_mode should only &
       & be called when the simulation is initialised with a single parallel mode")
     end if

    if (fapar > epsilon(0.0) .or. fbpar > epsilon(0.0)) &
      call mp_abort("The function ensure_single_parallel_mode is not implemented &
      & for apar and bpar yet, i.e. for electromagnetic runs. If you are getting &
      & this message and you are running electrostatically, set use_Apar and &
      & use_Bpar to .false.")


    !if (.not. initialized_transforms) call init_z_transforms(ntgrid)
    !write (*,*) "About to call init"
    call init_z_transforms(ntgrid)
    !write (*,*) "Called init"
    !initialized_transforms = .true.


    allocate(phi_trans(-ntgrid:ntgrid,1:nakx,1:naky)) 
    allocate(phi2(-ntgrid:ntgrid,1:nakx,1:naky)) 
    !write (*,*) "About to call forward"
    call fft_z_forward_field(phinew, phi_trans, nakx, naky)
    !write (*,*) "Called forward"
    phi2 = real(phinew*conjg(phinew))
    phi2_mag_before = sum(phi2(:,:,:))

    if (ikpar_init < 0) then
      ikpar_box_order = -ntgrid + (2 * ntgrid + 1 + ikpar_init)
    else
      ikpar_box_order = -ntgrid + ikpar_init
    end if
    do ig = -ntgrid, ntgrid
      if (ig .ne. ikpar_box_order) phi_trans(ig, :, :) = 0.
    end do
    !write (*,*) "About to call backward"
    call fft_z_backward_field(phi_trans, phinew, nakx, naky)
    !write (*,*) "Called backward"
    phinew = phinew / real(2 * ntgrid + 1) 
    phi2 = real(phinew*conjg(phinew))
    phi2_mag_after = sum(phi2(:,:,:))

    !do iglo = g_lo%llim_proc, g_lo%ulim_proc
      !do isgn = 1,2
            !non_zero_other = .false.
        !do ig = -ntgrid, ntgrid
          !g_temp(ig) = gnew(ig, isgn, iglo)
        !end do
        !!if (isgn .eq. 1 .and. iglo .eq. 1) write (*,*) "g_temp1", g_temp
        !!if (isgn .eq. 1 .and. iglo .eq. 1) write (*,*) "g_trans1", g_trans
        !call fft_z_forward_complex_one(g_temp, g_trans)
        !do ig = -ntgrid, ntgrid
          !if (ig .ne. ikpar_box_order .and. g_trans(ig) .ne.  epsilon(0.)) &
            !non_zero_other = .true.
        !end do
        !!if (proc0 .and. non_zero_other .and. mod(istep, 100) .eq. 0) write (*,*) "g_temp1", g_temp
       !if (proc0 .and. non_zero_other .and. mod(istep, 100) .eq. 0) write (*,*) "g_trans1", g_trans
        !do ig = -ntgrid, ntgrid
          !if (ig .ne. ikpar_box_order) g_trans(ig) = 0.
        !end do
        !call fft_z_backward_complex_one(g_trans, g_temp)
        !g_temp = g_temp / real(2 * ntgrid + 1) 
        !!if (proc0 .and. non_zero_other .and. mod(istep, 100) .eq. 0) write (*,*) "g_temp2", g_temp
       !!if (proc0 .and. non_zero_other .and. mod(istep, 100) .eq. 0) write (*,*) "g_trans2", g_trans
        !!if (proc0 .and. non_zero_other) call mp_abort("non_zero_other")
        !!if (isgn .eq. 1 .and. iglo .eq. 1) write (*,*) "g_temp1", g_temp
        !!if (isgn .eq. 1 .and. iglo .eq. 1) write (*,*) "g_trans1", g_trans
        !do ig = -ntgrid, ntgrid
          !gnew(ig, isgn, iglo) = g_temp(ig)
        !end do
      !end do
    !end do
    !!phinew = phinew * sqrt(phi2_mag_before / phi2_mag_after)
    !!write (*,*) "About to call deallocate"
    deallocate(phi_trans)
    !write (*,*) "Deallocated array 1"
    deallocate(phi2)
    !write (*,*) "Deallocated arrays"

  end subroutine ensure_single_parallel_mode

  subroutine getfield (phi, apar, bpar)
    use mp, only: nproc, sum_allreduce, proc0
    use kgrids, only: naky, nakx
    use fields_arrays, only: aminv
    use theta_grid, only: ntgrid
    use agk_mem, only: alloc8, dealloc8 
    use agk_layouts, only: f_lo, ik_idx, it_idx
    use job_manage, only: time_message
    implicit none
    complex, dimension (-ntgrid:,:,:), intent (in) :: phi, apar, bpar
    complex, dimension (:,:,:), allocatable :: fl
    complex, dimension (:), allocatable :: u
    integer :: iflo

    if (proc0) call time_message(.false.,time_field,' Field Solver')

    allocate (fl(nidx, nakx, naky)) ; call alloc8 (c3=fl, v="fl")

    allocate (u(f_lo%llim_world:f_lo%ulim_world)) ; u = 0. ; call alloc8 (c1=u, v="u")

    ! am*u = fl, Poisson's and Ampere's law, u is phi, apar, bpar 
    ! u = aminv*fl

    call get_field_vector (fl, phi, apar, bpar)
    
    if (proc0) call time_message(.false.,time_matmal,' Matrix Multiplication')
    do iflo = f_lo%llim_proc, f_lo%ulim_proc
       u(iflo) = - sum(aminv(:,iflo) * fl(:, it_idx(f_lo, iflo), ik_idx(f_lo, iflo)))
    end do
    if (proc0) call time_message(.false.,time_matmal,' Matrix Multiplication')
    call dealloc8 (c3=fl, v="fl")

    if (nproc > 1) call sum_allreduce (u)

    call get_field_solution (u)

    call dealloc8 (c1=u, v="u")

    if (proc0) call time_message(.false.,time_field,' Field Solver')

  end subroutine getfield

  subroutine get_field_vector (fl, phi, apar, bpar)
    use theta_grid, only: ntgrid
    use kgrids, only: naky, nakx
    use dist_fn, only: getfieldeq
    use run_parameters, only: use_Phi, use_Apar, use_Bpar
    use agk_mem, only: alloc8, dealloc8 
    implicit none
    complex, dimension (-ntgrid:,:,:), intent (in) :: phi, apar, bpar
    complex, dimension (:,:,:), intent (out) :: fl
    complex, dimension (:,:,:), allocatable :: fieldeq, fieldeqa, fieldeqp
    integer :: istart

    allocate (fieldeq (-ntgrid:ntgrid,nakx,naky)) ; call alloc8 (c3=fieldeq,  v="fieldeq")
    allocate (fieldeqa(-ntgrid:ntgrid,nakx,naky)) ; call alloc8 (c3=fieldeqa, v="fieldeqa")
    allocate (fieldeqp(-ntgrid:ntgrid,nakx,naky)) ; call alloc8 (c3=fieldeqp, v="fieldeqp")

    call getfieldeq (phi, apar, bpar, fieldeq, fieldeqa, fieldeqp)

    istart = 0

    ! BD Coding is different from GS2
    if (use_Phi) then
       istart = istart + 1
       fl(istart:nidx:nfield,:,:) = fieldeq(-ntgrid:ntgrid-1,:,:)
    end if

    if (use_Apar) then
       istart = istart + 1
       fl(istart:nidx:nfield,:,:) = fieldeqa(-ntgrid:ntgrid-1,:,:)
    end if

    if (use_Bpar) then
       istart = istart + 1
       fl(istart:nidx:nfield,:,:) = fieldeqp(-ntgrid:ntgrid-1,:,:)
    end if

    call dealloc8 (c3 = fieldeq,  v="fieldeq")
    call dealloc8 (c3 = fieldeqa, v="fieldeqa")
    call dealloc8 (c3 = fieldeqp, v="fieldeqp")

  end subroutine get_field_vector

  subroutine get_field_solution (u)
    use theta_grid, only: ntgrid
    use fields_arrays, only: phinew, aparnew, bparnew
    use run_parameters, only: use_Phi, use_Apar, use_Bpar
    use agk_layouts, only: f_lo
    use kgrids, only: nakx, naky
    implicit none
    complex, dimension (f_lo%llim_world:), intent (in) :: u
    integer :: istart, iflo, ik, it, ig

    istart = 0

    if (use_Phi) then
       istart = istart + 1
       ! TT: We could either do this:
!!$       phinew = reshape(u(istart-1::nfield), shape(phinew))
       ! TT: or this:
       iflo = istart - 1
       do ik=1, naky
          do it=1, nakx
             do ig=-ntgrid, ntgrid-1
                phinew(ig,it,ik) = u(iflo)
                iflo = iflo + nfield
             end do
          end do
       end do
       phinew(ntgrid,:,:) = phinew(-ntgrid,:,:)
    endif

    if (use_Apar) then
       istart = istart + 1
       iflo = istart - 1
       do ik=1, naky
          do it=1, nakx
             do ig=-ntgrid, ntgrid-1
                aparnew(ig,it,ik) = u(iflo)
                iflo = iflo + nfield
             end do
          end do
       end do
       aparnew(ntgrid,:,:) = aparnew(-ntgrid,:,:)
    endif

    if (use_Bpar) then
       istart = istart + 1
       iflo = istart - 1
       do ik=1, naky
          do it=1, nakx
             do ig=-ntgrid, ntgrid-1
                bparnew(ig,it,ik) = u(iflo)
                iflo = iflo + nfield
             end do
          end do
       end do
       bparnew(ntgrid,:,:) = bparnew(-ntgrid,:,:)
   endif

  end subroutine get_field_solution

  subroutine reset_init
    use fields_arrays, only: aminv
    use agk_mem, only: dealloc8
    implicit none

    initialized = .false.
    
    call dealloc8 (c2=aminv, v="aminv")

  end subroutine reset_init

  subroutine init_response_matrix

    use fields_arrays, only: phi, apar, bpar, phinew, aparnew, bparnew
    use theta_grid, only: ntgrid
    use kgrids, only: naky, nakx
    use dist_fn_arrays, only: g
    use run_parameters, only: use_Phi, use_Apar, use_Bpar
    use agk_layouts, only: init_fields_layouts, f_lo
    use agk_mem, only: alloc8, dealloc8 
    implicit none
    integer :: ig, ifield
    complex, dimension(:,:), allocatable :: am

    nfield = 0
    if (use_Phi)  nfield = nfield + 1
    if (use_Apar) nfield = nfield + 1
    if (use_Bpar) nfield = nfield + 1
    nidx = 2*ntgrid*nfield

    call init_fields_layouts (nidx, naky, nakx)

    allocate (am(nidx, f_lo%llim_proc:f_lo%ulim_alloc)) ; call alloc8 (c2=am, v="am")

    am = 0.0
    g = 0.0
    
    phi = 0.0
    apar = 0.0
    bpar = 0.0
    phinew = 0.0
    aparnew = 0.0
    bparnew = 0.0
    
    do ig = -ntgrid, ntgrid-1
       ifield = 0
       if (use_Phi) then
          ifield = ifield + 1
          phinew(ig,:,:) = 1.0
          phinew(ntgrid,:,:) = phinew(-ntgrid,:,:)
          call init_response_row (ig, ifield, am)
          phinew = 0.0
       end if
       
       if (use_Apar) then
          ifield = ifield + 1
          aparnew(ig,:,:) = 1.0
          aparnew(ntgrid,:,:) = aparnew(-ntgrid,:,:)
          call init_response_row (ig, ifield, am)
          aparnew = 0.0
       end if
       
       if (use_Bpar) then
          ifield = ifield + 1
          bparnew(ig,:,:) = 1.0
          bparnew(ntgrid,:,:) = bparnew(-ntgrid,:,:)
          call init_response_row (ig, ifield, am)
          bparnew = 0.0
       end if
    end do
    
    call init_inverse_matrix (am)

    call dealloc8 (c2=am, v="am")
    
  end subroutine init_response_matrix

  subroutine init_response_row (ig, ifield, am)
    use fields_arrays, only: phi, apar, bpar, phinew, aparnew, bparnew
    use theta_grid, only: ntgrid
    use kgrids, only: naky, nakx
    use dist_fn, only: getfieldeq, timeadv
    use run_parameters, only: use_Phi, use_Apar, use_Bpar
    use agk_mem, only: alloc8, dealloc8 
    use agk_layouts, only: f_lo, idx, idx_local
    implicit none
    integer, intent (in) :: ig, ifield
    complex, dimension(:,f_lo%llim_proc:), intent (in out) :: am
    complex, dimension (:,:,:), allocatable :: fieldeq, fieldeqa, fieldeqp
    integer :: irow, istart, ik, it, ifin, iflo

    allocate (fieldeq (-ntgrid:ntgrid, nakx, naky)) ; call alloc8 (c3=fieldeq,  v="fieldeq")
    allocate (fieldeqa(-ntgrid:ntgrid, nakx, naky)) ; call alloc8 (c3=fieldeqa, v="fieldeqa")
    allocate (fieldeqp(-ntgrid:ntgrid, nakx, naky)) ; call alloc8 (c3=fieldeqp, v="fieldeqp")

    call timeadv (phi, apar, bpar, phinew, aparnew, bparnew, 0)
    call getfieldeq (phinew, aparnew, bparnew, fieldeq, fieldeqa, fieldeqp)

    irow = ifield + nfield*(ig+ntgrid)

    do ik=1,naky
       do it=1,nakx
          iflo = idx(f_lo, irow, ik, it)
          if (idx_local(f_lo, iflo)) then

             istart = 0              
             if (use_Phi) then
                ifin = istart + nidx
                istart = istart + 1
                am(istart:ifin:nfield,iflo) = fieldeq(-ntgrid:ntgrid-1,it,ik) 
             end if

             if (use_Apar) then
                ifin = istart + nidx
                istart = istart + 1
                am(istart:ifin:nfield,iflo) = fieldeqa(-ntgrid:ntgrid-1,it,ik)
             end if
             
             if (use_Bpar) then
                ifin = istart + nidx
                istart = istart + 1
                am(istart:ifin:nfield,iflo) = fieldeqp(-ntgrid:ntgrid-1,it,ik)
             end if
          end if
       end do
    end do

    call dealloc8 (c3=fieldeq,  v="fieldeq")
    call dealloc8 (c3=fieldeqa, v="fieldeqa") 
    call dealloc8 (c3=fieldeqp, v="fieldeqp")

  end subroutine init_response_row

  subroutine init_inverse_matrix (am)
    use kgrids, only: aky, akx, nakx, naky
    use fields_arrays, only: aminv
    use agk_mem, only: alloc8, dealloc8 
    use agk_layouts, only: f_lo, idx, idx_local, proc_id
    use agk_layouts, only: if_idx, ik_idx, it_idx
    use mp, only: nproc
    implicit none
    complex, dimension(:,f_lo%llim_proc:), intent (in out) :: am
    complex, dimension(:,:,:), allocatable :: lhscol, rhsrow
    complex :: fac
    integer :: i, ik, it, ilo, jlo, irow

    allocate (lhscol (nidx,nakx,naky)) ; call alloc8 (c3=lhscol, v="lhscol")
    allocate (rhsrow (nidx,nakx,naky)) ; call alloc8 (c3=rhsrow, v="rhsrow")
       
    allocate (aminv(nidx,f_lo%llim_proc:f_lo%ulim_alloc)) ; call alloc8 (c2=aminv, v="aminv")

    aminv = 0.0
    do ilo = f_lo%llim_proc, f_lo%ulim_proc
       aminv(if_idx(f_lo, ilo), ilo) = 1.0
    end do
!
! Gauss-Jordan elimination: surely could be improved!
! Inverting naky*nakx arrays of size nidx*nidx in parallel
!
    do i=1,nidx
       lhscol = 0.0; rhsrow = 0.0
       do ik=1,naky
          do it=1,nakx
             ilo = idx(f_lo, i, ik, it)
             if (idx_local(f_lo, ilo)) then
                lhscol(:,it,ik) = am   (:,ilo)
                rhsrow(:,it,ik) = aminv(:,ilo)
             end if
          end do
       end do
       if (nproc > 1) then
          call sum_up_all (lhscol)
          call sum_up_all (rhsrow)
       end if

       do jlo = f_lo%llim_proc, f_lo%ulim_proc
          irow = if_idx(f_lo,jlo)
          ik = ik_idx(f_lo,jlo)
          it = it_idx(f_lo,jlo)

          if (aky(ik) /= 0.0 .or. akx(it) /= 0.0) then
             fac = am(i,jlo)/lhscol(i,it,ik)
             am(i,jlo) = am(i,jlo)/lhscol(i,it,ik)
             am(:i-1,jlo) = am(:i-1,jlo) - lhscol(:i-1,it,ik)*fac
             am(i+1:,jlo) = am(i+1:,jlo) - lhscol(i+1:,it,ik)*fac
             
             if (irow == i) then
                aminv(:,jlo) = aminv(:,jlo)/lhscol(i,it,ik)
             else
                aminv(:,jlo) = aminv(:,jlo) &
                     - rhsrow(:,it,ik)*lhscol(irow,it,ik)/lhscol(i,it,ik)
             end if
          else
             aminv(:,jlo) = 0.
          end if
       end do
    end do

    call dealloc8 (c3=lhscol, v="lhscol")
    call dealloc8 (c3=rhsrow, v="rhsrow")

  end subroutine init_inverse_matrix

  ! TT: We could put this subroutine in mp.fpp and use it for le_grids etc?
  subroutine sum_up_all (field)

    use agk_mem, only: alloc8, dealloc8
    use mp, only: sum_allreduce

    complex, dimension (:,:,:), intent (inout) :: field
    integer :: i, ik, it, ig, n1, n2, n3
    complex, dimension (:), allocatable :: work

    n1 = size (field,1)
    n2 = size (field,2)
    n3 = size (field,3)

    allocate ( work(n1*n2*n3) ) ; work = 0. ; call alloc8 (c1=work, v="work")

    i = 0
    do ik=1, n3
       do it=1, n2
          do ig=1, n1
             i = i + 1
             work(i) = field(ig,it,ik)
          end do
       end do
    end do
    call sum_allreduce (work)
    i = 0
    do ik=1, n3
       do it=1, n2
          do ig=1, n1
             i = i + 1
             field(ig,it,ik) = work(i)
          end do
       end do
    end do

    call dealloc8 (c1=work, v="work")

  end subroutine sum_up_all

end module fields_implicit
