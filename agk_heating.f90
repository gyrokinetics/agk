module agk_heating
  implicit none
  private

  public :: avg_hk
  !GGH Density-velocity perturbations
  public :: dens_vel_diagnostics
  public :: init_dvtype, zero_dvtype, del_dvtype
  public :: avg_dv, avg_dvk

  !GGH
  interface init_dvtype
     module procedure init_dvtype_0, init_dvtype_1, init_dvtype_2, init_dvtype_3
  end interface

  interface zero_dvtype
     module procedure zero_dvtype_0, zero_dvtype_1, zero_dvtype_2, zero_dvtype_3
  end interface

  interface del_dvtype
     module procedure del_dvtype_0, del_dvtype_2
  end interface


!GGH>
  type :: dens_vel_diagnostics
     !GGH NOTE: Dimension here is for species
     real, dimension(:), pointer :: dvpar  => null()
     real, dimension(:), pointer :: dvperp   => null()
     real, dimension(:), pointer :: dn => null()
  end type dens_vel_diagnostics
!<GGH
contains
!------------------------------------------------------------------------------
!                              AstroGK, 2008
!------------------------------------------------------------------------------
! Save heating by k values to history
! If it is time to write to output, then compute averages for heating by k
  subroutine avg_hk (hk, istep, navg, nwrite)
    use agk_heating_index, only: lemax
    use agk_mem, only: alloc8
    implicit none
    !Passed
    real, dimension(:,:,:), intent (inout) :: hk
    real, allocatable, save :: hk_hist(:,:,:,:)
    integer, intent (in) :: istep, navg, nwrite
    integer :: i, it, ik, nakx, naky
    
    !Get nakx and naky sizes of hk array
    nakx = size(hk,1); naky = size(hk,2)

    if(.not.allocated(hk_hist)) then
       ! RN> This array can be huge
       allocate(hk_hist(navg,nakx,naky,size(hk,3))); call alloc8(r4=hk_hist,v="hk_hist")
       hk_hist=0.
    endif

    hk_hist(2:navg,:,:,:)=hk_hist(1:navg-1,:,:,:) ! shift
    hk_hist(1,:,:,:)=hk(:,:,:)                    ! add new

    ! If it is time to write out heating data, then perform the average
    if (mod(istep,nwrite) .eq. 0) then
       do i=1,lemax
          do ik=1,naky
             do it=1,nakx
                hk(it,ik,i)=sum(hk_hist(:,it,ik,i))/min(istep+1,navg)
             enddo
          enddo
       enddo
    end if

  end subroutine avg_hk
!-----------------------------------------------------------------------------
!<GGH
! Density velocity perturbation routines
!-----------------------------------------------------------------------------
    subroutine init_dvtype_0 (dv, nspec)

    type (dens_vel_diagnostics) :: dv
    integer, intent (in) :: nspec

       allocate (dv % dvpar(nspec)) !!! RN> unable to account memory for structure variables
       allocate (dv % dvperp(nspec)) !!! RN> unable to account memory for structure variables
       allocate (dv % dn(nspec)) !!! RN> unable to account memory for structure variables

    call zero_dvtype (dv)

  end subroutine init_dvtype_0
!-----------------------------------------------------------------------------
  subroutine init_dvtype_1 (dv, nspec)
    type (dens_vel_diagnostics), dimension(:) :: dv
    integer, intent (in) :: nspec
    integer :: n, nmax

    nmax = size(dv)
    
    do n=1,nmax
       allocate (dv(n) % dvpar(nspec)) !!! RN> unable to account memory for structure variables
       allocate (dv(n) % dvperp(nspec)) !!! RN> unable to account memory for structure variables
       allocate (dv(n) % dn(nspec)) !!! RN> unable to account memory for structure variables
    end do

    call zero_dvtype (dv)

  end subroutine init_dvtype_1
!-----------------------------------------------------------------------------
  subroutine init_dvtype_2 (dv, nspec)
    type (dens_vel_diagnostics), dimension(:,:) :: dv
    integer, intent (in) :: nspec
    integer :: m, n, mmax, nmax

    mmax = size(dv, 1)
    nmax = size(dv, 2)

    do n = 1, nmax
       do m = 1, mmax
          allocate (dv(m,n) % dvpar(nspec)) !!! RN> unable to account memory for structure variables
          allocate (dv(m,n) % dvperp(nspec)) !!! RN> unable to account memory for structure variables
          allocate (dv(m,n) % dn(nspec)) !!! RN> unable to account memory for structure variables
       end do
    end do

    call zero_dvtype (dv)

  end subroutine init_dvtype_2
!-----------------------------------------------------------------------------
  subroutine init_dvtype_3 (dv, nspec)
    type (dens_vel_diagnostics), dimension(:,:,:) :: dv
    integer, intent (in) :: nspec
    integer :: l, m, n, lmax, mmax, nmax

    lmax = size(dv, 1)
    mmax = size(dv, 2)
    nmax = size(dv, 3)

    do n = 1, nmax
       do m = 1, mmax
          do l = 1, lmax    
             allocate (dv(l,m,n) % dvpar(nspec)) !!! RN> unable to account memory for structure variables
             allocate (dv(l,m,n) % dvperp(nspec)) !!! RN> unable to account memory for structure variables
             allocate (dv(l,m,n) % dn(nspec)) !!! RN> unable to account memory for structure variables
          end do
       end do 
    end do

    call zero_dvtype (dv)

  end subroutine init_dvtype_3
!-----------------------------------------------------------------------------
  subroutine zero_dvtype_0 (dv)

    type (dens_vel_diagnostics) :: dv

    dv % dvpar = 0. 
    dv % dvperp = 0.  
    dv % dn = 0.

  end subroutine zero_dvtype_0
!-----------------------------------------------------------------------------
  subroutine zero_dvtype_1 (dv)

    type (dens_vel_diagnostics), dimension(:) :: dv
    integer :: n, nmax

    nmax = size(dv)
    
    do n=1,nmax
       dv(n) % dvpar = 0. 
       dv(n) % dvperp = 0.  
       dv(n) % dn = 0.
    end do

  end subroutine zero_dvtype_1
!-----------------------------------------------------------------------------
 subroutine zero_dvtype_2 (dv)

    type (dens_vel_diagnostics), dimension(:,:) :: dv
    integer :: n, nmax, m, mmax

    mmax = size(dv, 1)
    nmax = size(dv, 2)
        
    do n=1,nmax
       do m=1,mmax
          dv(m,n) % dvpar = 0. 
          dv(m,n) % dvperp = 0.  
          dv(m,n) % dn = 0.
       end do
    end do

  end subroutine zero_dvtype_2
!-----------------------------------------------------------------------------
 subroutine zero_dvtype_3 (dv)

    type (dens_vel_diagnostics), dimension(:,:,:) :: dv
    integer :: n, nmax, m, mmax, l, lmax

    lmax = size(dv, 1)
    mmax = size(dv, 2)
    nmax = size(dv, 3)
        
    do n=1,nmax
       do m=1,mmax
          do l=1,lmax
             dv(l,m,n) % dvpar = 0. 
             dv(l,m,n) % dvperp = 0.  
             dv(l,m,n) % dn = 0.
          end do
       end do
    end do

  end subroutine zero_dvtype_3
!-----------------------------------------------------------------------------
  subroutine del_dvtype_0 (dv)
    type (dens_vel_diagnostics) :: dv
    
    deallocate (dv % dvpar) !!! RN> unable to account memory for structure variables
    deallocate (dv % dvperp) !!! RN> unable to account memory for structure variables
    deallocate (dv % dn) !!! RN> unable to account memory for structure variables

  end subroutine del_dvtype_0
!-----------------------------------------------------------------------------
 subroutine del_dvtype_2 (dv)

    type (dens_vel_diagnostics), dimension(:,:) :: dv
    integer :: m, mmax, n, nmax
    
    mmax = size (dv, 1)
    nmax = size (dv, 2)
    
    do n=1,nmax
       do m=1,mmax
          deallocate (dv(m,n) % dvpar) !!! RN> unable to account memory for structure variables
          deallocate (dv(m,n) % dvperp) !!! RN> unable to account memory for structure variables
          deallocate (dv(m,n) % dn) !!! RN> unable to account memory for structure variables
       end do
    end do

  end subroutine del_dvtype_2
!-----------------------------------------------------------------------------
  subroutine avg_dv (dv, dv_hist, istep, navg)
    use mp, only: proc0
    use species, only: nspec
    type (dens_vel_diagnostics) :: dv
    type (dens_vel_diagnostics), dimension (0:) :: dv_hist
    integer, intent (in) :: istep, navg
    integer :: i

    if (proc0) then
       if (navg > 1) then
          if (istep >= 1) then
             dv_hist(mod(istep,navg)) % dvpar(:)     = dv % dvpar(:)
             dv_hist(mod(istep,navg)) % dvperp(:)    = dv % dvperp(:)
             dv_hist(mod(istep,navg)) % dn(:)        = dv % dn(:)
          end if
          
          if (istep >= navg) then
             call zero_dvtype(dv)
             do i=0,navg-1
                dv % dvpar(:)  = dv % dvpar(:)  + dv_hist(i) % dvpar(:)  / real(navg)
                dv % dvperp(:) = dv % dvperp(:) + dv_hist(i) % dvperp(:) / real(navg)
                dv % dn(:)     = dv % dn(:)     + dv_hist(i) % dn (:)    / real(navg)
             end do
          end if
       end if
    end if

  end subroutine avg_dv
!-----------------------------------------------------------------------------
  subroutine avg_dvk (dvk, dvk_hist, istep, navg)
    use mp, only: proc0
    use species, only: nspec
    type (dens_vel_diagnostics), dimension(:,:) :: dvk
    type (dens_vel_diagnostics), dimension (:,:,0:) :: dvk_hist
    integer, intent (in) :: istep, navg
    integer :: i, m, n, mmax, nmax

    mmax = size(dvk, 1)
    nmax = size(dvk, 2)

    if (proc0) then
       if (navg > 1) then
          if (istep >= 1) then
             do n=1,nmax
                do m=1,mmax
                   dvk_hist(m,n,mod(istep,navg)) % dvpar(:)     = dvk(m,n) % dvpar(:)
                   dvk_hist(m,n,mod(istep,navg)) % dvperp(:)    = dvk(m,n) % dvperp(:)
                   dvk_hist(m,n,mod(istep,navg)) % dn(:)        = dvk(m,n) % dn(:)
             enddo
          enddo
          end if
          
          if (istep >= navg) then
             call zero_dvtype (dvk)
             do n=1,nmax
                do m=1,mmax
                   do i=0,navg-1
                      dvk(m,n) % dvpar(:)  = dvk(m,n) % dvpar(:)  + dvk_hist(m,n,i) % dvpar(:)  / real(navg)
                      dvk(m,n) % dvperp(:) = dvk(m,n) % dvperp(:) + dvk_hist(m,n,i) % dvperp(:) / real(navg)
                      dvk(m,n) % dn(:)     = dvk(m,n) % dn(:)     + dvk_hist(m,n,i) % dn (:)    / real(navg)
                   end do
                enddo
             enddo
          end if
       end if
    end if

  end subroutine avg_dvk
!>GGH
!-----------------------------------------------------------------------------

end module agk_heating
