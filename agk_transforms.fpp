# include "define.inc"

#ifdef ANSI_CPP

#ifdef SINGLE_PRECISION
#define FFTW_PREFIX(fn) sfftw##fn
#else
#define FFTW_PREFIX(fn) dfftw##fn
#endif

#else

#ifdef SINGLE_PRECISION
#define FFTW_PREFIX(fn) sfftw/**/fn
#else
#define FFTW_PREFIX(fn) dfftw/**/fn
#endif

#endif

module agk_transforms

  use redistribute, only: redist_type
  use fft_work, only: fft_type

  implicit none

  public :: init_transforms
!  public :: init_x_transform, init_zf, kz_spectrum
!  public :: transform_x, transform_y, transform2
!  public :: inverse_x, inverse_y, inverse2
  public :: transform2, inverse2

  ! these routines are used in diagnostics
  public :: init_zf, kz_spectrum
  public :: kz_transform ! MAB
  public :: kz_transform2 ! KDN

  ! these routine are used in fields_implicit
  public :: init_z_transforms, fft_z_forward_field, fft_z_backward_field ! EGH
  public :: fft_z_backward_complex_one, fft_z_forward_complex_one ! EGH

  public :: accel ! TT: temporary open for transfer function diagnostic

  private

  interface transform_x
     module procedure transform_x5d
  end interface

  interface transform_y
     module procedure transform_y5d
  end interface

  !! TT: transform2 changes input data for 5d, but not for 3d & 4d !!
  interface transform2
     module procedure transform2_5d_accel
     module procedure transform2_4d_accel ! used in kshell
     module procedure transform2_5d
!     module procedure transform2_4d ! note this is completely different from 4d_accel: not used?
     module procedure transform2_3d
     module procedure transform2_2d
  end interface

  interface inverse_x
     module procedure inverse_x5d
  end interface

  interface inverse_y
     module procedure inverse_y5d
  end interface

  interface inverse2
     module procedure inverse2_5d_accel
     module procedure inverse2_4d_accel ! used in kshell
     module procedure inverse2_5d
     module procedure inverse2_3d
     module procedure inverse2_2d
  end interface

  ! redistribution

  type (redist_type), save :: g2x, x2y

  ! fft

  !RN> save attribute is required for g95
  type (fft_type), save :: xf_fft, xb_fft, yf_fft, yb_fft, zf_fft, zf2_fft, zf3_fft
  type (fft_type), save :: yf4d_fft, yb4d_fft ! 4d_accel
  type (fft_type), save :: forward_z, backward_z ! EGH
  type (fft_type), save :: xf3d_cr, xf3d_rc

  logical :: xfft_initted = .false.

! accel will be set to true if the v layout is used AND the number of
! PEs is such that each PE has a complete copy of the x,y space --
! in that case, no communication is needed to evaluate the nonlinear
! terms
  logical :: accel = .false.

  logical :: test = .false.

  logical, save, dimension (:), allocatable :: aidx  ! aidx == aliased index
  integer, save, dimension (:), allocatable :: ia, iak
  complex, save, dimension (:, :), allocatable :: fft, xxf
  complex, save, dimension (:, :, :), allocatable :: ag

  logical :: debug = .false.

contains

  subroutine init_transforms &
       (ntgrid, naky, nakx, nlambda, negrid, nspec, nx, ny, accelerated)
    use mp, only: nproc
    use mp, only: proc0
    use redistribute, only: report_map_property
    use agk_layouts, only: init_agk_layouts
    use agk_layouts, only: pe_layout, init_accel_transform_layouts, allow_accel
    use agk_layouts, only: init_y_transform_layouts
    use agk_layouts, only: init_x_transform_layouts
    use fft_work, only: load_wisdom, save_wisdom
    use file_utils, only: run_name
    implicit none
    integer, intent (in) :: ntgrid, naky, nakx, nlambda, negrid, nspec
    integer, intent (in) :: nx, ny
    logical, intent (out) :: accelerated
    
    logical, save :: initialized = .false.
    character (1) :: char
    
    character (len=300) :: fft_wisdom_file

! BUG FIX for graphics output, George Stantchev 8.16.06
! (Incorrect value was returned after first call)
    accelerated = accel

    if (initialized) return
    initialized = .true.

    call get_wisdom_file(fft_wisdom_file)

    call load_wisdom(trim(fft_wisdom_file))

    call init_agk_layouts

    call pe_layout (char)

    !Below changed to force accel=F if desired by allow_accel=F, GGH 10 JUL 07
    if (char == 'v' .and. mod (negrid*nlambda*nspec, nproc) == 0 .and. allow_accel) then  
       accel = .true.
       call init_accel_transform_layouts (ntgrid, naky, nakx, nlambda, negrid, nspec, nx, ny)
    else
       call init_y_redist (ntgrid, naky, nakx, nlambda, negrid, nspec, nx, ny)
       if (test) then
          if (proc0) print *, '=== g2x map property ==='
          call report_map_property (g2x)
          if (proc0) print *, '=== x2y map property ==='
          call report_map_property (x2y)
       end if
    end if

!    write (*,*) 'accelerated = ',accel


! need these for movies
    call init_y_transform_layouts (ntgrid, naky, nakx, nlambda, negrid, nspec, nx, ny)
    call init_x_transform_layouts (ntgrid, naky, nakx, nlambda, negrid, nspec, nx)

    call init_y_fft (ntgrid)
    call init_z_transforms(ntgrid) 

    accelerated = accel

    if (proc0) call save_wisdom(trim(fft_wisdom_file))

  end subroutine init_transforms

  subroutine init_z_transforms(ntgrid)
    
    integer, intent (in) :: ntgrid
    !logical, save :: initialized = .false.

    !if (initialized) return
    !initialized = .true.

    call init_forward_z(ntgrid) ! EGH
    call init_backward_z(ntgrid) ! EGH

  end subroutine init_z_transforms

!!! not used
!!$  subroutine init_x_transform (ntgrid, naky, nakx, nlambda, negrid, nspec, nx)
!!$
!!$    use mp, only: nproc
!!$    use fft_work, only: init_ccfftw
!!$    use agk_layouts, only: xxf_lo, pe_layout, allow_accel
!!$    integer, intent (in) :: ntgrid, naky, nakx, nlambda, negrid, nspec, nx
!!$
!!$    logical, save :: initialized = .false.
!!$    character (1) :: char
!!$
!!$    if (initialized) return
!!$    initialized = .true.
!!$
!!$    call pe_layout (char)
!!$
!!$    !Below changed to force accel=F if desired by allow_accel=F, GGH 10 JUL 07
!!$    if (char == 'v' .and. mod (negrid*nlambda*nspec, nproc) == 0 .and. allow_accel) then
!!$       accel = .true.
!!$    else
!!$       call init_x_redist (ntgrid, naky, nakx, nlambda, negrid, nspec, nx)
!!$
!!$# if FFT == _FFTW_
!!$       call init_ccfftw (xf_fft,  1, xxf_lo%nx)
!!$       call init_ccfftw (xb_fft, -1, xxf_lo%nx)
!!$# elif FFT == _FFTW3_
!!$       write(6,*) 'init_x_transform'
!!$       if (.not.allocated(xxf)) allocate(xxf(xxf_lo%nx,xxf_lo%llim_proc:xxf_lo%ulim_alloc))
!!$       call init_ccfftw (xf_fft,  1, xxf_lo%nx, xxf_lo%ulim_alloc-xxf_lo%llim_proc+1, xxf)
!!$       call init_ccfftw (xb_fft, -1, xxf_lo%nx, xxf_lo%ulim_alloc-xxf_lo%llim_proc+1, xxf)
!!$# endif
!!$
!!$       xfft_initted = .true.
!!$    end if
!!$
!!$  end subroutine init_x_transform

  subroutine init_y_fft (ntgrid)

    use agk_layouts, only: xxf_lo, yxf_lo, accel_lo, accelx_lo, dealiasing, g_lo
    use fft_work, only: init_crfftw, init_rcfftw, init_ccfftw
    use agk_mem, only: alloc8
    implicit none
    integer, intent (in) :: ntgrid

    logical :: initialized = .false.
    integer :: idx, i

    if (initialized) return
    initialized = .true.

    if (accel) then

! prepare for dealiasing 
       allocate (ia (accel_lo%nia)); call alloc8(i1=ia,v="ia")
       allocate (iak(accel_lo%nia)); call alloc8(i1=iak,v="iak")
       allocate (aidx (accel_lo%llim_proc:accel_lo%ulim_alloc)) !!! RN> unable to account memory for logical variables

       allocate (ag (-ntgrid:ntgrid, 2, accel_lo%llim_proc:accel_lo%ulim_alloc)) ; call alloc8 (c3=ag, v="ag")
       aidx = .true.

       idx = g_lo%llim_proc
       do i = accel_lo%llim_proc, accel_lo%ulim_proc
          if (dealiasing(accel_lo, i)) cycle
          aidx(i) = .false.
          idx = idx + 1
       end do

       do idx = 1, accel_lo%nia
          ia (idx) = accelx_lo%llim_proc + (idx-1)*accelx_lo%nxny
          iak(idx) = accel_lo%llim_proc  + (idx-1)*accel_lo%nxnky
       end do

# if FFT == _FFTW_
       call init_crfftw (yf_fft,  1, accel_lo%ny, accel_lo%nx)
       call init_rcfftw (yb_fft, -1, accel_lo%ny, accel_lo%nx)
       call init_crfftw (yf4d_fft,  1, accel_lo%ny, accel_lo%nx)
       call init_rcfftw (yb4d_fft, -1, accel_lo%ny, accel_lo%nx)
# elif FFT == _FFTW3_
       i=(2*accel_lo%ntgrid+1)*2
       call init_crfftw (yf_fft,  1, accel_lo%ny, accel_lo%nx, i)
       call init_rcfftw (yb_fft, -1, accel_lo%ny, accel_lo%nx, i)
       i=2
       call init_crfftw (yf4d_fft,  1, accel_lo%ny, accel_lo%nx, i)
       call init_rcfftw (yb4d_fft, -1, accel_lo%ny, accel_lo%nx, i)
# endif

    else

       allocate (fft(yxf_lo%ny/2+1, yxf_lo%llim_proc:yxf_lo%ulim_alloc)) ; call alloc8 (c2=fft, v="fft")
       if (.not.allocated(xxf)) &
            & allocate (xxf(xxf_lo%nx,xxf_lo%llim_proc:xxf_lo%ulim_alloc)) ; call alloc8 (c2=xxf, v="xxf")

       if (.not. xfft_initted) then

# if FFT == _FFTW_
          call init_ccfftw (xf_fft,  1, xxf_lo%nx)
          call init_ccfftw (xb_fft, -1, xxf_lo%nx)
# elif FFT == _FFTW3_
          call init_ccfftw (xf_fft,  1, xxf_lo%nx, xxf_lo%ulim_alloc-xxf_lo%llim_proc+1, xxf)
          call init_ccfftw (xb_fft, -1, xxf_lo%nx, xxf_lo%ulim_alloc-xxf_lo%llim_proc+1, xxf)
# endif

          xfft_initted = .true.
       end if

# if FFT == _FFTW_
       call init_crfftw (yf_fft,  1, yxf_lo%ny)
       call init_rcfftw (yb_fft, -1, yxf_lo%ny)
# elif FFT == _FFTW3_
       call init_crfftw (yf_fft,  1, yxf_lo%ny, yxf_lo%ulim_alloc-yxf_lo%llim_proc+1)
       call init_rcfftw (yb_fft, -1, yxf_lo%ny, yxf_lo%ulim_alloc-yxf_lo%llim_proc+1)
# endif

    end if

  end subroutine init_y_fft

  subroutine init_x_redist (ntgrid, naky, nakx, nlambda, negrid, nspec, nx)
    use agk_layouts, only: init_x_transform_layouts
    use agk_layouts, only: g_lo, xxf_lo, proc_id, idx_local
    use agk_layouts, only: ik_idx, it_idx, il_idx, ie_idx, is_idx, idx
    use mp, only: nproc
    use redistribute, only: index_list_type, init_redist, delete_list
    implicit none
    type (index_list_type), dimension(0:nproc-1) :: to_list, from_list
    integer, dimension(0:nproc-1) :: nn_to, nn_from
    integer, dimension (3) :: from_low, from_high
    integer, dimension (2) :: to_high
    integer :: to_low
    integer, intent (in) :: ntgrid, naky, nakx, nlambda, negrid, nspec, nx
    logical :: initialized = .false.

    integer :: iglo, isign, ig, it, ixxf
    integer :: n, ip
    integer :: ik, il, ie, is

    if (initialized) return
    initialized = .true.

    call init_x_transform_layouts &
         (ntgrid, naky, nakx, nlambda, negrid, nspec, nx)

    ! count number of elements to be redistributed to/from each processor
    nn_to = 0
    nn_from = 0
    do iglo = g_lo%llim_world, g_lo%ulim_world
! TT: inline expansion of gidx2xxfidx
       ik = ik_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       do isign = 1, 2
          do ig = -ntgrid, ntgrid
             ixxf = idx(xxf_lo, ig, isign, ik, il, ie, is)
             if (idx_local(g_lo,iglo)) &
                  nn_from(proc_id(xxf_lo,ixxf)) = nn_from(proc_id(xxf_lo,ixxf)) + 1
             if (idx_local(xxf_lo,ixxf)) &
                nn_to(proc_id(g_lo,iglo)) = nn_to(proc_id(g_lo,iglo)) + 1
          end do
       end do
    end do

    do ip = 0, nproc-1
       if (nn_from(ip) > 0) then
          allocate (from_list(ip)%first(nn_from(ip))) !!! RN> unable to account memory for structure variables
          allocate (from_list(ip)%second(nn_from(ip))) !!! RN> unable to account memory for structure variables
          allocate (from_list(ip)%third(nn_from(ip))) !!! RN> unable to account memory for structure variables
       end if
       if (nn_to(ip) > 0) then
          allocate (to_list(ip)%first(nn_to(ip))) !!! RN> unable to account memory for structure variables
          allocate (to_list(ip)%second(nn_to(ip))) !!! RN> unable to account memory for structure variables
       end if
    end do


    ! get local indices of elements distributed to/from other processors
    nn_to = 0
    nn_from = 0
    do iglo = g_lo%llim_world, g_lo%ulim_world
! TT: inline expansion of gidx2xxfidx
       ik = ik_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       if (it > (xxf_lo%nakx+1)/2) then
          it = it - xxf_lo%nakx + xxf_lo%nx
       end if
       do isign = 1, 2
          do ig = -ntgrid, ntgrid
             ixxf = idx(xxf_lo, ig, isign, ik, il, ie, is)
             if (idx_local(g_lo,iglo)) then
                ip = proc_id(xxf_lo,ixxf)
                n = nn_from(ip) + 1
                nn_from(ip) = n
                from_list(ip)%first(n) = ig
                from_list(ip)%second(n) = isign
                from_list(ip)%third(n) = iglo
             end if
             if (idx_local(xxf_lo,ixxf)) then
                ip = proc_id(g_lo,iglo)
                n = nn_to(ip) + 1
                nn_to(ip) = n
                to_list(ip)%first(n) = it
                to_list(ip)%second(n) = ixxf
             end if
          end do
       end do
    end do

    from_low (1) = -ntgrid
    from_low (2) = 1
    from_low (3) = g_lo%llim_proc

    to_low = xxf_lo%llim_proc
    
    to_high(1) = xxf_lo%nx
    to_high(2) = xxf_lo%ulim_alloc

    from_high(1) = ntgrid
    from_high(2) = 2
    from_high(3) = g_lo%ulim_alloc

    call init_redist (g2x, 'c', to_low, to_high, to_list, &
         from_low, from_high, from_list)

    call delete_list (to_list)
    call delete_list (from_list)

  end subroutine init_x_redist

  subroutine init_y_redist (ntgrid, naky, nakx, nlambda, negrid, nspec, nx, ny)
    use agk_layouts, only: init_y_transform_layouts
    use agk_layouts, only: xxf_lo, yxf_lo, proc_id, idx_local
    use agk_layouts, only: ig_idx, ik_idx, isign_idx, il_idx, ie_idx, is_idx, idx
    use mp, only: nproc
    use redistribute, only: index_list_type, init_redist, delete_list
    implicit none
    type (index_list_type), dimension(0:nproc-1) :: to_list, from_list
    integer, dimension(0:nproc-1) :: nn_to, nn_from
    integer, dimension (2) :: from_low, from_high, to_high
    integer :: to_low
    integer, intent (in) :: ntgrid, naky, nakx, nlambda, negrid, nspec
    integer, intent (in) :: nx, ny

    integer :: it, ixxf, ik, iyxf
    integer :: n, ip
    integer :: ig, isign, il, ie, is
    logical :: initialized = .false.

    if (initialized) return
    initialized = .true.

    call init_x_redist (ntgrid, naky, nakx, nlambda, negrid, nspec, nx)
    call init_y_transform_layouts &
         (ntgrid, naky, nakx, nlambda, negrid, nspec, nx, ny)

    ! count number of elements to be redistributed to/from each processor
    nn_to = 0
    nn_from = 0
    do ixxf = xxf_lo%llim_world, xxf_lo%ulim_world
! TT: inline expansion of xxfidx2yxfidx
       ig = ig_idx(xxf_lo,ixxf)
       isign = isign_idx(xxf_lo,ixxf)
       il = il_idx(xxf_lo,ixxf)
       ie = ie_idx(xxf_lo,ixxf)
       is = is_idx(xxf_lo,ixxf)
       do it = 1, yxf_lo%nx
          iyxf = idx(yxf_lo, ig, isign, it, il, ie, is)
          if (idx_local(xxf_lo,ixxf)) &
             nn_from(proc_id(yxf_lo,iyxf)) = nn_from(proc_id(yxf_lo,iyxf)) + 1
          if (idx_local(yxf_lo,iyxf)) &
             nn_to(proc_id(xxf_lo,ixxf)) = nn_to(proc_id(xxf_lo,ixxf)) + 1
       end do
    end do

    do ip = 0, nproc-1
       if (nn_from(ip) > 0) then
          allocate (from_list(ip)%first(nn_from(ip))) !!! RN> unable to account memory for structure variables
          allocate (from_list(ip)%second(nn_from(ip))) !!! RN> unable to account memory for structure variables
       end if
       if (nn_to(ip) > 0) then
          allocate (to_list(ip)%first(nn_to(ip))) !!! RN> unable to account memory for structure variables
          allocate (to_list(ip)%second(nn_to(ip))) !!! RN> unable to account memory for structure variables
       end if
    end do

    ! get local indices of elements distributed to/from other processors
    nn_to = 0
    nn_from = 0
    do ixxf = xxf_lo%llim_world, xxf_lo%ulim_world
! TT: inline expansion of xxfidx2yxfidx
       ig = ig_idx(xxf_lo,ixxf)
       ik = ik_idx(xxf_lo,ixxf)
       isign = isign_idx(xxf_lo,ixxf)
       il = il_idx(xxf_lo,ixxf)
       ie = ie_idx(xxf_lo,ixxf)
       is = is_idx(xxf_lo,ixxf)
       do it = 1, yxf_lo%nx
          iyxf = idx(yxf_lo, ig, isign, it, il, ie, is)
          if (idx_local(xxf_lo,ixxf)) then
             ip = proc_id(yxf_lo,iyxf)
             n = nn_from(ip) + 1
             nn_from(ip) = n
             from_list(ip)%first(n) = it
             from_list(ip)%second(n) = ixxf
          end if
          if (idx_local(yxf_lo,iyxf)) then
             ip = proc_id(xxf_lo,ixxf)
             n = nn_to(ip) + 1
             nn_to(ip) = n
             to_list(ip)%first(n) = ik
             to_list(ip)%second(n) = iyxf
          end if
       end do
    end do

    from_low(1) = 1
    from_low(2) = xxf_lo%llim_proc

    to_low = yxf_lo%llim_proc

    to_high(1) = yxf_lo%ny/2+1
    to_high(2) = yxf_lo%ulim_alloc

    from_high(1) = xxf_lo%nx
    from_high(2) = xxf_lo%ulim_alloc

    call init_redist (x2y, 'c', to_low, to_high, to_list, &
         from_low, from_high, from_list)

    call delete_list (to_list)
    call delete_list (from_list)
    
  end subroutine init_y_redist

  subroutine transform_x5d (g, xxf)
    use agk_layouts, only: xxf_lo, g_lo
    use redistribute, only: measure_gather
    use agk_mem, only: alloc8, dealloc8
    implicit none
    complex, dimension (-xxf_lo%ntgrid:,:,g_lo%llim_proc:), intent (in) :: g
    complex, dimension (:,xxf_lo%llim_proc:), intent (out) :: xxf
    integer :: i

# if FFT == _FFTW_
    complex, dimension(:), allocatable :: aux
# endif
    
    ! intent statement in gather actually makes this next line non-standard: 
    xxf = 0.
    call measure_gather (g2x, g, xxf)

    ! do ffts
    i = xxf_lo%ulim_proc - xxf_lo%llim_proc + 1

# if FFT == _FFTW_
    allocate (aux(xf_fft%n)); call alloc8(c1=aux,v="aux")
    call fftw_f77 (xf_fft%plan, i, xxf, 1, xxf_lo%nx, aux, 0, 0)
    call dealloc8(c1=aux,v="aux") ! deallocate (aux)
#elif FFT == _FFTW3_
    call FFTW_PREFIX(_execute) (xf_fft%plan)
# endif

  end subroutine transform_x5d

  subroutine inverse_x5d (xxf, g)
    use agk_layouts, only: xxf_lo, g_lo
    use redistribute, only: measure_scatter
    use agk_mem, only: alloc8, dealloc8
    implicit none
    complex, dimension (:,xxf_lo%llim_proc:), intent (in out) :: xxf
    complex, dimension (-xxf_lo%ntgrid:,:,g_lo%llim_proc:), intent (out) :: g
    integer :: i

# if FFT == _FFTW_
    complex, dimension(:), allocatable :: aux
# endif
    
    i = xxf_lo%ulim_proc - xxf_lo%llim_proc + 1

    ! do ffts
# if FFT == _FFTW_
    allocate (aux(xb_fft%n)); call alloc8(c1=aux,v="aux")
    call fftw_f77 (xb_fft%plan, i, xxf, 1, xxf_lo%nx, aux, 0, 0)
    call dealloc8(c1=aux,v="aux") ! deallocate (aux)
#elif FFT == _FFTW3_
    call FFTW_PREFIX(_execute) (xb_fft%plan)
# endif

    call measure_scatter (g2x, xxf, g)

  end subroutine inverse_x5d

  subroutine transform_y5d (xxf, yxf)
    use agk_layouts, only: xxf_lo, yxf_lo
    use redistribute, only: measure_gather
    implicit none
    complex, dimension (:,xxf_lo%llim_proc:), intent (in) :: xxf
# ifdef FFT
    real, dimension (:,yxf_lo%llim_proc:), intent (out) :: yxf
# else
    real, dimension (:,yxf_lo%llim_proc:) :: yxf
# endif
# if FFT == _FFTW_
    integer :: i
# endif

    fft = 0.
    call measure_gather (x2y, xxf, fft)

    ! do ffts
# if FFT == _FFTW_
    i = yxf_lo%ulim_proc - yxf_lo%llim_proc + 1
    call rfftwnd_f77_complex_to_real (yf_fft%plan, i, fft, 1, yxf_lo%ny/2+1, yxf, 1, yxf_lo%ny)
# elif FFT == _FFTW3_
    call FFTW_PREFIX(_execute_dft_c2r) (yf_fft%plan, fft, yxf)
# endif

  end subroutine transform_y5d

  subroutine inverse_y5d (yxf, xxf)
    use agk_layouts, only: xxf_lo, yxf_lo
    use redistribute, only: measure_scatter
    implicit none
    real, dimension (:,yxf_lo%llim_proc:), intent (in out) :: yxf
    complex, dimension (:,xxf_lo%llim_proc:), intent (out) :: xxf
# if FFT == _FFTW_
    integer :: i
# endif

    ! do ffts
# if FFT == _FFTW_
    i = yxf_lo%ulim_proc - yxf_lo%llim_proc + 1
    call rfftwnd_f77_real_to_complex (yb_fft%plan, i, yxf, 1, yxf_lo%ny, fft, 1, yxf_lo%ny/2+1)
# elif FFT == _FFTW3_
    call FFTW_PREFIX(_execute_dft_r2c) (yb_fft%plan, yxf, fft)
# endif

    call measure_scatter (x2y, fft, xxf)

  end subroutine inverse_y5d

  !! TT: transform2 changes input data for 5d !!
  subroutine transform2_5d (g, yxf)
    use mp, only: proc0
    use agk_layouts, only: g_lo, yxf_lo, ik_idx
    implicit none
    complex, dimension (:,:,g_lo%llim_proc:), intent (in out) :: g
    real, dimension (:,yxf_lo%llim_proc:), intent (out) :: yxf
    integer :: iglo
    logical, save :: init_dbg = .true.
    if (debug .and. proc0 .and. init_dbg) then
       write(6,*) 'transform2_5d is called'
       init_dbg=.false.
    end if
    
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       if (ik_idx(g_lo, iglo) == 1) cycle
       g(:,:,iglo) = g(:,:,iglo) / 2.0
    end do

    call transform_x (g, xxf)
    call transform_y (xxf, yxf)

  end subroutine transform2_5d

  subroutine inverse2_5d (yxf, g)
    use mp, only: proc0
    use agk_layouts, only: g_lo, yxf_lo, ik_idx
    implicit none
    real, dimension (:,yxf_lo%llim_proc:), intent (in out) :: yxf
    complex, dimension (:,:,g_lo%llim_proc:), intent (out) :: g
    integer :: iglo
    logical, save :: init_dbg = .true.
    if (debug .and. proc0 .and. init_dbg) then
       write(6,*) 'inverse2_5d is called'
       init_dbg=.false.
    end if

    call inverse_y (yxf, xxf)
    call inverse_x (xxf, g)

    g = g * xb_fft%scale * yb_fft%scale

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       if (ik_idx(g_lo, iglo) == 1) cycle
       g(:,:,iglo) = g(:,:,iglo) * 2.0
    end do

  end subroutine inverse2_5d

  !! TT: transform2 changes input data for 5d !!
  subroutine transform2_5d_accel (g, axf, i)
    use mp, only: proc0
    use agk_layouts, only: g_lo, accel_lo, accelx_lo, ik_idx
    implicit none
    complex, dimension (:,:,g_lo%llim_proc:), intent (in out) :: g
# ifdef FFT
    real, dimension (:,:,accelx_lo%llim_proc:), intent (out) :: axf
# else
    real, dimension (:,:,accelx_lo%llim_proc:) :: axf
# endif
    integer :: iglo, k, i, idx
    logical, save :: init_dbg = .true.
    if (debug .and. proc0 .and. init_dbg) then
       write(6,*) 'transform2_5d_accel is called'
       init_dbg=.false.
    end if

! scale ky /= 0 modes
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       if (ik_idx(g_lo, iglo) == 1) cycle   ! there is a mod call in this statement
       g(:,:,iglo) = g(:,:,iglo) / 2.0
    end do

! dealias
    ag = 0.   
    idx = g_lo%llim_proc
    do k = accel_lo%llim_proc, accel_lo%ulim_proc
       if (aidx(k)) cycle
       ag(:,:,k) = g(:,:,idx)
       idx = idx + 1
    end do

! transform
    i = (2*accel_lo%ntgrid+1)*2
    idx = 1
    do k = accel_lo%llim_proc, accel_lo%ulim_proc, accel_lo%nxnky
# if FFT == _FFTW_
       call rfftwnd_f77_complex_to_real (yf_fft%plan, i, ag(:,:,k:), i, 1, &
            axf(:,:,ia(idx):), i, 1)
# elif FFT == _FFTW3_
       call FFTW_PREFIX(_execute_dft_c2r) (yf_fft%plan, ag(:,:,k:), axf(:,:,ia(idx):))
# endif

       idx = idx + 1
    end do

  end subroutine transform2_5d_accel

  subroutine inverse2_5d_accel (axf, g, i)
    use mp, only: proc0
    use agk_layouts, only: g_lo, accel_lo, accelx_lo, ik_idx
    implicit none
    real, dimension (:,:,accelx_lo%llim_proc:), intent (in out) :: axf
    complex, dimension (:,:,g_lo%llim_proc:), intent (out) :: g
    integer :: iglo, i, idx, k
    logical, save :: init_dbg = .true.
    if (debug .and. proc0 .and. init_dbg) then
       write(6,*) 'inverse2_5d_accel is called'
       init_dbg=.false.
    end if
    
! transform
    i = (2*accel_lo%ntgrid+1)*2
    idx = 1
    do k = accelx_lo%llim_proc, accelx_lo%ulim_proc, accelx_lo%nxny

# if FFT == _FFTW_
       call rfftwnd_f77_real_to_complex (yb_fft%plan, i, axf(:,:,k:), i, 1, &
            ag(:,:,iak(idx):), i, 1)
# elif FFT == _FFTW3_
       call FFTW_PREFIX(_execute_dft_r2c) (yb_fft%plan, axf(:,:,k:), ag(:,:,iak(idx):))
# endif

       idx = idx + 1
    end do

! dealias and scale
    idx = g_lo%llim_proc
    do k = accel_lo%llim_proc, accel_lo%ulim_proc
       if (aidx(k)) cycle
       g(:,:,idx) = ag(:,:,k) * yb_fft%scale
       idx = idx + 1
    end do

! scale
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       if (ik_idx(g_lo, iglo) == 1) cycle
       g(:,:,iglo) = g(:,:,iglo) * 2.0
    end do

  end subroutine inverse2_5d_accel

  !! TT: transform2 changes input data for 4d !!
  subroutine transform2_4d_accel (g, axf, i)
    use mp, only: proc0
    use agk_layouts, only: g_lo, accel_lo, accelx_lo, ik_idx
    implicit none
    complex, dimension (:,g_lo%llim_proc:), intent (in out) :: g
# ifdef FFT
    real, dimension (:,accelx_lo%llim_proc:), intent (out) :: axf
# else
    real, dimension (:,accelx_lo%llim_proc:) :: axf
# endif
    integer :: iglo, k, i, idx
    logical, save :: init_dbg = .true.
    if (debug .and. proc0 .and. init_dbg) then
       write(6,*) 'transform2_4d_accel is called'
       init_dbg=.false.
    end if
    
! scale ky /= 0 modes
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       if (ik_idx(g_lo, iglo) == 1) cycle   ! there is a mod call in this statement
       g(:,iglo) = g(:,iglo) / 2.0
    end do

! dealias
    ag = 0.   
    idx = g_lo%llim_proc
    do k = accel_lo%llim_proc, accel_lo%ulim_proc
       if (aidx(k)) cycle
       ag(1,:,k) = g(:,idx)
       idx = idx + 1
    end do

! transform
!    i = (2*accel_lo%ntgrid+1)*2
    i = 2
    idx = 1
    do k = accel_lo%llim_proc, accel_lo%ulim_proc, accel_lo%nxnky

# if FFT == _FFTW_
       call rfftwnd_f77_complex_to_real (yf4d_fft%plan, i, ag(1,:,k:), i, 1, &
            axf(:,ia(idx):), i, 1)
# elif FFT == _FFTW3_
       call FFTW_PREFIX(_execute_dft_c2r) (yf4d_fft%plan, ag(1,:,k:), axf(:,ia(idx):))
# endif

       idx = idx + 1
    end do
    
  end subroutine transform2_4d_accel

  subroutine inverse2_4d_accel (axf, g, i)
    use mp, only: proc0
    use agk_layouts, only: g_lo, accel_lo, accelx_lo, ik_idx
    implicit none
    real, dimension (:,accelx_lo%llim_proc:), intent (in out) :: axf
    complex, dimension (:,g_lo%llim_proc:), intent (out) :: g
    integer :: iglo, i, idx, k
    logical, save :: init_dbg = .true.
    if (debug .and. proc0 .and. init_dbg) then
       write(6,*) 'inverse2_4d_accel is called'
       init_dbg=.false.
    end if

! transform
!    i = (2*accel_lo%ntgrid+1)*2
    i = 2
    idx = 1
    do k = accelx_lo%llim_proc, accelx_lo%ulim_proc, accelx_lo%nxny

# if FFT == _FFTW_
       call rfftwnd_f77_real_to_complex (yb4d_fft%plan, i, axf(:,k:), i, 1, &
            ag(1,:,iak(idx):), i, 1)
# elif FFT == _FFTW3_
       call FFTW_PREFIX(_execute_dft_r2c) (yb4d_fft%plan, axf(:,k:), ag(1,:,iak(idx):))
# endif
       
       idx = idx + 1
    end do

! dealias and scale
    idx = g_lo%llim_proc
    do k = accel_lo%llim_proc, accel_lo%ulim_proc
       if (aidx(k)) cycle
       g(:,idx) = ag(1,:,k) * yb4d_fft%scale
       idx = idx + 1
    end do

! scale
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       if (ik_idx(g_lo, iglo) == 1) cycle
       g(:,iglo) = g(:,iglo) * 2.0
    end do

  end subroutine inverse2_4d_accel

  subroutine init_3d (nny_in, nnx_in, how_many_in)

    use fft_work, only: init_crfftw, init_rcfftw, delete_fft
    logical :: initialized = .false.
    integer, intent(in) :: nny_in, nnx_in, how_many_in
    integer, save :: nnx, nny
    integer, save :: how_many
    
    if (initialized) then
       if (nnx /= nnx_in .or. nny /= nny_in) then
          call delete_fft(xf3d_cr)
          call delete_fft(xf3d_rc)
# if FFT == _FFTW3_
       else if (how_many /= how_many_in) then
          call delete_fft(xf3d_cr)
          call delete_fft(xf3d_rc)
# endif
       else
          return
       end if
    end if
    initialized = .true.
    nny = nny_in
    nnx = nnx_in
    how_many = how_many_in

# if FFT == _FFTW_
    call init_crfftw (xf3d_cr,  1, nny, nnx)
    call init_rcfftw (xf3d_rc, -1, nny, nnx)
# elif FFT == _FFTW3_
    call init_crfftw (xf3d_cr,  1, nny, nnx, how_many)
    call init_rcfftw (xf3d_rc, -1, nny, nnx, how_many)
# endif
    
  end subroutine init_3d

  subroutine transform2_3d (phi, phixf, nny, nnx)
    use mp, only: proc0
    use theta_grid, only: ntgrid
    use kgrids, only: naky, nakx, nx, aky
    use agk_mem, only: alloc8, dealloc8
    implicit none
    integer :: nnx, nny
    complex, dimension (-ntgrid:,:,:), intent (in) :: phi
    real, dimension (:,:,-ntgrid:), intent (out) :: phixf  
    real, dimension (:,:,:), allocatable :: phix
    complex, dimension (:,:,:), allocatable :: aphi

    real :: fac
    integer :: ig, ik, it, i

    logical, save :: init_dbg = .true.
    if (debug .and. proc0 .and. init_dbg) then
       write(6,*) 'transform2_3d is called'
       init_dbg=.false.
    end if
    
! scale, dealias and transpose

!!!    call init_3d (nny, nnx)
    call init_3d (nny, nnx, 2*ntgrid+1)
    
    allocate (phix (-ntgrid:ntgrid, nny, nnx)) ; call alloc8 (r3=phix, v="phix")
    allocate (aphi (-ntgrid:ntgrid, nny/2+1, nnx)) ; call alloc8 (c3=aphi, v="aphi")

    aphi = 0.

    do ik=1,naky
       fac = 0.5
       if (aky(ik) < epsilon(0.)) fac = 1.0
       do it=1,(nakx+1)/2
          do ig=-ntgrid, ntgrid
             aphi(ig,ik,it) = phi(ig,it,ik)*fac
          end do
       end do
       do it=(nakx+1)/2+1,nakx
          do ig=-ntgrid, ntgrid
             aphi(ig,ik,it-nakx+nx) = phi(ig,it,ik)*fac
          end do
       end do
    end do

! transform
    i = 2*ntgrid+1
# if FFT == _FFTW_
    call rfftwnd_f77_complex_to_real (xf3d_cr%plan, i, aphi, i, 1, phix, i, 1)
# elif FFT == _FFTW3_
    call FFTW_PREFIX(_execute_dft_c2r) (xf3d_cr%plan, aphi, phix)
# endif

    do it=1,nnx
       do ik=1,nny
          do ig=-ntgrid, ntgrid
             phixf (it,ik,ig) = phix (ig,ik,it)
          end do
       end do
    end do

    call dealloc8 (r3=phix, v="phix") 
    call dealloc8 (c3=aphi, v="aphi") 

  end subroutine transform2_3d
  
  subroutine inverse2_3d (phixf, phi, nny, nnx)
    use mp, only: proc0
    use theta_grid, only: ntgrid
    use kgrids, only: naky, nakx, aky
    use agk_mem, only: alloc8, dealloc8
    implicit none
    real, dimension (:,:,-ntgrid:):: phixf
    complex, dimension (-ntgrid:,:,:) :: phi
    integer :: nnx, nny
    complex, dimension (:,:,:), allocatable :: aphi
    real, dimension (:,:,:), allocatable :: phix
    real :: fac
    integer :: i, ik, it, ig
    
    logical, save :: init_dbg = .true.
    if (debug .and. proc0 .and. init_dbg) then
       write(6,*) 'inverse2_3d is called'
       init_dbg=.false.
    end if
    
    allocate (aphi (-ntgrid:ntgrid, nny/2+1, nnx)) ; call alloc8 (c3=aphi, v="aphi") 
    allocate (phix (-ntgrid:ntgrid, nny, nnx))     ; call alloc8 (r3=phix, v="phix")

    do it=1,nnx
       do ik=1,nny
          do ig=-ntgrid, ntgrid
             phix (ig,ik,it) = phixf (it,ik,ig)
          end do
       end do
    end do

! transform
    i = 2*ntgrid+1
# if FFT == _FFTW_
    call rfftwnd_f77_real_to_complex (xf3d_rc%plan, i, phix, i, 1, aphi, i, 1)
# elif FFT == _FFTW3_
    call FFTW_PREFIX(_execute_dft_r2c) (xf3d_rc%plan, phix, aphi)
# endif

! dealias and scale
    do it=1,nakx
       do ik=1,naky
          fac = 2.0
          if (aky(ik) < epsilon(0.0)) fac = 1.0
          do ig=-ntgrid, ntgrid
             phi (ig,it,ik) = aphi (ig,ik,it)*fac*xf3d_rc%scale
          end do
       end do
    end do

    call dealloc8 (r3=phix, v="phix") 
    call dealloc8 (c3=aphi, v="aphi") 

  end subroutine inverse2_3d

  subroutine transform2_2d (phi, phixf, nny, nnx)
    use mp, only: proc0
    use fft_work, only: FFTW_BACKWARD, delete_fft, init_crfftw
    use kgrids, only: naky, nakx, nx, aky
    use agk_mem, only: alloc8, dealloc8
    implicit none
    integer :: nnx, nny
    complex, intent (in) :: phi(:,:)
    real, intent (out) :: phixf(:,:)
    real, allocatable :: phix(:,:)
    complex, allocatable :: aphi(:,:)
    real :: fac
    integer :: ik, it
    type (fft_type) :: xf2d

    logical, save :: init_dbg = .true.
    if (debug .and. proc0 .and. init_dbg) then
       write(6,*) 'transform2_2d is called'
       init_dbg=.false.
    end if
    
# if FFT == _FFTW_
    call init_crfftw (xf2d, FFTW_BACKWARD, nny, nnx)
# elif FFT == _FFTW3_
    call init_crfftw (xf2d, FFTW_BACKWARD, nny, nnx, 1)
# endif
    
    allocate (phix (nny, nnx))     ; call alloc8 (r2=phix, v="phix")
    allocate (aphi (nny/2+1, nnx)) ; call alloc8 (c2=aphi, v="aphi") 
    phix = 0. ; aphi = 0.

! scale, dealias and transpose
    do ik=1,naky
       fac = 0.5
       if (aky(ik) < epsilon(0.)) fac = 1.0
       do it=1,(nakx+1)/2
          aphi(ik,it) = phi(it,ik)*fac
       end do
       do it=(nakx+1)/2+1,nakx
          aphi(ik,it-nakx+nx) = phi(it,ik)*fac
       end do
    end do

! transform
# if FFT == _FFTW_
    call rfftwnd_f77_complex_to_real (xf2d%plan, 1, aphi, 1, 1, phix, 1, 1)
# elif FFT == _FFTW3_
    call FFTW_PREFIX(_execute_dft_c2r) (xf2d%plan, aphi, phix)
# endif

    phixf(:,:)=transpose(phix(:,:))  ! Is this efficient?  BD

    call dealloc8 (r2=phix, v="phix") 
    call dealloc8 (c2=aphi, v="aphi") 

!RN> this statement causes error for lahey with DEBUG. I don't know why
!    call delete_fft(xf2d)
  end subroutine transform2_2d

  subroutine inverse2_2d (phixf, phi, nny, nnx)
    use mp, only: proc0
    use fft_work, only: FFTW_FORWARD, delete_fft, init_rcfftw
    use kgrids, only: naky, nakx, aky
    use agk_mem, only: alloc8, dealloc8
    implicit none
    real, intent(in) :: phixf(:,:)
    complex, intent(out) :: phi(:,:)
    integer :: nnx, nny
    complex, allocatable :: aphi(:,:)
    real, allocatable :: phix(:,:)
    real :: fac
    integer :: ik, it
    type (fft_type) :: xf2d

    logical, save :: init_dbg = .true.
    if (debug .and. proc0 .and. init_dbg) then
       write(6,*) 'inverse2_2d is called'
       init_dbg=.false.
    end if
    
# if FFT == _FFTW_
    call init_rcfftw (xf2d, FFTW_FORWARD, nny, nnx)
# elif FFT == _FFTW3_
    call init_rcfftw (xf2d, FFTW_FORWARD, nny, nnx, 1)
# endif    

    allocate (aphi (nny/2+1, nnx)) ; call alloc8 (c2=aphi, v="aphi")
    allocate (phix (nny, nnx)) ; call alloc8 (r2=phix, v="phix")
    phix = 0.  ;  aphi = 0.

! Is this really wise?  In the old days, the transpose intrinsic was very slow.  BD
    phix(:,:)=transpose(phixf(:,:))

! transform
# if FFT == _FFTW_
    call rfftwnd_f77_real_to_complex (xf2d%plan, 1, phix, 1, 1, aphi, 1, 1)
# elif FFT == _FFTW3_
    call FFTW_PREFIX(_execute_dft_r2c) (xf2d%plan, phix, aphi)
# endif

! scale, dealias and transpose
    do it=1,nakx
       do ik=1,naky
          fac = 2.0
          if (aky(ik) < epsilon(0.0)) fac = 1.0
          phi(it,ik) = aphi(ik,it)*fac*xf2d%scale
       end do
    end do

    call dealloc8 (r2=phix, v="phix")  
    call dealloc8 (c2=aphi, v="aphi")  

!RN> this statement causes error for lahey with DEBUG. I don't know why
!    call delete_fft(xf2d)
  end subroutine inverse2_2d

  subroutine transform2_4d (den, phixf, nny, nnx)
    use mp, only: proc0
    use theta_grid, only: ntgrid
    use kgrids, only: naky, nakx, nx, aky
    use agk_mem, only: alloc8, dealloc8
    implicit none
    integer :: nnx, nny
    complex, dimension (-ntgrid:,:,:,:), intent (in) :: den
    real, dimension (:,:,-ntgrid:), intent (out) :: phixf  
    real, dimension (:,:,:), allocatable :: phix
    complex, dimension (:,:,:), allocatable :: aphi
    real :: fac
    integer :: ig, ik, it, i

    logical, save :: init_dbg = .true.
    if (debug .and. proc0 .and. init_dbg) then
       write(6,*) 'transform2_4d is called'
       init_dbg=.false.
    end if
    
! scale, dealias and transpose

!!!    call init_3d (nny, nnx)
    call init_3d (nny, nnx, 2*ntgrid+1)

    allocate (phix (-ntgrid:ntgrid, nny, nnx)) ; call alloc8 (r3=phix, v="phix") 
    allocate (aphi (-ntgrid:ntgrid, nny/2+1, nnx)) ; call alloc8 (c3=aphi, v="aphi")
    aphi = 0.

    do ik=1,naky
       fac = 0.5
       if (aky(ik) < epsilon(0.)) fac = 1.0
       do it=1,(nakx+1)/2
          do ig=-ntgrid, ntgrid
             aphi(ig,ik,it) = den(ig,it,ik,1)*fac
          end do
       end do
       do it=(nakx+1)/2+1,nakx
          do ig=-ntgrid, ntgrid
             aphi(ig,ik,it-nakx+nx) = den(ig,it,ik,1)*fac
          end do
       end do
    end do

! transform
    i = 2*ntgrid+1
# if FFT == _FFTW_
    call rfftwnd_f77_complex_to_real (xf3d_cr%plan, i, aphi, i, 1, phix, i, 1)
# elif FFT == _FFTW3_
    call FFTW_PREFIX(_execute_dft_c2r) (xf3d_cr%plan, aphi, phix)
# endif

    do it=1,nnx
       do ik=1,nny
          do ig=-ntgrid, ntgrid
             phixf (it,ik,ig) = phix (ig,ik,it)
          end do
       end do
    end do

    call dealloc8 (r3=phix, v="phix") 
    call dealloc8 (c3=aphi, v="aphi") 

  end subroutine transform2_4d

!!! RN> 2017/02/15
!!! this routine is called in init_par_filter in agk_diagnostics.
!!! zf_fft is used in kz_transform and kz_spectrum
!!! zf2_fft is used in kz_tranform2 (real -> complex)
!RN  subroutine init_zf (ntgrid, nperiod)
  subroutine init_zf (ntgrid, howmany)

    use agk_layouts, only: g_lo
    use fft_work, only: init_z, FFTW_FORWARD
    implicit none
!RN    integer, intent (in) :: ntgrid, nperiod
    integer, intent (in) :: ntgrid, howmany
    logical :: done = .false.
    integer :: i
    
    if (done) return
    done = .true.

    ! I think that this is buggy. The number of gridpoints in
    ! the z direction is 2 * ntgrid + 1, not 2*ntgrid
    ! Also, the correct value for  FFTW_FORWARD is -1, not 1
!RN    call init_z (zf_fft, 1, 2*ntgrid)
!RN    call init_z (zf2_fft, -1, 2*ntgrid)
    call init_z (zf_fft, FFTW_FORWARD, 2*ntgrid, howmany) ! kz_spectrum ... howmany = nakx*naky
    call init_z (zf2_fft, FFTW_FORWARD, 2*ntgrid, 1)
    i = 2*(g_lo%ulim_proc-g_lo%llim_proc+1)
    call init_z (zf3_fft, FFTW_FORWARD, 2*ntgrid, i) ! kz_transform .. howmany =
                                                           ! g_lo%ulim_proc-g_lo%llim_proc+1
    
  end subroutine init_zf

  !> Creates a plan (forward_z) for doing a forward transform 
  !! along the z (i.e. theta) axis. EGH

  subroutine init_forward_z (ntgrid)

    use fft_work, only: init_z, FFTW_FORWARD
    implicit none
    integer, intent (in) :: ntgrid
    logical,save :: done = .false.

    if (done) return
    done = .true.

    call init_z (forward_z, FFTW_FORWARD, 2*ntgrid+1, 1)

  end subroutine init_forward_z
  
  !> Creates a plan (backward_z) for doing a forward transform 
  !! along the z (i.e. theta) axis. EGH
  
  subroutine init_backward_z (ntgrid)

    use fft_work, only: init_z, FFTW_BACKWARD
    implicit none
    integer, intent (in) :: ntgrid
    logical,save :: done = .false.

    if (done) return
    done = .true.

    call init_z (backward_z, FFTW_BACKWARD, 2*ntgrid+1, 1)

  end subroutine init_backward_z
    
  !> Forward fourier transform a complex array in the z dirn

  subroutine fft_z_forward_complex_one (array_in, array_out)

    complex, dimension (:) :: array_in, array_out

    ! call fftw_f77(plan, howmany, in, istride, idist, out, ostride, odist)

# if FFT == _FFTW_
    call fftw_f77 (forward_z%plan, 1, array_in, 1, forward_z%n, array_out, 1, forward_z%n)
# elif FFT == _FFTW3_
    call FFTW_PREFIX(_execute_dft) (forward_z%plan, array_in, array_out)
# endif

  end subroutine fft_z_forward_complex_one

  !> Backward fourier transform a complex array in the z dirn

  subroutine fft_z_backward_complex_one (array_in, array_out)

    complex, dimension (:) :: array_in, array_out

    ! call fftw_f77(plan, howmany, in, istride, idist, out, ostride, odist)

# if FFT == _FFTW_
    call fftw_f77 (backward_z%plan, 1, array_in, 1, backward_z%n, array_out, 1, backward_z%n)
# elif FFT == _FFTW3_
    call FFTW_PREFIX(_execute_dft) (backward_z%plan, array_in, array_out)
# endif

  end subroutine fft_z_backward_complex_one

  !> Forward fourier transform a field in the z direction. EGH

  subroutine fft_z_forward_field (field_in, field_out, nakx, naky)

    complex, dimension (:,:,:) :: field_in, field_out
    integer, intent (in) :: nakx, naky

    ! call fftw_f77(plan, howmany, in, istride, idist, out, ostride, odist)

# if FFT == _FFTW_
    call fftw_f77 (forward_z%plan, nakx*naky, field_in, 1, forward_z%n, field_out, 1, forward_z%n)
# elif FFT == _FFTW3_
    call FFTW_PREFIX(_execute_dft) (forward_z%plan, field_in, field_out)
# endif

  end subroutine fft_z_forward_field
  
  !> Backward fourier transform a field in the z direction. EGH

  subroutine fft_z_backward_field (field_in, field_out, nakx, naky)

    complex, dimension (:,:,:) :: field_in, field_out
    integer, intent (in) :: nakx, naky

    ! call fftw_f77(plan, howmany, in, istride, idist, out, ostride, odist)

# if FFT == _FFTW_
    call fftw_f77 (backward_z%plan, nakx*naky, field_in, 1, backward_z%n, field_out, 1, backward_z%n)
# elif FFT == _FFTW3_
    call FFTW_PREFIX(_execute_dft) (backward_z%plan, field_in, field_out)
# endif

  end subroutine fft_z_backward_field

!!!RNU> this is field transfrom
!!!     from z,kx,ky to kz,kx,ky (all complex data), so this is forward(-1)
!  subroutine kz_spectrum (an, an2, nakx, naky)
  subroutine kz_spectrum (an, an2, howmany)
    use mp, only: proc0
    complex, dimension (:,:,:) :: an, an2
!    integer, intent (in) :: nakx, naky
    integer, intent (in), optional :: howmany

    logical, save :: init_dbg = .true.
    if (debug .and. proc0 .and. init_dbg) then
       write(6,*) 'kz_spectrum is called'
       init_dbg=.false.
    end if
    
# if FFT == _FFTW_
!    call fftw_f77 (zf_fft%plan, nakx*naky, an, 1, zf_fft%n+1, an2, 1, zf_fft%n+1)
    call fftw_f77 (zf_fft%plan, howmany, an, 1, zf_fft%n+1, an2, 1, zf_fft%n+1)
# elif FFT == _FFTW3_
    call FFTW_PREFIX(_execute_dft) (zf_fft%plan,an,an2)
# endif

    an2 = conjg(an2)*an2

  end subroutine kz_spectrum

!!!RNU> this is g transform
!> MAB
  subroutine kz_transform (an_in, an_out)
    use mp, only: proc0
    use agk_layouts, only: g_lo
    use theta_grid, only: ntgrid
    use agk_mem, only: alloc8

    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (in) :: an_in
    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (out) :: an_out
    complex, dimension (:,:,:), allocatable :: anmod_in, anmod_out
    integer :: i

    logical, save :: init_dbg = .true.
    if (debug .and. proc0 .and. init_dbg) then
       write(6,*) 'kz_transform is called'
       init_dbg=.false.
    end if
    
    i = 2*(g_lo%ulim_proc-g_lo%llim_proc+1)
    allocate (anmod_in(2*ntgrid+1,2,i/2)); call alloc8(c3=anmod_in,v="anmod_in")
    allocate (anmod_out(2*ntgrid+1,2,i/2)); call alloc8(c3=anmod_out,v="anmod_out")
    anmod_in = an_in

# if FFT == _FFTW_
    call fftw_f77 (zf3_fft%plan, i, anmod_in, 1, zf3_fft%n+1, anmod_out, 1, zf3_fft%n+1)
# elif FFT == _FFTW3_
    call FFTW_PREFIX(_execute_dft) (zf3_fft%plan, anmod_in, anmod_out)
# endif
    an_out = anmod_out

  end subroutine kz_transform
!< MAB

!> KDN
!  Subroutine for computing 1-D FT of z(theta)-space data into k_z spectrum.
  subroutine kz_transform2 (an_in,an_out)
    use mp, only: proc0
    use theta_grid, only: ntgrid

    complex, dimension (-ntgrid:), intent (in) :: an_in
    complex, dimension (-ntgrid:), intent (out) :: an_out

    logical, save :: init_dbg = .true.
    if (debug .and. proc0 .and. init_dbg) then
       write(6,*) 'kz_transform2 is called'
       init_dbg=.false.
    end if
    
# if FFT == _FFTW_
    call fftw_f77_one (zf2_fft%plan, an_in, an_out)
# elif FFT == _FFTW3_
    call FFTW_PREFIX(_execute_dft) (zf2_fft%plan, an_in, an_out)
# endif

  end subroutine kz_transform2
!< KDN

  subroutine get_wisdom_file(file)
    use mp, only: proc0
    use file_utils, only: run_name, stdout_unit
    use command_line, only: cl_getenv
    implicit none
    character (len=*), intent(out) :: file
    character (len=300) :: env_wisdom

    call cl_getenv("GK_FFTW_WISDOM", env_wisdom)

    if (trim(env_wisdom) == "") then
       file = trim(run_name)//'.fftw_wisdom'
    else
       file=env_wisdom
    end if

    if (proc0) &
         & write(stdout_unit,'(" FFTW wisdom file: ",a/)') trim(file)
    
  end subroutine get_wisdom_file
  
end module agk_transforms
