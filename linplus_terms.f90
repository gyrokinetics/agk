module linplus_terms

  implicit none

  public :: init_linplus_terms
  public :: add_linplus_terms
  public :: reset_init, linplus, linplus_eq
  
  private

  ! knobs
  integer :: linplus_mode_switch

  integer, parameter :: linplus_mode_none = 1, linplus_mode_tearing = 2, linplus_mode_force = 3

  real, save, dimension (:,:), allocatable :: ba, gb, bracket
  ! yxf_lo%ny, yxf_lo%llim_proc:yxf_lo%ulim_alloc

  real, save, dimension (:,:,:), allocatable :: aba, agb, abracket
  ! 2*ntgrid+1, 2, accelx_lo%llim_proc:accelx_lo%ulim_alloc

  integer :: force_spec
  real :: force_krange_min, force_krange_max
  real :: force_amp
  integer :: force_seed
  
  logical :: linplus = .false. ! global flag to set if additional linear term is on
  logical :: linplus_eq = .false. ! global flag to set if additional linear term is on
  logical :: initialized = .false.
  logical :: alloc = .true.
  logical :: accelerated = .false.
  
contains
  
  subroutine init_linplus_terms 
    use theta_grid, only: init_theta_grid, ntgrid
    use kgrids, only: init_kgrids, naky, nakx, nx, ny
    use le_grids, only: init_le_grids, nlambda, negrid
    use species, only: init_species, nspec
    use agk_layouts, only: init_dist_fn_layouts, yxf_lo, accelx_lo
    use agk_layouts, only: init_agk_layouts
    use agk_transforms, only: init_transforms
    use agk_mem, only: alloc8
    use ran, only: init_ranf, get_rnd_seed_length
    implicit none
    logical :: dum1, dum2
    integer :: i, nseed
    integer, allocatable :: seed(:)
    
    if (initialized) return
    initialized = .true.
    
    call init_agk_layouts
    call init_theta_grid
    call init_kgrids
    call init_le_grids (dum1, dum2)
    call init_species
    call init_dist_fn_layouts (ntgrid, naky, nakx, nlambda, negrid, nspec)

    call read_parameters

    if (linplus_mode_switch /= linplus_mode_none) linplus = .true.
    !    if (linplus_mode_switch /= linplus_mode_none) then
    if (linplus_mode_switch == linplus_mode_tearing) then
       !       linplus = .true.
       linplus_eq = .true.
       call init_transforms (ntgrid, naky, nakx, nlambda, negrid, nspec, nx, ny, accelerated)

       if (alloc) then
          if (accelerated) then
             allocate (     aba(2*ntgrid+1, 2, accelx_lo%llim_proc:accelx_lo%ulim_alloc)) ; call alloc8 (r3=aba, v="aba")
             allocate (     agb(2*ntgrid+1, 2, accelx_lo%llim_proc:accelx_lo%ulim_alloc)) ; call alloc8 (r3=agb, v="agb")
             allocate (abracket(2*ntgrid+1, 2, accelx_lo%llim_proc:accelx_lo%ulim_alloc)) ; call alloc8 (r3=abracket, v="abracket")
             aba = 0. ; agb = 0. ; abracket = 0.
          else
             allocate (     ba(yxf_lo%ny,yxf_lo%llim_proc:yxf_lo%ulim_alloc)) ; call alloc8 (r2=ba, v="ba")
             allocate (     gb(yxf_lo%ny,yxf_lo%llim_proc:yxf_lo%ulim_alloc)) ; call alloc8 (r2=gb, v="gb")
             allocate (bracket(yxf_lo%ny,yxf_lo%llim_proc:yxf_lo%ulim_alloc)) ; call alloc8 (r2=bracket, v="bracket")
             ba = 0. ; gb = 0. ; bracket = 0.
          end if
          alloc = .false.
       end if
    end if
    
    if (linplus_mode_switch == linplus_mode_force) then
       nseed=get_rnd_seed_length()
       allocate(seed(1:nseed))
       if (force_seed == 0) then
          call init_ranf(.true.,seed)
       else
          do i=1,nseed
             seed(i)=force_seed+i-1
          end do
          call init_ranf(.false.,seed)
       end if
       deallocate(seed)
    end if
       
  end subroutine init_linplus_terms

  subroutine read_parameters
    use file_utils, only: input_unit_exist, error_unit
    use text_options, only: text_option, get_option_value
    use mp, only: proc0, broadcast, mp_abort
    implicit none
    type (text_option), dimension (4), parameter :: linplusopts = &
         (/ text_option('default', linplus_mode_none), &
            text_option('none', linplus_mode_none), &
            text_option('tearing', linplus_mode_tearing), &
            text_option('force', linplus_mode_force) /)
    character(20) :: linplus_mode
    namelist /additional_linear_terms_knobs/ linplus_mode, &
         & force_spec, force_krange_min, force_krange_max, force_amp, force_seed
    
    integer :: ierr, in_file
    logical :: exist
    logical :: done = .false.
    integer :: ireaderr=0
    logical :: abort_readerr=.true.
    
    if (done) return
    done = .true.

    if (proc0) then
       linplus_mode = 'default'

       force_spec = 1
       force_krange_min = 0.
       force_krange_max = 0.
       force_amp = 0.
       force_seed = 0
       
       in_file=input_unit_exist("additional_linear_terms_knobs",exist)
       if(exist) read (unit=in_file,nml=additional_linear_terms_knobs, iostat=ireaderr)
       
       ierr = error_unit()
       call get_option_value &
            (linplus_mode, linplusopts, linplus_mode_switch, &
            ierr, "linplus_mode in additional_linear_terms_knobs")
    end if

    call broadcast (ireaderr)
    if (abort_readerr) then
       if (ireaderr > 0) then
          call mp_abort ('Read error at additional_linear_terms_knobs')
       else if (ireaderr < 0) then
          call mp_abort ('End of file/record occurred at additional_linear_terms_knobs')
       endif
    endif

    call broadcast (linplus_mode_switch)
    call broadcast (force_spec)
    call broadcast (force_krange_min)
    call broadcast (force_krange_max)
    call broadcast (force_amp)
    call broadcast (force_seed)

  end subroutine read_parameters

  subroutine add_linplus_terms (g1, g2, g3, phi, apar, bpar, istep, bd, fexp)
    use theta_grid, only: ntgrid
    use agk_layouts, only: g_lo
    implicit none
    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (in out) :: g1, g2, g3
    complex, dimension (-ntgrid:,:,:), intent (in) :: phi,    apar,    bpar
    real, intent (in) :: fexp
    real, intent (in) :: bd
    integer, intent (in) :: istep

    if (linplus_mode_switch == linplus_mode_force)  then
       if (istep /= 0) call add_linplus_force (g1, g2, g3, phi, apar, bpar, istep, bd, fexp)
    else if (linplus_mode_switch == linplus_mode_tearing)  then
       if (istep /= 0) call add_linplus (g1, g2, g3, phi, apar, bpar, istep, bd, fexp)
    end if
  end subroutine add_linplus_terms

  subroutine add_linplus_force (g1, g2, g3, phi, apar, bpar, istep, bd, fexp)
    ! random forcing
    use theta_grid, only: ntgrid
    use agk_layouts, only: g_lo, ik_idx, it_idx, is_idx
    use agk_transforms, only: transform2
    use ran, only: ranf
    use constants, only: zi, pi
    use species, only: spec, ion_species
    use kgrids, only: nakx, naky, kperp2, nx, ny
    use dist_fn_arrays, only: vperp2, aj0
    use mp, only: proc0, broadcast
    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (in out) :: g1, g2, g3
    complex, dimension (-ntgrid:,:,:), intent (in) :: phi, apar, bpar
    integer, intent (in) :: istep
    real, intent (in) :: bd
    real, intent (in) :: fexp
    integer :: istep_last = 0

    integer :: iglo, ig, it, ik, is
    complex :: ur(-ntgrid:ntgrid,nakx,naky)
    real :: uxy(nx,ny), rms

    real :: fac
    real :: dens = 1., tperp = 1.
    real :: bsq

    if (proc0) then
       ur = 0.
       do ig = -ntgrid, ntgrid
          do it = 1, nakx
             do ik = 1, naky
                if (kperp2(it,ik) >= force_krange_min**2 .and. kperp2(it,ik) <= force_krange_max**2) &
                     ur(ig,it,ik) = exp(zi*2.*pi*ranf())
             end do
          end do
          ! normalize
          call transform2(ur(ig,:,:),uxy,ny,nx)
          rms = sqrt(sum(uxy**2)/(nx*ny))
          ur(ig,:,:)=force_amp/rms*ur(ig,:,:)
          ! 0.5 is to take into account -k modes
          ur(ig,:,1)=0.5*ur(ig,:,1)
       end do
       ur(:,1,1) = 0.
       
       ! 2d
       if (ntgrid == 1) then
          do ig = -ntgrid, ntgrid
             ur(ig,:,:) = ur(0,:,:)
          end do
       end if

       ! reality
       do it = 1, nakx/2
          ur(:,it+(nakx+1)/2,1) = conjg(ur(:,(nakx+1)/2+1-it,1))
       end do
    end if
    call broadcast(ur)
    
    fac=sum(spec(:)%z**2*spec(:)%dens/spec(:)%temp)
    if (istep /= istep_last) then

       g3 = g2
       g2 = g1
       
       do iglo = g_lo%llim_proc, g_lo%ulim_proc
          it = it_idx(g_lo,iglo)
          ik = ik_idx(g_lo,iglo)
          is = is_idx(g_lo,iglo)
          
          bsq=.25*spec(is)%smz**2*kperp2(it,ik)
          fac=sum(spec(:)%z**2*spec(:)%dens/spec(:)%temp)

          if (is == force_spec) then
             dens = 1.
             tperp = - dens*spec(is)%temp/spec(is)%dens
             do ig = -ntgrid, ntgrid
                g1(ig,:,iglo) = ur(ig,it,ik) * exp(bsq)*(dens+tperp*(vperp2(iglo)-1.+bsq))
             end do
          else
             dens = 0.; tperp = 0.
             g1(:,:,iglo) = 0.
          end if
          do ig = -ntgrid, ntgrid
             g1(ig,:,iglo) = g1(ig,:,iglo) - ur(ig,it,ik) * &
                  & aj0(iglo)*(spec(is)%z*spec(is)%dens/spec(is)%temp)*spec(force_spec)%z*spec(force_spec)%dens/fac
          end do
       end do
       
       ! factor of one-half appears elsewhere
       do iglo = g_lo%llim_proc, g_lo%ulim_proc
          do ig = -ntgrid, ntgrid-1
             g1(ig,1,iglo) = (1.+bd)*g1(ig+1,1,iglo) + (1.-bd)*g1(ig,1,iglo)
             g1(ig,2,iglo) = (1.-bd)*g1(ig+1,2,iglo) + (1.+bd)*g1(ig,2,iglo)
          end do
       end do

    endif

    istep_last = istep

  end subroutine add_linplus_force
  
  subroutine add_linplus (g1, g2, g3, phi, apar, bpar, istep, bd, fexp)
    use mp, only: max_allreduce
    use theta_grid, only: ntgrid
    use agk_layouts, only: g_lo, ik_idx, it_idx, is_idx
    use agk_layouts, only: accelx_lo, yxf_lo
    use dist_fn_arrays, only: g
    use species, only: spec
    use agk_transforms, only: transform2, inverse2
    use run_parameters, only: use_Phi, use_Apar, use_Bpar
    use kgrids, only: aky, akx
    use constants, only: zi
    use fields_arrays, only: apar_eq
    use dist_fn_arrays, only: g_eq
    use dist_fn_arrays, only: vpa, aj0
    implicit none
    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (in out) :: g1, g2, g3
    complex, dimension (-ntgrid:,:,:), intent (in) :: phi, apar, bpar
    integer, intent (in) :: istep
    real, intent (in) :: bd
    real, intent (in) :: fexp
    integer :: istep_last = 0
    integer :: i, j, k
    real :: zero

    integer :: iglo, ik, it, is, ig, ia, isgn

    if (istep /= istep_last) then

       zero = epsilon(0.0)
       g3 = g2
       g2 = g1

       !=================================
       ! dApar0/dx
       g1=0.
       do iglo = g_lo%llim_proc, g_lo%ulim_proc
          it = it_idx(g_lo,iglo)
          ik = ik_idx(g_lo,iglo)
          is = is_idx(g_lo,iglo)
          do ig = -ntgrid, ntgrid
             g1(ig,1,iglo) = - zi*akx(it)*aj0(iglo)*spec(is)%stm &
                  *vpa(1,iglo)*apar_eq(ig,it,ik)
             g1(ig,2,iglo) = - zi*akx(it)*aj0(iglo)*spec(is)%stm &
                  *vpa(2,iglo)*apar_eq(ig,it,ik)
          end do
       end do
       !=================================

       if (accelerated) then
          call transform2 (g1, aba, ia)
       else
          call transform2 (g1, ba)
       end if
       ! ba/aba is fixed, calculated once is enough
       
       ! this is just multiplying i*ky to h
       ! transform of g to h can be done by g_adjust, but is not done because of circular dependency
       g1=0.
       if (use_Phi) call load_ky_phi
       if (use_Bpar) call load_ky_bpar
       
       do iglo = g_lo%llim_proc, g_lo%ulim_proc
          ik = ik_idx(g_lo,iglo)
          is = is_idx(g_lo,iglo)
          do isgn = 1, 2
             do ig = -ntgrid, ntgrid
                g1(ig,isgn,iglo) = g1(ig,isgn,iglo)*spec(is)%zt + zi*aky(ik)*g(ig,isgn,iglo)
             end do
          end do
       end do
       
       if (accelerated) then
          call transform2 (g1, agb, ia)
       else
          call transform2 (g1, gb)
       end if

       if (accelerated) then
          do k = accelx_lo%llim_proc, accelx_lo%ulim_proc
             do j = 1, 2
                do i = 1, 2*ntgrid+1
                   abracket(i,j,k) = aba(i,j,k)*agb(i,j,k)
                end do
             end do
          end do
       else
          do j = yxf_lo%llim_proc, yxf_lo%ulim_proc
             do i = 1, yxf_lo%ny
                bracket(i,j) = ba(i,j)*gb(i,j)
             end do
          end do
       endif

       g1 = 0.
       if (use_Phi)  call load_ky_phi
       if (use_Bpar) call load_ky_bpar
       if (use_Apar) call load_ky_apar

       if (accelerated) then
          call transform2 (g1, aba, ia)
       else
          call transform2 (g1, ba)
       end if

       ! g_eq
       g1=0.
       do iglo = g_lo%llim_proc, g_lo%ulim_proc
          it = it_idx(g_lo,iglo)
          is = is_idx(g_lo,iglo)
          do isgn = 1, 2
             do ig = -ntgrid, ntgrid
                g1(ig,isgn,iglo) = g1(ig,isgn,iglo)*spec(is)%zt + zi*akx(it)*g_eq(ig,isgn,iglo)
             end do
          end do
       end do

       if (accelerated) then
          call transform2 (g1, agb, ia)
       else
          call transform2 (g1, gb)
       end if
       ! gb/abg is fixed, calculated once is enough
       
       if (accelerated) then
          do k = accelx_lo%llim_proc, accelx_lo%ulim_proc
             do j = 1, 2
                do i = 1, 2*ntgrid+1
                   abracket(i,j,k) = abracket(i,j,k) - aba(i,j,k)*agb(i,j,k)
                end do
             end do
          end do
       else
          do j = yxf_lo%llim_proc, yxf_lo%ulim_proc
             do i = 1, yxf_lo%ny
                bracket(i,j) = bracket(i,j) - ba(i,j)*gb(i,j)
             end do
          end do
       end if

       if (accelerated) then
          call inverse2 (abracket, g1, ia)
       else
          call inverse2 (bracket, g1)
       end if
          
! factor of one-half appears elsewhere
       do iglo = g_lo%llim_proc, g_lo%ulim_proc
          do ig = -ntgrid, ntgrid-1
             g1(ig,1,iglo) = (1.+bd)*g1(ig+1,1,iglo) + (1.-bd)*g1(ig,1,iglo)
             g1(ig,2,iglo) = (1.-bd)*g1(ig+1,2,iglo) + (1.+bd)*g1(ig,2,iglo)
          end do
       end do

    endif

    istep_last = istep

  contains

    subroutine load_kx_phi

      use dist_fn_arrays, only: aj0
      complex :: fac

      do iglo = g_lo%llim_proc, g_lo%ulim_proc
         it = it_idx(g_lo,iglo)
         ik = ik_idx(g_lo,iglo)
         do ig = -ntgrid, ntgrid
            fac = zi*akx(it)*aj0(iglo)*phi(ig,it,ik)
            g1(ig,1,iglo) = fac
            g1(ig,2,iglo) = fac
         end do
      end do

    end subroutine load_kx_phi

    subroutine load_ky_phi

      use dist_fn_arrays, only: aj0
      complex :: fac

      do iglo = g_lo%llim_proc, g_lo%ulim_proc
         it = it_idx(g_lo,iglo)
         ik = ik_idx(g_lo,iglo)
         do ig = -ntgrid, ntgrid
            fac = zi*aky(ik)*aj0(iglo)*phi(ig,it,ik)
            g1(ig,1,iglo) = fac
            g1(ig,2,iglo) = fac
         end do
      end do

    end subroutine load_ky_phi

    subroutine load_kx_apar

      use dist_fn_arrays, only: vpa, aj0
      use agk_layouts, only: is_idx

      do iglo = g_lo%llim_proc, g_lo%ulim_proc
         it = it_idx(g_lo,iglo)
         ik = ik_idx(g_lo,iglo)
         is = is_idx(g_lo,iglo)
         do ig = -ntgrid, ntgrid
            g1(ig,1,iglo) = g1(ig,1,iglo) - zi*akx(it)*aj0(iglo)*spec(is)%stm &
                 *vpa(1,iglo)*apar(ig,it,ik)
         end do
         do ig = -ntgrid, ntgrid
            g1(ig,2,iglo) = g1(ig,2,iglo) - zi*akx(it)*aj0(iglo)*spec(is)%stm &
                 *vpa(2,iglo)*apar(ig,it,ik)
         end do
      end do

    end subroutine load_kx_apar

    subroutine load_ky_apar

      use dist_fn_arrays, only: vpa, aj0
      use agk_layouts, only: is_idx

      do iglo = g_lo%llim_proc, g_lo%ulim_proc
         it = it_idx(g_lo,iglo)
         ik = ik_idx(g_lo,iglo)
         is = is_idx(g_lo,iglo)
         do ig = -ntgrid, ntgrid
            g1(ig,1,iglo) = g1(ig,1,iglo) - zi*aky(ik)*aj0(iglo)*spec(is)%stm &
                 *vpa(1,iglo)*apar(ig,it,ik)
         end do
         do ig = -ntgrid, ntgrid
            g1(ig,2,iglo) = g1(ig,2,iglo) - zi*aky(ik)*aj0(iglo)*spec(is)%stm &
                 *vpa(2,iglo)*apar(ig,it,ik)
         end do
      end do

    end subroutine load_ky_apar

    subroutine load_kx_bpar

      use dist_fn_arrays, only: aj1vp2
      use agk_layouts, only: is_idx
      complex :: fac

      do iglo = g_lo%llim_proc, g_lo%ulim_proc
         it = it_idx(g_lo,iglo)
         ik = ik_idx(g_lo,iglo)
         is = is_idx(g_lo,iglo)
         do ig = -ntgrid, ntgrid
            fac = g1(ig,1,iglo) + zi*akx(it)*aj1vp2(iglo) &
                 *spec(is)%tz*bpar(ig,it,ik)
            g1(ig,1,iglo) = fac
            g1(ig,2,iglo) = fac
         end do
      end do

    end subroutine load_kx_bpar

    subroutine load_ky_bpar

      use dist_fn_arrays, only: aj1vp2
      use agk_layouts, only: is_idx
      complex :: fac

      do iglo = g_lo%llim_proc, g_lo%ulim_proc
         it = it_idx(g_lo,iglo)
         ik = ik_idx(g_lo,iglo)
         is = is_idx(g_lo,iglo)
         do ig = -ntgrid, ntgrid
            fac = g1(ig,1,iglo) + zi*aky(ik)*aj1vp2(iglo) &
                 *spec(is)%tz*bpar(ig,it,ik)
            g1(ig,1,iglo) = fac 
            g1(ig,2,iglo) = fac
         end do
      end do

    end subroutine load_ky_bpar

  end subroutine add_linplus

  subroutine reset_init

    initialized = .false.

  end subroutine reset_init

end module linplus_terms
