! TT: for variable step AB3 I made changes
!  dtime -> dtime(1)
! everywhere (3/28/10)

module agk_diagnostics

  use agk_heating, only: dens_vel_diagnostics

  implicit none

  public :: init_agk_diagnostics
  public :: finish_agk_diagnostics
  public :: loop_diagnostics
!!!RN>  public :: nsave

  private

  interface get_vol_average
     module procedure get_vol_average_one, get_vol_average_all, get_vol_average_vspec
  end interface

  ! knobs
  real :: omegatol, omegatinst
  logical :: debug
  logical :: print_line, print_flux_line, print_summary
  logical :: write_linear, write_nonlin
  logical :: write_omega, write_omavg, write_ascii=.false.
  logical :: write_gs
  logical :: write_g, write_gg
  logical :: write_vspace_slices
  logical :: write_epartot
  logical :: write_verr    ! MAB
  logical :: write_vspectrum, write_kspectrum    ! MAB
  logical :: write_final_fields
  logical :: write_final_moments
  logical :: write_avg_moments
  logical :: write_full_moments_r !RN
  logical :: write_full_moments !JMT
  logical :: write_final_epar, write_kpar
  logical :: write_hrate, write_lorentzian
  logical :: write_elsasser       !Write out Elsasser spectra (MHD only so far)
  logical :: write_adapt_hc       !Write values for adaptive hypercollisionality
  logical :: write_nl_flux, write_Epolar
  logical :: exit_when_converged
  logical :: make_movie
!!!RN>  logical :: save_for_restart
!>GGH
  logical :: write_density_velocity
  logical :: write_jext=.false.
!<GGH
! TT>
  logical :: write_init ! call loop_diagnostics at istep=0
  logical :: write_gkp  ! Gabe's vperp-space spectra
  logical :: write_hkv  ! vperp-space diagnostics
  logical :: write_ptrans ! velocity space transfer
  logical :: write_Eshell
! <TT
  logical :: write_freq ! freq diagnostic 

!<EGH

  logical:: write_phi_over_time, write_apar_over_time, write_bpar_over_time 

 !EGH>
  integer :: nwrite, igomega, nmovie, nwrite2
  integer :: navg
!!!RN>  integer :: nsave
  integer :: nwrite_freq, nwrite_fullmom ! JMT freq and full moments diagnostic write cadence
  integer :: nk_freq ! JMT freq diagnostic number of modes to save

  ! internal
  logical :: write_any, write_any_fluxes, dump_any
  logical, private :: initialized = .false.

  integer :: out_unit, kp_unit, heat_unit, polar_raw_unit, polar_avg_unit
  integer :: ptrans_unit, pflux_unit
  integer :: elz_raw_unit, elz_avg_unit
  integer, dimension (:), allocatable :: heat_unit2
  integer :: dv_unit, jext_unit   !GGH Additions
  integer :: res_unit  ! MAB
  integer :: vspec_unit, kspec_unit  ! MAB
  integer :: freq_unit ! JMT
  integer, dimension(:), allocatable :: ahc_unit    !Adaptive HC unit for up to four species
  logical :: nuh_changed=.false.   !Flag =T if any nu_h is changed by adaptive hypercollisions

  complex, dimension (:,:,:), allocatable :: omegahist
  ! (navg,nakx,naky)
  real, dimension(:), allocatable :: h              !Total heating
  real, dimension(:,:,:), allocatable :: hk         !Heating by k
  real, dimension(:,:,:), allocatable :: elzk       !Elsasser energies by k
  real, dimension(:,:), pointer :: elzavg => null ()!Average Elsasser energy spectra
  real, dimension(:,:), pointer :: elzraw => null ()!Raw Elsasser energy spectra
  integer, dimension(:,:), allocatable :: kyx_mode !JMT Modes to write for freq. diagnostic

  !GGH Density/velocity pertubration diagnostics
  type (dens_vel_diagnostics), dimension(:), allocatable :: dv_hist
  type (dens_vel_diagnostics), dimension(:,:,:), allocatable :: dvk_hist
  !GGH J_external
  real, dimension(:,:,:), allocatable ::  j_ext_hist
  !Polar spectrum variables
  integer :: nbx                                   !Number of polar bins
  real, dimension(:), allocatable :: kpbin,ebincorr
  integer, dimension(:), allocatable :: numavg
  real, dimension(:), allocatable :: kpavg
  real, dimension(:,:), pointer :: ebinarray => null ()
  real, dimension(:,:), pointer :: eavgarray => null ()
!  real, dimension(:,:), allocatable :: etmp
  integer, dimension(:,:), allocatable :: polar_index ! (nakx,naky) -> (1:nbx)
  integer, dimension(:), allocatable :: polar_avg_index ! (1:nbx) -> (1:nkpolar)
  integer, parameter :: iefluc = 1        !Total fluctuating energy
  integer, parameter :: ieapar = 2        !Energy in A_parallel
  integer, parameter :: iebpar = 3        !Energy in B_parallel
  integer, parameter :: iephis2= 4        !Energy in q_s^ n_s Phi^2/T_s
  integer, parameter :: iehs2   = 5       !Energy in hs^2
  integer, parameter :: iedelfs2= 6       !Energy in del f_s^2
  integer, dimension(:,:,:), allocatable :: ahc_index
  !GGH Adaptive Hypercollisionality variables
  real, dimension(:), allocatable :: pk1,pk2,ek,etot,wnl,kphc,bk
  real, dimension(:), allocatable :: fm1,fm2,ft !Measures for adjusting nu_h

  real, dimension (:,:,:,:), allocatable ::  qheat, qmheat, qbheat
  ! (nakx,naky,nspec,3)

  real, dimension (:,:,:), allocatable ::  pflux, pmflux, pbflux
  ! (nakx,naky,nspec)

  real, dimension (:,:,:), allocatable ::  vflux, vmflux, vbflux
  ! (nakx,naky,nspec)

  integer :: ntg
  
  !<KDN> For dynfields
  logical, save :: dynfields_inited
  logical :: do_write_dynfields
  integer :: dynfields_unit,nwrite_dyn
  !<\KDN>
  
  !<KDN> For stream_flux_potential
  logical :: debug_moi, write_moi,moi_no_fft
  integer :: n_modes_of_interest, moi_unit
  integer, dimension(:), allocatable  :: array_moi_units
  integer, dimension(:), allocatable :: ikx_moi,iky_moi,ikz_moi
  !<\KDN>

contains

  subroutine init_agk_diagnostics (list, nstep)
    use theta_grid, only: init_theta_grid
    use kgrids, only: init_kgrids, nakx, naky, nkpolar
    use run_parameters, only: init_run_parameters
    use species, only: init_species, nspec, adapt_hc_any
    use init_g, only: init_init_g
    use dist_fn, only: init_dist_fn
    use dist_fn, only: pass_nwrite   ! MAB
    use agk_io, only: init_agk_io
    use agk_heating, only: init_dvtype  !NOTE: This must be eliminated
    use mp, only: broadcast, proc0
    use le_grids, only: init_weights, init_vspectrum
    use agk_heating_index, only: init_heating_index,lemax,lelzmax
    use agk_mem, only: alloc8
    use hdf_wrapper, only: hdf_init
    use run_parameters, only: hdf5_stop, hdf5_dble
    use collisions, only: init_g2le_redistribute
    use kshell, only: init_shell_spectrum, write_ktrans, get_wtrans, get_etrans
    use kshell, only: use_qshell, write_gkshp
    use agk_save, only: save_for_restart, nsave
    implicit none
    logical, intent (in) :: list
    integer, intent (in) :: nstep
    integer :: nmovie_tot

    if (initialized) return
    initialized = .true.

    call init_theta_grid
    call init_kgrids
    call init_run_parameters
    call init_species
    call init_init_g !RN separated from init_dist_fn
    call init_dist_fn

    call real_init (list) ! real initialization
    if (debug .and. proc0) write(*,*) "init_agk_diagnostics: pre-broadcasts."
    call broadcast (navg)
    call broadcast (nwrite)
    call broadcast (nmovie)
    call broadcast (nwrite2)
    call broadcast (write_any)
    call broadcast (write_any_fluxes)
    call broadcast (write_nl_flux)
    call broadcast (write_omega)
    call broadcast (dump_any)
    call broadcast (make_movie)
    call broadcast (write_gs)
    call broadcast (write_g)
    call broadcast (write_gg)
    call broadcast (write_vspace_slices)

    call broadcast (ntg)
    call broadcast (write_hrate)
    call broadcast (write_elsasser)
    call broadcast (write_density_velocity)
    call broadcast (write_adapt_hc)
    call broadcast (write_Epolar)
    call broadcast (write_lorentzian)
!> MAB
    call broadcast (write_verr)
    call broadcast (write_vspectrum)
    call broadcast (write_kspectrum)
! <MAB
! TT>
    call broadcast (write_init)
    call broadcast (write_gkp)
    call broadcast (write_gkshp)
    call broadcast (write_hkv)
    call broadcast (write_Eshell)
    call broadcast (write_ktrans)
    call broadcast (get_wtrans)
    call broadcast (get_etrans)
    call broadcast (use_qshell)
    call broadcast (write_ptrans)
    call broadcast (write_avg_moments)
    call broadcast (write_full_moments_r) !RN
    call broadcast (write_full_moments) !JMT
    call broadcast (nwrite_fullmom) !JMT
    call broadcast (write_final_moments)
! <TT
! JMT>
    call broadcast(write_freq)
    call broadcast(nwrite_freq)
    call broadcast(nk_freq)
! <JMT
    if (debug .and. proc0) write(*,*) "init_agk_diagnostics: post-broadcasts."
!<EGH
    call broadcast (write_phi_over_time)
    call broadcast (write_apar_over_time)
    call broadcast (write_bpar_over_time)

    !!! RN>
    call broadcast (save_for_restart)
    call broadcast (nsave)
    !!! RN<

 !EGH>
    nmovie_tot = nstep/nmovie

!>MAB
! initialize weights for less accurate integrals used
! to provide an error estimate for v-space integrals (energy and untrapped)
    if (write_verr .and. proc0) call init_weights

    if (write_vspectrum) then
       call init_vspectrum
       call init_g2le_redistribute
    end if
!<MAB

    call pass_nwrite (nwrite, navg)  ! MAB

! allocate heating diagnostic data structures
    if (write_hrate) then
       !Initialize indices for heating rate calculations
       call init_heating_index(nspec)

       !Total heating
       allocate (h(lemax)) ; call alloc8 (r1=h, v="h")

       !Heating by k
       allocate (hk(nakx, naky, lemax)) ; call alloc8 (r3=hk, v="hk")

    else
       !NOTE: I don't know why this is necessary GGH 02APR08
       !RN> because of nc_loop?
       allocate (h(1)) ; call alloc8 (r1=h, v="h")
       allocate (hk(1,1,1)) ; call alloc8 (r3=hk, v="hk")
    end if

!GGH Allocate density and velocity perturbation diagnostic structures
    if (write_density_velocity) then
       allocate (dv_hist(0:navg-1)) !!! unable account memory for structure variables
       call init_dvtype (dv_hist,  nspec)

       allocate (dvk_hist(nakx,naky,0:navg-1)) !!! unable account memory for structure variables
       call init_dvtype (dvk_hist, nspec)
    end if
       
!GGH Allocate density and velocity perturbation diagnostic structures
    if (write_jext) then
       allocate (j_ext_hist(nakx, naky,0:navg-1))  ; call alloc8 (r3=j_ext_hist, v="j_ext_hist")
    end if

!Initialize polar spectrum diagnostic
! TT> everyone wants to use polar_index and polar_avg_index
     if (debug .and. proc0) write(*,*) "init_agk_diagnostics: preparing to fail on init_polar_spectrum."
!    if (write_Epolar .and. proc0) call init_polar_spectrum
    if (write_Epolar) call init_polar_spectrum
! <TT
    if (write_Eshell .and. proc0) call init_shell_spectrum
     if (debug .and. proc0) write(*,*) "init_agk_diagnostics: init_polar_spectrum succeeded?"

!Initialize Elsasser energy spectrum diagnostic
    if (write_Epolar .and. write_elsasser .and. proc0)  then
       ! This uses varaiables initialized in subroutine init_polar_spectrum
       allocate(elzk(nakx,naky,lelzmax)); call alloc8(r3=elzk,v="elzk")
       elzk(:,:,:)=0.
       allocate(elzavg(1:nkpolar,lelzmax)) !!! RN> unable to account memory for pointers
       elzavg(:,:)=0.
       allocate(elzraw(1:nbx,lelzmax)) !!! RN> unable to account memory for pointers
       elzraw(:,:)=0.
    endif

!Initialize adaptive hypercollisionality diagnostic
    if ((adapt_hc_any .or. write_adapt_hc) .and. proc0) call init_adapt_hc

    call init_agk_io (write_nl_flux, write_omega, write_avg_moments, &
!         write_full_moments_r, write_vspectrum, write_kspectrum, &
         write_full_moments_r, write_full_moments, write_vspectrum, &
         write_final_moments, write_hrate, make_movie, nmovie_tot, write_freq, &
         nk_freq, kyx_mode,write_phi_over_time, write_apar_over_time, write_bpar_over_time)

    if (write_gg) call hdf_init (stop=hdf5_stop,dbl=hdf5_dble)
    ! TT: when dbl is false, the code uses single precision for the hdf file.
    ! make dbl=.true. if you want double precision output.
    ! hdf library takes care of single-to-double (or vice versa) conversion,
    ! and it's faster than writing. :TT
    
!<KDN>  Make sure this starts out false to force initialization
    dynfields_inited = .false.
!<\KDN>

    ! moved from dist_fn
    if (write_kpar .or. write_gs .or. write_kspectrum) call init_par_filter
    
    if (proc0 .and. write_moi) call init_stream_flux_potentials
    if (debug .and. proc0) write(*,*) "init_agk_diagnostics: finished."
  end subroutine init_agk_diagnostics

  subroutine real_init (list)
    use run_parameters, only: use_Apar
    use file_utils, only: open_output_file
    use kgrids, only: naky, nakx
    use species, only: nspec
    use mp, only: proc0
    use constants
    use agk_mem, only: alloc8
    implicit none
    logical, intent (in) :: list
    character(20) :: datestamp, timestamp, zone, suffix
    integer :: is
    
    call read_parameters (list)
    if (debug .and. proc0) write(*,*) "init_agk_diagnostics:real_init: read_parameters"
    if (proc0) then
       if (write_ascii) then
          call open_output_file (out_unit, ".out")
!          if (write_kpar) call open_output_file (kp_unit, ".kp")
       end if
       
       if (write_hrate .and. write_ascii) then
          if (nspec == 2) then
             call open_output_file (heat_unit, ".heat")
          else
             allocate(heat_unit2(1:nspec)); call alloc8(i1=heat_unit2,v="heat_unit2")
             do is=1,nspec
                write(suffix,'(a5,i2.2)')".heat",is
                call open_output_file (heat_unit2(is),suffix)
             enddo
          end if
       end if

       !Adaptive hypercollisionality output
       if (write_adapt_hc .and. write_ascii) then
          !Allocate variable for unit numbers
          allocate(ahc_unit(1:nspec)); call alloc8(i1=ahc_unit,v="ahc_unit")
          do is= 1, nspec
             write(suffix,'(a4,i2.2)')".ahc",is
             call open_output_file (ahc_unit(is), suffix)
          enddo
       endif

       !GGH Density and velocity perturbations
       if (write_density_velocity .and. write_ascii) then
          call open_output_file (dv_unit, ".dv")
       end if

       !GGH J_external, only if A_parallel is being calculated.
       if (write_jext .and. use_Apar) then
          if (write_ascii) then
             call open_output_file (jext_unit, ".jext")
          end if
       else
          write_jext = .false.
       end if

       if (write_Epolar .and. write_ascii) then
          call open_output_file (polar_raw_unit, ".kspec_raw")
          call open_output_file (polar_avg_unit, ".kspec_avg")
       end if

       if (write_ptrans .and. write_ascii) then
          call open_output_file (ptrans_unit, '.ptrans')
          call open_output_file (pflux_unit, '.pflux')
       end if

       if (write_Epolar .and. write_elsasser) then
          call open_output_file (elz_raw_unit, ".elz_raw")
          call open_output_file (elz_avg_unit, ".elz_avg")
       end if

!>MAB
       if (write_verr .and. write_ascii) then
          call open_output_file (res_unit, ".vres")
          if (debug) write(*,*) "init_agk_diagnostics: real_init: open .vres"
       end if

       if (write_vspectrum .and. write_ascii) then
          call open_output_file (vspec_unit, ".vspectrum")
          if (debug) write(*,*) "init_agk_diagnostics: real_init: open .vspectrum"
       end if

       if (write_kspectrum .and. write_ascii) then
          call open_output_file (kspec_unit, ".kspectrum")
          if (debug) write(*,*) "init_agk_diagnostics: real_init: open .kspectrum"
       end if
!<MAB

       if (write_ascii) then
          write (unit=out_unit, fmt="('AstroGK $Revision$')")
!          write (unit=out_unit, fmt="('AstroGK Revision: ',i0)") &
!#include "Revision"
          datestamp(:) = ' '
          timestamp(:) = ' '
          zone(:) = ' '
          call date_and_time (datestamp, timestamp, zone)
          write (unit=out_unit, fmt="('Date: ',a,' Time: ',a,1x,a)") &
               trim(datestamp), trim(timestamp), trim(zone)
       end if

       if (debug) write(*,*) "init_agk_diagnostics: real_init: beginning allocates."
    
       allocate (omegahist(0:navg-1,nakx,naky)) ; call alloc8 (c3=omegahist, v="omegahist")
       omegahist = 0.0
    end if
    
    allocate (pflux (nakx, naky, nspec))    ; call alloc8 (r3=pflux, v="pflux")
    allocate (qheat (nakx, naky, nspec, 3)) ; call alloc8 (r4=qheat, v="qheat")
    allocate (vflux (nakx, naky, nspec))    ; call alloc8 (r3=vflux, v="vflux")

    pflux = 0. ; qheat = 0. ; vflux = 0.

    allocate (pmflux (nakx, naky, nspec))    ; call alloc8 (r3=pmflux, v="pmflux")
    allocate (qmheat (nakx, naky, nspec, 3)) ; call alloc8 (r4=qmheat, v="qmheat")
    allocate (vmflux (nakx, naky, nspec))    ; call alloc8 (r3=vmflux, v="vmflux")

    pmflux = 0. ; qmheat = 0. ; vmflux = 0.

    allocate (pbflux (nakx, naky, nspec))    ; call alloc8 (r3=pbflux, v="pbflux")
    allocate (qbheat (nakx, naky, nspec, 3)) ; call alloc8 (r4=qbheat, v="qbheat")
    allocate (vbflux (nakx, naky, nspec))    ; call alloc8 (r3=vbflux, v="vbflux")

    pbflux = 0. ; qbheat = 0. ; vbflux = 0.

    if (debug) write(*,*) "init_agk_diagnostics: real_init: finished allocates; done."
       
  end subroutine real_init

  subroutine read_parameters (list)
! TT: temporarily used for transfer function diagnostic
    use agk_layouts, only: layout
    use agk_transforms, only: accel
! <TT
    use kshell, only: write_ktrans, use_qshell, get_wtrans, get_etrans
    use kshell, only: write_gkshp
    use file_utils, only: input_unit, input_unit_exist, get_indexed_namelist_unit
    use theta_grid, only: nperiod, ntheta
    use kgrids, only: box, nx, ny, x0, y0, naky, nakx
    use mp, only: proc0, broadcast, mp_abort
    use agk_mem, only: alloc8
    use agk_save, only: save_for_restart, nsave
    implicit none
    integer :: in_file, i, it, ik
    integer :: kx, ky
    logical, intent (in) :: list
    logical :: exist
    integer :: ireaderr=0
    logical :: abort_readerr=.true.

    namelist /diagnostics/ &
         debug, print_line, print_flux_line, &
         write_linear, write_nonlin, &
         write_omega, write_omavg, write_ascii, write_kpar, &
         write_gs, write_g, write_gg, write_vspace_slices, &
         write_hrate, write_elsasser, &
         write_density_velocity, write_epartot, &
         write_final_fields, write_final_epar, write_final_moments, &
         write_avg_moments, write_Epolar, write_Eshell, write_nl_flux, &
         write_full_moments_r, & ! RN
         write_full_moments, nwrite_fullmom, & !JMT
         nwrite, nmovie, nwrite2, nsave, navg, omegatol, omegatinst, igomega, write_lorentzian, &
         exit_when_converged, make_movie, save_for_restart, &
         write_adapt_hc, &
         write_verr, write_vspectrum, write_kspectrum, &   ! MAB
         write_init, &   ! TT: call loop_diagnostics at istep=0
         write_gkp, &    ! TT: Gabe's velocity spectra
         write_gkshp, &  ! TT: Gabe's velocity spectra w/ kshell sum
         write_hkv, &    ! TT: [int|d(hk)/dvperp|^2dv/int|hk|^2]^{-1/2}
         write_ktrans, & ! TT: shell-to-shell transfer in wave-number space
         use_qshell, &   ! TT: zonal flow version of write_ktrans
         write_ptrans, & ! TT: shell-to-shell transfer in velocity dual space
         get_wtrans, get_etrans, & ! TT: supplemetal to shell transfer
         do_write_dynfields, & ! KDN: Use dynfields field outputs?
         nwrite_dyn, &      ! KDN: dynfields outputs every nwrite_dyn steps
         debug_moi, &       ! KDN: print debug statements for stream and flux potentials diagnostics?
         write_moi, &       ! KDN: Whether to output modes of interest (make sure n_modes_of_interest > 0 if true)
         moi_no_fft, &      ! GGH: Write out z structure instead of kz
         n_modes_of_interest, & ! KDN: Number of modes of interest
         write_freq, nwrite_freq, nk_freq,& ! JMT: Output fields for frequency analyses
         write_phi_over_time, write_apar_over_time, write_bpar_over_time ! EGH
         ! Write 3d spectral fields over time (v expensive)

    namelist /freq_mode/ kx, ky ! JMT: Output fields for set of kx, ky modes for frequency analysis

    if (proc0) then
       debug = .false.
       print_line = .true. ! TT: I want to make it .false.
       print_flux_line = .false.
       write_linear = .true.
       write_nonlin = .true.
       write_kpar = .false.
       write_hrate = .false.
       write_elsasser = .false.
       write_adapt_hc = .false.
       write_density_velocity = .false.
       write_Epolar = .false.
       write_gs = .false.
       write_g = .false.
       write_gg = .false.
       write_vspace_slices = .false.
       write_lorentzian = .false.
       write_omega = .false.
       write_ascii = .true.
       write_omavg = .false.
       write_epartot = .false.
       write_nl_flux = .false.
       write_final_moments = .false.
       write_avg_moments = .false.
       write_full_moments_r = .false. !RN
       write_full_moments = .false. !JMT
       nwrite_fullmom = -1 !JMT
       write_final_fields = .false.
       write_final_epar = .false.
       write_verr = .false.        ! MAB
       write_vspectrum = .false.   ! MAB
       write_kspectrum = .false.   ! MAB
       write_init = .true.    ! TT
       write_gkp = .false.    ! TT
       write_gkshp = .false.  ! TT
       write_hkv = .false.    ! TT
       write_Eshell = .false. ! TT
       write_ktrans = .false. ! TT
       use_qshell = .false.   ! TT
       get_wtrans = .true.    ! TT
       get_etrans = .true.    ! TT
       write_ptrans = .false. ! TT
       write_freq = .false. ! JMT  
       nwrite = 100
       nmovie = 1000
       nwrite2 = 100
       navg = 100
       nwrite_freq = 1000 ! JMT
       nk_freq = 0 ! JMT
!!!RN>       nsave = -1
       omegatol = 1e-3
       omegatinst = 1.0
       igomega = 0
       exit_when_converged = .true.
       make_movie = .false.
!!!RN>       save_for_restart = .false.
       do_write_dynfields = .false.
       nwrite_dyn = 1
       debug_moi = .false.
       write_moi = .false.
       moi_no_fft = .false.
       n_modes_of_interest=0
       write_phi_over_time = .false. !EGH
       write_bpar_over_time = .false. !EGH
       write_apar_over_time = .false. !EGH

       in_file = input_unit_exist ("diagnostics", exist)
       if (exist) read (unit=in_file, nml=diagnostics, iostat=ireaderr)

       print_summary = (list .and. (print_line .or. print_flux_line)) 

       if (list) then
          print_line = .false.
          print_flux_line = .false.
       end if

       if (.not. save_for_restart) nsave = -1

! Only calculate polar integrals in box layout
       write_Epolar = write_Epolar .and. box

! Disable polar integrals if nx /= ny
       if (nx /= ny) write_Epolar = .false.

       write_Eshell = write_Eshell .and. box .and. (nx==ny) .and. (x0==y0)

       ! transfer function diagnostic works in highly limited case only for now
       write_ktrans = write_ktrans .and. layout=='yxles' .and. accel
       write_ptrans = write_ptrans .and. layout=='yxles' .and. accel

       write_any = write_linear .or. write_omega     .or. write_omavg &
            .or. write_nonlin .or. write_nl_flux .or. write_Epolar &
            .or. write_kpar   .or. write_hrate .or. write_elsasser     &
            .or. write_lorentzian .or. write_gs .or. write_Eshell &
            .or. write_density_velocity .or. write_adapt_hc &
            .or. write_avg_moments &
            .or. write_full_moments_r .or. write_freq .or. write_full_moments
       write_any_fluxes =  write_nonlin .or. print_flux_line .or. write_nl_flux
       dump_any = make_movie .or. print_summary

       ntg = ntheta/2 + (nperiod-1)*ntheta
!>JMT
       ! Initialize arrays for set of single mode frequency analysis diagnostics
       if (write_freq) then
          if (nk_freq .gt. 0) then
             allocate (kyx_mode(nk_freq,2)); call alloc8(i2=kyx_mode,v="kyx_mode")
             do i=1,nk_freq
                call get_indexed_namelist_unit (in_file, "freq_mode", i)
                kx=1
                ky=1
                read (unit=in_file, nml=freq_mode)
                close(unit=in_file)
                kyx_mode(i,1) = 1 + mod(ky + naky, naky)
                kyx_mode(i,2) = 1 + mod(kx + nakx, nakx)
             enddo
          else
             nk_freq = naky*nakx
             allocate (kyx_mode(nk_freq,2)); call alloc8(i2=kyx_mode,v="kyx_mode")
             do ik = 1, naky
                do it = 1, nakx
                   kyx_mode((ik-1)*nakx + it,1) = ik; kyx_mode((ik-1)*nakx + it,2) = it
                enddo
             enddo
          endif
       endif
!<JMT

    end if
    
    call broadcast (ireaderr)
    if (abort_readerr) then
       if (ireaderr > 0) then
          call mp_abort('Read error at diagnostics')
       else if (ireaderr < 0) then
          call mp_abort('End of file/record occurred at diagnostics')
       endif
    endif

  end subroutine read_parameters

  subroutine finish_agk_diagnostics (istep)
    use file_utils, only: open_output_file, close_output_file
    use mp, only: proc0, nproc, iproc, sum_reduce!, broadcast, barrier
    use species, only: nspec, spec, adapt_hc_any
    use run_parameters, only: use_Phi, use_Apar, use_Bpar
    use theta_grid, only: ntgrid, theta, dz, kpar
    use kgrids, only: naky, nakx, nx, ny, aky, akx
    use fields_arrays, only: phi, apar, bpar, phinew, aparnew
    use dist_fn, only: get_epar, getmoms
    use dist_fn, only: write_f, write_vp
    use dist_fn, only: getmoms_r !JMT
    use dist_fn_arrays, only: gnew
    use agk_transforms, only: transform2, inverse2
    use agk_save, only: agk_save_for_restart, save_for_restart
    use constants
    use agk_time, only: time, dtime
    use agk_io, only: nc_final_fields, nc_final_epar
    use agk_io, only: nc_final_moments, nc_finish
    use agk_io, only: nc_loop_freq, nc_loop_fullmom2  ! JMT
!    use antenna, only: dump_ant_amp
    use splines, only: fitp_surf1, fitp_surf2
! RN> bug fix in Makefile (3/20/08)
!    use agk_dist_io, only: write_dist
    use agk_dist_io, only: hdf_finish_dist
!RN(moved to main)    use hdf_wrapper, only: hdf_finish
! <RN
    use nonlinear_terms, only: nonlin    !GGH
    use agk_heating_index, only: finish_heating_index
    use agk_mem, only: alloc8, dealloc8
! KDN> Wrap up gk_eigfields
    use gk_eigen, only: close_gk_eigfields,gk_eigen_inited
! <KDN
    
    use kshell, only: finish_kshell, write_ktrans, write_gkshp
    implicit none
    integer, intent (in) :: istep
    integer :: ig, ik, it, is, unit, ierr
    real, dimension (:,:,:), allocatable :: bxf, byf, vxf, vyf, bxfsavg, byfsavg
    real, dimension (:,:,:), allocatable :: bxfs, byfs, vxfs, vyfs, rvx, rvy, rx, ry
    complex, dimension (:,:,:), allocatable :: bx, by, vx, vy, vx2, vy2
    complex, dimension (:,:,:), allocatable :: phi2, apar2, bpar2, epar
    complex, dimension (:,:,:,:), allocatable :: ntot, density, upar, uperp, tpar, tperp
    complex, dimension (:,:,:,:), allocatable :: &
         dens, ux, uy, uz, pxx, pyy, pzz, pxy, pyz, pzx, qzzz, qzpp
    real, dimension (:), allocatable :: dl_over_b
    complex, dimension (nakx, naky) :: phi0
    real, dimension (nakx, naky) :: phi02
    real, dimension (:), allocatable :: xx4, yy4
    real, dimension (:,:), allocatable :: bxs, bys, vxs, vys
    real, dimension (:,:), allocatable :: bxsavg, bysavg
    real, dimension (:), allocatable :: stemp, zx1, zxm, zy1, zyn, xx, yy
    real :: zxy11, zxym1, zxy1n, zxymn, L_x, L_y, rxt, ryt, bxt, byt
    integer :: istatus, nnx, nny, nnx4, nny4, ulim, llim, iblock, i, nout_freq, nout_fullmom
    logical :: last = .true.
    !GGH
    real :: nt1,dens1,upar1,tpar1,tperp1
    real :: fac2                                     !Factor
    real, dimension (-ntgrid:ntgrid) :: wgt

    if (debug) write(*,*) "finish_agk_diagnostics: proc",iproc,&
                                      " starting."
! KDN>
    if (proc0 .and. gk_eigen_inited) call close_gk_eigfields
    if (proc0 .and. dynfields_inited) call close_dynfields
!<KDN

    !Set up weighting factors for z-sums on proc0
    if (proc0) then
!       allocate (wgt(-ntgrid:ntgrid))
       wgt = 0.
       do ig=-ntgrid,ntgrid-1
          wgt(ig) = dz
       end do
       wgt = wgt/sum(wgt)         
    endif

!    TT changed MAB's version to use phi and bpar from arrays
!    if (write_g) call write_f (last, phi, bpar)
    if (write_g) call write_f (last)

    if (debug .and. proc0) write (*,*) "finish_agk_diagnostics: call write_f done."

    !Write out v-space slices for each Fourier mode
    if (write_vspace_slices) call write_vp

    if (debug .and. proc0) write (*,*) "finish_agk_diagnostics: call write_vp done."

! RN> bug fix in Makefile (3/20/08)
!    if (write_gg) call write_dist (g)
    ! TT: this call moved to loop_diagnostics
!    if (write_gg) call hdf_write_dist (gnew)
! <RN

    phi0 = 1.

    if (proc0) then
       if (write_ascii) call close_output_file (out_unit)
       if (write_ascii .and. write_hrate) then
          if (nspec == 2) then
             call close_output_file (heat_unit)
          else
             do is= 1, nspec
                call close_output_file (heat_unit2(is))
             enddo
          end if
       endif
       if (write_adapt_hc .and. write_ascii) then
          do is= 1, nspec
             call close_output_file (ahc_unit(is))
          enddo
          !Deallocate variable for ahc_unit
!          deallocate(ahc_unit)
          call dealloc8(i1=ahc_unit,v="ahc_unit")
       endif
       if (write_ascii .and. write_density_velocity) call close_output_file (dv_unit)
       if (write_ascii .and. write_jext) call close_output_file (jext_unit)
       if (write_ascii .and. write_Epolar) then
          call close_output_file (polar_raw_unit)
          call close_output_file (polar_avg_unit)
       endif

      if (debug) write(*,*) "finish_agk_diagnostics: closed a bunch of files."

       if (write_Eshell .or. write_ktrans .or. write_gkshp) call finish_kshell

      if (debug) write(*,*) "finish_agk_diagnostics: after finish_kshell."

       if (write_ptrans .and. write_ascii) then
          call close_output_file (ptrans_unit)
          call close_output_file (pflux_unit)
       end if
!>MAB
       if (write_ascii .and. write_verr) then
          call close_output_file (res_unit)
       end if

       if (write_ascii .and. write_vspectrum) then
          call close_output_file (vspec_unit)
       end if

       if (write_ascii .and. write_kspectrum) then
          call close_output_file (kspec_unit)
       end if
!<MAB

      if (debug) write(*,*) "finish_agk_diagnostics: closed more files."

       !Finish polar spectrum diagnostic (deallocate variables)
       if (write_Epolar) call finish_polar_spectrum
! KDN 110509:  The debug statements in finish_polar_spectrum are causing crashes
!              when nonlinear_mode='off'
      if (debug) write(*,*) "finish_agk_diagnostics: after finish_polar_spectrum."

       !Finish elsasser spectrum diagnostic (deallocate variables)
       if (write_Epolar .and. write_elsasser) then
!          deallocate(elzk,elzavg,elzraw)
          call dealloc8(r3=elzk,v="elzk")
          deallocate(elzavg) !!! RN> unable to account memory for pointers
          deallocate(elzraw) !!! RN> unable to account memory for pointers
          call close_output_file(elz_raw_unit)
          call close_output_file(elz_avg_unit)
        if (debug) write(*,*) "finish_agk_diagnostics: finished closing elsasser diagnostic."
       endif

       !Finish adaptive hypercollisionality (deallocate variables)
       if ((adapt_hc_any .or. write_adapt_hc) .and. proc0) call finish_adapt_hc

      if (debug) write(*,*) "finish_agk_diagnostics: after finish_adapt_hc."

       if (write_final_fields) then
          if (write_ascii) then
             call open_output_file (unit, ".fields")
             do ik = 1, naky
                do it = 1, nakx
                   do ig = -ntgrid, ntgrid
                      write (unit, "(9(1x,e12.5))") &
                           theta(ig), aky(ik), akx(it), &
                           phi(ig,it,ik), apar(ig,it,ik), bpar(ig,it,ik)
                   end do
                   write (unit, "()")
                end do
             end do
             call close_output_file (unit)
          end if
          call nc_final_fields
        if (debug) write(*,*) "finish_agk_diagnostics: after nc_final_fields."
       end if
       
!>JMT
       if (write_freq) then
          if (mod(istep,nwrite_freq) .eq. 0) then
             nout_freq = istep / nwrite_freq + 1
             call nc_loop_freq(nout_freq, time)
          endif
          call dealloc8(i2=kyx_mode,v="kyx_mode")
       endif
!<JMT

       if (write_kpar) then

          allocate ( phi2 (-ntgrid:ntgrid, nakx, naky)) ; call alloc8 (c3= phi2, v= "phi2")
          allocate (apar2 (-ntgrid:ntgrid, nakx, naky)) ; call alloc8 (c3=apar2, v="apar2")
          allocate (bpar2 (-ntgrid:ntgrid, nakx, naky)) ; call alloc8 (c3=bpar2, v="bpar2")

          phi2 = 0. ; apar2 = 0. ; bpar2 = 0.

          if (use_Phi) call par_spectrum(phi, phi2)
          if (use_Apar) call par_spectrum(apar, apar2)
          if (use_Bpar) call par_spectrum(bpar, bpar2)

          call open_output_file (unit, ".kpar")
!!$          !RN> kpar is now defined in theta_grid
!!$          do ig = 1, ntgrid
!!$             kpar(ig) = (ig-1)*gradpar/real(2*nperiod-1)
!!$             kpar(2*ntgrid-ig+1)=-(ig)*gradpar/real(2*nperiod-1)
!!$          end do
          
          do ik = 1, naky
             do it = 1, nakx
                do ig = ntgrid+1,2*ntgrid
                   write (unit, "(9(1x,e12.5))") &
                        kpar(ig), aky(ik), akx(it), &
                        phi2(ig-ntgrid-1,it,ik), &
                        apar2(ig-ntgrid-1,it,ik), &
                        bpar2(ig-ntgrid-1,it,ik)                        
                end do
                do ig = 1, ntgrid
                   write (unit, "(9(1x,e12.5))") &
                        kpar(ig), aky(ik), akx(it), &
                        phi2(ig-ntgrid-1,it,ik), &
                        apar2(ig-ntgrid-1,it,ik), &
                        bpar2(ig-ntgrid-1,it,ik)
                end do
                write (unit, "()")
             end do
          end do
          call close_output_file (unit)

          call dealloc8(c3= phi2, v= "phi2") 
          call dealloc8(c3=apar2, v="apar2") 
          call dealloc8(c3=bpar2, v="bpar2") 

        if (debug) write(*,*) "finish_agk_diagnostics: after write_kpar instructions."
       end if

       if (write_final_epar) then

          allocate (epar(-ntgrid:ntgrid, nakx, naky)) ; call alloc8 (c3=epar, v="epar") 
          epar = 0.

          call get_epar (phi, apar, phinew, aparnew, epar)
          if (write_ascii) then
             call open_output_file (unit, ".epar")
             do ik = 1, naky
                do it = 1, nakx
                   do ig = -ntg, ntg-1
                      write (unit, "(5(1x,e12.5))") &
                           theta(ig), aky(ik), akx(it), epar(ig,it,ik)
                   end do
                   write (unit, "()")
                end do
             end do
             call close_output_file (unit)
          end if
          call nc_final_epar (epar  )

          call dealloc8 (c3=epar, v="epar")

        if (debug) write(*,*) "finish_agk_diagnostics: after write_epar instructions."

       end if
    end if

    if (debug .and. proc0) write (*,*) "finish_agk_diagnostics: long list of proc0 tasks done."

    if (write_final_moments) then
       allocate (   ntot (-ntgrid:ntgrid, nakx, naky, nspec)) ; call alloc8 (c4=   ntot, v="ntot")
       allocate (density (-ntgrid:ntgrid, nakx, naky, nspec)) ; call alloc8 (c4=density, v="density")
       allocate (   upar (-ntgrid:ntgrid, nakx, naky, nspec)) ; call alloc8 (c4=   upar, v="upar")
       allocate (   uperp (-ntgrid:ntgrid, nakx, naky, nspec)) ; call alloc8 (c4=   uperp, v="uperp")
       allocate (   tpar (-ntgrid:ntgrid, nakx, naky, nspec)) ; call alloc8 (c4=   tpar, v="tpar")
       allocate (  tperp (-ntgrid:ntgrid, nakx, naky, nspec)) ; call alloc8 (c4=  tperp, v="tperp")

       call getmoms (ntot, density, upar, uperp, tpar, tperp)

       if (proc0) then
          if (write_ascii) then
             call open_output_file (unit, ".moments")
             phi0 = 1.
             do is  = 1, nspec
! MAB> 
!                nt1=0.;dens1=0.;upar1=0.;tpar1=0.;tperp=0.
                nt1=0.;dens1=0.;upar1=0.;tpar1=0.;tperp1=0.
! <MAB
                do ik = 1, naky
                   fac2 = 0.5
                   if (aky(ik) < epsilon(0.0)) fac2 = 1.0
                   do it = 1, nakx
                      ! Skip mean value (k=0) components
                      if (nonlin .and. it == 1 .and. ik == 1) cycle
                      do ig = -ntg, ntg
                         write (unit, "(14(1x,e12.5))") &
                              theta(ig), aky(ik), akx(it), &
                              ntot(ig,it,ik,is)/phi0(it,ik), &
                              density(ig,it,ik,is)/phi0(it,ik), &
                              upar(ig,it,ik,is)/phi0(it,ik), &
                              uperp(ig,it,ik,is)/phi0(it,ik), &
                              tpar(ig,it,ik,is)/phi0(it,ik), &
                              tperp(ig,it,ik,is)/phi0(it,ik), &
                              real(is)
                         !GGH-Sum all of the values over ntgrid
                         nt1=nt1+abs(ntot(ig,it,ik,is))*wgt(ig)*fac2
                         dens1=dens1+abs(density(ig,it,ik,is))*wgt(ig)*fac2
                         upar1=upar1+abs(upar(ig,it,ik,is))*wgt(ig)*fac2
                         tpar1=tpar1+abs(tpar(ig,it,ik,is))*wgt(ig)*fac2
                         tperp1=tperp1+abs(tperp(ig,it,ik,is))*wgt(ig)*fac2
                      end do
                      write (unit,"(5es12.4)") nt1, dens1, upar1, tpar1, tperp1
                      write (unit, "()")
                   end do
                end do
             end do
             call close_output_file (unit)          
          end if
          call nc_final_moments (ntot, density, upar, tpar, tperp)

          if (write_ascii) then
             call open_output_file (unit, ".mom2")
             phi0 = 1.
             phi02=real(phi0*conjg(phi0))
             do is  = 1, nspec
                do ik = 1, naky
                   do it = 1, nakx
                      do ig = -ntg, ntg
                         write (unit, "(14(1x,e12.5))") &
                              theta(ig), aky(ik), akx(it), &
                              real(ntot(ig,it,ik,is)*conjg(ntot(ig,it,ik,is)))/phi02(it,ik), &
                              real(density(ig,it,ik,is)*conjg(density(ig,it,ik,is)))/phi02(it,ik), &
                              real(upar(ig,it,ik,is)*conjg(upar(ig,it,ik,is)))/phi02(it,ik), &
                              real(tpar(ig,it,ik,is)*conjg(tpar(ig,it,ik,is)))/phi02(it,ik), &
                              real(tperp(ig,it,ik,is)*conjg(tperp(ig,it,ik,is)))/phi02(it,ik), &
                              real(is)
                      end do
                      write (unit, "()")
                   end do
                end do
             end do

             call close_output_file (unit)
             call open_output_file (unit, ".amoments")
             write (unit,*) 'type    kx     re(phi)    im(phi)    re(ntot)   im(ntot)   ',&
                  &'re(dens)   im(dens)   re(upar)   im(upar)   re(tpar)',&
                  &'   im(tpar)   re(tperp)  im(tperp)'
             
             allocate (dl_over_b(-ntgrid:ntgrid)) ; call alloc8 (r1=dl_over_b, v="dl_over_b")

             dl_over_b = dz
             dl_over_b = dl_over_b / sum(dl_over_b)

             do is  = 1, nspec
                do it = 1, nakx/2+1
                   write (unit, "(i2,14(1x,e10.3))") spec(is)%type, akx(it), &
                        sum( phinew(:,it,1)   *dl_over_b), &
                        sum(   ntot(:,it,1,is)*dl_over_b), &
                        sum(density(:,it,1,is)*dl_over_b), &
                        sum(   upar(:,it,1,is)*dl_over_b), &
                        sum(   tpar(:,it,1,is)*dl_over_b), &
                        sum(  tperp(:,it,1,is)*dl_over_b)
                end do
             end do
             call dealloc8 (r1=dl_over_b, v="dl_over_b") 
             call close_output_file (unit)          
          end if
       end if

       call dealloc8(c4=ntot,v="ntot")       
       call dealloc8(c4=density,v="density") 
       call dealloc8(c4=upar,v="upar")  
       call dealloc8(c4=uperp,v="uperp")            
       call dealloc8(c4=tpar,v="tpar")       
       call dealloc8(c4=tperp,v="tperp")     

    end if
!    if (proc0) write(*,*)'After Final moments'     !DEBUG-GGH-080324
    if (debug .and. proc0) write(*,*) "finish_agk_diagnostics: after final moments."

    ! JMT> output all moments at particle coord
    if (write_full_moments) then !JMT
      if(debug.and.proc0) write(*,*) "Loop diagnostics: starting write_full_moments." !DEBUG-KDN-110718
       allocate(dens(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=dens,v="dens")
       allocate(  ux(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=ux,v="ux")
       allocate(  uy(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=uy,v="uy")
       allocate(  uz(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=uz,v="uz")
       allocate( pxx(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=pxx,v="pxx")
       allocate( pyy(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=pyy,v="pyy")
       allocate( pzz(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=pzz,v="pzz")
       allocate( pxy(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=pxy,v="pxy")
       allocate( pyz(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=pyz,v="pyz")
       allocate( pzx(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=pzx,v="pzx")
       allocate( qzzz(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=qzzz,v="qzzz")
       allocate( qzpp(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=qzpp,v="qzpp")
       dens(:,:,:,:)=cmplx(0.,0.);
       ux(:,:,:,:)=cmplx(0.,0.);  uy(:,:,:,:)=cmplx(0.,0.)
       uz(:,:,:,:)=cmplx(0.,0.);
       pxx(:,:,:,:)=cmplx(0.,0.); pyy(:,:,:,:)=cmplx(0.,0.) 
       pzz(:,:,:,:)=cmplx(0.,0.); pxy(:,:,:,:)=cmplx(0.,0.)
       pyz(:,:,:,:)=cmplx(0.,0.); pzx(:,:,:,:)=cmplx(0.,0.)
       qzpp(:,:,:,:)=cmplx(0.,0.); qzpp(:,:,:,:)=cmplx(0.,0.)
       call getmoms_r(dens=dens, ux=ux, uy=uy, uz=uz, &
            & pxx=pxx, pyy=pyy, pzz=pzz, pxy=pxy, pyz=pyz, pzx=pzx, &
            qzzz=qzzz, qzpp=qzpp)
       if(proc0) then
          nout_fullmom = 1
          if (nwrite_fullmom > 0) &
             nout_fullmom = istep / nwrite_fullmom + nout_fullmom
          call nc_loop_fullmom2(time, nout_fullmom, dens, ux, uy, uz, pxx, pyy, pzz, pxy, pyz, pzx, qzzz, qzpp)
       endif
       call dealloc8(c4=dens,v="dens")
       call dealloc8(c4=ux,v="ux")
       call dealloc8(c4=uy,v="uy")
       call dealloc8(c4=uz,v="uz")
       call dealloc8(c4=pxx,v="pxx")
       call dealloc8(c4=pyy,v="pyy")
       call dealloc8(c4=pzz,v="pzz")
       call dealloc8(c4=pxy,v="pxy")
       call dealloc8(c4=pyz,v="pyz")
       call dealloc8(c4=pzx,v="pzx")
       call dealloc8(c4=qzzz,v="qzzz")
       call dealloc8(c4=qzpp,v="qzpp")
      if(debug.and.proc0) write(*,*) "Loop diagnostics: finished write_full_moments_r." !DEBUG-KDN-110718

    endif
    ! <JMT

    if (save_for_restart) then
       call agk_save_for_restart (gnew, time, dtime(1), istatus, &
            use_Phi, use_Apar, use_Bpar, .true.)
    end if
    if (debug .and. proc0) write(*,*) "finish_agk_diagnostics: after save for restart."

    call nc_finish
!    if (proc0) write(*,*)'After nc_finish'     !DEBUG-GGH-080324

    !GGH- Commented this out as unnecessary 07 Jun 07
!    if (proc0) call dump_ant_amp

    if (write_gs) then
       nny = 2*ny
       nnx = 2*nx
       nnx4 = nnx+4
       nny4 = nny+4

       allocate (bx(-ntgrid:ntgrid,nakx,naky)) ; call alloc8 (c3=bx, v="bx")
       allocate (by(-ntgrid:ntgrid,nakx,naky)) ; call alloc8 (c3=by, v="by")
       allocate (vx(-ntgrid:ntgrid,nakx,naky)) ; call alloc8 (c3=vx, v="vx")
       allocate (vy(-ntgrid:ntgrid,nakx,naky)) ; call alloc8 (c3=vy, v="vy")

       do ik=1,naky
          do it=1,nakx
             do ig=-ntgrid, ntgrid
                bx(ig,it,ik) =  zi*aky(ik)*apar(ig,it,ik)
                by(ig,it,ik) = -zi*akx(it)*apar(ig,it,ik)
                vx(ig,it,ik) = -zi*aky(ik)*phi(ig,it,ik)
                vy(ig,it,ik) =  zi*akx(it)*phi(ig,it,ik)
             end do
          end do
       end do

       allocate (bxf(nnx,nny,-ntgrid:ntgrid)) ; call alloc8 (r3=bxf, v="bxf")
       allocate (byf(nnx,nny,-ntgrid:ntgrid)) ; call alloc8 (r3=byf, v="byf")
       allocate (vxf(nnx,nny,-ntgrid:ntgrid)) ; call alloc8 (r3=vxf, v="vxf")
       allocate (vyf(nnx,nny,-ntgrid:ntgrid)) ; call alloc8 (r3=vyf, v="vyf")

       call transform2 (bx, bxf, nny, nnx)
       call transform2 (by, byf, nny, nnx)
       call transform2 (vx, vxf, nny, nnx)
       call transform2 (vy, vyf, nny, nnx)
       
       ! fields come out as (x, y, z)

!       deallocate (bx, by)
       call dealloc8(c3=bx,v="bx")
       call dealloc8(c3=by,v="by")

       allocate (   bxfs(nnx4, nny4, -ntgrid:ntgrid)) ; call alloc8 (r3=bxfs, v="bxfs")
       allocate (   byfs(nnx4, nny4, -ntgrid:ntgrid)) ; call alloc8 (r3=byfs, v="byfs")
       allocate (bxfsavg(nnx4, nny4, -ntgrid:ntgrid)) ; call alloc8 (r3=bxfsavg, v="bxfsavg"); bxfsavg=0.
       allocate (byfsavg(nnx4, nny4, -ntgrid:ntgrid)) ; call alloc8 (r3=byfsavg, v="byfsavg"); byfsavg=0.
       allocate (   vxfs(nnx4, nny4, -ntgrid:ntgrid)) ; call alloc8 (r3=vxfs, v="vxfs")
       allocate (   vyfs(nnx4, nny4, -ntgrid:ntgrid)) ; call alloc8 (r3=vyfs, v="vyfs")

       do ig=-ntgrid,ntgrid
          do ik=1,2
             do it=3,nnx4-2
                bxfs(it,ik,ig) = bxf(it-2,nny-2+ik,ig)
                byfs(it,ik,ig) = byf(it-2,nny-2+ik,ig)
                vxfs(it,ik,ig) = vxf(it-2,nny-2+ik,ig)
                vyfs(it,ik,ig) = vyf(it-2,nny-2+ik,ig)

                bxfs(it,nny4-2+ik,ig) = bxf(it-2,ik,ig)
                byfs(it,nny4-2+ik,ig) = byf(it-2,ik,ig)
                vxfs(it,nny4-2+ik,ig) = vxf(it-2,ik,ig)
                vyfs(it,nny4-2+ik,ig) = vyf(it-2,ik,ig)
             end do
          end do
          do ik=3,nny4-2
             do it=3,nnx4-2
                bxfs(it,ik,ig) = bxf(it-2,ik-2,ig)
                byfs(it,ik,ig) = byf(it-2,ik-2,ig)
                vxfs(it,ik,ig) = vxf(it-2,ik-2,ig)
                vyfs(it,ik,ig) = vyf(it-2,ik-2,ig)
             end do
          end do
          do ik=1,nny4
             do it=1,2
                bxfs(it,ik,ig) = bxfs(nnx4-4+it,ik,ig)
                byfs(it,ik,ig) = byfs(nnx4-4+it,ik,ig)
                vxfs(it,ik,ig) = vxfs(nnx4-4+it,ik,ig)
                vyfs(it,ik,ig) = vyfs(nnx4-4+it,ik,ig)

                bxfs(nnx4-2+it,ik,ig) = bxfs(it+2,ik,ig)
                byfs(nnx4-2+it,ik,ig) = byfs(it+2,ik,ig)
                vxfs(nnx4-2+it,ik,ig) = vxfs(it+2,ik,ig)
                vyfs(nnx4-2+it,ik,ig) = vyfs(it+2,ik,ig)
             end do
          end do
       end do

       call dealloc8 (r3=vxf, v="vxf") 
       call dealloc8 (r3=vyf, v="vyf") 

       allocate (xx4(nnx4)); call alloc8(r1=xx4,v="xx4")
       allocate (xx(nnx));   call alloc8(r1=xx,v="xx")
       allocate (yy4(nny4)); call alloc8(r1=yy4,v="yy4")
       allocate (yy(nny));   call alloc8(r1=yy,v="yy")
       
       L_x = 2.0*pi/akx(2)
       L_y = 2.0*pi/aky(2)

       do it = 1, nnx
          xx4(it+2) = real(it-1)*L_x/real(nnx)
          xx(it) = real(it-1)*L_x/real(nnx)
       end do
       do it=1,2
          xx4(it) = xx4(nnx4-4+it)-L_x
          xx4(nnx4-2+it) = xx4(it+2)+L_x
       end do

       do ik = 1, nny
          yy4(ik+2) = real(ik-1)*L_y/real(nny)
          yy(ik)    = real(ik-1)*L_y/real(nny)
       end do
       do ik=1,2
          yy4(ik) = yy4(nny4-4+ik)-L_y
          yy4(nny4-2+ik) = yy4(ik+2)+L_y
       end do

       allocate (   bxs(3*nnx4*nny4,-ntgrid:ntgrid)) ; call alloc8 (r2=bxs, v="bxs")
       allocate (   bys(3*nnx4*nny4,-ntgrid:ntgrid)) ; call alloc8 (r2=bys, v="bys")
       allocate (   vxs(3*nnx4*nny4,-ntgrid:ntgrid)) ; call alloc8 (r2=vxs, v="vxs")
       allocate (   vys(3*nnx4*nny4,-ntgrid:ntgrid)) ; call alloc8 (r2=vys, v="vys")

       bxs = 0. ; bys = 0. ; vxs = 0. ; vys = 0.

       allocate (bxsavg(3*nnx4*nny4,-ntgrid:ntgrid)) ; call alloc8 (r2=bxsavg, v="bxsavg"); bxsavg=0.
       allocate (bysavg(3*nnx4*nny4,-ntgrid:ntgrid)) ; call alloc8 (r2=bysavg, v="bysavg"); bysavg=0.

       allocate (stemp(nnx4+2*nny4)) ; call alloc8 (r1=stemp, v="stemp"); stemp=0.
       allocate (zx1(nny4)); call alloc8(r1=zx1,v="zx1"); zx1=0.
       allocate (zxm(nny4)); call alloc8(r1=zxm,v="zxm"); zxm=0.
       allocate (zy1(nnx4)); call alloc8(r1=zy1,v="zy1"); zy1=0.
       allocate (zyn(nnx4)); call alloc8(r1=zyn,v="zyn"); zyn=0.

       do ig=-ntgrid, ntgrid
          call fitp_surf1(nnx4, nny4, xx4, yy4, bxfs(:,:,ig), &
               nnx4, zx1, zxm, zy1, zyn, zxy11, zxym1, zxy1n, zxymn, &
               255, bxs(:,ig), stemp, 1., ierr)

          call fitp_surf1(nnx4, nny4, xx4, yy4, byfs(:,:,ig), &
               nnx4, zx1, zxm, zy1, zyn, zxy11, zxym1, zxy1n, zxymn, &
               255, bys(:,ig), stemp, 1., ierr)

          call fitp_surf1(nnx4, nny4, xx4, yy4, vxfs(:,:,ig), &
               nnx4, zx1, zxm, zy1, zyn, zxy11, zxym1, zxy1n, zxymn, &
               255, vxs(:,ig), stemp, 1., ierr)

          call fitp_surf1(nnx4, nny4, xx4, yy4, vyfs(:,:,ig), &
               nnx4, zx1, zxm, zy1, zyn, zxy11, zxym1, zxy1n, zxymn, &
               255, vys(:,ig), stemp, 1., ierr)
       end do
       
       call dealloc8 (r1=stemp, v="stemp") 
!       deallocate (zx1, zxm, zy1, zyn)
       call dealloc8(r1=zx1,v="zx1")
       call dealloc8(r1=zxm,v="zxm")
       call dealloc8(r1=zy1,v="zy1")
       call dealloc8(r1=zyn,v="zyn")

       do ig=-ntgrid, ntgrid-1
          bxsavg(:,ig) = 0.5*(bxs(:,ig)+bxs(:,ig+1))
          bysavg(:,ig) = 0.5*(bys(:,ig)+bys(:,ig+1))

          bxfsavg(:,:,ig) = 0.5*(bxfs(:,:,ig)+bxfs(:,:,ig+1))
          byfsavg(:,:,ig) = 0.5*(byfs(:,:,ig)+byfs(:,:,ig+1))
       end do

       ! now, integrate to find a field line using the modified Euler method
       ! dx/dz = Bx, dy/dz = By
       allocate ( rx(nnx,nny,-ntgrid:ntgrid)) ; call alloc8 (r3=rx, v="rx"); rx=0.
       allocate ( ry(nnx,nny,-ntgrid:ntgrid)) ; call alloc8 (r3=ry, v="ry"); ry=0.
       allocate (rvx(nnx,nny,-ntgrid:ntgrid)) ; call alloc8 (r3=rvx, v="rvx"); rvx=0.
       allocate (rvy(nnx,nny,-ntgrid:ntgrid)) ; call alloc8 (r3=rvy, v="rvy"); rvy=0.

       do ik=1,nny
          do it=1,nnx
             rx(it,ik,-ntgrid) = xx(it)
             ry(it,ik,-ntgrid) = yy(ik)
          end do
       end do

       iblock = (nnx*nny-1)/nproc + 1
       llim = 1 + iblock * iproc
       ulim = min(nnx*nny, llim+iblock-1)

       do i=llim, ulim
          it = 1 + mod(i-1, nnx)
          ik = 1 + mod((i-1)/nnx, nny)
          
          ig = -ntgrid
          
          rxt = rx(it,ik,ig)
          ryt = ry(it,ik,ig)
          
          rvx(it,ik,ig) = fitp_surf2(rxt, ryt, nnx4, nny4, xx4, yy4, vxfs(:,:,ig), nnx4, vxs(:,ig), 1.)
          rvy(it,ik,ig) = fitp_surf2(rxt, ryt, nnx4, nny4, xx4, yy4, vyfs(:,:,ig), nnx4, vys(:,ig), 1.)
          
          do ig=-ntgrid,ntgrid-1

             bxt = fitp_surf2(rxt, ryt, nnx4, nny4, xx4, yy4, bxfs(:,:,ig), nnx4, bxs(:,ig), 1.)
             byt = fitp_surf2(rxt, ryt, nnx4, nny4, xx4, yy4, byfs(:,:,ig), nnx4, bys(:,ig), 1.)

             rxt = rx(it,ik,ig) + 0.5*dz*bxt
             ryt = ry(it,ik,ig) + 0.5*dz*byt

             if (rxt > L_x) rxt = rxt - L_x
             if (ryt > L_y) ryt = ryt - L_y

             if (rxt < 0.) rxt = rxt + L_x
             if (ryt < 0.) ryt = ryt + L_y

             bxt = fitp_surf2(rxt, ryt, nnx4, nny4, xx4, yy4, bxfsavg(:,:,ig), nnx4, bxsavg(:,ig), 1.)
             byt = fitp_surf2(rxt, ryt, nnx4, nny4, xx4, yy4, byfsavg(:,:,ig), nnx4, bysavg(:,ig), 1.)

             rxt = rx(it,ik,ig) + dz*bxt
             ryt = ry(it,ik,ig) + dz*byt

             if (rxt > L_x) rxt = rxt - L_x
             if (ryt > L_y) ryt = ryt - L_y

             if (rxt < 0.) rxt = rxt + L_x
             if (ryt < 0.) ryt = ryt + L_y

             rx(it,ik,ig+1) = rxt
             ry(it,ik,ig+1) = ryt

             rvx(it,ik,ig+1) = fitp_surf2(rxt, ryt, nnx4, nny4, xx4, yy4, vxfs(:,:,ig+1), nnx4, vxs(:,ig+1), 1.)
             rvy(it,ik,ig+1) = fitp_surf2(rxt, ryt, nnx4, nny4, xx4, yy4, vyfs(:,:,ig+1), nnx4, vys(:,ig+1), 1.)
          end do
       end do

       call dealloc8 (r3=vxfs, v="vxfs") 
       call dealloc8 (r3=vyfs, v="vyfs") 
       call dealloc8 (r3=bxfs, v="bxfs") 
       call dealloc8 (r3=byfs, v="byfs") 
       call dealloc8 (r3=bxfsavg, v="bxfsavg") 
       call dealloc8 (r3=byfsavg, v="byfsavg") 

       call dealloc8 (r3=rx, v="rx") 
       call dealloc8 (r3=ry, v="ry") 
       call dealloc8 (r2=bxs, v="bxs") 
       call dealloc8 (r2=bys, v="bys") 
       call dealloc8 (r2=vxs, v="vxs") 
       call dealloc8 (r2=vys, v="vys") 
       call dealloc8 (r2=bxsavg, v="bxsavg") 
       call dealloc8 (r2=bysavg, v="bysavg") 

       call sum_reduce(rvx,0)
       call sum_reduce(rvy,0)

       if (proc0) then
          call inverse2 (rvx, vx, nny, nnx)
          call inverse2 (rvy, vy, nny, nnx)

          allocate (vx2(-ntgrid:ntgrid,nakx,naky)); call alloc8(c3=vx2,v="vx2")
          allocate (vy2(-ntgrid:ntgrid,nakx,naky)); call alloc8(c3=vy2,v="vy2")

          call par_spectrum (vx, vx2)
          call par_spectrum (vy, vy2)

          call open_output_file (unit, ".gs")
!!$          !RN> kpar is now defined in theta_grid
!!$          do ig = 1, ntgrid
!!$             kpar(ig) = (ig-1)*gradpar/real(2*nperiod-1)
!!$             kpar(2*ntgrid-ig+1)=-(ig)*gradpar/real(2*nperiod-1)
!!$          end do
          do ik = 1, naky
             do it = 1, nakx
                do ig = ntgrid+1,2*ntgrid
                   write (unit, "(9(1x,e12.5))") &
                        kpar(ig), aky(ik), akx(it), &
                        real(vx2(ig-ntgrid-1,it,ik)), &
                        real(vy2(ig-ntgrid-1,it,ik))
                end do
                do ig = 1, ntgrid
                   write (unit, "(9(1x,e12.5))") &
                        kpar(ig), aky(ik), akx(it), &
                        real(vx2(ig-ntgrid-1,it,ik)), &
                        real(vy2(ig-ntgrid-1,it,ik))
                end do
                write (unit, "()")
             end do
          end do
          call close_output_file (unit)
!          deallocate (vx2, vy2)
          call dealloc8(c3=vx2,v="vx2")
          call dealloc8(c3=vy2,v="vy2")
       end if

       call dealloc8 (c3=vx, v="vx") 
       call dealloc8 (c3=vy, v="vy") 
       call dealloc8 (r3=rvx, v="rvx") 
       call dealloc8 (r3=rvy, v="rvy") 

       !GGH This may have been causing a crash???
!       if (proc0) deallocate(wgt)

    end if

    if (debug .and. proc0) write(*,*) "finish_agk_diagnostics: after write_gs."

!    if (proc0) write(*,*)'Diagnostics finalized'     !DEBUG-GGH-080324

    if (write_gg) call hdf_finish_dist
!!!RN moved to main routine
!!!RN    if (write_gg) call hdf_finish
    
    if (write_moi .and. proc0) call finish_stream_flux_potentials

    if (debug .and. proc0) write(*,*) "finish_agk_diagnostics: after sfpots."

    if (debug .and. proc0) write (*,*) "finish_agk_diagnostics: done."

  end subroutine finish_agk_diagnostics

  subroutine loop_diagnostics (istep, exit)
    use species, only: nspec, spec, adapt_hc_any, specie
    use theta_grid, only: ntgrid, dz, z0, delthet, jacob
    use theta_grid, only: kpar
    use kgrids, only: naky, nakx, aky, akx, aky
    use kgrids, only: nkpolar,kperp2
    use run_parameters, only: use_Phi, use_Apar, use_Bpar,beta
    use fields_arrays, only: phinew, aparnew, bparnew
    use dist_fn, only: flux, write_f
    use dist_fn, only: omega0, gamma0, getmoms
    use dist_fn, only: get_verr, g_adjust  ! MAB
! TT>
    use dist_fn, only: write_vspec
    use dist_fn_arrays, only: gnew, gp
    use agk_dist_io, only: hdf_write_dist
    use kshell, only: write_shell_spectrum, write_g2_kshell_p, write_gkshp
    use kshell, only: write_ktransfer, write_ktrans
! <TT
    use mp, only: proc0, broadcast
    use file_utils, only: flush_output_file
!    use agk_time, only: update_time, time ! RN
    use agk_time, only: time
    use agk_io, only: nc_qflux, nc_vflux, nc_pflux, nc_loop, nc_loop_moments
    use agk_io, only: nc_loop_fullmom
    use agk_io, only: nc_loop_movie
    use agk_io, only: nc_loop_vspec  ! MAB
    use agk_io, only: nc_loop_freq, nc_loop_fullmom2  ! JMT
    use agk_layouts, only: yxf_lo
    use agk_layouts, only: le_lo
    use agk_transforms, only: transform2
    use le_grids, only: nlambda, negrid, npgrid
    use le_grids, only: vtransform2d, nesub, nvpa, nvpe, integrate_moment  ! MAB
    use antenna, only: antenna_w
    use agk_heating, only: dens_vel_diagnostics, init_dvtype, del_dvtype
    use agk_heating_index
!>GGH - Needed for adaptive hypercollisionality, 2007 SEP 07
    use collisions, only: update_vnewh
!<GGH
! KDN> - Needed for gk_eigenfields
    use gk_eigen, only:gk_eigfields,gk_eigen_inited
!<KDN
    use collisions, only: redist_g2le  ! MAB
    use constants
    use dist_fn, only: getmoms_r, getmoms_r_temp
    use agk_mem, only: alloc8, dealloc8
    implicit none
    integer :: nout = 1
    integer :: nout_movie = 1
    integer :: nout_freq, nout_fullmom
    integer, intent (in) :: istep
    logical, intent (out) :: exit
    real, dimension(:,:,:), allocatable :: yxphi, yxapar, yxbpar
    real, dimension(:,:,:), allocatable :: yxden
    complex, dimension (nakx, naky) :: omega, omegaavg

    type (dens_vel_diagnostics) :: dv
    type (dens_vel_diagnostics), dimension(:,:), allocatable :: dvk
    !GGH J_external
    real, dimension(:,:), allocatable ::  j_ext

    real, dimension (nakx, naky) :: phitot
    real :: phi2, apar2, bpar2
    real, dimension (nakx, naky) :: phi2_by_mode, apar2_by_mode, bpar2_by_mode
    real, dimension (nakx, naky, nspec) :: ntot2_by_mode, ntot20_by_mode
    integer :: ig, ik, it, is, il, i, j, nnx, nny, ifield
    complex :: sourcefac
    complex, dimension (-ntgrid:ntgrid,nakx,naky,nspec) :: &
         & ntot, density, upar, uperp, tpar, tperp
    complex, dimension (:,:,:,:), allocatable :: &
         dens, ux, uy, uz, pxx, pyy, pzz, pxy, pyz, pzx, qzzz, qzpp
    complex, dimension (nakx, nspec) :: ntot00, density00, upar00, tpar00, tperp00
    complex, dimension (nakx) :: phi00
    complex, save :: wtmp_new
    complex :: wtmp_old = 0.
    real, dimension (nspec) ::  heat_fluxes,  part_fluxes, mom_fluxes,  ntot2, ntot20
    real, dimension (nspec) :: mheat_fluxes, mpart_fluxes, mmom_fluxes
    real, dimension (nspec) :: bheat_fluxes, bpart_fluxes, bmom_fluxes
    real, dimension (nspec) ::  heat_par,  heat_perp
    real, dimension (nspec) :: mheat_par, mheat_perp
    real, dimension (nspec) :: bheat_par, bheat_perp
    real, dimension (naky) :: fluxfac
!    real, dimension (:), allocatable :: phi_by_k, apar_by_k, bpar_by_k
!>MAB
    real, dimension (:,:), allocatable :: errest 
    integer, dimension (:,:), allocatable :: erridx
!<MAB
! TT> E(kperp,p)
    integer :: ip, iavg
    real, dimension (:,:,:), allocatable :: ekp
    real, dimension (:,:), allocatable :: ptrans
    ! velocity transfer function
    integer :: ipfrom, ipto
! <TT
    real :: hflux_tot, zflux_tot, vflux_tot
    real, dimension (-ntgrid:ntgrid) :: wgt

! MAB>
    integer :: nxi, iq, ie, ile
    real :: normfac1, normfac2, delp
    complex, dimension (:,:,:), allocatable :: gle, gvxi, gpq
    real, dimension (:,:,:,:), allocatable :: gvxiavg, gpqavg
    real, dimension (:,:,:), allocatable :: gvxi0, gpq0
    real, dimension (:,:,:), allocatable :: h2kk
! <MAB

    exit = .false.

    if (debug.and.proc0) write (*,*) "Loop diagnostics: starting." !DEBUG-KDN-110715
    if (proc0) call get_omegaavg (istep, exit, omegaavg)
    call broadcast (exit)
    if (debug.and.proc0) write (*,*) "Loop diagnostics: Finished get_omegaavg." !DEBUG-KDN-110715
! KDN>
    if (proc0 .and. gk_eigen_inited) call gk_eigfields(istep)
    if (proc0 .and. debug .and. gk_eigen_inited) write (*,*) "Loop diagnostics: gk_eigfields."
    if (proc0 .and. do_write_dynfields) call write_dynfields(istep)
    if (proc0 .and. debug .and. do_write_dynfields) write (*,*) "Loop diagnostics: dynfields"
!<KDN

    if (write_hrate .and. &
         nwrite-mod(istep+nwrite-1,nwrite) <= navg) then
       if(debug.and.proc0) write(*,*) "Loop diagnostics: starting write_hrate." !DEBUG-KDN-110718
       call heating (istep, h, hk)
       if(debug.and.proc0) write(*,*) "Loop diagnostics: finished write_hrate." !DEBUG-KDN-110718
    end if

!>JMT
    if (proc0 .and. write_freq) then
       
       if (mod(istep,nwrite_freq) .eq. 0) then
          nout_freq = istep / nwrite_freq + 1
          call nc_loop_freq(nout_freq, time)
       endif
    endif

      ! JMT> output all moments at particle coord
    if (write_full_moments) then !JMT
       if (mod(istep,nwrite_fullmom) .eq. 0 .and. nwrite_fullmom > 0) then
          if(debug.and.proc0) write(*,*) "Loop diagnostics: starting write_full_moments." !DEBUG-KDN-110718
          allocate(dens(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=dens,v="dens")
          allocate(  ux(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=ux,v="ux")
          allocate(  uy(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=uy,v="uy")
          allocate(  uz(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=uz,v="uz")
          allocate( pxx(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=pxx,v="pxx")
          allocate( pyy(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=pyy,v="pyy")
          allocate( pzz(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=pzz,v="pzz")
          allocate( pxy(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=pxy,v="pxy")
          allocate( pyz(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=pyz,v="pyz")
          allocate( pzx(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=pzx,v="pzx")
          allocate( qzzz(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=qzzz,v="qzzz")
          allocate( qzpp(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=qzpp,v="qzpp")
          dens(:,:,:,:)=cmplx(0.,0.);
          ux(:,:,:,:)=cmplx(0.,0.);  uy(:,:,:,:)=cmplx(0.,0.)
          uz(:,:,:,:)=cmplx(0.,0.);
          pxx(:,:,:,:)=cmplx(0.,0.); pyy(:,:,:,:)=cmplx(0.,0.) 
          pzz(:,:,:,:)=cmplx(0.,0.); pxy(:,:,:,:)=cmplx(0.,0.)
          pyz(:,:,:,:)=cmplx(0.,0.); pzx(:,:,:,:)=cmplx(0.,0.)
          qzpp(:,:,:,:)=cmplx(0.,0.); qzpp(:,:,:,:)=cmplx(0.,0.)
          call getmoms_r(dens=dens, ux=ux, uy=uy, uz=uz, &
               & pxx=pxx, pyy=pyy, pzz=pzz, pxy=pxy, pyz=pyz, pzx=pzx, &
               qzzz=qzzz, qzpp=qzpp)
          if(proc0) then
             nout_fullmom = istep / nwrite_fullmom + 1
             call nc_loop_fullmom2(time, nout_fullmom, &
                  dens, ux, uy, uz, pxx, pyy, pzz, pxy, pyz, pzx, qzzz, qzpp)
          endif
          call dealloc8(c4=dens,v="dens")
          call dealloc8(c4=ux,v="ux")
          call dealloc8(c4=uy,v="uy")
          call dealloc8(c4=uz,v="uz")
          call dealloc8(c4=pxx,v="pxx")
          call dealloc8(c4=pyy,v="pyy")
          call dealloc8(c4=pzz,v="pzz")
          call dealloc8(c4=pxy,v="pxy")
          call dealloc8(c4=pyz,v="pyz")
          call dealloc8(c4=pzx,v="pzx")
          call dealloc8(c4=qzzz,v="qzzz")
          call dealloc8(c4=qzpp,v="qzpp")
          if(debug.and.proc0) write(*,*) "Loop diagnostics: finished write_full_moments_r." !DEBUG-KDN-110718
          
       endif
    endif
       ! <JMT


!<JMT

!>GGH
    !Write density and velocity perturbations
    if (write_density_velocity) then
      if(debug.and.proc0) write(*,*) "Loop diagnostics: starting write_density_velocity." !DEBUG-KDN-110718
       call init_dvtype (dv,  nspec)   ! memory leak if turned on
       allocate (dvk(nakx, naky))      ! memory leak if turned on !!! RN> unable to account memory for structure variables
       call init_dvtype (dvk, nspec)   ! memory leak if turned on

       call dens_vel(istep,dv,dvk)
      if(debug.and.proc0) write(*,*) "Loop diagnostics: finished write_density_velocity." !DEBUG-KDN-110718
    endif
    !Write Jexternal vs. time
    if (write_jext) then
      if(debug.and.proc0) write(*,*) "Loop diagnostics: starting write_jext." !DEBUG-KDN-110718
       allocate (j_ext(nakx, naky)); call alloc8(r2=j_ext,v="j_ext")
       j_ext=0.
       call calc_jext(istep,j_ext)
      if(debug.and.proc0) write(*,*) "Loop diagnostics: finished write_jext." !DEBUG-KDN-110718
    endif
!<GGH

    ! RN> No reason to call this here
!    call update_time 
    ! <RN

    if (make_movie .and. mod(istep,nmovie)==0) then
      if(debug.and.proc0) write(*,*) "Loop diagnostics: starting make_movie." !DEBUG-KDN-110718
       ! EAB 09/17/03 -- modify dump_fields_periodically to print out 
       ! inverse fft of fields in x,y
       ! write(*,*) "iproc now in dump_fields_periodically case", iproc 
       nnx = yxf_lo%nx
       nny = yxf_lo%ny
       
       allocate (yxden(nnx,nny,-ntgrid:ntgrid)); call alloc8(r3=yxden,v="yxden")
       call getmoms(ntot,density,upar,uperp,tpar,tperp)
       ! RN>
       ! transform2_4d uses ntot(:,:,:,1) (ntot of spec=1)
       ! I think using transform2_3d is more intuitive
!       call transform2(ntot, yxden, nny, nnx)
       call transform2(ntot(:,:,:,1), yxden, nny, nnx)
       ! <RN

       if (use_Phi) then
          allocate (yxphi(nnx,nny,-ntgrid:ntgrid)); call alloc8(r3=yxphi,v="yxphi")
          call transform2 (phinew, yxphi, nny, nnx)
       end if
       if (use_Apar) then
          allocate (yxapar(nnx,nny,-ntgrid:ntgrid)); call alloc8(r3=yxapar,v="yxapar")
          call transform2 (aparnew, yxapar, nny, nnx)
       end if
       if (use_Bpar) then 
          allocate (yxbpar(nnx,nny,-ntgrid:ntgrid)); call alloc8(r3=yxbpar,v="yxbpar")
          call transform2 (bparnew, yxbpar, nny, nnx)
       end if

       if (proc0) then
          call nc_loop_movie(nout_movie, time, yxphi, yxapar, yxbpar, yxden)
       end if

!       deallocate (yxden)
       call dealloc8(r3=yxden,v="yxden")
!       if (use_Phi) deallocate (yxphi)
!       if (use_Apar) deallocate (yxapar)
!       if (use_Bpar) deallocate (yxbpar)
       if (use_Phi) call dealloc8 (r3=yxphi,v="yxphi")
       if (use_Apar) call dealloc8 (r3=yxapar,v="yxapar")
       if (use_Bpar) call dealloc8 (r3=yxbpar,v="yxbpar")
       nout_movie = nout_movie + 1

      if(debug.and.proc0) write(*,*) "Loop diagnostics: finished make_movie." !DEBUG-KDN-110718
    end if

    !----------------------------------------------------!
    ! Go back to the main routine if you don't write out !
    !----------------------------------------------------!
    if (mod(istep,nwrite) /= 0 .and. .not. exit) then
       !Deallocate variables for density and velocity perturbations
       if (write_density_velocity) then
          call del_dvtype (dv)
          call del_dvtype (dvk)
          deallocate(dvk) !!! RN> unable to account memory for structure variables
       endif
       ! Deallocate variable for Jexternal
!       if (write_jext) deallocate(j_ext)
       if (write_jext) call dealloc8(r2=j_ext,v="j_ext")
       return
    end if
    !----------------------------------------------------!
    ! Go back to the main routine if you don't write out !
    !----------------------------------------------------!

    if (istep==0 .and. .not.write_init) return

! TT: Gabe's velocity space spectra diagnostics
    if (write_gkp .or. write_gkshp) then
       if(debug.and.proc0) write(*,*) "Loop diagnostics: starting gkp."
       allocate (ekp(nakx, naky, npgrid)); call alloc8(r3=ekp,v="ekp")
       ! first write out E(p) in file .vspec
       call write_vspec (gnew, igomega, ekp) ! E_g(p)
       if (mod(istep,nwrite2)==0) then
          if (write_gkp) call write_gkpp (ekp) ! E_g(kperp,p)
          if (write_gkshp) call write_g2_kshell_p (ekp) ! E_g(kshell,p)
       end if
!       deallocate (ekp)
       call dealloc8(r3=ekp,v="ekp")
       if(debug.and.proc0) write(*,*) "Loop diagnostics: finished gkp."
    end if
! TT: another measure of the velocity space structure
    if (write_hkv) call write_dvperp

! TT: k-shell averaged spectrum
    if (write_Eshell) call write_shell_spectrum

! TT: shell-to-shell nonlinear transfer
!     This is heavy.  For 256^2 x 192^2 grid with 9216 procs,
!     a single call to this takes like 12 minutes on Kraken
    if (write_ktrans .and. mod(istep,nwrite2)==0) call write_ktransfer

    if (write_ptrans) then
       allocate (ptrans(npgrid,npgrid)); call alloc8(r2=ptrans,v="ptrans")
       call get_ptransfer (ptrans)
       if (proc0) then
          ! write transfer function
          do ipfrom=1, npgrid
             do ipto=1, npgrid
                write (ptrans_unit, '(3f10.5,es15.5)') time, gp(ipfrom), &
                     & gp(ipto), ptrans(ipfrom,ipto)
             end do
             write (ptrans_unit, '()')
          end do
          write (ptrans_unit, '()')
          ! write flux
          do ip=1, npgrid
             write (pflux_unit, '(2f10.5,2es15.5)') time, gp(ip), &
                  & sum(ptrans(1:ip-1,ip)), sum(ptrans(ip+1:npgrid,ip))
          end do
          write (pflux_unit, '()')
          ! flush files
          call flush_output_file (ptrans_unit)
          call flush_output_file (pflux_unit)
       end if
!       deallocate (ptrans)
       call dealloc8(r2=ptrans,v="ptrans")
    end if

!    if (write_g) call write_f (last)

!>MAB
    if (write_vspectrum) then

       nxi = 2*nlambda

       delp = min(nlambda,nesub)/real(nvpe)

       allocate (gle(nxi+1, negrid+1, le_lo%llim_proc:le_lo%ulim_alloc)); call alloc8(c3=gle,v="gle")
       gle = 0.0
       allocate (gvxi(nlambda, nesub, le_lo%llim_proc:le_lo%ulim_alloc)); call alloc8(c3=gvxi,v="gvxi")
       gvxi = 0.0
       allocate (gpq(nvpe, nvpa, le_lo%llim_proc:le_lo%ulim_alloc)); call alloc8(c3=gpq,v="gpq")
       gpq = 0.0
       allocate (gvxiavg(nlambda, nesub, -ntgrid:ntgrid, nspec)); call alloc8(r4=gvxiavg,v="gvxiavg")
       gvxiavg = 0.0
       allocate (gpqavg(nvpe, nvpa, -ntgrid:ntgrid, nspec)); call alloc8(r4=gpqavg,v="gpqavg")
       gpqavg = 0.0
       allocate (gvxi0(nlambda, nesub, nspec)); call alloc8(r3=gvxi0,v="gvxi0")
       gvxi0 = 0.0
       allocate (gpq0(nvpe, nvpa, nspec)); call alloc8(r3=gpq0,v="gpq0")
       gpq0 = 0.0

       ! change layout so that v and lambda are on processor
       call redist_g2le (gnew, gle)

       ! carry out 2d transform to get spectral coefficients in (v,xi) and (vpa,vpe)
       do ile = le_lo%llim_proc, le_lo%ulim_proc
          call vtransform2d (gle(:nxi,:negrid,ile), gvxi(:,:,ile), gpq(:,:,ile)) 
       end do

       ! average mod**2 of transformed g in plane perpendicular to equilibrium B-field
       call get_vol_average_vspec (real(conjg(gvxi)*gvxi), gvxiavg)
       call get_vol_average_vspec (real(conjg(gpq)*gpq), gpqavg)

       if (proc0) then
          ! now average along field line
          do is = 1, nspec
             do ie = 1, nesub
                do il = 1, nlambda
                   call get_fldline_average (gvxiavg(il,ie,:,is),gvxi0(il,ie,is))
                end do
             end do
             do iq = 1, nvpa
                do ip = 1, nvpe
                   call get_fldline_average (gpqavg(ip,iq,:,is),gpq0(ip,iq,is))
                end do
             end do
          end do
       end if
       
       do ie = 1, nesub
          do il = 1, nlambda
             call broadcast (gvxi0(il,ie,:))
          end do
       end do
       do iq = 1, nvpa
          do ip = 1, nvpe
             call broadcast (gpq0(ip,iq,:))
          end do
       end do

       do is = 1, nspec
          normfac1 = sum(gvxi0(:,:,is))
          normfac2 = sum(gpq0(:,:,is))
          if (proc0 .and. write_ascii) then
             do ie = 1, nesub
                do il = 1, nlambda
                   write (vspec_unit,*) 'gvxi0', time, is, ie-1, il-1, gvxi0(il,ie,is)/normfac1, gvxi0(il,ie,is)
                end do
             end do
             write (vspec_unit,*)
             do iq = 1, nvpa
                do ip = 1, nvpe
                   write (vspec_unit,*) 'gpq0', time, is, iq-1, real(ip-1)*delp, gpq0(ip,iq,is)/normfac2, gpq0(ip,iq,is)
                end do
             end do
             write (vspec_unit,*)
          end if
       end do
       if (proc0 .and. write_ascii) write (vspec_unit,*)
       
       call nc_loop_vspec (nout, gvxi0/normfac1, gpq0/normfac2)

!       deallocate (gle, gvxi, gpq, gvxiavg, gpqavg, gvxi0, gpq0)
       call dealloc8(c3=gle,v="gle")
       call dealloc8(c3=gvxi,v="gvxi")
       call dealloc8(c3=gpq,v="gpq")
       call dealloc8(r4=gvxiavg,v="gvxiavg")
       call dealloc8(r4=gpqavg,v="gpqavg")
       call dealloc8(r3=gvxi0,v="gvxi0")
       call dealloc8(r3=gpq0,v="gpq0")
    end if

    if (write_kspectrum .and. write_Epolar) then

       allocate (h2kk(-ntgrid:ntgrid,nkpolar,nspec)) ; call alloc8 (r3=h2kk, v="h2kk")
       
       ! convert from g(z,sign,iglo) to int d3v | h(kz,kperp,spec,vpa,vpe) |**2
       ! RN> this requires write_Epolar
       call get_kspectrum (gnew, h2kk)

!!$       !RN> kpar is now defined in theta_grid
!!$       ! get kpar values
!!$       do ig = 1, ntgrid
!!$          kpar(ig) = (ig-1)*gradpar/real(2*nperiod-1)
!!$          kpar(2*ntgrid-ig+1)=-(ig)*gradpar/real(2*nperiod-1)
!!$       end do
       
       ! only writing out first species for now
       if (proc0) then
          do iavg = 1, nkpolar
             do ig = ntgrid+1,2*ntgrid
                write (kspec_unit, "(3es13.5,e14.5e3)") &
                     time, kpar(ig), kpavg(iavg), h2kk(ig-ntgrid-1,iavg,1)
             end do
             do ig = 1, ntgrid
                write (kspec_unit, "(3es13.5,e14.5e3)") &
                     time, kpar(ig), kpavg(iavg), h2kk(ig-ntgrid-1,iavg,1)
             end do
             write (kspec_unit, "()")
          end do
       end if

       call dealloc8(r3=h2kk, v="h2kk")

    end if
    
    if (write_verr) then
      if(debug.and.proc0) write(*,*) "Loop diagnostics: start write_verr." !DEBUG-KDN-110718

       allocate(errest(4,2)); call alloc8(r2=errest,v="errest")
       allocate(erridx(4,3)); call alloc8(i2=erridx,v="erridx")
       errest = 0.0; erridx = 0

       ! error estimate obtained by comparing standard integral with less-accurate integral
       call get_verr (errest, erridx, phinew, bparnew)

       if (proc0 .and. write_ascii) &
            write(res_unit,"(5(1x,e16.8))") time, errest(:,2)
!            write (res_unit,"(5(1x,e16.8),8(1x,i8))") time, errest(:,2), erridx(1,2:3)
!       deallocate (errest,erridx)
       call dealloc8(r2=errest,v="errest")
       call dealloc8(i2=erridx,v="erridx")
      if(debug.and.proc0) write(*,*) "Loop diagnostics: finished write_verr." !DEBUG-KDN-110718
    end if
!<MAB

    if (proc0) then
       omega = omegahist(mod(istep,navg),:,:)
       sourcefac = exp((-zi*omega0+gamma0)*time)
       call phinorm (phitot)
       if (use_Phi)  call get_vol_average (phinew, phinew, phi2, phi2_by_mode)
       if (use_Apar) call get_vol_average (aparnew, aparnew, apar2, apar2_by_mode)
       if (use_Bpar) call get_vol_average (bparnew, bparnew, bpar2, bpar2_by_mode)
    end if

    if (write_any_fluxes) then
      if(debug.and.proc0) write(*,*) "Loop diagnostics: start writing fluxes." !DEBUG-KDN-110718
       call flux (phinew, aparnew, bparnew, &
             pflux,  qheat,  vflux, &
            pmflux, qmheat, vmflux, &
            pbflux, qbheat, vbflux)
       if (proc0) then
          if (use_Phi) then
             do is = 1, nspec
                qheat(:,:,is,1) = qheat(:,:,is,1)*spec(is)%dens*spec(is)%temp
                call get_volume_average (qheat(:,:,is,1), heat_fluxes(is))
                
                qheat(:,:,is,2) = qheat(:,:,is,2)*spec(is)%dens*spec(is)%temp
                call get_volume_average (qheat(:,:,is,2), heat_par(is))

                qheat(:,:,is,3) = qheat(:,:,is,3)*spec(is)%dens*spec(is)%temp
                call get_volume_average (qheat(:,:,is,3), heat_perp(is))
                
                pflux(:,:,is) = pflux(:,:,is)*spec(is)%dens
                call get_volume_average (pflux(:,:,is), part_fluxes(is))

                vflux(:,:,is) = vflux(:,:,is)*spec(is)%dens*spec(is)%mass*spec(is)%stm
                call get_volume_average (vflux(:,:,is), mom_fluxes(is))

             end do
          end if
          if (use_Apar) then
             do is = 1, nspec
                qmheat(:,:,is,1)=qmheat(:,:,is,1)*spec(is)%dens*spec(is)%temp
                call get_volume_average (qmheat(:,:,is,1), mheat_fluxes(is))

                qmheat(:,:,is,2)=qmheat(:,:,is,2)*spec(is)%dens*spec(is)%temp
                call get_volume_average (qmheat(:,:,is,2), mheat_par(is))

                qmheat(:,:,is,3)=qmheat(:,:,is,3)*spec(is)%dens*spec(is)%temp
                call get_volume_average (qmheat(:,:,is,3), mheat_perp(is))
                
                pmflux(:,:,is)=pmflux(:,:,is)*spec(is)%dens
                call get_volume_average (pmflux(:,:,is), mpart_fluxes(is))

                vmflux(:,:,is)=vmflux(:,:,is)*spec(is)%dens*spec(is)%mass*spec(is)%stm
                call get_volume_average (vmflux(:,:,is), mmom_fluxes(is))
             end do
          end if
          if (use_Bpar) then
             do is = 1, nspec
                qbheat(:,:,is,1)=qbheat(:,:,is,1)*spec(is)%dens*spec(is)%temp
                call get_volume_average (qbheat(:,:,is,1), bheat_fluxes(is))

                qbheat(:,:,is,2)=qbheat(:,:,is,2)*spec(is)%dens*spec(is)%temp
                call get_volume_average (qbheat(:,:,is,2), bheat_par(is))

                qbheat(:,:,is,3)=qbheat(:,:,is,3)*spec(is)%dens*spec(is)%temp
                call get_volume_average (qbheat(:,:,is,3), bheat_perp(is))
                
                pbflux(:,:,is)=pbflux(:,:,is)*spec(is)%dens
                call get_volume_average (pbflux(:,:,is), bpart_fluxes(is))

                vbflux(:,:,is)=vbflux(:,:,is)*spec(is)%dens*spec(is)%mass*spec(is)%stm
                call get_volume_average (vbflux(:,:,is), bmom_fluxes(is))
             end do
          end if
       end if
      if(debug.and.proc0) write(*,*) "Loop diagnostics: finished write any fluxes." !DEBUG-KDN-110718
    end if

    fluxfac = 0.5
    fluxfac(1) = 1.0

    if (proc0) then
       if (print_flux_line) then
          if (use_Phi) then
             write (unit=*, fmt="('t= ',e17.10,' < phi**2>= ',e11.4, &
                  & ' heat fluxes: ', 5(1x,e11.4))") time, phi2, heat_fluxes(1:min(nspec,5))
          end if
          if (use_Apar) then
             write (unit=*, fmt="('t= ',e17.10,' <apar**2>= ',e11.4, &
                  & ' heat flux m: ', 5(1x,e11.4))") time, apar2, mheat_fluxes(1:min(nspec,5))
          end if
          if (use_Bpar) then
             write (unit=*, fmt="('t= ',e17.10,' <bpar**2>= ',e11.4, &
                  & ' heat flux b: ', 5(1x,e11.4))") time, bpar2, bheat_fluxes(1:min(nspec,5))
          end if
       end if
       if (print_line) then
          do ik = 1, naky
             do it = 1, nakx
                write (unit=*, fmt="('ky= ',f8.4, ' kx= ',f8.4, &
                     &' om=',2es15.5,' omav=', 2es15.5,' phtot=',es10.3)") &
                     aky(ik), akx(it), &
                     real(omega(it,ik)),    aimag(omega(it,ik)), &
                     real(omegaavg(it,ik)), aimag(omegaavg(it,ik)), &
                     phitot(it,ik)
             end do
          end do
          write (*,*) 
       end if
    end if


!NOTE: THIS IS MOVED TO AFTER POLAR SPECTRUM CALCULATION
    !Call routine to determine adaptive hypercollisionality
    !NOTE: This must have write_hrate=T and come after call to heating
!    if (adapt_hc_any .and. write_hrate .and. proc0) call get_adapt_hc(nuh_changed)
!    call broadcast(nuh_changed)
!    if (nuh_changed) then 
!       call broadcast(spec(1:nspec)%nu_h)
!       !Update arrays in collisions.f90 if nuh_changed=.true.
!       call update_vnewh
!    endif
 
! Polar spectrum calculation----------------------------------------------
! BD:--- must have write_hrate = T for Epolar to work b/c hk array is used
    if (write_Epolar .and. write_hrate .and. proc0) then
      if (debug.and.proc0) write(*,*) "Loop diagnostics: Polar spectrum start." !DEBUG-KDN-110718
       ebinarray(:,:)=0.

       !Calculate polar spectrum of energies
       call get_polar_spectrum (hk(:,:,lenergy), ebinarray(:,iefluc))
       call get_polar_spectrum (hk(:,:,leapar),  ebinarray(:,ieapar))
       call get_polar_spectrum (hk(:,:,lebpar),  ebinarray(:,iebpar))
       
       do is=1,nspec
          call get_polar_spectrum(hk(:,:,lphis2(is)), ebinarray(:,iephis2  + (is-1)*3))
          call get_polar_spectrum(hk(:,:,lhs2(is)), ebinarray(:,iehs2    + (is-1)*3))
          call get_polar_spectrum(hk(:,:,ldelfs2(is)), ebinarray(:,iedelfs2 + (is-1)*3))
       enddo
       
       !Output raw kspectrum to file
       if (nspec == 1) then
          do i=1,nbx
             write (unit=polar_raw_unit, fmt="('t= ',e17.10,' kperp= ',e11.4, &
                  &' E= '     ,e12.4e3,' Eapar= ',e12.4e3,' Ebpar= ',e12.4e3, &
                  &' Ephis2= ',e12.4e3,' Ehss2= ',e12.4e3,' Edfs2= ',e12.4e3)") &
                  & time, kpbin(i),ebinarray(i,1:6)
          end do
       else  !Only writing this data for first two species for now
          ! Labels assume first species is ion species.
          do i=1,nbx
             write (unit=polar_raw_unit, fmt="('t= ',e17.10,' kperp= ',e11.4, &
                  &' E= '     ,e12.4e3,' Eapar= ',e12.4e3,' Ebpar= ',e12.4e3, &
                  &' Ephii2= ',e12.4e3,' Ehsi2= ',e12.4e3,' Edfi2= ',e12.4e3, &
                  &' Ephie2= ',e12.4e3,' Ehse2= ',e12.4e3,' Edfe2= ',e12.4e3)") &
                  & time, kpbin(i),ebinarray(i,1:9)
          end do
       end if
       write (unit=polar_raw_unit, fmt='(a)') ''      
       
       !Compute log-averaged polar spectrum
       
       do ifield = 1, 3+nspec*3
          eavgarray(:,ifield)=0.
          
          do i = 1,nbx
             j=polar_avg_index(i)
             eavgarray(j,ifield)=eavgarray(j,ifield)+log(ebinarray(i,ifield) + &
                  epsilon(0.0))
          enddo
          ! Finish log-averaging
          do j=1,nkpolar
             eavgarray(j,ifield)=eavgarray(j,ifield)/numavg(j)
          enddo
          eavgarray(:,ifield)=exp(eavgarray(:,ifield))
       end do
       
       ! Output log-averaged kspectrum to file
       if (nspec == 1) then
          do i=1,nkpolar
             write (unit=polar_avg_unit, fmt="('t= ',e17.10,' kperp= ',e11.4, &
                  &' E= '     ,e12.4e3,' Eapar= ',e12.4e3,' Ebpar= ',e12.4e3, &
                  &' Ephis2= ',e12.4e3,' Ehss2= ',e12.4e3,' Edfs2= ',e12.4e3)") &
                  & time, kpavg(i),eavgarray(i,1:6)
          end do
       else ! Only writing this data for first two species right now
          ! Labels assume first species is ion species.
          do i=1,nkpolar
             write (unit=polar_avg_unit, fmt="('t= ',e17.10,' kperp= ',e11.4, &
                  &' E= '     ,e12.4e3,' Eapar= ',e12.4e3,' Ebpar= ',e12.4e3, &
                  &' Ephii2= ',e12.4e3,' Ehsi2= ',e12.4e3,' Edfi2= ',e12.4e3, &
                  &' Ephie2= ',e12.4e3,' Ehse2= ',e12.4e3,' Edfe2= ',e12.4e3)") &
                  & time, kpavg(i),eavgarray(i,1:9)
          end do
       end if
       write (unit=polar_avg_unit, fmt='(a)') ''      
       
       !FLUSH OUTPUT FILES
       call flush_output_file (polar_raw_unit)
       call flush_output_file (polar_avg_unit)
       
       if (debug.and.proc0) write(*,*) "Loop diagnostics: Polar spectrum done." !DEBUG-KDN-110715
    end if! END Polar spectrum calculation------------------------------------

! Elsasser Variable spectrum calculation--------------------------------------
    if (write_elsasser .and. write_Epolar .and. proc0) then
      if (debug.and.proc0) write(*,*) "Loop diagnostics: Elsasser start." !DEBUG-KDN-110718
       !Calculate weights along z
       wgt = delthet*jacob
       wgt = wgt/sum(wgt)
       !Calculate the Elsasser energies for each (kx,ky) mode
       !Loop over all modes and sum Elsasser energies
       elzk(:,:,:)=0.
       do ik=1,naky
          do it = 1,nakx
             !Sum over all z gridpoints
             do ig=-ntgrid, ntgrid
                elzk(it,ik,lelzp)= elzk(it,ik,lelzp) +  &
                     (abs(phinew(ig,it,ik) - aparnew(ig,it,ik)/(sqrt(beta)*spec(1)%dens*spec(1)%mass)))**2. * &
                     0.25*kperp2(it,ik)*wgt(ig)
                elzk(it,ik,lelzm)= elzk(it,ik,lelzm) +  &
                     (abs(phinew(ig,it,ik) + aparnew(ig,it,ik)/(sqrt(beta)*spec(1)%dens*spec(1)%mass)))**2. * &
                     0.25*kperp2(it,ik)*wgt(ig)
                elzk(it,ik,lelzp2)= elzk(it,ik,lelzp2) +  &
                     (abs(phinew(ig,it,ik)*sqrt(1.+kperp2(it,ik)/(sqrt(beta))) -  &
                     aparnew(ig,it,ik)/(sqrt(beta)*spec(1)%dens*spec(1)%mass)))**2. * &
                     0.25*kperp2(it,ik)*wgt(ig)
                elzk(it,ik,lelzm2)= elzk(it,ik,lelzm2) +  &
                     (abs(phinew(ig,it,ik)*sqrt(1.+kperp2(it,ik)/(sqrt(beta))) +  &
                     aparnew(ig,it,ik)/(sqrt(beta)*spec(1)%dens*spec(1)%mass)))**2. * &
                     0.25*kperp2(it,ik)*wgt(ig)
             enddo
          enddo
       enddo
       
       !Calculate the polar spectra of Elsasser energies
       elzraw(:,:)=0.
       elzavg(:,:)=0.

       !RAW SPECTRA~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
       !Calculate polar spectrum of energies 
       call get_polar_spectrum (elzk(:,:,lelzp),  elzraw(:,lelzp))
       call get_polar_spectrum (elzk(:,:,lelzm),  elzraw(:,lelzm))
       call get_polar_spectrum (hk(:,:,leapar),  ebinarray(:,ieapar))

       !Output raw Elsasser spectra to file
       do i=1,nbx
          write (unit=elz_raw_unit, fmt="('t= ',e17.10,' kperp= ',e11.4, &
               &' Elzp= '     ,e12.4e3,' Elzm= ',e12.4e3, &
               &' Elzp2= '     ,e12.4e3,' Elzm2= ',e12.4e3)") &
               & time, kpbin(i),elzraw(i,1:lelzmax)
       end do
       write (unit=elz_raw_unit, fmt='(a)') ''      

       !AVG SPECTRA~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
       !Compute log-averaged Elsasser spectra
       do ifield = 1, lelzmax
          elzavg(:,ifield)=0.
          do i = 1,nbx
             j=polar_avg_index(i)
             elzavg(j,ifield)=elzavg(j,ifield)+log(elzraw(i,ifield))
          enddo
          ! Finish log-averaging
          do j=1,nkpolar
             elzavg(j,ifield)=elzavg(j,ifield)/numavg(j)
          enddo
          elzavg(:,ifield)=exp(elzavg(:,ifield))
       end do

       ! Output log-averaged  Elsasser spectra to file
       do i=1,nkpolar
          write (unit=elz_avg_unit, fmt="('t= ',e17.10,' kperp= ',e11.4, &
               &' Elzp= '     ,e12.4e3,' Elzm= ',e12.4e3,&
               &' Elzp2= '     ,e12.4e3,' Elzm2= ',e12.4e3)") &
               & time, kpavg(i),elzavg(i,1:lelzmax)
       end do
       write (unit=elz_avg_unit, fmt='(a)') ''      
       
       !FLUSH OUTPUT FILES
       call flush_output_file (elz_raw_unit)
       call flush_output_file (elz_avg_unit)
      if (debug.and.proc0) write(*,*) "Loop diagnostics: Elsasser done." !DEBUG-KDN-110715
    endif
! END Elsasser Variable spectrum calculation----------------------------------
    
    !----------------------------------------------------!
    ! Go back to the main routine if you don't write out !
    !----------------------------------------------------!
    if (.not. (write_any .or. dump_any)) then
       !Deallocate variables for density and velocity perturbations
       if (write_density_velocity) then
          call del_dvtype (dv)
          call del_dvtype (dvk)
          deallocate(dvk) !!! RN> unable to account memory for structure variables
       endif
       ! Deallocate variable for Jexternal
!       if (write_jext) deallocate(j_ext)
       if (write_jext) call dealloc8(r2=j_ext,v="j_ext")
       return
    end if
    !----------------------------------------------------!
    ! Go back to the main routine if you don't write out !
    !----------------------------------------------------!

    if (proc0 .and. write_any) then
       if (write_ascii) write (unit=out_unit, fmt=*) 'time=', time
!!!----------------------------------------------------------------
! BEGIN: Write out total heating diagnostics
!!!----------------------------------------------------------------
       if (write_ascii .and. write_hrate) then
         if (debug.and.proc0) write(*,*) "Loop diagnostics: total heating start." !DEBUG-KDN-110718
!LEGEND------------------------------------------------------------
! TWO SPECIES~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! Column   Item
!   1     time
! ENERGIES
!   2     Energy [lenergy]
!   3     (k_perp A)**2 or in the ES case 2D invariant gamtot*|phi|^2/2 [leapar]
!   4     B_par**2 [lebpar]
!   5     df**2   (1) [ldelfs2(1)]
!   6     df**2   (2) [ldelfs2(2)]
!   7     h**2    (1) [lhs2(1)]
!   8     h**2    (2) [lhs2(2)]
!   9     n*T*(q Phi/T)**2   (1) [lphis2(1)]
!  10     n*T*(q Phi/T)**2   (2) [lphis2(2)]
! BALANCES
!  11     Power Balance [lpowbal]
!  12     Collisional Power Balance [lpowbalcoll]
!  13     Entropy Balance   (1) [lentbal(1)]
!  14     Entropy Balance   (2) [lentbal(1)]
! HEATING
!  15     J_ant.E [lantenna]
!  16     dEnergy/dt [lenergy_dot]
!  17     [h(i+1)*h^*]/2*C[h_(i+1)]*T_0 (1) [limp_colls(1)]
!  18     [h(i+1)*h^*]/2*C[h_(i+1)]*T_0 (2) [limp_colls(2)]
!  19     [h H(h)]    (1) [lhypercoll(1)]
!  20     [h H(h)]    (2) [lhypercoll(2)]
!  21     [h C(h)]    (1) [lcollisions(1)]
!  22     [h C(h)]    (2) [lcollisions(2)]
!  23     [chi dh/dt] (1) [lheating(1)]
!  24     [chi dh/dt] (2) [lheating(2)]
!  25     [h w_* h]   (1) [lgradients(1)]
!  26     [h w_* h]   (2) [lgradients(2)]
!!! RN>
!  27     m*n*upar**2    (1) [lupar2(1)]
!  28     m*n*upar**2    (2) [lupar2(2)]
!  29     m*n*uperp**2   (1) [luperp2(1)]
!  30     m*n*uperp**2   (2) [luperp2(2)]
!  31     (k_perp A_pt)**2 [leapar_pt]
!  32     B_par_pt**2      [lebpar_pt]
!  33     df_pt**2 (1)     [ldelfs2_pt(1)]
!  34     df_pt**2 (2)     [ldelfs2_pt(2)]
!  35     h_pt**2 (1)      [lhs2_pt(1)]
!  36     h_pt**2 (2)      [lhs2_pt(2)]
!  37     n0*T0*(q Phi_pt/T0)**2 (1) [lphis2_pt(1)]
!  38     n0*T0*(q Phi_pt/T0)**2 (2) [lphis2_pt(2)]
!  39     m*n0*upar_pt**2     [lupar2_pt(1)]
!  40     m*n0*upar_pt**2     [lupar2_pt(2)]
!  41     m*n0*uperp_pt**2    [luperp2_pt(1)]
!  42     m*n0*uperp_pt**2    [luperp2_pt(2)]
!  43     n0*T0*n*Txx        [ltxx(1)]
!  44     n0*T0*n*Txx        [ltxx(2)]
!  45     n0*T0*n*Tyy        [ltyy(1)]
!  46     n0*T0*n*Tyy        [ltyy(2)]
!  47     n0*T0*n*Tzz        [ltzz(1)]
!  48     n0*T0*n*Tzz        [ltzz(2)]
!  49     n0*T0*n*Txx_pt     [ltxx_pt(1)]
!  50     n0*T0*n*Txx_pt     [ltxx_pt(2)]
!  51     n0*T0*n*Tyy_pt     [ltyy_pt(1)]
!  52     n0*T0*n*Tyy_pt     [ltyy_pt(2)]
!  53     n0*T0*n*Tzz_pt     [ltzz_pt(1)]
!  54     n0*T0*n*Tzz_pt     [ltzz_pt(2)]
!  55     m*n0*|k_perp*phi|^2 [lkphi2(1)]
!  56     m*n0*|k_perp*phi|^2 [lkphi2(2)]
!  57     m*n0*|k_perp*phi_pt|^2 [lkphi2_pt(1)]
!  58     m*n0*|k_perp*phi_pt|^2 [lkphi2_pt(2)]
!  59     n0*T0*(q phi/T0)^2 (1-gamma0) [lg0phi2(1)]
!  60     n0*T0*(q phi/T0)^2 (1-gamma0) [lg0phi2(2)]
!  61     n0*T0*(q phi_pt/T0)^2 (1-gamma0) [lg0phi2_pt(1)]
!  62     n0*T0*(q phi_pt/T0)^2 (1-gamma0) [lg0phi2_pt(2)]
!  63     int coll dt        [ldiss1] ! total dissipation
!  64     W(t)-W(0)          [ldiss2] ! total dissipation
! ONE SPECIES~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! Column   Item
!   1     time
! ENERGIES
!   2     Energy [lenergy]
!   3     (k_perp A)**2 or in the ES case 2D invariant gamtot*|phi|^2/2 [leapar]
!   4     B_par**2 [lebpar]
!   5     df**2   (1) [ldelfs2]
!   6     h**2    (1) [lhs2]
!   7     n*T*(q Phi/T)**2   (1) [lphis2]
!! BALANCES
!   8    Power Balance [lpowbal]
!   9     Collisional Power Balance [lpowbalcoll]
!  10     Entropy Balance   (1) [lentbal]
! HEATING
!  11     J_ant.E [lantenna]
!  12     dEnergy/dt [lenergy_dot]
!  13     [h(i+1)*h^*]/2*C[h_(i+1)]*T_0 (1) [limp_colls]
!  14     [h H(h)]    (1) [lhypercoll]
!  15     [h C(h)]    (1) [lcollisions]
!  16     [chi dh/dt] (1) [lheating]
!  17     [h w_* h]   (1) [lgradients]
! TT: Added lwg1
!  18     int g^2 dv dR (no 1/F0 factor)
! <TT
!END LEGEND--------------------------------------------------------

          !ONLY PRINT OUT THIS FORMAT if (nspec .eq. 2)
          if (nspec .eq. 2) then
             write(unit=heat_unit, fmt="(es16.8,64es12.4)")time, &
                  h(lenergy),h(leapar),h(lebpar),h(ldelfs2(1):ldelfs2(2)), &
                  h(lhs2(1):lhs2(2)),h(lphis2(1):lphis2(2)), &
                  h(lpowbal),h(lpowbalcoll),h(lentbal(1):lentbal(2)), &
                  h(lantenna),h(lenergy_dot),h(limp_colls(1):limp_colls(2)), &
                  h(lhypercoll(1):lhypercoll(2)),h(lcollisions(1):lcollisions(2)), &
                  h(lheating(1):lheating(2)),h(lgradients(1):lgradients(2)), &
                  h(lupar2(1):lupar2(2)),h(luperp2(1):luperp2(2)), &
                  h(leapar_pt), h(lebpar_pt), &
                  h(ldelfs2_pt(1):ldelfs2_pt(2)), &
                  h(lhs2_pt(1):lhs2_pt(2)), &
                  h(lphis2_pt(1):lphis2_pt(2)), &
                  h(lupar2_pt(1):lupar2_pt(2)), &
                  h(luperp2_pt(1):luperp2_pt(2)), &
                  h(ltxx(1):ltxx(2)), &
                  h(ltyy(1):ltyy(2)), &
                  h(ltzz(1):ltzz(2)), &
                  h(ltxx_pt(1):ltxx_pt(2)), &
                  h(ltyy_pt(1):ltyy_pt(2)), &
                  h(ltzz_pt(1):ltzz_pt(2)), &
                  h(lkphi2(1):lkphi2(2)), &
                  h(lkphi2_pt(1):lkphi2_pt(2)), &
                  h(lg0phi2(1):lg0phi2(2)), &
                  h(lg0phi2_pt(1):lg0phi2_pt(2)), &
                  h(ldiss1),h(ldiss2)
             call flush_output_file (heat_unit)
          else
          !Otherwise, just print out the heat file for each species separately
             do is=1,nspec
                write(unit=heat_unit2(is), fmt="(es16.8,17es12.4)")time, &
                     h(lenergy),h(leapar),h(lebpar),h(ldelfs2(is)), &
                     h(lhs2(is)),h(lphis2(is)), &
                     h(lpowbal),h(lpowbalcoll),h(lentbal(is)), &
                     h(lantenna),h(lenergy_dot),h(limp_colls(is)), &
                     h(lhypercoll(is)),h(lcollisions(is)), &
                     h(lheating(is)),h(lgradients(is)), &
                     h(lwg1) ! Added on 10/12/10 TT
                call flush_output_file (heat_unit2(is))
             end do
          end if
         if (debug.and.proc0) write(*,*) "Loop diagnostics: total heating computed." !DEBUG-KDN-110718
       end if
!------------------------------------------------------------------
! END: Write out total heating diagnostics
!------------------------------------------------------------------

!<GGH
       !Write out data for density and velocity peturbations
       if (write_ascii .and. write_density_velocity) then
          !NOTE: This only works for 2 species
          write (unit=dv_unit, fmt="('t= ',e13.6,' dvpar= ',e13.6,' ', &
               & e13.6,' dvperp= ',e13.6,' ',e13.6,' dn= ',e13.6,' ',e13.6)")  &
               time, dv % dvpar(:), dv % dvperp(:), dv % dn(:)
!          write (unit=dv_unit, fmt="('t= ',e13.6,' dvperp= ',e13.6)") time, dv % dvperp
!          write (unit=dv_unit, fmt="('t= ',e13.6,' dn= ',e13.6)") time, dv % dn
!          write (unit=heat_unit, fmt='(a)') ''
       end if
       !Write out data for j_external
       if (write_ascii .and. write_jext) then
          do ik=1,naky
             do it = 1, nakx
                if (j_ext(ik,it) .ne. 0.) then
                   write (unit=jext_unit, fmt="(es12.4,i4,i4,es12.4)")  &
                        time,it,ik,j_ext(ik,it)
                endif
             enddo
          enddo
       end if
!>GGH
       
       if (use_Apar .and. write_lorentzian .and. write_ascii) then
          wtmp_new = antenna_w()
          if (real(wtmp_old) /= 0. .and. wtmp_new /= wtmp_old) &
               write (unit=out_unit, fmt="('w= ',e17.10, &
               &  ' amp= ',e17.10)") real(wtmp_new), sqrt(2.*apar2)
          wtmp_old = wtmp_new                
       end if

       if (write_nonlin) then
          call nc_loop (nout, time, fluxfac, &
               phinew(igomega,:,:), phi2, phi2_by_mode, &
               aparnew(igomega,:,:), apar2, apar2_by_mode, &
               bparnew(igomega,:,:), bpar2, bpar2_by_mode, &
               h, hk, omega, omegaavg, phitot, write_omega, write_hrate)
          if (write_nl_flux) then
             hflux_tot = 0.
             zflux_tot = 0.
             if (use_Phi) then
                if (write_ascii) then
                   write (unit=out_unit, fmt="('t= ',e17.10,' <phi**2>= ',e11.4, &
                        & ' heat fluxes: ', 5(1x,e11.4))") &
                        time, phi2, heat_fluxes(1:min(nspec,5))
                   write (unit=out_unit, fmt="('t= ',e17.10,' <phi**2>= ',e11.4, &
                        & ' part fluxes: ', 5(1x,e11.4))") &
                        time, phi2, part_fluxes(1:min(nspec,5))
                end if
                hflux_tot = sum(heat_fluxes)
                vflux_tot = sum(mom_fluxes)
                zflux_tot = sum(part_fluxes*spec%z)
             end if
             if (use_Apar) then
                if (write_ascii) then
                   write (unit=out_unit, fmt="('t= ',e17.10,' <apar**2>= ',e11.4, &
                        & ' heat mluxes: ', 5(1x,e11.4))") &
                        time, apar2, mheat_fluxes(1:min(nspec,5))
                   write (unit=out_unit, fmt="('t= ',e17.10,' <apar**2>= ',e11.4, &
                        & ' part mluxes: ', 5(1x,e11.4))") &
                        time, apar2, mpart_fluxes(1:min(nspec,5))
                end if
                hflux_tot = hflux_tot + sum(mheat_fluxes)
                vflux_tot = vflux_tot + sum(mmom_fluxes)
                zflux_tot = zflux_tot + sum(mpart_fluxes*spec%z)
             end if
             if (use_Bpar) then
                if (write_ascii) then
                   write (unit=out_unit, fmt="('t= ',e17.10,' <bpar**2>= ',e11.4, &
                        & ' heat bluxes: ', 5(1x,e11.4))") &
                        time, bpar2, bheat_fluxes(1:min(nspec,5))
                   write (unit=out_unit, fmt="('t= ',e17.10,' <bpar**2>= ',e11.4, &
                        & ' part bluxes: ', 5(1x,e11.4))") &
                        time, bpar2, bpart_fluxes(1:min(nspec,5))
                end if
                hflux_tot = hflux_tot + sum(bheat_fluxes)
                vflux_tot = vflux_tot + sum(bmom_fluxes)
                zflux_tot = zflux_tot + sum(bpart_fluxes*spec%z)
             end if
             if (write_ascii) &
                  write (unit=out_unit, fmt="('t= ',e17.10,' h_tot= ',e11.4, &
                  & ' z_tot= ',e11.4)") time, hflux_tot, zflux_tot
             call nc_qflux (nout, qheat(:,:,:,1), qmheat(:,:,:,1), qbheat(:,:,:,1), &
                  heat_par, mheat_par, bheat_par, &
                  heat_perp, mheat_perp, bheat_perp, &
                  heat_fluxes, mheat_fluxes, bheat_fluxes, hflux_tot)
             call nc_vflux (nout, vflux, vmflux, vbflux, &
                  mom_fluxes, mmom_fluxes, bmom_fluxes, vflux_tot)
             call nc_pflux (nout, pflux, pmflux, pbflux, &
                  part_fluxes, mpart_fluxes, bpart_fluxes, zflux_tot)
          end if
       end if
       if (write_ascii) then
          do ik = 1, naky
             do it = 1, nakx
                if (write_linear) then
                   write (out_unit, "('t= ',e17.10,' aky= ',f6.2, ' akx= ',f6.2, &
                        &' om= ',2es15.5,' omav= ', 2es15.5,' phtot= ',es10.3)") &
                        time, aky(ik), akx(it), &
                        real( omega(it,ik)),    aimag(omega(it,ik)), &
                        real( omegaavg(it,ik)), aimag(omegaavg(it,ik)), &
                        phitot(it,ik)
                end if
                
                if (write_omega) write (out_unit, *) ' omega/(vt/a)=', &
                     real(omega(it,ik)), aimag(omega(it,ik))
                if (write_omavg) write (out_unit, *) ' omavg/(vt/a)=', &
                     real(omegaavg(it,ik)), aimag(omegaavg(it,ik))
             end do
          end do
       end if
      if(debug.and.proc0) write(*,*) "Loop diagnostics: finished writing total heating diagnostics." !DEBUG-KDN-110718
    end if

    if (write_avg_moments) then
      if(debug.and.proc0) write(*,*) "Loop diagnostics: start write_avg_moments." !DEBUG-KDN-110718
       call getmoms (ntot, density, upar, uperp, tpar, tperp)

       if (proc0) then
          ! summing ik==1 and *00 are arrays of size (nakx,nspec)
          ntot00    = sum (   ntot(:ntgrid-1,:,1,:), 1) * dz / z0 / 2.0/pi
          density00 = sum (density(:ntgrid-1,:,1,:), 1) * dz / z0 / 2.0/pi
          upar00    = sum (   upar(:ntgrid-1,:,1,:), 1) * dz / z0 / 2.0/pi
          tpar00    = sum (   tpar(:ntgrid-1,:,1,:), 1) * dz / z0 / 2.0/pi
          tperp00   = sum (  tperp(:ntgrid-1,:,1,:), 1) * dz / z0 / 2.0/pi

          phi00     = sum ( phinew(:ntgrid-1,:,1),   1) * dz / z0 / 2.0/pi

          do is = 1, nspec
             call get_vol_average (ntot(:,:,:,is), ntot(:,:,:,is), &
                  ntot2(is), ntot2_by_mode(:,:,is))
          end do

          do is = 1, nspec
             call get_vol_average (ntot(0,:,:,is), ntot(0,:,:,is), &
                  ntot20(is), ntot20_by_mode(:,:,is))
          end do

          call nc_loop_moments (nout, ntot2, ntot2_by_mode, ntot20, &
               ntot20_by_mode, phi00, ntot00, density00, upar00, &
               tpar00, tperp00)

       end if
      if(debug.and.proc0) write(*,*) "Loop diagnostics: finished write_avg_moments." !DEBUG-KDN-110718
    end if

    ! RN> output moments at particle coord. in x-y plane
    if (write_full_moments_r) then !RN
      if(debug.and.proc0) write(*,*) "Loop diagnostics: starting write_full_moments_r." !DEBUG-KDN-110718
       allocate(dens(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=dens,v="dens")
       allocate(  ux(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=ux,v="ux")
       allocate(  uy(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=uy,v="uy")
       allocate(  uz(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=uz,v="uz")
       allocate( pxx(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=pxx,v="pxx")
       allocate( pyy(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=pyy,v="pyy")
       allocate( pzz(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=pzz,v="pzz")
       allocate( pxy(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=pxy,v="pxy")
       allocate( pyz(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=pyz,v="pyz")
       allocate( pzx(-ntgrid:ntgrid,nakx,naky,nspec)); call alloc8(c4=pzx,v="pzx")
       dens(:,:,:,:)=cmplx(0.,0.);
         ux(:,:,:,:)=cmplx(0.,0.);  uy(:,:,:,:)=cmplx(0.,0.)
         uz(:,:,:,:)=cmplx(0.,0.);
        pxx(:,:,:,:)=cmplx(0.,0.); pyy(:,:,:,:)=cmplx(0.,0.) 
        pzz(:,:,:,:)=cmplx(0.,0.); pxy(:,:,:,:)=cmplx(0.,0.)
        pyz(:,:,:,:)=cmplx(0.,0.); pzx(:,:,:,:)=cmplx(0.,0.)
       call getmoms_r(dens=dens, ux=ux, uy=uy, uz=uz, &
            & pxx=pxx, pyy=pyy, pzz=pzz, pxy=pxy, pyz=pyz, pzx=pzx)
       if(proc0) then
          call nc_loop_fullmom(nout,time, &
               & dens(igomega,:,:,:), &
               & ux(igomega,:,:,:), uy(igomega,:,:,:), uz(igomega,:,:,:), &
               & pxx(igomega,:,:,:),pyy(igomega,:,:,:),pzz(igomega,:,:,:), &
               & pxy(igomega,:,:,:),pyz(igomega,:,:,:),pzx(igomega,:,:,:))
       endif
       call dealloc8(c4=dens,v="dens")
       call dealloc8(c4=ux,v="ux")
       call dealloc8(c4=uy,v="uy")
       call dealloc8(c4=uz,v="uz")
       call dealloc8(c4=pxx,v="pxx")
       call dealloc8(c4=pyy,v="pyy")
       call dealloc8(c4=pzz,v="pzz")
       call dealloc8(c4=pxy,v="pxy")
       call dealloc8(c4=pyz,v="pyz")
       call dealloc8(c4=pzx,v="pzx")
      if(debug.and.proc0) write(*,*) "Loop diagnostics: finished write_full_moments_r." !DEBUG-KDN-110718

    endif
    ! <RN

    !Call routine to determine adaptive hypercollisionality
    !NOTE: This must have write_hrate=T and come after call to heating
    if ((adapt_hc_any .or. write_adapt_hc)  .and. write_hrate .and. proc0) call get_adapt_hc(nuh_changed)
    call broadcast(nuh_changed)
    if (nuh_changed) then
       call broadcast(spec(1:nspec)%nu_h)
       !Update arrays in collisions.f90 if nuh_changed=.true.
       call update_vnewh
    endif

    nout = nout + 1
    if (write_ascii .and. mod(nout, 10) == 0 .and. proc0) &
         call flush_output_file (out_unit)

!>MAB
    if (write_ascii .and. mod(nout, 10) == 0 .and. proc0) then
       if (write_verr) call flush_output_file (res_unit)
       if (write_vspectrum) call flush_output_file (vspec_unit)
       if (write_kspectrum) call flush_output_file (kspec_unit)
    end if
!<MAB

!    if (write_hrate) then
!       call del_htype (h)
!       call del_htype (hk)
!       deallocate (hk)
!    end if
!>GGH
    !Deallocate variables for density and velocity perturbations
    if (write_density_velocity) then
       call del_dvtype (dv)
       call del_dvtype (dvk)
       deallocate(dvk) !!! RN> unable to account memory for structure variables
    endif
! Deallocate variable for Jexternal
!    if (write_jext) deallocate(j_ext)
    if (write_jext) call dealloc8(r2=j_ext,v="j_ext")
!<GGH

    if (write_gg .and. mod(istep,nmovie)==0) call hdf_write_dist (gnew)
    
    if (write_moi .and. proc0 .and. mod(istep,nwrite)==0) call loop_stream_flux_potentials

    if (debug.and.proc0) write(*,*) "Loop diagnostics: done." !DEBUG-KDN-110715

  end subroutine loop_diagnostics

  subroutine write_gkpp (ekp)

    use file_utils, only: open_output_file, flush_output_file, error_unit
    use mp, only: proc0
    use agk_time, only: time
    use le_grids, only: npgrid
    use kgrids, only: nkpolar
    use dist_fn_arrays, only: gp
    real, dimension (:,:,:), intent (in) :: ekp
    integer :: ip, iavg, ibin
    logical, save :: first=.true.
    integer, save :: unit

    if (first) then
       if (proc0) then
          if (write_Epolar) then
             call open_output_file (unit, '.gkpp')
             write (unit, '(a,2(10x,a),9x,a)') &
                  '#  time', 'p', 'kperp', 'E(kperp,p)'
          else
             write (error_unit(),*) &
                  'ERROR in write_gkpp: write_Epolar must be turned on'
          end if
       end if
       first = .false.
    end if

    if (.not. write_Epolar) return

    if (proc0) then
       do ip=1, npgrid
          ebinarray(:,1) = 0.0
          call get_polar_spectrum (ekp(:,:,ip), ebinarray(:,1))
          eavgarray(:,1) = 0.0
          do ibin=1, nbx
             iavg = polar_avg_index(ibin)
             eavgarray(iavg,1) = eavgarray(iavg,1) + log(ebinarray(ibin,1))
          end do
          eavgarray(:,1) = exp(eavgarray(:,1) / numavg(:))
          do iavg=1, nkpolar
             write (unit,'(3es13.5,es14.5e3)') &
                  time, gp(ip), kpavg(iavg), eavgarray(iavg,1)
          end do
          write (unit,*)
       end do
       write(unit,*)
       call flush_output_file (unit)
    end if

  end subroutine write_gkpp

  subroutine write_dvperp

    use mp, only: proc0
    use file_utils, only: open_output_file, flush_output_file, error_unit
    use kgrids, only: nakx, naky, nkpolar
    use species, only: nspec
    use agk_time, only: time
    use collisions, only: get_dvperp
    use dist_fn_arrays, only: gnew
    use dist_fn, only: g_adjust
    use fields_arrays, only: phinew, bparnew
    use run_parameters, only: fphi, fbpar
    logical, save :: first=.true.
    integer, save :: dv_unit
    integer :: ibin, is, iavg
    real, dimension (nakx, naky, nspec) :: dvperp
    real, dimension (nbx, nspec) :: dvperp_bin

    if (first) then
       if (proc0) then
          if (write_Epolar) then
             call open_output_file (dv_unit, '.dvperp')
             write (dv_unit, '(a,2(8x,a))') '#  time', 'kperp', 'dvperp'
          else
             write (error_unit(),*) &
                  'ERROR in write_dvperp: write_Epolar must be turned on'
          end if
       end if
       first = .false.
    end if

    if (.not. write_Epolar) return

    !!!RN>
    call g_adjust (gnew, phinew, bparnew, fphi, fbpar)
    call get_dvperp (dvperp)
    call g_adjust (gnew, phinew, bparnew, -fphi, -fbpar)

    if (proc0) then
       do is=1, nspec
          ! This is binned, but not averaged yet
          call get_polar_spectrum (dvperp(:,:,is), dvperp_bin(:,is))
       end do
       dvperp_bin = dvperp_bin / spread(kpbin,2,nspec)
!!$       do ibin=1, nbx
!!$          ! chosen is=1 to output
!!$          write (dv_unit,'(3es15.5)') time, kpbin(ibin), dvperp_bin(ibin,1)
!!$       end do

       ! average in each bin
       eavgarray(:,1) = 0.0
       do ibin=1, nbx
          iavg = polar_avg_index(ibin)
          eavgarray(iavg,1) = eavgarray(iavg,1) + log(dvperp_bin(ibin,1))
       end do
       eavgarray(:,1) = exp(eavgarray(:,1) / numavg(:))
       do iavg=1, nkpolar
          write (dv_unit,'(3es13.5)') time, kpavg(iavg), dvperp_bin(iavg,1)
       end do
       write (dv_unit,*)
       call flush_output_file (dv_unit)
    end if

  end subroutine write_dvperp


  subroutine get_ptransfer (ptrans)

    use constants, only: pi, zi
    use spfunc, only: j0
    use agk_layouts, only: g_lo, ik_idx, it_idx, is_idx, accelx_lo
    use agk_mem, only: alloc8, dealloc8
    use theta_grid, only: ntgrid
    use kgrids, only: nx, ny, nakx, naky, akx, aky
    use le_grids, only: npgrid, integrate_moment
    use species, only: nspec
    use dist_fn, only: Hankel_transform
    use dist_fn_arrays, only: gp, vperp2, gnew, aj0
    use fields_arrays, only: phinew
    use agk_transforms, only: transform2

    real, dimension (:,:), intent (out) :: ptrans

    integer :: iglo, it, ik, is, ipfrom, ipto, iac
    real, dimension (:,:), allocatable :: gtmp
    real, dimension (:,:,:), allocatable :: gto, pbr, vexbr
    real, dimension (nx,ny,nspec) :: mom
    complex, dimension (:,:), allocatable :: g0, vexb
    complex, dimension (:,:,:,:), allocatable :: gkp

    allocate (gkp(nakx,naky,npgrid,nspec)); call alloc8 (c4=gkp, v='gkp')

    ! second argument for ig=0
    ! last one to get gkp on all processors
!    call Hankel_transform (gnew, 0, gkp, 1)
    ! Hankel transform of \check{g} = g / sqrt{F_0}
    ! This factor is for entropy
    allocate (g0(2, g_lo%llim_proc:g_lo%ulim_alloc)); call alloc8 (c2=g0, v='g0_ptrans') ; g0 = (0.0, 0.0)
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       g0(:,iglo) = gnew(0,:,iglo) * exp(0.5*vperp2(iglo)) ! ig = 0
    end do
    call Hankel_transform (spread(g0,1,ntgrid*2+1), 0, gkp, 1)

    allocate (pbr(2, accelx_lo%llim_proc:accelx_lo%ulim_alloc, npgrid)); call alloc8 (r3=pbr, v='pbr_ptrans') ; pbr = 0.0

    allocate (gtmp(2, accelx_lo%llim_proc:accelx_lo%ulim_alloc)); call alloc8 (r2=gtmp, v='gtmp_ptrans') ; gtmp = 0.0

    ! Save vexbr
    allocate (vexb(2, g_lo%llim_proc:g_lo%ulim_alloc)); call alloc8 (c2=vexb, v='vexb_ptrans') ; vexb = (0.0, 0.0)
    allocate (vexbr(2, accelx_lo%llim_proc:accelx_lo%ulim_alloc, 2)); call alloc8 (r3=vexbr, v='vexbr_ptrans') ; vexbr = 0.0
    do iglo=g_lo%llim_proc, g_lo%ulim_proc
       it = it_idx(g_lo,iglo)
       ik = ik_idx(g_lo,iglo)
       vexb(:,iglo) = -zi * aky(ik) * aj0(iglo) * phinew(0,it,ik) ! ig=0
    end do
    call transform2 (vexb, vexbr(:,:,1), iac)
    do iglo=g_lo%llim_proc, g_lo%ulim_proc
       it = it_idx(g_lo,iglo)
       ik = ik_idx(g_lo,iglo)
       vexb(:,iglo) = zi * akx(it) * aj0(iglo) * phinew(0,it,ik) ! ig=0
    end do
    call transform2 (vexb, vexbr(:,:,2), iac)
    call dealloc8 (c2=vexb, v='vexb_ptrans')

    ! First npgrid loop for 'from' p-numbers
    do ipfrom=1, npgrid
       ! vx . grad g_Pf
       g0 = (0.0, 0.0)
       do iglo=g_lo%llim_proc, g_lo%ulim_proc
          it = it_idx(g_lo,iglo)
          ik = ik_idx(g_lo,iglo)
          is = is_idx(g_lo,iglo)
          g0(:,iglo) = zi * akx(it) *  gp(ipfrom) * gkp(it,ik,ipfrom,is) &
               * j0( gp(ipfrom) * sqrt(vperp2(iglo)) ) / sqrt(pi)
       end do
       call transform2 (g0, gtmp, iac)
       pbr(:,:,ipfrom) = vexbr(:,:,1) * gtmp
       ! vy . grad g_Pf
       g0 = (0.0, 0.0)
       do iglo=g_lo%llim_proc, g_lo%ulim_proc
          it = it_idx(g_lo,iglo)
          ik = ik_idx(g_lo,iglo)
          is = is_idx(g_lo,iglo)
          g0(:,iglo) = zi * aky(ik) * gp(ipfrom) * gkp(it,ik,ipfrom,is) &
               * j0( gp(ipfrom) * sqrt(vperp2(iglo)) ) / sqrt(pi)
       end do
       call transform2 (g0, gtmp, iac)
       pbr(:,:,ipfrom) = pbr(:,:,ipfrom) + vexbr(:,:,2) * gtmp
    end do

    call dealloc8 (r2=gtmp, v='gtmp_ptrans')
    call dealloc8 (r3=vexbr, v='vexbr_ptrans')

    allocate (gto(2, accelx_lo%llim_proc:accelx_lo%ulim_alloc, npgrid)); call alloc8 (r3=gto, v='gto_ptrans') ; gto = 0.0

    ! Second npgrid loop for 'to' p-numbers
    do ipto=1, npgrid
       g0 = (0.0, 0.0)
       do iglo = g_lo%llim_proc, g_lo%ulim_proc
          it = it_idx(g_lo,iglo)
          ik = ik_idx(g_lo,iglo)
          is = is_idx(g_lo,iglo)
          g0(:,iglo) = gp(ipto) * gkp(it,ik,ipto,is) &
               ! Here we need exp(vperp2) factor to cancel the Jacobian
               ! in the integrate_moment
               * j0( gp(ipto) * sqrt(vperp2(iglo)) ) * exp(vperp2(iglo)) / sqrt(pi)
       end do
       call transform2 (g0, gto(:,:,ipto), iac)
    end do

    call dealloc8 (c2=g0, v='g0_ptrans')
    call dealloc8 (c4=gkp, v='gkp')

    ! Computation of transfer function
    do ipto=1, npgrid
       do ipfrom=1, npgrid
          call integrate_moment &
               (accelx_lo, pbr(:,:,ipfrom)*gto(:,:,ipto), mom)
          ! T(pfrom,pto) = int (1/F_0) check{g}_{pto} v . grad check{g}_{pfrom}
          !                     F_0 v dv  <- Jacobian
          ptrans(ipfrom,ipto) = - sum(mom(:,:,1)) / (nx*ny) ! took ispec=1
       end do
    end do

    call dealloc8 (r3=gto, v='gto_ptrans')
    call dealloc8 (r3=pbr, v='pbr_ptrans')

  end subroutine get_ptransfer

  subroutine heating (istep, h, hk)
    use mp, only: proc0
    use dist_fn, only: get_heat
    use fields_arrays, only: phi, apar, bpar, phinew, aparnew, bparnew
    use agk_heating, only: avg_hk
    use agk_heating_index
    use agk_time, only: dtime
    implicit none
    integer, intent (in) :: istep
    real, intent (out) :: h(:), hk(:,:,:)
    integer :: ie

    if(debug.and.proc0) write(*,*) "agk_diagnostics: loop_diagnostics: heating: starting." !DEBUG-KDN-110718
    call get_heat (hk, phi, apar, bpar, phinew, aparnew, bparnew)
    if(debug.and.proc0) write(*,*) "agk_diagnostics: loop_diagnostics: heating: get_heat successful." !DEBUG-KDN-110718

    if (proc0) then
       if(navg > 1) call avg_hk(hk, istep, navg, nwrite)
       do ie=1,lemax
          h(ie)=sum(hk(:,:,ie))
       end do
      if(debug) write(*,*) "agk_diagnostics:loop_diagnostics:heating: avg_hk successful." !DEBUG-KDN-110718
    end if

    !>RN want to save initial energies
    if (proc0) then
       if (energy0==0.) then
          energy0=h(lenergy)
          eapar0=h(leapar)
          ebpar0=h(lebpar)
       end if
    end if
    !<RN

    !Calculate power and entropy balances
    if (proc0) then
       h(lpowbal)=h(lantenna)+h(lenergy_dot)+sum(h(lheating(:)))
       h(lpowbalcoll)=h(lantenna)+h(lenergy_dot)-sum(h(limp_colls(:)))
       h(lentbal(:))=h(limp_colls(:))+h(lheating(:))
    endif
    if (proc0) then
       h(ldiss1)=diss1+sum(h(limp_colls(:)))*dtime(1)
       diss1=h(ldiss1)
       h(ldiss2)=h(lenergy)-energy0
    endif
    if(debug.and.proc0) write(*,*) "agk_diagnostics: loop_diagnostics: heating: finished." !DEBUG-KDN-110718

  end subroutine heating
!>GGH
!-----------------------------------------------------------------------------
! Density: Calculate Density perturbations
!-----------------------------------------------------------------------------
 subroutine dens_vel (istep, dv, dvk)
    use dist_fn, only: get_dens_vel
    use fields_arrays, only: phi, apar, bpar, phinew, aparnew, bparnew
    use agk_heating, only: dens_vel_diagnostics, avg_dv, avg_dvk, zero_dvtype
    implicit none
    !Passed
    integer, intent (in) :: istep
    type (dens_vel_diagnostics) :: dv
    type (dens_vel_diagnostics), dimension(:,:) :: dvk

    !Initialize Density and velocity perturbations
    call zero_dvtype(dv)
    call zero_dvtype(dvk)

    !Call routine to calculate density and velocity perturbations
    call get_dens_vel(dv, dvk, phi, apar, bpar, phinew, aparnew, bparnew)    
    
    !Do averages with a history variable
    call avg_dv(dv, dv_hist, istep, navg)
    call avg_dvk(dvk, dvk_hist, istep, navg)

  end subroutine dens_vel
!-----------------------------------------------------------------------------
! Density: Calculate Density perturbations
!-----------------------------------------------------------------------------
 subroutine calc_jext (istep, j_ext)
    use mp, only: proc0
    use dist_fn, only: get_jext
    implicit none
    !Passed
    integer, intent (in) :: istep
    real, dimension(:,:) ::  j_ext
    !Local 
    integer :: i

    !Call routine to calculate density and velocity perturbations
    call get_jext(j_ext)    
    
    !Do averages with a history variable
    if (proc0) then
       !Save variable to history
       if (navg > 1) then
          if (istep > 1) &
               j_ext_hist(:,:,mod(istep,navg))= j_ext(:,:)

          !Use average of history
          if (istep >= navg) then
             j_ext=0.
             do i=0,navg-1
                j_ext(:,:) = j_ext(:,:) + j_ext_hist(:,:,i)/ real(navg)
             end do
          end if
       end if
    end if

  end subroutine calc_jext
!-----------------------------------------------------------------------------
!<GGH

  subroutine get_omegaavg (istep, exit, omegaavg)
    use kgrids, only: naky, nakx
    use fields_arrays, only: phi, apar, bpar, phinew, aparnew, bparnew
    use agk_time, only: dtime
    use constants
    use file_utils, only: error_unit
    implicit none
    integer, intent (in) :: istep
    logical, intent (in out) :: exit
    complex, dimension (:,:), intent (out) :: omegaavg
    complex, dimension (navg,nakx,naky) :: domega
    integer :: j

    j = igomega
    where (abs(phinew(j,:,:)+aparnew(j,:,:)+bparnew(j,:,:)) < epsilon(0.0) &
           .or. abs(phi(j,:,:)+apar(j,:,:)+bpar(j,:,:)) < epsilon(0.0))
       omegahist(mod(istep,navg),:,:) = 0.0
    elsewhere
       omegahist(mod(istep,navg),:,:) &
            = log((phinew(j,:,:) + aparnew(j,:,:) + bparnew(j,:,:)) &
                  /(phi(j,:,:)   + apar(j,:,:)    + bpar(j,:,:)))*zi/dtime(1)
    end where

    omegaavg = sum(omegahist/real(navg),dim=1)

    if (istep > navg) then
       domega = spread(omegaavg,1,navg) - omegahist
       if (all(sqrt(sum(abs(domega)**2/real(navg),dim=1)) &
            < min(abs(omegaavg),1.0)*omegatol)) &
       then
          if (write_ascii) write (out_unit, "('*** omega converged')")
          exit = .true. .and. exit_when_converged
       end if

       if (any(abs(omegaavg)*dtime(1) > omegatinst)) then
          ! This is not necessarily numerical instability
          if (write_ascii) write (out_unit, "('*** numerical instability detected')") 
          write (error_unit(), "('ERROR: Numerical instability detected',e16.8)") maxval(abs(omegaavg))*dtime(1)
          exit = .true.
       end if
    end if

  end subroutine get_omegaavg
!-----------------------------------------------------------------------------
! Set up corrections for polar energy spectrum
!-----------------------------------------------------------------------------
!NOTE: Here we calculate the correction factors for each possible kperp
  subroutine init_polar_spectrum
    use kgrids, only: naky, nakx, nkpolar, ikx, iky, kperp2, box
    use constants, only: pi
!    use nonlinear_terms, only: nonlin
    use species, only: nspec
    use agk_mem, only: alloc8, dealloc8
    implicit none
!    real, dimension(:,:), allocatable :: kp_by_mode
    integer, dimension(:), allocatable :: num, nbin
    real, dimension(:), allocatable :: kp
    real, dimension(:), allocatable :: kpavg_lim
    real :: kpmax,dkp
    integer :: nkperp                           !Total possible number of kperps
    integer :: ik, it, i,j,inbx !,nkx,nky

! NOTE: In this routine, a square domain is assumed! (nx=ny)
! In read_parameters, write_Epolar => .false. if nx /= ny

    !Determine total number of possible kperps and allocate array
    nkperp = nakx**2 + naky**2
    allocate (num(1:nkperp)); call alloc8(i1=num,v="num"); num=0
    allocate (kp(1:nkperp)); call alloc8(r1=kp,v="kp"); kp(:)=huge(kp)
!    allocate(kp_by_mode(nakx,naky)) ; kp_by_mode(:,:)=0.
    allocate (polar_index(nakx,naky)); call alloc8(i2=polar_index,v="polar_index"); polar_index(:,:)=0

! Loop through all modes and sum number at each kperp
    do it = 1,nakx
       do ik= 1,naky
           !Attempted fix for crash with nl_mode='off' GGH 19 AUG 2011
!          if (nonlin .and. it == 1 .and. ik == 1) cycle
          if (box .and. it == 1 .and. ik == 1) cycle

! Add to number and calculate magnitude of this kperp
          i = ikx(it)**2 + iky(ik)**2
          num(i)=num(i)+1
          kp(i) = sqrt(kperp2(it, ik))
          polar_index(it,ik)=i
       enddo
    enddo
   
    !Collapse bins to only existing values of kperp
    !Find total number of existing kperps
    nbx=0
    do i=1,nkperp
       if (num(i) > 0) nbx=nbx+1
    enddo
    
    !Allocate bin variables
    allocate (nbin(1:nbx)); call alloc8(i1=nbin,v="nbin"); nbin=0
    allocate (kpbin(1:nbx)); call alloc8(r1=kpbin,v="kpbin"); kpbin=0.
    allocate (ebincorr(1:nbx)); call alloc8(r1=ebincorr,v="ebincorr"); ebincorr=0.
    allocate (ebinarray(1:nbx,3+3*nspec)); ebinarray=0. !Used in loop diagnostics !!! RN> unable to account memory for pointers

    !Copy data
    inbx=0
    do i=1,nkperp
       if (num(i) > 0) then
          inbx=inbx+1
          nbin(inbx)=num(i)
          kpbin(inbx)=kp(i)
          !Correct polar_index
          do it = 1,nakx
             do ik= 1,naky
                if (polar_index(it,ik) == i) polar_index(it,ik)=inbx
             enddo
          enddo
       endif
    enddo

    !Calculate the correction factor for the discrete values
    ebincorr=pi*kpbin/real(nbin)
!ERROR: NOTE This needs to be corrected to look like 
!    ebincorr=pi*(kpbin/kmin)/real(nbin)
! to prevent an accidental division by kmin GGH Jun 07

    !Deallocate variables
!    deallocate(num,kp,nbin)
    call dealloc8(i1=num,v="num")    !<KDN> These dealloc8 statements
    call dealloc8(r1=kp,v="kp")      ! fail on Kraken
    call dealloc8(i1=nbin,v="nbin")

    !Allocate variables for log-averaged polar spectrum
    allocate(kpavg_lim(1:nkpolar+1)); call alloc8(r1=kpavg_lim,v="kpavg_lim"); kpavg_lim(:)=0.
    allocate(numavg(1:nkpolar)); call alloc8(i1=numavg,v="numavg"); numavg(:)=0.
    allocate(kpavg(1:nkpolar)) ; call alloc8(r1=kpavg,v="kpavg"); kpavg(:)=0.
    allocate(eavgarray(1:nkpolar,9)); eavgarray(:,:)=0. !Used in loop diagnostics !!! RN> unable account memory for pointers
    allocate(polar_avg_index(1:nbx)); call alloc8(i1=polar_avg_index,v="polar_avg_index"); polar_avg_index(:)=0

    !Determine limits of log-averaged kperp spectra bins
    kpmax=real(int(kpbin(nbx)/kpbin(1))+1)*kpbin(1)
    ! RN> These bins do not accommodate all kp eg. if dkp>kpbin(1)
!!$    dkp=(kpmax-kpbin(1))/real(nkpolar)
!!$    do i=1,nkpolar+1
!!$       kpavg_lim(i)=dkp*real(i)
!!$    enddo
    ! RN> should be like
    dkp=kpmax/real(nkpolar)
    do i=0,nkpolar
       kpavg_lim(i+1)=dkp*real(i)
    end do
    
    !Do log-average of kperp in each bin and build index
    do i =1,nbx
       do j=1,nkpolar
          if (kpbin(i) .ge. kpavg_lim(j) .and. kpbin(i) .lt. kpavg_lim(j+1) ) then
             numavg(j)=numavg(j)+1
             kpavg(j)=kpavg(j)+log(kpbin(i))
             polar_avg_index(i)=j
          endif
       enddo
    enddo
    
    !Finish log-averaging
    kpavg(:)=kpavg(:)/numavg(:)
    kpavg(:)=exp(kpavg(:))

!    deallocate(kpavg_lim)
    call dealloc8(r1=kpavg_lim,v="kpavg_lim")

    !Debug
!    if (0. .eq. 0.) then
!       do i=1,nbx
!          write(*,'(i8,es12.4,i8,es12.4)')i, kpbin(i), polar_avg_index(i)
!       enddo
!       write(*,*)'Total kperps= ',nbx,' Total binned= ',sum(numavg)
!       do i=1,nkpolar
!          write(*,'(i8,es12.4,i8)')i, kpavg(i), int(numavg(i))
!       enddo
!    endif

    !Debug
!    if (0. .eq. 1.) then
!       do i=1,nbx
!          write(*,'(i8,es12.4,i8,es12.4)')i, kpbin(i), int(nbin(i)), ebincorr(i)
!       enddo
!       do it = 1,nakx
!          do ik= 1,naky
!             nkx=-(mod((it-1)+int(nakx/2),nakx)-int(nakx/2))
!             nky=ik-1
!            write(*,'(5i8,2es12.4)') it,ik,nkx,nky,polar_index(it,ik),akx(it),aky(ik)
!          enddo
!       enddo
!    endif

  end subroutine init_polar_spectrum
!-----------------------------------------------------------------------------
! Calculate corrected polar spectrum 
!-----------------------------------------------------------------------------
! NOTE: polar_index connects a given mode to the correct binned kperp value
! NOTE: It is assumed here that weighting factors for ky=0 are already 
! incorporated in ee (already done in heating calculations)
  subroutine get_polar_spectrum(ee,ebin)
    use kgrids, only: naky, nakx, box
!    use nonlinear_terms, only: nonlin
    implicit none
    real, dimension (:,:), intent (in) :: ee   !variable ee by (kx,ky) mode
    real, dimension (:), intent (out) :: ebin  !variable ee in kperp bins
    integer :: ik, it

    !Initialize ebin
    ebin=0.

    !Loop through all modes and sum number at each kperp
    do it = 1,nakx
       do ik= 1,naky
          !Attempted fix for crash with nl_mode='off' GGH 19 AUG 2011
!          if (nonlin .and. it == 1 .and. ik == 1) cycle
          if (box .and. it == 1 .and. ik == 1) cycle
          ebin(polar_index(it,ik))= ebin(polar_index(it,ik))+ee(it,ik)
       enddo
    enddo
    
    !Make corrections for discrete spectrum
    ebin=ebin*ebincorr

  end subroutine get_polar_spectrum
!-----------------------------------------------------------------------------
! Calculate corrected polar spectrum with an average in z
!-----------------------------------------------------------------------------
! NOTE: polar_index connects a given mode to the correct binned kperp value
! NOTE: At the moment, this is unused in the code
  subroutine get_polar_spectrum_zavg(a,b,ebin)
    use theta_grid, only: ntgrid, delthet, jacob
    use kgrids, only: naky, nakx, aky, box
!    use nonlinear_terms, only: nonlin
    implicit none
    real, dimension (-ntgrid:,:,:), intent (in) :: a,b  !data by (ig,kx,ky) mode
    real, dimension (:), intent (out) :: ebin  !variable ee in kperp bins
    integer :: ik, it
    real, dimension (-ntgrid:ntgrid) :: wgt
    real :: anorm
    real :: fac                                !Factor for ky=0 modes

    !Get weighting for z-average
    wgt = delthet*jacob
    anorm = sum(wgt)

    !Initialize ebin
    write (*,*) size(ebin),' is size of ebin'
    ebin=0.

    !Loop through all modes and sum number at each kperp
    do it = 1,nakx
       do ik= 1,naky
          fac = 0.5
          if (aky(ik) < epsilon(0.)) fac = 1.0

          !Attempted fix for crash with nl_mode='off' GGH 19 AUG 2011
!          if (nonlin .and. it == 1 .and. ik == 1) cycle
          if (box .and. it == 1 .and. ik == 1) cycle

          write (*,*) polar_index(it,ik),' should be < nbx?'
          ebin(polar_index(it,ik))= ebin(polar_index(it,ik)) + &
               sum(real(a(:,it,ik)*b(:,it,ik)*wgt))/anorm*fac
       enddo
    enddo
    
    !Make corrections for discrete spectrum
    ebin=ebin*ebincorr

  end subroutine get_polar_spectrum_zavg
!-----------------------------------------------------------------------------
!
!-----------------------------------------------------------------------------
! Deallocate variables used for polar spectrum
  subroutine finish_polar_spectrum
    use agk_mem, only: dealloc8
    implicit none

    !Deallocate variables
!    deallocate(polar_index,kpbin,ebincorr,ebinarray)
!    deallocate(numavg,kpavg,eavgarray,polar_avg_index)
    call dealloc8(i2=polar_index,v="polar_index")
    call dealloc8(r1=kpbin,v="kpbin")
    call dealloc8(r1=ebincorr,v="ebincorr")
    deallocate(ebinarray) !!! RN> unable to account memory for pointers
    call dealloc8(i1=numavg,v="numavg")
    call dealloc8(r1=kpavg,v="kpavg")
    deallocate(eavgarray) !!! RN> unable to account memory for pointers
    call dealloc8(i1=polar_avg_index,v="polar_avg_index")

  end subroutine finish_polar_spectrum
!-----------------------------------------------------------------------------
! Initialize Adaptive Hypercollisionality 
!-----------------------------------------------------------------------------
  subroutine init_adapt_hc
    use kgrids, only: naky, nakx, kperp2
    use nonlinear_terms, only: nonlin
    use species, only: nspec, spec, specie
    use agk_mem, only: alloc8
    implicit none
    real :: kpmin,kpmax            !Min/max values of kperp for ahc control
    real :: kp                     !Value of kperp for this (it,ik) mode
    integer :: is,it,ik            !indices

    !Initialize and determine the index for modes within the adpative control
    allocate(ahc_index(1:nspec,1:nakx,1:naky)); call alloc8(i3=ahc_index,v="ahc_index"); ahc_index(:,:,:)=0

    !Loop over species and determine which values fall within the desired
    !   kperp band given by kp_hc +/- dkp_hc/2.
    do is=1,nspec
       kpmin=spec(is)%kp_hc-spec(is)%dkp_hc/2.
       kpmax=spec(is)%kp_hc+spec(is)%dkp_hc/2.
       do it = 1,nakx
          do ik= 1,naky
             if (nonlin .and. it == 1 .and. ik == 1) cycle
             kp = sqrt(kperp2(it, ik))
             !Set index to 1 if kperp falls within desired kperp band
             if (kp .ge. kpmin .and. kp .le. kpmax) ahc_index(is,it,ik)=1
             
             !DEBUG
             if (.false. .and. ahc_index(is,it,ik) .eq. 1) then
                write(*,'(a,3i6,a,f6.3)')'(is,it,ik) = ',is,it,ik,' kp= ',kp
             endif

          enddo
       enddo
    enddo

    !Allocate variables
    allocate(pk1(1:nspec)); call alloc8(r1=pk1,v="pk1")
    allocate(pk2(1:nspec)); call alloc8(r1=pk2,v="pk2")
    allocate(ek(1:nspec)); call alloc8(r1=ek,v="ek")
    allocate(etot(1:nspec)); call alloc8(r1=etot,v="etot")

    allocate(wnl(1:nspec)); call alloc8(r1=wnl,v="wnl")
    allocate(kphc(1:nspec)); call alloc8(r1=kphc,v="kphc")
    allocate(bk(1:nspec)); call alloc8(r1=bk,v="vk")

    allocate(fm1(1:nspec)); call alloc8(r1=fm1,v="fm1")
    allocate(fm2(1:nspec)); call alloc8(r1=fm2,v="vm2")
    allocate(ft(1:nspec)); call alloc8(r1=ft,v="ft")

    !Save kperp control values for each species and threshold
    kphc(:)=0.0 ; ft(:)=0.
    do is=1,nspec
       kphc(is)=spec(is)%kp_hc
       ft(is)=spec(is)%gw_hc
    enddo

  end subroutine init_adapt_hc
!-----------------------------------------------------------------------------
! Calculate Diagnostic for Adaptive Hypercollisionality and output results
!-----------------------------------------------------------------------------
! Hypercollisionality adjustment measures, at a given value of kperp (kp_hc) are
! IONS:           fm1= pci+phci / ( E_dfi * wnl ) 
!                 fm2 = phci / ( E_dfi * wnl ) 
! ELECTRONS:      fm1= pci+phci+pce_phce / ( E_tot * wnl ) 
!                 fm2 = phce / ( E_tot * wnl ) 
! To determine if we even try to adjust, we want to ensure significant energy in the
!    modes at this kperp, so we need to compare the local energy with a value
!    over all modes (it,ik).  Thus, 
! IONS:           Sum_k  E_dfi(k)
! ELECTRONS:      Sum_k  E_tot(k)
! The threshold value of gamma/omega is set to ft.  nu_h is changed when
!            abs(fm1-ft)/(fm1+ft) .gt. spec(is)%gw_frac
 subroutine get_adapt_hc(nuh_changed)
    use species, only: nspec, spec, specie
    use species, only: ion_species, electron_species, has_electron_species
    use run_parameters, only: use_Apar, tite
    use agk_heating_index
    use file_utils, only: error_unit, flush_output_file
    use kgrids, only: naky, nakx
    use nonlinear_terms, only: nonlin
    use run_parameters, only: beta
    use agk_time, only: time
    implicit none
    !Passed
    logical, intent(out) :: nuh_changed
    !Local
    integer :: is,it,ik ,is2          !indices
    !Kolmogorov Constants
!    real, parameter :: cm1 = 2.5
!    real, parameter :: cm2 = 2.2
!    real, parameter :: ck1 = 2.5
!    real, parameter :: ck2 = 2.2
    real, parameter :: c2 = 1.1
    integer :: ierr
    real :: tite_local
    integer :: nmode

    !Initialize variables
    pk1 = 0. ; pk2 = 0. ; ek = 0. ; etot = 0. !; kp = 0. !; nuhold = 0. 
    bk = 0. ; wnl = 0.
    fm1 = 0. ; fm2 = 0. !; ft =0.
    nuh_changed=.false.

    !Loop over all modes (it,ik) for each species to determine heating rates, etc.
    nmode=0
    do is=1,nspec
!       kp(is)=spec(is)%kp_hc
       !Save old value hypercollisionality
!       nuhold(is)=spec(is)%nu_h
!       ft(is)=spec(is)%gw_hc
       !For electron species, we want etot=sum e_tot=h%energy
       if (spec(is)%type .eq. electron_species) &
            etot(is)=h(lenergy)   !Total energy
       do it = 1,nakx
          do ik= 1,naky
             if (nonlin .and. it == 1 .and. ik == 1) cycle
             !For ion species, we want etot=sum e_delfi2
             if (spec(is)%type .eq. ion_species) &
                  etot(is)=etot(is)+hk(it,ik, ldelfs2(is))
             
             !Skip this mode if not within kperp value for HC control
             if (ahc_index(is,it,ik) .eq. 0) cycle

             !Sum up E_(kperp a_par) 
             if (use_Apar) then
                bk(is)=bk(is)+hk(it,ik,leapar)
! TT: I'm not sure about Z or T factor for lphis2
             else
                bk(is) = bk(is) + hk(it,ik,lphis2(is))
             end if
             nmode=nmode+1

             !Choose which values to add depending on species type
             select case(spec(is)%type)
             case (ion_species)
                !For ions, pk1=pci+phci, pk2 =phci and ek=e_delfi2
                pk1(is)=pk1(is)+hk(it,ik,lcollisions(is))+hk(it,ik,lhypercoll(is))
                pk2(is)=pk2(is)+hk(it,ik,lhypercoll(is))
                ek(is)=ek(is)+hk(it,ik,ldelfs2(is))
             case (electron_species)
                !For electrons, pk1=pci+phci+pce+phce, pk2=phce and ek=e_all
                do is2=1,nspec
                   pk1(is)=pk1(is)+hk(it,ik,lcollisions(is2))+hk(it,ik,lhypercoll(is2))
                enddo
                pk2(is)=pk2(is)+hk(it,ik,lhypercoll(is))
                ek(is)=ek(is)+hk(it,ik,lenergy)
             case default !Otherwise there is a problem
                ierr=error_unit()
                write(ierr,'(a,i2)')'Error in agk_diagnostics:get_adapt_hc: Unexpected species type ',spec(is)%type
             end select
          enddo
       enddo
    enddo

    !Get bk from E_(kperp a_par)  TT: This works only for EM runs
    !CHECK: Is this correct? It seems to be correct
!ERROR    Here I need to worry about which value of kperp and bk I am using
!NOTE: In normalized units, bk = kperp * Apar/( 2 * sqrt(beta_0))
!    bk(:)=sqrt(bk(:)/real(nmode))  !NOTE: THIS SEEMS TO NOT WORK
    bk(:)=sqrt(kphc(:)*bk(:))
    
    !Calculate wnl from bk(:)
    !GGH NOTE: This assumes ion and electron plasma only with ions as reference
    if (has_electron_species(spec)) then
       tite_local = spec(1)%temp / spec(2)%temp
    else
       tite_local = tite
    end if
    wnl(:)=c2*kphc(:)*bk(:)* sqrt(1.+  kphc(:)**2/(beta+2./(1.+1./tite_local)))

! TT: this subroutine only called for proc0
    if (any(ek==0.0) .or. any(wnl==0.0)) then
       write (error_unit(),'(a)') 'Error in agk_diagnostics:get_adapt_hc: &
            & division by zero (ek or wnl).'
       return
    end if
    !Construct measures for adapting collisionality
    fm1(:)=pk1(:)/(ek(:)*wnl(:))
    fm2(:)=pk2(:)/(ek(:)*wnl(:))

    !Update hypercollisional coefficients adaptively
    do is=1,nspec
       
       !Only consider adapting nu_h is there is sufficient energy at this kperp
       if (ek(is)/etot(is) .lt. 1.0E-06 .or. .not. spec(is)%adapt_hc) cycle

       if (abs(fm1(is)-ft(is))/(fm1(is)+ft(is)) .gt. spec(is)%gw_frac .and. &
            fm1(is) .gt. ft(is)) then
          !If measured damping is too strong, reduce nuh only
          !     if Phc (fm2) is dominant and if we are greater than min_nuh
          if (abs(fm2(is)-ft(is))/(fm2(is)+ft(is)) .gt. spec(is)%gw_frac .and. &
               fm2(is) .gt. ft(is) .and. spec(is)%nu_h .gt. spec(is)%min_nuh) then
             spec(is)%nu_h=max( spec(is)%nu_h* (ft(is)/fm2(is)),spec(is)%min_nuh )
!             if (spec(is)%nu_h .lt. spec(is)%min_nuh)  &
!                  spec(is)%nu_h = spec(is)%min_nuh
             nuh_changed=.true.
          endif
       elseif (abs(fm1(is)-ft(is))/(fm1(is)+ft(is)) .gt. spec(is)%gw_frac .and. &
            fm1(is) .lt. ft(is)) then
          !Otherwise, damping is not strong enough, so increase nuh but only 
          !     if we are less than max_nuh     
          if (spec(is)%nu_h .lt. spec(is)%max_nuh) then
             spec(is)%nu_h=min( spec(is)%nu_h* (ft(is)/fm1(is)),spec(is)%max_nuh)
!             if (spec(is)%nu_h .gt. spec(is)%max_nuh)  &
!                  spec(is)%nu_h = spec(is)%max_nuh
             nuh_changed=.true.
          endif
       endif
       
    enddo

    !Output values to ascii file if desired
    if (write_ascii .and. write_adapt_hc) then
       do  is=1,nspec
         write(ahc_unit(is),'(es16.8,10es12.4)')time,kphc(is),bk(is),wnl(is), &
              pk1(is),pk2(is),ek(is),etot(is),fm1(is),fm2(is), spec(is)%nu_h
         call flush_output_file(ahc_unit(is))
       enddo
    endif

  end subroutine get_adapt_hc
!-----------------------------------------------------------------------------
! Finalize Adaptive Hypercollisionality 
!-----------------------------------------------------------------------------
  subroutine finish_adapt_hc
    use agk_mem, only: dealloc8
    implicit none

!    deallocate(ahc_index)
!    deallocate(pk1,pk2,ek,etot)
!    deallocate(wnl,kphc,bk)
!    deallocate(fm1,fm2,ft)
    call dealloc8(i3=ahc_index,v="ahc_index")
    call dealloc8(r1=pk1,v="pk1")
    call dealloc8(r1=pk2,v="pk2")
    call dealloc8(r1=ek,v="ek")
    call dealloc8(r1=etot,v="etot")
    call dealloc8(r1=wnl,v="wnl")
    call dealloc8(r1=kphc,v="kphc")
    call dealloc8(r1=bk,v="bk")
    call dealloc8(r1=fm1,v="fm1")
    call dealloc8(r1=fm2,v="fm2")
    call dealloc8(r1=ft,v="ft")

  end subroutine finish_adapt_hc
!-----------------------------------------------------------------------------

!-----------------------------------------------------------------------------
  subroutine get_vol_average_all (a, b, axb, axb_by_mode)
    use theta_grid, only: ntgrid, delthet, jacob
    use kgrids, only: naky, nakx
    implicit none
    complex, dimension (-ntgrid:,:,:), intent (in) :: a, b
    real, intent (out) :: axb
    real, dimension (:,:), intent (out) :: axb_by_mode

    integer :: ik, it
    real, dimension (-ntgrid:ntgrid-1) :: wgt
    real :: anorm

    wgt = delthet*jacob  !NOTE: This is a constant in AstroGK
    anorm = sum(wgt)

    do ik = 1, naky
       do it = 1, nakx
          axb_by_mode(it,ik) = sum( real(conjg(a(-ntgrid:ntgrid-1,it,ik)) &
               * b(-ntgrid:ntgrid-1,it,ik)) * wgt ) / anorm
       end do
    end do

    call get_volume_average (axb_by_mode, axb)

  end subroutine get_vol_average_all

  subroutine get_vol_average_one (a, b, axb, axb_by_mode)
    use kgrids, only: naky, nakx
    implicit none
    complex, dimension (:,:), intent (in) :: a, b
    real, intent (out) :: axb
    real, dimension (:,:), intent (out) :: axb_by_mode

    integer :: ik, it

    do ik = 1, naky
       do it = 1, nakx
          axb_by_mode(it,ik) = real(conjg(a(it,ik))*b(it,ik))
       end do
    end do

    call get_volume_average (axb_by_mode, axb)
  end subroutine get_vol_average_one

  subroutine get_vol_average_vspec (f, favg, all)

    ! takes in f(x, y, sigma, lambda, E, species) and returns int |f|**2, where
    ! the integral is over all kperp-space

    use mp, only: nproc, proc0, sum_reduce, sum_allreduce
    use species, only: nspec
    use kgrids, only: aky
    use theta_grid, only: ntgrid
    use agk_layouts, only: le_lo, ik_idx, it_idx, ig_idx, is_idx
    use agk_mem, only: alloc8, dealloc8

    implicit none

    real, dimension (:,:,le_lo%llim_proc:), intent (in) :: f
    real, dimension (:,:,-ntgrid:,:), intent (out) :: favg
    integer, intent (in), optional :: all

    integer :: ile, ig, ik, is, ie, i, il, n1, n2
    real :: fac
    real, dimension (:), allocatable :: work

    favg = 0.
    do ile = le_lo%llim_proc, le_lo%ulim_proc
       ik = ik_idx(le_lo, ile)
       is = is_idx(le_lo, ile)
       ig = ig_idx(le_lo, ile)

       if (aky(ik) == 0.) then
          fac = 1.0
       else
          fac = 0.5
       end if
       favg(:,:,ig,is) = favg(:,:,ig,is) + fac*f(:,:,ile)
    end do

    if (nproc > 1) then
       n1 = size(favg,1) ; n2 = size(favg,2)
       allocate (work(n1*n2*(2*ntgrid+1)*nspec)); call alloc8(r1=work,v="work"); work = 0.
       i = 0
       do is = 1, nspec
          do ig = -ntgrid, ntgrid
             do ie = 1, n2
                do il = 1, n1
                   i = i + 1
                   work(i) = favg(il, ie, ig, is)
                end do
             end do
          end do
       end do

       if (present(all)) then
          call sum_allreduce (work)
       else
          call sum_reduce (work, 0)
       end if

       if (proc0 .or. present(all)) then
          i = 0
          do is = 1, nspec
             do ig = -ntgrid, ntgrid
                do ie = 1, n2
                   do il = 1, n1
                      i = i + 1
                      favg(il, ie, ig, is) = work(i)
                   end do
                end do
             end do
          end do
!          deallocate (work)
          call dealloc8(r1=work,v="work")
       end if
    end if
    
  end subroutine get_vol_average_vspec

  subroutine get_volume_average (f, favg)
    use kgrids, only: naky, nakx, aky
    implicit none
    real, dimension (:,:), intent (in) :: f
    real, intent (out) :: favg
    real :: fac
    integer :: ik, it

! ky=0 modes have correct amplitudes; rest must be scaled
! note contrast with scaling factors in FFT routines.

    favg = 0.
    do ik = 1, naky
       fac = 0.5
       if (aky(ik) == 0.) fac = 1.0
       do it = 1, nakx
          favg = favg + f(it, ik) * fac
       end do
    end do

  end subroutine get_volume_average

  subroutine get_fldline_average (f, favg)

    use constants, only: pi => dpi
    use theta_grid, only: ntgrid, dz, z0

    implicit none

    real, dimension (-ntgrid:), intent (in) :: f
    real, intent (out) :: favg

    favg = sum(f(:ntgrid-1)) * dz / (2.0*pi*z0)

  end subroutine get_fldline_average

  subroutine get_kspectrum (g0, hkk)

    use theta_grid, only: ntgrid
    use agk_layouts, only: g_lo
    use agk_mem, only: alloc8, dealloc8
    use fields_arrays, only: phinew, bparnew
    use dist_fn, only: g_adjust
    use le_grids, only: integrate_moment
    use kgrids, only: nakx, naky
    use run_parameters, only: fphi, fbpar
    use species, only: nspec

    ! TMP FOR TESTING -- MAB
!    use agk_layouts, only: it_idx, ik_idx
!    use theta_grid, only: theta, gradpar, nperiod
!    use mp, only: proc0
!    use kgrids, only: akx, aky

    implicit none
    
    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (in) :: g0
    real, dimension (-ntgrid:,:,:), intent (out) :: hkk

    complex, dimension (:,:,:), allocatable :: h0, g1
    real, dimension (:,:,:), allocatable :: h2
    real, dimension (:,:,:,:), allocatable :: h2int

    integer :: ig, is, iavg, ibin, all

    ! TMP FOR TESTING -- MAB
!    integer :: it, ik
!    real, dimension (2*ntgrid) :: kpar

    allocate (h0(-ntgrid:ntgrid,2,g_lo%llim_proc:g_lo%ulim_alloc)) ; call alloc8 (c3=h0, v="h0")
    allocate (g1(-ntgrid:ntgrid,2,g_lo%llim_proc:g_lo%ulim_alloc)) ; call alloc8 (c3=g1, v="g1")
    allocate (h2(-ntgrid:ntgrid,2,g_lo%llim_proc:g_lo%ulim_alloc)) ; call alloc8 (r3=h2, v="h2")
    allocate (h2int(-ntgrid:ntgrid,nakx,naky,nspec)) ; call alloc8 (r4=h2int, v="h2int")
    
    g1 = g0

    ! first switch from g to h
    call g_adjust (g1, phinew, bparnew, fphi, fbpar)

    ! TMP FOR TESTING -- MAB
!     do iglo=g_lo%llim_proc, g_lo%ulim_proc
!        it = it_idx(g_lo,iglo)
!        ik = ik_idx(g_lo,iglo)
!        do ig = -ntgrid, ntgrid
! !          g0(ig,:,iglo) = sin(2.*theta(ig))*cos(akx(it)**2+aky(ik)**2)+sin(2*pi*theta(ig))*cos(2*pi*(akx(it)**2+aky(ik)**2))
!           g1(ig,:,iglo) = (sin(gradpar*theta(ig))*cos(2.*gradpar*theta(ig))+cos(gradpar*theta(ig))**2)*exp(-0.1*(akx(it)**2+aky(ik)**2))
! !          g1(ig,:,iglo) = theta(ig)*sin(4*gradpar*theta(ig))
!        end do
!     end do
    
!     ! TMP FOR TESTING -- MAB
!     if (proc0) then
!        do ig = -ntgrid, ntgrid
!           write (*,"(a10,3(1x,e12.5))") 'h(theta): ', theta(ig), real(g1(ig,1,g_lo%llim_proc)), aimag(g1(ig,1,g_lo%llim_proc))
!        end do
!     end if

    ! transform from z to kpar
    call par_transform (g1, h0)
    
    ! won't be using g0 anymore, so switch back to g
    call g_adjust (g1, phinew, bparnew, -fphi, -fbpar)
    
    ! get |h|**2
    h2 = h0*conjg(h0)

    ! TMP FOR TESTING -- MAB
!     do ig = 1, ntgrid
!        kpar(ig) = (ig-1)*gradpar/real(2*nperiod-1)
!        kpar(2*ntgrid-ig+1)=-(ig)*gradpar/real(2*nperiod-1)
!     end do
!     if (proc0) then
!        ik = ik_idx(g_lo,g_lo%llim_proc)
!        it = it_idx(g_lo,g_lo%llim_proc)
!        do ig = ntgrid+1,2*ntgrid
!           write (*,"(a10,3(1x,e12.5))") 'h2(kpar): ', kpar(ig), sqrt(akx(it)**2+aky(ik)**2), h2(ig-ntgrid-1,1,g_lo%llim_proc)
!        end do
!        do ig = 1, ntgrid
!           write (*,"(a10,3(1x,e12.5))") 'h2(kpar): ', kpar(ig), sqrt(akx(it)**2+aky(ik)**2), h2(ig-ntgrid-1,1,g_lo%llim_proc)
!        end do
!        write (*,*)
!     end if
    
    ! eliminate velocity dependence by integrating over velocity space
    call integrate_moment (h2, h2int, all) ! do we need all here?

    ! TMP FOR TESTING -- MAB
!     if (proc0) then
!        do ig = ntgrid+1,2*ntgrid
!           write (*,"(a10,3(1x,e12.5))") 'h2(kpar): ', kpar(ig), sqrt(akx(2)**2+aky(2)**2), h2int(ig-ntgrid-1,2,2,1)
!        end do
!        do ig = 1, ntgrid
!           write (*,"(a10,3(1x,e12.5))") 'h2(kpar): ', kpar(ig), sqrt(akx(2)**2+aky(2)**2), h2int(ig-ntgrid-1,2,2,1)
!        end do
!        write (*,*)
!     end if
    
    ! bin kx and ky to get h2(kpar,kperp,spec)
    do is=1,nspec
       do ig=-ntgrid, ntgrid
          ebinarray(:,1) = 0.0
          call get_polar_spectrum (h2int(ig,:,:,is), ebinarray(:,1))
          eavgarray(:,1) = 0.0
          do ibin=1, nbx
             iavg = polar_avg_index(ibin)
             eavgarray(iavg,1) = eavgarray(iavg,1) + log(ebinarray(ibin,1))
          end do
          eavgarray(:,1) = exp(eavgarray(:,1) / numavg(:))
          hkk(ig,:,is)=eavgarray(:,1)
       end do
    end do

    call dealloc8(c3=h0, v="h0")
    call dealloc8(c3=g1, v="g1")
    call dealloc8(r3=h2, v="h2")
    call dealloc8(r4=h2int, v="h2int")
    
  end subroutine get_kspectrum
  
  subroutine write_dynfields(istep)
    use file_utils, only: open_output_file
    use theta_grid, only: ntgrid,theta
    use fields_arrays, only:apar,bpar,phi
    use agk_time, only: time
    integer :: ig
    integer, intent(in) :: istep
    character(30) :: extension

    if( .not. (mod(istep,nwrite_dyn) .eq. 0)) return
    if ( .not. dynfields_inited ) then
      dynfields_inited=.true.
      extension=".dynfields"
      call open_output_file (dynfields_unit, trim(extension))
!      write(dynfields_unit,'(2(a7,e12.5))') "# akx= ",akx(1),&
!                                            "  aky= ",aky(1)
!      write(dynfields_unit,'(a4,a12,a3,7(1x,a12))') "#   ","time","   ",&
!                           "z (theta)","Re(phi)","Im(phi)","Re(apar)",&
!                           "Im(apar)","Re(bpar)","Im(bpar)"
    end if
    do ig = -ntgrid, ntgrid
      write (dynfields_unit, '(a4,e12.5,a3,7(1x,e12.5))') " t= ",time,&
            " z=",theta(ig), phi(ig,1,1), apar(ig,1,1), bpar(ig,1,1)
    end do
    write (dynfields_unit, "()")

  end subroutine write_dynfields
  
  subroutine close_dynfields
    use file_utils, only: close_output_file
    call close_output_file(dynfields_unit)
  end subroutine close_dynfields
  
  subroutine init_stream_flux_potentials
    use file_utils, only: input_unit_exist, get_indexed_namelist_unit, &
                          open_output_file, error_unit 
    use kgrids, only: lukx,luky,nakx,naky,ikx,iky
    use theta_grid, only: ntgrid
    use agk_mem, only: alloc8
    
    integer :: in_file,imode,kx,ky,kz
    character(10) :: suffix
    namelist /mode_of_interest/ kx,ky,kz
    kx=0;ky=1;kz=0
    
    if ( n_modes_of_interest .lt. 1 ) then
      write_moi=.false.
      write(error_unit(), *) "write_moi set to .true., but n_modes_of_interest < 1."
      return
    else
      allocate( ikx_moi(n_modes_of_interest) ); call alloc8(i1=ikx_moi,v="ikx_moi")
      allocate( iky_moi(n_modes_of_interest) ); call alloc8(i1=iky_moi,v="iky_moi")
      allocate( ikz_moi(n_modes_of_interest) ); call alloc8(i1=ikz_moi,v="ikz_moi")
      do imode=1,n_modes_of_interest
        call get_indexed_namelist_unit (in_file, "mode_of_interest", imode)
        read (unit=in_file, nml=mode_of_interest)
        close(unit=in_file)
        !Make sure that none of the modes of interest are out of range;
        !If they are, panic and set the mode to (0,1,0), which should always
        !be represented by kgrids.
        if ( abs(kx) .gt. ((nakx-1)/2) ) then
          write (error_unit(),*) "Mode of Interest with out-of-range kx value"
          kx=0;ky=1;kz=0
        end if
        if ( abs(ky) .gt. (naky - 1) ) then
          write (error_unit(),*) "Mode of Interest with out-of-range ky value"
          kx=0;ky=1;kz=0
        end if
        if ( abs(kz) .gt. (ntgrid) ) then
          !This is thrown if |kz| > Nyquist mode
          write (error_unit(),*) "Mode of Interest with out-of-range kz value"
          kx=0;ky=1;kz=0
        end if
        ikx_moi(imode)=lukx(kx)
        iky_moi(imode)=luky(ky)
        ikz_moi(imode)=mod(kz+2*ntgrid,2*ntgrid)
      end do

      if (moi_no_fft) then 
         !Open files for each mode of interest
         allocate(array_moi_units(n_modes_of_interest));call alloc8(i1=array_moi_units,v="array_moi_units")
         do imode=1,n_modes_of_interest
            if (ikx(ikx_moi(imode)) .ge. 0) then
               write(suffix,'(a4,i2.2,a2,i2.2)')".kxp",ikx(ikx_moi(imode)),"ky",iky(iky_moi(imode))
            else
               write(suffix,'(a4,i2.2,a2,i2.2)')".kxm",abs(ikx(ikx_moi(imode))),"ky",iky(iky_moi(imode))
            endif
            call open_output_file(array_moi_units(imode),suffix)
         enddo

      else !Otherwise, open just one sfpots file
         ! Open file for writing
         call open_output_file(moi_unit,".sfpots")
         ! and write a legend
         write(moi_unit,'(a16,2(1x,a12),1x,a3,1x,a13,5(1x,a16))') "t","akx","aky","kz",&
                      "k_perp","Re(phi)","Im(phi)","Re(apar)","Im(apar)","E_tot"
      endif
    end if
  end subroutine init_stream_flux_potentials
  
  subroutine loop_stream_flux_potentials
    use agk_time, only: time
    use kgrids, only: akx,aky,kperp2
    use theta_grid, only:ntgrid,theta
    use fields_arrays, only:phi,apar,bpar
    use agk_transforms, only:kz_transform2
    use agk_heating_index, only: lenergy
    
    complex, dimension(2*ntgrid+1) :: kzphi,kzapar
    integer :: kx,ky,kz,imode,akz,ig
    
    kzphi=0; kzapar=0
    
    if (moi_no_fft) then !Write out fields along z (no FFTs) (GGH)a
       
       do imode=1, n_modes_of_interest
          kx=ikx_moi(imode)
          ky=iky_moi(imode)

          do ig = -ntgrid, ntgrid
             write (array_moi_units(imode), '(a4,e12.5,a4,1x,e12.5,2f9.4,6(1x,e12.5))') " t= ",time,&
                  " z= ",theta(ig), akx(kx),aky(ky), phi(ig,kx,ky), apar(ig,kx,ky), bpar(ig,kx,ky)
          end do
          write (array_moi_units(imode), "()")
       enddo

    else !Perform FFT in z and output  (kx,ky,kz) information

       do imode=1, n_modes_of_interest
          kx=ikx_moi(imode)
          ky=iky_moi(imode)
          call kz_transform2(phi(:,kx,ky),kzphi)
          call kz_transform2(apar(:,kx,ky),kzapar)
          do kz = ntgrid+1, 2*ntgrid
             !      kz=ikz_moi(imode)
             akz=mod(ntgrid+kz-1,2*ntgrid)-ntgrid
             write(moi_unit,'(e16.8,2(1x,e12.4),1x,i3,1x,e13.5,5(1x,e16.8))') time, &
                  akx(kx),aky(ky),akz,sqrt(kperp2(kx,ky)),kzphi(kz), &
                  kzapar(kz),hk(kx,ky,lenergy)
          end do
          do kz = 1, ntgrid
             akz=mod(ntgrid+kz-1,2*ntgrid)-ntgrid
             write(moi_unit,'(e16.8,2(1x,e12.4),1x,i3,1x,e13.5,5(1x,e16.8))') time, &
                  akx(kx),aky(ky),akz,sqrt(kperp2(kx,ky)),kzphi(kz), &
                  kzapar(kz),hk(kx,ky,lenergy)
          end do
       end do
       !Write a blank line between each step
       write(moi_unit,'()')
       
    endif

  end subroutine loop_stream_flux_potentials
  
  subroutine finish_stream_flux_potentials
    use file_utils, only: close_output_file
     integer :: imode

    call loop_stream_flux_potentials

    deallocate ( ikx_moi, iky_moi, ikz_moi )
    if (moi_no_fft) then
       do imode=1,n_modes_of_interest
          close(array_moi_units(imode))
       enddo
       deallocate(array_moi_units)
    else
       call close_output_file(moi_unit)
    endif

  end subroutine finish_stream_flux_potentials

!!!RN> 2017/02/15: moved from dist_fn as in gs2.
  subroutine init_par_filter
!RN    use theta_grid, only: ntgrid, nperiod
    use theta_grid, only: ntgrid
    use kgrids, only: nakx, naky
    use agk_transforms, only: init_zf

!RN    call init_zf (ntgrid, nperiod)
    call init_zf (ntgrid, nakx*naky)

  end subroutine init_par_filter

  subroutine par_spectrum(an, an2)

    use agk_transforms, only: kz_spectrum
    use theta_grid, only: ntgrid
    use kgrids, only: naky, nakx

    complex, dimension(:,:,:) :: an, an2    
    real :: scale

!RN    call kz_spectrum (an, an2, nakx, naky)
    call kz_spectrum (an, an2, nakx*naky)
    scale = 1./real(4*ntgrid**2)  
    an2 = an2*scale

  end subroutine par_spectrum

  subroutine par_transform (an_in, an_out)

    use agk_transforms, only: kz_transform
    use theta_grid, only: ntgrid
    use agk_layouts, only: g_lo

    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (in) :: an_in
    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (out) :: an_out
    real :: scale

    call kz_transform (an_in, an_out)
    scale = 1./real(2*ntgrid)
    an_out = an_out*scale

  end subroutine par_transform

  ! moved from fields
  subroutine phinorm (phitot)
    use fields_arrays, only: phinew, aparnew, bparnew
    use theta_grid, only: delthet, ntgrid
    use kgrids, only: naky, nakx
    use constants
    implicit none
    real, dimension (:,:), intent (out) :: phitot
    integer :: ik, it

! GS2 reported the square of this value.  Only used for rough diagnostic purposes.

    ! TT: In order to avoid double-counting of the end point for per BC
    ! we should sum (-ntgrid:ntgrid-1) :TT
    do ik = 1, naky
       do it = 1, nakx
          phitot(it,ik) = 0.5/pi &
               * sum( cabs(phinew(-ntgrid:ntgrid-1,it,ik)) &
               &    + cabs(aparnew(-ntgrid:ntgrid-1,it,ik)) &
               &    + cabs(bparnew(-ntgrid:ntgrid-1,it,ik)) ) * delthet
       end do
    end do

  end subroutine phinorm

end module agk_diagnostics
