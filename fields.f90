module fields

  use fields_arrays, only: phi, apar, bpar, phinew, aparnew, bparnew
  use fields_arrays, only: phi_ext, apar_ext

  implicit none

  public :: init_fields  ! initialize this module
  public :: advance      ! do a full time step
  public :: reset_init   ! Sets fields to zero.  Useful for certain kinds of restarts.
  public :: allocate_arrays

  private

  logical :: initialized = .false.
  logical :: debug=.false.

contains

  subroutine init_fields
    use mp, only: proc0
    use theta_grid, only: init_theta_grid
    use run_parameters, only: init_run_parameters
    use dist_fn, only: init_dist_fn
    use init_g, only: init_init_g, ginit
    use fields_implicit, only: init_fields_implicit, init_phi_implicit
    use nonlinear_terms, only: nl_finish_init => finish_init
    use antenna, only: init_antenna
    implicit none
    logical :: restarted

    if (initialized) return  ;  initialized = .true.

    call init_theta_grid       ! Set up grid along the field line.
    if (debug.and.proc0) write(*,*)'Theta Grid initialized'   !DEBUG-GGH-080324
    call init_init_g           ! Initialize module that handles initial conditions
    if (debug.and.proc0) write(*,*)'init_g initialized'   !DEBUG-GGH-080324
    call init_run_parameters   ! Get basic inputs, set up perpendicular grids.
    if (debug.and.proc0) write(*,*)'Run Parameters initialized'   !DEBUG-GGH-080324
    call init_dist_fn          ! Make sure code is ready to calculate with distribution function
    if (debug.and.proc0) write(*,*)'Dist Func initialized'   !DEBUG-GGH-080324
    call allocate_arrays       ! Allocate memory as necessary
    if (debug.and.proc0) write(*,*)'Arrays allocated'   !DEBUG-GGH-080324
    call init_fields_implicit  ! Calculate matrices for implicit soln of Maxwell eqns.
    if (debug.and.proc0) write(*,*)'Implicit Fields initialized'   !DEBUG-GGH-080324

! Turn on nonlinear terms if necessary.
    call nl_finish_init
    if (debug.and.proc0) write(*,*)'NL Finish initialized'   !DEBUG-GGH-080324

    call ginit (restarted)
    if (debug.and.proc0) write(*,*)'Ginit initialized'   !DEBUG-GGH-080324
    call init_antenna
    if (debug.and.proc0) write(*,*)'Antenna initialized'   !DEBUG-GGH-080324

    if (restarted) return

    call init_phi_implicit  ! actually: set up fields for first time step.
    if (debug.and.proc0) write(*,*)'Phi Implicit initialized'   !DEBUG-GGH-080324
    
  end subroutine init_fields

  subroutine allocate_arrays
    use theta_grid, only: ntgrid
    use kgrids, only: naky, nakx
    use agk_mem, only: alloc8 
    use run_parameters, only: store_eq
    use fields_arrays, only: phi_eq, apar_eq, bpar_eq
    implicit none
    logical :: alloc = .true.

    if (alloc) then
       allocate ( phi (-ntgrid:ntgrid, nakx, naky)) ; call alloc8 (c3= phi, v="phi")
       allocate (apar (-ntgrid:ntgrid, nakx, naky)) ; call alloc8 (c3=apar, v="apar")
       allocate (bpar (-ntgrid:ntgrid, nakx, naky)) ; call alloc8 (c3=bpar, v="bpar")

       allocate ( phinew (-ntgrid:ntgrid, nakx, naky)) ; call alloc8 (c3= phinew, v="phinew")
       allocate (aparnew (-ntgrid:ntgrid, nakx, naky)) ; call alloc8 (c3=aparnew, v="aparnew")
       allocate (bparnew (-ntgrid:ntgrid, nakx, naky)) ; call alloc8 (c3=bparnew, v="bparnew")

       allocate (apar_ext (-ntgrid:ntgrid, nakx, naky)) ; call alloc8 (c3=apar_ext, v="apar_ext")

       if(store_eq) then
          allocate ( phi_eq (-ntgrid:ntgrid, nakx, naky)); call alloc8 (c3= phi_eq, v="phi_eq")
          allocate (apar_eq (-ntgrid:ntgrid, nakx, naky)); call alloc8 (c3=apar_eq, v="apar_eq")
          allocate (bpar_eq (-ntgrid:ntgrid, nakx, naky)); call alloc8 (c3=bpar_eq, v="bpar_eq")
       end if
    endif

    phi  = 0. ; phinew  = 0.
    apar = 0. ; aparnew = 0.
    bpar = 0. ; bparnew = 0.

    apar_ext = 0.

    if (store_eq) then
       phi_eq=0.; apar_eq=0.; bpar_eq=0.
    end if

    alloc = .false.
  end subroutine allocate_arrays

  subroutine advance (istep)
    use fields_implicit, only: advance_implicit
    implicit none
    integer, intent (in) :: istep

! Could call different algorithm here...e.g., something explicit.

    call advance_implicit (istep)

  end subroutine advance

!!!RN> moved to agk_diagnostics
!!$  subroutine phinorm (phitot)
!!$    use theta_grid, only: delthet, ntgrid
!!$    use kgrids, only: naky, nakx
!!$    use constants
!!$    implicit none
!!$    real, dimension (:,:), intent (out) :: phitot
!!$    integer :: ik, it
!!$
!!$! GS2 reported the square of this value.  Only used for rough diagnostic purposes.
!!$
!!$    ! TT: In order to avoid double-counting of the end point for per BC
!!$    ! we should sum (-ntgrid:ntgrid-1) :TT
!!$    do ik = 1, naky
!!$       do it = 1, nakx
!!$          phitot(it,ik) = 0.5/pi &
!!$               * sum( cabs(phinew(-ntgrid:ntgrid-1,it,ik)) &
!!$               &    + cabs(aparnew(-ntgrid:ntgrid-1,it,ik)) &
!!$               &    + cabs(bparnew(-ntgrid:ntgrid-1,it,ik)) ) * delthet
!!$       end do
!!$    end do
!!$
!!$  end subroutine phinorm

  subroutine reset_init

    initialized  = .false.
    phi  = 0.  ;  phinew  = 0.
    apar = 0.  ;  aparnew = 0.  ! GS2 did not have these statements!
    bpar = 0.  ;  bparnew = 0.  ! GS2 did not have these statements!

  end subroutine reset_init

end module fields
