! AstroGK INPUT FILE
!  tearing.in: tearing mode test [Fig.7]
!  beta_e = 0.3, mi/me = 100
!
! Ryusuke Numata [rnumata@umd.edu]
! 2010 Oct 21
!
! Result:
!  Look at datafiles growth_rate.dat and current_width.dat produced by
!  diagnostics program, Aux/tearing_diag. 
!  gamma [growth rate] = 3.25e-4 [code unit]
!  l [layer width] = 5.02 [code unit]
!
!  gamma*tau_A = hat(gamma)*a*sqrt(beta*mass_i)/b0
!  l/a = hat(l)/a
!  S = 2.63/nu_e*a*sqrt(beta)*z_e^2/(mass_e*sqrt(mass_i)) = 7210/nu_e
!  where a = prof_width
!
&collisions_knobs
 conserve_moments= T
 collision_model='default'
 resistivity = T
 heating = T
/
&theta_grid
 ntheta= 2
/
&parameters
 beta = 0.3
 zeff = 1.0
/
&save_knobs
 save_for_restart = T
 restart_save_many = F
/
&diagnostics
 print_line = F
 print_flux_line = T
 write_ascii = T
 write_full_moments_r = T
 write_omega= T
 write_omavg= F
 nwrite=     100
 navg=       40
 omegatol=  -1.0e-3
 omegatinst = 500.0
/
&le_grids_knobs
 vgrid = T
 ngauss = 8
 negrid = 16
/
&kgrids
 grid_option='box'
 nx = 512
 ny = 4
 x0 = 80.
 y0 = 62.5 
/
&init_g_knobs
 adj_spec = 1,
 null_phi=T
 null_bpar=T
 b0 = 1.

 ! equlibrium B_0y * sin((delta k_x)*x)
 phiinit0= 1. ! give B_0y
 eq_type='porcelli'
 prof_width=50.

 ! perturbation
 phiinit = 1.e-5
 ikkk = 1 0 0
 ittt = 0 0 0
 ukxy_pt = ( .5,0.) (0.,0.) (0.,0.)

 chop_side = .false.
 ginit_option="recon3"
 scale = 1.0
/
&knobs
 avail_cpu_time = 1800.0
 store_eq = T
 use_Phi = T
 use_Apar = T
 use_Bpar = T
 eqzip_option = 'equilibrium'
 delt = .4e0
 nstep = 40000
/
&species_knobs
 nspec=          2
/
&species_parameters_1
 z=      1.0
 mass=   1.0
 dens=   1.0
 temp=   1.
 tprim=  0.0
 fprim=  0.0
 uprim=  0.0
 nu   =  0.0
 dens0 = 0.
 u0 = 0.
 tpar0 = 0.
 tperp0 = 0.
 type='ion'
/
&dist_fn_species_knobs_1
 fexp= 0.5
 bakdif= 0.0
/
&species_parameters_2
 z=      -1.0
 mass=   0.01
 dens=   1.0
 temp=   1.
 tprim=  0.0
 fprim=  0.0
 uprim=  0.0
 nu   =  5.e-1
 dens0 = 0.
 u0 = 1.
 tpar0 = 0.
 tperp0 = 0.
 type='electron'
/
&dist_fn_species_knobs_2
 fexp= 0.5
 bakdif= 0.0
/
&nonlinear_terms_knobs
 nonlinear_mode='on'
 cfl = 0.15
 zip = .false.
/
&reinit_knobs
 delt_adj = 2.0
 delt_minimum = 1.e-5
/
&layouts_knobs
 layout='yxles'
/
