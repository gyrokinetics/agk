!==============================================================================
! AstroGK INPUT FILE
!  kaw_dispersion.in: KAW dispersion relation test [Fig. 2]
!  beta_i = 1, T_0i/T_0e = 1, kperp*rho_i = 1
!
! Greg Howes
! 2007 JUN 26
!
! Result:
!  Look at apar2_by_mode in .out.nc file [kaw_dispersion_agk.dat], and
!  fit it with Eq. (64) to obtain the growth rate and frequency.
!  omega_r = 1.171
!  omega_i = -0.03740
!
!==============================================================================
!==============================================================================
!GENERAL PARAMETERS
&parameters
 beta = 1.0  !Reference Beta, not total beta:  beta=n_0 T_0 /( B^2 / (8 pi))
/
!==============================================================================
! PERPENDICULAR GRID SETUP	
&kgrids
 grid_option='single'    !'box' or 'single'
 akperp = 1.0
/
!==============================================================================
! PARALLEL GRID SETUP	
&theta_grid
 z0 = 1.
 ntheta = 32
 nperiod = 1
/
!==============================================================================
! PITCH ANGLE/ENERGY GRID SETUP	
&le_grids_knobs
 ngauss = 16
 negrid =32
 nesub=28
 nesuper=4
 vgrid=.true.
 vcut= 3.0  
 test = F
/
!==============================================================================
! BOUNDARY CONDITIONS: Always triply periodic (for now)
&dist_fn_knobs
 test = F   ! for debugging
/
!==============================================================================
! ALGORITHMIC CHOICES
&knobs
 use_Phi  = T
 use_Apar = T
 use_Bpar = T
 delt = 1.8e-1              !Time step
 nstep = 1500             !Maximum number of timesteps
! delt_option='check_restart'  !Use 'check_restart' to get initial timestep from 
			      ! restart file, 'default' otherwise.
/
&reinit_knobs
 delt_adj = 2.         !Adjust timestep by a factor of delt_adj.
 delt_minimum = 5.e-7  !The minimum time step is delt_minimum.
/
&layouts_knobs
 layout = 'yxles'    
/
!==============================================================================
! PHYSICS
!------------------------------------------------------------------------------
! COLLISIONS
&collisions_knobs
 conserve_moments = T
 heating = T
/
!------------------------------------------------------------------------------
! HYPERVISCOSITY
&hyper_knobs
 hyper_option='none'
/
!------------------------------------------------------------------------------
! NONLINEARITY
&nonlinear_terms_knobs
 nonlinear_mode='off'  !'on' Include nonlinear terms.
 cfl = 0.2             !The maximum delt < cfl * min(Delta_perp/v_perp)
/    
!==============================================================================
! EVOLVED SPECIES
&species_knobs
 nspec=          2    !Number of kinetic species evolved.
/
!------------------------------------------------------------------------------
! IONS
&species_parameters_1
 z     = 1.0   !Charge
 mass  = 1.0   !Mass
 dens  = 1.0   !Density	
 temp  = 1.0   !Temperature
 tprim = 0.0   !-1/T (dT/drho)
 fprim = 0.0   !-1/n (dn/drho)
 uprim = 0.0   ! ?
 nu    = 1.0E-02   !collisionality parameter
 type='ion'    !'ion' Thermal ion species 
/
&dist_fn_species_knobs_1
 fexp   = 0.5   ! Temporal implicitness parameter. Recommended value: 0.48
 bakdif = 0.    ! Spatial implicitness parameter. Recommended value: 0.05
/
!------------------------------------------------------------------------------
! ELECTRONS
&species_parameters_2
 z     = -1.0
 mass  =  5.446e-4
 dens  =  1.0
 temp  =  1.0
 tprim =  0.0
 fprim =  0.0
 uprim =  0.0
 nu    = 6.0E-03   !collisionality parameter
 type='electron'  !'electron' Thermal electron species 
/
&dist_fn_species_knobs_2
 fexp   = 0.5
 bakdif = 0.
/
!==============================================================================
! INITIAL CONDITIONS
&init_g_knobs
 restart_file = "nc/wg1.nc"   !Base of filenames with restart data.
 ginit_option="many"          !"many" for nl run restarts, "zero" for driven 
 ginit_option= "zero"             
/
!==============================================================================
! DRIVING SOURCES  (non-antenna sources)
&source_knobs
/
&driver
 amplitude = 20.       !Amplitude of Langevin antenna.
 w_antenna = (0.8647, -0.) !Frequency of Langevin antenna.
 nk_stir = 1             !Number of independent Fourier modes driven by antenna.
 restarting=.false.
/
&stir_1
 kx = 1
 ky = 0
 kz = 1
/
&stir_2
 kx = 0
 ky = 1
 kz = 1
/
&stir_3
 kx = -1
 ky = 0
 kz = 1
/
&stir_4
 kx = 1
 ky = 0
 kz = -1
/
&stir_5
 kx = 0
 ky = 1
 kz = -1
/
&stir_6
 kx = -1
 ky = 0
 kz = -1
/
!==============================================================================
! DIAGNOSTICS
&diagnostics
 print_line   = F                  ! to screen, linear run
 write_linear = T                  ! to file, linear run

 print_flux_line = F               ! to screen, nonlinear run
 write_nonlin    = T               ! to file, nonlinear run
 write_nl_flux = T	           ! to file, nl fluxes                   
            
 write_Epolar = F                  ! write polar kperp spectra
 write_hrate = T                   ! write heating info
 write_final_epar = .true.         ! write (linear) E_parallel
                                
 write_ascii = T
 write_lorentzian = F 
 write_kpar = T            
			   	
 write_omega=.false.            
 write_omavg=.false.            
								
 write_g = F
 write_final_fields=.true.      
				
 nwrite=     5
 navg=       1                  

 omegatol=  -1.0e-3             
 omegatinst = 500.0             

 save_for_restart = .false.     
/
!==============================================================================
!==============================================================================
