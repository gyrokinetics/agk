!==============================================================================
! AstroGK INPUT FILE
!  slow-mode.in: slow mode test [Fig.6]
!
! Michael Barnes
! 2010 Oct 22
!
! Result:
!  Look at bpar2_by_mode in .out.nc file [slow-mode_agk.dat], and
!  fit it with exp(-2*omega_i*t) to obtain the decay rate.
!  omega_i = 0.00564 
!  (note that bpar squared decays with 2*omega_i)
!
!  kpar*lambda_mfp = 1/z0*sqrt(temp_i/mass_i)/nu_i
!  omega_i/(kpar*v_A) = hat(omega_i)*z0*sqrt(beta*dens_i*mass_i)
! 
!==============================================================================

&parameters
 beta = 100.0  !Reference Beta, not total beta:  beta=n_0 T_0 /( B^2 / (8 pi))
 zeff = 1.0
/

&kgrids
 grid_option='single'
 akperp = 1.0e-5
/

&theta_grid
 ntheta= 32
 z0 = 1.0
/

&le_grids_knobs
 ngauss = 16    !# of untrapped pitch-angles moving in one direction along 
 negrid = 64
 vcut= 3.0      ! ecut=6.0 used to be used
 vgrid = T
/

&dist_fn_knobs
 adiabatic_option="default"
/

&knobs
 use_Phi = .false.
 use_Apar = .false.
 use_Bpar = .true.

 delt = 1.0
 nstep = 5000
/

&reinit_knobs
 delt_adj = 2.
 delt_minimum = 5.e-7
/

&layots_knobs
 layout = 'yxles'
/

&collisions_knobs
 collision_model='default'
 resistivity = T
 heating = T
/

&nonlinear_terms_knobs
 nonlinear_mode='off'
 cfl = 0.2
/    

&species_knobs
 nspec = 1
/

! IONS
&species_parameters_1
 z=      1.0
 mass=   1.0
 dens=   1.0
 temp=   1.0
 tprim=  0.0
 fprim=  0.0
 uprim=  0.0
 nu =  1.0e-3
 type = 'ion'
/
&dist_fn_species_knobs_1
 fexp = 0.5   !Temporal implicitness parameter. Recommended value: 0.48
 bakdif= 0.0   !Spatial implicitness parameter. Recommended value: 0.05
/

&init_g_knobs
 refac = 1.
 imfac = 1.
 den0 = 0.
 den1 = 1.
 upar1 = 0.
 tperp1 = 1.
 chop_side = F
 phiinit=   1.e-1
 restart_file = "nc/sm02.nc"
 ginit_option = "kpar"
/

! DIAGNOSTICS
&diagnostics
 print_flux_line = F
 write_nl_flux = F
 print_line = T
 nwrite=     50
 navg=       50
 omegatol=  -1.0e-3
 omegatinst = 500.0
 save_for_restart = F
/
