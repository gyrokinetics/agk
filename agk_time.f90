module agk_time

  implicit none

  private

! TT: For variable step AB3, I've made dtime an 1D array of size 3
! dtime(1) is the newest time step so changed dtime into dtime(1) everywhere
! except for init_delt where I set initial dtime for all three components
! dtime(2:3) = 0.0 might also work
!  real :: dtime
  real, dimension (3) :: dtime
! <TT
  real :: tstart
  real :: dt_cfl = -1.

  real :: dt_max, dt_min

! GGH points out that this initialization is not necessary (we think)
  real :: time = 0.

  public :: dtime, time, dt_max, dt_min, dt_cfl
  public :: save_dt_min, save_dt, save_dt_cfl, write_dt
  public :: init_tstart, init_delt, update_time
  
contains

  subroutine init_tstart (tstart)

    real, intent (in) :: tstart

    time = tstart

  end subroutine init_tstart

  subroutine init_delt (delt)
    real, intent (in) :: delt

! TT: This is called only from run_parameters so that I set for all three.
!    dtime = delt
    dtime(:) = delt
! <TT

  end subroutine init_delt

  subroutine update_time

    time = time + dtime(1)

  end subroutine update_time

  subroutine save_dt_cfl (delt_cfl)

    real, intent (in) :: delt_cfl

    dt_cfl = delt_cfl

  end subroutine save_dt_cfl

  subroutine save_dt_min (dt_min_in)

    real, intent (in) :: dt_min_in

    dt_min = dt_min_in

  end subroutine save_dt_min

  subroutine save_dt(delt)
    
    real, intent (in) :: delt

    dtime(1) = delt
    
  end subroutine save_dt

  subroutine write_dt

    if (dt_cfl > 0. .and. dt_cfl < 1.e7) write(*,*) dt_cfl,' : ',dtime(1)
       
  end subroutine write_dt

end module agk_time
