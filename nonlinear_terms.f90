module nonlinear_terms

  implicit none

  public :: init_nonlinear_terms
  public :: add_nonlinear_terms
  public :: finish_init, reset_init, nonlin

  private

  ! knobs
  integer :: nonlinear_mode_switch

  integer, parameter :: nonlinear_mode_none = 1, nonlinear_mode_on = 2

  !complex, dimension(:,:), allocatable :: phi_avg, apar_avg, bpar_avg  

  real, save, dimension (:,:), allocatable :: ba, gb, bracket
  ! yxf_lo%ny, yxf_lo%llim_proc:yxf_lo%ulim_alloc

  real, save, dimension (:,:,:), allocatable :: aba, agb, abracket
  ! 2*ntgrid+1, 2, accelx_lo%llim_proc:accelx_lo%ulim_alloc

  !complex, dimension (:,:), allocatable :: xax, xbx, g_xf
  ! xxf_lo%nx, xxf_lo%llim_proc:xxf_lo%ulim_alloc

! CFL coefficients
  real :: cfl, cflx, cfly

  logical :: nonlin = .false.
  logical :: initialized = .false.
  logical :: initializing = .true.
  logical :: alloc = .true.
  logical :: zip = .false.
  logical :: accelerated = .false.
  
contains
  
  subroutine init_nonlinear_terms 
    use theta_grid, only: init_theta_grid, ntgrid
    use kgrids, only: init_kgrids, naky, nakx, nx, ny, akx, aky
    use le_grids, only: init_le_grids, nlambda, negrid
    use species, only: init_species, nspec
    use agk_layouts, only: init_dist_fn_layouts, yxf_lo, accelx_lo
    use agk_layouts, only: init_agk_layouts
    use agk_transforms, only: init_transforms
    use agk_mem, only: alloc8
    implicit none
    logical :: dum1, dum2

    if (initialized) return
    initialized = .true.
    
    call init_agk_layouts
    call init_theta_grid
    call init_kgrids
    call init_le_grids (dum1, dum2)
    call init_species
    call init_dist_fn_layouts (ntgrid, naky, nakx, nlambda, negrid, nspec)

    call read_parameters

    if (nonlinear_mode_switch == nonlinear_mode_on)  then
       nonlin = .true.
    end if

    if (nonlinear_mode_switch /= nonlinear_mode_none) then
       call init_transforms (ntgrid, naky, nakx, nlambda, negrid, nspec, nx, ny, accelerated)

       if (alloc) then
          if (accelerated) then
             allocate (     aba(2*ntgrid+1, 2, accelx_lo%llim_proc:accelx_lo%ulim_alloc)) ; call alloc8 (r3=aba, v="aba")
             allocate (     agb(2*ntgrid+1, 2, accelx_lo%llim_proc:accelx_lo%ulim_alloc)) ; call alloc8 (r3=agb, v="agb")
             allocate (abracket(2*ntgrid+1, 2, accelx_lo%llim_proc:accelx_lo%ulim_alloc)) ; call alloc8 (r3=abracket, v="abracket")
             aba = 0. ; agb = 0. ; abracket = 0.
          else
             allocate (     ba(yxf_lo%ny,yxf_lo%llim_proc:yxf_lo%ulim_alloc)) ; call alloc8 (r2=ba, v="ba")
             allocate (     gb(yxf_lo%ny,yxf_lo%llim_proc:yxf_lo%ulim_alloc)) ; call alloc8 (r2=gb, v="gb")
             allocate (bracket(yxf_lo%ny,yxf_lo%llim_proc:yxf_lo%ulim_alloc)) ; call alloc8 (r2=bracket, v="bracket")
             ba = 0. ; gb = 0. ; bracket = 0.
          end if
          alloc = .false.
       end if

       cfly = aky(naky)/cfl*0.5
       cflx = akx((nakx+1)/2)/cfl*0.5
    end if

  end subroutine init_nonlinear_terms

  subroutine read_parameters
    use file_utils, only: input_unit_exist, error_unit
    use text_options, only: text_option, get_option_value
    use mp, only: proc0, broadcast, mp_abort
    implicit none
    type (text_option), dimension (4), parameter :: nonlinearopts = &
         (/ text_option('default', nonlinear_mode_none), &
            text_option('none', nonlinear_mode_none), &
            text_option('off', nonlinear_mode_none), &
            text_option('on', nonlinear_mode_on) /)
    character(20) :: nonlinear_mode
    namelist /nonlinear_terms_knobs/ nonlinear_mode, cfl, zip
    integer :: ierr, in_file
    logical :: exist
    logical :: done = .false.
    integer :: ireaderr=0
    logical :: abort_readerr=.true.
    
    if (done) return
    done = .true.

    if (proc0) then
       nonlinear_mode = 'default'
       cfl = 0.1

       in_file=input_unit_exist("nonlinear_terms_knobs",exist)
       if(exist) read (unit=in_file,nml=nonlinear_terms_knobs, iostat=ireaderr)
       
       ierr = error_unit()
       call get_option_value &
            (nonlinear_mode, nonlinearopts, nonlinear_mode_switch, &
            ierr, "nonlinear_mode in nonlinear_terms_knobs")
    end if

    call broadcast (ireaderr)
    if (abort_readerr) then
       if (ireaderr > 0) then
          call mp_abort ('Read error at nonlinear_terms_knobs')
       else if (ireaderr < 0) then
          call mp_abort ('End of file/record occurred at nonlinear_terms_knobs')
       endif
    endif

    call broadcast (nonlinear_mode_switch)
    call broadcast (cfl)
    call broadcast (zip)

  end subroutine read_parameters

  subroutine add_nonlinear_terms (g1, g2, g3, phi, apar, bpar, istep, bd, fexp)
    use theta_grid, only: ntgrid
    use agk_layouts, only: g_lo
    use agk_time, only: save_dt_cfl
    implicit none
    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (in out) :: g1, g2, g3
    complex, dimension (-ntgrid:,:,:), intent (in) :: phi,    apar,    bpar
    real, intent (in) :: fexp
    real, intent (in) :: bd
    integer, intent (in) :: istep
    real :: dt_cfl

    select case (nonlinear_mode_switch)
    case (nonlinear_mode_none)
!!! NEED TO DO SOMETHING HERE...  BD GGH
       dt_cfl = 1.e8
       call save_dt_cfl (dt_cfl)
    case (nonlinear_mode_on)
       if (istep /= 0) call add_nl (g1, g2, g3, phi, apar, bpar, istep, bd, fexp)
    end select
  end subroutine add_nonlinear_terms

  subroutine add_nl (g1, g2, g3, phi, apar, bpar, istep, bd, fexp)
    use mp, only: max_allreduce
    use theta_grid, only: ntgrid
    use agk_layouts, only: g_lo, ik_idx, it_idx, is_idx
    use agk_layouts, only: accelx_lo, yxf_lo
    use dist_fn_arrays, only: g
    use species, only: spec
    use agk_transforms, only: transform2, inverse2
    use run_parameters, only: use_Phi, use_Apar, use_Bpar
    use kgrids, only: aky, akx
    use agk_time, only: save_dt_cfl
    use constants, only: zi
    implicit none
    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (in out) :: g1, g2, g3
    complex, dimension (-ntgrid:,:,:), intent (in) :: phi, apar, bpar
    integer, intent (in) :: istep
    real, intent (in) :: bd
    real, intent (in) :: fexp
    integer :: istep_last = 0
    integer :: i, j, k
    real :: max_vel, zero
    real :: dt_cfl

    integer :: iglo, ik, it, is, ig, ia, isgn

    if (initializing) then
       dt_cfl = 1.e8
       call save_dt_cfl (dt_cfl)
       return
    endif

    if (istep /= istep_last) then

       zero = epsilon(0.0)
       g3 = g2
       g2 = g1

       if (use_Phi) then
          call load_kx_phi
       else
          g1 = 0.
       end if

       if (use_Bpar) call load_kx_bpar
       if (use_Apar) call load_kx_apar

       if (accelerated) then
          call transform2 (g1, aba, ia)
       else
          call transform2 (g1, ba)
       end if

       if (use_Phi) then
          call load_ky_phi
       else
          g1 = 0.
       end if
       if (use_Bpar) call load_ky_bpar

! more generally, there should probably be a factor of anon...

       do iglo = g_lo%llim_proc, g_lo%ulim_proc
          ik = ik_idx(g_lo,iglo)
          is = is_idx(g_lo,iglo)
          do isgn = 1, 2
             do ig = -ntgrid, ntgrid
                g1(ig,isgn,iglo) = g1(ig,isgn,iglo)*spec(is)%zt + zi*aky(ik)*g(ig,isgn,iglo)
             end do
          end do
       end do

       if (accelerated) then
          call transform2 (g1, agb, ia)
       else
          call transform2 (g1, gb)
       end if

       if (accelerated) then
          max_vel = 0.
          do k = accelx_lo%llim_proc, accelx_lo%ulim_proc
             do j = 1, 2
                do i = 1, 2*ntgrid+1
                   abracket(i,j,k) = aba(i,j,k)*agb(i,j,k)
                   max_vel = max(max_vel, abs(aba(i,j,k)))
                end do
             end do
          end do
          max_vel = max_vel * cfly
       else
          max_vel = 0.
          do j = yxf_lo%llim_proc, yxf_lo%ulim_proc
             do i = 1, yxf_lo%ny
                bracket(i,j) = ba(i,j)*gb(i,j)
                max_vel = max(max_vel,abs(ba(i,j)))
             end do
          end do
          max_vel = max_vel*cfly
       endif

       if (use_Phi) then
          call load_ky_phi
       else
          g1 = 0.
       end if

       if (use_Bpar) call load_ky_bpar
       if (use_Apar) call load_ky_apar

       if (accelerated) then
          call transform2 (g1, aba, ia)
       else
          call transform2 (g1, ba)
       end if

       if (use_Phi) then
          call load_kx_phi
       else
          g1 = 0.
       end if

       if (use_Bpar) call load_kx_bpar

! more generally, there should probably be a factor of anon...

       do iglo = g_lo%llim_proc, g_lo%ulim_proc
          it = it_idx(g_lo,iglo)
          is = is_idx(g_lo,iglo)
          do isgn = 1, 2
             do ig = -ntgrid, ntgrid
                g1(ig,isgn,iglo) = g1(ig,isgn,iglo)*spec(is)%zt + zi*akx(it)*g(ig,isgn,iglo)
             end do
          end do
       end do

       if (accelerated) then
          call transform2 (g1, agb, ia)
       else
          call transform2 (g1, gb)
       end if

       if (accelerated) then
          do k = accelx_lo%llim_proc, accelx_lo%ulim_proc
             do j = 1, 2
                do i = 1, 2*ntgrid+1
                   abracket(i,j,k) = abracket(i,j,k) - aba(i,j,k)*agb(i,j,k)
                   max_vel = max(max_vel, abs(aba(i,j,k))*cflx)
                end do
             end do
          end do
       else
          do j = yxf_lo%llim_proc, yxf_lo%ulim_proc
             do i = 1, yxf_lo%ny
                bracket(i,j) = bracket(i,j) - ba(i,j)*gb(i,j)
                max_vel = max(max_vel,abs(ba(i,j))*cflx)
             end do
          end do
       end if

       call max_allreduce(max_vel)

       dt_cfl = 1./max_vel
       call save_dt_cfl (dt_cfl)
       
       if (accelerated) then
          call inverse2 (abracket, g1, ia)
       else
          call inverse2 (bracket, g1)
       end if
          
! factor of one-half appears elsewhere
       do iglo = g_lo%llim_proc, g_lo%ulim_proc
          do ig = -ntgrid, ntgrid-1
             g1(ig,1,iglo) = (1.+bd)*g1(ig+1,1,iglo) + (1.-bd)*g1(ig,1,iglo)
             g1(ig,2,iglo) = (1.-bd)*g1(ig+1,2,iglo) + (1.+bd)*g1(ig,2,iglo)
          end do
       end do

    endif

    if (zip) then
       do iglo = g_lo%llim_proc, g_lo%ulim_proc
          it = it_idx(g_lo,iglo)
          ik = ik_idx(g_lo,iglo)
!          if (it == 3 .or. it == nakx-1) then
!          if (it /= 1) then
          if (ik /= 1) then
!          if (ik == 2 .and. it == 1) then
             g (:,1,iglo) = 0.
             g (:,2,iglo) = 0.
             g1(:,1,iglo) = 0.
             g1(:,2,iglo) = 0.
          end if
       end do
    end if
     
    istep_last = istep

  contains

    subroutine load_kx_phi

      use dist_fn_arrays, only: aj0
      complex :: fac

      do iglo = g_lo%llim_proc, g_lo%ulim_proc
         it = it_idx(g_lo,iglo)
         ik = ik_idx(g_lo,iglo)
         do ig = -ntgrid, ntgrid
            fac = zi*akx(it)*aj0(iglo)*phi(ig,it,ik)
            g1(ig,1,iglo) = fac
            g1(ig,2,iglo) = fac
         end do
      end do

    end subroutine load_kx_phi

    subroutine load_ky_phi

      use dist_fn_arrays, only: aj0
      complex :: fac

      do iglo = g_lo%llim_proc, g_lo%ulim_proc
         it = it_idx(g_lo,iglo)
         ik = ik_idx(g_lo,iglo)
         do ig = -ntgrid, ntgrid
            fac = zi*aky(ik)*aj0(iglo)*phi(ig,it,ik)
            g1(ig,1,iglo) = fac
            g1(ig,2,iglo) = fac
         end do
      end do

    end subroutine load_ky_phi

! should I use vpa or vpac in next two routines??

    subroutine load_kx_apar

      use dist_fn_arrays, only: vpa, aj0
      use agk_layouts, only: is_idx

      do iglo = g_lo%llim_proc, g_lo%ulim_proc
         it = it_idx(g_lo,iglo)
         ik = ik_idx(g_lo,iglo)
         is = is_idx(g_lo,iglo)
         do ig = -ntgrid, ntgrid
            g1(ig,1,iglo) = g1(ig,1,iglo) - zi*akx(it)*aj0(iglo)*spec(is)%stm &
                 *vpa(1,iglo)*apar(ig,it,ik)
         end do
         do ig = -ntgrid, ntgrid
            g1(ig,2,iglo) = g1(ig,2,iglo) - zi*akx(it)*aj0(iglo)*spec(is)%stm &
                 *vpa(2,iglo)*apar(ig,it,ik)
         end do
      end do

    end subroutine load_kx_apar

    subroutine load_ky_apar

      use dist_fn_arrays, only: vpa, aj0
      use agk_layouts, only: is_idx

      do iglo = g_lo%llim_proc, g_lo%ulim_proc
         it = it_idx(g_lo,iglo)
         ik = ik_idx(g_lo,iglo)
         is = is_idx(g_lo,iglo)
         do ig = -ntgrid, ntgrid
            g1(ig,1,iglo) = g1(ig,1,iglo) - zi*aky(ik)*aj0(iglo)*spec(is)%stm &
                 *vpa(1,iglo)*apar(ig,it,ik)
         end do
         do ig = -ntgrid, ntgrid
            g1(ig,2,iglo) = g1(ig,2,iglo) - zi*aky(ik)*aj0(iglo)*spec(is)%stm &
                 *vpa(2,iglo)*apar(ig,it,ik)
         end do
      end do

    end subroutine load_ky_apar

    subroutine load_kx_bpar

      use dist_fn_arrays, only: aj1vp2
      use agk_layouts, only: is_idx
      complex :: fac

      do iglo = g_lo%llim_proc, g_lo%ulim_proc
         it = it_idx(g_lo,iglo)
         ik = ik_idx(g_lo,iglo)
         is = is_idx(g_lo,iglo)
         do ig = -ntgrid, ntgrid
            fac = g1(ig,1,iglo) + zi*akx(it)*aj1vp2(iglo) &
                 *spec(is)%tz*bpar(ig,it,ik)
            g1(ig,1,iglo) = fac
            g1(ig,2,iglo) = fac
         end do
      end do

    end subroutine load_kx_bpar

    subroutine load_ky_bpar

      use dist_fn_arrays, only: aj1vp2
      use agk_layouts, only: is_idx
      complex :: fac

      do iglo = g_lo%llim_proc, g_lo%ulim_proc
         it = it_idx(g_lo,iglo)
         ik = ik_idx(g_lo,iglo)
         is = is_idx(g_lo,iglo)
         do ig = -ntgrid, ntgrid
            fac = g1(ig,1,iglo) + zi*aky(ik)*aj1vp2(iglo) &
                 *spec(is)%tz*bpar(ig,it,ik)
            g1(ig,1,iglo) = fac 
            g1(ig,2,iglo) = fac
         end do
      end do

    end subroutine load_ky_bpar

  end subroutine add_nl

  subroutine reset_init

    initialized = .false.
    initializing = .true.

  end subroutine reset_init

  subroutine finish_init

    initializing = .false.

  end subroutine finish_init

end module nonlinear_terms
