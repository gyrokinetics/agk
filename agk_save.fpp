# include "define.inc"
!
! TT: Concerns: In Cray NF90_FLOAT also comes as 8 byte real?
!
! TT: To do: use nf90_inquire_variable to resolve
! TT: the precision mismatch when read
!
module agk_save

# ifdef NETCDF
  use netcdf, only: NF90_FLOAT, NF90_DOUBLE, NF90_NOWRITE, NF90_NOERR
  use netcdf, only: NF90_CLOBBER, NF90_NETCDF4, NF90_MPIIO
  use netcdf, only: nf90_create, nf90_open, nf90_sync, nf90_close
  use netcdf, only: nf90_def_dim, nf90_def_var, nf90_enddef
  use netcdf, only: nf90_put_var, nf90_get_var, nf90_strerror
  use netcdf, only: nf90_inq_dimid, nf90_inquire_dimension
  use netcdf, only: nf90_inq_varid, nf90_inquire_variable
  use netcdf, only: nf90_var_par_access, NF90_COLLECTIVE

  use netcdf_utils, only: get_netcdf_code_precision
  use netcdf_utils, only: check_netcdf_file_precision
  use netcdf_utils, only: netcdf_error
  use netcdf_utils, only: netcdf_real, netcdf_int, kind_nf
# endif

  implicit none

  public :: agk_restore, agk_save_for_restart
  public :: init_agk_save
!  public :: init_save, init_dt, init_tstart, init_ant_amp
  public :: init_dt, init_tstart, init_ant_amp
!# ifdef NETCDF
!  public :: netcdf_real, kind_nf, get_netcdf_code_precision, netcdf_error
!# endif

  interface agk_restore
     module procedure agk_restore_many!, agk_restore_one
  end interface

  public :: save_for_restart, nsave
  public :: restart_file
  !!! these can be private
  !!! only used for backward compatibility reason.
  public :: get_suffix_format
  public :: restart_save_many
  public :: ndigits, separate_em_in, separate_em_out, restart_dir

  private
  logical :: save_for_restart
  integer :: nsave
  character (300) :: restart_file
  character (150) :: restart_dir
  logical :: restart_save_many

  integer :: ndigits           !Number of digits to keep in .nc.* restart filenames
  logical :: separate_em_in    !T=read EM fields from filename.nc.em
  logical :: separate_em_out   !T=write EM fields to filename.nc.em
 
# ifdef NETCDF
  real, allocatable, dimension(:,:,:) :: tmpr, tmpi, ftmpr, ftmpi
  real, allocatable, dimension(:) :: atmp
!  integer, parameter :: kind_nf = kind (NF90_NOERR)
  integer (kind_nf) :: ncid, thetaid, signid, gloid, kyid, kxid, nk_stir_dim
  integer (kind_nf) :: phir_id, phii_id, aparr_id, apari_id, bparr_id, bpari_id
  integer (kind_nf) :: delt0id, t0id, gr_id, gi_id
  integer (kind_nf) :: a_antr_id, b_antr_id, a_anti_id, b_anti_id
  integer (kind_nf) :: nseed_dim, init_seed_id, fin_seed_id

  integer (kind_nf) :: nceqid, nceqgid
  integer (kind_nf) :: thetaid_fm, thetaid_g, riid_fm, riid_g
  integer (kind_nf) :: geq_id
  integer (kind_nf) :: phi_eq_id, apar_eq_id, bpar_eq_id
!  integer (kind_nf) :: netcdf_real=0
  integer (kind_nf) :: eneid, eaparid, ebparid, dissid

  logical :: initialized = .false.
  logical :: debug = .false.
# endif

contains

  subroutine agk_save_for_restart &
       (g, t0, delt0, istatus, use_Phi, use_Apar, use_Bpar, exit_in)

# ifdef NETCDF
    use constants, only: kind_rs, kind_rd, pi
    use fields_arrays, only: phinew, aparnew, bparnew
    use kgrids, only: naky, nakx
    use antenna_data, only: nk_stir, a_ant, b_ant, ant_on, nseed, &
         init_seed, fin_seed
    use agk_heating_index, only: energy0, eapar0, ebpar0, diss1
    use agk_mem, only: alloc8, dealloc8
# endif
    use mp, only: iproc, proc0
    use mp, only: mp_comm, mp_info
    use theta_grid, only: ntgrid
! Must include g_layout_type here to avoid obscure bomb while compiling
! agk_diagnostics.f90 (which uses this module) with the Compaq F90 compiler:
    use agk_layouts, only: g_lo
    use layouts_type, only: g_layout_type
    use file_utils, only: error_unit
    implicit none
    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (in) :: g
    real, intent (in) :: t0, delt0
    logical, intent (in) :: use_Phi, use_Apar, use_Bpar
# ifdef NETCDF
    integer, intent (out) :: istatus

# else
    integer :: istatus
# endif
    logical, intent (in), optional :: exit_in
# ifdef NETCDF
    integer :: i, n_elements, ierr
    logical :: exit
    real, allocatable, dimension(:) :: r_init_seed, r_fin_seed
    integer (kind_nf) :: mode
    integer :: file_elements
    integer :: start(3), count(3)

    if (present(exit_in)) then
       exit = exit_in
    else
       exit = .false.
    end if

    n_elements = g_lo%ulim_proc-g_lo%llim_proc+1
    if (n_elements <= 0) return

    if (restart_save_many) then
       mode = NF90_CLOBBER
       file_elements=n_elements
       start = (/ 1,1,1 /)
       count = (/ 2*ntgrid+1,2, n_elements /)
    else
       mode = ior(NF90_NETCDF4, NF90_MPIIO)
       file_elements=g_lo%ulim_world+1
       start = (/ 1,1,g_lo%llim_proc+1 /)
       count = (/ 2*ntgrid+1,2, n_elements /)
    endif

    if (.not.initialized) then
       initialized = .true.

!       istatus = nf90_create (restart_file, NF90_CLOBBER, ncid)
       if (restart_save_many) then
          istatus = nf90_create (restart_file, mode, ncid)
       else
          istatus = nf90_create (restart_file, mode, ncid, &
               & comm=mp_comm, info=mp_info)
       endif
       
       if (debug) write (error_unit(),*) &
            'created netcdf file ', trim(restart_file), ' with ncid: ', ncid, &
            ' in agk_save_for_restart'

       if (istatus /= NF90_NOERR) then
          ierr = error_unit()
          write(ierr,*) "nf90_create error: ", nf90_strerror(istatus)
          goto 1
       end if

       if (n_elements > 0) then
          istatus = nf90_def_dim (ncid, "theta", 2*ntgrid+1, thetaid)
          if (istatus /= NF90_NOERR) then
             ierr = error_unit()
             write(ierr,*) "nf90_def_dim theta error: ", nf90_strerror(istatus)
             goto 1
          end if

          istatus = nf90_def_dim (ncid, "sign", 2, signid)
          if (istatus /= NF90_NOERR) then
             ierr = error_unit()
             write(ierr,*) "nf90_def_dim sign error: ", nf90_strerror(istatus)
             goto 1
          end if

!          istatus = nf90_def_dim (ncid, "glo", n_elements, gloid)
          istatus = nf90_def_dim (ncid, "glo", file_elements, gloid)
          if (istatus /= NF90_NOERR) then
             ierr = error_unit()
             write(ierr,*) "nf90_def_dim glo error: ", nf90_strerror(istatus)
             goto 1
          end if

          istatus = nf90_def_dim (ncid, "aky", naky, kyid)
          if (istatus /= NF90_NOERR) then
             ierr = error_unit()
             write(ierr,*) "nf90_def_dim aky error: ", nf90_strerror(istatus)
             goto 1
          end if

          istatus = nf90_def_dim (ncid, "akx", nakx, kxid)
          if (istatus /= NF90_NOERR) then
             ierr = error_unit()
             write(ierr,*) "nf90_def_dim akx error: ", nf90_strerror(istatus)
             goto 1
          end if
       end if

       if (netcdf_real == 0) netcdf_real = get_netcdf_code_precision()

       istatus = nf90_def_var (ncid, "t0", netcdf_real, t0id)
       if (istatus /= NF90_NOERR) then
          ierr = error_unit()
          write(ierr,*) "nf90_def_var t0 error: ", nf90_strerror(istatus)
          goto 1
       end if

       istatus = nf90_def_var (ncid, "delt0", netcdf_real, delt0id)
       if (istatus /= NF90_NOERR) then
          ierr = error_unit()
          write(ierr,*) "nf90_def_var delt0 error: ", nf90_strerror(istatus)
          goto 1
       end if
    
       if (ant_on) then
          istatus = nf90_def_dim (ncid, "nk_stir", nk_stir, nk_stir_dim)
          if (istatus /= NF90_NOERR) then
             ierr = error_unit()
             write(ierr,*) "nf90_def_dim nk_stir error: ", nf90_strerror(istatus)
             goto 1
          end if

          istatus = nf90_def_var (ncid, "a_ant_r", netcdf_real, nk_stir_dim, a_antr_id)
          if (istatus /= NF90_NOERR) then
             ierr = error_unit()
             write(ierr,*) "nf90_def_var a_ant_r error: ", nf90_strerror(istatus)
             goto 1
          end if

          istatus = nf90_def_var (ncid, "a_ant_i", netcdf_real, nk_stir_dim, a_anti_id)
          if (istatus /= NF90_NOERR) then
             ierr = error_unit()
             write(ierr,*) "nf90_def_var a_ant_i error: ", nf90_strerror(istatus)
             goto 1
          end if

          istatus = nf90_def_var (ncid, "b_ant_r", netcdf_real, nk_stir_dim, b_antr_id)
          if (istatus /= NF90_NOERR) then
             ierr = error_unit()
             write(ierr,*) "nf90_def_var b_ant_r error: ", nf90_strerror(istatus)
             goto 1
          end if

          istatus = nf90_def_var (ncid, "b_ant_i", netcdf_real, nk_stir_dim, b_anti_id)
          if (istatus /= NF90_NOERR) then
             ierr = error_unit()
             write(ierr,*) "nf90_def_var b_ant_i error: ", nf90_strerror(istatus)
             goto 1
          end if

          istatus = nf90_def_dim (ncid, "nseed", nseed, nseed_dim)
          if (istatus /= NF90_NOERR) then
             ierr = error_unit()
             write(ierr,*) "nf90_def_dim nseed error: ", nf90_strerror(istatus)
             goto 1
          end if

          istatus = nf90_def_var (ncid, "init_seed", netcdf_real, nseed_dim, init_seed_id)
          if (istatus /= NF90_NOERR) then
             ierr = error_unit()
             write(ierr,*) "nf90_def_var init_seed error: ", nf90_strerror(istatus)
             goto 1
          end if

          istatus = nf90_def_var (ncid, "fin_seed", netcdf_real, nseed_dim, fin_seed_id)
          if (istatus /= NF90_NOERR) then
             ierr = error_unit()
             write(ierr,*) "nf90_def_var fin_seed error: ", nf90_strerror(istatus)
             goto 1
          end if

       end if

       if (n_elements > 0) then
          istatus = nf90_def_var (ncid, "gr", netcdf_real, &
               (/ thetaid, signid, gloid /), gr_id)
          if (istatus /= NF90_NOERR) then
             ierr = error_unit()
             write(ierr,*) "nf90_def_var g error: ", nf90_strerror(istatus)
             goto 1
          end if

          istatus = nf90_def_var (ncid, "gi", netcdf_real, &
               (/ thetaid, signid, gloid /), gi_id)
          if (istatus /= NF90_NOERR) then
             ierr = error_unit()
             write(ierr,*) "nf90_def_var g error: ", nf90_strerror(istatus)
             goto 1
          end if

          if (use_Phi) then
             istatus = nf90_def_var (ncid, "phi_r", netcdf_real, &
                  (/ thetaid, kxid, kyid /), phir_id)
             if (istatus /= NF90_NOERR) then
                ierr = error_unit()
                write(ierr,*) "nf90_def_var phi error: ", nf90_strerror(istatus)
                goto 1
             end if

             istatus = nf90_def_var (ncid, "phi_i", netcdf_real, &
                  (/ thetaid, kxid, kyid /), phii_id)
             if (istatus /= NF90_NOERR) then
                ierr = error_unit()
                write(ierr,*) "nf90_def_var phi error: ", nf90_strerror(istatus)
                goto 1
             end if
          end if

          if (use_Apar) then
             istatus = nf90_def_var (ncid, "apar_r", netcdf_real, &
                  (/ thetaid, kxid, kyid /), aparr_id)
             if (istatus /= NF90_NOERR) then
                ierr = error_unit()
                write(ierr,*) "nf90_def_var apar error: ", nf90_strerror(istatus)
                goto 1
             end if

             istatus = nf90_def_var (ncid, "apar_i", netcdf_real, &
                  (/ thetaid, kxid, kyid /), apari_id)
             if (istatus /= NF90_NOERR) then
                ierr = error_unit()
                write(ierr,*) "nf90_def_var apar error: ", nf90_strerror(istatus)
                goto 1
             end if
          end if

          if (use_Bpar) then
             istatus = nf90_def_var (ncid, "bpar_r", netcdf_real, &
                  (/ thetaid, kxid, kyid /), bparr_id)
             if (istatus /= NF90_NOERR) then
                ierr = error_unit()
                write(ierr,*) "nf90_def_var bparr error: ", nf90_strerror(istatus)
                goto 1
             end if

             istatus = nf90_def_var (ncid, "bpar_i", netcdf_real, &
                  (/ thetaid, kxid, kyid /), bpari_id)
             if (istatus /= NF90_NOERR) then
                ierr = error_unit()
                write(ierr,*) "nf90_def_var bpari error: ", nf90_strerror(istatus)
                goto 1
             end if
          end if
       end if

       !!! RN> merged from agk_save_for_restart_energy
       istatus = nf90_def_var (ncid, "energy0", netcdf_real, eneid)
       if (istatus /= NF90_NOERR) then
          ierr = error_unit()
          write(ierr,*) "nf90_def_var energy0 error: ", nf90_strerror(istatus)
          goto 1
       end if
       istatus = nf90_def_var (ncid, "eapar0", netcdf_real, eaparid)
       if (istatus /= NF90_NOERR) then
          ierr = error_unit()
          write(ierr,*) "nf90_def_var eapar0 error: ", nf90_strerror(istatus)
          goto 1
       end if
       istatus = nf90_def_var (ncid, "ebpar0", netcdf_real, ebparid)
       if (istatus /= NF90_NOERR) then
          ierr = error_unit()
          write(ierr,*) "nf90_def_var ebpar0 error: ", nf90_strerror(istatus)
          goto 1
       end if

       istatus = nf90_def_var (ncid, "diss1", netcdf_real, dissid)
       if (istatus /= NF90_NOERR) then
          ierr = error_unit()
          write(ierr,*) "nf90_def_var diss1 error: ", nf90_strerror(istatus)
          goto 1
       end if
       !!! RN<

       istatus = nf90_enddef (ncid)
       if (istatus /= NF90_NOERR) then
          ierr = error_unit()
          write(ierr,*) "nf90_enddef error: ", nf90_strerror(istatus)
          goto 1
       end if
       if (debug) then
          ierr = error_unit()
          write (ierr,*) 'initialized became true in agk_save'
          write (ierr,*) 'ncid =', ncid
          write (ierr,*) 't0id =', t0id
       end if
    end if

    if (debug) write (error_unit(),*) 'before nf90_put_var (t0id)'
    istatus = nf90_put_var (ncid, t0id, t0)
    if (istatus /= NF90_NOERR) then
       ierr = error_unit()
       write (ierr,*) "nf90_put_var t0 error: ", nf90_strerror(istatus)
       write (ierr,*) 'ncid =', ncid
       write (ierr,*) 't0id =', t0id
       goto 1
    end if

    istatus = nf90_put_var (ncid, delt0id, delt0)
    if (istatus /= NF90_NOERR) then
       ierr = error_unit()
       write (ierr,*) "nf90_put_var delt0 error: ", nf90_strerror(istatus)
       goto 1
    end if

    if (proc0) then
       istatus = nf90_put_var (ncid, eneid, energy0)
       if (istatus /= NF90_NOERR) then
          ierr = error_unit()
          write (ierr,*) "nf90_put_var energy0 error: ", nf90_strerror(istatus)
          goto 1
       end if
       istatus = nf90_put_var (ncid, eaparid, eapar0)
       if (istatus /= NF90_NOERR) then
          ierr = error_unit()
          write (ierr,*) "nf90_put_var eapar0 error: ", nf90_strerror(istatus)
          goto 1
       end if
       istatus = nf90_put_var (ncid, ebparid, ebpar0)
       if (istatus /= NF90_NOERR) then
          ierr = error_unit()
          write (ierr,*) "nf90_put_var ebpar0 error: ", nf90_strerror(istatus)
          goto 1
       end if

       istatus = nf90_put_var (ncid, dissid, diss1)
       if (istatus /= NF90_NOERR) then
          ierr = error_unit()
          write (ierr,*) "nf90_put_var diss1 error: ", nf90_strerror(istatus)
          goto 1
       end if
    end if

1   continue
    if (istatus /= NF90_NOERR) then
       i = nf90_close (ncid)
       if (debug) write (ierr,*) 'close file and return from save_for_restart'
       return
    end if

    if (n_elements > 0) then

       if (ant_on) then

          if (.not. allocated(atmp)) then
             allocate (atmp(nk_stir)); call alloc8(r1=atmp,v="atmp")
          end if
          atmp = real(a_ant)
          istatus = nf90_put_var (ncid, a_antr_id, atmp)

          if (istatus /= NF90_NOERR) then
             ierr = error_unit()
             write (ierr,*) "nf90_put_var a_antr error: ", &
                  nf90_strerror(istatus), ' ', iproc
          end if

          atmp = aimag(a_ant)
          istatus = nf90_put_var (ncid, a_anti_id, atmp)

          if (istatus /= NF90_NOERR) then
             ierr = error_unit()
             write (ierr,*) "nf90_put_var a_anti error: ", &
                  nf90_strerror(istatus), ' ', iproc
          end if

          atmp = real(b_ant)
          istatus = nf90_put_var (ncid, b_antr_id, atmp)
          if (istatus /= NF90_NOERR) then
             ierr = error_unit()
             write (ierr,*) "nf90_put_var b_antr error: ", &
                  nf90_strerror(istatus), ' ', iproc
          end if

          atmp = aimag(b_ant)
          istatus = nf90_put_var (ncid, b_anti_id, atmp)
          if (istatus /= NF90_NOERR) then
             ierr = error_unit()
             write (ierr,*) "nf90_put_var b_anti error: ", &
                  nf90_strerror(istatus), ' ', iproc
          end if
          !Write initial random number seed used for this run (integer vector)
          !NOTE: Convert from integer to real to save init_seed
          allocate(r_init_seed(1:nseed)); r_init_seed=0.
          r_init_seed(:)=real(init_seed(:))
          istatus = nf90_put_var (ncid, init_seed_id, r_init_seed)
          if (istatus /= NF90_NOERR) then
             ierr = error_unit()
             write (ierr,*) "nf90_put_var init_seed error: ", &
                  nf90_strerror(istatus), ' ', iproc
          end if
          !Write final random number seed used for this run (integer vector)
          !NOTE: Convert from integer to real to save init_seed
          allocate(r_fin_seed(1:nseed)); r_fin_seed=0.
          r_fin_seed(:)=real(fin_seed(:))
          istatus = nf90_put_var (ncid, fin_seed_id, r_fin_seed)
          if (istatus /= NF90_NOERR) then
             ierr = error_unit()
             write (ierr,*) "nf90_put_var fin_seed error: ", &
                  nf90_strerror(istatus), ' ', iproc
          end if
!          deallocate (atmp)
          call dealloc8(r1=atmp,v="atmp")
          
          deallocate(r_init_seed,r_fin_seed)
       end if

       allocate (tmpr(2*ntgrid+1, 2, g_lo%llim_proc:g_lo%ulim_alloc)) ; call alloc8 (r3=tmpr, v="tmpr")

       if (.not.restart_save_many) then
          istatus = nf90_var_par_access(ncid, gr_id, NF90_COLLECTIVE)
          if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, gr_id)
          istatus = nf90_var_par_access(ncid, gi_id, NF90_COLLECTIVE)
          if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, gi_id)
       endif

       tmpr = real(g)
       istatus = nf90_put_var (ncid, gr_id, tmpr, start=start, count=count)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, gr_id)

       tmpr = aimag(g)
       istatus = nf90_put_var (ncid, gi_id, tmpr, start=start, count=count)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, gi_id)

       call dealloc8 (r3=tmpr, v="tmpr") 

       allocate (ftmpr (2*ntgrid+1, nakx, naky)) ; call alloc8 (r3=ftmpr, v="ftmpr")
       allocate (ftmpi (2*ntgrid+1, nakx, naky)) ; call alloc8 (r3=ftmpi, v="ftmpi")
       
       ! writing field data into single file with multi processors may take longer time
       if (use_Phi .and. proc0) then
          ftmpr = real(phinew)
          istatus = nf90_put_var (ncid, phir_id, ftmpr)
          if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, phir_id)

          ftmpi = aimag(phinew)
          istatus = nf90_put_var (ncid, phii_id, ftmpi)
          if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, phii_id)
       end if

       if (use_Apar .and. proc0) then
          ftmpr = real(aparnew)
          istatus = nf90_put_var (ncid, aparr_id, ftmpr)
          if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, aparr_id)

          ftmpi = aimag(aparnew)
          istatus = nf90_put_var (ncid, apari_id, ftmpi)
          if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, apari_id)
       end if

       if (use_Bpar .and. proc0) then
          ftmpr = real(bparnew)
          istatus = nf90_put_var (ncid, bparr_id, ftmpr)
          if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, bparr_id)

          ftmpi = aimag(bparnew)
          istatus = nf90_put_var (ncid, bpari_id, ftmpi)
          if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, bpari_id)
       end if

       call dealloc8 (r3=ftmpr, v="ftmpr") 
       call dealloc8 (r3=ftmpi, v="ftmpi")

    end if

    if (exit) then
       istatus = nf90_close (ncid)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid)
       initialized=.false.
    else
       istatus = nf90_sync (ncid)
       if (istatus /= NF90_NOERR) &
            call netcdf_error (istatus, message='nf90_sync error')
    end if

# else /* not defined NETCDF */

    if (proc0) write (error_unit(),*) &
         'WARNING: agk_save_for_restart is called without netcdf library'

# endif /* ifdef NETCDF */

  end subroutine agk_save_for_restart

  subroutine agk_restore_many (g, scale, istatus, use_Phi, use_Apar, use_Bpar, many)
# ifdef NETCDF
    use mp, only: iproc
    use mp, only: mp_comm, mp_info
    use mp, only: mp_abort, finish_mp, proc0
    use mp, only: broadcast
    use fields_arrays, only: phinew, aparnew, bparnew
    use fields_arrays, only: phi, apar, bpar
    use kgrids, only: naky, nakx
    use agk_heating_index, only: energy0, eapar0, ebpar0, diss1
    use agk_mem, only: alloc8, dealloc8
# endif
    use theta_grid, only: ntgrid
    use agk_layouts, only: g_lo
    use file_utils, only: error_unit
    implicit none
    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (out) :: g
    real, intent (in) :: scale
# ifdef NETCDF
    integer, intent (out) :: istatus
# else
    integer :: istatus
# endif
    logical, intent (in) :: use_Phi, use_Apar, use_Bpar
    logical, intent (in) :: many
# ifdef NETCDF
    integer :: n_elements
    integer :: i, ierr
    real :: fac
    integer (kind_nf) :: mode
    integer :: file_elements
    integer :: start(3), count(3)
    real :: sum_g, sum_phi, sum_apar, sum_bpar

    n_elements = g_lo%ulim_proc-g_lo%llim_proc+1
    if (n_elements <= 0) return

    if (restart_save_many) then
       mode = NF90_NOWRITE
       file_elements=n_elements
       start = (/ 1,1,1 /)
       count = (/ 2*ntgrid+1,2, n_elements /)
    else
       mode = ior(NF90_NOWRITE, NF90_MPIIO)
       file_elements=g_lo%ulim_world+1
       start = (/ 1,1,g_lo%llim_proc+1 /)
       count = (/ 2*ntgrid+1,2, n_elements /)
    endif

    if (.not.initialized) then

       ! file open
!       istatus = nf90_open (restart_file, NF90_NOWRITE, ncid)
       if (restart_save_many) then
          istatus = nf90_open (restart_file, mode, ncid)
       else
          istatus = nf90_open (restart_file, mode, ncid, &
               comm=mp_comm, info=mp_info)
       endif

       if (debug) write (error_unit(),*) &
            'opened netcdf file ', trim(restart_file), ' with ncid: ', ncid, &
            ' in agk_restore_many'
       if (istatus /= NF90_NOERR) &
            call netcdf_error (istatus, file=restart_file, abort=.true.)
       
       if (netcdf_real == 0) netcdf_real = get_netcdf_code_precision()
       call check_netcdf_file_precision (ncid)
       
       ! inquire dimension id
       istatus = nf90_inq_dimid (ncid, "theta", thetaid)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, dim='theta')
       
       istatus = nf90_inq_dimid (ncid, "sign", signid)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, dim='sign')
       
       istatus = nf90_inq_dimid (ncid, "glo", gloid)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, dim='glo')
       
       istatus = nf90_inq_dimid (ncid, "aky", kyid)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, dim='aky')
       
       istatus = nf90_inq_dimid (ncid, "akx", kxid)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, dim='akx')
       
       ! inquire dimension size
       istatus = nf90_inquire_dimension (ncid, thetaid, len=i)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, dimid=thetaid)
       if (i /= 2*ntgrid + 1) write(*,*) 'Restart error: ntgrid=? ',i,' : ',ntgrid,' : ',iproc
       
       istatus = nf90_inquire_dimension (ncid, signid, len=i)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, dimid=signid)
       if (i /= 2) write(*,*) 'Restart error: sign=? ',i,' : ',iproc
       
       istatus = nf90_inquire_dimension (ncid, gloid, len=i)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, dimid=gloid)
!       if (i /= g_lo%ulim_proc-g_lo%llim_proc+1) write(*,*) 'Restart error: glo=? ',i,' : ',iproc
       if (i /= file_elements) write(*,*) 'Restart error: glo=? ',i,' : ',iproc
       
       istatus = nf90_inquire_dimension (ncid, kyid, len=i)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, dimid=kyid)
       if (i /= naky) write(*,*) 'Restart error: naky=? ',i,' : ',naky,' : ',iproc
       
       istatus = nf90_inquire_dimension (ncid, kxid, len=i)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, dimid=kxid)
       if (i /= nakx) write(*,*) 'Restart error: nakx=? ',i,' : ',nakx,' : ',iproc
       
       ! inquire variable id
       if (use_Phi) then
          istatus = nf90_inq_varid (ncid, "phi_r", phir_id)
          if (istatus /= NF90_NOERR) call netcdf_error (istatus, var='phi_r')
          
          istatus = nf90_inq_varid (ncid, "phi_i", phii_id)
          if (istatus /= NF90_NOERR) call netcdf_error (istatus, var='phi_i')
       end if
       
       if (use_Apar) then
          istatus = nf90_inq_varid (ncid, "apar_r", aparr_id)
          if (istatus /= NF90_NOERR) call netcdf_error (istatus, var='apar_r')
          
          istatus = nf90_inq_varid (ncid, "apar_i", apari_id)
          if (istatus /= NF90_NOERR) call netcdf_error (istatus, var='apar_i')
       end if
       
       if (use_Bpar) then
          istatus = nf90_inq_varid (ncid, "bpar_r", bparr_id)
          if (istatus /= NF90_NOERR) call netcdf_error (istatus, var='bpar_r')
          
          istatus = nf90_inq_varid (ncid, "bpar_i", bpari_id)
          if (istatus /= NF90_NOERR) call netcdf_error (istatus, var='bpar_i')
       end if
       
       istatus = nf90_inq_varid (ncid, "gr", gr_id)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, var='gr')
       
       istatus = nf90_inq_varid (ncid, "gi", gi_id)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, var='gi')
       
    end if  ! if (.not.initialized)
    
    ! read variables
    allocate (tmpr (2*ntgrid+1, 2, g_lo%llim_proc:g_lo%ulim_alloc)) ; call alloc8 (r3=tmpr, v="tmpr")
    allocate (tmpi (2*ntgrid+1, 2, g_lo%llim_proc:g_lo%ulim_alloc)) ; call alloc8 (r3=tmpi, v="tmpi")

    if (.not.restart_save_many) then
       istatus = nf90_var_par_access(ncid, gr_id, NF90_COLLECTIVE)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, gr_id)
       istatus = nf90_var_par_access(ncid, gi_id, NF90_COLLECTIVE)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, gi_id)
    endif

    tmpr = 0.; tmpi = 0.
    istatus = nf90_get_var (ncid, gr_id, tmpr, start=start, count=count)
    if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, gr_id)

    istatus = nf90_get_var (ncid, gi_id, tmpi, start=start, count=count)
    if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, gi_id)

    g = cmplx(tmpr, tmpi)
    
    call dealloc8 (r3=tmpr, v="tmpr") 
    call dealloc8 (r3=tmpi, v="tmpi") 

    allocate (ftmpr (2*ntgrid+1, nakx, naky)) ; call alloc8 (r3=ftmpr, v="ftmpr")
    allocate (ftmpi (2*ntgrid+1, nakx, naky)) ; call alloc8 (r3=ftmpi, v="ftmpi")

    if (use_Phi .and. proc0) then
       istatus = nf90_get_var (ncid, phir_id, ftmpr)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, phir_id)

       istatus = nf90_get_var (ncid, phii_id, ftmpi)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, phii_id)

       phinew = cmplx(ftmpr, ftmpi)
    end if
    call broadcast(phinew)

    if (use_Apar .and. proc0) then
       istatus = nf90_get_var (ncid, aparr_id, ftmpr)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, aparr_id)
       
       istatus = nf90_get_var (ncid, apari_id, ftmpi)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, apari_id)

       aparnew = cmplx(ftmpr, ftmpi)
    end if
    call broadcast(aparnew)

    if (use_Bpar .and. proc0) then
       istatus = nf90_get_var (ncid, bparr_id, ftmpr)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, bparr_id)
       
       istatus = nf90_get_var (ncid, bpari_id, ftmpi)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, bpari_id)
       
       bparnew = cmplx(ftmpr, ftmpi)
    end if
    call broadcast(bparnew)

    call dealloc8 (r3=ftmpr, v="ftmpr") 
    call dealloc8 (r3=ftmpi, v="ftmpi") 
       
    if (scale > 0.) then
       g = g*scale
       phinew = phinew*scale
       aparnew = aparnew*scale
       bparnew = bparnew*scale
    else
       fac = - scale/(maxval(abs(phinew)))
       g = g*fac
       phinew = phinew*fac
       aparnew = aparnew*fac
       bparnew = bparnew*fac
    end if

    phi = phinew; apar = aparnew; bpar = bparnew

    !!! RN> merged from agk_restore_energy
    istatus = nf90_inq_varid (ncid, "energy0", eneid)
    if (istatus /= NF90_NOERR) then
       ierr = error_unit()
       write(ierr,*) "nf90_inq_varid energy0 error: ", nf90_strerror(istatus)
    end if
    istatus = nf90_get_var (ncid, eneid, energy0)
    if (istatus /= NF90_NOERR) then
       ierr = error_unit()
       write(ierr,*) "nf90_get_var energy0 error: ", nf90_strerror(istatus)
    end if

    istatus = nf90_inq_varid (ncid, "eapar0", eaparid)
    if (istatus /= NF90_NOERR) then
       ierr = error_unit()
       write(ierr,*) "nf90_inq_varid eapar0 error: ", nf90_strerror(istatus)
    end if
    istatus = nf90_get_var (ncid, eaparid, eapar0)
    if (istatus /= NF90_NOERR) then
       ierr = error_unit()
       write(ierr,*) "nf90_get_var eapar0 error: ", nf90_strerror(istatus)
    end if

    istatus = nf90_inq_varid (ncid, "ebpar0", ebparid)
    if (istatus /= NF90_NOERR) then
       ierr = error_unit()
       write(ierr,*) "nf90_inq_varid ebpar0 error: ", nf90_strerror(istatus)
    end if
    istatus = nf90_get_var (ncid, ebparid, ebpar0)
    if (istatus /= NF90_NOERR) then
       ierr = error_unit()
       write(ierr,*) "nf90_get_var ebpar0 error: ", nf90_strerror(istatus)
    end if

    istatus = nf90_inq_varid (ncid, "diss1", dissid)
    if (istatus /= NF90_NOERR) then
       ierr = error_unit()
       write(ierr,*) "nf90_inq_varid diss1 error: ", nf90_strerror(istatus)
    end if
    istatus = nf90_get_var (ncid, dissid, diss1)
    if (istatus /= NF90_NOERR) then
       ierr = error_unit()
       write(ierr,*) "nf90_get_var diss1 error: ", nf90_strerror(istatus)
    end if
    !!! RN<

    if (.not.initialized) then
       istatus = nf90_close (ncid)
       if (istatus /= NF90_NOERR) then
          ierr = error_unit()
          write(ierr,*) "nf90_close error: ", nf90_strerror(istatus),' ',iproc
       end if
    end if

    sum_g=sum(cabs(g(-ntgrid:ntgrid,:,g_lo%llim_proc:g_lo%ulim_proc)))
    sum_phi=sum(cabs(phinew))
    sum_apar=sum(cabs(aparnew))
    sum_bpar=sum(cabs(bparnew))
    if (sum_g+sum_phi+sum_apar+sum_bpar > 1.e10) then
       if (proc0) write(error_unit(),*) 'restart error: ',sum_g,sum_phi,sum_apar,sum_bpar
       call mp_abort('restart file')
    end if
    
# else /* not defined NETCDF */

    write (error_unit(),*) &
         'ERROR: agk_restore_many is called without netcdf'

# endif /* ifdef NETCDF */

  end subroutine agk_restore_many

  subroutine init_save (file,numdigits,sep_em_in,sep_em_out)
    character(300), intent (in) :: file
    integer, intent (in) :: numdigits         
    logical, intent (in) :: sep_em_in         
    logical, intent (in) :: sep_em_out       
    
    restart_file = file
    ndigits=numdigits
    separate_em_in=sep_em_in
    separate_em_out=sep_em_out

  end subroutine init_save

  subroutine init_dt (delt0, istatus)

# ifdef NETCDF
    use mp, only: proc0, broadcast
    use file_utils, only: error_unit
# endif
    implicit none
    real, intent (in out) :: delt0
# ifndef NETCDF
    integer :: istatus
# else
    integer, intent (out) :: istatus
    logical :: exist=.false.

    inquire(file=restart_file,exist=exist)
    if (.not.exist) then
       if (proc0) write(error_unit(),'(a)') &
            & 'Warning: restart file does not exist in init_dt!'
       istatus = -1
       return
    end if

    if (proc0) then

       if (.not. initialized) then

          istatus = nf90_open (restart_file, NF90_NOWRITE, ncid)
          if (debug) write (error_unit(),*) &
               'opened netcdf file ', trim(restart_file), ' with ncid: ', ncid
          if (istatus /= NF90_NOERR) call netcdf_error (istatus,file=restart_file)

          istatus = nf90_inq_varid (ncid, "delt0", delt0id)
          if (istatus /= NF90_NOERR) call netcdf_error (istatus, var='delt0')

       end if

       istatus = nf90_get_var (ncid, delt0id, delt0)
       if (istatus /= NF90_NOERR) then
          call netcdf_error (istatus, ncid, delt0id, message=' in init_dt')
          delt0 = -1.
       endif

       if (.not.initialized) then
          istatus = nf90_close (ncid)
       endif
    endif

    call broadcast (istatus)
    call broadcast (delt0)

# endif

  end subroutine init_dt

! This routine gets a_ant and b_ant for proc 0 only!!
  subroutine init_ant_amp (a_ant, b_ant, nk_stir, nseed, noseed, init_seed, istatus)
!
! TT: This subroutine may cause a problem with ncid because of the recent
! TT: change made in save_for_restart not closing file
!
# ifdef NETCDF
    use file_utils, only: error_unit
    use constants, only: zi
# endif
    use mp, only: proc0
    use agk_mem, only: alloc8, dealloc8
    implicit none
    complex, dimension(:), intent (in out) :: a_ant, b_ant
    integer, intent (in) :: nk_stir
    integer, intent(in) :: nseed
    logical, intent(out) :: noseed
    integer, dimension(:), intent(out) :: init_seed
    integer, intent (out) :: istatus
    real, allocatable, dimension(:) :: r_fin_seed
# ifdef NETCDF
    integer :: ierr, i

    if (proc0) then
       a_ant = 0. ; b_ant = 0.

       istatus = nf90_open (restart_file, NF90_NOWRITE, ncid)
       if (debug) write (error_unit(),*) &
            'opened netcdf file ', trim(restart_file), ' with ncid: ', ncid
       if (istatus /= NF90_NOERR) then
          ierr = error_unit()
          write(ierr,*) "nf90_open in init_ant_amp error: ", nf90_strerror(istatus) 
          write(ierr,*) "If you did not intend for this to be a restarted run with an external antenna,"
          write(ierr,*) "you may ignore the error message above."
          return
       endif

       istatus = nf90_inq_dimid (ncid, "nk_stir", nk_stir_dim)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, dim='nk_stir')

       istatus = nf90_inquire_dimension (ncid, nk_stir_dim, len=i)
       if (istatus /= NF90_NOERR) &
            call netcdf_error (istatus, ncid, dimid=nk_stir_dim)
       if (i /= nk_stir) write(error_unit(),*) 'Restart error: nk_stir=? ',i,' : ',nk_stir

       ! inquire variable id
       istatus = nf90_inq_varid (ncid, "a_ant_r", a_antr_id)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, var='a_ant_r')

       istatus = nf90_inq_varid (ncid, "a_ant_i", a_anti_id)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, var='a_ant_i')

       istatus = nf90_inq_varid (ncid, "b_ant_r", b_antr_id)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, var='b_ant_r')

       istatus = nf90_inq_varid (ncid, "b_ant_i", b_anti_id)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, var='b_ant_i')

       istatus = nf90_inq_dimid (ncid, "nseed", nseed_dim)
       if (istatus /= NF90_NOERR) then 
          noseed =.true.
          call netcdf_error (istatus, var='nseed')
       endif
       !If no seed in restart file, skip next statements
       if (.not. noseed) then
          istatus = nf90_inquire_dimension (ncid, nseed_dim, len=i)
          if (istatus /= NF90_NOERR) &
               call netcdf_error (istatus, ncid, dimid=nseed_dim)
          if (i /= nseed) then
             write(error_unit(),*) 'Restart error: nseed=? ',i,' : ',nseed
             write(error_unit(),*) 'Therefore, we choose to randomize seed'
             noseed=.true.
          endif

          istatus = nf90_inq_varid (ncid, "fin_seed", fin_seed_id)
          if (istatus /= NF90_NOERR) then 
             noseed =.true.
             call netcdf_error (istatus, var='fin_seed')
          endif
       endif

       if (.not. allocated(atmp)) then
          allocate (atmp(nk_stir)); call alloc8(r1=atmp,v="atmp")
       end if
       atmp = 0.

       ! read variables
       istatus = nf90_get_var (ncid, a_antr_id, atmp)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, a_antr_id)
       a_ant = atmp

       istatus = nf90_get_var (ncid, a_anti_id, atmp)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, a_anti_id)
       a_ant = a_ant + zi * atmp

       istatus = nf90_get_var (ncid, b_antr_id, atmp)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, b_antr_id)
       b_ant = atmp

       istatus = nf90_get_var (ncid, b_anti_id, atmp)
       if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, b_anti_id)
       b_ant = b_ant + zi * atmp

       if (.not. noseed) then
          !Read final seed from last run directly into init_seed
          !NOTE: Convert from saved real init_seed  to integer 
          allocate(r_fin_seed(1:nseed)); r_fin_seed=0.
          istatus = nf90_get_var (ncid, fin_seed_id, r_fin_seed)
          if (istatus /= NF90_NOERR) call netcdf_error (istatus, ncid, fin_seed_id)
          init_seed(:)=nint(r_fin_seed(:))
          deallocate(r_fin_seed)
       endif

!       deallocate (atmp)
       call dealloc8(r1=atmp,v="atmp")
       istatus = nf90_close (ncid)       
    endif

# else

    if (proc0) istatus = 2

# endif

  end subroutine init_ant_amp

  subroutine init_tstart (tstart, istatus)

# ifdef NETCDF
    use mp, only: proc0, broadcast
    use file_utils, only: error_unit
# endif
    implicit none
    real, intent (in out) :: tstart
# ifndef NETCDF
    integer :: istatus
# else
    integer, intent (out) :: istatus

    if (proc0) then

       if (.not.initialized) then

          istatus = nf90_open (restart_file, NF90_NOWRITE, ncid)
          if (debug) write (error_unit(),*) &
               'opened netcdf file ', trim(restart_file), ' with ncid: ', ncid
          if (istatus /= NF90_NOERR) call netcdf_error (istatus, file=restart_file)

          istatus = nf90_inq_varid (ncid, "t0", t0id)
          if (istatus /= NF90_NOERR) call netcdf_error (istatus, var='t0')

       end if

       istatus = nf90_get_var (ncid, t0id, tstart)
       if (istatus /= NF90_NOERR) then
          call netcdf_error (istatus, ncid, t0id, message=' in init_tstart')
          tstart = -1.
       end if

       if (.not.initialized) then
          istatus = nf90_close (ncid)
          if (debug) print *, 'ncid closed in init_tstart'
       end if
    endif

    call broadcast (istatus)
    call broadcast (tstart)

# endif

  end subroutine init_tstart

!-----------------------------------------------------------------------------
  subroutine get_suffix_format(nproc,suffix_format)
    ! Determine the optimal number of digits in netcdf restart files
    implicit none
    !Passed
    integer, intent(in) :: nproc !Number of processors for this job
    character (10), intent(out)  :: suffix_format
    !Local
    integer :: numdig            !Number of digits to use in restart filename

    !For NetCDF restart files, use the minimum number of digits to handle nproc
    !    unless ndigits has been set in init_g_knobs namelist (default is -1)
    if (nproc==1) then
       numdig=1
    else
       if (ndigits .eq. -1) then
          numdig=int(alog10(real(nproc-1)))+1
       else
          numdig=ndigits
       endif
    end if

    write(suffix_format,'(a,i1,a,i1,a)')'(a1,i',numdig,'.',numdig,')'
  end subroutine get_suffix_format

  subroutine init_agk_save
    !!! RN>
    !!! Parameters related to agk_save used to be defined in 
    !!! other places (init_g and agk_diagnostics) are redefined here.
    !!! For backward compatibility, namelist input of those parameters
    !!! given in init_init_g and diagnostics are still kept.
    !!! Not to override those input, this subroutine must be called before
    !!! init_init_g and init_agk_diagnostics.
    !!! RN<
    
    implicit none
    logical, save :: initialized = .false.

    if (initialized) return
    initialized = .true.

    call read_parameters

  end subroutine init_agk_save

  subroutine read_parameters
    use file_utils, only: input_unit, error_unit, run_name, input_unit_exist
    use mp, only: proc0, broadcast, mp_abort
    use mp, only: iproc, nproc
    implicit none
    integer :: in_file
    logical :: exist
    integer :: ireaderr=0
    logical :: abort_readerr=.true.
    character (10) :: suffix
    character (10)  :: suffix_format

    namelist /save_knobs/ save_for_restart, nsave, &
         restart_file, restart_dir, restart_save_many, &
         ndigits, separate_em_in, separate_em_out

    if (proc0) then
       save_for_restart=.false.
       nsave=-1
       restart_file=trim(run_name)//".nc"
       restart_dir=""
       restart_save_many=.true.
       ndigits=-1
       separate_em_in=.false.
       separate_em_out=.false.

       in_file = input_unit_exist("save_knobs", exist)
       if (exist) read (unit=in_file, nml=save_knobs, iostat=ireaderr)

       if (len_trim(restart_dir) /= 0) then
          if (restart_dir(len_trim(restart_dir):) /= "/") &
               restart_dir=trim(restart_dir)//"/"
       endif
       restart_file=trim(restart_dir)//trim(restart_file)

       if (.not.save_for_restart) nsave=-1
    endif

    call broadcast(ireaderr)
    if (abort_readerr) then
       if (ireaderr > 0) then
          call mp_abort ('Read error at save_knobs')
       else if (ireaderr < 0) then
          call mp_abort ('End of file/record occurred at save_knobs')
       endif
    endif

    call broadcast(save_for_restart)
    call broadcast(nsave)
    call broadcast(restart_file)
    call broadcast(restart_save_many)
    call broadcast(ndigits)
    call broadcast(separate_em_in)
    call broadcast(separate_em_out)

    if (restart_save_many) then
       call get_suffix_format(nproc,suffix_format)
       write (suffix,suffix_format) '.', iproc
       restart_file = trim(trim(restart_file)//adjustl(suffix))
    endif

  end subroutine read_parameters

end module agk_save
