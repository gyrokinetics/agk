module vars
  implicit none
  public :: phi,apar,bpar,jpar
  public :: dens,ux,uy,uz,pxx,pyy,pzz,pxy,pyz,pzx
  public :: phi_pt,apar_pt,bpar_pt,jpar_pt
  public :: dens_pt,ux_pt,uy_pt,uz_pt,pxx_pt,pyy_pt,pzz_pt,pxy_pt,pyz_pt,pzx_pt
  public :: xsheet,ysheet
  private

  complex, allocatable :: phi(:,:),apar(:,:),bpar(:,:),jpar(:,:)
  complex, allocatable :: dens(:,:,:),ux(:,:,:),uy(:,:,:),uz(:,:,:)
  complex, allocatable :: pxx(:,:,:),pyy(:,:,:),pzz(:,:,:)
  complex, allocatable :: pxy(:,:,:),pyz(:,:,:),pzx(:,:,:)
  complex, allocatable :: phi_pt(:,:),apar_pt(:,:),bpar_pt(:,:),jpar_pt(:,:)
  complex, allocatable :: dens_pt(:,:,:),ux_pt(:,:,:),uy_pt(:,:,:),uz_pt(:,:,:)
  complex, allocatable :: pxx_pt(:,:,:),pyy_pt(:,:,:),pzz_pt(:,:,:)
  complex, allocatable :: pxy_pt(:,:,:),pyz_pt(:,:,:),pzx_pt(:,:,:)
  real :: xsheet=.5, ysheet=.5

end module vars

module funcs
  implicit none
  public :: jfunc, djfunc
  private

contains

  function jfunc(x)
    use vars, only: jpar_pt, ysheet
    use constants, only: zi,pi
    use kgrids, only: akx,aky,nakx,naky,y0
    real :: jfunc
    real, intent(in) :: x
    integer :: it
    jfunc=0.
    do it=1,nakx
       jfunc=jfunc &
            & + real(2.*jpar_pt(it,1)*exp(zi*(akx(it)*x+aky(1)*ysheet))) &
            & + sum(real(jpar_pt(it,2:naky) * &
            & exp(zi*(akx(it)*x+aky(2:naky)*ysheet))))
    end do
  end function jfunc

  function djfunc(x)
    use vars, only: jpar_pt, ysheet
    use constants, only: zi,pi
    use kgrids, only: akx,aky,nakx,naky,y0
    real :: djfunc
    real, intent(in) :: x
    integer :: it
    djfunc=0.
    do it=1,nakx
       djfunc=djfunc &
            & + real(2.*zi*akx(it)*jpar_pt(it,1) &
            & * exp(zi*(akx(it)*x+aky(1)*ysheet))) &
            & + sum(real(zi*akx(it)*jpar_pt(it,2:naky) &
            & * exp(zi*(akx(it)*x+aky(2:naky)*ysheet))))
    end do
  end function djfunc

end module funcs

program tearing_diag
  use constants, only: pi
  use netcdf, only: NF90_NOWRITE, NF90_NOERR
  use netcdf, only: nf90_open
  use netcdf, only: nf90_inquire_dimension, nf90_inq_dimid
  use netcdf, only: nf90_inq_varid, nf90_get_var
  use netcdf_utils, only: kind_nf, netcdf_error
  use mp, only: init_mp, finish_mp, proc0
  use hdf_wrapper, only: hdf_init, hdf_finish
  use file_utils, only: init_file_utils, run_name, stdout=>stdout_unit
  use kgrids, only: nx, ny, kperp2
  use convert, only: r2c
  use agk_transforms, only: inverse2, transform2
  use constants, only: zi
  use kgrids, only: akx, aky
  use vars, only: phi,apar,bpar,jpar
  use vars, only: dens,ux,uy,uz,pxx,pyy,pzz,pxy,pyz,pzx
  use vars, only: phi_pt,apar_pt,bpar_pt,jpar_pt
  use vars, only: dens_pt,ux_pt,uy_pt,uz_pt,pxx_pt,pyy_pt,pzz_pt,pxy_pt,pyz_pt,pzx_pt
  use vars, only: xsheet, ysheet
  use funcs, only: jfunc, djfunc
  use fields_arrays, only: phi_eq, apar_eq, bpar_eq
  use dist_fn_arrays, only: g_eq
  use dist_fn_arrays, only: dens_eq, ux_eq, uy_eq, uz_eq
  use dist_fn_arrays, only: pxx_eq, pyy_eq, pzz_eq, pxy_eq, pyz_eq, pzx_eq
  use agk_equil_io, only: agk_restore_eq
  use fields, only: allocate_arrays
  use dist_fn, only: init_dist_fn_minimal
  use run_parameters, only: init_run_parameters, store_eq

  implicit none

  integer (kind=kind_nf) :: file_id
  integer (kind=kind_nf) :: kxdimid,kydimid
  integer (kind=kind_nf) :: kxvarid,kyvarid
  integer (kind=kind_nf) :: time_id,tdim_id
  integer (kind=kind_nf) :: nspec_id
  integer (kind=kind_nf) :: phi_id,apar_id,bpar_id
  integer (kind=kind_nf) :: dens_id, ux_id, uy_id, uz_id
  integer (kind=kind_nf) :: pxx_id, pyy_id, pzz_id, pxy_id, pyz_id, pzx_id
  integer (kind=kind_nf) :: betaid, zeffid

  integer :: nkx,nky,nnx,nny
  integer :: ispec,nspec
  integer :: itime,ntime
  integer :: i,j
  integer :: istatus
  integer :: xcut1,xcut2,xcut3,xcut4,ycut1,ycut2,ycut3,ycut4

  real, parameter :: del=1.e-3
  real :: width,width_guess,jmax_guess,xjmax,width2
  real :: time
  real :: dkx,dky,lx,ly,x,y
  real :: beta,zeff
  real, allocatable :: tmp1_ri(:,:,:), tmp2_ri(:,:,:,:)
  real, allocatable :: phi_xy(:,:),phi_pt_xy(:,:)
  real, allocatable :: apar_xy(:,:),apar_pt_xy(:,:)
  real, allocatable :: bpar_xy(:,:),bpar_pt_xy(:,:)
  real, allocatable :: jpar_xy(:,:),jpar_pt_xy(:,:)
  real, allocatable :: dens_xy(:,:,:), dens_pt_xy(:,:,:)
  real, allocatable :: ux_xy(:,:,:), ux_pt_xy(:,:,:)
  real, allocatable :: uy_xy(:,:,:), uy_pt_xy(:,:,:)
  real, allocatable :: uz_xy(:,:,:), uz_pt_xy(:,:,:)
  real, allocatable :: pxx_xy(:,:,:), pxx_pt_xy(:,:,:)
  real, allocatable :: pyy_xy(:,:,:), pyy_pt_xy(:,:,:)
  real, allocatable :: pzz_xy(:,:,:), pzz_pt_xy(:,:,:)
  real, allocatable :: pxy_xy(:,:,:), pxy_pt_xy(:,:,:)
  real, allocatable :: pyz_xy(:,:,:), pyz_pt_xy(:,:,:)
  real, allocatable :: pzx_xy(:,:,:), pzx_pt_xy(:,:,:)
  real, allocatable :: bpar_dx_xy(:,:), bpar_dy_xy(:,:)
  real, allocatable :: dens_dx_xy(:,:,:), dens_dy_xy(:,:,:)
  real, allocatable :: pxx_dx_xy(:,:,:), pxx_dy_xy(:,:,:)
  real, allocatable :: pyy_dx_xy(:,:,:), pyy_dy_xy(:,:,:)
  real, allocatable :: pzz_dx_xy(:,:,:), pzz_dy_xy(:,:,:)
  real, allocatable :: pxy_dx_xy(:,:,:), pxy_dy_xy(:,:,:)
  real, allocatable :: pyz_dx_xy(:,:,:), pyz_dy_xy(:,:,:)
  real, allocatable :: pzx_dx_xy(:,:,:), pzx_dy_xy(:,:,:)
!  real :: growth, island_width, time_old=0., apar_old=0., apar_pt_old=0., epar
  real :: growth, island_width, time_old=0., apar_pt_old=0., epar_C, epar_X
  integer :: xpoint_x, xpoint_y
  real, allocatable :: apar_old(:,:)
  real :: island_width2, apar_X
  real, allocatable :: check(:)
  real :: tiny=1.e-10
  character (len=300) :: filename
  logical :: list
  logical :: debug=.false.
  logical :: write_xyprof=.false.

  call init_mp
  call init_file_utils(list)
  call init_dist_fn_minimal
  call allocate_arrays

  call init_run_parameters

  if (debug .and. proc0) write(stdout,*) 'initialization done'

  xcut1=1; xcut2=nx/4+1; xcut3=nx/2+1; xcut4=nx/2+nx/4+1
  ycut1=1; ycut2=ny/4+1; ycut3=ny/2+1; ycut4=ny/2+ny/4+1

  filename=trim(trim(run_name)//'.out.nc')
  istatus=nf90_open(filename, NF90_NOWRITE, file_id)
  if (istatus /= NF90_NOERR) call netcdf_error (istatus, file_id)
  if(debug .and. proc0) write(stdout,*) 'DEBUG:: input file ', &
       & 'filename= ', trim(filename), &
       & ' file_id= ', file_id

  istatus = nf90_inq_dimid (file_id, "kx", kxdimid)
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, dim='kx')
  istatus = nf90_inq_dimid (file_id, "ky", kydimid)
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, dim='ky')
  istatus = nf90_inquire_dimension (file_id, kxdimid, len=nkx)
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, dimid=kxdimid)
  istatus = nf90_inquire_dimension (file_id, kydimid, len=nky)
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, dimid=kydimid)

  istatus = nf90_inq_varid (file_id, "kx", kxvarid)
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, var='kx')
  istatus = nf90_inq_varid (file_id, "ky", kyvarid)
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, var='ky')
  istatus = nf90_get_var (file_id, kxvarid, dkx, start=(/ 2 /))
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, kxvarid)
  istatus = nf90_get_var (file_id, kyvarid, dky, start=(/ 2 /))
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, kyvarid)
  lx=2.*pi/dkx; ly=2.*pi/dky
  xsheet=xsheet*lx
  ysheet=ysheet*ly
  if(debug .and. proc0) write(stdout,*) 'DEBUG:: inquire grid ', &
       & 'kxdimid= ', kxdimid, &
       & ' kydimid= ', kydimid, &
       & ' nkx =     ', nkx, &
       & ' nky =     ', nky, &
       & ' dkx =     ', dkx, &
       & ' dky =     ', dky, &
       & ' lx =     ', lx, &
       & ' ly =     ', ly

  istatus = nf90_inq_varid (file_id, "beta", betaid)
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, var='beta')
  istatus = nf90_get_var (file_id, betaid, beta)
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, betaid)
  istatus = nf90_inq_varid (file_id, "zeff", zeffid)
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, var='zeff')
  istatus = nf90_get_var (file_id, zeffid, zeff)
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, zeffid)
  if(debug .and. proc0) write(stdout,*) 'DEBUG:: inquire parameters ', &
       & 'betaid= ', betaid, &
       & ' zeffid= ', zeffid, &
       & ' beta= ', beta, &
       & ' zeff= ', zeff

  istatus = nf90_inq_varid (file_id, "nspecies", nspec_id)
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, nspec_id, var="nspecies")
  if(debug .and. proc0) write(stdout,*) 'DEBUG:: inquire variable ', &
       & 'nspec_id', nspec_id
  istatus = nf90_get_var (file_id, nspec_id, nspec)
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, nspec_id, varid=nspec_id)

  istatus = nf90_inq_varid (file_id, "t", time_id)
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, var='t')

  istatus = nf90_inq_dimid (file_id, "t", tdim_id)
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, var='t')
  istatus = nf90_inquire_dimension (file_id, tdim_id, len=ntime)
  if (istatus /= NF90_NOERR) &
       & call netcdf_error (istatus, file_id, dimid=tdim_id)
  if(debug .and. proc0) write(stdout,*) 'DEBUG:: inquire time ', &
       & 'time_id', time_id, &
       & ' ntime', ntime

  istatus = nf90_inq_varid (file_id, 'phi0', phi_id)
  if (istatus /= NF90_NOERR) call netcdf_error &
       & (istatus, file_id, var='phi0')
  if(debug .and. proc0) write(stdout,*) 'DEBUG:: inquire variable ', &
       & 'phi_id', phi_id
  istatus = nf90_inq_varid (file_id, 'apar0', apar_id)
  if (istatus /= NF90_NOERR) call netcdf_error &
       & (istatus, file_id, var='apar0')
  if(debug .and. proc0) write(stdout,*) 'DEBUG:: inquire variable ', &
       & 'apar_id', apar_id
  istatus = nf90_inq_varid (file_id, 'bpar0', bpar_id)
  if (istatus /= NF90_NOERR) call netcdf_error &
       & (istatus, file_id, var='bpar0')
  if(debug .and. proc0) write(stdout,*) 'DEBUG:: inquire variable ', &
       & 'bpar_id', bpar_id

  istatus = nf90_inq_varid (file_id, 'dens0', dens_id)
  if (istatus /= NF90_NOERR) call netcdf_error &
       & (istatus, file_id, var='dens0')
  if(debug .and. proc0) write(stdout,*) 'DEBUG:: inquire variable ', &
       & 'dens_id', dens_id
  istatus = nf90_inq_varid (file_id, 'ux0', ux_id)
  if (istatus /= NF90_NOERR) call netcdf_error &
       & (istatus, file_id, var='ux0')
  if(debug .and. proc0) write(stdout,*) 'DEBUG:: inquire variable ', &
       & 'ux_id', ux_id
  istatus = nf90_inq_varid (file_id, 'uy0', uy_id)
  if (istatus /= NF90_NOERR) call netcdf_error &
       & (istatus, file_id, var='uy0')
  if(debug .and. proc0) write(stdout,*) 'DEBUG:: inquire variable ', &
       & 'uy_id', uy_id
  istatus = nf90_inq_varid (file_id, 'uz0', uz_id)
  if (istatus /= NF90_NOERR) call netcdf_error &
       & (istatus, file_id, var='uz0')
  if(debug .and. proc0) write(stdout,*) 'DEBUG:: inquire variable ', &
       & 'uz_id', uz_id
  istatus = nf90_inq_varid (file_id, 'pxx0', pxx_id)
  if (istatus /= NF90_NOERR) call netcdf_error &
       & (istatus, file_id, var='pxx0')
  if(debug .and. proc0) write(stdout,*) 'DEBUG:: inquire variable ', &
       & 'pxx_id', pxx_id
  istatus = nf90_inq_varid (file_id, 'pyy0', pyy_id)
  if (istatus /= NF90_NOERR) call netcdf_error &
       & (istatus, file_id, var='pyy0')
  if(debug .and. proc0) write(stdout,*) 'DEBUG:: inquire variable ', &
       & 'pyy_id', pyy_id
  istatus = nf90_inq_varid (file_id, 'pzz0', pzz_id)
  if (istatus /= NF90_NOERR) call netcdf_error &
       & (istatus, file_id, var='pzz0')
  if(debug .and. proc0) write(stdout,*) 'DEBUG:: inquire variable ', &
       & 'pzz_id', pzz_id
  istatus = nf90_inq_varid (file_id, 'pxy0', pxy_id)
  if (istatus /= NF90_NOERR) call netcdf_error &
       & (istatus, file_id, var='pxy0')
  if(debug .and. proc0) write(stdout,*) 'DEBUG:: inquire variable ', &
       & 'pxy_id', pxy_id
  istatus = nf90_inq_varid (file_id, 'pyz0', pyz_id)
  if (istatus /= NF90_NOERR) call netcdf_error &
       & (istatus, file_id, var='pyz0')
  if(debug .and. proc0) write(stdout,*) 'DEBUG:: inquire variable ', &
       & 'pyz_id', pyz_id
  istatus = nf90_inq_varid (file_id, 'pzx0', pzx_id)
  if (istatus /= NF90_NOERR) call netcdf_error &
       & (istatus, file_id, var='pzx0')
  if(debug .and. proc0) write(stdout,*) 'DEBUG:: inquire variable ', &
       & 'pzx_id', pzx_id

  if(nx>nkx) then
     nnx=nx
  else
     nnx=(3*nkx+1)/2
  endif
  if(ny>nky) then
     nny=ny
  else
     nny=3*nky
  endif

  allocate(phi_xy(nx,ny),apar_xy(nx,ny),bpar_xy(nx,ny),jpar_xy(nx,ny))
  phi_xy(:,:)=0.; apar_xy(:,:)=0.; bpar_xy(:,:)=0.; jpar_xy(:,:)=0.
  allocate(phi_pt_xy(nx,ny),apar_pt_xy(nx,ny),bpar_pt_xy(nx,ny),jpar_pt_xy(nx,ny))
  phi_pt_xy(:,:)=0.; apar_pt_xy(:,:)=0.; bpar_pt_xy(:,:)=0.; jpar_pt_xy(:,:)=0.

  allocate(apar_old(nx,ny)); apar_old(:,:)=0.

  allocate(bpar_dx_xy(nx,ny),bpar_dy_xy(nx,ny))
  bpar_dx_xy=0.; bpar_dy_xy=0.

  allocate(dens_xy(nx,ny,nspec))
  allocate(ux_xy(nx,ny,nspec),uy_xy(nx,ny,nspec),uz_xy(nx,ny,nspec))
  allocate(pxx_xy(nx,ny,nspec),pyy_xy(nx,ny,nspec),pzz_xy(nx,ny,nspec))
  allocate(pxy_xy(nx,ny,nspec),pyz_xy(nx,ny,nspec),pzx_xy(nx,ny,nspec))
  dens_xy(:,:,:)=0.; ux_xy(:,:,:)=0.; uy_xy(:,:,:)=0.; uz_xy(:,:,:)=0.
  pxx_xy(:,:,:)=0.; pyy_xy(:,:,:)=0.; pzz_xy(:,:,:)=0.
  pxy_xy(:,:,:)=0.; pyz_xy(:,:,:)=0.; pzx_xy(:,:,:)=0.

  allocate(dens_pt_xy(nx,ny,nspec))
  allocate(ux_pt_xy(nx,ny,nspec),uy_pt_xy(nx,ny,nspec),uz_pt_xy(nx,ny,nspec))
  allocate(pxx_pt_xy(nx,ny,nspec),pyy_pt_xy(nx,ny,nspec),pzz_pt_xy(nx,ny,nspec))
  allocate(pxy_pt_xy(nx,ny,nspec),pyz_pt_xy(nx,ny,nspec),pzx_pt_xy(nx,ny,nspec))
  dens_pt_xy(:,:,:)=0.; ux_pt_xy(:,:,:)=0.; uy_pt_xy(:,:,:)=0.; uz_pt_xy(:,:,:)=0.
  pxx_pt_xy(:,:,:)=0.; pyy_pt_xy(:,:,:)=0.; pzz_pt_xy(:,:,:)=0.
  pxy_pt_xy(:,:,:)=0.; pyz_pt_xy(:,:,:)=0.; pzx_pt_xy(:,:,:)=0.

  allocate(pxx_dx_xy(nx,ny,nspec),pxx_dy_xy(nx,ny,nspec))
  allocate(pyy_dx_xy(nx,ny,nspec),pyy_dy_xy(nx,ny,nspec))
  allocate(pzz_dx_xy(nx,ny,nspec),pzz_dy_xy(nx,ny,nspec))
  allocate(pxy_dx_xy(nx,ny,nspec),pxy_dy_xy(nx,ny,nspec))
  allocate(pyz_dx_xy(nx,ny,nspec),pyz_dy_xy(nx,ny,nspec))
  allocate(pzx_dx_xy(nx,ny,nspec),pzx_dy_xy(nx,ny,nspec))
  pxx_dx_xy=0.; pxx_dy_xy=0.; pyy_dx_xy=0.; pyy_dy_xy=0.
  pzz_dx_xy=0.; pzz_dy_xy=0.; pxy_dx_xy=0.; pxy_dy_xy=0.
  pyz_dx_xy=0.; pyz_dy_xy=0.; pzx_dx_xy=0.; pzx_dy_xy=0.

  allocate(dens_dx_xy(nx,ny,nspec),dens_dy_xy(nx,ny,nspec))
  dens_dx_xy=0.; dens_dy_xy=0.

  if (debug .and. proc0) write(stdout,*) 'DEBUG:: set equilibrium'
  if (store_eq) then
     call hdf_init
     call agk_restore_eq(g_eq, .true., .true., .true.)
     call hdf_finish
     deallocate(g_eq)
  endif
  if (debug .and. proc0) write(stdout,*) 'DEBUG:: set equilibrium done'

  allocate(phi(nkx,nky)); phi=0.
  allocate(apar(nkx,nky)); apar=0.
  allocate(bpar(nkx,nky)); bpar=0.
  allocate(jpar(nkx,nky)); jpar=0.

  allocate(dens(nkx,nky,nspec)); dens=0.
  allocate(ux(nkx,nky,nspec)); ux=0.
  allocate(uy(nkx,nky,nspec)); uy=0.
  allocate(uz(nkx,nky,nspec)); uz=0.
  allocate(pxx(nkx,nky,nspec)); pxx=0.
  allocate(pyy(nkx,nky,nspec)); pyy=0.
  allocate(pzz(nkx,nky,nspec)); pzz=0.
  allocate(pxy(nkx,nky,nspec)); pxy=0.
  allocate(pyz(nkx,nky,nspec)); pyz=0.
  allocate(pzx(nkx,nky,nspec)); pzx=0.

  allocate(phi_pt(nkx,nky)); phi_pt=0.
  allocate(apar_pt(nkx,nky)); apar_pt=0.
  allocate(bpar_pt(nkx,nky)); bpar_pt=0.
  allocate(jpar_pt(nkx,nky)); jpar_pt=0.

  allocate(dens_pt(nkx,nky,nspec)); dens_pt=0.
  allocate(ux_pt(nkx,nky,nspec)); ux_pt=0.
  allocate(uy_pt(nkx,nky,nspec)); uy_pt=0.
  allocate(uz_pt(nkx,nky,nspec)); uz_pt=0.
  allocate(pxx_pt(nkx,nky,nspec)); pxx_pt=0.
  allocate(pyy_pt(nkx,nky,nspec)); pyy_pt=0.
  allocate(pzz_pt(nkx,nky,nspec)); pzz_pt=0.
  allocate(pxy_pt(nkx,nky,nspec)); pxy_pt=0.
  allocate(pyz_pt(nkx,nky,nspec)); pyz_pt=0.
  allocate(pzx_pt(nkx,nky,nspec)); pzx_pt=0.

  open(unit=51,file='xprof1.dat',status='unknown')
  open(unit=52,file='xprof2.dat',status='unknown')
  open(unit=53,file='xprof3.dat',status='unknown')
  open(unit=54,file='xprof4.dat',status='unknown')
  open(unit=55,file='yprof1.dat',status='unknown')
  open(unit=56,file='yprof2.dat',status='unknown')
  open(unit=57,file='yprof3.dat',status='unknown')
  open(unit=58,file='yprof4.dat',status='unknown')
  open(unit=61,file='growth_rate.dat',status='unknown')
  open(unit=71,file='current_width.dat',status='unknown')
  open(unit=81,file='xpoint.dat',status='unknown')
  
  if(debug .and. proc0) open(91,file='function.dat',status='unknown')

  write(51,'(a,f12.8)') '# x profile at y = ',ly/ny*(ycut1-1)
  write(52,'(a,f12.8)') '# x profile at y = ',ly/ny*(ycut2-1)
  write(53,'(a,f12.8)') '# x profile at y = ',ly/ny*(ycut3-1)
  write(54,'(a,f12.8)') '# x profile at y = ',ly/ny*(ycut4-1)
  write(55,'(a,f12.8)') '# y profile at x = ',lx/nx*(xcut1-1)
  write(56,'(a,f12.8)') '# y profile at x = ',lx/nx*(xcut2-1)
  write(57,'(a,f12.8)') '# y profile at x = ',lx/nx*(xcut3-1)
  write(58,'(a,f12.8)') '# y profile at x = ',lx/nx*(xcut4-1)
  write(51,'(a1,a19,49(1x,a19))') '#','1 time','2 x', &
       & '3 phi','4 apar','5 bpar','6 current', &
       & '7 phi_norm','8 apar_norm','9 bpar_norm','10 current_norm', &
       & '11 dens1', '12 ux1', '13 uy1', '14 uz1', &
       & '15 pxx1', '16 pyy1', '17 pzz1', '18 pxy1', '19 pyz1', '20 pzx1', &
       & '21 dens2', '22 ux2', '23 uy2', '24 uz2', &
       & '25 pxx2', '26 pyy2', '27 pzz2', '28 pxy2', '29 pyz2', '30 pzx2', &
       & '31 dens1_norm', '32 ux1_norm', '33 uy1_norm', '34 uz1_norm', &
       & '35 pxx1_norm', '36 pyy1_norm', '37 pzz1_norm', &
       & '38 pxy1_norm', '39 pyz1_norm', '40 pzx1_norm', &
       & '41 dens2_norm', '42 ux2_norm', '43 uy2_norm', '44 uz2_norm', &
       & '45 pxx2_norm', '46 pyy2_norm', '47 pzz2_norm', &
       & '48 pxy2_norm', '49 pyz2_norm', '50 pzx2_norm'
  write(52,'(a1,a19,49(1x,a19))') '#','1 time','2 x', &
       & '3 phi','4 apar','5 bpar','6 current', &
       & '7 phi_norm','8 apar_norm','9 bpar_norm','10 current_norm', &
       & '11 dens1', '12 ux1', '13 uy1', '14 uz1', &
       & '15 pxx1', '16 pyy1', '17 pzz1', '18 pxy1', '19 pyz1', '20 pzx1', &
       & '21 dens2', '22 ux2', '23 uy2', '24 uz2', &
       & '25 pxx2', '26 pyy2', '27 pzz2', '28 pxy2', '29 pyz2', '30 pzx2', &
       & '31 dens1_norm', '32 ux1_norm', '33 uy1_norm', '34 uz1_norm', &
       & '35 pxx1_norm', '36 pyy1_norm', '37 pzz1_norm', &
       & '38 pxy1_norm', '39 pyz1_norm', '40 pzx1_norm', &
       & '41 dens2_norm', '42 ux2_norm', '43 uy2_norm', '44 uz2_norm', &
       & '45 pxx2_norm', '46 pyy2_norm', '47 pzz2_norm', &
       & '48 pxy2_norm', '49 pyz2_norm', '50 pzx2_norm'
  write(53,'(a1,a19,49(1x,a19))') '#','1 time','2 x', &
       & '3 phi','4 apar','5 bpar','6 current', &
       & '7 phi_norm','8 apar_norm','9 bpar_norm','10 current_norm', &
       & '11 dens1', '12 ux1', '13 uy1', '14 uz1', &
       & '15 pxx1', '16 pyy1', '17 pzz1', '18 pxy1', '19 pyz1', '20 pzx1', &
       & '21 dens2', '22 ux2', '23 uy2', '24 uz2', &
       & '25 pxx2', '26 pyy2', '27 pzz2', '28 pxy2', '29 pyz2', '30 pzx2', &
       & '31 dens1_norm', '32 ux1_norm', '33 uy1_norm', '34 uz1_norm', &
       & '35 pxx1_norm', '36 pyy1_norm', '37 pzz1_norm', &
       & '38 pxy1_norm', '39 pyz1_norm', '40 pzx1_norm', &
       & '41 dens2_norm', '42 ux2_norm', '43 uy2_norm', '44 uz2_norm', &
       & '45 pxx2_norm', '46 pyy2_norm', '47 pzz2_norm', &
       & '48 pxy2_norm', '49 pyz2_norm', '50 pzx2_norm'
  write(54,'(a1,a19,49(1x,a19))') '#','1 time','2 x', &
       & '3 phi','4 apar','5 bpar','6 current', &
       & '7 phi_norm','8 apar_norm','9 bpar_norm','10 current_norm', &
       & '11 dens1', '12 ux1', '13 uy1', '14 uz1', &
       & '15 pxx1', '16 pyy1', '17 pzz1', '18 pxy1', '19 pyz1', '20 pzx1', &
       & '21 dens2', '22 ux2', '23 uy2', '24 uz2', &
       & '25 pxx2', '26 pyy2', '27 pzz2', '28 pxy2', '29 pyz2', '30 pzx2', &
       & '31 dens1_norm', '32 ux1_norm', '33 uy1_norm', '34 uz1_norm', &
       & '35 pxx1_norm', '36 pyy1_norm', '37 pzz1_norm', &
       & '38 pxy1_norm', '39 pyz1_norm', '40 pzx1_norm', &
       & '41 dens2_norm', '42 ux2_norm', '43 uy2_norm', '44 uz2_norm', &
       & '45 pxx2_norm', '46 pyy2_norm', '47 pzz2_norm', &
       & '48 pxy2_norm', '49 pyz2_norm', '50 pzx2_norm'
  write(55,'(a1,a19,49(1x,a19))') '#','1 time','2 y', &
       & '3 phi','4 apar','5 bpar','6 current', &
       & '7 phi_norm','8 apar_norm','9 bpar_norm','10 current_norm', &
       & '11 dens1', '12 ux1', '13 uy1', '14 uz1', &
       & '15 pxx1', '16 pyy1', '17 pzz1', '18 pxy1', '19 pyz1', '20 pzx1', &
       & '21 dens2', '22 ux2', '23 uy2', '24 uz2', &
       & '25 pxx2', '26 pyy2', '27 pzz2', '28 pxy2', '29 pyz2', '30 pzx2', &
       & '31 dens1_norm', '32 ux1_norm', '33 uy1_norm', '34 uz1_norm', &
       & '35 pxx1_norm', '36 pyy1_norm', '37 pzz1_norm', &
       & '38 pxy1_norm', '39 pyz1_norm', '40 pzx1_norm', &
       & '41 dens2_norm', '42 ux2_norm', '43 uy2_norm', '44 uz2_norm', &
       & '45 pxx2_norm', '46 pyy2_norm', '47 pzz2_norm', &
       & '48 pxy2_norm', '49 pyz2_norm', '50 pzx2_norm'
  write(56,'(a1,a19,49(1x,a19))') '#','1 time','2 y', &
       & '3 phi','4 apar','5 bpar','6 current', &
       & '7 phi_norm','8 apar_norm','9 bpar_norm','10 current_norm', &
       & '11 dens1', '12 ux1', '13 uy1', '14 uz1', &
       & '15 pxx1', '16 pyy1', '17 pzz1', '18 pxy1', '19 pyz1', '20 pzx1', &
       & '21 dens2', '22 ux2', '23 uy2', '24 uz2', &
       & '25 pxx2', '26 pyy2', '27 pzz2', '28 pxy2', '29 pyz2', '30 pzx2', &
       & '31 dens1_norm', '32 ux1_norm', '33 uy1_norm', '34 uz1_norm', &
       & '35 pxx1_norm', '36 pyy1_norm', '37 pzz1_norm', &
       & '38 pxy1_norm', '39 pyz1_norm', '40 pzx1_norm', &
       & '41 dens2_norm', '42 ux2_norm', '43 uy2_norm', '44 uz2_norm', &
       & '45 pxx2_norm', '46 pyy2_norm', '47 pzz2_norm', &
       & '48 pxy2_norm', '49 pyz2_norm', '50 pzx2_norm'
  write(57,'(a1,a19,49(1x,a19))') '#','1 time','2 y', &
       & '3 phi','4 apar','5 bpar','6 current', &
       & '7 phi_norm','8 apar_norm','9 bpar_norm','10 current_norm', &
       & '11 dens1', '12 ux1', '13 uy1', '14 uz1', &
       & '15 pxx1', '16 pyy1', '17 pzz1', '18 pxy1', '19 pyz1', '20 pzx1', &
       & '21 dens2', '22 ux2', '23 uy2', '24 uz2', &
       & '25 pxx2', '26 pyy2', '27 pzz2', '28 pxy2', '29 pyz2', '30 pzx2', &
       & '31 dens1_norm', '32 ux1_norm', '33 uy1_norm', '34 uz1_norm', &
       & '35 pxx1_norm', '36 pyy1_norm', '37 pzz1_norm', &
       & '38 pxy1_norm', '39 pyz1_norm', '40 pzx1_norm', &
       & '41 dens2_norm', '42 ux2_norm', '43 uy2_norm', '44 uz2_norm', &
       & '45 pxx2_norm', '46 pyy2_norm', '47 pzz2_norm', &
       & '48 pxy2_norm', '49 pyz2_norm', '50 pzx2_norm'
  write(58,'(a1,a19,49(1x,a19))') '#','1 time','2 y', &
       & '3 phi','4 apar','5 bpar','6 current', &
       & '7 phi_norm','8 apar_norm','9 bpar_norm','10 current_norm', &
       & '11 dens1', '12 ux1', '13 uy1', '14 uz1', &
       & '15 pxx1', '16 pyy1', '17 pzz1', '18 pxy1', '19 pyz1', '20 pzx1', &
       & '21 dens2', '22 ux2', '23 uy2', '24 uz2', &
       & '25 pxx2', '26 pyy2', '27 pzz2', '28 pxy2', '29 pyz2', '30 pzx2', &
       & '31 dens1_norm', '32 ux1_norm', '33 uy1_norm', '34 uz1_norm', &
       & '35 pxx1_norm', '36 pyy1_norm', '37 pzz1_norm', &
       & '38 pxy1_norm', '39 pyz1_norm', '40 pzx1_norm', &
       & '41 dens2_norm', '42 ux2_norm', '43 uy2_norm', '44 uz2_norm', &
       & '45 pxx2_norm', '46 pyy2_norm', '47 pzz2_norm', &
       & '48 pxy2_norm', '49 pyz2_norm', '50 pzx2_norm'

  if (proc0) then
     write(61,'(a1,a19,7(1x,a19))') '#','1 time','2 growth','3 island width1','4 island width 2','5 epar at C','6 epar at X','7 apar_pt at C','8 apar_pt at X'
     write(71,'(a1,a19,4(1x,a19))') '#','1 time','2 width 1','3 width 2','4 jpar_pt at C','5 jpar_pt at X'
     write(81,'(a1,a19,4(1x,a19))') '#','1 time','2 x coord','3 y coord','4 x index','5 y index'
  endif

  timeloop : do itime=1,ntime
     if(debug .and. proc0) write(stdout,*) 'time=',itime

     !
     ! time
     !

     istatus = nf90_get_var (file_id, time_id, time, start=(/ itime /))
     if (istatus /= NF90_NOERR) &
          & call netcdf_error (istatus, file_id, varid=time_id)

     if (.not.allocated(tmp1_ri)) then
        allocate(tmp1_ri(2,nkx,nky)); tmp1_ri = 0.
     endif

     ! 
     ! fields
     !
     if(debug .and. proc0) write(stdout,*) 'fields'

     ! phi
     istatus = nf90_get_var (file_id, phi_id, tmp1_ri(1:2,1:nkx,1:nky), &
          & start=(/ 1,1,1,itime /),  count=(/ 2,nkx,nky,1 /))
     if (istatus /= NF90_NOERR) &
          & call netcdf_error (istatus, file_id, phi_id)
     call r2c(phi,tmp1_ri)
     ! apar
     istatus = nf90_get_var (file_id, apar_id, tmp1_ri(1:2,1:nkx,1:nky), &
          & start=(/ 1,1,1,itime /),  count=(/ 2,nkx,nky,1 /))
     if (istatus /= NF90_NOERR) &
          & call netcdf_error (istatus, file_id, apar_id)
     call r2c(apar,tmp1_ri)
     istatus = nf90_get_var (file_id, bpar_id, tmp1_ri(1:2,1:nkx,1:nky), &
          & start=(/ 1,1,1,itime /),  count=(/ 2,nkx,nky,1 /))
     if (istatus /= NF90_NOERR) &
          & call netcdf_error (istatus, file_id, bpar_id)
     call r2c(bpar,tmp1_ri)

     if(allocated(tmp1_ri)) deallocate(tmp1_ri)

     if(debug .and. proc0) write(stdout,*) 'fields read done'

     if (store_eq) then
        phi_pt = phi - phi_eq(0,:,:)
        apar_pt = apar - apar_eq(0,:,:)
        bpar_pt = bpar - bpar_eq(0,:,:)
     endif

     jpar(:,:)=apar(:,:)*kperp2(:,:)/beta*.5
     jpar_pt(:,:)=apar_pt(:,:)*kperp2(:,:)/beta*.5

     call transform2(phi, phi_xy,nny,nnx)
     call transform2(apar,apar_xy,nny,nnx)
     call transform2(bpar,bpar_xy,nny,nnx)
     call transform2(jpar,jpar_xy,nny,nnx)
     call transform2(phi_pt, phi_pt_xy,nny,nnx)
     call transform2(apar_pt,apar_pt_xy,nny,nnx)
     call transform2(bpar_pt,bpar_pt_xy,nny,nnx)
     call transform2(jpar_pt,jpar_pt_xy,nny,nnx)

     ! derivatives of perturbed bpar
     call transform2(zi*spread(akx,2,nky)*bpar,bpar_dx_xy,nny,nnx)
     call transform2(zi*spread(aky,1,nkx)*bpar,bpar_dy_xy,nny,nnx)
     if(debug .and. proc0) write(stdout,*) 'fields transform done'

     !
     ! moments
     !
     if(debug .and. proc0) write(stdout,*) 'moments'

     if (.not.allocated(tmp2_ri)) then
        allocate(tmp2_ri(2,nkx,nky,nspec)); tmp2_ri=0.
     endif

     ! dens
     istatus = nf90_get_var (file_id, dens_id, &
          & tmp2_ri(1:2,1:nkx,1:nky,1:nspec), &
          & start=(/ 1,1,1,1,itime /),  count=(/ 2,nkx,nky,nspec,1 /))
     if (istatus /= NF90_NOERR) &
          & call netcdf_error (istatus, file_id, dens_id)
     call r2c(dens,tmp2_ri)
     ! ux
     istatus = nf90_get_var (file_id, ux_id, &
          & tmp2_ri(1:2,1:nkx,1:nky,1:nspec), &
          & start=(/ 1,1,1,1,itime /),  count=(/ 2,nkx,nky,nspec,1 /))
     if (istatus /= NF90_NOERR) &
          & call netcdf_error (istatus, file_id, ux_id)
     call r2c(ux,tmp2_ri)
     ! uy
     istatus = nf90_get_var (file_id, uy_id, &
          & tmp2_ri(1:2,1:nkx,1:nky,1:nspec), &
          & start=(/ 1,1,1,1,itime /),  count=(/ 2,nkx,nky,nspec,1 /))
     if (istatus /= NF90_NOERR) &
          & call netcdf_error (istatus, file_id, uy_id)
     call r2c(uy,tmp2_ri)
     ! uz
     istatus = nf90_get_var (file_id, uz_id, &
          & tmp2_ri(1:2,1:nkx,1:nky,1:nspec), &
          & start=(/ 1,1,1,1,itime /),  count=(/ 2,nkx,nky,nspec,1 /))
     if (istatus /= NF90_NOERR) &
          & call netcdf_error (istatus, file_id, uz_id)
     call r2c(uz,tmp2_ri)
     ! pxx
     istatus = nf90_get_var (file_id, pxx_id, &
          & tmp2_ri(1:2,1:nkx,1:nky,1:nspec), &
          & start=(/ 1,1,1,1,itime /),  count=(/ 2,nkx,nky,nspec,1 /))
     if (istatus /= NF90_NOERR) &
          & call netcdf_error (istatus, file_id, pxx_id)
     call r2c(pxx,tmp2_ri)
     ! pyy
     istatus = nf90_get_var (file_id, pyy_id, &
          & tmp2_ri(1:2,1:nkx,1:nky,1:nspec), &
          & start=(/ 1,1,1,1,itime /),  count=(/ 2,nkx,nky,nspec,1 /))
     if (istatus /= NF90_NOERR) &
          & call netcdf_error (istatus, file_id, pyy_id)
     call r2c(pyy,tmp2_ri)
     ! pzz
     istatus = nf90_get_var (file_id, pzz_id, &
          & tmp2_ri(1:2,1:nkx,1:nky,1:nspec), &
          & start=(/ 1,1,1,1,itime /),  count=(/ 2,nkx,nky,nspec,1 /))
     if (istatus /= NF90_NOERR) &
          & call netcdf_error (istatus, file_id, pzz_id)
     call r2c(pzz,tmp2_ri)
     ! pxy
     istatus = nf90_get_var (file_id, pxy_id, &
          & tmp2_ri(1:2,1:nkx,1:nky,1:nspec), &
          & start=(/ 1,1,1,1,itime /),  count=(/ 2,nkx,nky,nspec,1 /))
     if (istatus /= NF90_NOERR) &
          & call netcdf_error (istatus, file_id, pxy_id)
     call r2c(pxy,tmp2_ri)
     ! pyz
     istatus = nf90_get_var (file_id, pyz_id, &
          & tmp2_ri(1:2,1:nkx,1:nky,1:nspec), &
          & start=(/ 1,1,1,1,itime /),  count=(/ 2,nkx,nky,nspec,1 /))
     if (istatus /= NF90_NOERR) &
          & call netcdf_error (istatus, file_id, pyz_id)
     call r2c(pyz,tmp2_ri)
     ! pzx
     istatus = nf90_get_var (file_id, pzx_id, &
          & tmp2_ri(1:2,1:nkx,1:nky,1:nspec), &
          & start=(/ 1,1,1,1,itime /),  count=(/ 2,nkx,nky,nspec,1 /))
     if (istatus /= NF90_NOERR) &
          & call netcdf_error (istatus, file_id, pzx_id)
     call r2c(pzx,tmp2_ri)

     if (allocated(tmp2_ri)) deallocate(tmp2_ri)
     if(debug .and. proc0) write(stdout,*) 'moments read done'

     do ispec=1,nspec

        if (store_eq) then
           dens_pt = dens - dens_eq(0,:,:,:)
           ux_pt = ux - ux_eq(0,:,:,:)
           uy_pt = uy - uy_eq(0,:,:,:)
           uz_pt = uz - uz_eq(0,:,:,:)
           pxx_pt = pxx - pxx_eq(0,:,:,:)
           pyy_pt = pyy - pyy_eq(0,:,:,:)
           pzz_pt = pzz - pzz_eq(0,:,:,:)
           pxy_pt = pxy - pxy_eq(0,:,:,:)
           pyz_pt = pyz - pyz_eq(0,:,:,:)
           pzx_pt = pzx - pzx_eq(0,:,:,:)
        endif
        
        call transform2(dens(:,:,ispec),dens_xy(:,:,ispec),nny,nnx)
        call transform2(ux(:,:,ispec),ux_xy(:,:,ispec),nny,nnx)
        call transform2(uy(:,:,ispec),uy_xy(:,:,ispec),nny,nnx)
        call transform2(uz(:,:,ispec),uz_xy(:,:,ispec),nny,nnx)
        call transform2(pxx(:,:,ispec),pxx_xy(:,:,ispec),nny,nnx)
        call transform2(pyy(:,:,ispec),pyy_xy(:,:,ispec),nny,nnx)
        call transform2(pzz(:,:,ispec),pzz_xy(:,:,ispec),nny,nnx)
        call transform2(pxy(:,:,ispec),pxy_xy(:,:,ispec),nny,nnx)
        call transform2(pyz(:,:,ispec),pyz_xy(:,:,ispec),nny,nnx)
        call transform2(pzx(:,:,ispec),pzx_xy(:,:,ispec),nny,nnx)

        call transform2(dens_pt(:,:,ispec),dens_pt_xy(:,:,ispec),nny,nnx)
        call transform2(ux_pt(:,:,ispec),ux_pt_xy(:,:,ispec),nny,nnx)
        call transform2(uy_pt(:,:,ispec),uy_pt_xy(:,:,ispec),nny,nnx)
        call transform2(uz_pt(:,:,ispec),uz_pt_xy(:,:,ispec),nny,nnx)
        call transform2(pxx_pt(:,:,ispec),pxx_pt_xy(:,:,ispec),nny,nnx)
        call transform2(pyy_pt(:,:,ispec),pyy_pt_xy(:,:,ispec),nny,nnx)
        call transform2(pzz_pt(:,:,ispec),pzz_pt_xy(:,:,ispec),nny,nnx)
        call transform2(pxy_pt(:,:,ispec),pxy_pt_xy(:,:,ispec),nny,nnx)
        call transform2(pyz_pt(:,:,ispec),pyz_pt_xy(:,:,ispec),nny,nnx)
        call transform2(pzx_pt(:,:,ispec),pzx_pt_xy(:,:,ispec),nny,nnx)
        
        call transform2(zi*spread(akx,2,nky)*dens(:,:,ispec), &
             & dens_dx_xy(:,:,ispec),nny,nnx)
        call transform2(zi*spread(aky,1,nkx)*dens(:,:,ispec), &
             & dens_dy_xy(:,:,ispec),nny,nnx)

        call transform2(zi*spread(akx,2,nky)*pxx(:,:,ispec), &
             & pxx_dx_xy(:,:,ispec),nny,nnx)
        call transform2(zi*spread(aky,1,nkx)*pxx(:,:,ispec), &
             & pxx_dy_xy(:,:,ispec),nny,nnx)
        call transform2(zi*spread(akx,2,nky)*pyy(:,:,ispec), &
             & pyy_dx_xy(:,:,ispec),nny,nnx)
        call transform2(zi*spread(aky,1,nkx)*pyy(:,:,ispec), &
             & pyy_dy_xy(:,:,ispec),nny,nnx)
        call transform2(zi*spread(akx,2,nky)*pzz(:,:,ispec), &
             & pzz_dx_xy(:,:,ispec),nny,nnx)
        call transform2(zi*spread(aky,1,nkx)*pzz(:,:,ispec), &
             & pzz_dy_xy(:,:,ispec),nny,nnx)
        call transform2(zi*spread(akx,2,nky)*pxy(:,:,ispec), &
             & pxy_dx_xy(:,:,ispec),nny,nnx)
        call transform2(zi*spread(aky,1,nkx)*pxy(:,:,ispec), &
             & pxy_dy_xy(:,:,ispec),nny,nnx)
        call transform2(zi*spread(akx,2,nky)*pyz(:,:,ispec), &
             & pyz_dx_xy(:,:,ispec),nny,nnx)
        call transform2(zi*spread(aky,1,nkx)*pyz(:,:,ispec), &
             & pyz_dy_xy(:,:,ispec),nny,nnx)
        call transform2(zi*spread(akx,2,nky)*pzx(:,:,ispec), &
             & pzx_dx_xy(:,:,ispec),nny,nnx)
        call transform2(zi*spread(aky,1,nkx)*pzx(:,:,ispec), &
             & pzx_dy_xy(:,:,ispec),nny,nnx)
        
     end do
     if(debug .and. proc0) write(stdout,*) 'moments transform done'

!!$! curl div P 
!!$! should be zero
     allocate(check(nspec))
     do ispec=1,nspec
        check(ispec)= &
             sum(cabs((spread(aky,1,size(akx))**2-spread(akx,2,size(aky))**2)*pxy(:,:,ispec) &
             - spread(akx,2,size(aky))*spread(aky,1,size(akx))*(pyy(:,:,ispec)-pxx(:,:,ispec))))
     end do
     if (sum(check) > tiny) write(stdout,*) 'Curl Div P /= 0',check(1:nspec)
     deallocate(check)

! div P term in Ohm's law
! should be zero
     if (sum(abs(pzx_dx_xy(:,:,:)+pyz_dy_xy(:,:,:))) > tiny) &
          & write(stdout,*) 'Div P term in Ohms law /= 0', &
          & sum(abs(pzx_dx_xy(:,:,:)+pyz_dy_xy(:,:,:)))


! baroclinic vector
!!$     do i=1,nx
!!$        x=lx/nx*(i-1)
!!$        do j=1,ny
!!$           y=ly/ny*(j-1)
!!$           write(1000+itime,'(5d20.12)') x,y, &
!!$                & dens_dx_xy(i,j,1:nspec) * &
!!$                & (pxy_dx_xy(i,j,1:nspec)+pyy_dy_xy(i,j,1:nspec)) - &
!!$                & dens_dy_xy(i,j,1:nspec) * &
!!$                & (pxx_dx_xy(i,j,1:nspec)+pxy_dy_xy(i,j,1:nspec))
!!$        end do
!!$        write(1000+itime,*)
!!$     end do

! pressure balance check
!!$     do i=1,nx
!!$        x=lx/nx*(i-1)
!!$        do j=1,ny
!!$           y=ly/ny*(j-1)
!!$           write(1100+itime,'(5d20.12)') x,y, &
!!$                & bpar_dx_xy(i,j) + &
!!$                & .5*beta*sum(pxx_dx_xy(i,j,1:nspec)+pxy_dy_xy(i,j,1:nspec)), &
!!$                & bpar_dy_xy(i,j) + &
!!$                & .5*beta*sum(pxy_dx_xy(i,j,1:nspec)+pyy_dy_xy(i,j,1:nspec)), &
!!$                & sum(pzx_dx_xy(i,j,1:nspec)+pyz_dy_xy(i,j,1:nspec))
!!$        end do
!!$        write(1100+itime,*)
!!$     end do

! x-y space profile
     if (write_xyprof) then
        write(filename,'("prof_xy_t",i5.5,".dat")') itime
        open(100,file=trim(filename))
        write(100,*) '# Generated by tearing_diag with write_xyprof=T'
        write(100,*) '# Time =',time
        write(100,'(1x,a1,a18,9(1x,a19))') '#','x','y','phi','apar','bpar','jpar','dens_i','dens_e','pzz_i','pzz_e'
        do i=1,nx
           x=lx/nx*(i-1)
           do j=1,ny
              y=ly/ny*(j-1)
              write(100,'(18e20.12)') x, y, &
                   & phi_xy(i,j), apar_xy(i,j), bpar_xy(i,j), jpar_xy(i,j), &
                   & dens_xy(i,j,1:2), pzz_xy(i,j,1:2), &
                   & phi_pt_xy(i,j), apar_pt_xy(i,j), bpar_pt_xy(i,j), jpar_pt_xy(i,j), &
                   & dens_pt_xy(i,j,1:2), pzz_pt_xy(i,j,1:2)
           end do
           write(100,*)
        end do
        close(100)
     endif

     do i=1,nx
        x=lx/nx*(i-1)
        write(51,'(50d20.12)') time,x, &
             & phi_pt_xy(i,ycut1),apar_pt_xy(i,ycut1), &
             & bpar_pt_xy(i,ycut1),jpar_pt_xy(i,ycut1), &
             & phi_pt_xy(i,ycut1)/abs((phi_pt_xy(nx/2+2,ycut1)-phi_pt_xy(nx/2,ycut1))/(2.*lx/nx)), &
             & apar_pt_xy(i,ycut1)/abs(apar_pt_xy(nx/2+1,ycut1)), &
             & bpar_pt_xy(i,ycut1)/abs((bpar_pt_xy(nx/2+2,ycut1)-bpar_pt_xy(nx/2,ycut1))/(2.*lx/nx)), &
             & jpar_pt_xy(i,ycut1)/abs(jpar_pt_xy(nx/2+1,ycut1)), &
             & (dens_pt_xy(i,ycut1,ispec), &
             &  ux_pt_xy(i,ycut1,ispec),uy_pt_xy(i,ycut1,ispec),uz_pt_xy(i,ycut1,ispec), &
             &  pxx_pt_xy(i,ycut1,ispec),pyy_pt_xy(i,ycut1,ispec), &
             &  pzz_pt_xy(i,ycut1,ispec),pxy_pt_xy(i,ycut1,ispec), &
             &  pyz_pt_xy(i,ycut1,ispec),pzx_pt_xy(i,ycut1,ispec), ispec=1,nspec), &
             & (dens_pt_xy(i,ycut1,ispec)/abs((dens_pt_xy(nx/2+2,ycut1,ispec)-dens_pt_xy(nx/2,ycut1,ispec))/(2.*lx/nx)), &
             &  ux_pt_xy(i,ycut1,ispec)/abs(ux_pt_xy(nx/2+1,ycut1,ispec)), &
             &  uy_pt_xy(i,ycut1,ispec)/abs(uy_pt_xy(nx/2+1,ycut1,ispec)), &
             &  uz_pt_xy(i,ycut1,ispec)/abs(uz_pt_xy(nx/2+1,ycut1,ispec)), &
             &  pxx_pt_xy(i,ycut1,ispec)/abs(pxx_pt_xy(nx/2+1,ycut1,ispec)), &
             &  pyy_pt_xy(i,ycut1,ispec)/abs(pyy_pt_xy(nx/2+1,ycut1,ispec)), &
             &  pzz_pt_xy(i,ycut1,ispec)/abs(pzz_pt_xy(nx/2+1,ycut1,ispec)), &
             &  pxy_pt_xy(i,ycut1,ispec)/abs(pxy_pt_xy(nx/2+1,ycut1,ispec)), &
             &  pyz_pt_xy(i,ycut1,ispec)/abs(pyz_pt_xy(nx/2+1,ycut1,ispec)), &
             &  pzx_pt_xy(i,ycut1,ispec)/abs(pzx_pt_xy(nx/2+1,ycut1,ispec)), &
             &  ispec=1,nspec)
        write(52,'(50d20.12)') time,x, &
             & phi_pt_xy(i,ycut2),apar_pt_xy(i,ycut2), &
             & bpar_pt_xy(i,ycut2),jpar_pt_xy(i,ycut2), &
             & phi_pt_xy(i,ycut2)/abs((phi_pt_xy(nx/2+2,ycut2)-phi_pt_xy(nx/2,ycut2))/(2.*lx/nx)), &
             & apar_pt_xy(i,ycut2)/abs(apar_pt_xy(nx/2+1,ycut2)), &
             & bpar_pt_xy(i,ycut2)/abs((bpar_pt_xy(nx/2+2,ycut2)-bpar_pt_xy(nx/2,ycut2))/(2.*lx/nx)), &
             & jpar_pt_xy(i,ycut2)/abs(jpar_pt_xy(nx/2+1,ycut2)), &
             & (dens_pt_xy(i,ycut2,ispec), &
             &  ux_pt_xy(i,ycut2,ispec),uy_pt_xy(i,ycut2,ispec),uz_pt_xy(i,ycut2,ispec), &
             &  pxx_pt_xy(i,ycut2,ispec),pyy_pt_xy(i,ycut2,ispec), &
             &  pzz_pt_xy(i,ycut2,ispec),pxy_pt_xy(i,ycut2,ispec), &
             &  pyz_pt_xy(i,ycut2,ispec),pzx_pt_xy(i,ycut2,ispec),ispec=1,nspec), &
             & (dens_pt_xy(i,ycut2,ispec)/abs((dens_pt_xy(nx/2+2,ycut2,ispec)-dens_pt_xy(nx/2,ycut2,ispec))/(2.*lx/nx)), &
             &  ux_pt_xy(i,ycut2,ispec)/abs(ux_pt_xy(nx/2+1,ycut2,ispec)), &
             &  uy_pt_xy(i,ycut2,ispec)/abs(uy_pt_xy(nx/2+1,ycut2,ispec)), &
             &  uz_pt_xy(i,ycut2,ispec)/abs(uz_pt_xy(nx/2+1,ycut2,ispec)), &
             &  pxx_pt_xy(i,ycut2,ispec)/abs(pxx_pt_xy(nx/2+1,ycut2,ispec)), &
             &  pyy_pt_xy(i,ycut2,ispec)/abs(pyy_pt_xy(nx/2+1,ycut2,ispec)), &
             &  pzz_pt_xy(i,ycut2,ispec)/abs(pzz_pt_xy(nx/2+1,ycut2,ispec)), &
             &  pxy_pt_xy(i,ycut2,ispec)/abs(pxy_pt_xy(nx/2+1,ycut2,ispec)), &
             &  pyz_pt_xy(i,ycut2,ispec)/abs(pyz_pt_xy(nx/2+1,ycut2,ispec)), &
             &  pzx_pt_xy(i,ycut2,ispec)/abs(pzx_pt_xy(nx/2+1,ycut2,ispec)), &
             &  ispec=1,nspec)
        write(53,'(50d20.12)') time,x, &
             & phi_pt_xy(i,ycut3),apar_pt_xy(i,ycut3), &
             & bpar_pt_xy(i,ycut3),jpar_pt_xy(i,ycut3), &
             & phi_pt_xy(i,ycut3)/abs((phi_pt_xy(nx/2+2,ycut3)-phi_pt_xy(nx/2,ycut3))/(2.*lx/nx)), &
             & apar_pt_xy(i,ycut3)/abs(apar_pt_xy(nx/2+1,ycut3)), &
             & bpar_pt_xy(i,ycut3)/abs((bpar_pt_xy(nx/2+2,ycut3)-bpar_pt_xy(nx/2,ycut3))/(2.*lx/nx)), &
             & jpar_pt_xy(i,ycut3)/abs(jpar_pt_xy(nx/2+1,ycut3)), &
             & (dens_pt_xy(i,ycut3,ispec), &
             &  ux_pt_xy(i,ycut3,ispec),uy_pt_xy(i,ycut3,ispec),uz_pt_xy(i,ycut3,ispec), &
             &  pxx_pt_xy(i,ycut3,ispec),pyy_pt_xy(i,ycut3,ispec), &
             &  pzz_pt_xy(i,ycut3,ispec),pxy_pt_xy(i,ycut3,ispec), &
             &  pyz_pt_xy(i,ycut3,ispec),pzx_pt_xy(i,ycut3,ispec),ispec=1,nspec), &
             & (dens_pt_xy(i,ycut3,ispec)/abs((dens_pt_xy(nx/2+2,ycut3,ispec)-dens_pt_xy(nx/2,ycut3,ispec))/(2.*lx/nx)), &
             &  ux_pt_xy(i,ycut3,ispec)/abs(ux_pt_xy(nx/2+1,ycut3,ispec)), &
             &  uy_pt_xy(i,ycut3,ispec)/abs(uy_pt_xy(nx/2+1,ycut3,ispec)), &
             &  uz_pt_xy(i,ycut3,ispec)/abs(uz_pt_xy(nx/2+1,ycut3,ispec)), &
             &  pxx_pt_xy(i,ycut3,ispec)/abs(pxx_pt_xy(nx/2+1,ycut3,ispec)), &
             &  pyy_pt_xy(i,ycut3,ispec)/abs(pyy_pt_xy(nx/2+1,ycut3,ispec)), &
             &  pzz_pt_xy(i,ycut3,ispec)/abs(pzz_pt_xy(nx/2+1,ycut3,ispec)), &
             &  pxy_pt_xy(i,ycut3,ispec)/abs(pxy_pt_xy(nx/2+1,ycut3,ispec)), &
             &  pyz_pt_xy(i,ycut3,ispec)/abs(pyz_pt_xy(nx/2+1,ycut3,ispec)), &
             &  pzx_pt_xy(i,ycut3,ispec)/abs(pzx_pt_xy(nx/2+1,ycut3,ispec)), &
             &  ispec=1,nspec)
        write(54,'(50d20.12)') time,x, &
             & phi_pt_xy(i,ycut4),apar_pt_xy(i,ycut4), &
             & bpar_pt_xy(i,ycut4),jpar_pt_xy(i,ycut4), &
             & phi_pt_xy(i,ycut4)/abs((phi_pt_xy(nx/2+2,ycut4)-phi_pt_xy(nx/2,ycut4))/(2.*lx/nx)), &
             & apar_pt_xy(i,ycut4)/abs(apar_pt_xy(nx/2+1,ycut4)), &
             & bpar_pt_xy(i,ycut4)/abs((bpar_pt_xy(nx/2+2,ycut4)-bpar_pt_xy(nx/2,ycut4))/(2.*lx/nx)), &
             & jpar_pt_xy(i,ycut4)/abs(jpar_pt_xy(nx/2+1,ycut4)), &
             & (dens_pt_xy(i,ycut4,ispec), &
             &  ux_pt_xy(i,ycut4,ispec),uy_pt_xy(i,ycut4,ispec),uz_pt_xy(i,ycut4,ispec), &
             &  pxx_pt_xy(i,ycut4,ispec),pyy_pt_xy(i,ycut4,ispec), &
             &  pzz_pt_xy(i,ycut4,ispec),pxy_pt_xy(i,ycut4,ispec), &
             &  pyz_pt_xy(i,ycut4,ispec),pzx_pt_xy(i,ycut4,ispec),ispec=1,nspec), &
             & (dens_pt_xy(i,ycut4,ispec)/abs((dens_pt_xy(nx/2+2,ycut4,ispec)-dens_pt_xy(nx/2,ycut4,ispec))/(2.*lx/nx)), &
             &  ux_pt_xy(i,ycut4,ispec)/abs(ux_pt_xy(nx/2+1,ycut4,ispec)), &
             &  uy_pt_xy(i,ycut4,ispec)/abs(uy_pt_xy(nx/2+1,ycut4,ispec)), &
             &  uz_pt_xy(i,ycut4,ispec)/abs(uz_pt_xy(nx/2+1,ycut4,ispec)), &
             &  pxx_pt_xy(i,ycut4,ispec)/abs(pxx_pt_xy(nx/2+1,ycut4,ispec)), &
             &  pyy_pt_xy(i,ycut4,ispec)/abs(pyy_pt_xy(nx/2+1,ycut4,ispec)), &
             &  pzz_pt_xy(i,ycut4,ispec)/abs(pzz_pt_xy(nx/2+1,ycut4,ispec)), &
             &  pxy_pt_xy(i,ycut4,ispec)/abs(pxy_pt_xy(nx/2+1,ycut4,ispec)), &
             &  pyz_pt_xy(i,ycut4,ispec)/abs(pyz_pt_xy(nx/2+1,ycut4,ispec)), &
             &  pzx_pt_xy(i,ycut4,ispec)/abs(pzx_pt_xy(nx/2+1,ycut4,ispec)), &
             &  ispec=1,nspec)
     end do
     write(51,*);write(52,*);write(53,*);write(54,*)

     do j=1,ny
        y=ly/ny*(j-1)
        write(55,'(30d20.12)') time,y, &
             & phi_pt_xy(xcut1,j),apar_pt_xy(xcut1,j), &
             & bpar_pt_xy(xcut1,j),jpar_pt_xy(xcut1,j), &
             & phi_pt_xy(xcut1,j), &
             & apar_pt_xy(xcut1,j), &
             & bpar_pt_xy(xcut1,j), &
             & jpar_pt_xy(xcut1,j), &
             & (dens_pt_xy(xcut1,j,ispec), &
             &  ux_pt_xy(xcut1,j,ispec),uy_pt_xy(xcut1,j,ispec),uz_pt_xy(xcut1,j,ispec), &
             &  pxx_pt_xy(xcut1,j,ispec),pyy_pt_xy(xcut1,j,ispec), &
             &  pzz_pt_xy(xcut1,j,ispec),pxy_pt_xy(xcut1,j,ispec), &
             &  pyz_pt_xy(xcut1,j,ispec),pzx_pt_xy(xcut1,j,ispec),ispec=1,nspec)
        write(56,'(30d20.12)') time,y, &
             & phi_pt_xy(xcut2,j),apar_pt_xy(xcut2,j), &
             & bpar_pt_xy(xcut2,j),jpar_pt_xy(xcut2,j), &
             & phi_pt_xy(xcut2,j), &
             & apar_pt_xy(xcut2,j), &
             & bpar_pt_xy(xcut2,j), &
             & jpar_pt_xy(xcut2,j), &
             & (dens_pt_xy(xcut2,j,ispec), &
             &  ux_pt_xy(xcut2,j,ispec),uy_pt_xy(xcut2,j,ispec),uz_pt_xy(xcut2,j,ispec), &
             &  pxx_pt_xy(xcut2,j,ispec),pyy_pt_xy(xcut2,j,ispec), &
             &  pzz_pt_xy(xcut2,j,ispec),pxy_pt_xy(xcut2,j,ispec), &
             &  pyz_pt_xy(xcut2,j,ispec),pzx_pt_xy(xcut2,j,ispec),ispec=1,nspec)
        write(57,'(30d20.12)') time,y, &
             & phi_pt_xy(xcut3,j),apar_pt_xy(xcut3,j), &
             & bpar_pt_xy(xcut3,j),jpar_pt_xy(xcut3,j), &
             & phi_pt_xy(xcut3,j), &
             & apar_pt_xy(xcut3,j), &
             & bpar_pt_xy(xcut3,j), &
             & jpar_pt_xy(xcut3,j), &
             & (dens_pt_xy(xcut3,j,ispec), &
             &  ux_pt_xy(xcut3,j,ispec),uy_pt_xy(xcut3,j,ispec),uz_pt_xy(xcut3,j,ispec), &
             &  pxx_pt_xy(xcut3,j,ispec),pyy_pt_xy(xcut3,j,ispec), &
             &  pzz_pt_xy(xcut3,j,ispec),pxy_pt_xy(xcut3,j,ispec), &
             &  pyz_pt_xy(xcut3,j,ispec),pzx_pt_xy(xcut3,j,ispec),ispec=1,nspec)
        write(58,'(30d20.12)') time,y, &
             & phi_pt_xy(xcut4,j),apar_pt_xy(xcut4,j), &
             & bpar_pt_xy(xcut4,j),jpar_pt_xy(xcut4,j), &
             & phi_pt_xy(xcut4,j), &
             & apar_pt_xy(xcut4,j), &
             & bpar_pt_xy(xcut4,j), &
             & jpar_pt_xy(xcut4,j), &
             & (dens_pt_xy(xcut4,j,ispec), &
             &  ux_pt_xy(xcut4,j,ispec),uy_pt_xy(xcut4,j,ispec),uz_pt_xy(xcut4,j,ispec), &
             &  pxx_pt_xy(xcut4,j,ispec),pyy_pt_xy(xcut4,j,ispec), &
             &  pzz_pt_xy(xcut4,j,ispec),pxy_pt_xy(xcut4,j,ispec), &
             &  pyz_pt_xy(xcut4,j,ispec),pzx_pt_xy(xcut4,j,ispec),ispec=1,nspec)
     end do
     write(55,*);write(56,*);write(57,*);write(58,*)

     if(debug .and. proc0) write(stdout,*) 'growth'

     if(itime/=1) then
!        growth=(apar_pt_xy(xcut3,ycut3)-apar_pt_old)/(time-time_old)/apar_pt_xy(xcut3,ycut3)
        growth=log(apar_pt_xy(xcut3,ycut3)/apar_pt_old)/(time-time_old)
!        epar_C=-(apar_xy(xcut3,ycut3)-apar_old)/(time-time_old)
        epar_C=-(apar_xy(xcut3,ycut3)-apar_old(xcut3,ycut3))/(time-time_old)
        island_width=4.*sqrt( &
             -apar_pt_xy(xcut3,ycut3)/(jpar_xy(xcut3,ycut3)-jpar_pt_xy(xcut3,ycut3))*.5/beta &
             )
        apar_X=apar_xy(xcut3,ycut3)
        
        ! Is secondary island forming?
        epar_X=epar_C
        xpoint_x=xcut3; xpoint_y=ycut3;
        if (apar_X > apar_xy(xcut3,ycut3+1)) then
           ! search the actual X point.
           xsearch : do j=ycut3+1,ny
              if (apar_xy(xcut3,j)<apar_xy(xcut3,j+1)) then
                 apar_X=apar_xy(xcut3,j)
                 epar_X=-(apar_xy(xcut3,j)-apar_old(xcut3,j))/(time-time_old)
                 xpoint_y=j

                 island_width=4.*sqrt( &
                      -apar_pt_xy(xcut3,j)/(jpar_xy(xcut3,j)-jpar_pt_xy(xcut3,j))*.5/beta &
                      )
                 exit xsearch
              endif
           end do xsearch
        endif

        if (proc0) write(81,'(3e20.12,2i20)') time,lx/nx*(xpoint_x-1),ly/ny*(xpoint_y-1),xpoint_x,xpoint_y

        search : do i=xcut3,nx
           if ((apar_xy(i,ycut1)-apar_X)*(apar_xy(i+1,ycut1)-apar_X)<0.) then
              island_width2=2.*(i-xcut3+(apar_xy(i,ycut1)-apar_X)/(apar_xy(i,ycut1)-apar_xy(i+1,ycut1)))*lx/nx
              exit search
           endif
        end do search
        if (proc0) then
!           write(61,'(7d20.12)') time,apar_pt_xy(xcut3,ycut3),growth,island_width,island_width2,epar,epar2
           write(61,'(8e20.12)') time,growth,island_width,island_width2, &
                & epar_C,epar_X,apar_pt_xy(xcut3,ycut3),apar_pt_xy(xpoint_x,xpoint_y)
        endif
     endif
!     time_old=time; apar_old=apar_xy(xcut3,ycut3); apar_pt_old=apar_pt_xy(xcut3,ycut3)
     time_old=time; apar_old=apar_xy; apar_pt_old=apar_pt_xy(xcut3,ycut3)

     if(debug .and. proc0) then
        do i=1,nx
           x=lx/nx*(i-1)
           write(91,'(5e20.12)') time,x,jpar_pt_xy(i,ycut3),jfunc(x),djfunc(x)
        end do
        write(91,*)
     endif

     if(debug .and. proc0) write(stdout,*) 'current width'

     if(itime/=1) then
        width_guess=xsheet
        guess1 : do while (jfunc(xsheet)*jfunc(width_guess)>=0.)
           width_guess=width_guess+del*lx
           if(width_guess>=lx) then
              width_guess=xsheet
              exit guess1
           endif
        end do guess1
        jmax_guess=width_guess
        guess2 : do while (djfunc(width_guess)*djfunc(jmax_guess)>=0.)
           jmax_guess=jmax_guess+del*lx
           if(jmax_guess>=lx) then
              jmax_guess=width_guess
              exit guess2
           endif
        end do guess2
        ! find position where dj/dx=0
        call root_finder_bisect(width_guess,jmax_guess, &
             0.,xjmax,djfunc)

        width=xsheet; width2=xsheet
        call root_finder_bisect(xsheet,width_guess, &
             .5*jfunc(xsheet),width,jfunc)
        call root_finder_bisect(width,jmax_guess, &
             .5*(jfunc(xsheet)+jfunc(xjmax)),width2,jfunc)
        width=2.*(width-xsheet)
        width2=2.*(width2-xsheet)

!        if (proc0) write(71,'(4d20.12)') time,jpar_pt_xy(xcut3,ycut3),width,width2
        if (proc0) write(71,'(5e20.12)') time,width,width2, &
             & jpar_pt_xy(xcut3,ycut3),jpar_pt_xy(xpoint_x,xpoint_y)

     endif
  end do timeloop

  close(51); close(52); close(53); close(54)
  close(55); close(56); close(57); close(58)
  close(61)
  close(71)
  close(81)

  if(debug .and. proc0) close(91)

  call finish_mp

contains

  subroutine root_finder_bisect (g0, g1, val, sol, func)

    real, intent(in) :: g0, g1, val
    real, intent(out) :: sol

    interface
       function func (x)
         real, intent(in) :: x
         real :: func
       end function func
    end interface

    real :: x0, x1, xc, y0, y1, yc

    x0 = g0; x1 = g1
    ! rhs - lhs
    y0 = func(x0) - val
    y1 = func(x1) - val

    if (y0*y1 > 0.0) then
       write (0,*) 'no solution in specified domain'
       !       stop
       return
    end if

    do while (abs((x0-x1)/max(abs(x0),abs(x1))) > 1.e-10)

       xc = (x0+x1)/2.0
       yc = func(xc) - val

       if (y0*yc > 0.0) then
          x0 = xc; y0 = yc
       else
          x1 = xc; y1 = yc
       end if

    end do

    if (abs(y0) < abs(y1)) then
       sol = x0
    else
       sol = x1
    end if

  end subroutine root_finder_bisect

end program tearing_diag
