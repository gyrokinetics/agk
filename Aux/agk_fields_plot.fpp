# include "define.inc"

program agk_fields_plot

  use mp, only: init_mp, finish_mp, proc0, broadcast
  use file_utils, only: init_file_utils, get_unused_unit
  use file_utils, only: run_name2 => run_name
  use command_line, only: cl_getarg
  use pgplot_utils, only: pgkind
  use fields_plot_utils, only: init_datafile, finish_datafile
  use fields_plot_utils, only: init_readvar
  use fields_plot_utils, only: get_boxsize, get_dimensions
  use fields_plot_utils, only: get_field, get_moment, get_time
  use fields_plot_utils, only: init_fft, finish_fft
  use fields_plot_utils, only: init_plot, finish_plot
  use fields_plot_utils, only: set_viewport, set_coordinate
  use fields_plot_utils, only: newpage, newpanel
  use fields_plot_utils, only: get_minmax
  use fields_plot_utils, only: infinity
  use fields_plot_utils, only: init_equilibrium
  use netcdf_utils, only: kind_nf
  use fields, only: allocate_arrays
  use dist_fn, only: init_dist_fn_minimal
  
  implicit none

  integer, parameter :: stdout=6, stderr=0

  integer :: i
  integer :: j
  integer :: nkx, nky, nx, ny, nspec
  integer :: itime, ntime
  integer :: ispec
  integer :: istatus
  integer, parameter :: nvar_field=4 ! number of field variables
  integer, parameter :: nvar_moment=10 ! number of moment variables
  integer :: nvar_field_plot=nvar_field ! number of field variables to plot
  integer :: nvar_moment_plot=nvar_moment ! number of moment variables to plot

  real :: time
  real :: lx, ly
  real (kind=pgkind), allocatable :: xp(:)
  real, allocatable :: field_var(:,:)

  character (len=100) :: filename_in, filename_out(4)

  logical :: do_field_plot=.false., do_moment_plot=.false.
  logical :: do_field_cut_plot=.false.
  logical :: do_moment_cut_plot=.false.

# ifdef NETCDF
  integer (kind=kind_nf) :: file_id 
  integer (kind=kind_nf) :: time_id
  integer (kind=kind_nf) :: var_field_id(nvar_field)
  integer (kind=kind_nf) :: var_moment_id(nvar_moment)
# else
  integer :: file_id
  integer :: time_id
  integer :: var_field_id(nvar_field)
  integer :: var_moment_id(nvar_moment)
# endif

  integer :: pg_ipanel
  integer, parameter :: pg_npanel_field=4
  integer :: pg_ptr_field(pg_npanel_field)=(/ (i,i=1,pg_npanel_field) /)
  integer :: pg_nrow_field=1
  integer :: pg_icut_field(pg_npanel_field)=(/ (1,i=1,pg_npanel_field) /)

  integer, parameter :: nspec_max=2
  integer :: nspec_plot=nspec_max
  integer :: pg_npanel_moment
  integer :: pg_ptr_moment(nvar_moment)=(/ (i,i=1,nvar_moment) /)
  integer :: pg_ptr_spec(nspec_max)=(/ (i,i=1,nspec_max) /)
  integer :: pg_nrow_moment
  integer :: pg_icut_moment(nvar_moment)=(/ (1,i=1,nvar_moment) /)

  integer :: pg_idx

  character (len=100) :: pg_txttl ! title
  character (len=100) :: pg_xlab='\fix/\gr\d\fri\fi\u'
  character (len=100) :: pg_ylab='\fiy/\gr\d\fri\fi\u'
  character (len=200) :: pg_tlab
  character (len=100) :: pg_tlab_field(nvar_field)= (/ &
       & '\gf\fr            ', &
       & '\fiA\fr\d\(0737)\u', &
       & '\fiB\fr\d\(0737)\u', &
       & '\fiJ\fr\d\(0737)\u' /)
  character (len=100) :: pg_tlab_moment(nvar_moment)= (/ &
       & '\fin\fr            ', & ! n
       & '\fiu\fr\d\fix\fr\u ', & ! u_x
       & '\fiu\fr\d\fiy\fr\u ', & ! u_y
       & '\fiu\fr\d\fiz\fr\u ', & ! u_z
       & '\fiP\fr\d\fixx\fr\u', & ! P_xx
       & '\fiP\fr\d\fiyy\fr\u', & ! P_yy
       & '\fiP\fr\d\fizz\fr\u', & ! P_zz
       & '\fiP\fr\d\fixy\fr\u', & ! P_xy
       & '\fiP\fr\d\fiyz\fr\u', & ! P_yz
       & '\fiP\fr\d\fizx\fr\u'  & ! P_zx
       & /)
  character (len=2) :: pg_cwdg='BI' ! wedge
  !  logical, parameter :: pg_crange_global = .true.
  logical :: pg_crange_global = .false.
  real (kind=pgkind) :: pg_trc(6) ! transformation matrix
  real (kind=pgkind) :: pg_wedge(2) ! wedge position and height
  real (kind=pgkind), allocatable :: pg_var(:,:)
  real (kind=pgkind) :: pg_cmin, pg_cmax
  real (kind=pgkind) :: pg_cmin_global_field(nvar_field)
  real (kind=pgkind) :: pg_cmax_global_field(nvar_field)
  real (kind=pgkind), allocatable :: pg_cmin_global_moment(:,:)
  real (kind=pgkind), allocatable :: pg_cmax_global_moment(:,:)
  real (kind=pgkind) :: pg_field_min, pg_field_max
  integer, parameter :: pg_nc=20
  real (kind=pgkind) :: pg_clev(pg_nc), pg_dc
  real (kind=pgkind) :: pg_ccrop(4)=(/ -infinity,-infinity,infinity,infinity /)
  integer :: pg_nccrop(4)
  ! transform origin to center from left-bottom corner
  logical :: origin_center=.false.
  ! keep aspect raio
  logical :: keep_aspect=.true.
  ! plot perturbed part only
  logical :: perturbed=.false.
  logical :: list
  integer :: unit
  logical :: grid = .false.
  character (len=100) :: run_name

  namelist /field_plot/ &
       & nvar_field_plot, &
       & nvar_moment_plot, nspec_plot, &
       & do_field_plot, do_moment_plot, &
       & do_field_cut_plot, do_moment_cut_plot, &
       & pg_ccrop, &
       & keep_aspect, perturbed, origin_center, pg_crange_global, &
       & pg_ptr_field, pg_ptr_moment, pg_ptr_spec

  ! this block is necessary to use transform2
  call init_mp
  if (proc0) read(5,field_plot)
  call broadcast(do_field_plot)
  call broadcast(do_moment_plot)
  call broadcast(do_field_cut_plot)
  call broadcast(do_moment_cut_plot)
  call broadcast(perturbed)
  if (proc0) call init_file_utils(list)
  if (proc0) run_name = run_name2
  call broadcast(run_name)
  call init_dist_fn_minimal
  call allocate_arrays

  filename_in=trim(run_name)//'.out.nc'
  filename_out(1)=trim(run_name)//'_field.ps/cps'
  filename_out(2)=trim(run_name)//'_moment.ps/cps'
  filename_out(3)=trim(run_name)//'_field_cut.ps/cps'
  filename_out(4)=trim(run_name)//'_moment_cut.ps/cps'

  ! open file
  call init_datafile(filename_in,file_id)

  ! get dimensions
  call get_dimensions(file_id,nkx,nky,nx,ny,nspec)

  ! get box size
  call get_boxsize(file_id,lx,ly)

  ! initialize reading variables
  call init_readvar(file_id,time_id,ntime,var_field_id,var_moment_id)

  ! initialize fft
  call init_fft(nx,ny)

  ! allocate coordinates and field variables
  allocate(pg_var(nx+1,ny+1),stat=istatus)
  if (istatus /= 0)  stop 'allocation error'
  allocate(field_var(nx,ny),stat=istatus)
  if (istatus /= 0)  stop 'allocation error'

!!! get equilibrium profile
  if(perturbed) call init_equilibrium

!!! field plot

  if(do_field_plot.and.proc0) then


     ! setup pgplot
     call init_plot(filename_out(1))
     call set_viewport(nvar_field_plot, nvar_field_plot, pg_cwdg, pg_wedge)
     call set_coordinate(lx,ly,nx,ny,pg_trc,origin_center,pg_ccrop,pg_nccrop)
     ! contour range global
     if(pg_crange_global) then
        pg_cmin_global_field(1:nvar_field)=+infinity
        pg_cmax_global_field(1:nvar_field)=-infinity
        do itime = 1, ntime
           do pg_idx=1,nvar_field
              call get_field(file_id,itime,var_field_id(pg_idx), &
                   & nkx,nky,nx,ny,pg_ptr_field(pg_idx),field_var, &
                   & origin_center)
              call get_minmax(field_var,pg_field_min,pg_field_max)
              pg_cmin_global_field(pg_idx)= &
                   & min(pg_cmin_global_field(pg_idx),pg_field_min)
              pg_cmax_global_field(pg_idx)= &
                   & max(pg_cmax_global_field(pg_idx),pg_field_max)
           end do
        end do
     endif

     do itime = 1, ntime
        ! get time
        call get_time(file_id,itime,time_id,time)
        write(stdout,*) 'time = ',time
        write(pg_txttl,'(a,f10.4,a)') 'time = ',time,' [\fiL/\fiv\d\frth\u]'

        ! new page and title
        call newpage(pg_txttl, showgrid=grid)

        ! make pg_npanel panels in one page 
        do pg_ipanel=1,nvar_field_plot

           ! get variables
           call get_field(file_id,itime, &
                & var_field_id(pg_ptr_field(pg_ipanel)), &
                & nkx,nky,nx,ny,pg_ptr_field(pg_ipanel),field_var, &
                & origin_center)
           call get_minmax(field_var,pg_field_min,pg_field_max)

           pg_var(1:nx,1:ny)=field_var(1:nx,1:ny)

           ! apply periodic boundary condition
           pg_var(  nx+1,1:ny)= pg_var(1     ,1:ny)
           pg_var(1:nx+1,ny+1)= pg_var(1:nx+1,1   )

           if(pg_crange_global) then
              pg_cmin=pg_cmin_global_field(pg_ptr_field(pg_ipanel))
              pg_cmax=pg_cmax_global_field(pg_ptr_field(pg_ipanel))
           else
              pg_cmin=pg_field_min
              pg_cmax=pg_field_max
           end if

           pg_dc=(pg_cmax-pg_cmin)/(pg_nc-1)
           do i=1,pg_nc
              pg_clev(i)=pg_cmin+pg_dc*(i-1)
           end do
           call newpanel(pg_ipanel,pg_xlab,pg_ylab, &
                & pg_tlab_field(pg_ptr_field(pg_ipanel)),keep_aspect)

           call pgimag(pg_var(1:nx+1,1:ny+1),nx+1,ny+1, &
                & 1,nx+1,1,ny+1, &
                & pg_cmin,pg_cmax,pg_trc)

           call pgcont(pg_var(1:nx+1,1:ny+1),nx+1,ny+1, &
                & pg_nccrop(1), pg_nccrop(3), pg_nccrop(2), pg_nccrop(4), &
                & pg_clev,pg_nc,pg_trc)
           call pgwedg(pg_cwdg,pg_wedge(1),pg_wedge(2),pg_cmin,pg_cmax,'')
        end do

     end do

     ! finish plot
     call finish_plot

     write(6,*) 'field_plot done'

  end if

!!! moment plot

  if(do_moment_plot.and.proc0) then

     write(6,*) 'moment_plot'

     pg_npanel_moment=nvar_moment_plot*nspec_plot
     pg_nrow_moment=nvar_moment_plot

     ! setup pgplot
     call init_plot(filename_out(2))
     call set_viewport(pg_npanel_moment, pg_nrow_moment, pg_cwdg, pg_wedge)
     call set_coordinate(lx,ly,nx,ny,pg_trc,origin_center,pg_ccrop,pg_nccrop)
     
     ! contour range global
     if(pg_crange_global) then
        allocate(pg_cmin_global_moment(nvar_moment_plot,nspec_plot))
        allocate(pg_cmax_global_moment(nvar_moment_plot,nspec_plot))
        pg_cmin_global_moment(1:nvar_moment_plot,1:nspec_plot)=+infinity
        pg_cmax_global_moment(1:nvar_moment_plot,1:nspec_plot)=-infinity
        do itime = 1, ntime
           do pg_idx=1,nvar_moment_plot
              do ispec=1,nspec_plot
                 call get_moment(file_id,itime,var_moment_id(pg_idx),ispec, &
                      & nkx,nky,nx,ny,nspec,pg_idx,field_var, &
                      & origin_center)
                 call get_minmax(field_var,pg_field_min,pg_field_max)
                 pg_cmin_global_moment(pg_idx,ispec)= &
                      & min(pg_cmin_global_moment(pg_idx,ispec),pg_field_min)
                 pg_cmax_global_moment(pg_idx,ispec)= &
                      & max(pg_cmax_global_moment(pg_idx,ispec),pg_field_max)
              end do
           end do
        enddo
     endif

     do itime = 1, ntime
        ! get time
        call get_time(file_id,itime,time_id,time)
        write(stdout,*) 'time = ',time
        write(pg_txttl,'(a,f10.4,a)') 'time = ',time,' [\fiL/\fiv\d\frth\u]'

        ! new page and title
        call newpage(pg_txttl, showgrid=grid)

        ! make pg_npanel panels in one page 
        do pg_ipanel=1,pg_npanel_moment
           pg_idx=mod(pg_ipanel-1,nvar_moment_plot)+1
           ispec=(pg_ipanel-1)/nvar_moment_plot+1
           ! get moments
           call get_moment(file_id,itime, &
                & var_moment_id(pg_ptr_moment(pg_idx)), &
                & pg_ptr_spec(ispec),nkx,nky,nx,ny,nspec, &
                & pg_ptr_moment(pg_idx),field_var, &
                & origin_center)
           call get_minmax(field_var,pg_field_min,pg_field_max)
           pg_var(1:nx,1:ny)=field_var(1:nx,1:ny)
           
           ! apply periodic boundary condition
           pg_var(  nx+1,1:ny)= pg_var(1     ,1:ny)
           pg_var(1:nx+1,ny+1)= pg_var(1:nx+1,1   )

           if(pg_crange_global) then
              pg_cmin=pg_cmin_global_moment( &
                   & pg_ptr_moment(pg_idx), &
                   & pg_ptr_spec(ispec))
              pg_cmax=pg_cmax_global_moment( &
                   & pg_ptr_moment(pg_idx), &
                   & pg_ptr_spec(ispec))
           else
              pg_cmin=pg_field_min
              pg_cmax=pg_field_max
           end if

           pg_dc=(pg_cmax-pg_cmin)/(pg_nc-1)
           do i=1,pg_nc
              pg_clev(i)=pg_cmin+pg_dc*(i-1)
           end do
           write(pg_tlab,'(a,a,i0)') &
                & trim(adjustl(pg_tlab_moment(pg_ptr_moment(pg_idx)))), &
                & ' spec = ', pg_ptr_spec(ispec)

           call newpanel(pg_ipanel,pg_xlab,pg_ylab,pg_tlab,keep_aspect)

           call pgimag(pg_var(1:nx+1,1:ny+1),nx+1,ny+1, &
                & 1,nx+1,1,ny+1, &
                & pg_cmin,pg_cmax,pg_trc)
           call pgcont(pg_var(1:nx+1,1:ny+1),nx+1,ny+1, &
                & pg_nccrop(1), pg_nccrop(3), pg_nccrop(2), pg_nccrop(4), &
                & pg_clev,pg_nc,pg_trc)
           call pgwedg(pg_cwdg,pg_wedge(1),pg_wedge(2),pg_cmin,pg_cmax,'')
        end do

     end do

     ! finish plot
     call finish_plot

     write(6,*) 'moment_plot done'

  end if

!!! field cut
  if(do_field_cut_plot.and.proc0) then

     write(6,*) 'field_cut_plot'

     ! setup pgplot
     call init_plot(filename_out(3))
     call set_viewport(nvar_field_plot, nvar_field_plot, pg_cwdg, pg_wedge)
     call set_coordinate(lx,ly,nx,ny,pg_trc,origin_center,pg_ccrop,pg_nccrop)
     if(.not.allocated(xp)) allocate(xp(nx+1))
     pg_icut_field(1:pg_npanel_field)=(/ ny/4+1, ny/2+1, ny/4+1, ny/2+1 /)

     ! contour range global
     if(pg_crange_global) then
        pg_cmin_global_field(1:nvar_field)=+infinity
        pg_cmax_global_field(1:nvar_field)=-infinity
        do itime = 1, ntime
           do pg_idx=1,nvar_field
              call get_field(file_id,itime,var_field_id(pg_idx), &
                   & nkx,nky,nx,ny,pg_ptr_field(pg_idx),field_var, &
                   & origin_center)
              call get_minmax( &
                   & field_var( &
                   & pg_nccrop(1):min(nx,pg_nccrop(3)), &
                   & pg_icut_field(pg_idx):pg_icut_field(pg_idx)), &
                   & pg_field_min,pg_field_max)
              pg_cmin_global_field(pg_idx)= &
                   & min(pg_cmin_global_field(pg_idx),pg_field_min)
              pg_cmax_global_field(pg_idx)= &
                   & max(pg_cmax_global_field(pg_idx),pg_field_max)
           end do
        end do
     endif

     do itime = 1, ntime
        ! get time
        call get_time(file_id,itime,time_id,time)
        write(stdout,*) 'time = ',time
        write(pg_txttl,'(a,f10.4,a)') 'time = ',time,' [\fiL/\fiv\d\frth\u]'

        ! new page and title
        call newpage(pg_txttl, showgrid=grid)
        call pgbox('BCNST',0._pgkind,0,'BCNST',0._pgkind,0)

        ! make pg_npanel panels in one page 
        do pg_ipanel=1,nvar_field_plot

           ! get variables
           call get_field(file_id,itime, &
                & var_field_id(pg_ptr_field(pg_ipanel)), &
                & nkx,nky,nx,ny,pg_ptr_field(pg_ipanel),field_var, &
                & origin_center)
           call get_minmax( &
                & field_var(pg_nccrop(1):min(nx,pg_nccrop(3)), &
                & pg_icut_field(pg_ipanel):pg_icut_field(pg_ipanel)), &
                & pg_field_min,pg_field_max)

           pg_var(1:nx,1:ny)=field_var(1:nx,1:ny)

           ! apply periodic boundary condition
           pg_var(  nx+1,1:ny)= pg_var(1     ,1:ny)
           pg_var(1:nx+1,ny+1)= pg_var(1:nx+1,1   )

           if(pg_crange_global) then
              pg_cmin=pg_cmin_global_field(pg_ptr_field(pg_ipanel))
              pg_cmax=pg_cmax_global_field(pg_ptr_field(pg_ipanel))
           else
              pg_cmin=pg_field_min
              pg_cmax=pg_field_max
           end if
           if(origin_center) then
              xp(1:nx+1)=(/ ((i-1)*lx/nx-.5*lx,i=1,nx+1) /)
           else
              xp(1:nx+1)=(/ ((i-1)*lx/nx,i=1,nx+1) /)
           end if
           if(pg_cmax<=pg_cmin) pg_cmax=pg_cmin+1.
           call newpanel(pg_ipanel,pg_xlab, &
                & pg_tlab_field(pg_ptr_field(pg_ipanel)), &
                & '',.false., &
                & (/ xp(pg_nccrop(1)),xp(pg_nccrop(3)),pg_cmin,pg_cmax/))
           call pgline( &
                & pg_nccrop(3)-pg_nccrop(1)+1, &
                & xp(pg_nccrop(1):pg_nccrop(3)), &
                & pg_var(pg_nccrop(1):pg_nccrop(3),pg_icut_field(pg_ipanel)))
        end do

     end do

     deallocate(xp)
     ! finish plot
     call finish_plot

     write(6,*) 'field_cut_plot done'

  endif

!!! field cut
  if(do_moment_cut_plot.and.proc0) then

     write(6,*) 'moment_cut_plot'

     pg_npanel_moment=nvar_moment_plot*nspec_plot
     pg_nrow_moment=nvar_moment_plot

     ! setup pgplot
     call init_plot(filename_out(4))
     call set_viewport(pg_npanel_moment, pg_nrow_moment, pg_cwdg, pg_wedge)
     call set_coordinate(lx,ly,nx,ny,pg_trc,origin_center,pg_ccrop,pg_nccrop)
     if(.not.allocated(xp)) allocate(xp(nx+1))
     pg_icut_moment(1:nvar_moment)= &
          & (/ ny/4+1, ny/2+1, ny/2+1, ny/2+1, &
          &    ny/4+1, ny/4+1, ny/4+1, ny/4+1, ny/4+1, ny/4+1 /)

     ! contour range global
     if(pg_crange_global) then
        allocate(pg_cmin_global_moment(nvar_moment_plot,nspec_plot))
        allocate(pg_cmax_global_moment(nvar_moment_plot,nspec_plot))
        pg_cmin_global_moment(1:nvar_moment_plot,1:nspec_plot)=+infinity
        pg_cmax_global_moment(1:nvar_moment_plot,1:nspec_plot)=-infinity
        do itime = 1, ntime
           do pg_idx=1,nvar_moment_plot
              do ispec=1,nspec_plot
                 call get_moment(file_id,itime,var_moment_id(pg_idx),ispec, &
                      & nkx,nky,nx,ny,nspec,pg_idx,field_var, &
                      & origin_center)
                 call get_minmax(field_var( &
                   & pg_nccrop(1):min(nx,pg_nccrop(3)), &
                   & pg_icut_moment(pg_idx):pg_icut_moment(pg_idx)), &
                   & pg_field_min,pg_field_max)
                 pg_cmin_global_moment(pg_idx,ispec)= &
                      & min(pg_cmin_global_moment(pg_idx,ispec),pg_field_min)
                 pg_cmax_global_moment(pg_idx,ispec)= &
                      & max(pg_cmax_global_moment(pg_idx,ispec),pg_field_max)
              end do
           end do
        enddo
     endif

     do itime = 1, ntime
        ! get time
        call get_time(file_id,itime,time_id,time)
        write(stdout,*) 'time = ',time
        write(pg_txttl,'(a,f10.4,a)') 'time = ',time,' [\fiL/\fiv\d\frth\u]'

        ! new page and title
        call newpage(pg_txttl, showgrid=grid)
        call pgbox('BCNST',0._pgkind,0,'BCNST',0._pgkind,0)

        ! make pg_npanel panels in one page 
        do pg_ipanel=1,pg_npanel_moment
           pg_idx=mod(pg_ipanel-1,nvar_moment_plot)+1
           ispec=(pg_ipanel-1)/nvar_moment_plot+1

           ! get variables
           call get_moment(file_id,itime, &
                & var_moment_id(pg_ptr_moment(pg_idx)), &
                & pg_ptr_spec(ispec),nkx,nky,nx,ny,nspec, &
                & pg_ptr_moment(pg_idx),field_var, &
                & origin_center)
           call get_minmax( &
                & field_var(pg_nccrop(1):min(nx,pg_nccrop(3)), &
                & pg_icut_moment(pg_idx):pg_icut_moment(pg_idx)), &
                & pg_field_min,pg_field_max)

           pg_var(1:nx,1:ny)=field_var(1:nx,1:ny)

           ! apply periodic boundary condition
           pg_var(  nx+1,1:ny)= pg_var(1     ,1:ny)
           pg_var(1:nx+1,ny+1)= pg_var(1:nx+1,1   )

           if(pg_crange_global) then
              pg_cmin=pg_cmin_global_moment( &
                   & pg_ptr_moment(pg_idx), &
                   & pg_ptr_spec(ispec))
              pg_cmax=pg_cmax_global_moment( &
                   & pg_ptr_moment(pg_idx), &
                   & pg_ptr_spec(ispec))
           else
              pg_cmin=pg_field_min
              pg_cmax=pg_field_max
           end if
           if(origin_center) then
              xp(1:nx+1)=(/ ((i-1)*lx/nx-.5*lx,i=1,nx+1) /)
           else
              xp(1:nx+1)=(/ ((i-1)*lx/nx,i=1,nx+1) /)
           end if
           if(pg_cmax<=pg_cmin) pg_cmax=pg_cmin+1.
           write(pg_tlab,'(a,a,i0)') &
                & trim(adjustl(pg_tlab_moment(pg_ptr_moment(pg_idx)))), &
                & ' spec = ', pg_ptr_spec(ispec)
           call newpanel(pg_ipanel,pg_xlab, &
                & pg_tlab,'',.false., &
                & (/ xp(pg_nccrop(1)),xp(pg_nccrop(3)),pg_cmin,pg_cmax/))
           call pgline( &
                & pg_nccrop(3)-pg_nccrop(1)+1, &
                & xp(pg_nccrop(1):pg_nccrop(3)), &
                & pg_var(pg_nccrop(1):pg_nccrop(3),pg_icut_moment(pg_idx)))
        end do

     end do

     deallocate(xp)
     ! finish plot
     call finish_plot

     write(6,*) 'moment_cut_plot done'

  endif

  ! finish fft, datafile
  call finish_fft
  call finish_datafile(file_id)

  ! deallocate field arrays
  deallocate(pg_var,stat=istatus)
  if (istatus /= 0)  stop 'deallocation error'
  deallocate(field_var,stat=istatus)
  if (istatus /= 0)  stop 'deallocation error'

  call finish_mp

  ! done
  stop

end program agk_fields_plot
