# include "define.inc"

module fields_plot_utils
  use pgplot_utils, only: pgkind
  use fft_work, only: fft_type
# ifdef NETCDF
  use netcdf, only: NF90_NOERR, NF90_NOWRITE
  use netcdf, only: nf90_inq_dimid, nf90_inquire_dimension
  use netcdf, only: nf90_inq_varid, nf90_inquire_variable
  use netcdf, only: nf90_get_var
  use netcdf_utils, only: kind_nf,netcdf_error
# endif
  implicit none
  private

  public :: infinity
  public :: init_datafile, finish_datafile
  public :: init_readvar
  public :: get_boxsize, get_dimensions
  public :: get_field, get_moment, get_time
  public :: init_fft, finish_fft
  public :: init_plot, finish_plot
  public :: set_viewport, set_coordinate
  public :: newpage, newpanel
  public :: get_minmax
  public :: init_equilibrium

  integer, parameter :: stdout=6, stderr=0
# ifdef NETCDF
  integer (kind=kind_nf) :: istatus
# else
  integer :: istatus
# endif
  real, parameter :: infinity=1.e15

  type (fft_type) :: fft

  character (len=10), parameter :: varname_field(4) = &
       & (/ 'phi0 ', 'apar0', 'bpar0', 'apar0' /)
  character (len=10), parameter :: varname_moment(10) = &
       & (/ 'dens0', 'ux0  ', 'uy0  ', 'uz0  ', &
       &    'pxx0 ', 'pyy0 ', 'pzz0 ', &
       &    'pxy0 ', 'pyz0 ', 'pzx0 ' /)

  real (kind=pgkind) :: pg_xch, pg_ych ! font height in x, y directions
  real (kind=pgkind) :: pg_wzp(4) ! window
  real (kind=pgkind), allocatable :: pg_vp(:,:) ! view port

  logical :: perturbed=.false.
  logical :: debug=.false.

contains

  subroutine init_datafile(filename,file_id)
# ifdef NETCDF
    use netcdf, only: nf90_open
# else    
    use file_utils, only: get_unused_unit
# endif
    character (len=*), intent(in) :: filename
    integer, intent(out) :: file_id
# ifdef NETCDF
  istatus = nf90_open (filename, NF90_NOWRITE, file_id)
  if (istatus /= NF90_NOERR) call netcdf_error (istatus, file_id)
# else
  call get_unused_unit(file_id)
  open(unit=file_id,file=filename)
# endif
  end subroutine init_datafile

  subroutine init_equilibrium
    use file_utils, only: get_unused_unit, error_unit
    use dist_fn_arrays, only: g_eq
    use dist_fn, only: init_dist_fn_minimal
    use fields, only: allocate_arrays
    use fields_arrays, only: apar_eq
    use agk_equil_io, only: agk_restore_eq
    use hdf_wrapper, only: hdf_init, hdf_finish
    call allocate_arrays
    call init_dist_fn_minimal
    call hdf_init
    call agk_restore_eq(g_eq, .true., .true., .true.)
    call hdf_finish
    perturbed = .true.
  end subroutine init_equilibrium

  subroutine finish_datafile(file_id)
# ifdef NETCDF
    use netcdf, only: nf90_close
# else
# endif
    integer, intent(in) :: file_id
# ifdef NETCDF
    istatus = nf90_close (file_id)
    if (istatus /= NF90_NOERR) call netcdf_error (istatus, file_id)
# else
    close(file_id)
# endif
  end subroutine finish_datafile

  subroutine get_dimensions(file_id,nkx,nky,nx,ny,nspec)
    use kgrids, only: nx_input => nx, ny_input => ny
    integer, intent(out) :: nkx,nky,nx,ny,nspec
# ifdef NETCDF
    integer (kind=kind_nf), intent(in) :: file_id
    integer (kind=kind_nf) :: kxdimid, kydimid, nspecid
# else
    integer, intent(in) :: file_id
# endif
# ifdef NETCDF
    istatus = nf90_inq_dimid (file_id, "kx", kxdimid)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, file_id, dim='kx')
    istatus = nf90_inq_dimid (file_id, "ky", kydimid)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, file_id, dim='ky')
    istatus = nf90_inq_dimid (file_id, "species", nspecid)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, file_id, dim='nspec')
    istatus = nf90_inquire_dimension (file_id, kxdimid, len=nkx)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, file_id, dimid=kxdimid)
    istatus = nf90_inquire_dimension (file_id, kydimid, len=nky)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, file_id, dimid=kydimid)
    istatus = nf90_inquire_dimension (file_id, nspecid, len=nspec)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, file_id, dimid=nspecid)
    if(debug) write(stdout,*) 'DEBUG:: get_dimensions ', &
         & 'kxdimid= ', kxdimid, &
         & 'kydimid= ', kydimid, &
         & 'nspecid=  ', nspecid, &
         & 'nkx =     ', nkx, &
         & 'nky =     ', nky, &
         & 'nspec =   ', nspec
# else
# endif
    nx=(nkx-1)*3/2+1
    ny=(nky-1)*3+1
    if(nx /= nx_input) then
       nx=nx_input
       if(debug) write(stdout,*) 'nx and nx_input',nx,nx_input
    endif
    if(ny /= ny_input) then
       ny=ny_input
       if(debug) write(stdout,*) 'ny and ny_input',ny,ny_input
    endif
  end subroutine get_dimensions

  subroutine get_boxsize(file_id,lx,ly)
    use constants, only: pi
    real, intent(out) :: lx,ly
    real :: dkx,dky
# ifdef NETCDF  
    integer (kind=kind_nf), intent(in) :: file_id
    integer (kind=kind_nf) :: kxvarid, kyvarid
# else
    integer, intent(in) :: file_id
# endif
# ifdef NETCDF
    istatus = nf90_inq_varid (file_id, "kx", kxvarid)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, file_id, var='kx')
    istatus = nf90_inq_varid (file_id, "ky", kyvarid)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, file_id, var='ky')
    istatus = nf90_get_var (file_id, kxvarid, dkx, start=(/ 2 /))
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, file_id, kxvarid)
    istatus = nf90_get_var (file_id, kyvarid, dky, start=(/ 2 /))
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, file_id, kyvarid)
    if(debug) write(stdout,*) 'DEBUG:: get_boxsize ', &
         & 'kxvarid= ',kxvarid, 'kyvarid= ',kyvarid
# else
# endif
    lx=2.*pi/dkx
    ly=2.*pi/dky
  end subroutine get_boxsize

  subroutine init_readvar(file_id,time_id,ntime,var_field_id,var_moment_id)
    integer, intent(out) :: ntime
    integer :: i
# ifdef NETCDF
    integer (kind=kind_nf), intent(in) :: file_id
    integer (kind=kind_nf), intent(out) :: time_id
    integer (kind=kind_nf), intent(out) :: var_field_id(:), var_moment_id(:)
    integer (kind=kind_nf) :: tdim_id
# else
    integer, intent(in) :: file_id
    integer, intent(out) :: time_id
    integer, intent(out) :: var_field_id(:), var_moment_id(:)
# endif
# ifdef NETCDF
    istatus = nf90_inq_varid (file_id, "t", time_id)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, file_id, var='t')

    istatus = nf90_inq_dimid (file_id, "t", tdim_id)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, file_id, var='t')
    istatus = nf90_inquire_dimension (file_id, tdim_id, len=ntime)
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, file_id, dimid=tdim_id)
    if(debug) write(stdout,*) 'DEBUG:: init_readvar ', &
         & 'tdim_id', tdim_id

    do i=1,size(var_field_id)
       istatus = nf90_inq_varid (file_id, varname_field(i), var_field_id(i))
       if (istatus /= NF90_NOERR) call netcdf_error &
            & (istatus, file_id, var=varname_field(var_field_id(i)))
       if(debug) write(stdout,*) 'DEBUG:: init_readvar ', &
            & varname_field(i), var_field_id(i)
    end do
    do i=1,size(var_moment_id)
       istatus = nf90_inq_varid (file_id, varname_moment(i), var_moment_id(i))
       if (istatus /= NF90_NOERR) call netcdf_error &
            & (istatus, file_id, var=varname_moment(i))
       if(debug) write(stdout,*) 'DEBUG:: init_readvar ', &
            & varname_moment(i), var_moment_id(i)
    end do
# else
# endif
  end subroutine init_readvar

  subroutine get_time(file_id,istep,time_id,time)
    integer, intent(in) :: istep
    real, intent(out) :: time
# ifdef NETCDF
    integer (kind=kind_nf), intent(in) :: file_id
    integer (kind=kind_nf), intent(in) :: time_id
# else
    integer, intent(in) :: file_id
    integer, intent(in) :: time_id
# endif
# ifdef NETCDF
    istatus = nf90_get_var (file_id, time_id, time, start=(/ istep /))
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, file_id, varid=time_id)
# else
# endif
  end subroutine get_time

  subroutine get_field(file_id,istep,var_id,nkx,nky,nx,ny,ptr,field, &
       & origin_center)
    use agk_transforms, only: transform2, inverse2
    use convert, only: r2c
    use kgrids, only: ikx, iky, kperp2
    use file_utils, only: input_unit_exist
# ifdef NETCDF
    use fields_arrays, only: phi_eq, apar_eq, bpar_eq
    integer (kind=kind_nf), intent(in) :: file_id
    integer (kind=kind_nf), intent(in) :: var_id
# else
    integer, intent(in) :: file_id
    integer, intent(in) :: var_id
# endif
    integer, intent(in) :: istep
    integer, intent(in) :: nkx,nky,nx,ny
    integer, intent(in) :: ptr
    real, intent(out) :: field(nx,ny)
    logical, intent(in) :: origin_center
    real :: field_ri(2,nkx,nky)
    complex :: field_c(nkx,nky)
    complex :: field_eq(nkx,nky)
    integer :: nnx,nny
    integer :: i,j

    real :: beta, zeff
    logical :: exist
    integer :: in_file
    
    namelist /parameters/ beta, zeff
    in_file = input_unit_exist("parameters",exist)
    if(exist) read(unit=in_file,nml=parameters)

    field_ri(1:2,1:nkx,1:nky)=0.; field_c(1:nkx,1:nky)=cmplx(0.,0.)
    field_eq(1:nkx,1:nky)=cmplx(0.,0.)
# ifdef NETCDF
    istatus = nf90_get_var (file_id, var_id, field_ri(1:2,1:nkx,1:nky), &
         & start=(/ 1,1,1,istep /),  count=(/ 2,nkx,nky,1 /))
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, file_id, var_id)
# else
# endif
    if(nx>nkx) then
       nnx=nx
    else
       nnx=(3*nkx+1)/2
    endif
    if(ny>nky) then
       nny=ny
    else
       nny=3*nky
    endif
    call r2c(field_c,field_ri)
 
    if(perturbed) then
       if(ptr==1) field_eq=phi_eq(0,:,:)
       if(ptr==2) field_eq=apar_eq(0,:,:)
       if(ptr==3) field_eq=bpar_eq(0,:,:)
       if(ptr==4) field_eq=apar_eq(0,:,:)
       field_c(:,:)=field_c(:,:)-field_eq(:,:)
    endif
    if(ptr==4) then
       field_c(:,:)=field_c(:,:)*kperp2(:,:)/beta*.5
    endif
    if(origin_center) then
       do i=1,nkx
          do j=1,nky
             if(mod(abs(ikx(i)+iky(j)),2)==1) &
                  field_c(i,j)=-field_c(i,j)
          end do
       end do
    endif
    call transform2(field_c,field,nny,nnx)
  end subroutine get_field

  subroutine get_moment(file_id,istep,var_id,spec,nkx,nky,nx,ny,nspec,ptr, &
       field,origin_center)
    use agk_transforms, only: transform2,inverse2
    use convert, only: r2c
    use kgrids, only: ikx, iky
# ifdef NETCDF
    use dist_fn_arrays, only: dens_eq, ux_eq, uy_eq, uz_eq
    use dist_fn_arrays, only: pxx_eq, pyy_eq, pzz_eq, pxy_eq, pyz_eq, pzx_eq
    integer (kind=kind_nf), intent(in) :: file_id
    integer (kind=kind_nf), intent(in) :: var_id
# else
    integer, intent(in) :: file_id
    integer, intent(in) :: var_id
# endif
    integer, intent(in) :: istep
    integer, intent(in) :: spec ! 0 = sum, positive int = corresponding specie
    integer, intent(in) :: nkx,nky,nx,ny,nspec
    integer, intent(in) :: ptr
    real, intent(out) :: field(nx,ny)
    logical, intent(in) :: origin_center
    real :: field_ri(2,nkx,nky)
    complex :: field_c(nkx,nky)
    complex :: field_eq(nkx,nky)
    integer :: nnx,nny
    integer :: i,j
    field_ri(1:2,1:nkx,1:nky)=0.; field_c(1:nkx,1:nky)=cmplx(0.,0.)
    field_eq(1:nkx,1:nky)=cmplx(0.,0.)

# ifdef NETCDF
    istatus = nf90_get_var (file_id, var_id, &
         & field_ri(1:2,1:nkx,1:nky), &
         & start=(/ 1,1,1,spec,istep /),  count=(/ 2,nkx,nky,1,1 /))
    if (istatus /= NF90_NOERR) &
         & call netcdf_error (istatus, file_id, var_id)
# else
# endif
    if(nx>nkx) then
       nnx=nx
    else
       nnx=(3*nkx+1)/2
    endif
    if(ny>nky) then
       nny=ny
    else
       nny=3*nky
    endif
    call r2c(field_c,field_ri)
    if(perturbed) then
       if(ptr==1) field_eq=dens_eq(0,:,:,spec)
       if(ptr==2) field_eq=ux_eq(0,:,:,spec)
       if(ptr==3) field_eq=uy_eq(0,:,:,spec)
       if(ptr==4) field_eq=uz_eq(0,:,:,spec)
       if(ptr==5) field_eq=pxx_eq(0,:,:,spec)
       if(ptr==6) field_eq=pyy_eq(0,:,:,spec)
       if(ptr==7) field_eq=pzz_eq(0,:,:,spec)
       if(ptr==8) field_eq=pxy_eq(0,:,:,spec)
       if(ptr==9) field_eq=pyz_eq(0,:,:,spec)
       if(ptr==10) field_eq=pzx_eq(0,:,:,spec)
       field_c(:,:)=field_c(:,:)-field_eq(:,:)
    endif

    if(origin_center) then
       do i=1,nkx
          do j=1,nky
             if(mod(abs(ikx(i)+iky(j)),2)==1) &
                  field_c(i,j)=-field_c(i,j)
          end do
       end do
    endif
    call transform2(field_c,field,nny,nnx)
  end subroutine get_moment

  ! FFT
  subroutine init_fft(nx,ny)
    use fft_work, only: init_crfftw, FFTW_BACKWARD
    integer, intent(in) :: nx, ny
    call init_crfftw (fft, FFTW_BACKWARD, ny, nx)
  end subroutine init_fft
  subroutine finish_fft
    use fft_work, only: delete_fft
    call delete_fft(fft)
  end subroutine finish_fft

  ! PGPLOT
  subroutine init_plot(device)
    use constants, only: kind_rs
    use pgplot_utils, only: palett
    character (len=*), intent(in) :: device
    integer :: pgopen
    integer :: col_def ! default color index
    real (kind=pgkind), parameter :: ch=1.
    real (kind=pgkind) :: vsf(2) ! view surface
    real (kind=pgkind) :: aspect ! aspect ratio
    real (kind=pgkind) :: xch, ych ! fonr height in x, y direction
    real (kind=pgkind) :: dum ! dummy

    if(pgopen(device).lt.1) stop 'cannot open graphic device'
    ! get default color
    call pgqci(col_def)
    ! define new color for index 7 (0-6) are pre-defined
    call pgscr(7,.54_pgkind,0._pgkind,.54_pgkind)
    ! set character font to 2=roman
    call pgscf(2)
    ! set character height
    call pgsch(ch)
    ! inquire the size of view surface
    call pgqvsz(1,dum,vsf(1),dum,vsf(2))
    aspect=vsf(2)/vsf(1)
    ! change the size of view surface
    call pgpap(vsf(1),aspect)
    ! inquire character height in normalized device coordinate
    call pgqcs(0,xch,ych)
    xch=ych*aspect
    pg_xch=xch ; pg_ych=ych
    ! set color palette for pgimg
    call palett(2,1._pgkind,.5_pgkind)
  end subroutine init_plot
  subroutine finish_plot
    call pgclos
    if(allocated(pg_vp)) deallocate(pg_vp)
  end subroutine finish_plot

  subroutine set_viewport(npanel,nrow,cwedge,wedge)
    ! calculate view ports for given number of panels
    !! plotting region size of one panel relative to paper size
    integer, intent(in) :: npanel, nrow
    character (len=*), intent(in) :: cwedge
    real (kind=pgkind), intent(out) :: wedge(2)
    integer :: ncol
    integer :: mrow, mcol
    integer :: ipanel
    
    ! wedge displacement and width in font size unit
    real (kind=pgkind), parameter :: wedge_disp(4) = (/ 4., 0.5, 4., -3. /)
    real (kind=pgkind), parameter :: wedge_width=3.
    ! relative size of paper margins (left-right-bottom-top)
    real (kind=pgkind) :: pmrgn(4) = (/ 0. ,0.05, 0., 0.05 /)
    ! relative size of margins for wedge displacement [unit=font size] (lrbt)
    real (kind=pgkind) :: amrgn(4) = (/ 0. ,0.  , 0., 0.   /)
    ! relative size of margins for wedge width (lrbt)
    real (kind=pgkind) :: wmrgn(4) = (/ 0. ,0.  , 0., 0.   /)
    real (kind=pgkind) :: len_row, len_col
    integer :: i

    allocate(pg_vp(4,npanel),stat=istatus)
    if(istatus /= 0) stop 'allocation error'

    ncol=int(ceiling(real(npanel)/real(nrow)))

    len_row=(1.-pmrgn(1)-pmrgn(2))/nrow
    len_col=(1.-pmrgn(3)-pmrgn(4))/ncol

    wedge(2)=wedge_width
    amrgn(1:4)=wedge_disp(1:4)
    if(cwedge(1:1) == 'L' ) then
       wedge(1)=abs(wedge_disp(1))
       amrgn(1)=wedge_disp(1)
       wmrgn(1)= wedge_width*pg_xch
    else if(cwedge(1:1) == 'R' ) then
       wedge(1)=abs(wedge_disp(2))
       amrgn(2)=wedge_disp(2)
       wmrgn(2)=-wedge_width*pg_xch
    else if(cwedge(1:1) == 'B' ) then
       wedge(1)=abs(wedge_disp(3))
       amrgn(3)=wedge_disp(3)
       wmrgn(3)= wedge_width*pg_ych
    else if(cwedge(1:1) == 'T' ) then
       wedge(1)=abs(wedge_disp(4))
       amrgn(4)=wedge_disp(4)
       wmrgn(4)=-wedge_width*pg_ych
    endif
    amrgn(1:2)=amrgn(1:2)*pg_xch
    amrgn(3:4)=amrgn(3:4)*pg_ych
    

    do ipanel=1,npanel
       mrow=mod(ipanel-1,nrow)+1
       mcol=ncol-(ipanel-1)/nrow
       pg_vp(1,ipanel)=pmrgn(1)+wmrgn(1)+amrgn(1)+(mrow-1)*len_row
       pg_vp(2,ipanel)=pmrgn(1)+wmrgn(2)+amrgn(2)+ mrow   *len_row
       pg_vp(3,ipanel)=pmrgn(3)+wmrgn(3)+amrgn(3)+(mcol-1)*len_col
       pg_vp(4,ipanel)=pmrgn(3)+wmrgn(4)+amrgn(4)+ mcol   *len_col
       if( pg_vp(1,ipanel) >= pg_vp(2,ipanel) .or. &
            & pg_vp(3,ipanel) >= pg_vp(4,ipanel) ) then
          write(stdout,'(i5,4f12.4)') ipanel, &
               & (pg_vp(i,ipanel),i=1,4)
          stop 'No Viewport!'
       endif
    end do
  end subroutine set_viewport

  subroutine set_coordinate(lx,ly,nx,ny,transform,origin_center,crop,ncrop)
    real, intent(in) :: lx, ly
    integer, intent(in) :: nx, ny
    real (kind=pgkind), intent(out) :: transform(6)
    logical, intent(in) :: origin_center
    real (kind=pgkind), intent(in) :: crop(:)
    integer, intent(out) :: ncrop(:)
    real (kind=pgkind) :: dx, dy
    real (kind=pgkind) :: xx(nx+1),yy(ny+1)
    real (kind=pgkind) :: crop_loc(size(crop))
    integer :: i,j
    dx=lx/nx
    dy=ly/ny
    do i=1,nx+1
       xx(i)=dx*(i-1)
    end do
    do j=1,ny+1
       yy(j)=dy*(j-1)
    end do
    if(origin_center) then
       do i=1,nx+1
          xx(i)=xx(i)-lx/2.
       end do
       do j=1,ny+1
          yy(j)=yy(j)-ly/2.
       end do
    end if
    transform=(/ xx(1)-dx, dx, 0._pgkind, &
         &    yy(1)-dy, 0._pgkind, dy /)

    ! cropping
    if(crop(1) >= crop(3) .or. crop(2) >= crop(4)) then
       crop_loc(1)=xx(1); crop_loc(3)=xx(nx+1)
       crop_loc(2)=yy(1); crop_loc(4)=yy(ny+1)
    else
       crop_loc(1:4)=crop(1:4)
    endif
    if(crop_loc(1) < xx(1)) crop_loc(1)=xx(1)
    if(crop_loc(2) < yy(1)) crop_loc(2)=yy(1)
    if(crop_loc(3) > xx(nx+1)) crop_loc(3)=xx(nx+1)
    if(crop_loc(4) > yy(ny+1)) crop_loc(4)=yy(ny+1)
    ncrop(1)=int((crop_loc(1)-xx(1))/dx)+1
    ncrop(2)=int((crop_loc(2)-yy(1))/dy)+1
    ncrop(3)=nx+1-int((xx(nx+1)-crop_loc(3))/dx)
    ncrop(4)=ny+1-int((yy(ny+1)-crop_loc(4))/dy)
    if(ncrop(1) < 1 .or. ncrop(2) < 1 .or. ncrop(3) > nx+1 .or. ncrop(4) > ny+1 ) &
         & stop 'cropping error in set_coordinate'
    pg_wzp=(/ xx(ncrop(1)), xx(ncrop(3)), yy(ncrop(2)), yy(ncrop(4)) /)
  end subroutine set_coordinate

  subroutine newpage(title, showgrid)
    use pgplot_utils, only: papergrid
    character (len=*), intent(in) :: title
    logical, intent(in), optional :: showgrid
    real (kind=pgkind) :: pg_tpos(2)=(/.5,.98/) ! title position
    call pgpage
    call pgsvp (0._pgkind,1._pgkind,0._pgkind,1._pgkind)
    call pgswin(0._pgkind,1._pgkind,0._pgkind,1._pgkind)
    call pgptxt(pg_tpos(1),pg_tpos(2),0._pgkind,.5_pgkind,title)
    if(present(showgrid)) then
       if(showgrid) call papergrid(.true.)
    end if
  end subroutine newpage

  subroutine newpanel(ipanel,xlabel,ylabel,title,aspect,wzp)
    integer, intent(in) :: ipanel
    character (len=*), intent(in) :: xlabel, ylabel, title
    logical, intent(in) :: aspect
    real (kind=pgkind), optional :: wzp(:)
    if(present(wzp)) pg_wzp(1:4)=wzp(1:4)
    call pgsvp( &
         & pg_vp(1,ipanel),pg_vp(2,ipanel), &
         & pg_vp(3,ipanel),pg_vp(4,ipanel))
    if(aspect) then
       call pgwnad(pg_wzp(1),pg_wzp(2),pg_wzp(3),pg_wzp(4))
    else
       call pgswin(pg_wzp(1),pg_wzp(2),pg_wzp(3),pg_wzp(4))
    endif
    call pgbox('BCNST',0._pgkind,0,'BCNST',0._pgkind,0)
    call pglab(xlabel,ylabel,title)
  end subroutine newpanel

  subroutine get_minmax(field,min,max)
    real, intent(in) :: field(:,:)
    real (kind=pgkind), intent(out) :: min, max
    min=minval(field(:,:))
    max=maxval(field(:,:))
    if(min > max) then
       write(stderr,*) 'Invalid minmax ',min,max
       stop
    else if (min==max) then
       write(stdout,*) 'field is constant: ',min,max
    endif
  end subroutine get_minmax
end module fields_plot_utils
