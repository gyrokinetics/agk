module antenna_data

! quick fix for now; put amplitudes here and thus be 
! certain that there are no weird module dependencies

  implicit none
  public

  complex, dimension(:), allocatable :: a_ant, b_ant
  ! (nk_stir)
  integer :: nk_stir
  logical :: ant_on = .false.
  !Random number seed variables
  integer, dimension(:),allocatable :: init_seed, fin_seed   !Initial and final seeds
  integer :: nseed                   !Length of random number  seed integer vector

contains

  subroutine init_antenna_data (nk_stir_in)
    use agk_mem, only: alloc8
    integer, intent (in) :: nk_stir_in
    logical :: initialized = .false.

! do not reallocate this array on this processor...

    if (initialized) return
    initialized = .true.

    if (nk_stir_in <= 0) then
       ant_on = .false.
       return
    else
       ant_on = .true.
    end if

    nk_stir = nk_stir_in

    allocate (a_ant(nk_stir)); call alloc8(c1=a_ant,v="a_ant")
    allocate (b_ant(nk_stir)); call alloc8(c1=b_ant,v="b_ant")

  end subroutine init_antenna_data
end module antenna_data
