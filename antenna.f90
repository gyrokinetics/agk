module antenna
!!  
!! Originally by Hammett, Dorland and Quataert, Dec. 5, 2001
!!
!! Provides random forcing for Alfven wave problem
!!
!! init_antenna should be called once per run
!! antenna_amplitudes provides the necessary info per time step
!!
!! added terms needed to calculate heating by antenna (apar_new)
!!
!! two kinds of namelists should be added to the input file:
!! 
!! driver:
!!       
!!    amplitude : RMS amplitude of | apar_antenna |
!!    w_antenna : frequency of driving, normalized to kpar v_Alfven
!!                Note: the imaginary part is the decorrelation rate
!!    nk_stir   : Number of k modes that should be driven
!!    write_antenna: .true. for direct antenna output to runname.antenna
!!                   default is .false.
!!
!! stir: (stir_1, stir_2, stir_3, etc., one for each k we want to drive)
!!
!!    kx, ky, kz : each of size nk_stir (not larger!)
!!               : list of k components that are to be driven; integers
!!    travel     : logical; choose false to make a standing wave
!!               : default value is .true.

  use antenna_data, only: nk_stir, a_ant, b_ant, nseed, init_seed, fin_seed
  implicit none
  complex :: w_antenna
  complex, dimension(:), allocatable :: w_stir
  real , dimension(:), allocatable :: k_amplitude !Separate amplitude for each mode
  complex, save, dimension (:,:,:), allocatable :: apar_new, apar_old
  integer, dimension(:), allocatable :: kx_stir, ky_stir, kz_stir
  real :: amplitude, t0, w_dot
  real :: mode_amplitude              !Allow Separate amplitude for each mode
  integer :: out_unit
  logical :: restarting = .false.
  logical :: no_driver = .false.
  logical :: write_antenna = .false.
  logical, dimension (:), allocatable :: trav
  logical :: ant_off = .false.
  real :: beta_s
  complex :: wtmp
  !Random number seed variables
  logical :: noseed                  !T=seed not present in restart file
  logical :: randomize               !Set seed based on date and time (and save in init_seed)
 
  private
  public :: init_antenna, dump_ant_amp, amplitude, finalize_antenna
  public :: nseed, init_seed, fin_seed
  public :: antenna_w, antenna_apar, antenna_amplitudes, a_ext_data

contains
!-----------------------------------------------------------------------------
  subroutine init_antenna
    use mp, only: proc0
    use species, only: spec, nspec, init_species
    use run_parameters, only: beta
    use antenna_data, only: init_antenna_data
    use agk_mem, only: alloc8
    use ran, only: get_rnd_seed_length
    use file_utils, only: error_unit
    implicit none
    logical, save :: initialized = .false.
    integer :: i
    integer :: ierr

    if (initialized) return
    initialized = .true.

    call init_species
    
    !Get random number seed length and allocate init_seed and fin_seed
    !   NOTE: Still need to account for this allocation in agk_mem 
    nseed=get_rnd_seed_length()
    allocate(init_seed(1:nseed)); init_seed=0
    allocate(fin_seed(1:nseed)); fin_seed=0
    if (nseed .ne. 2) then
       ierr=error_unit()
       if (proc0) write(ierr,'(a,i6,a)')'Warning: Random number seed length nseed = ',nseed,' /= 2'
    endif 

    call read_parameters
    if (no_driver) then
       i = -1
       call init_antenna_data (i)
       return
    end if

    allocate (w_stir(nk_stir)); call alloc8(c1=w_stir,v="w_stir")
    
    beta_s = 0.
    if (beta > epsilon(0.0)) then
       do i=1,nspec
          !GGH What does this mean?  Is this a normalization?
          !GGH This converts from input frequency (normalized to kpar vA)
          !    to the code frequency
          !GGH This seems to give beta_s = 2*n0*T0/v_A^2
          beta_s = beta_s + beta*spec(i)%dens*spec(i)%mass
       end do
    else
       beta_s = 2.
    end if

  end subroutine init_antenna
!-----------------------------------------------------------------------------
! NOTE: As of 20 JAN 2012, this routine is not called, so init_seed and fin_seed
!       are never deallocated
  subroutine finalize_antenna
    implicit none
    !Note: this will be called whether or not the antenna is used
    deallocate (init_seed,fin_seed)
  end subroutine finalize_antenna
!-----------------------------------------------------------------------------
  subroutine read_parameters 
    ! <doc>
    !  Read parameters for antenna driver:
    !  1) Random seed initialization: 
    !     If seed is specified in input file, 
    !        use it
    !     else if no seed in input file:
    !        If not restarting:
    !           randomize seed
    !        else if restarting:
    !           If seed is present in .nc.### restart file:
    !              use it
    !           else randomize
    !   2) If MT random number generator is used, randomize should
    !       always be used.
    ! </doc>
    use file_utils
    use mp, only: proc0, broadcast, mp_abort
    use antenna_data, only: init_antenna_data
    use agk_save, only: init_ant_amp
    use agk_mem, only: alloc8
    use ran, only: init_ranf
    implicit none
    complex :: a, b
    integer :: kx, ky, kz, i,in_file, ierr
    logical :: exist, travel
    integer :: ireaderr1=0
    integer, allocatable :: ireaderr2(:)
    integer  :: seed1, seed2  !Two integer seeds
    logical :: abort_readerr=.true.
    character (len=500) :: msg

    namelist /driver/ amplitude, w_antenna, nk_stir, write_antenna, ant_off, &
         w_dot, t0, restarting, seed1, seed2, randomize
    namelist /stir/ mode_amplitude,kx, ky, kz, travel, a, b

    !Set Default values
    t0 = -1.
    w_dot = 0.
    w_antenna = (1., 0.0)
    amplitude = 0.
    nk_stir = 1
    ant_off = .false.
    write_antenna=.false.
    restarting=.false.
    seed1= -1
    seed2= -1
    randomize=.false.

    if (proc0) then
       in_file=input_unit_exist("driver",exist)
       if (.not. exist) then
          no_driver = .true.
       else

          read (unit=in_file, nml=driver, iostat=ireaderr1)

          no_driver = ant_off
       end if
    end if

    call broadcast (ireaderr1)
    if (abort_readerr) then
       if (ireaderr1 > 0) then
          call mp_abort ('Read error at driver')
       else if (ireaderr1 < 0) then
          call mp_abort ('End of file/record occurred at driver')
       endif
    endif

    call broadcast (no_driver)
    if (no_driver) return

    call broadcast (nk_stir)  ! this statement corrects a bug? BD

    call init_antenna_data (nk_stir)
    allocate (kx_stir(nk_stir)); call alloc8(i1=kx_stir,v="kx_stir")
    allocate (ky_stir(nk_stir)); call alloc8(i1=ky_stir,v="ky_stir")
    allocate (kz_stir(nk_stir)); call alloc8(i1=kz_stir,v="kz_stir")
    allocate (k_amplitude(nk_stir)); call alloc8(r1=k_amplitude,v="k_amplitude")
    allocate (trav(nk_stir)) !!! RN> unable to account memory for logical variables
          
! BD
! get initial antenna amplitudes from restart file
! if none are found, a_ant = b_ant = 0 will be returned
! and ierr will be non-zero.  
!
! TO DO: need to know if there IS a restart file to check...
!
    allocate(ireaderr2(nk_stir))
    if (proc0) then
       ierr = 1
       noseed=.false.
       !NOTE: init_ant_amp checks that seed length nseed matches restart file.
       !    If not, it just specifies noseed=.true.
       if (restarting) call init_ant_amp (a_ant, b_ant, nk_stir, nseed, &
            noseed, init_seed, ierr)

       do i=1,nk_stir
          call get_indexed_namelist_unit (in_file, "stir", i)
          kx=1
          ky=1
          kz=1
          a = -1.
          b = -1.
          mode_amplitude=amplitude
          travel = .true.
          ! check io error
          read (unit=in_file, nml=stir, iostat=ireaderr2(i))
          close(unit=in_file)
          kx_stir(i) = kx 
          ky_stir(i) = ky
          kz_stir(i) = kz
          k_amplitude(i)=mode_amplitude
          trav(i) = travel
! If a, b are not specified in the input file:
          if (a == -1.0 .and. b == -1.0) then
! And if a, b are not specified in the restart file 
! (else use values from restart file by default)
             if (ierr /= 0) then                   
                a_ant(i) = k_amplitude(i)*cmplx(1.,1.)/2. 
                b_ant(i) = k_amplitude(i)*cmplx(1.,1.)/2. 
             end if
! Else if a, b ARE specified in the input file (ignore restart file):
          else
             a_ant(i) = a
             b_ant(i) = b
          end if
       end do

       !Set random seed (Note that only proc0 uses ranf, so only proc0 needs initialization)
       !Check that both random seed and randomize are not specified together
       if ( (seed1 .ne. -1 .or. seed2 .ne. -1) .and. randomize) then
          ierr = error_unit()
          write(ierr,*) "Both random seed and randomize are specified for antenna."
          write(ierr,*) "Specified random seed will be used, ignoring randomize=T."
       endif
       !If a random seed is specfied in input file, use it
       if (seed1 .ne. -1 .or. seed2 .ne. -1) then
          init_seed(1)=seed1
          if (nseed .ge. 2) init_seed(2)=seed2
          call init_ranf(.false.,init_seed)
       else !Otherwise, check for  random seed from restart file or randomize
          if (restarting .and. .not. noseed .and. .not. randomize) then !Use seed value from restart .nc.### file
             call init_ranf(.false.,init_seed)
          else !Otherwise, randomize
             call init_ranf(.true.,init_seed)
          endif
       endif
    end if

    call broadcast (ireaderr2)
    if (abort_readerr) then
       do i=1,nk_stir
          if (ireaderr2(i) > 0) then
             write(msg,*) '  Read error at stir: ',i
             call mp_abort (trim(msg))
          else if (ireaderr2(i) < 0) then
             write(msg,*) '  End of file/record occurred at stir: ',i
             call mp_abort (trim(msg))
          endif
       end do
    endif

    if (proc0 .and. write_antenna) then
       !NOTE: If write_antenna=T, this out_unit is never closed
       call get_unused_unit (out_unit)
       call open_output_file (out_unit, ".antenna")
    end if

    call broadcast (w_antenna)
    call broadcast (w_dot)
    call broadcast (t0)
    call broadcast (amplitude)
!    call broadcast (nk_stir)  with bug fix this is not needed BD
    call broadcast (write_antenna)
    call broadcast (restarting)
    call broadcast (init_seed)

!!$    if (.not. proc0) then
!!$       allocate (kx_stir(nk_stir)); call alloc8(i1=kx_stir,v="kx_stir")
!!$       allocate (ky_stir(nk_stir)); call alloc8(i1=ky_stir,v="ky_stir")
!!$       allocate (kz_stir(nk_stir)); call alloc8(i1=kz_stir,v="kz_stir")
!!$       allocate (k_amplitude(nk_stir)); call alloc8(r1=k_amplitude,v="k_amplitude")
!!$       call init_antenna_data (nk_stir)
!!$       allocate (trav(nk_stir)) !!! RN> unable to account memory for logical variables
!!$    end if
    
    call broadcast (trav)
    call broadcast (kx_stir)
    call broadcast (ky_stir)
    call broadcast (kz_stir)
    call broadcast (k_amplitude)
    call broadcast (a_ant)
    call broadcast (b_ant)
       
  end subroutine read_parameters
!-----------------------------------------------------------------------------
  subroutine antenna_amplitudes (apar)
    ! <doc>
    !  antenna_amplitudes determines apar for the external antenna
    !  Note that random numbers are generated on proc0 and broadcast
    ! </doc>

    use mp
    use agk_time, only: dtime, time
    use kgrids, only: naky, nakx, reality
    use theta_grid, only: theta, ntgrid, gradpar
    use constants
    use agk_mem, only: alloc8
    use ran, only: ranf,get_rnd_seed
    implicit none

    complex, dimension (-ntgrid:,:,:), intent(out) :: apar
    complex :: force_a, force_b
    real :: dt, sigma
    integer :: i, it, ik

    logical, save :: first = .true.

    apar = 0.
    if (no_driver) return

    if (first) then
       allocate (apar_new (-ntgrid:ntgrid, nakx, naky)) ; call alloc8 (c3=apar_new, v="apar_new")
       allocate (apar_old (-ntgrid:ntgrid, nakx, naky)) ; call alloc8 (c3=apar_old, v="apar_old")
       apar_new = 0. 
       first = .false.
    end if

    apar_old = apar_new

! TT: variable step AB3
!    dt = dtime
    dt = dtime(1)
! <TT

    if (time > t0) then       
       wtmp = w_antenna + w_dot*(time-t0)
    else
       wtmp = w_antenna
    end if

! GGH fixed a bug with the frequency sweeping here.  11.17.05
! NOTE: This converts from (kpar v_A) to (kpar v_t0) normalization
    do i = 1, nk_stir
! TT> added condition
       w_stir(i) = gradpar*abs(kz_stir(i))*sqrt(1./beta_s) * wtmp
!!$       if (aant) then
!!$          w_stir(i) = gradpar*abs(kz_stir(i))*sqrt(1./beta_s) * wtmp
!!$       else
!!$          w_stir(i) = wtmp
!!$       end if
! <TT
    end do

    do i=1, nk_stir
       
       it = 1 + mod(kx_stir(i)+nakx,nakx)
       ik = 1 + mod(ky_stir(i)+naky,naky)

       if (proc0) then
          !GGH Decorrelation
          sigma = sqrt(12.*abs(aimag(w_stir(i)))/dt)*k_amplitude(i)

          force_a = cmplx(ranf() - 0.5, ranf() - 0.5) * sigma
          if (trav(i)) then
             force_b = cmplx(ranf() - 0.5, ranf() - 0.5) * sigma
          else
             force_b = force_a
          end if
          
          if (trav(i) .and. abs(aimag(w_stir(i))) < epsilon(0.0)) then
             a_ant(i) = a_ant(i)*exp(-zi*w_stir(i)*dt)+force_a*dt
             b_ant(i) = 0.
          else
             a_ant(i) = a_ant(i)*exp(-zi*w_stir(i)*dt)+force_a*dt
             b_ant(i) = b_ant(i)*exp(zi*conjg(w_stir(i))*dt)+force_b*dt
          end if
! TT> testing
!!$          a_ant(i) = cmplx(ranf() - 0.5, ranf() - 0.5) * amplitude
!!$          b_ant(i) = cmplx(ranf() - 0.5, ranf() - 0.5) * amplitude
! <TT
       end if

! potential existed for a bug here, if different processors 
! got different random numbers; antennas driving different parts of
! velocity space at the same k could be driven differently.  BD 6.7.05

       !Get random seed at this point in calculation and broadcast
       if (proc0) call get_rnd_seed(fin_seed)

       call broadcast (a_ant)
       call broadcast (b_ant)
       call broadcast (fin_seed)

!       if (time < t0) then
!          apar(:,it,ik) = apar(:,it,ik) &
!               + (a_ant(i)+b_ant(i))/sqrt(2.)*exp(zi*kz_stir(i)*theta) &
!               * (0.5-0.5*cos(time*pi/t0)
!       else
! NOTE: Is the apar(:,it,ik) unnecessary on the RHS here (=0)?
! ANSWER: No, we are inside the nk_stir loop.  In general case, apar /= 0 here.
          apar(:,it,ik) = apar(:,it,ik) &
               + (a_ant(i)+b_ant(i))/sqrt(2.)*exp(zi*kz_stir(i)*theta) 
!       end if

       if (write_antenna) then
         if (proc0) write(out_unit, fmt='(8(1x,e13.6),1x,i2)') &
	 & time, real((a_ant(i)+b_ant(i))/sqrt(2.)), &
         &     aimag((a_ant(i)+b_ant(i))/sqrt(2.)),real(a_ant(i)), aimag(a_ant(i)), &
         &     real(b_ant(i)), aimag(b_ant(i)), real(wtmp), i
       end if
    end do

    if (reality) then
       apar(:,1,1) = 0.0
       
       do it = 1, nakx/2
          apar(:,it+(nakx+1)/2,1) = conjg(apar(:,(nakx+1)/2+1-it,1))
       enddo
    end if

    !GGH NOTE: Here apar_new is equal to apar+ apar_ext 
    !NO!-- The variable apar is strictly local, so it includes only apar_ext!
    apar_new = apar


  end subroutine antenna_amplitudes

!-----------------------------------------------------------------------------

  subroutine antenna_apar (kperp2, j_ext)
!      GGH ERR? Is this correct? It uses apar_new rather than just the applied
!      current, so does this include the plasma response? 
!      BD: No error.  Here, apar and apar_new are local variables for the antenna only. 7.1.06
!
!      this routine returns (kperp**2 * A_ext) at the current time

    use kgrids, only: naky, nakx
    use run_parameters, only: beta, fapar
    use theta_grid, only: ntgrid

    complex, dimension (-ntgrid:,:,:) :: j_ext
    real, dimension (:,:) :: kperp2
    integer :: ik, it, ig

    if (fapar > epsilon(0.0)) then

       if (.not. allocated(apar_new)) return
       
       do ik = 1, naky
          do it = 1, nakx
             do ig = -ntgrid, ntgrid
                j_ext(ig,it,ik) = kperp2(it,ik)*apar_new(ig,it,ik)
             end do
          end do
       end do
     
! GGH Is this some normalization?  Yes.
       if (beta > 0.) j_ext = j_ext * 0.5 / beta

    else ! if apar itself is not being advanced in time, there should be no j_ext
       j_ext = 0.
    end if

  end subroutine antenna_apar

!-----------------------------------------------------------------------------

  subroutine a_ext_data (A_ext_old, A_ext_new)
!      this routine returns current and previous A_ext vectors
    use theta_grid, only: ntgrid

    complex, dimension (-ntgrid:,:,:) :: A_ext_old, A_ext_new

    if (.not. allocated(apar_new)) then
       A_ext_old = 0.
       A_ext_new = 0.
       return
    else
       A_ext_old = apar_old
       A_ext_new = apar_new
    end if

  end subroutine a_ext_data
!-----------------------------------------------------------------------------
  function antenna_w ()
    complex :: antenna_w

    antenna_w = wtmp

  end function antenna_w
!-----------------------------------------------------------------------------
  subroutine dump_ant_amp
    use agk_time, only: time
    use mp, only: proc0
    implicit none
    integer :: i

    if (no_driver) return
!    if (.not. write_antenna) then
    if (write_antenna) then
       do i=1,nk_stir
         if (proc0) write(out_unit, fmt='(7(1x,e13.6),1x,i2)') &
	 & time, real((a_ant(i)+b_ant(i))/sqrt(2.)), &
         &     aimag((a_ant(i)+b_ant(i))/sqrt(2.)),real(a_ant(i)), aimag(a_ant(i)), &
         &     real(b_ant(i)), aimag(b_ant(i)),i
  
       end do
    end if

  end subroutine dump_ant_amp
!-----------------------------------------------------------------------------
end module antenna

