module egrid

! By Tomo Tatsuno, Aug 2005
! Improved accuracy and speed and maximum number of energy grid points
!

  implicit none

  public :: energy, xgrid, setegrid, setvgrid, init_egrid
  public :: zeroes, x0

  private

  real :: x0
  real, dimension (:), allocatable :: zeroes

contains

  subroutine init_egrid (negrid)
    
    use agk_mem, only: alloc8

    integer, intent (in) :: negrid
    logical :: first = .true.

    if (first) then
       first = .false.
       allocate (zeroes(negrid)); call alloc8(r1=zeroes,v="zeroes"); zeroes = 0.
    end if

  end subroutine init_egrid

  subroutine setegrid (Ecut, negrid, epts, wgts, test)

    use gauss_quad, only: get_legendre_grids_from_cheb
    use file_utils, only: error_unit
    use agk_mem, only: alloc8
    implicit none

    integer, intent (in) :: negrid
    real, intent (inout) :: ecut
    real, dimension (:), intent (out) :: epts, wgts
    logical, intent (in) :: test

    integer :: ie, np
    integer :: ier, ierr

    call init_egrid (negrid)
    
    if (Ecut > 20.0) then
       ierr = error_unit()
       write (ierr,*) 'WARNING: E -> x transformation is not numerically correct for such a large Ecut'
       write (ierr,*) 'WARNING: x(E=20) =', xgrid(20.), ' is too close to 1.0'
       write (ierr,*) 'WARNING: instead we have removed ecut and make the whole e-space spectral'
       Ecut = -1.0
    end if

    if (negrid > 4096) then
       ierr = error_unit()
       write (ierr,*) 'WARNING: We have never checked if such a large number of negrid works.'
       write (ierr,*) 'WARNING: Recommend using fewer number (negrid <= 4096)'
    end if

    if (Ecut > 0.0) then
       np = negrid-1
       x0 = xgrid(ecut)      ! function xgrid
    else ! negative Ecut uses whole energy domain
       np = negrid
       x0 = 1.0
    end if

    call get_legendre_grids_from_cheb (0., x0, zeroes(1:np), wgts(1:np))

    if (test) then
       print*, 'zeroes in setegrid: ie, zeroes(ie)'
       do ie=1, np
          print*, ie, zeroes(ie)
       end do
       write (*,*) 'x0 & 1-x0 are ', x0, 1.-x0
       ! z format specifier doesn't work for g95
!       write (*,'(a,2z20)') 'x0 & 1-x0 are in hex ', x0, 1.-x0
    end if

    do ie=1, np
       epts(ie) = energy(zeroes(ie), Ecut, ier)
       if (ier /= 0) then
          ierr = error_unit()
          write (ierr, '(a,i8,2(a,f16.10))') 'ERROR in ie= ', ie, &
               & ', zero= ', zeroes(ie), ', epts= ', epts(ie)
       end if
    end do

    if (Ecut > 0.0) then
       zeroes(negrid) = x0
       epts(negrid) = ecut
       wgts(negrid) = 1.-x0

       if (wgts(negrid) > wgts(np)) then
          ierr = error_unit()
          write (ierr,*) 'WARNING: weight at ecut: ', wgts(negrid)
          write (ierr,*) 'WARNING: is larger than the one at lower grid: ', wgts(np)
          write (ierr,*) 'WARNING: Recommend using fewer energy grid points'
          if (ecut < 20.0) &
               & write (ierr,*) 'WARNING: or you should increase ecut (<= 20)'
       end if
    end if

    if (test) then
       print *, 'energy grid points: ie, e, w and v'
       do ie=1, negrid
          write (*,'(i8,3e15.5)') ie, epts(ie), wgts(ie), sqrt(epts(ie))
       end do
       print *, 'sum of weights in setegrid: ', sum(wgts)
    end if

  end subroutine setegrid

  function energy (xeval, ecut, ier)
    real, intent (in) :: xeval, ecut
    real :: xerrbi, xerrsec, a, b, energy
    integer, intent (out) :: ier

    xerrbi = epsilon(a)**0.3
    xerrsec = epsilon(a) * 2.0

    a = 0.0
    if (ecut > 0.0) then
       b = ecut
    else
       b = 20.
    end if
    call roote (xeval, a, b, xerrbi, xerrsec, 1, ier, energy)

  end function energy

  subroutine roote (fval, a, b, xerrbi, xerrsec, nsolv, ier, soln)

    use mp, only: proc0
    use file_utils, only: error_unit
    !
    ! solve xgrid(E) == fval for E
    !   xerrbi: error max in x for bisection routine
    !   xerrsec: error max in x for secant (Newton-Raphson) method
    !
    integer, intent(in) :: nsolv
    integer, intent(out) :: ier
    real, intent(in) :: fval, a, b, xerrbi, xerrsec
    real, intent(out) :: soln
    integer, parameter :: maxit=30 ! maximum iteration for Newton scheme
    integer :: i, niter, isolv, ierr
    real :: a1, b1, f1, f2, f3, trial, aold

    ier=0
    a1=a
    b1=b
    f1=xgrid(a1)-fval
    f2=xgrid(b1)-fval

    if (xerrbi > 0.) then
       if (f1*f2 < 0.) then
          niter = int(log(abs(b-a)/xerrbi)/log(2.))+1
          do i=1, niter
             trial = 0.5*(a1+b1)
             f3 = xgrid(trial) - fval
             if (f3*f1 > 0.) then
                a1 = trial
                f1 = f3
             else
                b1 = trial
                f2 = f3
             endif
             !      write(11,*) 'i,a1,f1,b1,f2 ',i,a1,f1,b1,f2
          enddo
       else
          if (proc0) then
             ierr = error_unit()
             write(ierr,*) 'f1 and f2 have same sign in bisection routine'
             write(ierr,*) 'a1,f1,b1,f2=',a1,f1,b1,f2
             write(ierr,*) 'fval=',fval
          end if
          ier=1
       endif
    end if

    ! to make (a1,f1) the closest
    if ( abs(f1) > abs(f2) ) then
       f1=f2
       aold=a1
       a1=b1
       b1=aold
    endif

! Newton-Raphson method (used to be secant method)
    isolv = 0
    do i=1, maxit
       b1 = a1
       a1 = a1 - f1 / xgrid_prime(a1)
! TT: The next line is too severe for large ecut
!       if (abs(a1-b1) < xerrsec) isolv = isolv+1
       if ( min(abs((a1-b1)/a1),abs(f1)/a1) < xerrsec ) isolv = isolv+1
       if (isolv >= nsolv) exit
       f1 = xgrid(a1) - fval
    end do

    if (i > maxit) then
       if (proc0) then
          ierr = error_unit()
          write (ierr,*) 'le_grids:roote: bad convergence'
       end if
       ier = 1
    end if

    soln = a1

  end subroutine roote

  elemental function xgrid (e)
    !
    ! This is a function
    !              2           E
    !   x(E) = ---------- * int   exp(-e) sqrt(e) de
    !           sqrt(pi)       0
    ! which gives energy integral with a Maxwellian weight
    ! x is a monotonic function of E with
    !    E | 0 -> infinity
    !   -------------------
    !    x | 0 -> 1
    ! The integral is an error function and numerical evaluation is
    ! obtained from the formula of incomplete gamma function
    !
    use constants, only: pi => dpi
    real, intent (in) :: e
    real :: xgrid
    double precision :: xg, denom
    integer :: kmax, k

    kmax = 100
    xg = dble(0.0)
    denom = dble(1.0)
    do k = 0, kmax
       denom = denom * dble(1.5+k)
       xg = xg + dble(e)**dble(1.5+k) / denom
    end do

    xgrid = xg * exp(-dble(e)) * dble(2.) / sqrt(pi)

  end function xgrid

  elemental function xgrid_prime (e)
    !
    ! This is a function
    !               2
    !   x'(E) = ---------- * exp(-e) sqrt(e)
    !            sqrt(pi)
    !
    real, intent (in) :: e
    real :: xgrid_prime
    double precision :: pi

    pi = asin(dble(1.0)) * dble(2.0)
    xgrid_prime = exp(-dble(e)) * sqrt(dble(e)) * dble(2.) / sqrt(pi)

  end function xgrid_prime

  subroutine setvgrid (vcut, negrid, epts, wgts, nesub, test)

    use constants, only: pi
    use gauss_quad, only: get_legendre_grids_from_cheb, get_laguerre_grids
    use agk_mem, only: alloc8

    implicit none
    
    integer, intent (in) :: negrid
    real, intent (in) :: vcut
    integer, intent (in) :: nesub
    real, dimension (:), intent (out) :: epts, wgts
    logical, intent (in) :: test
    integer :: ie

    call init_egrid (negrid)
    
    ! get grid points in v up to vcut (epts is not E yet)
    call get_legendre_grids_from_cheb (0., vcut, epts(:nesub), wgts(:nesub))

    ! change from v to E
    epts(:nesub) = epts(:nesub)**2

    ! absorb exponential and volume element in weights
    wgts(:nesub) = wgts(:nesub)*epts(:nesub)*exp(-epts(:nesub))/sqrt(pi)

    if (negrid > nesub) then

       ! get grid points in y = E - vcut**2 (epts not E yet)
       call get_laguerre_grids (epts(nesub+1:), wgts(nesub+1:))

       ! change from y to E
       epts(nesub+1:) = epts(nesub+1:) + vcut**2

       ! absorb exponential and volume element in weights
       wgts(nesub+1:) = wgts(nesub+1:)*0.5*sqrt(epts(nesub+1:)/pi)*exp(-vcut**2)

    end if

!    zeroes = sqrt(epts(:negrid-1))
    zeroes = sqrt(epts)
    x0 = sqrt(epts(negrid)) ! TT: needed?

    if (test) then
       print *, 'energy grid points: ie, e, w and v'
       do ie=1, negrid
          write (*,'(i8,3e15.5)') ie, epts(ie), wgts(ie), sqrt(epts(ie))
       end do
       print *, 'sum of weights in setvgrid: ', sum(wgts)
    end if

  end subroutine setvgrid

end module egrid

module le_grids
  
  implicit none

  public :: init_le_grids, integrate_moment, integrate_species
  public :: e, dele, al, delal
  public :: w, wl
  public :: negrid, nlambda, ng2
  public :: xx, testfac, ecut
  public :: eint_error, lint_error, integrate_test
! MAB>
  public :: init_weights, init_vspectrum, vtransform2d
  public :: lpv, lpl, hpvpa, aj0p
  public :: nvpa, nvpe
! <MAB
  public :: npgrid ! TT: number of Hankel grid
  public :: vgrid, nesub

  private

  interface integrate_species
     module procedure integrate_species0, integrate_species1
     module procedure integrate_species_hankel
  end interface
  interface integrate_moment
     module procedure integrate_moment_c34, integrate_moment_r34
     module procedure integrate_moment_c23, integrate_moment_r23
     module procedure integrate_moment_lec, integrate_moment_ler
     module procedure integrate_moment_lz, integrate_moment_accelx
  end interface

  real, dimension (:,:), allocatable :: e, w, dele ! (negrid,nspec)
  real, dimension (:), allocatable :: al, delal, wl ! (nlambda)

  real, dimension (:), allocatable :: xx ! (nlambda)
  real, dimension (:,:), allocatable, save :: werr, wlerr

  real, dimension (:,:), allocatable :: lpv, lpl
  real, dimension (:,:,:), allocatable :: hpvpa, aj0p

 ! knobs
  integer :: ngauss, negrid
  real :: ecut

  integer :: nlambda, ng2
  logical :: accel_x = .false.
  logical :: accel_v = .false.
  logical :: test = .false.

  integer :: testfac = 1
  integer :: nmax = 500
  real :: wgt_fac = 10.0

  logical :: vgrid
  real :: vcut
  integer :: nesub, nesuper

  integer :: nvpa, nvpe  ! MAB
  integer :: npgrid ! TT: number of Hankel grid

contains

  subroutine init_le_grids (accelerated_x, accelerated_v)
    use mp, only: proc0, finish_mp
    use species, only: init_species
    use theta_grid, only: init_theta_grid
    use kgrids, only: init_kgrids
    use agk_layouts, only: init_agk_layouts
    implicit none
    logical, intent (out) :: accelerated_x, accelerated_v
    logical, save :: initialized = .false.
    integer :: il, ie

    if (initialized) return
    initialized = .true.

    call init_agk_layouts
    call init_species
    call init_theta_grid
    call init_kgrids

    if (proc0) then
       call read_parameters
       call set_grids
    end if
    call broadcast_results
    call init_integrations

    accelerated_x = accel_x
    accelerated_v = accel_v

    if (test) then
       if (proc0) then
          print*, 'lambda grids:'
          do il = 1, nlambda
             write(*,*) al(il)
          end do
          write(*,*) 
          print*, 'energy grids:'
          do ie = 1, negrid
             write(*,*) e(ie,1)
          end do
       end if
!       call finish_mp
!       stop
    endif
    
  end subroutine init_le_grids

  subroutine broadcast_results
    use mp, only: proc0, broadcast
    use species, only: nspec
    use egrid, only: zeroes, x0, init_egrid
    use agk_mem, only: alloc8
    implicit none
    integer :: is!, tsize

    call broadcast (ngauss)
    call broadcast (negrid)
    call broadcast (ecut)
    call broadcast (nlambda)
    call broadcast (ng2)
    call broadcast (test)
    call broadcast (testfac)
    call broadcast (nmax)
    call broadcast (wgt_fac)
    call broadcast (vgrid)
    call broadcast (vcut)
    call broadcast (nesub)
    call broadcast (nesuper)
    call broadcast (npgrid)

    if (.not. proc0) then
       allocate (e(negrid,nspec)); call alloc8(r2=e,v="e")
       allocate (w(negrid,nspec)); call alloc8(r2=w,v="w")
       allocate (dele(negrid,nspec)); call alloc8(r2=dele,v="dele")
       allocate (al(nlambda)); call alloc8(r1=al,v="al")
       allocate (delal(nlambda)); call alloc8(r1=delal,v="delal")
       allocate (wl(nlambda)); call alloc8(r1=wl,v="wl")
       allocate (xx(nlambda)); call alloc8(r1=xx,v="xx")
    end if

    call init_egrid (negrid)

    call broadcast (al)
    call broadcast (delal)
    call broadcast (xx)
    call broadcast (x0)
    call broadcast (zeroes)

    do is = 1, nspec
       call broadcast (e(:,is))
       call broadcast (w(:,is))
       call broadcast (dele(:,is))
    end do
    call broadcast (wl)

  end subroutine broadcast_results

  subroutine read_parameters
    use file_utils, only: input_unit, error_unit, input_unit_exist
    use mp, only: mp_abort
    implicit none
    integer :: in_file
    logical :: exist
    integer :: ireaderr=0
    logical :: abort_readerr=.true.

    namelist /le_grids_knobs/ ngauss, negrid, ecut, test, &
         testfac, nmax, wgt_fac, vgrid, vcut, nesub, nesuper

    ngauss = 8    ! Note: nlambda = 2 * ngauss
    nesub = 8
    nesuper = 2
    negrid = -10  ! determined from nesub & nesuper by default
    vcut = 4.0
    vgrid = .false.
    ecut = 6.0    ! new default value for advanced scheme
    in_file=input_unit_exist("le_grids_knobs", exist)
    if (exist) read (unit=in_file, nml=le_grids_knobs, iostat=ireaderr)

    if (abort_readerr) then
       if (ireaderr > 0) then
          call mp_abort ('Read error at le_grids_knobs')
       else if (ireaderr < 0) then
          call mp_abort ('End of file/record occurred at le_grids_knobs')
       endif
    endif

    if (negrid <= 0) then
       negrid = nesub + nesuper
    else if (negrid /= nesuper + nesub) then
! Report problem to error file, and continue, using nesuper and negrid
       nesub = negrid - nesuper
       if (vgrid) write (unit=error_unit(), fmt='("Forcing nesub = ",i5)') nesub
    end if
    npgrid = negrid / 2 ! TT: number of Hankel grid

  end subroutine read_parameters

  subroutine init_integrations
    use mp, only: nproc
    use theta_grid, only: ntgrid
    use kgrids, only: nakx, naky
    use species, only: nspec
    use agk_layouts, only: init_dist_fn_layouts, pe_layout
    implicit none
    character (1) :: char
    logical :: first = .true.

    call init_dist_fn_layouts (ntgrid, naky, nakx, nlambda, negrid, nspec)

    if (first) then
       first = .false.
       call pe_layout (char)
       if (char == 'x') then
          accel_x = mod(nakx*naky*nspec, nproc) == 0
          accel_v = .false.
       end if
       if (char == 'v') then
          accel_x = .false.
          accel_v = mod(negrid*nlambda*nspec, nproc) == 0
       end if
    end if
          
  end subroutine init_integrations

  subroutine init_weights

    use file_utils, only: open_output_file, close_output_file
    use egrid, only: x0, zeroes
    use constants, only: pi
    use agk_mem, only: alloc8, dealloc8

    implicit none

    real, dimension (:), allocatable :: modzeroes, werrtmp  ! (negrid-2)
    real, dimension (:), allocatable :: lmodzeroes, wlerrtmp ! (nlambda-1)
    integer :: ipt, ndiv, divmax, np
    logical :: eflag = .false.
    real :: emax

! loop to obtain weights for energy grid points.  negrid-1 sets
! of weights are needed because we want to compute integrals
! for negrid-1 sets of energy points (corresponding to negrid-1
! points that we can choose to drop from the guassian quadrature)

    if (vgrid) then
       np = nesub
       emax = vcut
    else
       if (ecut > 0.0) then
          np = negrid-1
       else
          np = negrid
       end if
       emax = x0
    end if

    allocate (modzeroes(np-1)); call alloc8(r1=modzeroes,v="modzeroes")
    allocate (werrtmp(np-1)); call alloc8(r1=werrtmp,v="werrtmp")
    allocate (werr(negrid,np)); call alloc8(r2=werr,v="werr")
    werr = 0.0 ; modzeroes = 0.0 ; werrtmp = 0.0

    do ipt=1, np
       ! drops the point corresponding to ipt from the energy grid
       if (ipt /= 1) modzeroes(:ipt-1) = zeroes(:ipt-1)
       if (ipt /= np) modzeroes(ipt:np-1) = zeroes(ipt+1:np)
       ! get weights for energy grid points
       call get_weights (nmax,0.0,emax,modzeroes,werrtmp,ndiv,divmax,eflag)
       ! a zero is left in the position corresponding to the dropped point
       if (ipt /= 1) werr(:ipt-1,ipt) = werrtmp(:ipt-1)
       if (ipt /= np) werr(ipt+1:np,ipt) = werrtmp(ipt:np-1)
    end do

    if (vgrid) then
       do ipt=1, np
          werr(nesub+1:negrid,ipt) = w(nesub+1:negrid,1)
          ! absorbing volume element into weights
          werr(:nesub,ipt) = werr(:nesub,ipt)*e(:nesub,1)*exp(-e(:nesub,1))/sqrt(pi)
       end do
    else
       werr(1:np,:) = werr(1:np,:) * 0.25
       if (np < negrid) werr(negrid,:) = w(negrid,1)
    end if

    if (test) then
       ! called only for proc0
       do ipt=1, np
          print *, 'ipt= ', ipt, ' sum(werr)= ', sum(werr(:,ipt))
       end do
       ! We shouldn't stop here
!       call finish_mp
!       stop
    end if

! same thing done here for lamdba as was
! done earlier for energy space

    allocate (lmodzeroes(nlambda-1)); call alloc8(r1=lmodzeroes,v="lmodzeroes")
    allocate (wlerrtmp(nlambda-1)); call alloc8(r1=wlerrtmp,v="wlerrtmp")
    allocate (wlerr(nlambda,nlambda)); call alloc8(r2=wlerr,v="wlerr")
    wlerr = 0.0; lmodzeroes = 0.0; wlerrtmp = 0.0

    do ipt=1, nlambda

       if (ipt /= 1) lmodzeroes(:ipt-1) = xx(:ipt-1)
       if (ipt /= nlambda) lmodzeroes(ipt:nlambda-1) = xx(ipt+1:)

       call get_weights (nmax,1.0,0.0,lmodzeroes,wlerrtmp,ndiv,divmax,eflag)

       if (ipt /= 1) wlerr(:ipt-1,ipt) = 2.0*wlerrtmp(:ipt-1)
       if (ipt /= nlambda) wlerr(ipt+1:,ipt) = 2.0*wlerrtmp(ipt:)

    end do

!    deallocate(modzeroes,werrtmp,lmodzeroes,wlerrtmp)
    call dealloc8(r1=modzeroes,v="modzeroes")
    call dealloc8(r1=werrtmp,v="werrtmp")
    call dealloc8(r1=lmodzeroes,v="lmodzeroes")
    call dealloc8(r1=wlerrtmp,v="wlerrtmp")

  end subroutine init_weights

! the get_weights subroutine determines how to divide up the integral into 
! subintervals and how many grid points should be in each subinterval

  subroutine get_weights (maxpts_in, llim, ulim, nodes, wgts, ndiv, divmax, err_flag)
    use agk_mem, only: alloc8, dealloc8
    implicit none

    integer, intent (in) :: maxpts_in
    real, intent (in) :: llim, ulim
    real, dimension (:), intent (in) :: nodes
    real, dimension (:), intent (out) :: wgts
    logical, intent (out) :: err_flag
    integer, intent (out) :: ndiv, divmax

    integer :: npts, rmndr, basepts, divrmndr, base_idx, idiv, epts, maxpts
    integer, dimension (:), allocatable :: divpts

    real :: wgt_max

! npts is the number of grid points in the integration interval
    npts = size(nodes)

    wgts = 0.0; epts = npts; basepts = nmax; divrmndr = 0; ndiv = 1; divmax = npts

! maxpts is the max number of pts in an integration subinterval
    maxpts = min(maxpts_in,npts)

    do

!       wgt_max = wgt_fac/maxpts
       wgt_max = abs(ulim-llim)*wgt_fac/npts

! only need to subdivide integration interval if maxpts < npts
       if (maxpts .ge. npts) then
          call get_intrvl_weights (llim, ulim, nodes, wgts)
       else
          rmndr = mod(npts-maxpts,maxpts-1)
          
! if rmndr is 0, then each subinterval contains maxpts pts
          if (rmndr == 0) then
! ndiv is the number of subintervals
             ndiv = (npts-maxpts)/(maxpts-1) + 1
             allocate (divpts(ndiv)); call alloc8(i1=divpts,v="divpts")
! divpts is an array containing the # of pts for each subinterval
             divpts = maxpts
          else
             ndiv = (npts-maxpts)/(maxpts-1) + 2
             allocate (divpts(ndiv)); call alloc8(i1=divpts,v="divpts")
! epts is the effective number of pts after taking into account double
! counting of some grid points (those that are boundaries of subintervals
! are used twice)
             epts = npts + ndiv - 1
             basepts = epts/ndiv
             divrmndr = mod(epts,ndiv)
             
! determines if all intervals have same # of pts
             if (divrmndr == 0) then
                divpts = basepts
             else
                divpts(:divrmndr) = basepts + 1
                divpts(divrmndr+1:) = basepts
             end if
          end if
          
          base_idx = 0
          
! loop calls subroutine to get weights for each subinterval
          do idiv=1,ndiv
             if (idiv == 1) then
                call get_intrvl_weights (llim, nodes(base_idx+divpts(idiv)), &
                     nodes(base_idx+1:base_idx+divpts(idiv)),wgts(base_idx+1:base_idx+divpts(idiv)))
             else if (idiv == ndiv) then
                call get_intrvl_weights (nodes(base_idx+1), ulim, &
                     nodes(base_idx+1:base_idx+divpts(idiv)),wgts(base_idx+1:base_idx+divpts(idiv)))
             else
                call get_intrvl_weights (nodes(base_idx+1), nodes(base_idx+divpts(idiv)), &
                     nodes(base_idx+1:base_idx+divpts(idiv)),wgts(base_idx+1:base_idx+divpts(idiv)))
             end if
             base_idx = base_idx + divpts(idiv) - 1
          end do
          
          divmax = maxval(divpts)

!          deallocate (divpts)
          call dealloc8(i1=divpts,v="divpts")
       end if

! check to make sure the weights do not get too large
       if (abs(maxval(wgts)) .gt. wgt_max) then
          if (maxpts .lt. 3) then
             err_flag = .true.
             exit
          end if
          maxpts = divmax - 1
       else
          exit
       end if

       wgts = 0.0; epts = npts; divrmndr = 0; basepts = nmax
    end do

  end subroutine get_weights

  subroutine get_intrvl_weights (llim, ulim, nodes, wgts)
    use gauss_quad, only: get_legendre_grids_from_cheb
    use agk_mem, only: alloc8
    
    implicit none
    
    ! llim (ulim) is lower (upper) limit of integration
    real, intent (in) :: llim, ulim
    real, dimension (:), intent (in) :: nodes
    real, dimension (:), intent (in out) :: wgts
    
    ! stuff needed to do guassian quadrature 
    real, dimension (:), allocatable :: gnodes, gwgts, omprod
    integer :: ix, iw

    allocate (gnodes(size(nodes)/2+1)); call alloc8(r1=gnodes,v="gnodes")
    allocate (gwgts(size(wgts)/2+1)); call alloc8(r1=gwgts,v="gwgts")
    allocate (omprod(size(nodes)/2+1)); call alloc8(r1=omprod,v="omprod")

    call get_legendre_grids_from_cheb (llim, ulim, gnodes, gwgts)

    do iw=1,size(wgts)
       omprod = 1.0
       
       do ix=1,size(nodes)
          if (ix /= iw) omprod = omprod*(gnodes - nodes(ix))/(nodes(iw) - nodes(ix))
       end do
       
       do ix=1,size(gwgts)
          wgts(iw) = wgts(iw) + omprod(ix)*gwgts(ix)
       end do
    end do
       
  end subroutine get_intrvl_weights

  subroutine set_grids ! called in proc0 only
    use species, only: init_species, nspec
    use theta_grid, only: init_theta_grid
    use egrid, only: setegrid, setvgrid
    use agk_mem, only: alloc8
    implicit none
    integer :: is

    call init_theta_grid
    call init_species

    allocate (e(negrid,nspec)); call alloc8(r2=e,v="e")
    allocate (w(negrid,nspec)); call alloc8(r2=w,v="w")
    allocate (dele(negrid,nspec)); call alloc8(r2=dele,v="dele")

    if (vgrid) then
       call setvgrid (vcut, negrid, e(:,1), w(:,1), nesub, test)
    else
       call setegrid (ecut, negrid, e(:,1), w(:,1), test)
       w(:,1) = 0.25*w(:,1)
    end if
    ! TT: Do we need to keep egrid for each species --- could be just one?
    do is = 2, nspec
       e(:,is) = e(:,1)
       w(:,is) = w(:,1)
    end do
    dele(1,:) = e(1,:)
    dele(2:,:) = e(2:,:)-e(:negrid-1,:)

    ng2 = 2*ngauss
    nlambda = ng2
    allocate (al(nlambda)); call alloc8(r1=al,v="al")
    allocate (delal(nlambda)); call alloc8(r1=delal,v="delal")
    allocate (wl(nlambda)); call alloc8(r1=wl,v="wl")
    allocate (xx(nlambda)); call alloc8(r1=xx,v="xx")
    call setlgrid
    delal(1) = al(1)
    delal(2:) = al(2:) - al(:nlambda-1)

  end subroutine set_grids

  subroutine setlgrid

    use gauss_quad, only: get_legendre_grids_from_cheb
    implicit none

    real, dimension (2*ngauss) :: wx

    call get_legendre_grids_from_cheb (1., 0., xx, wx)

    wl = 2.0*wx
    al = 1.0 - xx**2

  end subroutine setlgrid

  subroutine integrate_species1 (g, weights, total)
    use theta_grid, only: ntgrid
    use kgrids, only: naky, nakx
    use agk_layouts, only: g_lo, idx, idx_local
    use agk_layouts, only: is_idx, ik_idx, it_idx, ie_idx, il_idx
    use mp, only: sum_allreduce
    use agk_mem, only: alloc8, dealloc8
    implicit none

    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (in) :: g
    complex, dimension (-ntgrid:,:,:), intent (out) :: total ! (ntgrid,nakx,naky)
    real, dimension (:), intent (in) :: weights

! total = total(theta, kx, ky)
    complex, dimension (:), allocatable :: work
    real :: fac
    integer :: is, il, ie, ik, it, iglo, ig, i

    total = 0.
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       fac = weights(is)*w(ie,is)*wl(il)

       total(:, it, ik) = total(:, it, ik) + fac*(g(:,1,iglo)+g(:,2,iglo))
    end do

    allocate (work((2*ntgrid+1)*naky*nakx)) ; work = 0. ; call alloc8 (c1=work, v="work")
    i = 0
    do ik = 1, naky
       do it = 1, nakx
          do ig = -ntgrid, ntgrid
             i = i + 1
             work(i) = total(ig, it, ik)
          end do
       end do
    end do
    
    call sum_allreduce (work) 

    i = 0
    do ik = 1, naky
       do it = 1, nakx
          do ig = -ntgrid, ntgrid
             i = i + 1
             total(ig, it, ik) = work(i)
          end do
       end do
    end do

    call dealloc8 (c1=work, v="work")

  end subroutine integrate_species1

  subroutine integrate_species0 (g, weights, total)
    use kgrids, only: naky, nakx
    use agk_layouts, only: g_lo, idx, idx_local
    use agk_layouts, only: is_idx, ik_idx, it_idx, ie_idx, il_idx
    use mp, only: sum_allreduce
    use agk_mem, only: alloc8, dealloc8 
    implicit none

    complex, dimension (:,g_lo%llim_proc:), intent (in) :: g
    complex, dimension (:,:), intent (out) :: total
    real, dimension (:), intent (in) :: weights

! total = total(theta, kx, ky)
    complex, dimension (:), allocatable :: work
    real :: fac
    integer :: is, il, ie, ik, it, iglo, i

    total = 0.
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       fac = weights(is)*w(ie,is)*wl(il)

       total(it, ik) = total(it, ik) + fac*(g(1,iglo)+g(2,iglo))
    end do

    allocate (work(naky*nakx)) ; work = 0. ; call alloc8 (c1=work, v="work")
    i = 0
    do ik = 1, naky
       do it = 1, nakx
          i = i + 1
          work(i) = total(it, ik)
       end do
    end do
    
    call sum_allreduce (work) 

    i = 0
    do ik = 1, naky
       do it = 1, nakx
          i = i + 1
          total(it, ik) = work(i)
       end do
    end do

    call dealloc8 (c1=work, v="work")

  end subroutine integrate_species0

! TT: This should actually be called integrate_moments, but...
  subroutine integrate_species_hankel (g, total, all)
    use mp, only: nproc
    use species, only: nspec
    use kgrids, only: naky, nakx
    use agk_layouts, only: g_lo, is_idx, ik_idx, it_idx, ie_idx, il_idx
    use mp, only: sum_reduce, sum_allreduce, proc0
    use agk_mem, only: alloc8, dealloc8
    implicit none
    complex, dimension (:,g_lo%llim_proc:,:), intent (in) :: g
    complex, dimension (:,:,:,:), intent (out) :: total
    integer, optional, intent (in) :: all

    complex, dimension (:), allocatable :: work
    real :: fac
    integer :: is, il, ie, ik, it, iglo, i, ip

    total = 0.
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       fac = w(ie,is)*wl(il)

       total(it,ik,:,is) = total(it,ik,:,is) &
               & + fac*(g(1,iglo,:)+g(2,iglo,:))
    end do

    if (nproc > 1) then
       allocate (work(naky*nakx*npgrid*nspec)) ; work = 0. ; call alloc8(c1=work,v="work")
       i = 0
       do is = 1, nspec
          do ip = 1, npgrid
             do ik = 1, naky
                do it = 1, nakx
                   i = i + 1
                   work(i) = total(it,ik,ip,is)
                end do
             end do
          end do
       end do

       if (present(all)) then
          call sum_allreduce (work)
       else
          call sum_reduce (work, 0)
       end if

       if (proc0 .or. present(all)) then
          i = 0
          do is = 1, nspec
             do ip = 1, npgrid
                do ik = 1, naky
                   do it = 1, nakx
                      i = i + 1
                      total(it,ik,ip,is) = work(i)
                   end do
                end do
             end do
          end do
       end if
!       deallocate (work)
       call dealloc8(c1=work,v="work")
    end if

  end subroutine integrate_species_hankel
! <TT

! MAB>
  subroutine integrate_moment_vspectrum (g, total)

    complex, dimension (:,:), intent (in) :: g
    complex, intent (out) :: total
    integer :: ixi, nxi, ie, il
    real :: fac

    nxi = nlambda*2
    total = 0.0
    do ie=1, negrid
       do ixi=1, nxi
          il = min(ixi, nxi+1-ixi)
          fac = w(ie,1) * wl(il)
          total = total + fac * g(ixi,ie)
       end do
    end do

  end subroutine integrate_moment_vspectrum
! <MAB

  subroutine integrate_test (g, weights, total, istep)
    use theta_grid, only: ntgrid
    use kgrids, only: naky, nakx
    use agk_layouts, only: g_lo, idx, idx_local
    use agk_layouts, only: is_idx, ik_idx, it_idx, ie_idx, il_idx
    use mp, only: sum_allreduce
    use agk_mem, only: alloc8, dealloc8

    implicit none

    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (in) :: g
    complex, dimension (-ntgrid:,:,:), intent (out) :: total
    real, dimension (:), intent (in) :: weights
    integer, intent (in) :: istep

    complex, dimension (:), allocatable :: work
    real :: fac
    integer :: is, il, ie, ik, it, iglo, ig, i

!    real, dimension (:), allocatable :: xpt
    real, dimension (:), allocatable :: ypt

!    allocate(xpt(negrid))
!    xpt(:negrid-1) = zeroes
!    xpt(negrid) = x0
       
    allocate(ypt(nlambda)); call alloc8(r1=ypt,v="ypt")
    ypt = 0.0

    total = 0.
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       fac = weights(is)*w(ie,is)*wl(il)

! MUST COMMENT IMMEDIATELY AFTER TESTING
!       wl(:ng2) = 0.0

!       do ig=-ntgrid,ntgrid
!          if (.not. forbid(ig,il)) then
!             ypt(ig,il) = sqrt(max(1.0-bmag(ig)*al(il),0.0))
!          else
!             ypt(ig,il) = 0
!          end if
!       end do

       total(:, it, ik) = total(:, it, ik) + fac*cos(istep*0.1*ypt(il))
    end do

    allocate (work((2*ntgrid+1)*naky*nakx)) ; work = 0. ; call alloc8 (c1=work, v="work")
    i = 0
    do ik = 1, naky
       do it = 1, nakx
          do ig = -ntgrid, ntgrid
             i = i + 1
             work(i) = total(ig, it, ik)
          end do
       end do
    end do
    
    call sum_allreduce (work) 

    i = 0
    do ik = 1, naky
       do it = 1, nakx
          do ig = -ntgrid, ntgrid
             i = i + 1
             total(ig, it, ik) = work(i)
          end do
       end do
    end do
    
    call dealloc8 (c1=work, v="work")

!    deallocate(ypt)
    call dealloc8(r1=ypt,v="ypt")
!    deallocate(xpt)
  end subroutine integrate_test

  subroutine eint_error (g, weights, total)
    use theta_grid, only: ntgrid
    use kgrids, only: naky, nakx
    use agk_layouts, only: g_lo, idx, idx_local
    use agk_layouts, only: is_idx, ik_idx, it_idx, ie_idx, il_idx
    use mp, only: sum_allreduce, broadcast
    use agk_mem, only: alloc8, dealloc8

    implicit none

    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (in) :: g
    real, dimension (:), intent (in) :: weights
    complex, dimension (-ntgrid:,:,:,:), intent (out) :: total

    complex, dimension (:), allocatable :: work
    real :: fac
    integer :: is, il, ie, ik, it, iglo, ig, i, ipt, np
    logical, save :: first = .true.

    if (first) then
       if (vgrid) then
          np = nesub
       else
          if (ecut > 0.0) then
             np = negrid-1
          else
             np = negrid
          end if
       end if
       if (.not.allocated(werr)) then
          allocate (werr(negrid,np)); call alloc8(r2=werr,v="werr")
       end if

       do ie = 1, np
          call broadcast (werr(:,ie))
       end do

       first = .false.
    end if

    np = size(werr,2)
    do ipt=1, np
       total(:,:,:,ipt) = 0.
       do iglo = g_lo%llim_proc, g_lo%ulim_proc
          ik = ik_idx(g_lo,iglo)
          it = it_idx(g_lo,iglo)
          ie = ie_idx(g_lo,iglo)
          is = is_idx(g_lo,iglo)
          il = il_idx(g_lo,iglo)
          fac = weights(is)*werr(ie,ipt)*wl(il)

          total(:, it, ik, ipt) = total(:, it, ik, ipt) + fac*(g(:,1,iglo)+g(:,2,iglo))
       end do

       allocate (work((2*ntgrid+1)*naky*nakx)) ; work = 0. ; call alloc8 (c1=work, v="work")
       i = 0
       do ik = 1, naky
          do it = 1, nakx
             do ig = -ntgrid, ntgrid
                i = i + 1
                work(i) = total(ig, it, ik, ipt)
             end do
          end do
       end do
       
       call sum_allreduce (work) 
       
       i = 0
       do ik = 1, naky
          do it = 1, nakx
             do ig = -ntgrid, ntgrid
                i = i + 1
                total(ig, it, ik, ipt) = work(i)
             end do
          end do
       end do
       call dealloc8 (c1=work, v="work")
    end do

  end subroutine eint_error

  subroutine lint_error (g, weights, total)
    use theta_grid, only: ntgrid
    use kgrids, only: naky, nakx
    use agk_layouts, only: g_lo, idx, idx_local
    use agk_layouts, only: is_idx, ik_idx, it_idx, ie_idx, il_idx
    use mp, only: sum_allreduce, broadcast
    use agk_mem, only: alloc8, dealloc8 
    use constants, only: pi

    implicit none

    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (in) :: g
    real, dimension (:), intent (in) :: weights
    complex, dimension (-ntgrid:,:,:,:), intent (out) :: total
    complex, dimension (:), allocatable :: work
    real :: fac
    integer :: is, il, ie, ik, it, iglo, ig, i, ipt
    logical, save :: first = .true.

    if (first) then
       if (.not. allocated(wlerr)) then
          allocate (wlerr(nlambda, nlambda)); call alloc8(r2=wlerr,v="wlerr")
       end if
       do il = 1, nlambda
          call broadcast (wlerr(il,:))
       end do
       first = .false.
    end if

    do ipt=1,nlambda
       total(:,:,:,ipt) = 0.
       do iglo = g_lo%llim_proc, g_lo%ulim_proc
          ik = ik_idx(g_lo,iglo)
          it = it_idx(g_lo,iglo)
          ie = ie_idx(g_lo,iglo)
          is = is_idx(g_lo,iglo)
          il = il_idx(g_lo,iglo)
          fac = weights(is)*w(ie,is)*wlerr(il,ipt)

          total(:, it, ik, ipt) = total(:, it, ik, ipt) + fac*(g(:,1,iglo)+g(:,2,iglo))
       end do


       allocate (work((2*ntgrid+1)*naky*nakx)) ; work = 0. ; call alloc8 (c1=work, v="work")

       i = 0
       do ik = 1, naky
          do it = 1, nakx
             do ig = -ntgrid, ntgrid
                i = i + 1
                work(i) = total(ig, it, ik, ipt)
             end do
          end do
       end do
       
       call sum_allreduce (work) 
       
       i = 0
       do ik = 1, naky
          do it = 1, nakx
             do ig = -ntgrid, ntgrid
                i = i + 1
                total(ig, it, ik, ipt) = work(i)
             end do
          end do
       end do
       call dealloc8 (c1=work, v="work")
    end do

  end subroutine lint_error

  subroutine integrate_moment_c34 (g, total, all)
! returns results to PE 0 [or to all processors if 'all' is present in input arg list]
! NOTE: Takes f = f(x, y, z, sigma, lambda, E, species) and returns int f, where the integral
! is over all velocity space
    use mp, only: nproc
    use theta_grid, only: ntgrid
    use species, only: nspec
    use kgrids, only: naky, nakx
    use agk_layouts, only: g_lo, is_idx, ik_idx, it_idx, ie_idx, il_idx
    use mp, only: sum_reduce, proc0, sum_allreduce
    use agk_mem, only: alloc8, dealloc8 
    implicit none
    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (in) :: g
    complex, dimension (-ntgrid:,:,:,:), intent (out) :: total
    integer, optional, intent(in) :: all

    complex, dimension (:), allocatable :: work
    real :: fac
    integer :: is, il, ie, ik, it, iglo, ig, i

    total = 0.
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       fac = w(ie,is)*wl(il)

       total(:, it, ik, is) = total(:, it, ik, is) &
               & + fac*(g(:,1,iglo)+g(:,2,iglo))
    end do

    if (nproc > 1) then

       allocate (work((2*ntgrid+1)*naky*nakx*nspec)) ; work = 0. ; call alloc8 (c1=work, v="work")

       i = 0
       do is = 1, nspec
          do ik = 1, naky
             do it = 1, nakx
                do ig = -ntgrid, ntgrid
                   i = i + 1
                   work(i) = total(ig, it, ik, is)
                end do
             end do
          end do
       end do
       
       if (present(all)) then
          call sum_allreduce (work)
       else
          call sum_reduce (work, 0)
       end if

       if (proc0 .or. present(all)) then
          i = 0
          do is = 1, nspec
             do ik = 1, naky
                do it = 1, nakx
                   do ig = -ntgrid, ntgrid
                      i = i + 1
                      total(ig, it, ik, is) = work(i)
                   end do
                end do
             end do
          end do
       end if
       call dealloc8 (c1=work, v="work")
    end if

  end subroutine integrate_moment_c34

  subroutine integrate_moment_c23 (g, total, all)
! returns results to PE 0 [or to all processors if 'all' is present in input arg list]
! NOTE: Takes f = f(x, y, sigma, lambda, E, species) and returns int f, where the integral
! is over all velocity space
    use mp, only: nproc
    use species, only: nspec
    use kgrids, only: naky, nakx
    use agk_layouts, only: g_lo, is_idx, ik_idx, it_idx, ie_idx, il_idx
    use mp, only: sum_reduce, proc0, sum_allreduce
    use agk_mem, only: alloc8, dealloc8
    implicit none
    complex, dimension (:,g_lo%llim_proc:), intent (in) :: g
    complex, dimension (:,:,:), intent (out) :: total
    integer, optional, intent(in) :: all

    complex, dimension (:), allocatable :: work
    real :: fac
    integer :: is, il, ie, ik, it, iglo, i

    total = 0.
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       fac = w(ie,is)*wl(il)

       total(it, ik, is) = total(it, ik, is) + fac*(g(1,iglo)+g(2,iglo))
    end do

    if (nproc > 1) then
       allocate (work(naky*nakx*nspec)) ; work = 0.; call alloc8(c1=work,v="work")
       i = 0
       do is = 1, nspec
          do ik = 1, naky
             do it = 1, nakx
                i = i + 1
                work(i) = total(it, ik, is)
             end do
          end do
       end do

       if (present(all)) then
          call sum_allreduce (work)
       else
          call sum_reduce (work, 0)
       end if

       if (proc0 .or. present(all)) then
          i = 0
          do is = 1, nspec
             do ik = 1, naky
                do it = 1, nakx
                   i = i + 1
                   total(it, ik, is) = work(i)
                end do
             end do
          end do
       end if
!       deallocate (work)
       call dealloc8(c1=work,v="work")
    end if

  end subroutine integrate_moment_c23

  subroutine integrate_moment_r34 (g, total, all)
! returns results to PE 0 [or to all processors if 'all' is present in input arg list]
! NOTE: Takes f = f(x, y, z, sigma, lambda, E, species) and returns int f, where the integral
! is over all velocity space
    use mp, only: nproc
    use theta_grid, only: ntgrid
    use species, only: nspec
    use kgrids, only: naky, nakx
    use agk_layouts, only: g_lo, is_idx, ik_idx, it_idx, ie_idx, il_idx
    use mp, only: sum_reduce, proc0, sum_allreduce
    use agk_mem, only: alloc8, dealloc8 
    implicit none
    real, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (in) :: g
    real, dimension (-ntgrid:,:,:,:), intent (out) :: total
    integer, optional, intent(in) :: all

    real, dimension (:), allocatable :: work
    real :: fac
    integer :: is, il, ie, ik, it, iglo, ig, i

    total = 0.
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       fac = w(ie,is)*wl(il)

       total(:, it, ik, is) = total(:, it, ik, is) &
               & + fac*(g(:,1,iglo)+g(:,2,iglo))
    end do

    if (nproc > 1) then

       allocate (work((2*ntgrid+1)*naky*nakx*nspec)) ; work = 0. ; call alloc8 (r1=work, v="work")

       i = 0
       do is = 1, nspec
          do ik = 1, naky
             do it = 1, nakx
                do ig = -ntgrid, ntgrid
                   i = i + 1
                   work(i) = total(ig, it, ik, is)
                end do
             end do
          end do
       end do
       
       if (present(all)) then
          call sum_allreduce (work)
       else
          call sum_reduce (work, 0)
       end if

       if (proc0 .or. present(all)) then
          i = 0
          do is = 1, nspec
             do ik = 1, naky
                do it = 1, nakx
                   do ig = -ntgrid, ntgrid
                      i = i + 1
                      total(ig, it, ik, is) = work(i)
                   end do
                end do
             end do
          end do
       end if
       call dealloc8 (r1=work, v="work")
    end if

  end subroutine integrate_moment_r34

  subroutine integrate_moment_r23 (g, total, all)
! returns results to PE 0 [or to all processors if 'all' is present in input arg list]
! NOTE: Takes f = f(x, y, sigma, lambda, E, species) and returns int f, where the integral
! is over all velocity space
    use mp, only: nproc
    use species, only: nspec
    use kgrids, only: naky, nakx
    use agk_layouts, only: g_lo, is_idx, ik_idx, it_idx, ie_idx, il_idx
    use mp, only: sum_reduce, proc0, sum_allreduce
    use agk_mem, only: alloc8, dealloc8
    implicit none
    real, dimension (:,g_lo%llim_proc:), intent (in) :: g ! (nlambda,g_lo)
    real, dimension (:,:,:), intent (out) :: total ! (nakx,naky,nspec)
    integer, optional, intent(in) :: all

    real, dimension (:), allocatable :: work
    real :: fac
    integer :: is, il, ie, ik, it, iglo, i

    total = 0.
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       ie = ie_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       fac = w(ie,is)*wl(il)

       total(it, ik, is) = total(it, ik, is) + fac*(g(1,iglo)+g(2,iglo))
    end do

    if (nproc > 1) then
       allocate (work(naky*nakx*nspec)) ; work = 0.; call alloc8(r1=work,v="work")
       i = 0
       do is = 1, nspec
          do ik = 1, naky
             do it = 1, nakx
                i = i + 1
                work(i) = total(it, ik, is)
             end do
          end do
       end do

       if (present(all)) then
          call sum_allreduce (work)
       else
          call sum_reduce (work, 0)
       end if

       if (proc0 .or. present(all)) then
          i = 0
          do is = 1, nspec
             do ik = 1, naky
                do it = 1, nakx
                   i = i + 1
                   total(it, ik, is) = work(i)
                end do
             end do
          end do
       end if
!       deallocate (work)
       call dealloc8(r1=work,v="work")
    end if

  end subroutine integrate_moment_r23

  subroutine integrate_moment_ler (lo, g, total)

    use theta_grid, only: ntgrid
    use layouts_type, only: le_layout_type
    use agk_layouts, only: ig_idx, it_idx, ik_idx, is_idx
    type (le_layout_type), intent (in) :: lo
    real, dimension (:,:,lo%llim_proc:), intent (in) :: g
    real, dimension (-ntgrid:,:,:,:), intent (out) :: total
    integer :: ixi, nxi, ie, il, ile, is, it, ik, ig
    real :: fac

    nxi = nlambda*2
    total = 0.0
    do ile = lo%llim_proc, lo%ulim_proc
       ig = ig_idx (lo,ile)
       it = it_idx (lo,ile)
       ik = ik_idx (lo,ile)
       is = is_idx (lo,ile)
       do ie=1, negrid
          do ixi=1, nxi
             il = min(ixi, nxi+1-ixi)
             fac = w(ie,is) * wl(il)
             total(ig,it,ik,is) = total(ig,it,ik,is) + fac * g(ixi,ie,ile)
          end do
       end do
       ! alternatively...
!!$       total (ig,it,ik,is) = sum( spread(w(:,is),1,nlambda) &
!!$            * spread(wl,2,negrid) * (g(1:nlambda,:negrid,ile) &
!!$            + g(nlambda*2:nlambda+1:-1,:negrid,ile)) )
    end do

    ! No need for communication since all velocity grid points are together
    ! and each prcessor does not touch the unset place
    ! They actually don't need to keep all 4D array
    ! Do we stay in le_layout for total?
    ! --- ile contains necessary and sufficient information for (ig,it,ik,is)

  end subroutine integrate_moment_ler

  subroutine integrate_moment_lec (lo, g, total)

    use theta_grid, only: ntgrid
    use layouts_type, only: le_layout_type
    use agk_layouts, only: ig_idx, it_idx, ik_idx, is_idx
    type (le_layout_type), intent (in) :: lo
    complex, dimension (:,:,lo%llim_proc:), intent (in) :: g
    complex, dimension (-ntgrid:,:,:,:), intent (out) :: total
    integer :: ixi, nxi, ie, il, ile, is, it, ik, ig
    real :: fac

    nxi = nlambda*2
    total = 0.0
    do ile = lo%llim_proc, lo%ulim_proc
       ig = ig_idx (lo,ile)
       it = it_idx (lo,ile)
       ik = ik_idx (lo,ile)
       is = is_idx (lo,ile)
       do ie=1, negrid
          do ixi=1, nxi
             il = min(ixi, nxi+1-ixi)
             fac = w(ie,is) * wl(il)
             total(ig,it,ik,is) = total(ig,it,ik,is) + fac * g(ixi,ie,ile)
          end do
       end do
       ! alternatively...
!!$       total (ig,it,ik,is) = sum( spread(w(:,is),1,nlambda) &
!!$            * spread(wl,2,negrid) * (g(1:nlambda,:negrid,ile) &
!!$            + g(nlambda*2:nlambda+1:-1,:negrid,ile)) )
    end do

    ! No need for communication since all velocity grid points are together
    ! and each prcessor does not touch the unset place
    ! They actually don't need to keep all 4D array
    ! Do we stay in le_layout for total?
    ! --- ile contains necessary and sufficient information for (ig,it,ik,is)

  end subroutine integrate_moment_lec

  subroutine integrate_moment_lz (lo, g, total, all)

    use mp, only: proc0, nproc
    use species, only: nspec
    use theta_grid, only: ntgrid
    use kgrids, only: nakx, naky
    use layouts_type, only: lz_layout_type
    use agk_layouts, only: ig_idx, it_idx, ik_idx, ie_idx, is_idx
    use agk_mem, only: alloc8, dealloc8 
    use mp, only: sum_allreduce, sum_reduce
    type (lz_layout_type), intent (in) :: lo
    complex, dimension (:,lo%llim_proc:), intent (in) :: g
    complex, dimension (-ntgrid:,:,:,:), intent (out) :: total
    integer, optional, intent(in) :: all
    integer :: nxi, ilz, ig, it, ik, ie, is, i
    complex, dimension (:), allocatable :: work

    nxi = nlambda*2
    total = 0.0
    do ilz = lo%llim_proc, lo%ulim_proc
       ig = ig_idx (lo,ilz)
       it = it_idx (lo,ilz)
       ik = ik_idx (lo,ilz)
       ie = ie_idx (lo,ilz)
       is = is_idx (lo,ilz)
       total(ig,it,ik,is) = total(ig,it,ik,is) + w(ie,is) &
            * sum(wl * (g(:nlambda,ilz) + g(nxi:nlambda+1:-1,ilz)))
    end do

    if (nproc > 1) then

       allocate (work((2*ntgrid+1)*naky*nakx*nspec)) ; work = 0. ; call alloc8 (c1=work, v="work")

       i = 0
       do is = 1, nspec
          do ik = 1, naky
             do it = 1, nakx
                do ig = -ntgrid, ntgrid
                   i = i + 1
                   work(i) = total(ig, it, ik, is)
                end do
             end do
          end do
       end do
       
       if (present(all)) then
          call sum_allreduce (work)
       else
          call sum_reduce (work, 0)
       end if

       if (proc0 .or. present(all)) then
          i = 0
          do is = 1, nspec
             do ik = 1, naky
                do it = 1, nakx
                   do ig = -ntgrid, ntgrid
                      i = i + 1
                      total(ig, it, ik, is) = work(i)
                   end do
                end do
             end do
          end do
       end if
       call dealloc8 (c1=work, v="work")
    end if

  end subroutine integrate_moment_lz

  subroutine integrate_moment_accelx (lo, g, total, all)

    use mp, only: proc0, nproc, sum_reduce, sum_allreduce
    use kgrids, only: nx, ny
    use species, only: nspec
!    use layouts_type, only: accelx_layout_type
    use agk_layouts, only: it_idx, ik_idx, il_idx, ie_idx, is_idx, accelx_layout_type
    use agk_mem, only: alloc8, dealloc8
    type (accelx_layout_type), intent (in) :: lo
    real, dimension (:,lo%llim_proc:), intent (in) :: g ! (sign,accelx_lo)
    real, dimension (:,:,:), intent (out) :: total ! (nx,ny,nspec)
    integer, optional, intent(in) :: all
    integer :: ie, il, is, it, ik, iac, i
    real :: fac
    real, dimension (:), allocatable :: work

    total = 0.0
    do iac = lo%llim_proc, lo%ulim_proc
       it = it_idx (lo,iac)
       ik = ik_idx (lo,iac)
       ie = ie_idx (lo,iac)
       il = il_idx (lo,iac)
       is = is_idx (lo,iac)
       fac = w(ie,is) * wl(il)
       total(it,ik,is) = total(it,ik,is) + fac * (g(1,iac)+g(2,iac))
    end do

    if (nproc > 1) then
       allocate (work(nx*ny*nspec)) ; work = 0.; call alloc8(r1=work,v="work")
       i = 0
       do is = 1, nspec
          do ik = 1, ny
             do it = 1, nx
                i = i + 1
                work(i) = total(it, ik, is)
             end do
          end do
       end do

       if (present(all)) then
          call sum_allreduce (work)
       else
          call sum_reduce (work, 0)
       end if

       if (proc0 .or. present(all)) then
          i = 0
          do is = 1, nspec
             do ik = 1, ny
                do it = 1, nx
                   i = i + 1
                   total(it, ik, is) = work(i)
                end do
             end do
          end do
       end if
!       deallocate (work)
       call dealloc8(r1=work,v="work")
    end if

  end subroutine integrate_moment_accelx

  subroutine init_vspectrum

    use spfunc, only: j0
    use constants, only: pi => dpi
    use agk_mem, only: alloc8, dealloc8

    implicit none

    integer :: nxi
    integer :: il, ie, ixi, ip
    real :: delp
    real, dimension (:), allocatable :: xi
    real, dimension (:,:), allocatable :: vpapts

    nxi = 2*nlambda
    nvpa = min(nlambda,nesub)
    nvpe = min(nlambda,nesub)*2
!    nvpe = min(nlambda,nesub)/2

    ! delp is spacing between adjacent p-values (p is dual to vperp)
    delp = min(nlambda,nesub)/real(nvpe)

    ! temporary arrays
    allocate (xi(nxi)); call alloc8(r1=xi,v="xi")
    allocate (vpapts(nxi,negrid)); call alloc8(r2=vpapts,v="vpapts")

    ! saved arrays for later transforms
    allocate (lpv(negrid,0:nesub-1)) ; lpv = 0.0; call alloc8(r2=lpv,v="lpv")
    allocate (lpl(nxi,0:nlambda-1)) ; lpl = 0.0; call alloc8(r2=lpl,v="lpl")
    allocate (hpvpa(nxi,negrid,0:nvpa-1)) ; hpvpa = 0.0; call alloc8(r3=hpvpa,v="hpvpa")
    allocate (aj0p(nxi,negrid,0:nvpe-1)) ; aj0p = 1.0; call alloc8(r3=aj0p,v="aj0p")

    xi(1:nlambda) = -sqrt(1.0-al)
    xi(nlambda+1:) = sqrt(1.0-al(nlambda:1:-1))

    ! get legendre polynomials in velocity
    call get_legendrep (0.0, vcut, sqrt(e(:nesub,1)), lpv(:nesub,:))
    ! lpv(i,j) contains j-th order Legendre polynomial at points sqrt(e(i))
    ! for i in (1:nesub).  Note lpv(nesub+1:,:) are all zeros
    ! define lpv so that integral of lpv**2 = 1
    do ie = 1, nesub
       lpv(:,ie-1) = sqrt(pi)*lpv(:,ie-1)*sqrt((2.*ie-1.)/vcut)*exp(e(:,1))/(2.*e(:,1))
    end do

    ! get legendre polynomials in pitch angle
    ! TT: Q: Can't we get these in a single call w/ (-1,1)?
    call get_legendrep (-1.0, 0.0, xi(:nlambda), lpl(:nlambda,:))
    call get_legendrep (0.0, 1.0, xi(nlambda+1:), lpl(nlambda+1:,:))
    ! define lpl so that integral of lpl**2 = 1
    do il = 1, nlambda
       lpl(:,il-1) = lpl(:,il-1)*sqrt((2.*il-1.)*0.5)
    end do

    ! need vparallel grid to get hermite polynomials
    do ixi = 1, nxi
       do ie = 1, negrid
          vpapts(ixi,ie) = sqrt(e(ie,1))*xi(ixi)
       end do
    end do

    ! hermite polynomials returned have normalization coefficients absorbed
    call get_hermitep (vpapts, hpvpa)
    ! need additional coefficient to take into account maxwellian in integration weights
    hpvpa = sqrt(pi)*0.5*hpvpa

    ! get bessel functions on fine grid
    do ip = 1, nvpe-1
       do ie = 1, negrid
          do ixi = 1, nxi
             aj0p(ixi,ie,ip) = j0(sqrt(e(ie,1)-vpapts(ixi,ie)**2)*ip*delp)
          end do
       end do
    end do

!    deallocate (xi, vpapts)
    call dealloc8(r1=xi,v="xi")
    call dealloc8(r2=vpapts,v="vpapts")

  end subroutine init_vspectrum

  subroutine vtransform2d (g0, gvxi, gvpavpe)

    use agk_mem, only: alloc8, dealloc8
    implicit none

    complex, dimension (:,:), intent (in)  :: g0      ! (1:nxi,     1:negrid)
    complex, dimension (:,:), intent (out) :: gvxi    ! (1:nlambda, 1:nesub)
    complex, dimension (:,:), intent (out) :: gvpavpe ! (1:nvpe,    1:nvpa)

    integer :: nxi
    integer :: il, ie, ip, iq

    complex, dimension (:,:), allocatable :: g1, g2

    nxi = 2*nlambda

    allocate (g1(nxi,negrid)); call alloc8(c2=g1,v="g1")
    allocate (g2(nxi,negrid)); call alloc8(c2=g2,v="g2")

    ! gvxi(il,ie) = int Legendre_{ie-1}(sqrt(e)) Legendre_{il-1}(xi) g d^2v
    do ie = 1, nesub
       ! Note: lpv(nesub+1:,:)==0
       g1 = g0*spread(lpv(:,ie-1),1,nxi)
       do il = 1, nlambda
          g2 = spread(lpl(:,il-1),2,negrid)
          call integrate_moment_vspectrum (g1*g2, gvxi(il,ie))
       end do
    end do

    do iq = 1, nvpa
       g1 = g0*hpvpa(:,:,iq-1)
       do ip = 1, nvpe
          call integrate_moment_vspectrum (g1*aj0p(:,:,ip-1), gvpavpe(ip,iq))
       end do
    end do

!    deallocate(g1,g2)
    call dealloc8(c2=g1,v="g1")
    call dealloc8(c2=g2,v="g2")

  end subroutine vtransform2d

  subroutine get_legendrep (llim, ulim, xptsdum, lpdum)

    implicit none

    real, intent (in) :: ulim, llim
    real, dimension (:), intent (in)   :: xptsdum
    real, dimension (:,0:), intent(out) :: lpdum

    integer :: j, mmax
    double precision, dimension (:), allocatable :: lp1, lp2, lp3, zshift

    lpdum = 0.0

    mmax = size(xptsdum)

    allocate(lp1(mmax)) !!! RN> unable to account memory for double precision variables
    allocate(lp2(mmax)) !!! RN> unable to account memory for double precision variables
    allocate(lp3(mmax)) !!! RN> unable to account memory for double precision variables
    allocate(zshift(mmax)) !!! RN> unable to account memory for double precision variables

    lp1 = real(1.0,kind(lp1(1)))
    lp2 = real(0.0,kind(lp2(1)))

    lpdum(:,0) = real(1.0,kind(lpdum))

    zshift = real(2.0,kind(zshift))*(xptsdum-llim)/(ulim-llim) - real(1.0,kind(zshift))

    do j=1, size(lpdum,2)-1
       lp3 = lp2
       lp2 = lp1
       lp1 = ((2*j-1) * zshift * lp2 - (j-1) * lp3) / j
       lpdum(:,j) = lp1
    end do

!    deallocate(lp1,lp2,lp3,zshift)
    deallocate(lp1) !!! RN> unable to account memory for double precision variables
    deallocate(lp2) !!! RN> unable to account memory for double precision variables
    deallocate(lp3) !!! RN> unable to account memory for double precision variables
    deallocate(zshift) !!! RN> unable to account memory for double precision variables

  end subroutine get_legendrep

  subroutine get_laguerrep (llim, xptsdum, lpdum)

    implicit none

    real, intent (in) :: llim
    real, dimension (:), intent (in)   :: xptsdum
    real, dimension (:,0:), intent(out) :: lpdum

    integer :: j, mmax
    double precision, dimension (:), allocatable :: lp1, lp2, lp3, zshift

    lpdum = 0.0

    mmax = size(xptsdum)

    allocate(lp1(mmax)) !!! RN> unable to account memory for double precision variables
    allocate(lp2(mmax)) !!! RN> unable to account memory for double precision variables
    allocate(lp3(mmax)) !!! RN> unable to account memory for double precision variables
    allocate(zshift(mmax)) !!! RN> unable to account memory for double precision variables

    lp1 = real(1.0,kind(lp1(1)))
    lp2 = real(0.0,kind(lp2(1)))

    lpdum(:,0) = real(1.0,kind(lpdum))

    zshift = real(xptsdum**2,kind(zshift)) - real(llim**2,kind(zshift))

    if (size(lpdum,2) > 1) then
       do j=1, size(lpdum,2)-1
          lp3 = lp2
          lp2 = lp1
          lp1 = ((2*j-1-zshift) * lp2 + (1-j) * lp3) / j
          lpdum(:,j) = lp1
       end do
    end if

!    deallocate(lp1,lp2,lp3,zshift)
    deallocate(lp1) !!! RN> unable to account memory for double precision variables
    deallocate(lp2) !!! RN> unable to account memory for double precision variables
    deallocate(lp3) !!! RN> unable to account memory for double precision variables
    deallocate(zshift) !!! RN> unable to account memory for double precision variables

  end subroutine get_laguerrep

  subroutine get_hermitep (xptsdum, hpdum)

    use constants, only: pi => dpi

    implicit none

    real, dimension (:,:), intent (in)   :: xptsdum ! (1:nxi, 1:negrid)
    ! Here xptsdum contains values of vpar in (xi,e)-plane
    real, dimension (:,:,0:), intent(out) :: hpdum ! (1:nxi, 1:negrid, 0:nvpa-1)
    ! Thus hpdum(:,:,k) contains values of k-th order normalized Hermite
    ! polynomial of vpar in (xi,e)-plane

    integer :: j, mmax1, mmax2
    double precision :: normfac
    double precision, dimension (:,:), allocatable :: hp1, hp2, hp3

    hpdum = 0.0

    mmax1 = size(xptsdum,1)
    mmax2 = size(xptsdum,2)

    allocate(hp1(mmax1,mmax2)) !!! RN> unable to account memory for double precision variables
    allocate(hp2(mmax1,mmax2)) !!! RN> unable to account memory for double precision variables
    allocate(hp3(mmax1,mmax2)) !!! RN> unable to account memory for double precision variables

    hp1 = real(1.0,kind(hp1(1,1)))
    hp2 = real(0.0,kind(hp2(1,1)))

    ! define hermite polynomials with normalization such that
    ! integral H^2 = 1
    normfac = sqrt(pi)
    hpdum(:,:,0) = real(1.0/sqrt(normfac),kind(hpdum))

    do j=1, size(hpdum,3)-1
       hp3 = hp2
       hp2 = hp1
       hp1 = 2.*xptsdum*hp2 - 2*(j-1)*hp3
       normfac = normfac*2.*j
       hpdum(:,:,j) = hp1/sqrt(normfac)
    end do

!    deallocate(hp1,hp2,hp3)
    deallocate(hp1) !!! RN> unable to account memory for double precision variables
    deallocate(hp2) !!! RN> unable to account memory for double precision variables
    deallocate(hp3) !!! RN> unable to account memory for double precision variables

  end subroutine get_hermitep

end module le_grids
