!-----------------------------------------------------------------------------!
!-----------------------------------------------------------------------------!
!!*                                                                         *!!
!!                 AstroGK Index for Heating Diagnostics Module              !!
!!                                   2008                                    !!
!!*                                                                         *!!
!-----------------------------------------------------------------------------!
!-----------------------------------------------------------------------------!
! Here are defined the indices for each calculated heating and energy quantity


module agk_heating_index
  implicit none
  private

! Quantities independent of nspec-------------------------------------------

! Energy quantities
  integer,parameter :: lenergy=1          !Total Energy (W)
  integer,parameter :: leapar=2           !Total Bperp energy (A_parallel)
  integer,parameter :: lebpar=3           !Total Bpar energy
! TT: Added lwg1
  integer :: lwg1                         ! Total g^2 w/o division by F0
! <TT

! Heating Quantities (rate of change of energy)
  integer,parameter :: lantenna=4         !Total Antenna (input) energy
  integer,parameter :: lenergy_dot=5      !Total rate of change of energy (dW/dt)

! Equilibrium components if exist
  integer, parameter :: leapar_pt=6
  integer, parameter :: lebpar_pt=7

  integer, parameter :: ldiss1=8           !Total dissipation (cummulative) from collision
  integer, parameter :: ldiss2=9           !Total dissipation (cummulative) from W(t)-W(0)
  integer, parameter :: ldiss3=10          !Total dissipation (cummulative) from difference of diss1 and diss2

! Balance Quantities
  integer,parameter :: lpowbal=11          !Power Balance
  integer,parameter :: lpowbalcoll=12      !Collisional Power Balance

! Quantities dependent on nspec---------------------------------------------

! Balance Quantities  
  integer, dimension(:), allocatable :: lentbal       !Entropy Balance

! Energy quantities
  integer, dimension(:), allocatable :: ldelfs2       !int T/F0 dfs^2/2
  integer, dimension(:), allocatable :: lhs2          !int T/F0  hs^2/2
  integer, dimension(:), allocatable :: lphis2        !int  q^2 n/T phi^2/2
  integer, dimension(:), allocatable :: lupar2        !int mn0 upar^2/2
  integer, dimension(:), allocatable :: luperp2       !int mn0 (ux^2+uy^2)/2
  integer, dimension(:), allocatable :: lkphi2        !int mn0 kphi^2/2
  integer, dimension(:), allocatable :: lg0phi2       !int n0T0 (q phi/T0)^2 (1-gamma0)

  integer, dimension(:), allocatable :: ldelfs2_pt    !int T/F0 dfs^2/2
  integer, dimension(:), allocatable :: lhs2_pt       !int T/F0  hs^2/2
  integer, dimension(:), allocatable :: lphis2_pt     !int  q^2 n/T phi^2/2
  integer, dimension(:), allocatable :: lupar2_pt     !int mn0 upar^2
  integer, dimension(:), allocatable :: luperp2_pt    !int mn0 (ux^2+uy^2)
  integer, dimension(:), allocatable :: lkphi2_pt     !int mn0 kphi^2/2
  integer, dimension(:), allocatable :: lg0phi2_pt    !int n0T0 (q phi/T0)^2 (1-gamma0)

  integer, dimension(:), allocatable :: ltxx          !int n0 T0 n Txx
  integer, dimension(:), allocatable :: ltyy          !int n0 T0 n Tyy
  integer, dimension(:), allocatable :: ltzz          !int n0 T0 n Tzz
  integer, dimension(:), allocatable :: ltxx_pt       !int n0 T0 n Txx
  integer, dimension(:), allocatable :: ltyy_pt       !int n0 T0 n Tyy
  integer, dimension(:), allocatable :: ltzz_pt       !int n0 T0 n Tzz

! Heating Quantities (rate of change of energy)
  integer, dimension(:), allocatable :: limp_colls    !Implicit heating
  integer, dimension(:), allocatable :: lhypercoll    !Hypercollisional heating
  integer, dimension(:), allocatable :: lcollisions   !Collisional heating
  integer, dimension(:), allocatable :: lheating      !Dist Func heating input
  integer, dimension(:), allocatable :: lgradients    !Gradient heating

! Other useful numbers
  integer :: lemax                         !Numbers of heating quantities
  logical :: initialized = .false.

!Elsasser Energy Spectra Indices
  integer :: lelzp=1
  integer :: lelzm=2
  integer :: lelzp2=3
  integer :: lelzm2=4
  integer :: lelzmax=4

  real, save :: energy0=0., eapar0=0., ebpar0=0.
  real, save :: diss1=0.
  public :: energy0, eapar0, ebpar0, diss1

  public :: lenergy,leapar,lebpar,ldelfs2,lhs2,lphis2,lupar2,luperp2,lkphi2,lg0phi2
  public :: leapar_pt, lebpar_pt, ldelfs2_pt, lhs2_pt, lphis2_pt
  public :: lupar2_pt, luperp2_pt, lkphi2_pt, lg0phi2_pt
  public :: ltxx, ltyy, ltzz, ltxx_pt, ltyy_pt, ltzz_pt
  public :: lpowbal,lpowbalcoll,lentbal
  public :: lantenna,lenergy_dot
  public :: limp_colls,lhypercoll,lcollisions,lheating,lgradients
  public :: lemax
  public :: ldiss1, ldiss2, ldiss3
! TT: Added lwg1
  public :: lwg1
! <TT

  public :: lelzp,lelzm,lelzmax,lelzp2,lelzm2

  public :: init_heating_index,finish_heating_index

contains
!------------------------------------------------------------------------------
!                              AstroGK, 2008
!------------------------------------------------------------------------------

! Initialize indices for all nspec dependent quantities

  subroutine init_heating_index(nspec)
    use agk_mem, only: alloc8
    implicit none
! Passed
    integer :: nspec                   !Number of species
! Local
    integer :: n,i

    if (initialized) return
    initialized = .true.
    
! Allocate nspec size for all dependent quantities

    allocate(lentbal(1:nspec)); call alloc8(i1=lentbal,v="lentval")
    allocate(ldelfs2(1:nspec)); call alloc8(i1=ldelfs2,v="ldelfs2")
    allocate(lhs2(1:nspec)); call alloc8(i1=lhs2,v="lhs2")
    allocate(lphis2(1:nspec)); call alloc8(i1=lphis2,v="lphis2")
    allocate(limp_colls(1:nspec)); call alloc8(i1=limp_colls,v="limp_colls")
    allocate(lhypercoll(1:nspec)); call alloc8(i1=lhypercoll,v="lhypercoll")
    allocate(lcollisions(1:nspec)); call alloc8(i1=lcollisions,v="lcollisions")
    allocate(lheating(1:nspec)); call alloc8(i1=lheating,v="lheating")
    allocate(lgradients(1:nspec)); call alloc8(i1=lgradients,v="lgradients")
    allocate(lupar2(1:nspec)); call alloc8(i1=lupar2,v="lupar2")
    allocate(luperp2(1:nspec)); call alloc8(i1=luperp2,v="luperp2")
    allocate(lkphi2(1:nspec)); call alloc8(i1=lkphi2,v="lkphi2")
    allocate(lg0phi2(1:nspec)); call alloc8(i1=lg0phi2,v="lg0phi2")
    allocate(ldelfs2_pt(1:nspec)); call alloc8(i1=ldelfs2_pt,v="ldelfs2_pt")
    allocate(lhs2_pt(1:nspec)); call alloc8(i1=lhs2_pt,v="lhs2_pt")
    allocate(lphis2_pt(1:nspec)); call alloc8(i1=lphis2_pt,v="lphis2_pt")
    allocate(lupar2_pt(1:nspec)); call alloc8(i1=lupar2_pt,v="lupar2_pt")
    allocate(luperp2_pt(1:nspec)); call alloc8(i1=luperp2_pt,v="luperp2_pt")
    allocate(lkphi2_pt(1:nspec)); call alloc8(i1=lkphi2_pt,v="lkphi2_pt")
    allocate(lg0phi2_pt(1:nspec)); call alloc8(i1=lg0phi2_pt,v="lg0phi2_pt")
    allocate(ltxx(1:nspec)); call alloc8(i1=ltxx,v="ltxx")
    allocate(ltyy(1:nspec)); call alloc8(i1=ltyy,v="ltyy")
    allocate(ltzz(1:nspec)); call alloc8(i1=ltzz,v="ltzz")
    allocate(ltxx_pt(1:nspec)); call alloc8(i1=ltxx_pt,v="ltxx_pt")
    allocate(ltyy_pt(1:nspec)); call alloc8(i1=ltyy_pt,v="ltyy_pt")
    allocate(ltzz_pt(1:nspec)); call alloc8(i1=ltzz_pt,v="ltzz_pt")

! Set indices for these quantities

    n=lpowbalcoll
    do i=1,nspec
       lentbal(i)=n+i
    enddo

    n=n+nspec
    do i=1,nspec
       ldelfs2(i)=n+i
    enddo

    n=n+nspec
    do i=1,nspec
       lhs2(i)=n+i
    enddo

    n=n+nspec
    do i=1,nspec
       lphis2(i)=n+i
    enddo

    n=n+nspec
    do i=1,nspec
       lupar2(i)=n+i
    enddo

    n=n+nspec
    do i=1,nspec
       luperp2(i)=n+i
    enddo

    n=n+nspec
    do i=1,nspec
       lkphi2(i)=n+i
    enddo

    n=n+nspec
    do i=1,nspec
       lg0phi2(i)=n+i
    enddo

    n=n+nspec
    do i=1,nspec
       ldelfs2_pt(i)=n+i
    enddo

    n=n+nspec
    do i=1,nspec
       lhs2_pt(i)=n+i
    enddo

    n=n+nspec
    do i=1,nspec
       lphis2_pt(i)=n+i
    enddo

    n=n+nspec
    do i=1,nspec
       lupar2_pt(i)=n+i
    enddo

    n=n+nspec
    do i=1,nspec
       luperp2_pt(i)=n+i
    enddo

    n=n+nspec
    do i=1,nspec
       lkphi2_pt(i)=n+i
    enddo

    n=n+nspec
    do i=1,nspec
       lg0phi2_pt(i)=n+i
    enddo

    n=n+nspec
    do i=1,nspec
       ltxx(i)=n+i
    enddo

    n=n+nspec
    do i=1,nspec
       ltyy(i)=n+i
    enddo

    n=n+nspec
    do i=1,nspec
       ltzz(i)=n+i
    enddo

    n=n+nspec
    do i=1,nspec
       ltxx_pt(i)=n+i
    enddo

    n=n+nspec
    do i=1,nspec
       ltyy_pt(i)=n+i
    enddo

    n=n+nspec
    do i=1,nspec
       ltzz_pt(i)=n+i
    enddo

    n=n+nspec
    do i=1,nspec
       limp_colls(i)=n+i
    enddo

    n=n+nspec
    do i=1,nspec
       lhypercoll(i)=n+i
    enddo

    n=n+nspec
    do i=1,nspec
       lcollisions(i)=n+i
    enddo

    n=n+nspec
    do i=1,nspec
       lheating(i)=n+i
    enddo

    n=n+nspec
    do i=1,nspec
       lgradients(i)=n+i
    enddo

    n=n+nspec
! TT: Added lwg1
    n = n+1
    lwg1 = n
! <TT
    lemax=n

  end subroutine init_heating_index
!------------------------------------------------------------------------------
!                              AstroGK, 2008
!------------------------------------------------------------------------------
! Deallocate heating indices

  subroutine finish_heating_index
    use agk_mem, only: dealloc8
    implicit none

    !NOTE: There seems to be some problem with the code crashing here GGH 02APR08

! Deallocate all dependent quantities
!    deallocate(lentbal,ldelfs2,lhs2,lphis2,limp_colls)
    call dealloc8(i1=lentbal,v="lentbal")
    call dealloc8(i1=ldelfs2,v="ldelfs2")
    call dealloc8(i1=lhs2,v="lhs2")
    call dealloc8(i1=lphis2,v="lphis2")
    call dealloc8(i1=limp_colls,v="limp_colls")
!    deallocate(lhypercoll,lcollisions,lheating,lgradients)
    call dealloc8(i1=lhypercoll,v="lhypercoll")
    call dealloc8(i1=lcollisions,v="lcollisions")
    call dealloc8(i1=lheating,v="lheating")
    call dealloc8(i1=lgradients,v="lgradients")
!    deallocate(lupar2,luperp2)
    call dealloc8(i1=lupar2,v="lupar2")
    call dealloc8(i1=luperp2,v="luperp2")
!    deallocate(lkphi2,lg0phi2)
    call dealloc8(i1=lkphi2,v="lkphi2")
    call dealloc8(i1=lg0phi2,v="lg0phi2")
!    deallocate(ltxx,ltyy,ltzz)
    call dealloc8(i1=ltxx,v="ltxx")
    call dealloc8(i1=ltyy,v="ltyy")
    call dealloc8(i1=ltzz,v="ltzz")
!    deallocate(ldelfs2_pt,lhs2_pt,lphis2_pt)
    call dealloc8(i1=ldelfs2_pt,v="ldelfs2_pt")
    call dealloc8(i1=lhs2_pt,v="lhs2_pt")
    call dealloc8(i1=lphis2_pt,v="lphis2_pt")
!    deallocate(lupar2_pt,luperp2_pt)
    call dealloc8(i1=lupar2_pt,v="lupar2_pt")
    call dealloc8(i1=luperp2_pt,v="luperp2_pt")
!    deallocate(lkphi2_pt,lg0phi2_pt)
    call dealloc8(i1=lkphi2_pt,v="lkphi2")
    call dealloc8(i1=lg0phi2_pt,v="lg0phi2_pt")
!    deallocate(ltxx_pt,ltyy_pt,ltzz_pt)
    call dealloc8(i1=ltxx_pt,v="lxx_pt")
    call dealloc8(i1=ltyy_pt,v="ltyy_pt")
    call dealloc8(i1=ltzz_pt,v="ltzz_pt")

  end subroutine finish_heating_index
!------------------------------------------------------------------------------
end module agk_heating_index
