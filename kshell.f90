!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!      Make shell summed spectra similar to .kspec_avg      !!!
!!!                              written by Tomo TATSUNO      !!!
!!!   Simple sum over the shell rather than geometric mean    !!!
!!!   used in .kspec_avg so that the sum over k of spectral   !!!
!!!   plot reflects the total amount.                         !!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module kshell

  implicit none

  public :: init_shell_spectrum, finish_kshell
  public :: write_shell_spectrum, write_g2_kshell_p
  public :: write_ktransfer, get_wtrans, get_etrans
  public :: write_ktrans, use_qshell, write_gkshp ! flags

  private

  logical :: write_ktrans, use_qshell, write_gkshp
  logical :: get_wtrans, get_etrans
  integer :: kshell_unit, gkshp_unit
  integer :: ktrans_unit=-1, kflux_unit=-1
  integer :: nkshell ! number of k's in shell-averaged spectra
  integer :: nkshz=-1 ! number of negative k's for zonal component
  real, dimension (:), allocatable :: kpshell ! (-nkshz:nkshell)
  integer, dimension (:,:), allocatable :: shell_index ! (1:nakx,1:naky)->(-nkshz:nkshell)

contains

  subroutine init_shell_spectrum

    use file_utils, only: open_output_file
    use species, only: nspec
    integer :: ispec

! NOTE: In this routine, a square domain is assumed (nx=ny & x0=y0)
! In agk_diagnostics, write_Eshell => .false. if nx /= ny or x0 /= y0

    ! allocate and define wave-number shells and map
    call init_kshell

    call open_output_file (kshell_unit, '.kshell')
    write (kshell_unit, '(a,6x,a,4x,a)', advance='no') '#  time', 'kperp', 'Ephik'
    do ispec=1, nspec
       write (kshell_unit, '(7x,a,i2,a)', advance='no') 'Wgk(', ispec, ')'
    end do
    write (kshell_unit, *)

  end subroutine init_shell_spectrum

  subroutine init_kshell
    !
    ! define nkshell, kpshell and shell_index
    !
!    use mp, only: proc0
    use kgrids, only: nakx, naky, akx, aky, kperp2
    use run_parameters, only: tite
    use agk_mem, only: alloc8
    real :: dkp
    integer :: j

    if (allocated(kpshell)) return

    ! Determine limits of kperp spectra shells
    dkp = aky(2)
    nkshell = int(aky(naky)/dkp) ! is equal to naky-1
    if (use_qshell) nkshz = int(sqrt(tite)/dkp) + 1

    allocate (kpshell(-nkshz:nkshell)); call alloc8(r1=kpshell,v="kpshell")
    kpshell = (/ (dkp * j, j=-nkshz, nkshell) /)

    allocate (shell_index(nakx,naky)); call alloc8(i2=shell_index,v="shell_index")
    shell_index = anint(sqrt(kperp2)/dkp)
    shell_index(1,1) = nkshell * 2 ! exclude (kx,ky)=(0,0) from count
!    if (proc0) print *, shell_index

    if (use_qshell) shell_index(:,1) &
         & = anint( sign( sqrt(abs(akx**2-tite))/dkp, akx**2-tite ) )
!    if (proc0) print *, shell_index(:,1)

  end subroutine init_kshell

  subroutine finish_kshell

    use file_utils, only: close_output_file
    use agk_mem, only: dealloc8

    if (ktrans_unit /= -1) call close_output_file (ktrans_unit)
    if (kflux_unit /= -1)  call close_output_file (kflux_unit)

!    deallocate (kpshell, shell_index)
    call dealloc8(r1=kpshell,v="kpshell")
    call dealloc8(i2=shell_index,v="shell_index")
    call close_output_file (kshell_unit)

  end subroutine finish_kshell

! NOTE: shell_index connects a given mode to the correct kperp shell
! NOTE: It is assumed here that weighting factors for ky=0 are already 
! incorporated in ee
  subroutine get_shell_spectrum (ee, esh)

    use kgrids, only: nakx, naky

    real, dimension (:,:), intent (in) :: ee   ! variable ee by (kx,ky) mode
    real, dimension (-nkshz:), intent (out) :: esh ! variable ee in kperp shells
    integer :: ik, it, ip

    esh = 0.0

    ! Loop through all modes and sum number at each kperp
    do ik=1, naky
       do it=1, nakx
          ip = shell_index(it,ik)
          if (ip <= nkshell) esh(ip) = esh(ip) + ee(it,ik)
       end do
    end do

  end subroutine get_shell_spectrum

  subroutine write_shell_spectrum

    use mp, only: proc0
    use file_utils, only: flush_output_file
    use agk_layouts, only: g_lo
    use kgrids, only: nakx, naky
    use species, only: nspec
    use fields_arrays, only: phinew
    use dist_fn_arrays, only: gnew
    use dist_fn, only: gamtot
    use agk_time, only: time
    use le_grids, only: integrate_moment

    integer :: iksh, ispec, ig=0 ! took ig=0 for simplicity
    real, dimension (nakx,naky,nspec) :: Wgk2 ! nspec=1
    real, dimension (-nkshz:nkshell,nspec) :: Wgk
    real, dimension (nakx,naky) :: Ephik2
    real, dimension (-nkshz:nkshell) :: Ephik
    real, dimension (2, g_lo%llim_proc:g_lo%ulim_alloc) :: gtmp

    ! W_g(k) spectrum
    ! sum(Wgk2) gives total W_g = (1/2) int |g|^2/(2F_0) dv dR
    gtmp(:,:) = 0.5 * real( conjg(gnew(ig,:,:)) * gnew(ig,:,:) )
    call integrate_moment (gtmp, Wgk2)
    Wgk2(:,2:,:) = 0.5 * Wgk2(:,2:,:) ! extra factor for ky/=0
    if (proc0) then
       do ispec=1, nspec
          call get_shell_spectrum (Wgk2(:,:,ispec), Wgk(:,ispec))
       end do
    end if

    ! E_phi(k) spectrum
    ! sum(Ephik2) gives total E_phi = (1/2) sum_k (1-Gamma_0) |phik|^2 
    if (proc0) then
       Ephik2 = 0.5 * gamtot * real(conjg(phinew(ig,:,:)) * phinew(ig,:,:))
       Ephik2(:,2:) = 0.5 * Ephik2(:,2:) ! extra factor for ky/=0
       call get_shell_spectrum (Ephik2, Ephik)
    end if

    if (proc0) then
       do iksh=-nkshz, nkshell
          write (kshell_unit, '(2f10.5,e12.4e3)', advance='no') time, kpshell(iksh), Ephik(iksh)
          do ispec=1, nspec
             write (kshell_unit, '(e12.4e3)', advance='no') Wgk(iksh,ispec)
          end do
          write (kshell_unit, *)
       end do
       write (kshell_unit, *)
       call flush_output_file (kshell_unit)
    end if

  end subroutine write_shell_spectrum

  subroutine write_g2_kshell_p (ek2p)

    use file_utils, only: open_output_file, flush_output_file, error_unit
    use mp, only: proc0
    use agk_time, only: time
    use le_grids, only: npgrid
    use dist_fn_arrays, only: gp
    real, dimension (:,:,:), intent (in) :: ek2p ! (nakx, naky, npgrid)
!    real, dimension (-nkshz:nkshell) :: esh
    real, allocatable :: esh(:)
    integer :: ip, iksh
    logical, save :: first=.true.

    if (first) then
       if (proc0) then
! now the 2D spectrum uses simple shell summation
! write_Eshell does not need to be on
          call init_kshell
          call open_output_file (gkshp_unit, '.gkshp')
          write (gkshp_unit, '(a,2(10x,a),9x,a)') &
               '#  time', 'p', 'kperp', 'E(kperp,p)'
       end if
       first = .false.
    end if

    if (proc0) then
       if (.not.allocated(esh)) allocate(esh(-nkshz:nkshell))
       do ip=1, npgrid
          call get_shell_spectrum (ek2p(:,:,ip), esh)
          do iksh=1, nkshell
             write (gkshp_unit,'(3es13.5,es14.5e3)') &
                  time, gp(ip), kpshell(iksh), esh(iksh)
          end do
          write (gkshp_unit,*)
       end do
       write(gkshp_unit,*)
       call flush_output_file (gkshp_unit)
       if (allocated(esh)) deallocate(esh)
    end if

  end subroutine write_g2_kshell_p

  subroutine write_ktransfer

    use mp, only: proc0
    use file_utils, only: open_output_file, flush_output_file
    use agk_time, only: time
    use agk_mem, only: alloc8, dealloc8
    integer :: ikfrom, ikto, ip
    real, dimension (:,:), allocatable :: wtrans, etrans

    if (.not.allocated(shell_index)) call init_kshell ! set nkshell for iproc/=0
    allocate (wtrans(-nkshz:nkshell,-nkshz:nkshell)); call alloc8(r2=wtrans,v="wtrans")
    allocate (etrans(-nkshz:nkshell,-nkshz:nkshell)); call alloc8(r2=etrans,v="etrans")
    call get_ktransfer (wtrans, etrans)

    if (proc0) then

       if (ktrans_unit == -1) call open_output_file (ktrans_unit, '.ktrans')
       if (kflux_unit == -1)  call open_output_file (kflux_unit, '.kflux')

       ! write transfer function
       do ikfrom=-nkshz, nkshell
          do ikto=-nkshz, nkshell
             write (ktrans_unit, '(3f10.5,2es15.5e3)') time, kpshell(ikfrom), &
                  & kpshell(ikto), wtrans(ikfrom,ikto), etrans(ikfrom,ikto)
          end do
          write (ktrans_unit, '()')
       end do
       write (ktrans_unit, '()')

       ! write flux
       do ip=1, nkshell
          write (kflux_unit, '(2f10.5,4es15.5e3)') time, kpshell(ip), &
               & sum(wtrans(1:ip-1,ip)), sum(wtrans(ip+1:nkshell,ip)), &
               & sum(etrans(1:ip-1,ip)), sum(etrans(ip+1:nkshell,ip))
       end do
       write (kflux_unit, '()')

       call flush_output_file (ktrans_unit)
       call flush_output_file (kflux_unit)

    end if

!    deallocate (wtrans, etrans)
    call dealloc8 (r2=wtrans,v="wtrans")
    call dealloc8 (r2=etrans,v="etrans")

  end subroutine write_ktransfer

  ! TT: requires lots of memory
  ! can be reduced with extra cost in computational time
  subroutine get_ktransfer (wtrans, etrans)

    use constants, only: zi
    use kgrids, only: akx, aky, nx, ny
    use le_grids, only: integrate_moment
    use species, only: nspec
    use agk_layouts, only: it_idx, ik_idx, g_lo, accelx_lo
    use agk_mem, only: alloc8, dealloc8
    use fields_arrays, only: phinew
    use dist_fn_arrays, only: gnew, aj0
    use agk_transforms, only: transform2
    real, dimension (-nkshz:,-nkshz:), intent (out) :: wtrans, etrans
    integer :: iglo, it, ik, ikfrom, ikto, iac
    real, dimension (nx,ny,nspec) :: mom
    ! glo_index: (nsign, g_lo) -> (-nkshz:nkshell)
    integer, dimension (:,:), allocatable, save :: glo_index
    complex, dimension (:,:), allocatable :: g0, vexb
    real, dimension (:,:), allocatable :: gtmp
    real, dimension (:,:,:), allocatable :: gto, pbrw, phito, pbre, vexbr, gr

    wtrans = 0.0 ; etrans = 0.0
    if ( (.not.get_wtrans) .and. (.not.get_etrans) ) return

    if (.not.allocated(glo_index)) then
       allocate (glo_index(2, g_lo%llim_proc:g_lo%ulim_alloc)); call alloc8 (i2=glo_index, v='glo_index')
       glo_index = nkshell * 2 ! invalid value
       do iglo=g_lo%llim_proc, g_lo%ulim_proc
          it = it_idx(g_lo,iglo)
          ik = ik_idx(g_lo,iglo)
          if (it==1 .and. ik==1) cycle
          glo_index(:,iglo) = shell_index(it,ik)
       end do
    end if

    ! These arrays are smaller than pbrw/e, phito, and gto
    allocate (gtmp(2, accelx_lo%llim_proc:accelx_lo%ulim_alloc)); call alloc8 (r2=gtmp, v='gtmp_ktrans') ; gtmp = 0.0
    allocate (g0(2, g_lo%llim_proc:g_lo%ulim_alloc)); call alloc8 (c2=g0, v='g0_ktrans') ; g0 = (0.0, 0.0)
    allocate (vexb(2, g_lo%llim_proc:g_lo%ulim_alloc)); call alloc8 (c2=vexb, v='vexb_ktrans') ; vexb = (0.0, 0.0)

    ! For now I assume yxles layout with proper # of processors
    ! no species factor (such as charge, temperature etc) taken care

    if (get_wtrans) then

       allocate (pbrw(2, accelx_lo%llim_proc:accelx_lo%ulim_alloc, -nkshz:nkshell)); call alloc8 (r3=pbrw, v='pbrw')
       allocate (gto(2, accelx_lo%llim_proc:accelx_lo%ulim_alloc, -nkshz:nkshell)); call alloc8 (r3=gto, v='gto_ktrans')
       pbrw = 0.0 ; gto = 0.0

       ! Save vexbr
       allocate (vexbr(2, accelx_lo%llim_proc:accelx_lo%ulim_alloc, 2)); call alloc8 (r3=vexbr, v='vexbr_ktrans') ; vexbr = 0.0
       do iglo=g_lo%llim_proc, g_lo%ulim_proc
          it = it_idx(g_lo,iglo)
          ik = ik_idx(g_lo,iglo)
          vexb(:,iglo) = -zi * aky(ik) * aj0(iglo) * phinew(0,it,ik) ! ig=0
       end do
       call transform2 (vexb, vexbr(:,:,1), iac)
       do iglo=g_lo%llim_proc, g_lo%ulim_proc
          it = it_idx(g_lo,iglo)
          ik = ik_idx(g_lo,iglo)
          vexb(:,iglo) = zi * akx(it) * aj0(iglo) * phinew(0,it,ik) ! ig=0
       end do
       call transform2 (vexb, vexbr(:,:,2), iac)

       ! First nkshell loop for 'from' wave-number shell (poisson bracket)
       do ikfrom=-nkshz, nkshell
          ! vx . grad g_Q
          g0 = (0.0, 0.0)
          do iglo=g_lo%llim_proc, g_lo%ulim_proc
             if (glo_index(1,iglo) /= ikfrom) cycle
             it = it_idx(g_lo,iglo)
             g0(:,iglo) = zi * akx(it) * gnew(0,:,iglo) ! took ig=0
          end do
          call transform2 (g0, gtmp, iac)
          pbrw(:,:,ikfrom) = vexbr(:,:,1) * gtmp
          ! vy . grad g_Q
          g0 = (0.0, 0.0)
          do iglo=g_lo%llim_proc, g_lo%ulim_proc
             if (glo_index(1,iglo) /= ikfrom) cycle
             ik = ik_idx(g_lo,iglo)
             g0(:,iglo) = zi * aky(ik) * gnew(0,:,iglo) ! took ig=0
          end do
          call transform2 (g0, gtmp, iac)
          pbrw(:,:,ikfrom) = pbrw(:,:,ikfrom) + vexbr(:,:,2) * gtmp
       end do

       call dealloc8 (r3=vexbr, v='vexbr_ktrans')

       ! Second nkshell loop for 'to' wave-number shell
       do ikto=-nkshz, nkshell
          ! g_K
          g0 = (0.0, 0.0)
          where (glo_index == ikto) g0 = gnew(0,:,:) ! took ig=0
          call transform2 (g0, gto(:,:,ikto), iac)
       end do

       ! Computation of transfer function
       ! In order to save memory, the above two loops for 'from' and 'to' may be
       ! placed within this loop --- 'to' loop should be inner so that heavier
       ! poisson bracket in the 'from' loop is evaluated fewer times
       do ikto=-nkshz, nkshell
          do ikfrom=-nkshz, nkshell
             call integrate_moment &
                  (accelx_lo, pbrw(:,:,ikfrom)*gto(:,:,ikto), mom)
             wtrans(ikfrom,ikto) = - sum(mom(:,:,1)) / (nx*ny) ! took ispec=1
          end do
       end do

       call dealloc8 (r3=pbrw, v='pbrw')
       call dealloc8 (r3=gto, v='gto_ktrans')

    end if

    if (get_etrans) then

       ! Allocation
       allocate (pbre(2, accelx_lo%llim_proc:accelx_lo%ulim_alloc, -nkshz:nkshell)); call alloc8 (r3=pbre, v='pbre')
       allocate (phito(2, accelx_lo%llim_proc:accelx_lo%ulim_alloc, -nkshz:nkshell)); call alloc8 (r3=phito, v='phito')
       pbre = 0.0 ; phito = 0.0

       allocate (gr(2, accelx_lo%llim_proc:accelx_lo%ulim_alloc, 2)); call alloc8 (r3=gr, v='gr') ; gr = 0.0
       do iglo=g_lo%llim_proc, g_lo%ulim_proc
          it = it_idx(g_lo,iglo)
          g0(:,iglo) = zi * akx(it) * gnew(0,:,iglo) ! took ig=0
       end do
       call transform2 (g0, gr(:,:,1), iac)
       do iglo=g_lo%llim_proc, g_lo%ulim_proc
          ik = ik_idx(g_lo,iglo)
          g0(:,iglo) = zi * aky(ik) * gnew(0,:,iglo) ! took ig=0
       end do
       call transform2 (g0, gr(:,:,2), iac)

       ! First nkshell loop for 'from' wave-number shell (poisson bracket)
       do ikfrom=-nkshz, nkshell
          ! vx_Q . grad g
          vexb = (0.0, 0.0)
          do iglo=g_lo%llim_proc, g_lo%ulim_proc
             if (glo_index(1,iglo) /= ikfrom) cycle
             it = it_idx(g_lo,iglo)
             ik = ik_idx(g_lo,iglo)
             vexb(:,iglo) = -zi * aky(ik) * aj0(iglo) * phinew(0,it,ik) ! ig=0
          end do
          call transform2 (vexb, gtmp, iac)
          pbre(:,:,ikfrom) = gtmp * gr(:,:,1)
          ! vy_Q . grad g
          vexb = (0.0, 0.0)
          do iglo=g_lo%llim_proc, g_lo%ulim_proc
             if (glo_index(1,iglo) /= ikfrom) cycle
             it = it_idx(g_lo,iglo)
             ik = ik_idx(g_lo,iglo)
             vexb(:,iglo) = zi * akx(it) * aj0(iglo) * phinew(0,it,ik) ! ig=0
          end do
          call transform2 (vexb, gtmp, iac)
          pbre(:,:,ikfrom) = pbre(:,:,ikfrom) + gtmp * gr(:,:,2)
       end do

       call dealloc8 (r3=gr, v='gr')

       ! Second nkshell loop for 'to' wave-number shell
       do ikto=-nkshz, nkshell
          ! <phi_K>
          g0 = (0.0, 0.0)
          do iglo=g_lo%llim_proc, g_lo%ulim_proc
             if (glo_index(1,iglo) /= ikto) cycle
             it = it_idx(g_lo,iglo)
             ik = ik_idx(g_lo,iglo)
             g0(:,iglo) = aj0(iglo) * phinew(0,it,ik) ! took ig=0
          end do
          call transform2 (g0, phito(:,:,ikto), iac)
       end do

       ! Computation of transfer function
       do ikto=-nkshz, nkshell
          do ikfrom=-nkshz, nkshell
             call integrate_moment &
                  (accelx_lo, pbre(:,:,ikfrom)*phito(:,:,ikto), mom)
             etrans(ikfrom,ikto) = - sum(mom(:,:,1)) / (nx*ny) ! took ispec=1
          end do
       end do

       call dealloc8 (r3=pbre, v='pbre')
       call dealloc8 (r3=phito, v='phito')

    end if

    call dealloc8 (r2=gtmp, v='gtmp_ktrans')
    call dealloc8 (c2=g0, v='g0_ktrans')
    call dealloc8 (c2=vexb, v='vexb_ktrans')

  end subroutine get_ktransfer

end module kshell
