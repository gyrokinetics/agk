module layouts_type
  ! This can be made by just replacing ntheta0 by nakx
  !   from GS2's layouts_type.f90

  implicit none
  public

  type :: g_layout_type
     sequence
     integer :: iproc
     integer :: naky, nakx, nlambda, negrid, nspec
     integer :: llim_world, ulim_world, llim_proc, ulim_proc, ulim_alloc, blocksize
  end type g_layout_type

  type :: lz_layout_type
     sequence
     integer :: iproc
     integer :: ntgrid, naky, nakx, negrid, nspec, nlambda
     integer :: llim_world, ulim_world, llim_proc, ulim_proc, ulim_alloc, blocksize, gsize
     integer :: llim_group, ulim_group, igroup, ngroup, nprocset, iset, nset, groupblocksize
  end type lz_layout_type

  type :: e_layout_type
     sequence
     integer :: iproc
     integer :: ntgrid, naky, nakx, nlambda, nspec, nsign
     integer :: llim_world, ulim_world, llim_proc, ulim_proc, ulim_alloc, blocksize
  end type e_layout_type

  type :: le_layout_type
     sequence
     integer :: iproc
     integer :: ntgrid, naky, nakx, nspec
     integer :: llim_world, ulim_world, llim_proc, ulim_proc, ulim_alloc, blocksize
  end type le_layout_type

end module layouts_type
