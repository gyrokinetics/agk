module species
  implicit none

  public :: init_species
  public :: nspec, specie, spec
  public :: ion_species, electron_species, tracer_species
  public :: has_electron_species
  public :: adapt_hc_any

  type :: specie
     real :: z
     real :: mass
     real :: dens
     real :: dens0,u0,tpar0,tperp0
     real :: temp
     real :: tprim
     real :: fprim
     real :: uprim
     real :: nu, nu_h !Collisional and hypercollisional coefficients
     real :: nexp_h    !Exponent for hypercollisionality operator (nu_h ~ kperp^nexp_h)
     logical :: adapt_hc   !T=adaptive hypercollisionality (only used for electrons)
     real :: kp_hc         !kperp value for hypercollisional control
     real :: dkp_hc        !Width of kperp band for determining hypercollisional control
     real :: gw_hc         !Target value of gamma/omega 
     real :: gw_frac       !Max Fractional difference threshold |fm-ft|/(fm+ft)
     real :: min_nuh,max_nuh  !Bracketing limits for adaptive nuh 
     real :: stm, zstm, tz, smz, zt
     integer :: type
  end type specie

  private

  integer, parameter :: ion_species = 1
  integer, parameter :: electron_species = 2 ! for collision operator
  integer, parameter :: tracer_species = 3 ! for test particle diffusion studies

  integer :: nspec
  type (specie), dimension (:), allocatable :: spec
  logical :: adapt_hc_any=.false.

contains

  subroutine init_species
    implicit none
    logical, save :: initialized = .false.
    if (initialized) return
    initialized = .true.

    call read_parameters
  end subroutine init_species

  subroutine read_parameters
    use file_utils, only: input_unit, error_unit, get_indexed_namelist_unit, input_unit_exist
    use text_options, only: text_option, get_option_value
    use mp, only: proc0, broadcast, mp_abort
    implicit none
    real :: z, mass, dens, dens0, u0, tpar0, tperp0, &
         & temp, tprim, fprim, uprim, nu, nu_h, nexp_h
    logical :: adapt_hc
    real :: kp_hc, dkp_hc, gw_hc, gw_frac, min_nuh, max_nuh
    character(20) :: type
    integer :: unit
    integer :: is
    namelist /species_knobs/ nspec
    namelist /species_parameters/ z, mass, dens, dens0, u0, tpar0, tperp0, &
         temp, tprim, fprim, uprim, type, nu, nu_h,  nexp_h, &
         adapt_hc, kp_hc, dkp_hc, gw_hc, gw_frac, min_nuh, max_nuh
    integer :: ierr, in_file
    logical :: exist
    integer :: ireaderr1=0
    integer, allocatable :: ireaderr2(:)
    logical :: abort_readerr=.true.
    character (len=500) :: msg

    type (text_option), dimension (5), parameter :: typeopts = &
         (/ text_option('default', ion_species), &
            text_option('ion', ion_species), &
            text_option('electron', electron_species), &
            text_option('e', electron_species), &
            text_option('trace', tracer_species) /)

    if (proc0) then
       nspec = 2
       in_file = input_unit_exist("species_knobs", exist)
       if (exist) read (unit=in_file, nml=species_knobs, iostat=ireaderr1)

       if (nspec < 1) then
          ierr = error_unit()
          write (unit=ierr, &
               fmt="('Invalid nspec in species_knobs: ', i5)") nspec
          stop
       end if
    end if

    call broadcast (ireaderr1)
    if (abort_readerr) then
       if (ireaderr1 > 0) then
          call mp_abort ('Read error at species_knobs')
       else if (ireaderr1 < 0) then
          call mp_abort ('End of file/record occurred at species_knobs')
       endif
    endif

    call broadcast (nspec)
    allocate (spec(nspec))

    allocate (ireaderr2(nspec)) 
    ireaderr2=0
    if (proc0) then
       do is = 1, nspec
          call get_indexed_namelist_unit (unit, "species_parameters", is)
          u0 = 0.
          dens0 = 1.0
          tpar0 = 0.
          tperp0 = 0.
          uprim = 0.0
          nu = 0.0
          nu_h = 0.0
          nexp_h = 4.0
          adapt_hc=.false.
          kp_hc = 0.0
          dkp_hc = 0.0
          gw_hc = 1.0
          gw_frac = 0.10
          min_nuh = 0.
          max_nuh = 0.
          type = "default"
          read (unit=unit, nml=species_parameters, iostat=ireaderr2(is))
          close (unit=unit)

          spec(is)%z = z
          spec(is)%mass = mass
          spec(is)%dens = dens
          spec(is)%dens0 = dens0
          spec(is)%u0 = u0
          spec(is)%tpar0 = tpar0
          spec(is)%tperp0 = tperp0
          spec(is)%temp = temp
          spec(is)%tprim = tprim
          spec(is)%fprim = fprim
          spec(is)%uprim = uprim
          spec(is)%nu = nu

          !Hypercollisionality with adaptive control
          spec(is)%nu_h = nu_h
          spec(is)%nexp_h = nexp_h
          spec(is)%adapt_hc = adapt_hc
          spec(is)%kp_hc = kp_hc
          spec(is)%dkp_hc = dkp_hc
          spec(is)%gw_hc = gw_hc
          spec(is)%gw_frac = gw_frac
          spec(is)%min_nuh = min_nuh
          spec(is)%max_nuh = max_nuh
          !Set flag if any species has adaptive hypercollisionality
          adapt_hc_any = adapt_hc_any .or. adapt_hc

          spec(is)%stm = sqrt(temp/mass)
          spec(is)%zstm = z/sqrt(temp*mass)
          spec(is)%tz = temp/z
          spec(is)%zt = z/temp
          spec(is)%smz = abs(sqrt(temp*mass)/z)

          ierr = error_unit()
          call get_option_value (type, typeopts, spec(is)%type, ierr, "type in species_parameters_x")
       end do
    end if

    call broadcast (ireaderr2)
    if (abort_readerr) then
       do is=1,nspec
          if (ireaderr2(is) > 0) then
             write(msg,*) '  Read error at species_parameters: ',is
             call mp_abort (trim(msg))
          else if (ireaderr2(is) < 0) then
             write(msg,*) '  End of file/record occurred at species_parameters: ',is
             call mp_abort (trim(msg))
          endif
       end do
    endif

    ! quasi-neutrality of the background
    if (proc0) then
       if (nspec > 1) then
          if (has_electron_species(spec) .and. sum(spec(:)%dens*spec(:)%z)/=0.) then
             ierr = error_unit()
             write(ierr,*) 'WARNING: background plasma does not satisfy quasi-neutrality'
          end if
       end if
    end if

    do is = 1, nspec
       call broadcast (spec(is)%z)
       call broadcast (spec(is)%mass)
       call broadcast (spec(is)%dens)
       call broadcast (spec(is)%dens0)
       call broadcast (spec(is)%u0)
       call broadcast (spec(is)%tpar0)
       call broadcast (spec(is)%tperp0)
       call broadcast (spec(is)%temp)
       call broadcast (spec(is)%tprim)
       call broadcast (spec(is)%fprim)
       call broadcast (spec(is)%uprim)
       call broadcast (spec(is)%nu)
       call broadcast (spec(is)%nu_h)
       call broadcast (spec(is)%nexp_h)
       call broadcast (spec(is)%adapt_hc)
       call broadcast (spec(is)%kp_hc)
       call broadcast (spec(is)%dkp_hc)
       call broadcast (spec(is)%gw_hc)
       call broadcast (spec(is)%gw_frac)
       call broadcast (spec(is)%min_nuh)
       call broadcast (spec(is)%max_nuh)
       call broadcast (spec(is)%stm)
       call broadcast (spec(is)%zstm)
       call broadcast (spec(is)%tz)
       call broadcast (spec(is)%zt)
       call broadcast (spec(is)%smz)
       call broadcast (spec(is)%type)
    end do
  end subroutine read_parameters

  pure function has_electron_species (spec)
    implicit none
    type (specie), dimension (:), intent (in) :: spec
    logical :: has_electron_species
    has_electron_species = any(spec%type == electron_species)
  end function has_electron_species

end module species
