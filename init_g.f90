module init_g
  implicit none

  public :: ginit
  public :: init_init_g
  public :: tstart
  public :: reset_init

  public :: single_kpar
  logical ::  single_kpar = .false.
  ! This is used for the function ginit_single_parallel_mode, and specify the
  !  kparallel to initialize. Of course, the box 
  ! is periodic in the parallel direction, and so only harmonics of the box size 
  ! (i.e. ikpar_init) are allowed  EGH</doc>

  public :: ikpar_init
  integer :: ikpar_init
  
  public :: force_single_kpar
  logical :: force_single_kpar
  private

  ! knobs
  integer :: ginitopt_switch
  integer, parameter :: ginitopt_default = 1, ginitopt_test1 = 2, &
       ginitopt_zero = 3, &
       ginitopt_noise = 5, ginitopt_restart_many = 6, ginitopt_continue = 7, &
       ginitopt_nl = 8, ginitopt_kz0 = 9, ginitopt_restart_small = 10, &
       ginitopt_nl2 = 11, ginitopt_nl3 = 12, ginitopt_nl4 = 13, & 
       ginitopt_alf = 15, ginitopt_kpar = 16, &
       ginitopt_gs = 19, ginitopt_recon = 20, &
       ginitopt_nl3r = 21, ginitopt_smallflat = 22, ginitopt_recon2 = 23, &
       ginitopt_recon3 = 24, ginitopt_gk_eigen = 25, ginitopt_single_parallel_mode = 26
  real :: dphiinit, phiinit, imfac, refac, zf_init
  real :: k0, kw, k1, kw1, p1, pw1, diag2bath
  real :: den0, upar0, tpar0, tperp0
  real :: den1, upar1, tpar1, tperp1
  real :: den2, upar2, tpar2, tperp2
  real :: tstart, scale, apar0
  logical :: chop_side, left, even
  !!! RN> definitions related to agk_save is moved to agk_save.fpp
!!!  character(300) :: restart_file
!!!  character (len=150) :: restart_dir
!!!  integer :: ndigits                   !Number of digits to keep in .nc.* restart filenames
!!!  logical :: separate_em_in            !T=read EM fields from filename.nc.em
!!!  logical :: separate_em_out           !T=write EM fields to filename.nc.em
  !!! RN<
  character (len=20) :: nl_option, nl_option2
  integer, dimension(2) :: ikk, itt
  integer, dimension(6) :: ikkk, ittt
  complex, dimension (6) :: phiamp, aparamp
  ! RN> for recon3
  real :: phiinit0 ! amplitude of equilibrium
  real :: phiinit_rand ! amplitude of random perturbation
  integer :: phiinit_rand_seed
  real :: a0,b0 ! amplitude of equilibrium Apara or By 
  ! if b0 /= 0, u0 is rescaled to give this By amplitude by overriding a0
  ! if a0 /= 0, u0 is rescaled to give this Apara amplitude
  logical :: null_phi, null_bpar, null_apar ! nullify fields
  ! if use_{Phi,Bpar,Apar} = F, override corresponding flags
  integer :: adj_spec ! adjust input parameter of this spec
  ! if = 0, just warn if given conditions are not satisfied
  character (len=20) :: eq_type = ''
  ! equilibrium type = 'sinusoidal', 'porcelli', 'doubleharris'
  real :: prof_width=-.05
  ! width of porcelli, doubleharris profile (for porcelli and doubleharris)
  ! if negative, this gives the ratio to the box size
  integer :: eq_mode_n=0, eq_mode_u=1
  ! mode number in x for sinusoidal equilibrium
  complex :: nkxy_pt(6), ukxy_pt(6)
  ! <RN
  
  logical :: debug=.false.

  !>  This is used  in linear runs with flow shear  in order to track the
  !! evolution of a single Lagrangian mode. EGH
  integer :: ikx_init

contains

  subroutine init_init_g
!    use agk_save, only: init_save
    use agk_layouts, only: init_agk_layouts

    implicit none
    logical, save :: initialized = .false.

    if (initialized) return
    initialized = .true.

!    call init_agk_layouts

    call read_parameters

  end subroutine init_init_g

  subroutine ginit (restarted)

    use agk_save, only: init_tstart
    use file_utils, only: error_unit
    logical, intent (out) :: restarted
    real :: t0
    integer :: istatus

    restarted = .false.
    select case (ginitopt_switch)
    case (ginitopt_default)
       call ginit_default
    case (ginitopt_kz0)
       call ginit_kz0
    case (ginitopt_noise)
       call ginit_noise
    case (ginitopt_single_parallel_mode)
       call ginit_single_parallel_mode
    case (ginitopt_kpar)
       call ginit_kpar
    case (ginitopt_gs)
       call ginit_gs
    case (ginitopt_nl) ! ES decaying turbulence problem
       call ginit_nl
    case (ginitopt_nl2) ! Orszag-Tang vortex problem
       call ginit_nl2
    case (ginitopt_nl3r)
       call ginit_nl3r
    case (ginitopt_nl4)
       call ginit_nl4
       call init_tstart (tstart, istatus)
       restarted = .true.
       scale = 1.
    case (ginitopt_recon)
       t0 = tstart
       call init_tstart (tstart, istatus)
       call ginit_recon
       tstart = t0
       restarted = .true.
       scale = 1.
    case (ginitopt_recon2)
       t0 = tstart
       call init_tstart (tstart, istatus)
       call ginit_recon2
       tstart = t0
       restarted = .true.
       scale = 1.
    case (ginitopt_recon3)
       call ginit_recon3
    case (ginitopt_alf)
       call ginit_alf
    case (ginitopt_zero)
       call ginit_zero
    case (ginitopt_restart_many)
       call ginit_restart_many
       call init_tstart (tstart, istatus)
       restarted = .true.
       scale = 1.
    case (ginitopt_restart_small)
       call ginit_restart_small
       call init_tstart (tstart, istatus)
       restarted = .true.
       scale = 1.
    case (ginitopt_smallflat)
       call ginit_restart_smallflat
       call init_tstart (tstart, istatus)
       restarted = .true.
       scale = 1.
    case (ginitopt_continue)
       restarted = .true.
       scale = 1.
    case (ginitopt_gk_eigen)
      call ginit_gk_eigen
    case default
       write (error_unit(),*) 'WARNING: No initial condition subroutine found'
    end select
  end subroutine ginit

  subroutine read_parameters
    use file_utils, only: input_unit, error_unit, input_unit_exist
    use text_options, only: text_option, get_option_value
    use mp, only: proc0, broadcast, mp_abort

    ! backward compat.
    use mp, only: iproc, nproc
    use agk_save, only: restart_file_save => restart_file
    use agk_save, only: restart_dir
    use agk_save, only: ndigits, separate_em_in, separate_em_out
    use agk_save, only: restart_save_many
    use agk_save, only: get_suffix_format
    implicit none

    type (text_option), dimension (22), parameter :: ginitopts = &
         (/ text_option('default', ginitopt_default), &
            text_option('noise', ginitopt_noise), &
            text_option('test1', ginitopt_test1), &
            text_option('zero', ginitopt_zero), &
            text_option('many', ginitopt_restart_many), &
            text_option('small', ginitopt_restart_small), &
            text_option('cont', ginitopt_continue), &
            text_option('kz0', ginitopt_kz0), &
            text_option('nl', ginitopt_nl), &
            text_option('nl2', ginitopt_nl2), &
            text_option('nl3', ginitopt_nl3), &
            text_option('nl3r', ginitopt_nl3r), &
            text_option('nl4', ginitopt_nl4), &
            text_option('alf', ginitopt_alf), &
            text_option('gs', ginitopt_gs), &
            text_option('kpar', ginitopt_kpar), &
            text_option('smallflat', ginitopt_smallflat), &
            text_option('recon', ginitopt_recon), &
            text_option('recon2', ginitopt_recon2), &
            text_option('recon3', ginitopt_recon3), &
            text_option('gk_eigen', ginitopt_gk_eigen),&
            text_option('single_parallel_mode', ginitopt_single_parallel_mode) /)
    character(20) :: ginit_option

    namelist /init_g_knobs/ ginit_option, phiinit, k0, kw, chop_side, &
         restart_file, restart_dir, &
         left, ikk, itt, scale, tstart, zf_init, &
         den0, upar0, tpar0, tperp0, imfac, refac, even, &
         den1, upar1, tpar1, tperp1, &
         den2, upar2, tpar2, tperp2, dphiinit, apar0, ikkk, ittt, phiamp, aparamp, &
         phiinit0, phiinit_rand, phiinit_rand_seed, a0, b0, null_phi, null_bpar, null_apar, adj_spec, &
         eq_type, prof_width, eq_mode_u, eq_mode_n, &
         nkxy_pt, ukxy_pt, nl_option, nl_option2, &
         ndigits, separate_em_in, separate_em_out, k1, kw1, p1, pw1, diag2bath, &
         ikpar_init, ikx_init, force_single_kpar

    integer :: ierr, in_file
    logical :: exist
    integer :: ireaderr=0
    logical :: abort_readerr=.true.

    ! backward compt.
    character (300) :: restart_file=''
    character (10) :: suffix, suffix_format
    logical :: input_restart_file = .false.

    tstart = 0.
    scale = 1.0
    ginit_option = "default"
    refac = 1.
    imfac = 0.
    den0 = 1.
    upar0 = 0.
    tpar0 = 0.
    tperp0 = 0.
    den1 = 0.
    upar1 = 0.
    tpar1 = 0.
    tperp1 = 0.
    den2 = 0.
    upar2 = 0.
    tpar2 = 0.
    tperp2 = 0.
    phiinit = 1.0
    dphiinit = 1.0
    zf_init = 1.0
    k0 = 1.0
    kw = 1.0
    k1 = 1.0
    kw1 = 1.0
    p1 = 1.0
    pw1 = 1.0
    diag2bath = 1.0
    apar0 = 0.
    chop_side = .false.
    left = .true.
    even = .true.
    ikk(1) = 1
    ikk(2) = 2
    itt(1) = 1
    itt(2) = 2
    ikkk(1:6) = 1
    ittt(1:6) = 1
    phiamp(1:6) = cmplx(0.0,0.0)
    aparamp(1:6) = cmplx(0.0,0.0)
    ikkk(1) = 1
    ikkk(2) = 2
    ikkk(3) = 2
    ittt(1) = 1
    ittt(2) = 2
    ittt(3) = 2
    nl_option = 'default'
    nl_option2 = 'default'
    ! >RN
    phiinit0 = 0.
    phiinit_rand = 0.
    phiinit_rand_seed = 0
    a0 = 0.
    b0 = 0.
    null_phi = .false.
    null_bpar = .false.
    null_apar = .false.
    adj_spec = 0
    eq_type = 'sinusoidal'
    prof_width=-0.1
    eq_mode_n=0
    eq_mode_u=1
    nkxy_pt=cmplx(0.,0.)
    ukxy_pt=cmplx(0.,0.)
    !< EGH
    ikpar_init = 0
    force_single_kpar = .false.
    ikx_init = 0
    ! EGH >
    ! <RN

    !!! RN> definitions related to agk_save is moved to agk_save.fpp
!!!    restart_file = trim(run_name)//".nc"
!!!    restart_dir = "./"
!!!    ndigits=-1     !Number of digits in restart filenames automatically determined
!!!    separate_em_in=.false.
!!!    separate_em_out=.false.
    !!! RN<

    if (proc0) then
       in_file = input_unit_exist ("init_g_knobs", exist)
       if (exist) read (unit=in_file, nml=init_g_knobs, iostat=ireaderr)

       if (len_trim(restart_file) /= 0) input_restart_file=.true.

       ierr = error_unit()
       call get_option_value &
            (ginit_option, ginitopts, ginitopt_switch, &
            ierr, "ginit_option in ginit_knobs")
    endif

    call broadcast (ireaderr)
    if (abort_readerr) then
       if (ireaderr > 0) then
          call mp_abort ('Read error at init_g_knobs')
       else if (ireaderr < 0) then
          call mp_abort ('End of file/record occurred at init_g_knobs')
       endif
    endif

    call broadcast (ginitopt_switch)
    call broadcast (refac)
    call broadcast (imfac)
    call broadcast (den0)
    call broadcast (upar0)
    call broadcast (tpar0)
    call broadcast (tperp0)
    call broadcast (den1)
    call broadcast (upar1)
    call broadcast (tpar1)
    call broadcast (tperp1)
    call broadcast (den2)
    call broadcast (upar2)
    call broadcast (tpar2)
    call broadcast (tperp2)
    call broadcast (phiinit)
    call broadcast (dphiinit)
    call broadcast (zf_init)
    call broadcast (k0)
    call broadcast (kw)
    call broadcast (k1)
    call broadcast (kw1)
    call broadcast (p1)
    call broadcast (pw1)
    call broadcast (diag2bath)
    call broadcast (apar0)
    call broadcast (tstart)
    call broadcast (chop_side)
    call broadcast (even)
    call broadcast (left)

    call broadcast (ikk)
    call broadcast (itt) 
    call broadcast (ikkk)
    call broadcast (ittt) 
    call broadcast (phiamp)
    call broadcast (aparamp)
    call broadcast (scale)

    ! RN>
    call broadcast (phiinit0)
    call broadcast (phiinit_rand)
    call broadcast (phiinit_rand_seed)
    call broadcast (a0)
    call broadcast (b0)
    call broadcast (null_phi)
    call broadcast (null_bpar)
    call broadcast (null_apar)
    call broadcast (adj_spec)
    call broadcast (eq_type)
    call broadcast (prof_width)
    call broadcast (eq_mode_n)
    call broadcast (eq_mode_u)
    call broadcast (nkxy_pt)
    call broadcast (ukxy_pt)
    call broadcast (ikpar_init)
    call broadcast (force_single_kpar)
    call broadcast (ikx_init)

    !!! RN> backward compat.
    call broadcast (ndigits)
    call broadcast (separate_em_in)
    call broadcast (separate_em_out)

    ! RN> prepend restart_dir to restart_file
    ! append trailing slash if not exists
    if (len_trim(restart_dir) /= 0) then
       if (restart_dir(len_trim(restart_dir):) /= "/") &
         restart_dir=trim(restart_dir)//"/"
    endif
    restart_file=trim(restart_dir)//trim(restart_file)
    ! <RN

    call broadcast(input_restart_file)
    if (input_restart_file) then
       call broadcast (restart_file)
       if (restart_save_many) then
          call get_suffix_format(nproc,suffix_format)
          write (suffix,suffix_format) '.', iproc
          restart_file = trim(trim(restart_file)//adjustl(suffix))
       endif
       restart_file_save = restart_file
    endif
    !!! RN<

  end subroutine read_parameters

  subroutine ginit_default
    use species, only: spec
    use theta_grid, only: ntgrid
    use kgrids, only: naky, nakx, aky, reality
    use dist_fn_arrays, only: g, gnew
    use agk_layouts, only: g_lo, ik_idx, it_idx, il_idx, is_idx
    implicit none
    complex, dimension (-ntgrid:ntgrid,nakx,naky) :: phi
    logical :: right
    integer :: iglo
    integer :: ik, it, il, is

    right = .not. left

    if (chop_side .and. left) phi(:-1,:,:) = 0.0
    if (chop_side .and. right) phi(1:,:,:) = 0.0
    
    if (reality) then
       phi(:,1,1) = 0.0

       if (naky > 1 .and. aky(1) == 0.0) then
          phi(:,:,1) = 0.0
       end if

! not used:
! reality condition for k_theta = 0 component:
       do it = 1, nakx/2
          phi(:,it+(nakx+1)/2,1) = conjg(phi(:,(nakx+1)/2+1-it,1))
       enddo
    end if

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       g(:,1,iglo) = phi(:,it,ik)*spec(is)%z*phiinit
       g(:,2,iglo) = g(:,1,iglo)
    end do
    gnew = g
  end subroutine ginit_default

  subroutine ginit_kz0
    use species, only: spec
    use theta_grid, only: ntgrid 
    use kgrids, only: naky, nakx, aky, reality
    use dist_fn_arrays, only: g, gnew
    use agk_layouts, only: g_lo, ik_idx, it_idx, il_idx, is_idx
    implicit none
    complex, dimension (-ntgrid:ntgrid,nakx,naky) :: phi
    logical :: right
    integer :: iglo
    integer :: ik, it, il, is

    right = .not. left

    phi = cmplx(1.0,1.0)
    if (chop_side .and. left) phi(:-1,:,:) = 0.0
    if (chop_side .and. right) phi(1:,:,:) = 0.0
    
    if (reality) then
       phi(:,1,1) = 0.0

       if (naky > 1 .and. aky(1) == 0.0) then
          phi(:,:,1) = 0.0
       end if

! not used:
! reality condition for k_theta = 0 component:
       do it = 1, nakx/2
          phi(:,it+(nakx+1)/2,1) = conjg(phi(:,(nakx+1)/2+1-it,1))
       enddo
    end if

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       g(:,1,iglo) = -phi(:,it,ik)*spec(is)%z*phiinit
       g(:,2,iglo) = g(:,1,iglo)
    end do
    gnew = g
  end subroutine ginit_kz0

  subroutine ginit_noise
    use species, only: spec, tracer_species
    use theta_grid, only: ntgrid 
    use kgrids, only: naky, nakx, aky, reality
    use dist_fn_arrays, only: g, gnew
    use agk_layouts, only: g_lo, ik_idx, it_idx, il_idx, is_idx
    use ran
    implicit none
    complex, dimension (-ntgrid:ntgrid,nakx,naky) :: phi, phit
    real :: a, b
    integer :: iglo
    integer :: ig, ik, it, il, is, nn

    phit = 0.
    do it=2,nakx/2+1
       nn = it-1
! extra factor of 4 to reduce the high k wiggles for now
       phit (:, it, 1) = (-1)**nn*exp(-8.*(real(nn)/nakx)**2)
    end do

! keep old (it, ik) loop order to get old results exactly: 
    do it = 1, nakx
       do ik = 1, naky
          do ig = -ntgrid, ntgrid
             a = ranf()-0.5
             b = ranf()-0.5
!             phi(:,it,ik) = cmplx(a,b)
             phi(ig,it,ik) = cmplx(a,b)
          end do
          if (chop_side) then
             if (left) then
                phi(:-1,it,ik) = 0.0
             else
                phi(1:,it,ik) = 0.0
             end if
          end if
       end do
    end do

    if (naky > 1 .and. aky(1) == 0.0) then
       phi(:,:,1) = phi(:,:,1)*zf_init
    end if
! reality condition for k_theta = 0 component:
    if (reality) then
       do it = 1, nakx/2
          phi(:,it+(nakx+1)/2,1) = conjg(phi(:,(nakx+1)/2+1-it,1))
          phit(:,it+(nakx+1)/2,1) = conjg(phit(:,(nakx+1)/2+1-it,1))
       enddo
    end if
       
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       if (spec(is)%type == tracer_species) then          
          g(:,1,iglo) =-phit(:,it,ik)*spec(is)%z*phiinit
       else
          g(:,1,iglo) = -phi(:,it,ik)*spec(is)%z*phiinit
       end if
       g(:,2,iglo) = g(:,1,iglo)
    end do
    gnew = g

  end subroutine ginit_noise

  subroutine ginit_single_parallel_mode
    use species, only: spec, tracer_species
    use theta_grid, only: ntgrid, theta
    use kgrids, only: naky, nakx, aky, reality
    use dist_fn_arrays, only: g, gnew
    use agk_layouts, only: g_lo, ik_idx, it_idx, il_idx, is_idx
    use ran
    use mp, only: mp_abort
    implicit none
    complex, dimension (-ntgrid:ntgrid,nakx,naky) :: phi, phit
    real :: a, b
    integer :: iglo
    integer :: ig, ik, it, il, is, nn

    single_kpar = .true.

    phit = 0.
    do it=2,nakx/2+1
       nn = it-1
! extra factor of 4 to reduce the high k wiggles for now
       phit (:, it, 1) = (-1)**nn*exp(-8.*(real(nn)/nakx)**2)
    end do

    if (ikpar_init+1 > ntgrid) then 
      call mp_abort("Error: this value of k_parallel is too large. Increase ntheta or decrease ikpar_init.")
!         stop 'Aborting...'
    end if
! keep old (it, ik) loop order to get old results exactly: 
    do it = 1, nakx
       do ik = 1, naky
          do ig = -ntgrid, ntgrid
                a = cos(ikpar_init * theta(ig)) 
                b = sin(ikpar_init * theta(ig))
!             phi(:,it,ik) = cmplx(a,b)
             phi(ig,it,ik) = cmplx(a,b)
          end do
          if (chop_side) then
             if (left) then
                phi(:-1,it,ik) = 0.0
             else
                phi(1:,it,ik) = 0.0
             end if
          end if
       end do
    end do

    if (naky > 1 .and. aky(1) == 0.0) then
       phi(:,:,1) = phi(:,:,1)*zf_init
    end if
    if (ikx_init  > 0) call single_initial_kx(phi)
! reality condition for k_theta = 0 component:
    if (reality) then
       do it = 1, nakx/2
          phi(:,it+(nakx+1)/2,1) = conjg(phi(:,(nakx+1)/2+1-it,1))
          phit(:,it+(nakx+1)/2,1) = conjg(phit(:,(nakx+1)/2+1-it,1))
       enddo
    end if
       
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       if (spec(is)%type == tracer_species) then          
          g(:,1,iglo) =-phit(:,it,ik)*spec(is)%z*phiinit
       else
          g(:,1,iglo) = -phi(:,it,ik)*spec(is)%z*phiinit
       end if
       g(:,2,iglo) = g(:,1,iglo)
    end do
    gnew = g

  end subroutine ginit_single_parallel_mode

  subroutine single_initial_kx(phi)
    use species, only: tracer_species
    use theta_grid, only: ntgrid
    use kgrids, only: naky, nakx
    use agk_layouts, only: ik_idx, it_idx, il_idx, is_idx
    use ran
    use mp, only: mp_abort

    implicit none
    complex, dimension (-ntgrid:ntgrid,nakx,naky), intent(inout) :: phi
    real :: a, b
    integer :: ig, ik, it

    if (ikx_init  < 2 .or. ikx_init > (nakx+1)/2) then
      call mp_abort("The subroutine single_initial_kx should only be called when 1 < ikx_init < (nakx+1)/2")
    end if

    do it = 1, nakx
      if (it .ne. ikx_init) then 
        !write (*,*) "zeroing out kx_index: ", it, "at kx: ", akx(it)
         do ik = 1, naky
            do ig = -ntgrid, ntgrid
               a = 0.0
               b = 0.0 
  !             phi(:,it,ik) = cmplx(a,b)
               phi(ig,it,ik) = cmplx(a,b)
             end do
         end do
       end if
    end do
  end subroutine single_initial_kx

! TT: made up for decaying turbulence simulation
  subroutine ginit_nl
    use mp, only: proc0, broadcast, nproc
    use file_utils, only: error_unit
    use text_options, only: text_option, get_option_value
    use species, only: spec
    use theta_grid, only: ntgrid, theta
    use kgrids, only: naky, nakx, reality, x0, kperp2
    use le_grids, only: al, e
    use dist_fn_arrays, only: g, gnew, vpa, vperp2, aj0
    use agk_layouts, only: g_lo, ik_idx, it_idx, ie_idx, il_idx, is_idx
    use spfunc, only: j0, j1
    use constants, only: zi, pi
    use ran, only: ranf
    use agk_mem, only: alloc8, dealloc8
    ! TT: For test of Hankel transform
!!$    use dist_fn, only: Hankel_transform
!!$    use dist_fn_arrays, only: gp
!!$    use le_grids, only: negrid, e
!!$    use species, only: nspec
!!$    use agk_layouts, only: ie_idx
    ! <TT
    implicit none
    integer, parameter :: nlopt_direct = 1, nlopt_inverse = 2, &
         nlopt_inverse_random = 3, nlopt_flat = 4, nlopt_inverse_lw = 5, &
         nlopt_dfw = 6 ! Dual forward cascade
    integer, parameter :: nlopt2_gauss = 1, nlopt2_zdens = 2
    type (text_option), dimension (10), parameter :: nlopts = &
         (/ text_option('default',     nlopt_direct), &
            text_option('direct',      nlopt_direct), &
            text_option('forward',     nlopt_direct), &
            text_option('inverse',     nlopt_inverse), &
            text_option('backward',    nlopt_inverse), &
            text_option('inverse_random',    nlopt_inverse_random), &
            text_option('flat',        nlopt_flat), &
            text_option('white noise', nlopt_flat), &
            text_option('inverse_lw', nlopt_inverse_lw), &
            text_option('dual forward', nlopt_dfw) /)
    type (text_option), dimension (3), parameter :: nlopts2 = &
         (/ text_option('default',     nlopt2_gauss), &
            text_option('gaussian', nlopt2_gauss), &
            text_option('zero density', nlopt2_zdens) /)
    complex :: amp
    complex, dimension (-ntgrid:ntgrid,nakx,naky) :: phi, odd
    real, dimension (-ntgrid:ntgrid) :: dfac, ufac, tparfac, tperpfac, ct, st, c2t, s2t
    integer :: nlopt_switch, nlopt2_switch
    integer :: iglo, ik, it, il, ie, is, j, nrand=50, n
    real, dimension (:), allocatable :: vdep, work
    real, dimension (:,:,:), allocatable :: pa, pj
    ! TT: for test of Hankel transform
!!$    integer :: ip, ie
!!$    complex, dimension (nakx, naky, negrid, nspec) :: gkp
    ! <TT

    if (proc0) then
       call get_option_value (nl_option, nlopts, nlopt_switch, &
            error_unit(), 'nl_option in ginit_knobs')
       call get_option_value (nl_option2, nlopts2, nlopt2_switch, &
            error_unit(), 'nl_option2 in ginit_knobs')
    end if
    call broadcast (nlopt_switch)
    call broadcast (nlopt2_switch)

    phi = 0.
    odd = 0.

    select case (nlopt_switch)

    case (nlopt_direct)
       amp = 2.0 * pi ! define it so that tau_{rho,init} becomes unity
       allocate (work(3)) ; work=0.0
       do j = 1, 3
          work(j) = ranf()
       end do
       ! make all processors have common random numbers
       if (nproc > 1) call broadcast (work)
       do j = 1, 3
          ik = ikkk(j)
          it = ittt(j)
          phi(:,it,ik) = amp * exp(zi*work(j)*pi*2.0) ! homogeneous along z
          if (ik == 1) phi(:,it,ik) = phi(:,it,ik) / 2.0
       end do
       deallocate (work)
       ! TT: add seed components to all modes
       allocate (pj(2,nakx,naky)) ; call alloc8(r3=pj,v='pj') ; pj=0.0
       do ik=1, naky
          do it=1, nakx
             do j=1, 2
                pj(j,it,ik) = ranf() - 0.5 ! amplitude in (-0.5,0.5)
             end do
          end do
       end do
       if (nproc > 1) then ! make all processors have common random numbers
          n = 2*nakx*naky
          allocate (work(n)) ; call alloc8 (r1=work,v="work") ; work=0.0
          if (proc0) work(1:n) = reshape(pj, (/n/))
          call broadcast (work)
          if (.not.proc0) pj = reshape(work(1:n), (/2,nakx,naky/))
          call dealloc8 (r1=work,v="work")
       end if
       do ik=1, naky
          do it=1, nakx
             if ( (it==1) .and. (ik==1) ) cycle
             phi(:,it,ik) = phi(:,it,ik) + 1.e-8 * cmplx(pj(1,it,ik),pj(2,it,ik))
          end do
       end do
       call dealloc8 (r3=pj,v='pj')

    case (nlopt_inverse, nlopt_inverse_random, nlopt_dfw)
       allocate (pj(1,nakx,naky)) ; call alloc8(r3=pj,v='pj') ; pj=0.0
       do ik=1, naky
          do it=1, nakx
             pj(1,it,ik) = ranf() - 0.5 ! phase in (-0.5,0.5)
          end do
       end do
       if (nproc > 1) then ! make all processors have common random numbers
          n = nakx*naky
          allocate (work(n)) ; call alloc8 (r1=work,v="work") ; work=0.0
          if (proc0) work(1:n) = reshape(pj, (/n/))
          call broadcast (work)
          if (.not.proc0) pj = reshape(work(1:n), (/1,nakx,naky/))
          call dealloc8 (r1=work,v="work")
       end if
       do ik=1, naky
          do it=1, nakx
             phi(:,it,ik) = exp( - ( (sqrt(kperp2(it,ik))-k0)/kw )**2 ) &
                  * (kperp2(it,ik)/k0**2) * exp(zi*pi*2.*pj(1,it,ik))
          end do
       end do
       phi(:,:,1) = phi(:,:,1) / 2.0 ! ky==0 component
       call dealloc8 (r3=pj,v='pj')

    case (nlopt_flat)
       allocate (pj(2,nakx,naky)) ; call alloc8(r3=pj,v='pj') ; pj=0.0
       do ik=1, naky
          do it=1, nakx
             do j=1, 2
                pj(j,it,ik) = ranf() - 0.5 ! amplitude in (-0.5,0.5)
             end do
          end do
       end do
       if (nproc > 1) then ! make all processors have common random numbers
          n = 2*nakx*naky
          allocate (work(n)) ; call alloc8 (r1=work,v="work") ; work=0.0
          if (proc0) work(1:n) = reshape(pj, (/n/))
          call broadcast (work)
          if (.not.proc0) pj = reshape(work(1:n), (/2,nakx,naky/))
          call dealloc8 (r1=work,v="work")
       end if
       do ik=1, naky
          do it=1, nakx
             if ( (it==1) .and. (ik==1) ) cycle
             phi(:,it,ik) = cmplx(pj(1,it,ik),pj(2,it,ik)) / sqrt(kperp2(it,ik))
          end do
       end do
       phi(:,:,1) = phi(:,:,1) / 2.0 ! ky==0 component
       call dealloc8 (r3=pj,v='pj')

    case (nlopt_inverse_lw)
! <GGP: init zonal component at amplitude reduced by factor k0**2
       allocate (pj(1,nakx,naky)) ; call alloc8(r3=pj,v='pj') ; pj=0.0
       do ik=1, naky
          do it=1, nakx
             pj(1,it,ik) = ranf() - 0.5 ! amplitude in (-0.5,0.5)
          end do
       end do
       if (nproc > 1) then ! make all processors have common random numbers
          n = nakx*naky
          allocate (work(n)) ; call alloc8 (r1=work,v="work") ; work=0.0
          if (proc0) work(1:n) = reshape(pj, (/n/))
          call broadcast (work)
          if (.not.proc0) pj = reshape(work(1:n), (/1,nakx,naky/))
          call dealloc8 (r1=work,v="work")
       end if
       do it=1, nakx
          phi(:,it,1) = k0**2 * exp( - ( (sqrt(kperp2(it,1))-k0)/kw )**2 ) &
               * (kperp2(it,1)/k0**2) * exp(zi*pi*2.*pj(1,it,1))
       end do
       do ik=2, naky
          do it=1, nakx
             phi(:,it,ik) = exp( - ( (sqrt(kperp2(it,ik))-k0)/kw )**2 ) &
                  * (kperp2(it,ik)/k0**2) * exp(zi*pi*2.*pj(1,it,ik))
          end do
       end do
       phi(:,:,1) = phi(:,:,1) / 2.0 ! ky==0 component
       call dealloc8 (r3=pj,v='pj')

    case default
       if (proc0) write (error_unit(),*) 'Illegal nl_option in ginit_nl'

    end select

    odd = zi * phi

! reality condition for kx = 0 component:
    if (reality) then
       do it = 1, nakx/2
          phi(:,it+(nakx+1)/2,1) = conjg(phi(:,(nakx+1)/2+1-it,1))
          odd(:,it+(nakx+1)/2,1) = conjg(odd(:,(nakx+1)/2+1-it,1))
       end do
    end if

    if (even) then
       ct = cos(theta)
       st = sin(theta)

       c2t = cos(2.*theta)
       s2t = sin(2.*theta)
    else
       ct = sin(theta)
       st = cos(theta)

       c2t = sin(2.*theta)
       s2t = cos(2.*theta)
    end if

    dfac     = den0   + den1 * ct + den2 * c2t
    ufac     = upar0  + upar1* st + upar2* s2t
    tparfac  = tpar0  + tpar1* ct + tpar2* c2t
    tperpfac = tperp0 + tperp1*ct + tperp2*c2t

! charge dependence keeps initial Phi from being too small
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       g(:,1,iglo) = phiinit* &!spec(is)%z* &
            ( dfac*spec(is)%dens0                * phi(:,it,ik) &
            + 2.*ufac* vpa(1,iglo)*spec(is)%u0 * odd(:,it,ik) &
            + tparfac*(vpa(1,iglo)**2-0.5)     * phi(:,it,ik) &
            +tperpfac*(vperp2(iglo)-1.)        * phi(:,it,ik))

       g(:,2,iglo) = phiinit* &!spec(is)%z* &
            ( dfac*spec(is)%dens0                * phi(:,it,ik) &
            + 2.*ufac* vpa(2,iglo)*spec(is)%u0 * odd(:,it,ik) &
            + tparfac*(vpa(2,iglo)**2-0.5)     * phi(:,it,ik) &
            +tperpfac*(vperp2(iglo)-1.)        * phi(:,it,ik))
    end do

    if (nlopt_switch == nlopt_inverse) &
         ! add Bessel function dependence
         g = g * spread( spread(aj0,1,2), 1, ntgrid*2+1 )

    if (nlopt_switch == nlopt_inverse_random) then
       ! add random velocity dependence
       allocate (vdep(g_lo%llim_proc:g_lo%ulim_alloc))
       call alloc8(r1=vdep,v="vdep") ; vdep=0.0
       allocate (pa(nrand,nakx,naky)); call alloc8(r3=pa,v="pa"); pa=0.0
       allocate (pj(nrand,nakx,naky)); call alloc8(r3=pj,v="pj"); pj=0.0
       vdep = 0.0
       if (proc0) then
          do ik=1, naky
             do it=1, nakx
                do j=1, nrand
                   ! normal-distribution random number: Box-Muller transform
!!$                   pj(j,it,ik) = abs( sqrt(kperp2(it,ik)) &
!!$                        + kw * sqrt(-log(ranf())) * sin(2.0*pi*ranf()) )
                   pa(j,it,ik) = ranf() * 2.0 - 1.0 ! amplitude
                   ! uniform random number in (0,kperp+kw)
                   pj(j,it,ik) = (sqrt(kperp2(it,ik)) + kw) * ranf()
                end do
             end do
          end do
          if (reality) then
             do it=1, nakx/2
                pa(:,it+(nakx+1)/2,1) = pa(:,(nakx+1)/2+1-it,1)
                pj(:,it+(nakx+1)/2,1) = pj(:,(nakx+1)/2+1-it,1)
             end do
          end if
       end if

       if (nproc > 1) then ! make all processors have common random numbers
          n = nrand*nakx*naky
          allocate (work(2*n)); call alloc8(r1=work,v="work"); work=0.0
          if (proc0) then
             work(1:n) = reshape(pa, (/n/))
             work(n+1:2*n) = reshape(pj, (/n/))
          end if
          call broadcast (work)
          if (.not.proc0) then
             pa = reshape(work(1:n), (/nrand,nakx,naky/))
             pj = reshape(work(n+1:2*n), (/nrand,nakx,naky/))
          end if
          call dealloc8 (r1=work,v="work")
       end if

       do iglo=g_lo%llim_proc, g_lo%ulim_proc
          it = it_idx(g_lo,iglo)
          ik = ik_idx(g_lo,iglo)
          do j=1, nrand
             vdep(iglo) = vdep(iglo) &
!                  + pa(j,it,ik) * cos(pj(j,it,ik)*sqrt(vperp2(iglo)) + ph(j,it,ik)) / nrand
                  ! use Bessel function instead of cosine
                  + pa(j,it,ik) * j0(pj(j,it,ik)*sqrt(vperp2(iglo))) &
                    * sqrt(pj(j,it,ik)) / nrand
          end do
       end do
       g = g * spread( spread(vdep,1,2), 1, ntgrid*2+1 )
       call dealloc8 (r1=vdep,v="vdep")
       call dealloc8 (r3=pa,v="pa")
       call dealloc8 (r3=pj,v="pj")
    end if

    if (nlopt_switch == nlopt_dfw) then
       ! add Bessel function dependence to make up diagonal components
       g = diag2bath * g * spread( spread(aj0,1,2), 1, ntgrid*2+1 )
       ! diagonal part done

       !!!!!!!!!!!!!
       ! bath part !
       !!!!!!!!!!!!!
       allocate (pj(1,nakx,naky)) ; call alloc8(r3=pj,v='pj') ; pj=0.0
       do ik=1, naky
          do it=1, nakx
             pj(1,it,ik) = ranf() - 0.5 ! amplitude in (-0.5,0.5)
          end do
       end do
       if (nproc > 1) then ! make all processors have common random numbers
          n = nakx*naky
          allocate (work(n)) ; call alloc8 (r1=work,v="work") ; work=0.0
          if (proc0) work(1:n) = reshape(pj, (/n/))
          call broadcast (work)
          if (.not.proc0) pj = reshape(work(1:n), (/1,nakx,naky/))
          call dealloc8 (r1=work,v="work")
       end if
       !! wave-number dependence
       !        kperp^2           kperp - k1        2pi*i*(rand-0.5)
       !  phi = ------- exp [ - (------------)^2 ] e
       !          k1^2                kw1
       do ik=1, naky
          do it=1, nakx
             phi(:,it,ik) = exp( - ( (sqrt(kperp2(it,ik))-k1)/kw1)**2 ) &
                  * (kperp2(it,ik)/k1**2) * exp(zi*pi*2.*pj(1,it,ik))
          end do
       end do
       call dealloc8 (r3=pj,v='pj')
       phi(:,:,1) = phi(:,:,1) / 2.0 ! ky==0 component
       if (reality) then
          do it=1, nakx/2
             phi(:,it+(nakx+1)/2,1) = conjg(phi(:,(nakx+1)/2+1-it,1))
          end do
       end if
       odd = zi * phi

       !! velocity dependence
       if (nlopt2_switch == nlopt2_gauss) then ! random bath

          ! add random velocity dependence
          allocate (vdep(g_lo%llim_proc:g_lo%ulim_alloc))
          call alloc8 (r1=vdep,v="vdep") ; vdep=0.0
          allocate (pa(nrand,nakx,naky)) ; call alloc8 (r3=pa,v="pa") ; pa=0.0
          allocate (pj(nrand,nakx,naky)) ; call alloc8 (r3=pj,v="pj") ; pj=0.0
          if (proc0) then
             do ik=1, naky
                do it=1, nakx
                   do j=1, nrand
                      ! normal-distribution random number: Box-Muller transform
                      pj(j,it,ik) = abs( p1 + pw1 * sqrt(-2.0*log(ranf())) &
                           * sin(2.0*pi*ranf()) )
                      pa(j,it,ik) = ranf() * 2.0 - 1.0 ! amplitude in (-1,1)
                   end do
                end do
             end do
             if (reality) then
                do it=1, nakx/2
                   pj(:,it+(nakx+1)/2,1) = pj(:,(nakx+1)/2+1-it,1)
                   pa(:,it+(nakx+1)/2,1) = pa(:,(nakx+1)/2+1-it,1)
                end do
             end if
          end if

          if (nproc > 1) then ! make all processors have common random numbers
             n = nrand*nakx*naky
             allocate (work(2*n)) ; call alloc8 (r1=work,v="work") ; work=0.0
             if (proc0) then
                work(1:n) = reshape(pa, (/n/))
                work(n+1:2*n) = reshape(pj, (/n/))
             end if
             call broadcast (work)
             if (.not.proc0) then
                pa = reshape(work(1:n), (/nrand,nakx,naky/))
                pj = reshape(work(n+1:2*n), (/nrand,nakx,naky/))
             end if
             call dealloc8 (r1=work,v="work")
          end if

          do iglo=g_lo%llim_proc, g_lo%ulim_proc
             it = it_idx(g_lo,iglo)
             ik = ik_idx(g_lo,iglo)
             do j=1, nrand
                vdep(iglo) = vdep(iglo) &
                     ! use Bessel function instead of cosine
                     + pa(j,it,ik) * j0(pj(j,it,ik)*sqrt(vperp2(iglo))) &
                     * sqrt(pj(j,it,ik)) / nrand
             end do
          end do

          call dealloc8 (r3=pa,v="pa")
          call dealloc8 (r3=pj,v="pj")

       else if (nlopt2_switch == nlopt2_zdens) then ! zero density bath

          allocate (vdep(g_lo%llim_proc:g_lo%ulim_alloc))
          call alloc8 (r1=vdep,v="vdep") ; vdep = 0.0

          ! zero density bath
          do iglo=g_lo%llim_proc, g_lo%ulim_proc
             it = it_idx(g_lo,iglo)
             ik = ik_idx(g_lo,iglo)
             ie = ie_idx(g_lo,iglo)
             il = il_idx(g_lo,iglo)
             is = is_idx(g_lo,iglo)
             vdep(iglo) = kperp2(it,ik) - 4.0 + 4.0 * al(il) * e(ie,is)
          end do

       end if

       do iglo = g_lo%llim_proc, g_lo%ulim_proc
          ik = ik_idx(g_lo,iglo)
          it = it_idx(g_lo,iglo)
          is = is_idx(g_lo,iglo)
          g(:,1,iglo) = g(:,1,iglo) + vdep(iglo) * phiinit* &!spec(is)%z* &
               ( dfac*spec(is)%dens0                * phi(:,it,ik) &
               + 2.*ufac* vpa(1,iglo)*spec(is)%u0 * odd(:,it,ik) &
               + tparfac*(vpa(1,iglo)**2-0.5)     * phi(:,it,ik) &
               +tperpfac*(vperp2(iglo)-1.)        * phi(:,it,ik))

          g(:,2,iglo) = g(:,2,iglo) + vdep(iglo) * phiinit* &!spec(is)%z* &
               ( dfac*spec(is)%dens0                * phi(:,it,ik) &
               + 2.*ufac* vpa(2,iglo)*spec(is)%u0 * odd(:,it,ik) &
               + tparfac*(vpa(2,iglo)**2-0.5)     * phi(:,it,ik) &
               +tperpfac*(vperp2(iglo)-1.)        * phi(:,it,ik))
       end do
       
       call dealloc8 (r1=vdep,v="vdep")

    end if

    ! TT: Testing Hankel transform
!!$    call Hankel_transform (g, 0, gkp, 1) ! last one for all processors
!!$    g = (0.0, 0.0)
!!$    do iglo = g_lo%llim_proc, g_lo%ulim_proc
!!$       it = it_idx(g_lo,iglo)
!!$       ik = ik_idx(g_lo,iglo)
!!$       is = is_idx(g_lo,iglo)
!!$       do ip=1, negrid
!!$          g(:,:,iglo) = g(:,:,iglo) &
!!$               + gp(ip) * gkp(it,ik,ip,is) * j0(gp(ip)*sqrt(vperp2(iglo)))
!!$       end do
!!$       g(:,:,iglo) = g(:,:,iglo) * exp(vperp2(iglo)) / sqrt(pi)
!!$    end do
    ! <TT

    gnew = g

  end subroutine ginit_nl

  ! Orszag-Tang 2D vortex problem
  subroutine ginit_nl2
    use species, only: spec
    use theta_grid, only: ntgrid
    use kgrids, only: naky, nakx, reality, kperp2
    use dist_fn_arrays, only: g, gnew, vpa
    use agk_layouts, only: g_lo, ik_idx, it_idx, is_idx
    use constants, only: pi
    use fields_arrays, only: phinew, aparnew, bparnew
    use dist_fn, only: get_init_field
    implicit none
    integer :: iglo, ik, it, is, i
    real :: fac
    complex, dimension (-ntgrid:ntgrid,nakx,naky) :: phi, jpar !! local !!
    real, dimension (-ntgrid:ntgrid) :: dfac, ufac

    !! phi, jpar are local !!
    phi = 0.0 ; jpar = 0.0
!!$    phi(:,1,2) = phiinit * cmplx(2.0, 0.0)  ! 2 cos(y)
!!$    phi(:,2,1) = phiinit * cmplx(1.0, 0.0)  ! 2 cos(x)
!!$    jpar(:,1,2) = apar0 * cmplx(2.0, 0.0) ! 2 cos(y)
!!$    jpar(:,3,1) = apar0 * cmplx(2.0, 0.0) ! 4 cos(2x)
    do i=1, 6
       it = ittt(i)
       ik = ikkk(i)
       phi(:,it,ik) = phiamp(i)
       jpar(:,it,ik) = aparamp(i) * kperp2(it,ik)
    end do

! reality condition for ky = 0 component:
    if (reality) then
       do it = 1, nakx/2
          phi(:,it+(nakx+1)/2,1) = conjg(phi(:,(nakx+1)/2+1-it,1))
          jpar(:,it+(nakx+1)/2,1) = conjg(jpar(:,(nakx+1)/2+1-it,1))
       end do
    end if

    dfac     = den0
    ufac     = upar0

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       it = it_idx(g_lo,iglo)
       ik = ik_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)

       g(:,1,iglo) = &
            ( dfac*spec(is)%dens0                * phi(:,it,ik) &
            + 2.*ufac* vpa(1,iglo)*spec(is)%u0 * jpar(:,it,ik) )

       g(:,2,iglo) = &
            ( dfac*spec(is)%dens0                * phi(:,it,ik) &
            + 2.*ufac* vpa(2,iglo)*spec(is)%u0 * jpar(:,it,ik) )

    end do

    gnew = g

    ! normalize
    call get_init_field (phinew, aparnew, bparnew)
    do i=1, 6
       it = ittt(i)
       ik = ikkk(i)
       if (abs(phiamp(i)) > epsilon(0.0)) then
          !RN> complex/complex?
          fac = phiamp(i) / phinew(0,it,ik)
          phi(:,it,ik) = phi(:,it,ik) * fac
       end if
       if (abs(aparamp(i)) > epsilon(0.0)) then
          !RN> complex/complex?
          fac = aparamp(i) / aparnew(0,it,ik)
          jpar(:,it,ik) = jpar(:,it,ik) * fac
       end if
    end do

    ! redefine g
    if (reality) then
       do it = 1, nakx/2
          phi(:,it+(nakx+1)/2,1) = conjg(phi(:,(nakx+1)/2+1-it,1))
          jpar(:,it+(nakx+1)/2,1) = conjg(jpar(:,(nakx+1)/2+1-it,1))
       end do
    end if
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       it = it_idx(g_lo,iglo)
       ik = ik_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)

       g(:,1,iglo) = &
            ( dfac*spec(is)%dens0                * phi(:,it,ik) &
            + 2.*ufac* vpa(1,iglo)*spec(is)%u0 * jpar(:,it,ik) )

       g(:,2,iglo) = &
            ( dfac*spec(is)%dens0                * phi(:,it,ik) &
            + 2.*ufac* vpa(2,iglo)*spec(is)%u0 * jpar(:,it,ik) )

    end do

    gnew = g

  end subroutine ginit_nl2

  subroutine ginit_nl3r
    use species, only: spec, has_electron_species, electron_species
    use theta_grid, only: ntgrid, theta
    use kgrids, only: naky, nakx, reality
    use fields_arrays, only: apar
    use fields_arrays, only: aparnew
    use dist_fn_arrays, only: g, gnew, vpa, vperp2
    use agk_layouts, only: g_lo, ik_idx, it_idx, il_idx, is_idx
    use constants
    use ran
    implicit none
    complex, dimension (-ntgrid:ntgrid,nakx,naky) :: phi, odd
    real, dimension (-ntgrid:ntgrid) :: dfac, ufac, tparfac, tperpfac, ct, st, c2t, s2t
    integer :: iglo
    integer :: ig, ik, it, il, is, j
    
    phi = 0.
    odd = 0.
    do j = 1, 2
       ik = ikk(j)
       it = itt(j)
       do ig = -ntgrid, ntgrid
          phi(ig,it,ik) = cmplx(refac, imfac)
          apar(ig,it,ik) = apar0*cmplx(refac, imfac)
       end do
       if (chop_side) then
          if (left) then
             phi(:-1,it,ik) = 0.0
          else
             phi(1:,it,ik) = 0.0
          end if
       end if
    end do

    odd = zi * phi
    
! reality condition for k_theta = 0 component:
    if (reality) then
       do it = 1, nakx/2
          apar(:,it+(nakx+1)/2,1) = conjg(apar(:,(nakx+1)/2+1-it,1))
          phi(:,it+(nakx+1)/2,1) = conjg(phi(:,(nakx+1)/2+1-it,1))
          odd(:,it+(nakx+1)/2,1) = conjg(odd(:,(nakx+1)/2+1-it,1))
       enddo
    end if

    aparnew = apar

    if (even) then
       ct = cos(theta)
       st = sin(theta)

       c2t = cos(2.*theta)
       s2t = sin(2.*theta)
    else
       ct = sin(theta)
       st = cos(theta)

       c2t = sin(2.*theta)
       s2t = cos(2.*theta)
    end if

    dfac     = den0   + den1 * ct + den2 * c2t
    ufac     = upar0  + upar1* st + upar2* s2t
    tparfac  = tpar0  + tpar1* ct + tpar2* c2t
    tperpfac = tperp0 + tperp1*ct + tperp2*c2t


! charge dependence keeps initial Phi from being too small
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       g(:,1,iglo) = phiinit* &!spec(is)%z* &
            ( dfac*spec(is)%dens0                * phi(:,it,ik) &
            + 2.*ufac* vpa(1,iglo)*spec(is)%u0 * odd(:,it,ik) &
            + tparfac*(vpa(1,iglo)**2-0.5)     * phi(:,it,ik) &
            +tperpfac*(vperp2(iglo)-1.)        * phi(:,it,ik))

       g(:,2,iglo) = phiinit* &!spec(is)%z* &
            ( dfac*spec(is)%dens0                * phi(:,it,ik) &
            + 2.*ufac* vpa(2,iglo)*spec(is)%u0 * odd(:,it,ik) &
            + tparfac*(vpa(2,iglo)**2-0.5)     * phi(:,it,ik) &
            +tperpfac*(vperp2(iglo)-1.)        * phi(:,it,ik))

    end do

!    if (has_electron_species(spec)) then
!       call flae (g, gnew)
!       g = g - gnew
!    end if

    gnew = g
  end subroutine ginit_nl3r

  subroutine ginit_recon2
    use mp, only: proc0
    use species, only: spec, has_electron_species
    use theta_grid, only: ntgrid, theta
    use kgrids, only: naky, nakx, reality
    use agk_save, only: agk_restore
    use dist_fn_arrays, only: g, gnew, vpa, vperp2
    use fields_arrays, only: phi, apar, bpar
    use fields_arrays, only: phinew, aparnew, bparnew
    use agk_layouts, only: g_lo, ik_idx, it_idx, il_idx, is_idx
    use file_utils, only: error_unit
    use run_parameters, only: use_Phi, use_Apar, use_Bpar
    use constants
    use ran
    use agk_save, only: restart_file
    implicit none
    complex, dimension (-ntgrid:ntgrid,nakx,naky) :: phiz, odd
    real, dimension (-ntgrid:ntgrid) :: dfac, ufac, tparfac, tperpfac, ct, st, c2t, s2t
    integer :: iglo, istatus, ierr
    integer :: ig, ik, it, il, is, j
    logical :: many = .true.

    call agk_restore (g, scale, istatus, use_Phi, use_Apar, use_Bpar, many)
    if (istatus /= 0) then
       ierr = error_unit()
       if (proc0) write(ierr,*) "Error reading file: ", trim(restart_file)
       g = 0.
    end if

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       if (ik == 2) cycle

       g (:,1,iglo) = 0.
       g (:,2,iglo) = 0.
    end do
	
    phinew(:,:,1) = 0.
    aparnew(:,:,1) = 0.
    bparnew(:,:,1) = 0.
    phi(:,:,1) = 0.
    apar(:,:,1) = 0.
    bpar(:,:,1) = 0.

    phinew(:,:,3:naky) = 0.
    aparnew(:,:,3:naky) = 0.
    bparnew(:,:,3:naky) = 0.
    phi(:,:,3:naky) = 0.
    apar(:,:,3:naky) = 0.
    bpar(:,:,3:naky) = 0.

    phiz = 0.
    odd = 0.
    do j = 1, 2
       ik = ikk(j)
       it = itt(j)
       do ig = -ntgrid, ntgrid
          phiz(ig,it,ik) = cmplx(refac, imfac)
       end do
    end do

    odd = zi * phiz
    
! reality condition for k_theta = 0 component:
    if (reality) then
       do it = 1, nakx/2
          phiz(:,it+(nakx+1)/2,1) = conjg(phiz(:,(nakx+1)/2+1-it,1))
          odd (:,it+(nakx+1)/2,1) = conjg(odd (:,(nakx+1)/2+1-it,1))
       enddo
    end if

    if (even) then
       ct = cos(theta)
       st = sin(theta)

       c2t = cos(2.*theta)
       s2t = sin(2.*theta)
    else
       ct = sin(theta)
       st = cos(theta)

       c2t = sin(2.*theta)
       s2t = cos(2.*theta)
    end if

    dfac     = den0   + den1 * ct + den2 * c2t
    ufac     = upar0  + upar1* st + upar2* s2t
    tparfac  = tpar0  + tpar1* ct + tpar2* c2t
    tperpfac = tperp0 + tperp1*ct + tperp2*c2t


! charge dependence keeps initial Phi from being too small
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)       

       g(:,1,iglo) = phiinit* &!spec(is)%z* &
            ( dfac*spec(is)%dens0            * phiz(:,it,ik) &
            + 2.*ufac* vpa(1,iglo)         * odd (:,it,ik) &
            + tparfac*(vpa(1,iglo)**2-0.5) * phiz(:,it,ik) &
            +tperpfac*(vperp2(iglo)-1.)    * phiz(:,it,ik))

       g(:,2,iglo) = phiinit* &!spec(is)%z* &
            ( dfac*spec(is)%dens0            * phiz(:,it,ik) &
            + 2.*ufac* vpa(2,iglo)         * odd (:,it,ik) &
            + tparfac*(vpa(2,iglo)**2-0.5) * phiz(:,it,ik) &
            +tperpfac*(vperp2(iglo)-1.)    * phiz(:,it,ik))

    end do

!    if (has_electron_species(spec)) then
!       call flae (g, gnew)
!       g = g - gnew
!    end if

    gnew = g
  end subroutine ginit_recon2

  subroutine ginit_recon3
    use mp, only: proc0, broadcast
    use species, only: nspec, spec, has_electron_species
    use theta_grid, only: ntgrid, theta
    use kgrids, only: naky, nakx, akx, reality
    use kgrids, only: x0, y0, nx, ny, kperp2
    use kgrids, only: box
    use dist_fn_arrays, only: g, gnew, vpa, vperp2, g_eq
    use agk_layouts, only: g_lo, ik_idx, it_idx, is_idx
    use agk_transforms, only: inverse2,transform2
    use run_parameters, only: use_Phi, use_Apar, use_Bpar, fphi, fbpar
    use run_parameters, only: beta
    use le_grids, only: integrate_moment
    use dist_fn, only: getmoms_r
    use dist_fn, only: get_init_field, get_init_field_h
    use dist_fn, only: gamtot, gamtot1, gamtot2
    use dist_fn, only: g_adjust
    use dist_fn, only: is_g_gnew
    use run_parameters, only: store_eq
    use constants, only: pi, zi
    use file_utils, only: error_unit, stdout_unit, get_unused_unit
    use fields_arrays, only: phinew, aparnew, bparnew
    use fields_arrays, only: phi_eq, apar_eq, bpar_eq
    use dist_fn_arrays, only: dens_eq, ux_eq, uy_eq, uz_eq
    use dist_fn_arrays, only: pxx_eq, pyy_eq, pzz_eq, pxy_eq, pyz_eq, pzx_eq
    use dist_fn_arrays, only: eq_set
    use agk_equil_io, only: agk_save_eq
    use agk_mem, only: alloc8, dealloc8
    use ran, only: ranf, init_ranf, get_rnd_seed_length
    use linplus_terms, only: linplus_eq
    implicit none
    integer :: iglo
    integer :: ig, ik, it, is
    integer :: j

    ! nkxyz and ukxyz determine profiles in kx-ky plane
    ! nkxyz is for N, T, and ukxyz is for U
    ! [do not control amplitude by these variables]
    complex :: nkxyz(-ntgrid:ntgrid,nakx,naky)
    complex :: ukxyz(-ntgrid:ntgrid,nakx,naky)
    complex :: nkxyz_eq(-ntgrid:ntgrid,nakx,naky)
    complex :: ukxyz_eq(-ntgrid:ntgrid,nakx,naky)

    ! equilibrium and perturbation
    complex :: nkxy_eq(nakx,naky), ukxy_eq(nakx,naky)

    ! *fac determine z profile
    ! [do not control amplitude by these variables]
    real :: dfac(-ntgrid:ntgrid), ufac(-ntgrid:ntgrid)
    real :: tparfac(-ntgrid:ntgrid), tperpfac(-ntgrid:ntgrid)
    real :: ct(-ntgrid:ntgrid), st(-ntgrid:ntgrid)
    real :: c2t(-ntgrid:ntgrid), s2t(-ntgrid:ntgrid)

    ! normalization
    real, allocatable :: unrm(:,:), unrm_eq(:,:)

    real :: check(3)=0.,current=0.
    character (len=2) :: cfit='A0'
    complex, allocatable :: phi(:,:,:),apar(:,:,:),bpar(:,:,:)

    real :: save_dens0, save_tperp0, save_u0
    real :: ratio

    ! real space profile to be Fourier transformed
    real :: xx,dx,lx,ly
    integer, parameter :: nfxp=2**10
    integer :: nfx=nfxp
    real, allocatable :: nxy(:,:),uxy(:,:)

    real :: xmax, bymax, xa, xl1, xl2
    real :: ff
    real :: dominant_amp_n, dominant_amp_u
    real :: truncate_amp=1.e-8

    real :: bsq

    complex :: ukxyz_rnd(-ntgrid:ntgrid,nakx,naky)
    real, allocatable :: uxy_rnd(:,:)
    integer, allocatable :: seed(:)
    integer :: i,nseed
    real :: rms
    real :: spectrum, k_cutoff, klog_min, klog_max
    ! controls spectral form of initial noise
    real, parameter :: scutoff=0.5 ! cutoff range ratio in log(k)
    integer, parameter :: spow=8   ! cutoff range slope

    if(debug.and.proc0) write(stdout_unit,*) 'Initialization recon3'

    if(nfxp<nx) nfx=nx

!!! adjust input parameters to kill initial field if wanted
    if(debug.and.proc0) write(stdout_unit,*) 'check parameters'
    if(.not.use_Phi)  null_phi=.true.
    if(.not.use_Bpar) null_bpar=.true.
    if(.not.use_Apar) null_apar=.true.

    do is=1,nspec
       if(adj_spec == is) cycle
       check(1)=check(1)+spec(is)%z*spec(is)%dens*spec(is)%dens0
       check(2)=check(2)+spec(is)%dens*spec(is)%temp* &
            & (spec(is)%dens0+spec(is)%tperp0)
       check(3)=check(3)+spec(is)%z*spec(is)%dens*spec(is)%stm*spec(is)%u0
    end do

    if(adj_spec == 0) then
       ! just warn if fields don't satisfy given conditions
       if(null_phi.and.null_bpar) then
          if(sum(check(1:2)) /= 0.) then
             if(proc0) write(stdout_unit,'(a)') &
                  'WARNING: finite Phi or Bpar in initial condition'
          endif
       else if(null_bpar.and..not.null_phi) then
          ratio=gamtot1(eq_mode_n+1,1)/gamtot(eq_mode_n+1,1)
          if(check(1)/check(2) /= ratio) then
             if(proc0) write(stdout_unit,'(a)') &
                  'WARNING: finite Bpar in initial condition'
          endif
       else if(null_phi.and..not.null_bpar) then
          ratio=-(2.*gamtot2(eq_mode_n+1,1)+2./beta) &
               & /gamtot1(eq_mode_n+1,1)
          if(check(1)/check(2) /= ratio) then
             if(proc0) write(stdout_unit,'(a)') &
                  'WARNING: finite Bpar in initial condition'
          endif
       endif
       if(null_apar) then
          if(check(3) /= 0.) then
             if(proc0) write(stdout_unit,'(a)') &
                  'WARNING: finite Apar in initial condition'
          endif
       endif
    else
       ! adjust input parameter to satisfy given conditions
       if(null_phi.and.null_bpar) then
          save_dens0=spec(adj_spec)%dens0
          save_tperp0=spec(adj_spec)%tperp0
          spec(adj_spec)%dens0=-check(1)/(spec(adj_spec)%z*spec(adj_spec)%dens)
          spec(adj_spec)%tperp0=-spec(adj_spec)%dens0 &
               & -check(2)/(spec(adj_spec)%dens*spec(adj_spec)%temp)

          if(spec(adj_spec)%dens0 /= save_dens0) then
             if(proc0) write(stdout_unit,'(a,i0,a,f10.2,a)') &
                  & 'WARNING: Initial density of spec=', spec(adj_spec)%type, &
                  & ' is adjusted to ', spec(adj_spec)%dens0, &
                  & ' to kill Phi and Bpar'
          endif
          if(spec(adj_spec)%tperp0 /= save_tperp0) then
             if(proc0) write(stdout_unit,'(a,i0,a,f10.2,a)') &
                  & 'WARNING: Initial Tperp of spec=', spec(adj_spec)%type, &
                  & ' is adjusted to ', spec(adj_spec)%tperp0, &
                  & ' to kill Phi and Bpar'
          endif
       else if(null_bpar.and..not.null_phi.and.eq_type.eq.'sinusoidal') then
          save_tperp0=spec(adj_spec)%tperp0
          check(1)=check(1)+ &
               & spec(adj_spec)%z*spec(adj_spec)%dens*spec(adj_spec)%dens0
          ratio=gamtot1(eq_mode_n+1,1)/gamtot(eq_mode_n+1,1)

          spec(adj_spec)%tperp0=(-check(1)*ratio-check(2)) &
               & /(spec(adj_spec)%dens*spec(adj_spec)%temp) &
               & -spec(adj_spec)%dens0
          if(spec(adj_spec)%tperp0 /= save_tperp0) then
             if(proc0) write(stdout_unit,'(a,i0,a,f10.2,a)') &
                  & 'WARNING: Initial Tperp of spec=', spec(adj_spec)%type, &
                  & ' is adjusted to ', spec(adj_spec)%tperp0, &
                  & ' to kill Bpar'
          endif
       else if(null_phi.and..not.null_bpar.and.eq_type.eq.'sinusoidal') then
          save_tperp0=spec(adj_spec)%tperp0
          check(1)=check(1)+ &
               & spec(adj_spec)%z*spec(adj_spec)%dens*spec(adj_spec)%dens0
          ratio=-(2.*gamtot2(eq_mode_n+1,1)+2./beta) &
               & /gamtot1(eq_mode_n+1,1)

          spec(adj_spec)%tperp0=(-check(1)*ratio-check(2)) &
               & /(spec(adj_spec)%dens*spec(adj_spec)%temp) &
               & -spec(adj_spec)%dens0

          if(spec(adj_spec)%tperp0 /= save_tperp0) then
             if(proc0) write(stdout_unit,'(a,i0,a,f10.2,a)') &
                  & 'WARNING: Initial Tperp of spec=', spec(adj_spec)%type, &
                  & ' is adjusted to ', spec(adj_spec)%tperp0, &
                  & ' to kill Phi'
          endif
       endif
    
       if (null_apar) then
          save_u0=spec(adj_spec)%u0
          spec(adj_spec)%u0=-check(3)/ &
               & (spec(adj_spec)%z*spec(adj_spec)%dens*spec(adj_spec)%stm)
          if(spec(adj_spec)%u0 /= save_u0) then
             if(proc0) write(stdout_unit,'(a,i0,a,f10.2,a)') &
                  & 'WARNING: Initial U of spec=', spec(adj_spec)%type, &
                  & ' is adjusted to ', spec(adj_spec)%u0, &
                  & ' to kill Apar'
          endif
       endif
    endif

!!! initialize
    if(debug.and.proc0) write(stdout_unit,*) 'initialize variable'
    nkxyz(-ntgrid:ntgrid,1:nakx,1:naky)=cmplx(0.,0.)
    ukxyz(-ntgrid:ntgrid,1:nakx,1:naky)=cmplx(0.,0.)
    nkxyz_eq(-ntgrid:ntgrid,1:nakx,1:naky)=cmplx(0.,0.)
    ukxyz_eq(-ntgrid:ntgrid,1:nakx,1:naky)=cmplx(0.,0.)

    nkxy_eq(1:nakx,1:naky)=cmplx(0.,0.)
    ukxy_eq(1:nakx,1:naky)=cmplx(0.,0.)

    dfac(-ntgrid:ntgrid)=1.
    ufac(-ntgrid:ntgrid)=1.
    tparfac(-ntgrid:ntgrid)=1.
    tperpfac(-ntgrid:ntgrid)=1.

!!! equilibrium
    lx=2.*pi*x0; ly=2.*pi*y0

    if(phiinit0 /= 0.) then
       if(.not. box) then ! grid_option = single case
          nkxy_eq(1,1)=cmplx(.5,0.)
          ukxy_eq(1,1)=cmplx(.5,0.)
       else
          if(debug.and.proc0) write(stdout_unit,*) 'set equilibrium profile'
          allocate(nxy(nfx,ny)); call alloc8(r2=nxy,v='nxy')
          allocate(uxy(nfx,ny)); call alloc8(r2=uxy,v='uxy')
          dx=lx/nfx
          ! if width is negative, it gives the ratio to the box size
          if(prof_width < 0. ) prof_width=-prof_width*lx
          select case (eq_type)
          case ('sinusoidal')
             ! this gives n,Tpara,Tperp \propto cos^2 (2 pi/Lx)
             ! nxy(eq_mode1(1),eq_mode1(2))=cmplx(.25, 0.)
             ! this gives Apara \propto cos(2 pi/Lx), By \propto sin(2 pi x/Lx)
             ! uxy(eq_mode2(1),eq_mode2(2))=cmplx(.5, 0.)
             do it=1,nfx
                xx=dx*(it-1)
                nxy(it,1:ny)=0.
                uxy(it,1:ny)=-cos(2.*pi/lx*xx*eq_mode_u)
             end do
          case ('porcelli')
             do it=1,nfx
                xx=dx*(it-1)
                nxy(it,1:ny)=0.
                uxy(it,1:ny)=1./cosh((xx-.5*lx)/prof_width)**2 &
                     & * (tanh(2.*pi/lx*xx)**2+tanh(2.*pi/lx*(xx-lx))**2 &
                     & - tanh(2.*pi)**2) / (2.*tanh(pi)**2-tanh(2.*pi)**2)
             end do
          case ('doubleharris')
             do it=1,nfx
                ff=.5*(tanh(.25*lx/prof_width)+tanh(.75*lx/prof_width))
                xx=dx*(it-1)
                nxy(it,1:ny)=0.
                uxy(it,1:ny)= &
                     & log(cosh((xx-.25*lx)/prof_width)) -&
                     & log(cosh((xx-.75*lx)/prof_width)) &
                     & -ff/prof_width*xx
             end do
          case default
             if(proc0) write(error_unit(),'(2a)') &
                  & 'ERROR: Invalid equilibrium type', eq_type
             stop
          end select
          ! subtract (0,0) mode
          ! since it (constant part of potential) does nothing
          do ik=1,ny
             uxy(1:nfx,ik)=uxy(1:nfx,ik) &
                  - sum(uxy(1:nfx,ik))/nfx
          end do
          call inverse2(nxy, nkxy_eq, ny, nfx)
          call inverse2(uxy, ukxy_eq, ny, nfx)

          call dealloc8(r2=nxy,v='nxy')
          call dealloc8(r2=uxy,v='uxy')
       endif
    endif

    do ig = -ntgrid, ntgrid
       nkxyz(ig,1:nakx,1:naky) = phiinit0*nkxy_eq(1:nakx,1:naky)
       ukxyz(ig,1:nakx,1:naky) = phiinit0*ukxy_eq(1:nakx,1:naky)
    end do
    if(box) then
       nkxyz(-ntgrid:ntgrid,1,1)=cmplx(0.,0.)
       ukxyz(-ntgrid:ntgrid,1,1)=cmplx(0.,0.)
    endif

!!! save equilibrium profile
    nkxyz_eq(-ntgrid:ntgrid,1:nakx,1:naky)= &
         & nkxyz(-ntgrid:ntgrid,1:nakx,1:naky)
    ukxyz_eq(-ntgrid:ntgrid,1:nakx,1:naky)= &
         & ukxyz(-ntgrid:ntgrid,1:nakx,1:naky)

!!! perturbation
    if (linplus_eq) then
       ! clear equilibrium component for test 2015/1/19
       ukxyz=0.
       nkxyz=0.
    end if
    
    !!!
    if(phiinit /= 0.) then
       if(debug.and.proc0) write(stdout_unit,*) 'set perturbation profile'
       do j = 1, 6
          if(ikkk(j).eq.0) ukxy_pt(j)=.5*ukxy_pt(j) !reality
          if(ikkk(j).eq.0) nkxy_pt(j)=.5*nkxy_pt(j) !reality
          ik = ikkk(j)+1
          if(ittt(j) >= 0) then
             it = ittt(j)+1
          else
             it = nakx + ittt(j) + 1
          endif
          do ig = -ntgrid, ntgrid
             ukxyz(ig,it,ik) = ukxyz(ig,it,ik) + phiinit*ukxy_pt(j)
             nkxyz(ig,it,ik) = nkxyz(ig,it,ik) + phiinit*nkxy_pt(j)
          end do
       end do
    endif

    if(phiinit_rand /= 0.) then
       if(debug.and.proc0) write(stdout_unit,*) 'set random perturbation'

       if (proc0) then
          nseed=get_rnd_seed_length()
          allocate(seed(1:nseed))
          if (phiinit_rand_seed == 0) then
             call init_ranf(.true.,seed)
          else
             do i=1,nseed
                seed(i)=phiinit_rand_seed+i-1
             end do
             call init_ranf(.false.,seed)
          end if
          deallocate(seed)
          
          klog_min=log(sqrt(minval(kperp2,kperp2 /= 0.)))
          klog_max=log(sqrt(maxval(kperp2)))
          k_cutoff=exp((klog_max-klog_min)*scutoff+klog_min)

          if (.not. allocated(uxy_rnd)) allocate(uxy_rnd(nx,ny)); call alloc8(r2=uxy_rnd,v='uxy_rnd')

          ukxyz_rnd=0.
          do ig = -ntgrid, ntgrid
             do it = 1, nakx
                do ik = 1, naky
                   if (kperp2(it,ik) == 0.) cycle
                   spectrum=k_cutoff**spow/sqrt(k_cutoff**(2*spow)+kperp2(it,ik)**spow)/kperp2(it,ik)
                   ukxyz_rnd(ig,it,ik)=spectrum*exp(zi*2.*pi*ranf())
                end do
             end do

             ! normalize
             call transform2(ukxyz_rnd(ig,:,:),uxy_rnd,ny,nx)
             rms = sqrt(sum(uxy_rnd**2)/(nx*ny))
             ukxyz_rnd(ig,:,:)=phiinit_rand/rms*ukxyz_rnd(ig,:,:)
             ! 0.5 is to take into account -k modes
             ukxyz_rnd(ig,:,1)=0.5*ukxyz_rnd(ig,:,1)
          end do
       
          if (allocated(uxy_rnd)) call dealloc8(r2=uxy_rnd,v='uxy_rnd')

          ! 2d
          if (ntgrid == 1) then 
             do ig = -ntgrid, ntgrid
                ukxyz_rnd(ig,:,:) = ukxyz_rnd(0,:,:)
             end do
          end if
       end if

       call broadcast(ukxyz_rnd)
       
       ! add random perturbation
       ukxyz = ukxyz + ukxyz_rnd
    end if

!!! reality condition for k_theta = 0 component:
    if(debug.and.proc0) write(stdout_unit,*) 'set reality condition'
    if (reality) then
       do it = 1, nakx/2
          nkxyz(-ntgrid:ntgrid,it+(nakx+1)/2,1) = &
               & conjg(nkxyz(-ntgrid:ntgrid,(nakx+1)/2+1-it,1))
          ukxyz(-ntgrid:ntgrid,it+(nakx+1)/2,1) = &
               & conjg(ukxyz(-ntgrid:ntgrid,(nakx+1)/2+1-it,1))
          nkxyz_eq(-ntgrid:ntgrid,it+(nakx+1)/2,1) = &
               & conjg(nkxyz_eq(-ntgrid:ntgrid,(nakx+1)/2+1-it,1))
          ukxyz_eq(-ntgrid:ntgrid,it+(nakx+1)/2,1) = &
               & conjg(ukxyz_eq(-ntgrid:ntgrid,(nakx+1)/2+1-it,1))
       enddo
    end if

!!! parallel profile
    if(debug.and.proc0) write(stdout_unit,*) 'set parallel profile'
    if (even) then
       ct = cos(theta)
       st = sin(theta)

       c2t = cos(2.*theta)
       s2t = sin(2.*theta)
    else
       ct = sin(theta)
       st = cos(theta)

       c2t = sin(2.*theta)
       s2t = cos(2.*theta)
    end if

    dfac     = dfac     + den1   * ct + den2   * c2t
    ufac     = ufac     + upar1  * st + upar2  * s2t
    tparfac  = tparfac  + tpar1  * ct + tpar2  * c2t
    tperpfac = tperpfac + tperp1 * ct + tperp2 * c2t

!!! normalization
    if(debug.and.proc0) write(stdout_unit,*) 'normalization'
    bymax=1.
    if (box) then
       if(eq_type == 'porcelli') then
          xmax=.5*prof_width*log(2.+sqrt(3.))+.5*lx
          xmax=get_xmax(xmax)
          xa=(xmax-.5*lx)/prof_width
          xl1=2.*pi/lx*xmax; xl2=xl1-2.*pi
          bymax=(-2./prof_width*sinh(xa)/cosh(xa)**3* &
               & (tanh(xl1)**2+tanh(xl2)**2-tanh(2.*pi)**2) &
               & + 1./cosh(xa)**2*4.*pi/lx* &
               & (sinh(xl1)/cosh(xl1)**3+sinh(xl2)/cosh(xl2)**3) ) &
               & / (2.*tanh(pi)**2-tanh(2.*pi)**2)
       else if(eq_type == 'doubleharris') then
          bymax=1./prof_width
       else if(eq_type == 'sinusoidal') then
          bymax=akx(eq_mode_u+1)
       endif
    else
       bymax=sqrt(kperp2(1,1))
    endif

    ! By0 = hat{By}*(B0/2*rho_0/a0)
    ! hat{By} = d/d(hat{x}) hat{A}
    if(b0 /= 0.) then
       a0 = b0/abs(bymax)
       cfit='B0'
    endif

    allocate(unrm(nakx,naky)); call alloc8(r2=unrm,v='unrm'); unrm=1.
    allocate(unrm_eq(nakx,naky)); call alloc8(r2=unrm_eq,v='unrm_eq'); unrm_eq=1.

    !       if(a0 /= 0. .and. .not. linplus) then
    if(a0 /= 0.) then
       if(proc0) write(stdout_unit,*) 'INFO: rescale parallel momentum to set a given Apar amplitude'
       current=sum(spec(1:nspec)%z*spec(1:nspec)%dens &
            & *spec(1:nspec)%stm*spec(1:nspec)%u0)
       if(current==0.) then
          if(proc0) write(error_unit(),'(a)') &
               & 'ERROR in init_g: invalid input a0, u0'
          stop
       endif
       where(cabs(ukxyz(0,:,:)) /= 0.)
          unrm(:,:)=2.*beta*current/kperp2(:,:)/a0
       endwhere
       where(cabs(ukxyz_eq(0,:,:)) /= 0.)
          unrm_eq(:,:)=2.*beta*current/kperp2(:,:)/a0
       endwhere
    end if

    if(debug.and.proc0) write(stdout_unit,*) 'calculate g'
    g=cmplx(0.,0.)
    g_eq=cmplx(0.,0.)
    dominant_amp_n=maxval(cabs(nkxyz))
    dominant_amp_u=maxval(cabs(ukxyz))
    if(dominant_amp_n == 0.) dominant_amp_n=1.
    if(dominant_amp_u == 0.) dominant_amp_u=1.
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       bsq=.25*spec(is)%smz**2*kperp2(it,ik)
       ! truncate modes if amplitude is much smaller than the dominant mode
       ! this is necessary for nnrm << 1 where (k_perp rho) huge
       ! If divergence happens for, eg, high temperature case,
       ! truncate_amp should be larger, but make sure result does not change
       where( &
            cabs(nkxyz(:,it,ik))/dominant_amp_n > truncate_amp &
            .or. &
            cabs(ukxyz(:,it,ik))/dominant_amp_u > truncate_amp )

          g(:,1,iglo) = exp(bsq) * ( &
               & ( dfac*spec(is)%dens0 &
               & + tparfac*spec(is)%tpar0 * (vpa(1,iglo)**2-.5) &
               & + tperpfac*spec(is)%tperp0 * (vperp2(iglo)-1.+bsq) &
               & ) * nkxyz(:,it,ik) &
               & + ufac*2.*vpa(1,iglo)*spec(is)%u0 / unrm(it,ik) &
               & * ukxyz(:,it,ik) &
               )
          g(:,2,iglo) = exp(bsq) * ( &
               & ( dfac*spec(is)%dens0 &
               & + tparfac*spec(is)%tpar0 * (vpa(2,iglo)**2-.5) &
               & + tperpfac*spec(is)%tperp0 * (vperp2(iglo)-1.+bsq) &
               & ) * nkxyz(:,it,ik) &
               & + ufac*2.*vpa(2,iglo)*spec(is)%u0 / unrm(it,ik) &
               & * ukxyz(:,it,ik) &
               )

       end where
    
       where( &
            cabs(nkxyz_eq(:,it,ik))/dominant_amp_n > truncate_amp &
            .or. &
            cabs(ukxyz_eq(:,it,ik))/dominant_amp_u > truncate_amp )
          g_eq(:,1,iglo) = exp(bsq) * ( &
               & ( dfac*spec(is)%dens0 &
               & + tparfac*spec(is)%tpar0 * (vpa(1,iglo)**2-.5) &
               & + tperpfac*spec(is)%tperp0 * (vperp2(iglo)-1.+bsq) &
               & ) * nkxyz_eq(:,it,ik) &
               & + ufac*2.*vpa(1,iglo)*spec(is)%u0 / unrm_eq(it,ik) &
               & * ukxyz_eq(:,it,ik) &
               )
          g_eq(:,2,iglo) = exp(bsq) * ( &
               & ( dfac*spec(is)%dens0 &
               & + tparfac*spec(is)%tpar0 * (vpa(2,iglo)**2-.5) &
               & + tperpfac*spec(is)%tperp0 * (vperp2(iglo)-1.+bsq) &
               & ) * nkxyz_eq(:,it,ik) &
               & + ufac*2.*vpa(2,iglo)*spec(is)%u0 / unrm_eq(it,ik) &
               & * ukxyz_eq(:,it,ik) &
               )
       end where
    end do
    call dealloc8(r2=unrm,v='unrm')
    call dealloc8(r2=unrm_eq,v='unrm_eq')

    allocate(phi(-ntgrid:ntgrid,1:nakx,1:naky)); call alloc8(c3=phi,v='phi')
    allocate(apar(-ntgrid:ntgrid,1:nakx,1:naky)); call alloc8(c3=apar,v='apr')
    allocate(bpar(-ntgrid:ntgrid,1:nakx,1:naky)); call alloc8(c3=bpar,v='bpar')
    phi(:,:,:)=cmplx(0.,0.)
    apar(:,:,:)=cmplx(0.,0.)
    bpar(:,:,:)=cmplx(0.,0.)

    if (store_eq .and. linplus_eq) then
       gnew=0.
       phi_eq=0.; apar_eq=0.; bpar_eq=0.
       dens_eq=0.;
       ux_eq=0.; uy_eq=0.; uz_eq=0.
       pxx_eq=0.; pyy_eq=0.; pzz_eq=0.
       pxy_eq=0.; pyz_eq=0.; pzx_eq=0.
       if(debug.and.proc0) write(stdout_unit,*) 'save equilibrium profile'
       ! agk_save_eq
       call agk_save_eq (gnew, use_Phi, use_Apar, use_Bpar)
    end if
    
    ! get equilibrium fields
    gnew(:,:,:)=g_eq(:,:,:)
    call get_init_field_h(phi,apar,bpar)
    ! currently, gnew is actually h_equilibrium
    call g_adjust(gnew,phi,bpar,-fphi,-fbpar,is_g_gnew)
    ! now, gnew becomes really g_equilibrium
    phinew=phi; aparnew=apar; bparnew=bpar ! this is necessary when calling getmoms_r

    g_eq = gnew
    phi_eq = phi; apar_eq = apar; bpar_eq = bpar
    call getmoms_r(dens_eq, ux_eq, uy_eq, uz_eq, pxx_eq, pyy_eq, pzz_eq, &
         & pxy_eq, pyz_eq, pzx_eq)

    eq_set = .true.
    
!!! store equilibrium fields
    if (store_eq .and. .not.linplus_eq) then
       if(debug.and.proc0) write(stdout_unit,*) 'save equilibrium profile'
       ! agk_save_eq
       call agk_save_eq (g_eq, use_Phi, use_Apar, use_Bpar)
    end if

    if(debug.and.proc0) write(stdout_unit,*) 'generate g and fields'
    ! now store the actual g in gnew
    gnew(:,:,:) = g(:,:,:)
    call get_init_field_h(phi,apar,bpar)
    ! currently, gnew is actually h
    is_g_gnew = .false.
    call g_adjust(gnew,phi,bpar,-fphi,-fbpar,is_g_gnew)
    ! now, gnew becomes really g
    phinew=phi; aparnew=apar; bparnew=bpar ! this is necessary when 
                                           ! calling getmoms_r

    call dealloc8(c3=phi,v='phi')
    call dealloc8(c3=apar,v='apar')
    call dealloc8(c3=bpar,v='bpar')
    
    if(debug.and.proc0) write(stdout_unit,*) 'init recon3 done'
    
  contains
    function get_xmax(xguess) result(xsol)
      real :: xsol
      real, intent(in) :: xguess
      real, parameter :: tol=1.e-20
      real :: xold
      integer :: i
      integer :: itmax=1000
      xold=xguess
      do i=1,itmax
         xsol=xold-f(xold)/fprime(xold)
         if(abs(xsol-xold) > tol) then
            xold=xsol
         else
            return
         endif
      end do
    end function get_xmax

    function f(x)
      real :: f
      real, intent(in) :: x
      real :: xa, xl1, xl2
      xa=(x-.5*lx)/prof_width
      xl1=2.*pi/lx*x
      xl2=xl1-2.*pi

      f= &
           & 2./prof_width**2*(cosh(2.*xa)-2.)/cosh(xa)**4* & ! f''
           & (tanh(xl1)**2+tanh(xl2)**2-tanh(2.*pi)**2) & ! g
           & + 2. * &
           & (-2./prof_width)*sinh(xa)/cosh(xa)**3* & ! f'
           & 4.*pi/lx*(sinh(xl1)/cosh(xl1)**3+sinh(xl2)/cosh(xl2)**3) & ! g'
           & + &
           & 1./cosh(xa)**2* & ! f
           & 8.*(pi/lx)**2*((2.-cosh(2.*xl1))/cosh(xl1)**4 & ! g''
           &               +(2.-cosh(2.*xl2))/cosh(xl2)**4)

    end function f

    function fprime(x)
      real :: fprime
      real, intent(in) :: x
      real :: xa, xl1, xl2
      xa=(x-.5*lx)/prof_width
      xl1=2.*pi/lx*x
      xl2=xl1-2.*pi

      fprime = &
           & 8./prof_width**3*(2.*sinh(xa)-sinh(xa)**3)/cosh(xa)**5* & ! f''' 
           & (tanh(xl1)**2+tanh(xl2)**2-tanh(2.*pi)**2) & ! g
           & + 3. * &
           & 2./prof_width**2*(cosh(2.*xa)-2.)/cosh(xa)**4* & ! f''
           & 4.*pi/lx*(sinh(xl1)/cosh(xl1)**3+sinh(xl2)/cosh(xl2)**3) & ! g'
           & + 3. * &
           & (-2./prof_width)*sinh(xa)/cosh(xa)**3* & ! f'
           & 8.*(pi/lx)**2*((2.-cosh(2.*xl1))/cosh(xl1)**4 & ! g''
           &               +(2.-cosh(2.*xl2))/cosh(xl2)**4) &
           & + &
           & 1./cosh(xa)**2* & ! f
           & (-64.)*(pi/lx)**3 * ( & ! g'''
           & (2.*sinh(xl1)-sinh(xl1)**3)/cosh(xl1)**5 + &
           & (2.*sinh(xl2)-sinh(xl2)**3)/cosh(xl2)**5 )
    end function fprime

  end subroutine ginit_recon3

  subroutine ginit_recon
    use mp, only: proc0
    use species, only: spec, has_electron_species
    use theta_grid, only: ntgrid, theta
    use kgrids, only: naky, nakx, reality
    use agk_save, only: agk_restore, restart_file
    use dist_fn_arrays, only: g, gnew, vpa, vperp2
    use fields_arrays, only: phi, apar, bpar
    use fields_arrays, only: phinew, aparnew, bparnew
    use agk_layouts, only: g_lo, ik_idx, it_idx, il_idx, is_idx
    use file_utils, only: error_unit
    use run_parameters, only: use_Phi, use_Apar, use_Bpar
    use constants
    use ran
    implicit none
    complex, dimension (-ntgrid:ntgrid,nakx,naky) :: phiz, odd
    real, dimension (-ntgrid:ntgrid) :: dfac, ufac, tparfac, tperpfac, ct, st, c2t, s2t
    real :: kfac
    integer :: iglo, istatus, ierr
    integer :: ig, ik, it, il, is, j
    logical :: many = .true.
    
!
! hard-wiring some changes for a test run.  7/13/05
!

    call agk_restore (g, scale, istatus, use_Phi, use_Apar, use_Bpar, many)
    if (istatus /= 0) then
       ierr = error_unit()
       if (proc0) write(ierr,*) "Error reading file: ", trim(restart_file)
       g = 0.
    end if

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       if (ik == 1) cycle

       g (:,1,iglo) = 0.
       g (:,2,iglo) = 0.
    end do
	
    phinew(:,:,2:naky) = 0.
    aparnew(:,:,2:naky) = 0.
    bparnew(:,:,2:naky) = 0.
    phi(:,:,2:naky) = 0.
    apar(:,:,2:naky) = 0.
    bpar(:,:,2:naky) = 0.

    phiz = 0.
    odd = 0.
    kfac = 1.
    do j = 1, 3
       ik = ikkk(j)
       it = ittt(j)
       if (j == 2) kfac =  0.5
       if (j == 3) kfac = -0.5
       do ig = -ntgrid, ntgrid
          phiz(ig,it,ik) = cmplx(refac, imfac)*kfac
       end do
    end do

    odd = zi * phiz
    
! reality condition for k_theta = 0 component:
    if (reality) then
       do it = 1, nakx/2
          phiz(:,it+(nakx+1)/2,1) = conjg(phiz(:,(nakx+1)/2+1-it,1))
          odd (:,it+(nakx+1)/2,1) = conjg(odd (:,(nakx+1)/2+1-it,1))
       enddo
    end if

    if (even) then
       ct = cos(theta)
       st = sin(theta)

       c2t = cos(2.*theta)
       s2t = sin(2.*theta)
    else
       ct = sin(theta)
       st = cos(theta)

       c2t = sin(2.*theta)
       s2t = cos(2.*theta)
    end if

    dfac     = den0   + den1 * ct + den2 * c2t
    ufac     = upar0  + upar1* st + upar2* s2t
    tparfac  = tpar0  + tpar1* ct + tpar2* c2t
    tperpfac = tperp0 + tperp1*ct + tperp2*c2t


! charge dependence keeps initial Phi from being too small
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)       

       g(:,1,iglo) = phiinit* &!spec(is)%z* &
            ( dfac*spec(is)%dens0            * phiz(:,it,ik) &
            + 2.*ufac* vpa(1,iglo)         * odd (:,it,ik) &
            + tparfac*(vpa(1,iglo)**2-0.5) * phiz(:,it,ik) &
            +tperpfac*(vperp2(iglo)-1.)    * phiz(:,it,ik))

       g(:,2,iglo) = phiinit* &!spec(is)%z* &
            ( dfac*spec(is)%dens0            * phiz(:,it,ik) &
            + 2.*ufac* vpa(2,iglo)         * odd (:,it,ik) &
            + tparfac*(vpa(2,iglo)**2-0.5) * phiz(:,it,ik) &
            +tperpfac*(vperp2(iglo)-1.)    * phiz(:,it,ik))

    end do

!    if (has_electron_species(spec)) then
!       call flae (g, gnew)
!       g = g - gnew
!    end if

    gnew = g
  end subroutine ginit_recon

! TT> imported from GS2 for recon run
! nl4 has been monkeyed with over time.  Do not expect to recover old results
! that use this startup routine.
  subroutine ginit_nl4
    use mp, only: proc0
    use species, only: spec
    use agk_save, only: agk_restore, restart_file
    use theta_grid, only: ntgrid
    use kgrids, only: naky, nakx
    use dist_fn_arrays, only: g, gnew
    use fields_arrays, only: phi, apar, bpar
    use fields_arrays, only: phinew, aparnew, bparnew
    use agk_layouts, only: g_lo, ik_idx, it_idx, is_idx, il_idx
    use file_utils, only: error_unit
    use run_parameters, only: use_Phi, use_Apar, use_Bpar
    use ran, only: ranf
    implicit none
    complex, dimension (-ntgrid:ntgrid,nakx,naky) :: phiz
    integer :: iglo, istatus
    integer :: ig, ik, it, is, il, ierr
    logical :: many = .true.
    
    call agk_restore (g, scale, istatus, use_Phi, use_Apar, use_Bpar, many)
    if (istatus /= 0) then
       ierr = error_unit()
       if (proc0) write(ierr,*) "Error reading file: ", trim(restart_file)
       g = 0.
    end if

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       it = it_idx(g_lo,iglo)
       ik = ik_idx(g_lo,iglo)
!       if (ik == 1) cycle
       if ((it == 2 .or. it == nakx) .and. ik == 1) cycle

       g (:,1,iglo) = 0.
       g (:,2,iglo) = 0.
    end do

    do ik = 1, naky
       do it=1, nakx
          if ((it == 2 .or. it == nakx) .and. ik == 1) cycle
          phinew(:,it,ik) = 0.
          aparnew(:,it,ik) = 0.
          bparnew(:,it,ik) = 0.
          phi(:,it,ik) = 0.
          apar(:,it,ik) = 0.
          bpar(:,it,ik) = 0.          
       end do
    end do
    
    do ik = 1, naky
       do it = 1, nakx
          do ig = -ntgrid, ntgrid
             phiz(ig,it,ik) = cmplx(ranf()-0.5,ranf()-0.5)
          end do
          if (chop_side) then
             if (left) then
                phiz(:-1,it,ik) = 0.0
             else
                phiz(1:,it,ik) = 0.0
             end if
          end if
       end do
    end do

! reality condition for k_theta = 0 component:
    do it = 1, nakx/2
       phiz(:,it+(nakx+1)/2,1) = conjg(phiz(:,(nakx+1)/2+1-it,1))
    end do

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       g(:,1,iglo) = g(:,1,iglo)-phiz(:,it,ik)*spec(is)%z*phiinit
       g(:,2,iglo) = g(:,2,iglo)-phiz(:,it,ik)*spec(is)%z*phiinit
    end do
    gnew = g

  end subroutine ginit_nl4

  subroutine ginit_kpar
    use species, only: spec, has_electron_species
    use theta_grid, only: ntgrid, theta
    use kgrids, only: naky, nakx
    use dist_fn_arrays, only: g, gnew, vpa, vperp2
    use agk_layouts, only: g_lo, ik_idx, it_idx!, is_idx
    use constants
    use ran
    implicit none
    complex, dimension (-ntgrid:ntgrid,nakx,naky) :: phi, odd
    real, dimension (-ntgrid:ntgrid) :: dfac, ufac, tparfac, tperpfac
    integer :: iglo
    integer :: ik, it

    phi = cmplx(refac, imfac)
    if (chop_side) then
       if (left) then
          phi(:-1,:,:) = 0.0
       else
          phi(1:,:,:) = 0.0
       end if
    end if

    odd = zi * phi
        
    dfac     = den0   + den1 * cos(theta) + den2 * cos(2.*theta) 
    ufac     = upar0  + upar1* sin(theta) + upar2* sin(2.*theta) 
    tparfac  = tpar0  + tpar1* cos(theta) + tpar2* cos(2.*theta) 
    tperpfac = tperp0 + tperp1*cos(theta) + tperp2*cos(2.*theta) 

! charge dependence keeps initial Phi from being too small
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
!       is = is_idx(g_lo,iglo)       

       g(:,1,iglo) = phiinit* &!spec(is)%z* &
            ( dfac                           * phi(:,it,ik) &
            + 2.*ufac* vpa(1,iglo)         * odd(:,it,ik) &
            + tparfac*(vpa(1,iglo)**2-0.5) * phi(:,it,ik) &
            +tperpfac*(vperp2(iglo)-1.)    * phi(:,it,ik))

       g(:,2,iglo) = phiinit* &!spec(is)%z* &
            ( dfac                           * phi(:,it,ik) &
            + 2.*ufac* vpa(2,iglo)         * odd(:,it,ik) &
            + tparfac*(vpa(2,iglo)**2-0.5) * phi(:,it,ik) &
            +tperpfac*(vperp2(iglo)-1.)    * phi(:,it,ik))

    end do

    if (has_electron_species(spec)) then
       call flae (g, gnew)
       g = g - gnew
    end if

    gnew = g
  end subroutine ginit_kpar

  subroutine ginit_gs
    use species, only: spec, has_electron_species
    use theta_grid, only: ntgrid, theta
    use kgrids, only: naky, nakx
    use dist_fn_arrays, only: g, gnew, vpa, vperp2
    use agk_layouts, only: g_lo, ik_idx, it_idx, il_idx, is_idx
    use constants
    use ran
    implicit none
    complex, dimension (-ntgrid:ntgrid,nakx,naky) :: phi, odd
    integer :: iglo
    integer :: ik, it, il, is
    real :: phase
    
    phi = 0.
    odd = 0.
    do ik=1,naky
       do it=1,nakx
          phase = 2.*pi*ranf()
          phi(:,it,ik) = cos(theta+phase)*cmplx(refac,imfac)
          odd(:,it,ik) = sin(theta+phase)*cmplx(refac,imfac) * zi
       end do
    end do
    
! reality condition for k_theta = 0 component:
    do it = 1, nakx/2
       phi(:,it+(nakx+1)/2,1) = conjg(phi(:,(nakx+1)/2+1-it,1))
       odd(:,it+(nakx+1)/2,1) = conjg(odd(:,(nakx+1)/2+1-it,1))
    enddo

! charge dependence keeps initial Phi from being too small
    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)       

       g(:,1,iglo) = phiinit* &!spec(is)%z* &
            ( den1                         * phi(:,it,ik) &
            + 2.*upar1* vpa(1,iglo)      * odd(:,it,ik) &
            + tpar1*(vpa(1,iglo)**2-0.5) * phi(:,it,ik) &
            + tperp1*(vperp2(iglo)-1.)   * phi(:,it,ik))
       
       g(:,2,iglo) = phiinit* &!spec(is)%z* &
            ( den1                         * phi(:,it,ik) &
            + 2.*upar1* vpa(2,iglo)      * odd(:,it,ik) &
            + tpar1*(vpa(2,iglo)**2-0.5) * phi(:,it,ik) &
            + tperp1*(vperp2(iglo)-1.)   * phi(:,it,ik))
    end do

    if (has_electron_species(spec)) then
       call flae (g, gnew)
       g = g - gnew
    end if

    gnew = g
  end subroutine ginit_gs

  subroutine ginit_alf
    use theta_grid, only: theta
    use dist_fn_arrays, only: g, gnew, vpa
    use agk_layouts, only: g_lo, il_idx, is_idx
    use species, only: spec, electron_species

    implicit none
    integer :: iglo
    integer :: il, is

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       is = is_idx(g_lo,iglo)
       if (spec(is_idx(g_lo, iglo))%type == electron_species) cycle
       il = il_idx(g_lo,iglo)
       g(:,1,iglo) = sin(theta)*vpa(1,iglo)*spec(is)%z
       g(:,2,iglo) = sin(theta)*vpa(2,iglo)*spec(is)%z
    end do
    g = phiinit * g 
    gnew = g
  end subroutine ginit_alf

  subroutine ginit_zero
    use dist_fn_arrays, only: g, gnew
    implicit none
    g = 0.0
    gnew = 0.0
  end subroutine ginit_zero

  subroutine ginit_restart_many
    use dist_fn_arrays, only: g, gnew
    use dist_fn, only: is_g_gnew
    use agk_save, only: agk_restore, restart_file
    use mp, only: proc0
    use file_utils, only: error_unit
    use run_parameters, only: use_Phi, use_Apar, use_Bpar
    use run_parameters, only: store_eq
    use dist_fn_arrays, only: g_eq
    use agk_equil_io, only: agk_restore_eq

    implicit none
    integer :: istatus, ierr
    logical :: many = .true.

    call agk_restore (g, scale, istatus, use_Phi, use_Apar, use_Bpar, many)

    if (istatus /= 0) then
       ierr = error_unit()
       if (proc0) write(ierr,*) "Error reading file: ", trim(restart_file)
       g = 0.
    end if
    gnew = g
    is_g_gnew=.true.

    if (store_eq) call agk_restore_eq(g_eq, use_Phi, use_Apar, use_Bpar)

  end subroutine ginit_restart_many

  subroutine ginit_restart_small
    use dist_fn_arrays, only: g, gnew
    use agk_save, only: agk_restore, restart_file
    use mp, only: proc0
    use file_utils, only: error_unit
    use run_parameters, only: use_Phi, use_Apar, use_Bpar
    implicit none
    integer :: istatus, ierr
    logical :: many = .true.

    call ginit_noise

    call agk_restore (g, scale, istatus, use_Phi, use_Apar, use_Bpar, many)
    if (istatus /= 0) then
       ierr = error_unit()
       if (proc0) write(ierr,*) "Error reading file: ", trim(restart_file)
       g = 0.
    end if
    g = g + gnew
    gnew = g 

  end subroutine ginit_restart_small

  subroutine ginit_restart_smallflat

    use agk_save, only: agk_restore, restart_file
    use mp, only: proc0
    use file_utils, only: error_unit
    use species, only: spec
    use theta_grid, only: ntgrid 
    use kgrids, only: naky, nakx, aky, reality
    use dist_fn_arrays, only: g, gnew
    use agk_layouts, only: g_lo, ik_idx, it_idx, il_idx, is_idx
    use run_parameters, only: use_Phi, use_Apar, use_Bpar
    use ran

    implicit none
    complex, dimension (-ntgrid:ntgrid,nakx,naky) :: phi
    integer :: istatus, ierr
    logical :: many = .true.
    real :: a, b
    integer :: iglo
    integer :: ik, it, il, is

    do it = 1, nakx
       do ik = 1, naky
          a = ranf()-0.5
          b = ranf()-0.5
          phi(:,it,ik) = cmplx(a,b)
       end do
    end do

    if (naky > 1 .and. aky(1) == 0.0) then
       phi(:,:,1) = phi(:,:,1)*zf_init
    end if
! reality condition for k_theta = 0 component:
    if (reality) then
       do it = 1, nakx/2
          phi(:,it+(nakx+1)/2,1) = conjg(phi(:,(nakx+1)/2+1-it,1))
       enddo
    end if

    do iglo = g_lo%llim_proc, g_lo%ulim_proc
       ik = ik_idx(g_lo,iglo)
       it = it_idx(g_lo,iglo)
       il = il_idx(g_lo,iglo)
       is = is_idx(g_lo,iglo)
       g(:,1,iglo) = -phi(:,it,ik)*spec(is)%z*phiinit
       g(:,2,iglo) = g(:,1,iglo)
    end do
    gnew = g

    call agk_restore (g, scale, istatus, use_Phi, use_Apar, use_Bpar, many)
    if (istatus /= 0) then
       ierr = error_unit()
       if (proc0) write(ierr,*) "Error reading file: ", trim(restart_file)
       g = 0.
    end if
    g = g + gnew
    gnew = g 

  end subroutine ginit_restart_smallflat

  subroutine ginit_gk_eigen
    use gk_eigen, only: gk_eigen_ginit
    call gk_eigen_ginit
  end subroutine ginit_gk_eigen

  subroutine reset_init

    ginitopt_switch = ginitopt_restart_many

  end subroutine reset_init

  subroutine flae (g, gavg)
    use species, only: spec, electron_species 
    use theta_grid, only: ntgrid, delthet, jacob
    use kgrids, only: aky
    use agk_layouts, only: g_lo, is_idx, ik_idx
    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (in) :: g
    complex, dimension (-ntgrid:,:,g_lo%llim_proc:), intent (out) :: gavg

    real :: wgt
    integer :: iglo
    
    gavg = 0.
    wgt = 1./(delthet*jacob)

    do iglo = g_lo%llim_proc, g_lo%ulim_proc       
       if (spec(is_idx(g_lo, iglo))%type /= electron_species) cycle
       if (aky(ik_idx(g_lo, iglo)) /= 0.) cycle
       gavg(:,1,iglo) = sum(g(:,1,iglo)*delthet*jacob)*wgt
       gavg(:,2,iglo) = sum(g(:,2,iglo)*delthet*jacob)*wgt
    end do

  end subroutine flae

end module init_g
