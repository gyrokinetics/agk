! TT: for variable step AB3 I made changes
!  dtime -> dtime(1)
! everywhere (3/28/10)

module agk_reinit
  implicit none

  private
  public :: reset_time_step
  public :: check_time_step
  public :: time_reinit

  real :: delt_adj, dt0
  logical :: abort_rapid_time_step_change
  real :: delt_cushion = 1.5
  real :: delt_minimum
  real, save :: time_reinit(2)=0.

contains

  subroutine reset_time_step (istep, exit)

    use collisions, only: c_reset => reset_init
    use dist_fn, only: d_reset => reset_init
    use fields, only: f_reset => reset_init, init_fields
    use fields_implicit, only: fi_reset => reset_init
    use init_g, only: g_reset => reset_init
    use run_parameters, only: use_Phi, use_Apar, use_Bpar
    use agk_time, only: dtime, dt_cfl, save_dt, dt_min, time
    use agk_save, only: agk_save_for_restart
    use dist_fn_arrays, only: gnew
    use nonlinear_terms, only: nl_reset => reset_init
    use mp, only: proc0
    use file_utils, only: error_unit
    use antenna, only: dump_ant_amp
    use job_manage, only: time_message

    logical :: exit
    integer :: istep 
    integer, save :: istep_last = -1 ! allow adjustment on first time step
    integer :: istatus
    integer, save :: nconsec=0

! save fields and distribution function

! calls on consecutive time steps is probably an error
    if (istep_last + 1 == istep) then
       nconsec=nconsec+1
    else
       nconsec=0
    endif

    if (abort_rapid_time_step_change .and. nconsec .gt. 4) then
       exit = .true.
       if (proc0) write(error_unit(), *) &
            'Time step changing rapidly.  Abort run.'
       return
    end if

    if (dtime(1)/delt_adj <= dt_min) then
       dtime(1) = dt_min  ! set it so restart is ok
       if (proc0) write(error_unit(), *) &
            'Time step is too small.'
       exit = .true.
       return
    end if

    if (proc0) call time_message(.true.,time_reinit,' Re-initialize')

    !GGH- Commented this out as unnecessary 07 Jun 07
!    if (proc0) call dump_ant_amp
    call agk_save_for_restart &
         (gnew, time, dtime(1), istatus, use_Phi, use_Apar, use_Bpar, exit_in=.true.)

    gnew = 0.

! change timestep 

! If timestep is too big, make it smaller
    if (dtime(1) > dt_cfl) then
       dtime(1) = dtime(1)/delt_adj

! If timestep is too small, make it bigger

    else if (dtime(1) < min(dt0, dt_cfl/delt_adj/delt_cushion)) then
       dtime(1) = min(dtime(1)*delt_adj, dt0)

    endif

    call save_dt (dtime(1))

    if (proc0) write(*,'(a,es16.8,a,es16.8,a,i8)') &
         'Changing time step to ', dtime(1), ' time= ', time,' step= ',istep
    
! prepare to reinitialize inversion matrix, etc.
    call d_reset ! dist_fn
    call c_reset ! collisions
    call f_reset ! fields
    call fi_reset ! fields_implicit
    call g_reset ! init_g
    call nl_reset ! nonlinear terms

! reinitialize
    call init_fields

    if (proc0) call time_message(.true.,time_reinit,' Re-initialize')

    istep_last = istep

  end subroutine reset_time_step

  subroutine check_time_step (istep, reset, exit)

    use agk_time, only: dt_cfl, dtime

    integer :: istep
    logical :: reset, exit
    logical :: first = .true.

    if (first) call init_reinit
    first = .false.
    reset = .false.

! nothing to do if exiting in this iteration
    if (exit) return

! If timestep is too big, make it smaller
    if (dtime(1) > dt_cfl) reset = .true.
       
! If timestep is too small, make it bigger
    if (dtime(1) < min(dt0, dt_cfl/delt_adj/delt_cushion)) reset = .true.

! other choices
!     if (mod(istep,200) == 0) reset = .true.
!     if (dtime > dt_cfl) exit = .true.

  end subroutine check_time_step

  subroutine init_reinit

    use run_parameters, only: delt_max
    use mp, only: proc0, broadcast, mp_abort
    use file_utils, only: input_unit, input_unit_exist
    use agk_time, only: save_dt_min
    integer :: in_file
    logical :: exist
    integer :: ireaderr=0
    logical :: abort_readerr=.true.

    namelist /reinit_knobs/ delt_adj, delt_minimum, abort_rapid_time_step_change
    
    if (proc0) then
       dt0 = delt_max
       delt_adj = 2.0
       delt_minimum = 1.e-5
       abort_rapid_time_step_change = .true.
       in_file = input_unit_exist("reinit_knobs",exist)
       if(exist) read (unit=in_file, nml=reinit_knobs, iostat=ireaderr)
    endif

    call broadcast (ireaderr)
    if (abort_readerr) then
       if (ireaderr > 0) then
          call mp_abort ('Read error at reinit_knobs')
       else if (ireaderr < 0) then
          call mp_abort ('End of file/record occurred at reinit_knobs')
       endif
    endif

    call broadcast (dt0)
    call broadcast (delt_adj)
    call broadcast (abort_rapid_time_step_change)
    call broadcast (delt_minimum)
    
    call save_dt_min (delt_minimum)

  end subroutine init_reinit

end module agk_reinit

