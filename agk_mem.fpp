# include "define.inc"

!-----------------------------------------------------------------------------!
!-----------------------------------------------------------------------------!
!!*                                                                         *!!
!!              AstroGK Memory Management and Diagnostic Module              !!
!!                                   2008                                    !!
!!*                                                                         *!!
!-----------------------------------------------------------------------------!
!-----------------------------------------------------------------------------!
! This module contains routines for the accounting of the allocation of large
!     arrays in AstroGK.  It prints out memory diagnostics to a file
!     runname.mem, with a running account of total memory allocated.
!NAMELIST memory:
!   mem_account     !T=turn on accounting statistics
!   mem_output_on   !T=Output running tally of memory
!   mem_debug       !T=Turn on output of allocated variables with dimensions
!NOTE: These routines are only run from proc0

module agk_mem
  use agk_time, only: time 
  implicit none
  private

  !Variables specifying allocated variable type
  integer, parameter :: numtype = 5 ! number of variable types
  integer, parameter :: tcomplex = 1
  integer, parameter :: treal = 2
  integer, parameter :: tinteger =3
  integer, parameter :: tdouble = 4
  integer, parameter :: tdcomplex = 5
  integer, parameter :: ttotal = 0
  character(8), dimension(0:numtype) :: ttype
  integer :: mem_unit                         !Unit for run_name.mem file
  logical :: mem_account           !T=turn on accounting statistics
  logical :: mem_output_on         !T=Output running tally of memory
  logical :: mem_debug             !T=Turn on output of allocated variables with dimensions
  logical :: initialized = .false.
  real, dimension(0:numtype) :: memsize        !Size allocated of each type(MB)
  real :: memmax                         !Maximum allocated memory (MB)
  real, dimension(1:numtype) :: tbytes         !Megabytes for each type (MB)
  logical :: no_driver = .false.

  interface attempt
     module procedure attempt1, attempt2, attempt3, attempt4, attempt5, attempt6
  end interface attempt

  interface mem_deallocate
     module procedure mem_deallocate1, mem_deallocate2, mem_deallocate3
     module procedure mem_deallocate4, mem_deallocate5, mem_deallocate6
  end interface mem_deallocate

!!$  interface alloc8
!!$     module procedure alloc8_old
!!$  end interface alloc8
  interface alloc8
     module procedure alloc8_r1, alloc8_r2, alloc8_r3, alloc8_r4, alloc8_r5, alloc8_r6
     module procedure alloc8_c1, alloc8_c2, alloc8_c3, alloc8_c4, alloc8_c5, alloc8_c6
     module procedure alloc8_i1, alloc8_i2, alloc8_i3, alloc8_i4, alloc8_i5, alloc8_i6
# ifdef QUAD_PRECISION
!!! effective when real/double is promoted to double/quad (quad is available)
     module procedure alloc8_d1, alloc8_d2, alloc8_d3, alloc8_d4, alloc8_d5, alloc8_d6
     module procedure alloc8_dc1, alloc8_dc2, alloc8_dc3, alloc8_dc4, alloc8_dc5, alloc8_dc6
# endif
  end interface alloc8

!!$  interface dealloc8
!!$     module procedure dealloc8_old
!!$  end interface dealloc8
  interface dealloc8
     module procedure dealloc8_r1, dealloc8_r2, dealloc8_r3, dealloc8_r4, dealloc8_r5, dealloc8_r6
     module procedure dealloc8_c1, dealloc8_c2, dealloc8_c3, dealloc8_c4, dealloc8_c5, dealloc8_c6
     module procedure dealloc8_i1, dealloc8_i2, dealloc8_i3, dealloc8_i4, dealloc8_i5, dealloc8_i6
# ifdef QUAD_PRECISION
!!! effective when real/double is promoted to double/quad (quad is available)
     module procedure dealloc8_d1, dealloc8_d2, dealloc8_d3, dealloc8_d4, dealloc8_d5, dealloc8_d6
     module procedure dealloc8_dc1, dealloc8_dc2, dealloc8_dc3, dealloc8_dc4, dealloc8_dc5, dealloc8_dc6
# endif
  end interface dealloc8
  
  public :: alloc8, dealloc8
  public :: init_mem, finish_mem
  public :: highwater_memory
  public :: memmax

contains
!------------------------------------------------------------------------------
!                              AstroGK, 2008
!------------------------------------------------------------------------------
! Open output file run_name.mem if mem_output_on=.true. and on proc0

  subroutine init_mem
    use file_utils, only: open_output_file
    use mp, only: proc0
    use constants, only: zi, pi, dpi, dzi
# ifdef NO_SIZEOF
    use constants, only: size_of
# endif
    implicit none
    real :: mb
    if (initialized) return
    initialized = .true.

    call read_parameters

    if (.not. proc0 .or. .not. mem_account) return

    if (mem_output_on .or. mem_debug) call open_output_file(mem_unit,".mem")

! Initialize allocated memory of each type
    memsize(:)=0
    memmax=memsize(ttotal)
! Set variable type names and sizes
    ttype(tcomplex)="complex"
    ttype(treal)="real"
    ttype(tdouble)="double"
    ttype(tinteger)="integer"
    ttype(tdcomplex)="dcomplex"
    ttype(ttotal)="total"
! Set type sizes (Mbytes) 
! TT: 'sizeof' is a non-standard fortran statement
! TT: but works fine on most modern compilers except gfortran.
! TT: Checked with ifort, xlf, g95, pathscale & pgi
! TT: but not with absoft, nag & lahay
! NOTE: Assumes promotion to double precision during compilation
!    tbytes(tcomplex)=16./1024.**2.
!    tbytes(treal)=8./1024.**2.
!    tbytes(tinteger)=8./1024.**2.
    mb=1./real(1024**2)
# ifdef NO_SIZEOF
    tbytes(tcomplex)=real(size_of(zi))*mb
    tbytes(treal)=real(size_of(pi))*mb
    tbytes(tdouble)=real(size_of(dpi))*mb
    tbytes(tinteger)=real(size_of(1))*mb
    tbytes(tdcomplex)=real(size_of(dzi))*mb
# else
    tbytes(tcomplex)=real(sizeof(zi))*mb
    tbytes(treal)=real(sizeof(pi))*mb
    tbytes(tdouble)=real(sizeof(dpi))*mb
    tbytes(tinteger)=real(sizeof(1))*mb
    tbytes(tdcomplex)=real(sizeof(dzi))*mb
# endif

! For output of memory accounting, temporarily set time=0
    time=0.

  end subroutine init_mem
!------------------------------------------------------------------------------
!                              AstroGK, 2008
!------------------------------------------------------------------------------
! Read parameters from memory namelist
  subroutine read_parameters 
    use file_utils
    use mp, only: proc0, broadcast, mp_abort
    implicit none
    integer :: in_file
    logical :: exist
    integer :: ireaderr=0
    logical :: abort_readerr=.true.

    namelist /memory/ mem_account, mem_output_on, mem_debug
    
! Default values
    mem_account=.false.
    mem_output_on=.false.
    mem_debug=.false.

    if (proc0) then
       in_file=input_unit_exist("memory",exist)
       if (.not. exist) then
          no_driver = .true.
       else
          read (unit=input_unit("memory"), nml=memory, iostat=ireaderr)
       endif
    endif
    
    call broadcast(ireaderr)
    if (abort_readerr) then
       if (ireaderr > 0) then
          call mp_abort ('Read error at memory')
       else if (ireaderr < 0) then
          call mp_abort ('End of file/record occurred at memory')
       endif
    endif

    call broadcast (mem_account)
    call broadcast (mem_output_on)
    call broadcast (mem_debug)
    call broadcast (no_driver)

  end subroutine read_parameters
!------------------------------------------------------------------------------
!                              AstroGK, 2008
!------------------------------------------------------------------------------
! Close output file run_name.mem if mem_output_on=.true. and on proc0
  subroutine finish_mem
    use file_utils, only: close_output_file
    use mp, only: proc0
    implicit none

    if (.not. proc0 .or. .not. mem_account) return

    if (mem_output_on .or. mem_debug) call close_output_file(mem_unit)

  end subroutine finish_mem

!------------------------------------------------------------------------------
!                              AstroGK, 2008
!------------------------------------------------------------------------------
! Document allocation of array 
! Account for size and output running tally

  subroutine alloc8_r1(r1,v)
    use mp, only: proc0
    implicit none
    real, intent(in) :: r1(:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = treal
    ndim = size(shape(r1))
    call attempt0 (v,ntype,ndim,shape(r1))

    return
  end subroutine alloc8_r1
    
  subroutine alloc8_r2(r2,v)
    use mp, only: proc0
    implicit none
    real, intent(in) :: r2(:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = treal
    ndim = size(shape(r2))
    call attempt0 (v,ntype,ndim,shape(r2))

    return
  end subroutine alloc8_r2
    
  subroutine alloc8_r3(r3,v)
    use mp, only: proc0
    implicit none
    real, intent(in) :: r3(:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = treal
    ndim = size(shape(r3))
    call attempt0 (v,ntype,ndim,shape(r3))

    return
  end subroutine alloc8_r3
    
  subroutine alloc8_r4(r4,v)
    use mp, only: proc0
    implicit none
    real, intent(in) :: r4(:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = treal
    ndim = size(shape(r4))
    call attempt0 (v,ntype,ndim,shape(r4))

    return
  end subroutine alloc8_r4
    
  subroutine alloc8_r5(r5,v)
    use mp, only: proc0
    implicit none
    real, intent(in) :: r5(:,:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = treal
    ndim = size(shape(r5))
    call attempt0 (v,ntype,ndim,shape(r5))

    return
  end subroutine alloc8_r5
    
  subroutine alloc8_r6(r6,v)
    use mp, only: proc0
    implicit none
    real, intent(in) :: r6(:,:,:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = treal
    ndim = size(shape(r6))
    call attempt0 (v,ntype,ndim,shape(r6))

    return
  end subroutine alloc8_r6

# ifdef QUAD_PRECISION
  subroutine alloc8_d1(d1,v)
    use mp, only: proc0
    implicit none
    double precision, intent(in) :: d1(:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = tdouble
    ndim = size(shape(d1))
    call attempt0 (v,ntype,ndim,shape(d1))

    return
  end subroutine alloc8_d1
    
  subroutine alloc8_d2(d2,v)
    use mp, only: proc0
    implicit none
    double precision, intent(in) :: d2(:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = tdouble
    ndim = size(shape(d2))
    call attempt0 (v,ntype,ndim,shape(d2))

    return
  end subroutine alloc8_d2
    
  subroutine alloc8_d3(d3,v)
    use mp, only: proc0
    implicit none
    double precision, intent(in) :: d3(:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = tdouble
    ndim = size(shape(d3))
    call attempt0 (v,ntype,ndim,shape(d3))

    return
  end subroutine alloc8_d3
    
  subroutine alloc8_d4(d4,v)
    use mp, only: proc0
    implicit none
    double precision, intent(in) :: d4(:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = tdouble
    ndim = size(shape(d4))
    call attempt0 (v,ntype,ndim,shape(d4))

    return
  end subroutine alloc8_d4
    
  subroutine alloc8_d5(d5,v)
    use mp, only: proc0
    implicit none
    double precision, intent(in) :: d5(:,:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = tdouble
    ndim = size(shape(d5))
    call attempt0 (v,ntype,ndim,shape(d5))

    return
  end subroutine alloc8_d5
    
  subroutine alloc8_d6(d6,v)
    use mp, only: proc0
    implicit none
    double precision, intent(in) :: d6(:,:,:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = tdouble
    ndim = size(shape(d6))
    call attempt0 (v,ntype,ndim,shape(d6))

    return
  end subroutine alloc8_d6
# endif

  subroutine alloc8_c1(c1,v)
    use mp, only: proc0
    implicit none
    complex, intent(in) :: c1(:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = tcomplex
    ndim = size(shape(c1))
    call attempt0 (v,ntype,ndim,shape(c1))

    return
  end subroutine alloc8_c1
    
  subroutine alloc8_c2(c2,v)
    use mp, only: proc0
    implicit none
    complex, intent(in) :: c2(:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = tcomplex
    ndim = size(shape(c2))
    call attempt0 (v,ntype,ndim,shape(c2))

    return
  end subroutine alloc8_c2
    
  subroutine alloc8_c3(c3,v)
    use mp, only: proc0
    implicit none
    complex, intent(in) :: c3(:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = tcomplex
    ndim = size(shape(c3))
    call attempt0 (v,ntype,ndim,shape(c3))

    return
  end subroutine alloc8_c3
    
  subroutine alloc8_c4(c4,v)
    use mp, only: proc0
    implicit none
    complex, intent(in) :: c4(:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = tcomplex
    ndim = size(shape(c4))
    call attempt0 (v,ntype,ndim,shape(c4))

    return
  end subroutine alloc8_c4
    
  subroutine alloc8_c5(c5,v)
    use mp, only: proc0
    implicit none
    complex, intent(in) :: c5(:,:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = tcomplex
    ndim = size(shape(c5))
    call attempt0 (v,ntype,ndim,shape(c5))

    return
  end subroutine alloc8_c5
    
  subroutine alloc8_c6(c6,v)
    use mp, only: proc0
    implicit none
    complex, intent(in) :: c6(:,:,:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = tcomplex
    ndim = size(shape(c6))
    call attempt0 (v,ntype,ndim,shape(c6))

    return
  end subroutine alloc8_c6

# ifdef QUAD_PRECISION
  subroutine alloc8_dc1(dc1,v)
    use mp, only: proc0
    implicit none
    complex (kind=kind(0.d0)), intent(in) :: dc1(:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = tdcomplex
    ndim = size(shape(dc1))
    call attempt0 (v,ntype,ndim,shape(dc1))

    return
  end subroutine alloc8_dc1
    
  subroutine alloc8_dc2(dc2,v)
    use mp, only: proc0
    implicit none
    complex (kind=kind(0.d0)), intent(in) :: dc2(:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = tdcomplex
    ndim = size(shape(dc2))
    call attempt0 (v,ntype,ndim,shape(dc2))

    return
  end subroutine alloc8_dc2
    
  subroutine alloc8_dc3(dc3,v)
    use mp, only: proc0
    implicit none
    complex (kind=kind(0.d0)), intent(in) :: dc3(:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = tdcomplex
    ndim = size(shape(dc3))
    call attempt0 (v,ntype,ndim,shape(dc3))

    return
  end subroutine alloc8_dc3
    
  subroutine alloc8_dc4(dc4,v)
    use mp, only: proc0
    implicit none
    complex (kind=kind(0.d0)), intent(in) :: dc4(:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = tdcomplex
    ndim = size(shape(dc4))
    call attempt0 (v,ntype,ndim,shape(dc4))

    return
  end subroutine alloc8_dc4
    
  subroutine alloc8_dc5(dc5,v)
    use mp, only: proc0
    implicit none
    complex (kind=kind(0.d0)), intent(in) :: dc5(:,:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = tdcomplex
    ndim = size(shape(dc5))
    call attempt0 (v,ntype,ndim,shape(dc5))

    return
  end subroutine alloc8_dc5
    
  subroutine alloc8_dc6(dc6,v)
    use mp, only: proc0
    implicit none
    complex (kind=kind(0.d0)), intent(in) :: dc6(:,:,:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = tdcomplex
    ndim = size(shape(dc6))
    call attempt0 (v,ntype,ndim,shape(dc6))

    return
  end subroutine alloc8_dc6
# endif
  
  subroutine alloc8_i1(i1,v)
    use mp, only: proc0
    implicit none
    integer, intent(in) :: i1(:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = tinteger
    ndim = size(shape(i1))
    call attempt0 (v,ntype,ndim,shape(i1))

    return
  end subroutine alloc8_i1

  subroutine alloc8_i2(i2,v)
    use mp, only: proc0
    implicit none
    integer, intent(in) :: i2(:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = tinteger
    ndim = size(shape(i2))
    call attempt0 (v,ntype,ndim,shape(i2))

    return
  end subroutine alloc8_i2

  subroutine alloc8_i3(i3,v)
    use mp, only: proc0
    implicit none
    integer, intent(in) :: i3(:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = tinteger
    ndim = size(shape(i3))
    call attempt0 (v,ntype,ndim,shape(i3))

    return
  end subroutine alloc8_i3
    
  subroutine alloc8_i4(i4,v)
    use mp, only: proc0
    implicit none
    integer, intent(in) :: i4(:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = tinteger
    ndim = size(shape(i4))
    call attempt0 (v,ntype,ndim,shape(i4))

    return
  end subroutine alloc8_i4
    
  subroutine alloc8_i5(i5,v)
    use mp, only: proc0
    implicit none
    integer, intent(in) :: i5(:,:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = tinteger
    ndim = size(shape(i5))
    call attempt0 (v,ntype,ndim,shape(i5))

    return
  end subroutine alloc8_i5
    
  subroutine alloc8_i6(i6,v)
    use mp, only: proc0
    implicit none
    integer, intent(in) :: i6(:,:,:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim
    
    if (.not. (proc0 .and. mem_account)) return
    
    ntype = tinteger
    ndim = size(shape(i6))
    call attempt0 (v,ntype,ndim,shape(i6))

    return
  end subroutine alloc8_i6
    
  subroutine alloc8_old(r1, r2, r3, r4, r5, r6, &
       c1,  c2, c3, c4, c5, c6, &
       i1,  i2, i3, i4, i5, i6, &
       v)

    use mp, only: proc0
    implicit none

    real, optional, intent(in), dimension(:) :: r1
    real, optional, intent(in), dimension(:,:) :: r2
    real, optional, intent(in), dimension(:,:,:) :: r3
    real, optional, intent(in), dimension(:,:,:,:) :: r4
    real, optional, intent(in), dimension(:,:,:,:,:) :: r5
    real, optional, intent(in), dimension(:,:,:,:,:,:) :: r6

    complex, optional, intent(in), dimension(:) :: c1
    complex, optional, intent(in), dimension(:,:) :: c2
    complex, optional, intent(in), dimension(:,:,:) :: c3
    complex, optional, intent(in), dimension(:,:,:,:) :: c4
    complex, optional, intent(in), dimension(:,:,:,:,:) :: c5
    complex, optional, intent(in), dimension(:,:,:,:,:,:) :: c6

    integer, optional, intent(in), dimension(:) :: i1
    integer, optional, intent(in), dimension(:,:) :: i2
    integer, optional, intent(in), dimension(:,:,:) :: i3
    integer, optional, intent(in), dimension(:,:,:,:) :: i4
    integer, optional, intent(in), dimension(:,:,:,:,:) :: i5
    integer, optional, intent(in), dimension(:,:,:,:,:,:) :: i6

    character (*), intent(in) :: v          ! Name of variable allocated
    integer :: n1, n2, n3, n4, n5, n6
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then

       if (present(r1)) then
          n1 = size(r1, 1)
          ntype = treal
          ndim = 1
          call attempt (v,ntype,ndim,n1)
          return
       end if

       if (present(r2)) then
          n1 = size(r2, 1) ; n2 = size(r2, 2)
          ntype = treal
          ndim = 2
          call attempt (v,ntype,ndim,n1,n2)
          return
       end if

       if (present(r3)) then
          n1 = size(r3, 1) ; n2 = size(r3, 2) ; n3 = size(r3, 3)
          ntype = treal
          ndim = 3
          call attempt (v,ntype,ndim,n1,n2,n3)
          return
       end if
          
       if (present(r4)) then
          n1 = size(r4, 1) ; n2 = size(r4, 2) ; n3 = size(r4, 3)
          n4 = size(r4, 4)
          ntype = treal
          ndim = 4
          call attempt (v,ntype,ndim,n1,n2,n3,n4)
          return
       end if

       if (present(r5)) then
          n1 = size(r5, 1) ; n2 = size(r5, 2) ; n3 = size(r5, 3)
          n4 = size(r5, 4) ; n5 = size(r5, 5)
          ntype = treal
          ndim = 5
          call attempt (v,ntype,ndim,n1,n2,n3,n4,n5)
          return
       end if

       if (present(r6)) then
          n1 = size(r6, 1) ; n2 = size(r6, 2) ; n3 = size(r6, 3)          
          n4 = size(r6, 4) ; n5 = size(r6, 5) ; n6 = size(r6, 6)          
          ntype = treal
          ndim = 6
          call attempt (v,ntype,ndim,n1,n2,n3,n4,n5,n6)
          return
       end if

       if (present(c1)) then
          n1 = size(c1, 1)
          ntype = tcomplex
          ndim = 1
          call attempt (v,ntype,ndim,n1)
          return
       end if

       if (present(c2)) then
          n1 = size(c2, 1) ; n2 = size(c2, 2)
          ntype = tcomplex
          ndim = 2
          call attempt (v,ntype,ndim,n1,n2)
          return
       end if

       if (present(c3)) then
          n1 = size(c3, 1) ; n2 = size(c3, 2) ; n3 = size(c3, 3)
          ntype = tcomplex
          ndim = 3
          call attempt (v,ntype,ndim,n1,n2,n3)
          return
       end if

       if (present(c4)) then
          n1 = size(c4, 1) ; n2 = size(c4, 2) ; n3 = size(c4, 3)
          n4 = size(c4, 4)
          ntype = tcomplex
          ndim = 4
          call attempt (v,ntype,ndim,n1,n2,n3,n4)
          return
       end if

       if (present(c5)) then
          n1 = size(c5, 1) ; n2 = size(c5, 2) ; n3 = size(c5, 3)
          n4 = size(c5, 4) ; n5 = size(c5, 5)
          ntype = tcomplex
          ndim = 5
          call attempt (v,ntype,ndim,n1,n2,n3,n4,n5)
          return
       end if

       if (present(c6)) then
          n1 = size(c6, 1) ; n2 = size(c6, 2) ; n3 = size(c6, 3)          
          n4 = size(c6, 4) ; n5 = size(c6, 5) ; n5 = size(c6, 6)          
          ntype = tcomplex
          ndim = 6
          call attempt (v,ntype,ndim,n1,n2,n3,n4,n5,n6)
          return
       end if

       if (present(i1)) then
          n1 = size(i1, 1)
          ntype = tinteger
          ndim = 1
          call attempt (v,ntype,ndim,n1)
          return
       end if

       if (present(i2)) then
          n1 = size(i2, 1) ; n2 = size(i2, 2)
          ntype = tinteger
          ndim = 2
          call attempt (v,ntype,ndim,n1,n2)
          return
       end if

       if (present(i3)) then
          n1 = size(i3, 1) ; n2 = size(i3, 2) ; n3 = size(i3, 3)
          ntype = tinteger
          ndim = 3
          call attempt (v,ntype,ndim,n1,n2,n3)
          return
       end if

       if (present(i4)) then
          n1 = size(i4, 1) ; n2 = size(i4, 2) ; n3 = size(i4, 3)
          n4 = size(i4, 4)
          ntype = tinteger
          ndim = 4
          call attempt (v,ntype,ndim,n1,n2,n3,n4)
          return
       end if

       if (present(i5)) then
          n1 = size(i5, 1) ; n2 = size(i5, 2) ; n3 = size(i5, 3)
          n4 = size(i5, 4) ; n5 = size(i5, 5)
          ntype = tinteger
          ndim = 5
          call attempt (v,ntype,ndim,n1,n2,n3,n4,n5)
          return
       end if

       if (present(i6)) then
          n1 = size(i6, 1) ; n2 = size(i6, 2) ; n3 = size(i6, 3)          
          n4 = size(i6, 4) ; n5 = size(i6, 5) ; n5 = size(i6, 6)          
          ntype = tinteger
          ndim = 6
          call attempt (v,ntype,ndim,n1,n2,n3,n4,n5,n6)
          return
       end if

    end if

  end subroutine alloc8_old

!!!RN> All the attempt routines can be combined
  subroutine attempt0(v,ntype,ndim,nn)
    use file_utils, only: flush_output_file
    implicit none
    character (*), intent(in) :: v          !Name of variable to allocate
    integer, intent(in) :: ntype            !Variable type
    integer, intent(in) :: ndim             !Number of dimensions
    integer, intent(in) :: nn(:)            !Dimensions
    real :: bytetot
    integer :: nntot
    integer :: i

    nntot=product(nn)
    bytetot=tbytes(ntype)*real(nntot)
    
!!! Output line about completed allocation
    if (mem_debug) then
       write(mem_unit,'(a,a8,a,a3)',advance='no') &
            'Allocated ',ttype(ntype),v,' ( '
       do i=1,size(nn)-1
          write(mem_unit,'(i12,a2)',advance='no') nn(i),', '
       end do
       write(mem_unit,'(i12,a2,1x,i11,6x,f10.3)') nn(size(nn)),' )', nntot, bytetot
    end if
    
!!! Account for size of allocation
    memsize(ntype)=memsize(ntype) + bytetot
!!! Sum for totals.
    memsize(ttotal)=sum(memsize(1:numtype))
    if (memsize(ttotal) > memmax) memmax=memsize(ttotal)
    
!!! Write accounting of total sizes
    if (mem_output_on) then
       write(mem_unit,'(es14.6,f10.3,1x,a8,1x,a16,1x)',advance='no') &
            time,bytetot,ttype(ntype),v
       do i=1,numtype
          write(mem_unit,'(a8,f10.3,1x)',advance='no') &
               ttype(i),memsize(i)
       end do
       write(mem_unit,'(a8,f10.3,1x)',advance='no') &
            ttype(ttotal),memsize(ttotal)
       write(mem_unit,*)
    end if
       
    call flush_output_file(mem_unit,".mem")
    
  end subroutine attempt0

!------------------------------------------------------------------------------
!                              AstroGK, 2008
!------------------------------------------------------------------------------
! 1-dimensional array
! Document allocation of variable v. Account for size and output running tally

  subroutine attempt1(v,ntype,ndim,n1)
    use file_utils, only: flush_output_file
    implicit none
    character (*), intent(in) :: v          !Name of variable to allocate
    integer, intent(in) :: ntype                  !Variable type
    integer, intent(in) :: ndim                   !Number of dimensions
    integer, intent(in) :: n1                     !Dimensions
  
! Output line about completed allocation
    if (mem_debug) write(mem_unit,'(a,a8,a,a3,1(i12,a2),6x,f10.3)')'Allocated ',ttype(ntype),v, &
         ' ( ',n1,' )',tbytes(ntype)*real(n1)
        
! Account for size of allocation
    memsize(ntype)=memsize(ntype) + tbytes(ntype)*real(n1)
! Sum for totals.
    memsize(ttotal)=sum(memsize(1:numtype))
    if (memsize(ttotal) .gt. memmax) memmax=memsize(ttotal)
    
! Write accounting of total sizes
    if (mem_output_on) write(mem_unit,'(es14.6,f10.3,1x,a8,1x,a16,1x,4(a8,f10.3,1x))')time, &
         tbytes(ntype)*real(n1),ttype(ntype),v, &
         ttype(tcomplex),memsize(tcomplex), &
         ttype(treal),memsize(treal),&
         ttype(tinteger),memsize(tinteger), &
         ttype(ttotal),memsize(ttotal)
    
    call flush_output_file(mem_unit,".mem")
        
  end subroutine attempt1

!------------------------------------------------------------------------------
!                              AstroGK, 2008
!------------------------------------------------------------------------------
! 2-dimensional array
! Document allocation of variable v. Account for size and output running tally

  subroutine attempt2(v,ntype,ndim,n1,n2)
    use file_utils, only: flush_output_file
    implicit none
    character (*), intent(in) :: v          !Name of variable to allocate
    integer, intent(in) :: ntype                  !Variable type
    integer, intent(in) :: ndim                   !Number of dimensions
    integer, intent(in) :: n1,n2                  !Dimensions

! Output line about completed allocation
    if (mem_debug) write(mem_unit,'(a,a8,a,a3,2(i12,a2),i12,6x,f10.3)')'Allocated ',ttype(ntype),v, &
         ' ( ',n1,', ',n2,' )',n1*n2,tbytes(ntype)*real(n1*n2)
        
! Account for size of allocation
    memsize(ntype)=memsize(ntype) + tbytes(ntype)*real(n1*n2)
! Sum for totals.
    memsize(ttotal)=sum(memsize(1:numtype))
    if (memsize(ttotal) .gt. memmax) memmax=memsize(ttotal)
    
! Write accounting of total sizes
    if (mem_output_on) write(mem_unit,'(es14.6,f10.3,1x,a8,1x,a16,1x,4(a8,f10.3,1x))')time, &
         tbytes(ntype)*real(n1*n2),ttype(ntype),v, &
         ttype(tcomplex),memsize(tcomplex), &
         ttype(treal),memsize(treal), &
         ttype(tinteger),memsize(tinteger), &
         ttype(ttotal),memsize(ttotal)
        
    call flush_output_file(mem_unit,".mem")
        
  end subroutine attempt2


!------------------------------------------------------------------------------
!                              AstroGK, 2008
!------------------------------------------------------------------------------
! 3-dimensional array
! Document allocation of allocate variable v. Account for size and output running tally

  subroutine attempt3(v,ntype,ndim,n1,n2,n3)
    use file_utils, only: flush_output_file
    implicit none
    character (*), intent(in) :: v          !Name of variable to allocate
    integer, intent(in) :: ntype                  !Variable type
    integer, intent(in) :: ndim                   !Number of dimensions
    integer, intent(in) :: n1,n2,n3               !Dimensions

! Output line about completed allocation
    if (mem_debug) write(mem_unit,'(a,a8,a,a3,3(i12,a2),i12,6x,f10.3)')'Allocated ',ttype(ntype),v, &
         ' ( ',n1,', ',n2,', ',n3,' )',n1*n2*n3,tbytes(ntype)*real(n1*n2*n3)
        
! Account for size of allocation
    memsize(ntype)=memsize(ntype) + tbytes(ntype)*real(n1*n2*n3)
! Sum for totals.
    memsize(ttotal)=sum(memsize(1:numtype))
    if (memsize(ttotal) .gt. memmax) memmax=memsize(ttotal)
    
! Write accounting of total sizes
    if (mem_output_on) write(mem_unit,'(es14.6,f10.3,1x,a8,1x,a16,1x,4(a8,f10.3,1x))')time, &
         tbytes(ntype)*real(n1*n2*n3),ttype(ntype),v, &
         ttype(tcomplex),memsize(tcomplex), &
         ttype(treal),memsize(treal), &
         ttype(tinteger),memsize(tinteger), &
         ttype(ttotal),memsize(ttotal)
        
    call flush_output_file(mem_unit,".mem")
        
  end subroutine attempt3
!------------------------------------------------------------------------------
!                              AstroGK, 2008
!------------------------------------------------------------------------------
! 4-dimensional array
! Document allocation of variable v. Account for size and output running tally

  subroutine attempt4(v,ntype,ndim,n1,n2,n3,n4)
    use file_utils, only: flush_output_file
    implicit none
    character (*), intent(in) :: v          !Name of variable to allocate
    integer, intent(in) :: ntype                  !Variable type
    integer, intent(in) :: ndim                   !Number of dimensions
    integer, intent(in) :: n1,n2,n3,n4            !Dimensions

! Output line about completed allocation
    if (mem_debug) write(mem_unit,'(a,a8,a,a3,4(i12,a2),i12,6x,f10.3)')'Allocated ',ttype(ntype),v, &
         ' ( ',n1,', ',n2,', ',n3,', ',n4,' )',n1*n2*n3*n4,tbytes(ntype)*real(n1*n2*n3*n4)
        
! Account for size of allocation
    memsize(ntype)=memsize(ntype) + tbytes(ntype)*real(n1*n2*n3*n4)
! Sum for totals.
    memsize(ttotal)=sum(memsize(1:numtype))
    if (memsize(ttotal) .gt. memmax) memmax=memsize(ttotal)
    
    !Write accounting of total sizes
    if (mem_output_on) write(mem_unit,'(es14.6,f10.3,1x,a8,1x,a16,1x,4(a8,f10.3,1x))')time, &
         tbytes(ntype)*real(n1*n2*n3*n4),ttype(ntype),v, &
         ttype(tcomplex),memsize(tcomplex), &
         ttype(treal),memsize(treal), &
         ttype(tinteger),memsize(tinteger), &
         ttype(ttotal),memsize(ttotal)
        
    call flush_output_file(mem_unit,".mem")
        
  end subroutine attempt4
!------------------------------------------------------------------------------
!                              AstroGK, 2008
!------------------------------------------------------------------------------
! 5-dimensional array
! Document allocation of variable v. Account for size and output running tally

  subroutine attempt5(v,ntype,ndim,n1,n2,n3,n4,n5)
    use file_utils, only: flush_output_file
    implicit none
    character (*), intent(in) :: v          !Name of variable to allocate
    integer, intent(in) :: ntype                  !Variable type
    integer, intent(in) :: ndim                   !Number of dimensions
    integer, intent(in) :: n1,n2,n3,n4,n5         !Dimensions

! Output line about completed allocation
    if (mem_debug) write(mem_unit,'(a,a8,a,a3,5(i12,a2),i12,6x,f10.3)')'Allocated ',ttype(ntype),v, &
         ' ( ',n1,', ',n2,', ',n3,', ',n4,', ',n5,' )',n1*n2*n3*n4*n5,tbytes(ntype)*real(n1*n2*n3*n4*n5)
        
! Account for size of allocation
    memsize(ntype)=memsize(ntype) + tbytes(ntype)*real(n1*n2*n3*n4*n5)
! Sum for totals.
    memsize(ttotal)=sum(memsize(1:numtype))
    if (memsize(ttotal) .gt. memmax) memmax=memsize(ttotal)
    
! Write accounting of total sizes
    if (mem_output_on) write(mem_unit,'(es14.6,f10.3,1x,a8,1x,a16,1x,4(a8,f10.3,1x))')time, &
         tbytes(ntype)*real(n1*n2*n3*n4*n5),ttype(ntype),v, &
         ttype(tcomplex),memsize(tcomplex), &
         ttype(treal),memsize(treal), &
         ttype(tinteger),memsize(tinteger), &
         ttype(ttotal),memsize(ttotal)
        
    call flush_output_file(mem_unit,".mem")
        
  end subroutine attempt5
!------------------------------------------------------------------------------
!                              AstroGK, 2008
!------------------------------------------------------------------------------
! 6-dimensional array
! Document allocation of variable v. Account for size and output running tally

  subroutine attempt6(v,ntype,ndim,n1,n2,n3,n4,n5,n6)
    use file_utils, only: flush_output_file
    implicit none
    character (*), intent(in) :: v          !Name of variable to allocate
    integer, intent(in) :: ntype                  !Variable type
    integer, intent(in) :: ndim                   !Number of dimensions
    integer, intent(in) :: n1,n2,n3,n4,n5,n6      !Dimensions

! Output line about completed allocation
    if (mem_debug) write(mem_unit,'(a,a8,a,a3,6(i12,a2),i12,6x,f10.3)')'Allocated ',ttype(ntype),v, &
         ' ( ',n1,', ',n2,', ',n3,', ',n4,', ',n5,', ',n6,' )',n1*n2*n3*n4*n5*n6,tbytes(ntype)*real(n1*n2*n3*n4*n5*n6)
        
! Account for size of allocation
    memsize(ntype)=memsize(ntype) + tbytes(ntype)*real(n1*n2*n3*n4*n5*n6)
! Sum for totals.
    memsize(ttotal)=sum(memsize(1:numtype))
    if (memsize(ttotal) .gt. memmax) memmax=memsize(ttotal)
    
! Write accounting of total sizes
    if (mem_output_on) write(mem_unit,'(es14.6,f10.3,1x,a8,1x,a16,1x,4(a8,f10.3,1x))')time, &
         tbytes(ntype)*real(n1*n2*n3*n4*n5*n6),ttype(ntype),v, &
         ttype(tcomplex),memsize(tcomplex), &
         ttype(treal),memsize(treal), &
         ttype(tinteger),memsize(tinteger), &
         ttype(ttotal),memsize(ttotal)
        
    call flush_output_file(mem_unit,".mem")
        
  end subroutine attempt6

!------------------------------------------------------------------------------
!                              AstroGK, 2008
!------------------------------------------------------------------------------
! Document deallocation of array 
! Account for size and output running tally

  subroutine dealloc8_r1(r1,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    real, allocatable :: r1(:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = treal
       ndim = size(shape(r1))
       call mem_deallocate0 (v,ntype,ndim,shape(r1))
    end if

    deallocate (r1)

    return
  end subroutine dealloc8_r1
  
  subroutine dealloc8_r2(r2,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    real, allocatable :: r2(:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = treal
       ndim = size(shape(r2))
       call mem_deallocate0 (v,ntype,ndim,shape(r2))
    end if

    deallocate (r2)

    return
  end subroutine dealloc8_r2
  
  subroutine dealloc8_r3(r3,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    real, allocatable :: r3(:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = treal
       ndim = size(shape(r3))
       call mem_deallocate0 (v,ntype,ndim,shape(r3))
    end if

    deallocate (r3)

    return
  end subroutine dealloc8_r3
  
  subroutine dealloc8_r4(r4,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    real, allocatable :: r4(:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = treal
       ndim = size(shape(r4))
       call mem_deallocate0 (v,ntype,ndim,shape(r4))
    end if

    deallocate (r4)

    return
  end subroutine dealloc8_r4
  
  subroutine dealloc8_r5(r5,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    real, allocatable :: r5(:,:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = treal
       ndim = size(shape(r5))
       call mem_deallocate0 (v,ntype,ndim,shape(r5))
    end if

    deallocate (r5)

    return
  end subroutine dealloc8_r5
  
  subroutine dealloc8_r6(r6,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    real, allocatable :: r6(:,:,:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = treal
       ndim = size(shape(r6))
       call mem_deallocate0 (v,ntype,ndim,shape(r6))
    end if

    deallocate (r6)

    return
  end subroutine dealloc8_r6

# ifdef QUAD_PRECISION
    subroutine dealloc8_d1(d1,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    double precision, allocatable :: d1(:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = tdouble
       ndim = size(shape(d1))
       call mem_deallocate0 (v,ntype,ndim,shape(d1))
    end if

    deallocate (d1)

    return
  end subroutine dealloc8_d1
  
  subroutine dealloc8_d2(d2,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    double precision, allocatable :: d2(:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = tdouble
       ndim = size(shape(d2))
       call mem_deallocate0 (v,ntype,ndim,shape(d2))
    end if

    deallocate (d2)

    return
  end subroutine dealloc8_d2
  
  subroutine dealloc8_d3(d3,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    double precision, allocatable :: d3(:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = tdouble
       ndim = size(shape(d3))
       call mem_deallocate0 (v,ntype,ndim,shape(d3))
    end if

    deallocate (d3)

    return
  end subroutine dealloc8_d3
  
  subroutine dealloc8_d4(d4,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    double precision, allocatable :: d4(:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = tdouble
       ndim = size(shape(d4))
       call mem_deallocate0 (v,ntype,ndim,shape(d4))
    end if

    deallocate (d4)

    return
  end subroutine dealloc8_d4
  
  subroutine dealloc8_d5(d5,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    double precision, allocatable :: d5(:,:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = tdouble
       ndim = size(shape(d5))
       call mem_deallocate0 (v,ntype,ndim,shape(d5))
    end if

    deallocate (d5)

    return
  end subroutine dealloc8_d5
  
  subroutine dealloc8_d6(d6,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    double precision, allocatable :: d6(:,:,:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = tdouble
       ndim = size(shape(d6))
       call mem_deallocate0 (v,ntype,ndim,shape(d6))
    end if

    deallocate (d6)

    return
  end subroutine dealloc8_d6

# endif
  
  subroutine dealloc8_c1(c1,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    complex, allocatable :: c1(:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = tcomplex
       ndim = size(shape(c1))
       call mem_deallocate0 (v,ntype,ndim,shape(c1))
    end if

    deallocate (c1)

    return
  end subroutine dealloc8_c1
  
  subroutine dealloc8_c2(c2,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    complex, allocatable :: c2(:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = tcomplex
       ndim = size(shape(c2))
       call mem_deallocate0 (v,ntype,ndim,shape(c2))
    end if

    deallocate (c2)

    return
  end subroutine dealloc8_c2
  
  subroutine dealloc8_c3(c3,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    complex, allocatable :: c3(:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = tcomplex
       ndim = size(shape(c3))
       call mem_deallocate0 (v,ntype,ndim,shape(c3))
    end if

    deallocate (c3)

    return
  end subroutine dealloc8_c3
  
  subroutine dealloc8_c4(c4,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    complex, allocatable :: c4(:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = tcomplex
       ndim = size(shape(c4))
       call mem_deallocate0 (v,ntype,ndim,shape(c4))
    end if

    deallocate (c4)

    return
  end subroutine dealloc8_c4
  
  subroutine dealloc8_c5(c5,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    complex, allocatable :: c5(:,:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = tcomplex
       ndim = size(shape(c5))
       call mem_deallocate0 (v,ntype,ndim,shape(c5))
    end if

    deallocate (c5)

    return
  end subroutine dealloc8_c5
  
  subroutine dealloc8_c6(c6,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    complex, allocatable :: c6(:,:,:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = tcomplex
       ndim = size(shape(c6))
       call mem_deallocate0 (v,ntype,ndim,shape(c6))
    end if

    deallocate (c6)

    return
  end subroutine dealloc8_c6

# ifdef QUAD_PRECISION

  subroutine dealloc8_dc1(dc1,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    complex (kind=kind(0.d0)), allocatable :: dc1(:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = tdcomplex
       ndim = size(shape(dc1))
       call mem_deallocate0 (v,ntype,ndim,shape(dc1))
    end if

    deallocate (dc1)

    return
  end subroutine dealloc8_dc1
  
  subroutine dealloc8_dc2(dc2,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    complex (kind=kind(0.d0)), allocatable :: dc2(:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = tdcomplex
       ndim = size(shape(dc2))
       call mem_deallocate0 (v,ntype,ndim,shape(dc2))
    end if

    deallocate (dc2)

    return
  end subroutine dealloc8_dc2
  
  subroutine dealloc8_dc3(dc3,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    complex (kind=kind(0.d0)), allocatable :: dc3(:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = tdcomplex
       ndim = size(shape(dc3))
       call mem_deallocate0 (v,ntype,ndim,shape(dc3))
    end if

    deallocate (dc3)

    return
  end subroutine dealloc8_dc3
  
  subroutine dealloc8_dc4(dc4,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    complex(kind=kind(0.d0)), allocatable :: dc4(:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = tdcomplex
       ndim = size(shape(dc4))
       call mem_deallocate0 (v,ntype,ndim,shape(dc4))
    end if

    deallocate (dc4)

    return
  end subroutine dealloc8_dc4
  
  subroutine dealloc8_dc5(dc5,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    complex (kind=kind(0.d0)), allocatable :: dc5(:,:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = tdcomplex
       ndim = size(shape(dc5))
       call mem_deallocate0 (v,ntype,ndim,shape(dc5))
    end if

    deallocate (dc5)

    return
  end subroutine dealloc8_dc5
  
  subroutine dealloc8_dc6(dc6,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    complex (kind=kind(1.d0)) , allocatable :: dc6(:,:,:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = tdcomplex
       ndim = size(shape(dc6))
       call mem_deallocate0 (v,ntype,ndim,shape(dc6))
    end if

    deallocate (dc6)

    return
  end subroutine dealloc8_dc6

# endif
  
  subroutine dealloc8_i1(i1,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    integer, allocatable :: i1(:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = tinteger
       ndim = size(shape(i1))
       call mem_deallocate0 (v,ntype,ndim,shape(i1))
    end if

    deallocate (i1)

    return
  end subroutine dealloc8_i1
  
  subroutine dealloc8_i2(i2,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    integer, allocatable :: i2(:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = tinteger
       ndim = size(shape(i2))
       call mem_deallocate0 (v,ntype,ndim,shape(i2))
    end if

    deallocate (i2)

    return
  end subroutine dealloc8_i2
  
  subroutine dealloc8_i3(i3,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    integer, allocatable :: i3(:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = tinteger
       ndim = size(shape(i3))
       call mem_deallocate0 (v,ntype,ndim,shape(i3))
    end if

    deallocate (i3)

    return
  end subroutine dealloc8_i3
  
  subroutine dealloc8_i4(i4,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    integer, allocatable :: i4(:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = tinteger
       ndim = size(shape(i4))
       call mem_deallocate0 (v,ntype,ndim,shape(i4))
    end if

    deallocate (i4)

    return
  end subroutine dealloc8_i4
  
  subroutine dealloc8_i5(i5,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    integer, allocatable :: i5(:,:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = tinteger
       ndim = size(shape(i5))
       call mem_deallocate0 (v,ntype,ndim,shape(i5))
    end if

    deallocate (i5)

    return
  end subroutine dealloc8_i5
  
  subroutine dealloc8_i6(i6,v)
    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    integer, allocatable :: i6(:,:,:,:,:,:)
    character (*), intent(in) :: v ! Name of variable allocated
    integer :: ntype, ndim

    if (proc0 .and. mem_account) then
       ntype = tinteger
       ndim = size(shape(i6))
       call mem_deallocate0 (v,ntype,ndim,shape(i6))
    end if

    deallocate (i6)

    return
  end subroutine dealloc8_i6

  subroutine dealloc8_old(r1, r2, r3, r4, r5, r6, &
       c1, c2, c3, c4, c5, c6, &
       i1, i2, i3, i4, i5, i6, &
       v)

    use mp, only: proc0
    implicit none

    ! RN> Violating Fortran 95 standard
    ! RN> Fortran 2003: allocatable attribute with dummy attribute
    real, allocatable, optional, dimension(:) :: r1
    real, allocatable, optional, dimension(:,:) :: r2
    real, allocatable, optional, dimension(:,:,:) :: r3
    real, allocatable, optional, dimension(:,:,:,:) :: r4
    real, allocatable, optional, dimension(:,:,:,:,:) :: r5
    real, allocatable, optional, dimension(:,:,:,:,:,:) :: r6

    complex, allocatable, optional, dimension(:) :: c1
    complex, allocatable, optional, dimension(:,:) :: c2
    complex, allocatable, optional, dimension(:,:,:) :: c3
    complex, allocatable, optional, dimension(:,:,:,:) :: c4
    complex, allocatable, optional, dimension(:,:,:,:,:) :: c5
    complex, allocatable, optional, dimension(:,:,:,:,:,:) :: c6

    integer, allocatable, optional, dimension(:) :: i1
    integer, allocatable, optional, dimension(:,:) :: i2
    integer, allocatable, optional, dimension(:,:,:) :: i3
    integer, allocatable, optional, dimension(:,:,:,:) :: i4
    integer, allocatable, optional, dimension(:,:,:,:,:) :: i5
    integer, allocatable, optional, dimension(:,:,:,:,:,:) :: i6

    character (*), intent(in) :: v          ! Name of variable allocated
    integer :: n1, n2, n3, n4, n5, n6
    integer :: ntype, ndim

    if (present(r1)) then
       if (proc0 .and. mem_account) then
          n1 = size(r1, 1)
          ntype = treal
          ndim = 1
          call mem_deallocate (v,ntype,ndim,n1)
       end if
       deallocate (r1)
       return
    end if

    if (present(r2)) then
       if (proc0 .and. mem_account) then
          n1 = size(r2, 1) ; n2 = size(r2, 2)
          ntype = treal
          ndim = 2
          call mem_deallocate (v,ntype,ndim,n1,n2)
       end if
       deallocate (r2)
       return
    end if

    if (present(r3)) then
       if (proc0 .and. mem_account) then
          n1 = size(r3, 1) ; n2 = size(r3, 2) ; n3 = size(r3, 3)
          ntype = treal
          ndim = 3
          call mem_deallocate (v,ntype,ndim,n1,n2,n3)
       end if
       deallocate (r3)
       return
    end if

    if (present(r4)) then
       if (proc0 .and. mem_account) then
          n1 = size(r4, 1) ; n2 = size(r4, 2) ; n3 = size(r4, 3)
          n4 = size(r4, 4)
          ntype = treal
          ndim = 4
          call mem_deallocate (v,ntype,ndim,n1,n2,n3,n4)
       end if
       deallocate (r4)
       return
    end if

    if (present(r5)) then
       if (proc0 .and. mem_account) then
          n1 = size(r5, 1) ; n2 = size(r5, 2) ; n3 = size(r5, 3)
          n4 = size(r5, 4) ; n5 = size(r5, 5)
          ntype = treal
          ndim = 5
          call mem_deallocate (v,ntype,ndim,n1,n2,n3,n4,n5)
       end if
       deallocate (r5)
       return
    end if

    if (present(r6)) then
       if (proc0 .and. mem_account) then
          n1 = size(r6, 1) ; n2 = size(r6, 2) ; n3 = size(r6, 3)          
          n4 = size(r6, 4) ; n5 = size(r6, 5) ; n6 = size(r6, 6)          
          ntype = treal
          ndim = 6
          call mem_deallocate (v,ntype,ndim,n1,n2,n3,n4,n5,n6)
       end if
       deallocate (r6)
       return
    end if

    if (present(c1)) then
       if (proc0 .and. mem_account) then
          n1 = size(c1, 1)
          ntype = tcomplex
          ndim = 1
          call mem_deallocate (v,ntype,ndim,n1)
       end if
       deallocate (c1)
       return
    end if

    if (present(c2)) then
       if (proc0 .and. mem_account) then
          n1 = size(c2, 1) ; n2 = size(c2, 2)
          ntype = tcomplex
          ndim = 2
          call mem_deallocate (v,ntype,ndim,n1,n2)
       end if
       deallocate (c2)
       return
    end if

    if (present(c3)) then
       if (proc0 .and. mem_account) then
          n1 = size(c3, 1) ; n2 = size(c3, 2) ; n3 = size(c3, 3)
          ntype = tcomplex
          ndim = 3
          call mem_deallocate (v,ntype,ndim,n1,n2,n3)
       end if
       deallocate (c3)
       return
    end if

    if (present(c4)) then
       if (proc0 .and. mem_account) then
          n1 = size(c4, 1) ; n2 = size(c4, 2) ; n3 = size(c4, 3)
          n4 = size(c4, 4)
          ntype = tcomplex
          ndim = 4
          call mem_deallocate (v,ntype,ndim,n1,n2,n3,n4)
       end if
       deallocate (c4)
       return
    end if

    if (present(c5)) then
       if (proc0 .and. mem_account) then
          n1 = size(c5, 1) ; n2 = size(c5, 2) ; n3 = size(c5, 3)
          n4 = size(c5, 4) ; n5 = size(c5, 5)
          ntype = tcomplex
          ndim = 5
          call mem_deallocate (v,ntype,ndim,n1,n2,n3,n4,n5)
       end if
       deallocate (c5)
       return
    end if

    if (present(c6)) then
       if (proc0 .and. mem_account) then
          n1 = size(c6, 1) ; n2 = size(c6, 2) ; n3 = size(c6, 3)          
          n4 = size(c6, 4) ; n5 = size(c6, 5) ; n5 = size(c6, 6)          
          ntype = tcomplex
          ndim = 6
          call mem_deallocate (v,ntype,ndim,n1,n2,n3,n4,n5,n6)
       end if
       deallocate (c6)
       return
    end if

    if (present(i1)) then
       if (proc0 .and. mem_account) then
          n1 = size(i1, 1)
          ntype = tinteger
          ndim = 1
          call mem_deallocate (v,ntype,ndim,n1)
       end if
       deallocate (i1)
       return
    end if

    if (present(i2)) then
       if (proc0 .and. mem_account) then
          n1 = size(i2, 1) ; n2 = size(i2, 2)
          ntype = tinteger
          ndim = 2
          call mem_deallocate (v,ntype,ndim,n1,n2)
       end if
       deallocate (i2)
       return
    end if

    if (present(i3)) then
       if (proc0 .and. mem_account) then
          n1 = size(i3, 1) ; n2 = size(i3, 2) ; n3 = size(i3, 3)
          ntype = tinteger
          ndim = 3
          call mem_deallocate (v,ntype,ndim,n1,n2,n3)
       end if
       deallocate (i3)
       return
    end if

    if (present(i4)) then
       if (proc0 .and. mem_account) then
          n1 = size(i4, 1) ; n2 = size(i4, 2) ; n3 = size(i4, 3)
          n4 = size(i4, 4)
          ntype = tinteger
          ndim = 4
          call mem_deallocate (v,ntype,ndim,n1,n2,n3,n4)
       end if
       deallocate (i4)
       return
    end if

    if (present(i5)) then
       if (proc0 .and. mem_account) then
          n1 = size(i5, 1) ; n2 = size(i5, 2) ; n3 = size(i5, 3)
          n4 = size(i5, 4) ; n5 = size(i5, 5)
          ntype = tinteger
          ndim = 5
          call mem_deallocate (v,ntype,ndim,n1,n2,n3,n4,n5)
       end if
       deallocate (i5)
       return
    end if

    if (present(i6)) then
       if (proc0 .and. mem_account) then
          n1 = size(i6, 1) ; n2 = size(i6, 2) ; n3 = size(i6, 3)          
          n4 = size(i6, 4) ; n5 = size(i6, 5) ; n5 = size(i6, 6)          
          ntype = tinteger
          ndim = 6
          call mem_deallocate (v,ntype,ndim,n1,n2,n3,n4,n5,n6)
       end if
       deallocate (i6)
       return
    end if

  end subroutine dealloc8_old
  
!!!RN> All the mem_deallocate routines can be combined
  subroutine mem_deallocate0(v,ntype,ndim,nn)
    use file_utils, only: flush_output_file
    implicit none
    character (*), intent(in) :: v          !Name of variable to deallocate
    integer, intent(in) :: ntype            !Variable type
    integer, intent(in) :: ndim             !Number of dimensions
    integer, intent(in) :: nn(:)            !Dimensions
    real :: bytetot
    integer :: nntot
    integer :: i

    nntot=product(nn)
    bytetot=tbytes(ntype)*real(nntot)
    
!!! Output line about attempted allocation
    if (mem_debug) then
       write(mem_unit,'(a,a8,a,a3,1(i12,a2),6x,f10.3)',advance='no') &
            'Deallocating ',ttype(ntype),v,' ( '
       do i=1,size(nn)-1
          write(mem_unit,'(i12,a2)',advance='no') nn(i),', '
       end do
       write(mem_unit,'(i12,a2,1x,i11,6x,f10.3)') nn(size(nn)),' ', nntot, bytetot
    end if
        
!!! Account for size of allocation
    memsize(ntype)=memsize(ntype) - bytetot
!!! Sum for totals.
    memsize(ttotal)=sum(memsize(1:numtype))
    if (memsize(ttotal) > memmax) memmax=memsize(ttotal)
        
!!! Write accounting of total sizes
    if (mem_output_on) then
       write(mem_unit,'(es14.6,f10.3,1x,a8,1x,a16,1x)',advance='no') &
            time,-bytetot,ttype(ntype),v
       do i=1,numtype
          write(mem_unit,'(a8,f10.3,1x)',advance='no') &
               ttype(i),memsize(i)
       end do
       write(mem_unit,'(a8,f10.3,1x)',advance='no') &
            ttype(ttotal),memsize(ttotal)
       write(mem_unit,*) ' *** '
    endif
    
    call flush_output_file(mem_unit,".mem")
        
  end subroutine mem_deallocate0

!------------------------------------------------------------------------------
!                              AstroGK, 2008
!------------------------------------------------------------------------------
! 1-dimensional array
! Document deallocation of variable v. Account for size and output running tally

  subroutine mem_deallocate1(v,ntype,ndim,n1)

    use mp, only: proc0
    use file_utils, only: flush_output_file
    implicit none
    character (*), intent(in) :: v          !Name of variable to deallocate
    integer, intent(in) :: ntype                  !Variable type
    integer, intent(in) :: ndim                   !Number of dimensions
    integer, intent(in) :: n1                     !Dimensions


    if (proc0 .and. mem_account) then

! Output line about attempted allocation
       if (mem_debug) write(mem_unit,'(a,a8,a,a3,1(i12,a2),6x,f10.3)')'Deallocating ',ttype(ntype),v, &
            ' ( ',n1,' )',tbytes(ntype)*real(n1)
        
! Account for size of allocation
       memsize(ntype)=memsize(ntype) - tbytes(ntype)*real(n1)
! Sum for totals.
       memsize(ttotal)=sum(memsize(1:numtype))
       if (memsize(ttotal) .gt. memmax) memmax=memsize(ttotal)
        
! Write accounting of total sizes
       if (mem_output_on) write(mem_unit,'(es14.6,f10.3,1x,a8,1x,a16,1x,4(a8,f10.3,1x))')time, &
            -tbytes(ntype)*real(n1),ttype(ntype),v, &
            ttype(tcomplex),memsize(tcomplex), &
            ttype(treal),memsize(treal), &
            ttype(tinteger),memsize(tinteger), &
            ttype(ttotal),memsize(ttotal)
        
       call flush_output_file(mem_unit,".mem")
        
    end if


  end subroutine mem_deallocate1
!------------------------------------------------------------------------------
!                              AstroGK, 2008
!------------------------------------------------------------------------------
! 2-dimensional array
! Document deallocation of variable v. Account for size and output running tally

  subroutine mem_deallocate2(v,ntype,ndim,n1,n2)
    use mp, only: proc0
    use file_utils, only: flush_output_file
    implicit none
    character (*), intent(in) :: v          ! Name of variable to deallocate
    integer, intent(in) :: ntype                  ! Variable type
    integer, intent(in) :: ndim                   ! Number of dimensions
    integer, intent(in) :: n1,n2                  ! Dimensions

    if (proc0 .and. mem_account) then
! Output line about attempted allocation
       if (mem_debug) write(mem_unit,'(a,a8,a,a3,2(i12,a2),6x,f10.3)')'Deallocating ',ttype(ntype),v, &
            ' ( ',n1,', ',n2,' )',tbytes(ntype)*real(n1*n2)
        
! Account for size of allocation
       memsize(ntype)=memsize(ntype) - tbytes(ntype)*real(n1*n2)
! Sum for totals.
       memsize(ttotal)=sum(memsize(1:numtype))
       if (memsize(ttotal) .gt. memmax) memmax=memsize(ttotal)
        
! Write accounting of total sizes
       if (mem_output_on) write(mem_unit,'(es14.6,f10.3,1x,a8,1x,a16,1x,4(a8,f10.3,1x))')time, &
            -tbytes(ntype)*real(n1*n2),ttype(ntype),v, &
            ttype(tcomplex),memsize(tcomplex), &
            ttype(treal),memsize(treal), &
            ttype(tinteger),memsize(tinteger), &
            ttype(ttotal),memsize(ttotal)
        
       call flush_output_file(mem_unit,".mem")
    end if

  end subroutine mem_deallocate2
!------------------------------------------------------------------------------
!                              AstroGK, 2008
!------------------------------------------------------------------------------
! 3-dimensional array
! Document deallocation of variable v. Account for size and output running tally

  subroutine mem_deallocate3(v,ntype,ndim,n1,n2,n3)
    use mp, only: proc0
    use file_utils, only: flush_output_file
    implicit none
    character (*), intent(in) :: v          ! Name of variable to deallocate
    integer, intent(in) :: ntype                  ! Variable type
    integer, intent(in) :: ndim                   ! Number of dimensions
    integer, intent(in) :: n1,n2,n3               ! Dimensions

    if (proc0 .and. mem_account) then
    
! Output line about attempted allocation
       if (mem_debug) write(mem_unit,'(a,a8,a,a3,3(i12,a2),6x,f10.3)')'Deallocating ',ttype(ntype),v, &
            ' ( ',n1,', ',n2,', ',n3,' )',tbytes(ntype)*real(n1*n2*n3)
        
! Account for size of allocation
       memsize(ntype)=memsize(ntype) - tbytes(ntype)*real(n1*n2*n3)
! Sum for totals.
       memsize(ttotal)=sum(memsize(1:numtype))
       if (memsize(ttotal) .gt. memmax) memmax=memsize(ttotal)
        
! Write accounting of total sizes
       if (mem_output_on) write(mem_unit,'(es14.6,f10.3,1x,a8,1x,a16,1x,4(a8,f10.3,1x))')time, &
            -tbytes(ntype)*real(n1*n2*n3),ttype(ntype),v, &
            ttype(tcomplex),memsize(tcomplex), &
            ttype(treal),memsize(treal), &
            ttype(tinteger),memsize(tinteger), &
            ttype(ttotal),memsize(ttotal)
        
       call flush_output_file(mem_unit,".mem")
        
    end if

  end subroutine mem_deallocate3
!------------------------------------------------------------------------------
!                              AstroGK, 2008
!------------------------------------------------------------------------------
! 4-dimensional array
! Document deallocation of variable v. Account for size and output running tally

  subroutine mem_deallocate4(v,ntype,ndim,n1,n2,n3,n4)
    use mp, only: proc0
    use file_utils, only: flush_output_file
    implicit none
    character (*), intent(in) :: v          ! Name of variable to deallocate
    integer, intent(in) :: ntype                  ! Variable type
    integer, intent(in) :: ndim                   ! Number of dimensions
    integer, intent(in) :: n1,n2,n3,n4            ! Dimensions

    if (proc0 .and. mem_account) then

! Output line about attempted allocation
       if (mem_debug) write(mem_unit,'(a,a8,a,a3,4(i12,a2),6x,f10.3)')'Deallocating ',ttype(ntype),v, &
            ' ( ',n1,', ',n2,', ',n3,', ',n4,' )', tbytes(ntype)*real(n1*n2*n3*n4)
        
! Account for size of allocation
       memsize(ntype)=memsize(ntype) - tbytes(ntype)*real(n1*n2*n3*n4)
! Sum for totals.
       memsize(ttotal)=sum(memsize(1:numtype))
       if (memsize(ttotal) .gt. memmax) memmax=memsize(ttotal)
        
! Write accounting of total sizes
       if (mem_output_on) write(mem_unit,'(es14.6,f10.3,1x,a8,1x,a16,1x,4(a8,f10.3,1x))')time, &
            -tbytes(ntype)*real(n1*n2*n3*n4),ttype(ntype),v, &
            ttype(tcomplex),memsize(tcomplex), &
            ttype(treal),memsize(treal), &
            ttype(tinteger),memsize(tinteger), &
            ttype(ttotal),memsize(ttotal)
        
       call flush_output_file(mem_unit,".mem")
    end if

  end subroutine mem_deallocate4
!------------------------------------------------------------------------------
!                              AstroGK, 2008
!------------------------------------------------------------------------------
! 5-dimensional array
! Document deallocation of variable v. Account for size and output running tally

  subroutine mem_deallocate5(v,ntype,ndim,n1,n2,n3,n4,n5)
    use mp, only: proc0
    use file_utils, only: flush_output_file
    implicit none
    character (*), intent(in) :: v           ! Name of variable to deallocate
    integer, intent(in) :: ntype                   ! Variable type
    integer, intent(in) :: ndim                    ! Number of dimensions
    integer, intent(in) :: n1,n2,n3,n4,n5          ! Dimensions

    if  (proc0 .and. mem_account) then

! Output line about attempted allocation
       if (mem_debug) write(mem_unit,'(a,a8,a,a3,5(i12,a2),6x,f10.3)')'Deallocating ',ttype(ntype),v, &
            ' ( ',n1,', ',n2,', ',n3,', ',n4,', ',n5,' )',tbytes(ntype)*real(n1*n2*n3*n4*n5)
        
! Account for size of allocation
       memsize(ntype)=memsize(ntype) - tbytes(ntype)*real(n1*n2*n3*n4*n5)
! Sum for totals.
       memsize(ttotal)=sum(memsize(1:numtype))
        
! Write accounting of total sizes
       if (mem_output_on) write(mem_unit,'(es14.6,f10.3,1x,a8,1x,a16,1x,4(a8,f10.3,1x))')time, &
            -tbytes(ntype)*real(n1*n2*n3*n4*n5),ttype(ntype),v, &
            ttype(tcomplex),memsize(tcomplex), &
            ttype(treal),memsize(treal), &
            ttype(tinteger),memsize(tinteger), &
            ttype(ttotal),memsize(ttotal)
        
       call flush_output_file(mem_unit,".mem")
    end if

  end subroutine mem_deallocate5
!------------------------------------------------------------------------------
!                              AstroGK, 2008
!------------------------------------------------------------------------------
! 6-dimensional array
! Document deallocation of variable v. Account for size and output running tally

  subroutine mem_deallocate6(v,ntype,ndim,n1,n2,n3,n4,n5,n6)
    use mp, only: proc0
    use file_utils, only: flush_output_file
    implicit none
    character (*), intent(in) :: v             ! Name of variable to deallocate
    integer, intent(in) :: ntype                     ! Variable type
    integer, intent(in) :: ndim                      ! Number of dimensions
    integer, intent(in) :: n1,n2,n3,n4,n5,n6         ! Dimensions

    if (proc0 .and. mem_account) then

! Output line about attempted allocation
       if (mem_debug) write(mem_unit,'(a,a8,a,a3,6(i12,a2),6x,f10.3)')'Deallocating ',ttype(ntype),v, &
            ' ( ',n1,', ',n2,', ',n3,', ',n4,', ',n5,', ',n6,' )',tbytes(ntype)*real(n1*n2*n3*n4*n5*n6)
        
! Account for size of allocation
       memsize(ntype)=memsize(ntype) - tbytes(ntype)*real(n1*n2*n3*n4*n5*n6)
! Sum for totals.
       memsize(ttotal)=sum(memsize(1:numtype))
        
! Write accounting of total sizes
       if (mem_output_on) write(mem_unit,'(es14.6,f10.3,1x,a8,1x,a16,1x,4(a8,f10.3,1x))')time, &
            -tbytes(ntype)*real(n1*n2*n3*n4*n5*n6),ttype(ntype),v, &
            ttype(tcomplex),memsize(tcomplex), &
            ttype(treal),memsize(treal), &
            ttype(tinteger),memsize(tinteger), &
            ttype(ttotal),memsize(ttotal)
        
       call flush_output_file(mem_unit,".mem")
    end if

  end subroutine mem_deallocate6


!------------------------------------------------------------------------------
!                              AstroGK, 2008
!------------------------------------------------------------------------------
! Output High-water memory at end of run

  subroutine highwater_memory
    use mp, only: proc0
    implicit none

    if (.not. proc0 .or. .not. mem_account) return

    PRINT '(/,'' High-Water Memory'',T25,0pf12.3,'' MB'',/)',memmax
        
  end subroutine highwater_memory
!------------------------------------------------------------------------------
!------------------------------------------------------------------------------
end module agk_mem


!Dependencies
! agk_mem.o: mp.o file_utils.o
!NOTES:
!1. mem_output_on=.true. needed to output run_name.mem file
