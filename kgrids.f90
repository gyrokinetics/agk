module kgrids
  implicit none

  public :: init_kgrids   ! procedure to initialize this module
  public :: calculate_kgrids ! Assign actual values to the kgrids EGH
  public :: aky, akx, akperp  ! (ky, kx, k_perp) * rho_ref
  public :: kperp2       ! (k_perp * rho_ref)^2
  public :: kinv         ! 1/(k_perp*rho_ref)
  public :: naky, nakx   ! #s of spectral modes
  public :: nx, ny       ! #s of grid points
  public :: x0, y0       ! Box length [x,y] = 2 * pi * [x,y]0
  public :: aky_min       ! aky(ik) = aky_min + (ik-1) / y0 <EGH
                         !     - i.e. aky_min sets the minimum ky (default is zero) 
  public :: nkpolar      ! for diagnostics of fluctuation spectra
  public :: ikx, iky     ! map from indices of k-arrays to k-values.
  public :: lukx, luky   ! map from k-values to indices of k-arrays. (Stands for look-up)
  public :: box          ! true if calculation is in 2-D domain.  Otherwise running linearly for single mode.
  public :: single       ! true if running linearly for single mode.
  public :: reality      ! true if reality condition should be enforced (for box domains)
  private

  real :: akperp, x0, y0, aky_min
  real :: akx_in, aky_in
  real, dimension (:), allocatable :: akx, aky
  real, allocatable :: kperp2(:,:)
  real, allocatable :: kinv(:,:)
  integer :: naky, nakx, nx, ny, nkpolar
  integer, dimension(:), allocatable :: ikx, iky,lukx,luky
  logical :: reality = .false.
  logical :: box = .false.
  logical :: single = .false.

  ! internal variables
  integer :: gridopt_switch
  integer, parameter :: gridopt_single = 1, gridopt_box = 2

contains

  subroutine init_kgrids
    use mp, only: proc0, broadcast, mp_abort
    use file_utils, only: input_unit, error_unit, input_unit_exist
    use text_options, only: text_option, get_option_value
    use agk_mem, only: alloc8
    implicit none

    type (text_option), dimension (3), parameter :: gridopts = &
         (/ text_option('default', gridopt_single), &
            text_option('single', gridopt_single), &
            text_option('box', gridopt_box) /)
    character(20) :: grid_option
    logical :: initialized = .false.
    namelist /kgrids/ grid_option, akperp, akx_in, aky_in, x0, y0, nx, ny, nkpolar, aky_min
    integer :: ierr, in_file
    logical :: exist
    integer :: ireaderr=0
    logical :: abort_readerr=.true.

    if (initialized) return
    initialized = .true.

! Set defaults
    nkpolar = 0
    akperp = 0.4
    akx_in = 0.
    aky_in = 0.
    nakx = 0
    naky = 0
    nx = 0
    ny = 0

    x0 = 10.
    y0 = 10.
    aky_min = 0.

    if (proc0) then  ! Processor 0 reads namelist 
       grid_option = 'default'
       in_file = input_unit_exist ("kgrids", exist)
       if (exist) read (unit=in_file, nml=kgrids, iostat=ireaderr)

       ierr = error_unit()
       call get_option_value (grid_option, gridopts, gridopt_switch, &
            ierr, "grid_option in kgrids")
       
       select case (gridopt_switch)
       case (gridopt_single)

          nakx = 1 ; naky = 1 ; nx = 0 ; ny = 0
          
          single = .true.

       case (gridopt_box)

          if (y0 < 0) y0 = -1./y0
          if (x0 < 0) x0 = -1./x0

          naky = (ny-1)/3 + 1            ! Allow for dealiasing
          nakx = 2*((nx-1)/3) + 1        ! Allow for dealiasing

          if (nkpolar == 0) nkpolar = int(real(naky-1.)*sqrt(2.))

          ! EGH enforce reality if (as is usual) the minimum ky is 0
          if (aky_min .eq. 0.0) reality = .true. 
          box = .true.

       end select
    end if

    call broadcast (ireaderr)
    if (abort_readerr) then
       if (ireaderr > 0) then
          call mp_abort ('Read error at kgrids')
       else if (ireaderr < 0) then
          call mp_abort ('End of file/record occurred at kgrids')
       endif
    endif

! Share input data among processors
    call broadcast (nx)
    call broadcast (ny)
    call broadcast (nakx)
    call broadcast (naky)
    call broadcast (x0)
    call broadcast (y0)
    call broadcast (aky_min)
    call broadcast (akperp)
    call broadcast (akx_in)
    call broadcast (aky_in)
    call broadcast (reality)
    call broadcast (box)
    call broadcast (single)
    call broadcast (nkpolar)
    call broadcast (gridopt_switch)

    allocate (akx(nakx)); call alloc8(r1=akx,v="akx")
    allocate (ikx(nakx)); call alloc8(i1=ikx,v="ikx")
    allocate (aky(naky)); call alloc8(r1=aky,v="aky")
    allocate (iky(naky)); call alloc8(i1=iky,v="iky")
    allocate (lukx((-(nakx/2)):(((nakx+1)/2)-1))); call alloc8(i1=lukx,v="lukx")
    allocate (luky(0:naky-1)); call alloc8(i1=luky,v="luky")

    call calculate_kgrids(0.0, 0.0)


  end subroutine init_kgrids

  !> Assign actual values to the kgrids. The value of kx may change in time if
  !! flow shear is being used in a linear run, with time_varying_kx set to
  !! .true.
  !    EGH

  subroutine calculate_kgrids(g_exb, shear_time)
    use mp, only: broadcast, mp_abort
    use file_utils, only: input_unit, error_unit, input_unit_exist
    use text_options, only: text_option, get_option_value
    use agk_mem, only: alloc8
    implicit none

    real, intent (in) :: g_exb, shear_time
    integer :: i

    ! Set up grids
    select case (gridopt_switch)
    case (gridopt_single)

       if (akx_in**2+aky_in /= 0.) then
          ! ignores akperp
          aky = aky_in; iky = 0
          akx = akx_in; ikx = 0
       else 
          aky = akperp  ; iky = 0
          akx = 0.; ikx = 0
       end if
       akx = akx + shear_time * aky * g_exb
       
    case (gridopt_box)
       do i = 1, naky
          iky(i) = i-1
          luky(i-1) = i
          aky(i) = real(i-1)/y0 + aky_min
       end do

       do i = 1, (nakx+1)/2
          ikx(i) = i-1
          lukx(i-1)=i
          akx(i) = real(i-1)/x0
       end do

       do i = (nakx+1)/2+1, nakx
          ikx(i) = i-nakx-1
          lukx(i-nakx-1)=i
          akx(i) = real(i-nakx-1)/x0
       end do
    end select

    if(.not.allocated(kperp2)) then
       allocate(kperp2(nakx,naky)); call alloc8(r2=kperp2,v="kperp2")
    end if
    kperp2(1:nakx,1:naky) = &
! MAB> replaced following two lines because they give error when running on bassi
!         & spread(akx(1:nakx)**2,2,naky) + &
!         & spread(aky(1:naky)**2,1,nakx)
         & spread(akx(1:nakx),2,naky)**2 + &
         & spread(aky(1:naky),1,nakx)**2
! <MAB


    ! <doc>
    !  kinv is used to avoid branching when dividing by |k|
    !  |k|=0 occurs only when it=ik=1
    !  kinv for this mode is set to 1 (result is meaningless).
    ! </doc>
    if (.not.allocated(kinv)) then
       allocate(kinv(nakx,naky)); call alloc8(r2=kinv,v="kinv")
    end if
    kinv = 0.
    where (kperp2(:,:) /= 0.) 
       kinv(:,:) = 1./sqrt(kperp2)
    end where
    kinv(1,1) = 1.
  end subroutine calculate_kgrids

end module kgrids

