module theta_grid
  implicit none


  public :: init_theta_grid   ! initialize this module
  public :: gradpar, jacob    ! related to d/dtheta; mostly holdovers from gs2
  public :: ntheta            ! # of grid points along field line if nperiod = 1
  public :: ntgrid            ! total # of grid points along field line
  public :: nperiod           ! holdover from gs2; probably = 1 for foreseeable future
  public :: theta             ! Coordinate along the field line; -pi <= theta <= pi
  public :: delthet           ! theta(i+1)-theta(i)
  public :: z0
  public :: dz
  public :: kpar

! z0 sets box length along the field line: Lz = 2 * pi * z0, since box is 2*pi in extent.

  real, dimension(:), allocatable :: theta
  real :: gradpar, z0, jacob, delthet, dz
  integer :: ntheta, ntgrid, nperiod
  real, allocatable :: kpar(:)

  private

contains

  subroutine init_theta_grid

    use mp, only: proc0, broadcast, mp_abort
    use file_utils, only: input_unit, input_unit_exist
    use constants, only: pi
    use agk_mem, only: alloc8
    implicit none
    logical, save :: initialized = .false.
    integer :: in_file, i
    logical :: exist
    integer :: ireaderr=0
    logical :: abort_readerr=.true.
    integer :: ig

    namelist /theta_grid/ z0, ntheta, nperiod

    if (initialized) return
    initialized = .true.

    if (proc0) then

       z0 = 1.0
       ntheta = 24
       nperiod = 1
       in_file = input_unit_exist("theta_grid", exist)
       if (exist) read (unit=in_file, nml=theta_grid, iostat=ireaderr)

    end if

    call broadcast (ireaderr)
    if (abort_readerr) then
       if (ireaderr > 0) then
          call mp_abort ('Read error at theta_grid')
       else if (ireaderr < 0) then
          call mp_abort ('End of file/record occurred at theta_grid')
       endif
    endif

    call broadcast (z0)
    call broadcast (ntheta)
    call broadcast (nperiod)
    
    gradpar = 1./z0  ! Box length is 2*pi*z0 along the equilibrium magnetic field
    jacob = 1.0/gradpar  ! = z0

    ntgrid = ntheta/2 + (nperiod-1)*ntheta
    ! Nz = 2*ntgrid+1 = ntheta * (2*nperiod-1) + 1 for ntheta even
    !                 = ntheta * (2*nperiod-1)     for ntehta odd

    allocate (theta(-ntgrid:ntgrid)); call alloc8(r1=theta,v="theta")
    theta = (/ (real(i)*2.0*pi/real(ntheta), i=-ntgrid,ntgrid) /)

    delthet = theta(1) - theta(0) ! 2pi / ntheta
                                  ! = 2pi / (Nz-1) for ntheta even, nperiod=1

    dz = delthet * jacob ! 2pi * z0 / ntheta
                         ! = 2pi * z0 / (Nz-1) for ntheta even, nperiod=1

    allocate (kpar(2*ntgrid)); call alloc8(r1=kpar,v="kpar")
    do ig = 1, ntgrid
       kpar(ig) = (ig-1)*gradpar/real(2*nperiod-1)
       kpar(2*ntgrid-ig+1)=-(ig)*gradpar/real(2*nperiod-1)
    end do
    
  end subroutine init_theta_grid

end module theta_grid
