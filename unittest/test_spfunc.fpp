program test_spfunc
  use spfunc, only: j0, j1, erf_ext, lgamma_ext
  implicit none
  integer, parameter :: n=500
  real, parameter :: dx=0.01
  integer :: i
  real :: x, y

  x=0.1
  
  write(6,*) 'single j0        ',j0(real(x,4))
  write(6,*) 'double j0        ',j0(real(x,8))
# ifdef QUAD_PRECISION
  write(6,*) '  quad j0        ',j0(real(x,16))
# endif
  
  write(6,*) 'single j1        ',j1(real(x,4))
  write(6,*) 'double j1        ',j1(real(x,8))
# ifdef QUAD_PRECISION
  write(6,*) '  quad j1        ',j1(real(x,16))
# endif
  
  write(6,*) 'single erf_ext   ',erf_ext(real(x,4))
  write(6,*) 'double erf_ext   ',erf_ext(real(x,8))
# ifdef QUAD_PRECISION
  write(6,*) '  quad erf_ext   ',erf_ext(real(x,16))
# endif

  write(6,*) 'single lgamma_ext',lgamma_ext(real(x,4))
  write(6,*) 'double lgamma_ext',lgamma_ext(real(x,8))
# ifdef QUAD_PRECISION
  write(6,*) '  quad lgamma_ext',lgamma_ext(real(x,16))
# endif
  
  open(10,file='test_spfunc.dat',status='unknown')
  do i=0,n
     x=i*dx
     write(10,'(5(e24.16,1x))') x, j0(x), j1(x), erf_ext(x), lgamma_ext(x)
  end do
  close(10)
  
end program test_spfunc
