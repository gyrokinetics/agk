1;

# data from the code
a=load('test_spfunc.dat');
# data from Mathematica
b=load('spfunc.dat');

err(2:4)=sum(abs(a(1:501,2:4)-b(1:501,2:4)),1);
sum_b(2:4)=sum(abs(b(1:501,2:4)),1);
err(5)=sum(abs(a(2:501,5)-b(2:501,5)));
sum_b(5)=sum(abs(b(2:501,5)));

err(2:5)./=sum_b(2:5);

printf("Error of J0       : %10.4e\n",err(2));
printf("Error of J1/x     : %10.4e\n",err(3));
printf("Error of Erf      : %10.4e\n",err(4));
printf("Error of LogGamma : %10.4e\n",err(5));
