# include "define.inc"

module agk_io

# ifdef NETCDF
  use netcdf, only: NF90_NOERR
!  use agk_save, only: kind_nf
  use netcdf_utils, only: kind_nf
# endif

  implicit none

  private

  public :: init_agk_io, nc_final_fields, nc_final_epar
  public :: nc_final_moments, nc_finish
  public :: nc_qflux, nc_vflux, nc_pflux, nc_loop, nc_loop_moments
  public :: nc_loop_fullmom ! RN
  public :: nc_loop_vspec   ! MAB
  public :: nc_loop_movie
  public :: nc_loop_freq    ! JMT
  public :: nc_loop_fullmom2 ! JMT

  logical, parameter :: serial_io = .true.
# ifdef NETCDF
  logical :: proc_write
  integer (kind_nf) :: ncid

  integer (kind_nf) :: naky_dim, nakx_dim, nttot_dim, negrid_dim, nlambda_dim, nspec_dim
  integer (kind_nf) :: nsign_dim, time_dim, char10_dim, char200_dim, ri_dim, nlines_dim, nheat_dim
  integer (kind_nf) :: nesub_dim, nvpa_dim, nvpe_dim  ! MAB

  integer, dimension (5) :: field_dim, final_mom_dim, heatk_dim
  integer, dimension (4) :: omega_dim, fluxk_dim, final_field_dim, loop_mom_dim, vxi_dim, vpavpe_dim
  integer, dimension (3) :: fluxx_dim
  integer, dimension (3) :: mode_dim, phase_dim, loop_phi_dim, heat_dim
  integer, dimension (2) :: kx_dim, ky_dim, om_dim, flux_dim, nin_dim, fmode_dim

  ! added by EAB 03/05/04 for movies
  logical :: my_make_movie
  integer :: ncid_movie
  integer :: nx_dim, ny_dim, nth_dim, time_movie_dim
  integer, dimension (4) :: xmode_dim
  integer :: nx_id, ny_id, nth_id, x_id, y_id, th_id, time_movie_id

!>JMT frequency analysis variables
  logical :: freq_out
  integer :: ncid_freq, nk_freq
  integer :: nakyx_freq_dim, ntheta_freq_dim, time_freq_dim, ri_freq_dim, kykx_freq_dim
  integer :: time_freq_id, akyx_freq_id, theta_freq_id, phi_freq_id, apar_freq_id, bpar_freq_id, epar_freq_id
  integer, dimension(4) :: freq_mode_dim
  integer, dimension(2) :: kykx_mode_dim
  integer, dimension(:,:), allocatable :: kyx_freq
!>JMT full moment output variables
  logical :: write_full_moments
  integer, dimension (6) :: momf_dim
  integer ::  time_fullmom_dim
  integer :: nkx_fullmom_dim, nky_fullmom_dim, ntheta_fullmom_dim, ri_fullmom_dim, nspec_fullmom_dim
  integer :: akx_fullmom_id, aky_fullmom_id, theta_fullmom_id, time_fullmom_id
  integer :: ncid_fullmom, densf0_id
  integer :: uxf0_id, uyf0_id, uzf0_id
  integer :: pxxf0_id, pyyf0_id, pzzf0_id, pxyf0_id, pyzf0_id, pzxf0_id
  integer :: qzzzf0_id, qzppf0_id
!<JMT  
  integer :: density_by_xmode_id
  integer :: phi_by_xmode_id, apar_by_xmode_id, bpar_by_xmode_id
  integer :: code_id_movie

  integer :: nakx_id, naky_id, nttot_id, akx_id, aky_id, theta_id, nspec_id
  integer :: time_id, phi2_id, apar2_id, bpar2_id, nproc_id, nmesh_id
  integer :: phi2_by_mode_id, apar2_by_mode_id, bpar2_by_mode_id
  integer :: phtot_id, dmix_id, kperpnorm_id
  integer :: phi2_by_kx_id, apar2_by_kx_id, bpar2_by_kx_id
  integer :: phi2_by_ky_id, apar2_by_ky_id, bpar2_by_ky_id
  integer :: phi0_id, apar0_id, bpar0_id, sourcefac_id
  integer :: omega_id, omegaavg_id, phase_id, phiavg_id
  integer :: es_heat_flux_id, es_mom_flux_id, es_part_flux_id
  integer :: es_heat_par_id, es_heat_perp_id
  integer :: apar_heat_flux_id, apar_mom_flux_id, apar_part_flux_id
  integer :: apar_heat_par_id, apar_heat_perp_id
  integer :: bpar_heat_flux_id, bpar_mom_flux_id, bpar_part_flux_id
  integer :: bpar_heat_par_id, bpar_heat_perp_id
  integer :: hflux_tot_id, zflux_tot_id, vflux_tot_id
  integer :: es_heat_by_k_id, es_mom_by_k_id, es_part_by_k_id
  integer :: apar_heat_by_k_id, apar_mom_by_k_id, apar_part_by_k_id
  integer :: bpar_heat_by_k_id, bpar_mom_by_k_id, bpar_part_by_k_id
  integer :: phi_t_id, apar_t_id, bpar_t_id
  integer :: phi_norm_id, apar_norm_id, bpar_norm_id
  integer :: phi_id, apar_id, bpar_id, epar_id
  ! final moments
  integer :: ntot_id, density_id, upar_id, tpar_id, tperp_id
  ! nc_loop_moments
  integer :: ntot2_id, ntot2_by_mode_id, ntot20_id, ntot20_by_mode_id
  integer :: phi00_id, ntot00_id, density00_id, upar00_id, tpar00_id, tperp00_id

  integer :: qflux_neo_by_k_id, pflux_neo_by_k_id, input_id
  integer :: charge_id, mass_id, dens_id, temp_id, tprim_id, fprim_id
  integer :: uprim_id, uprim2_id, nu_id, spec_type_id
  integer :: jacob_id, gradpar_id
  integer :: code_id, datestamp_id, timestamp_id, timezone_id
  integer :: h_energy_id, h_energy_dot_id, h_antenna_id
  integer :: h_eapar_id, h_ebpar_id
  integer :: h_delfs2_id, h_hs2_id, h_phis2_id
  integer :: h_collisions_id
  integer :: h_gradients_id, h_hypercoll_id, h_heating_id
  integer :: h_imp_colls_id
  integer :: hk_energy_id, hk_energy_dot_id, hk_antenna_id
  integer :: hk_eapar_id, hk_ebpar_id
  integer :: hk_delfs2_id, hk_hs2_id, hk_phis2_id
  integer :: hk_collisions_id
  integer :: hk_gradients_id, hk_hypercoll_id, hk_heating_id, hk_imp_colls_id

  ! RN >
  integer, dimension (5) :: mom_dim
  integer :: dens0_id
  integer :: ux0_id, uy0_id, uz0_id
  integer :: pxx0_id, pyy0_id, pzz0_id, pxy0_id, pyz0_id, pzx0_id
  ! RN <

  integer :: gvxi_id, gpq_id, gkk_id  ! MAB

  logical :: write_apar_t, write_phi_t, write_bpar_t ! EGH fields written out every nwrite
# endif
  real :: zero

contains

  subroutine init_agk_io (write_nl_flux, write_omega, write_avg_moments, &
       write_full_moments_r, write_full_moments_t, write_vspectrum, &
       write_final_moments, write_hrate, make_movie, nmovie_tot, write_freq, &
       nk_freq_temp, kyx_freq_temp, write_phi_over_time, write_apar_over_time, &
      write_bpar_over_time)

!David has made some changes to this subroutine (may 2005) now should be able to do movies for 
!linear box runs as well as nonlinear box runs.

    use mp, only: proc0, barrier
    use file_utils, only: run_name, error_unit
    use agk_transforms, only: init_transforms
    use kgrids, only: naky, nakx,nx,ny
    use theta_grid, only: ntgrid
    use le_grids, only: nlambda, negrid
    use species, only: nspec
    use constants, only: kind_rs, kind_rd, pi
    use agk_mem, only: alloc8
# ifdef NETCDF
!    use agk_save, only: get_netcdf_code_precision, netcdf_real, netcdf_error
    use netcdf_utils, only: get_netcdf_code_precision, netcdf_real, netcdf_error
    use netcdf, only: NF90_CLOBBER, nf90_create
# endif
    logical, intent (in) :: write_nl_flux, write_omega, write_avg_moments
!    logical, intent (in) :: write_full_moments_r, write_vspectrum, write_kspectrum
    logical, intent (in) :: write_full_moments_r, write_vspectrum
    logical, intent (in) :: write_full_moments_t
    logical, intent (in) :: write_final_moments, write_hrate, make_movie
    integer, intent (in) :: nmovie_tot, nk_freq_temp
    logical, intent (in) :: write_freq
    integer, dimension(:,:), intent(in) :: kyx_freq_temp
    logical, intent(in) ::  write_phi_over_time, write_apar_over_time,  write_bpar_over_time !EGH

# ifdef NETCDF
    logical :: accelerated
    character (300) :: filename, filename_movie, filename_freq, filename_fullmom
    integer :: status

    !EGH
    write_phi_t = write_phi_over_time
    write_apar_t = write_apar_over_time
    write_bpar_t = write_bpar_over_time
    zero = epsilon(0.0)

    call netcdf_init (serial_io)
    if (netcdf_real == 0) netcdf_real = get_netcdf_code_precision()

    status = NF90_NOERR

    filename = trim(trim(run_name)//'.out.nc')
    if (serial_io) then
       ! only proc0 opens the file:
       if (proc0) then
          status = nf90_create (trim(filename), NF90_CLOBBER, ncid)
          if (status /= NF90_NOERR) call netcdf_error (status, file=filename)
       end if
    else
       ! all processors open the file
       call barrier
!       status = nf_create(trim(filename), 0, ncid) ! overwrites old
       ! TT: do we want NF90_SHARE?
       status = nf90_create(trim(filename), NF90_CLOBBER, ncid)
       if (proc0 .and. (status /= NF90_NOERR)) &
            call netcdf_error (status, file=filename)
       call barrier
    endif

!>JMT
    freq_out = write_freq
    if (freq_out) then
       filename_freq = trim(trim(run_name)//'.freq.nc')
       if (serial_io) then
          ! only proc0 opens the file:
          if (proc0) then
             status = nf90_create (trim(filename_freq), NF90_CLOBBER, ncid_freq)
             if (status /= NF90_NOERR) call netcdf_error (status, file=filename_freq)
          end if
       else
          ! all processors open the file
          call barrier
          status = nf90_create(trim(filename_freq), NF90_CLOBBER, ncid_freq)
          if (proc0 .and. (status /= NF90_NOERR)) &
               call netcdf_error (status, file=filename_freq)
          call barrier
       endif
       if (proc0) then
          nk_freq = nk_freq_temp
          allocate (kyx_freq(nk_freq,2)); call alloc8(i2=kyx_freq,v="kyx_freq")
          kyx_freq = kyx_freq_temp
       endif
    endif

    write_full_moments = write_full_moments_t
    if (write_full_moments) then
       filename_fullmom = trim(trim(run_name)//'.fullmom.nc')
       if (serial_io) then
          ! only proc0 opens the file:
          if (proc0) then
             status = nf90_create (trim(filename_fullmom), NF90_CLOBBER, ncid_fullmom)
             if (status /= NF90_NOERR) call netcdf_error (status, file=filename_fullmom)
          end if
       else
          ! all processors open the file
          call barrier
          status = nf90_create(trim(filename_fullmom), NF90_CLOBBER, ncid_fullmom)
          if (proc0 .and. (status /= NF90_NOERR)) &
               call netcdf_error (status, file=filename_fullmom)
          call barrier
       endif
    endif

!<JMT

    ! added by EAB 03/2004 for making movie netcdf files
    my_make_movie = make_movie
    if (my_make_movie) then
    !perhaps put a call to init_y_transform
!    call init_y_transform_layouts  
       call init_transforms(ntgrid, naky, nakx, nlambda, negrid, nspec, nx, ny, accelerated)
       filename_movie = trim(trim(run_name)//'.movie.nc')
       if (serial_io) then
          ! only proc0 opens the file:
          if (proc0) then
             status = nf90_create(trim(filename_movie), NF90_CLOBBER, ncid_movie) 
             if (status /= NF90_NOERR) &
                  call netcdf_error (status, file=filename_movie)
          end if
       else
          ! all processors open the file
          call barrier
          status = nf90_create(trim(filename_movie), NF90_CLOBBER, ncid_movie) ! overwrites old
          if (proc0 .and. (status /= NF90_NOERR)) &
               call netcdf_error (status, file=filename_movie)
          call barrier
       end if
    endif
# endif
    if (proc0) then
       call define_dims (write_vspectrum, nmovie_tot)
       call define_vars (write_nl_flux, write_omega, write_avg_moments, &
!            write_full_moments_r, write_vspectrum, write_kspectrum, &
            write_full_moments_r, write_vspectrum, &
            write_final_moments, write_hrate)
       call nc_grids
       call nc_species
       call nc_geo
       call nc_param
    end if

  end subroutine init_agk_io

  subroutine define_dims (write_vspectrum, nmovie_tot)
    use file_utils, only: num_input_lines
    use kgrids, only: naky, nakx
    use agk_layouts, only: yxf_lo    
    use theta_grid, only: ntgrid
    use le_grids, only: nlambda, negrid, nesub, nvpa, nvpe
    use species, only: nspec
# ifdef NETCDF
!    use agk_save, only: netcdf_error
    use netcdf_utils, only: netcdf_error
    use netcdf, only: nf90_def_dim, NF90_UNLIMITED, NF90_NOERR
# endif

    logical, intent (in) :: write_vspectrum
    integer, intent (in) :: nmovie_tot
# ifdef NETCDF
    integer :: status, nx, ny

    status = nf90_def_dim (ncid, 'ky', naky, naky_dim)
    if (status /= NF90_NOERR) call netcdf_error (status, dim='ky')
    status = nf90_def_dim (ncid, 'kx', nakx, nakx_dim)
    if (status /= NF90_NOERR) call netcdf_error (status, dim='kx')
    status = nf90_def_dim (ncid, 'theta', 2*ntgrid+1, nttot_dim)
    if (status /= NF90_NOERR) call netcdf_error (status, dim='theta')
    status = nf90_def_dim (ncid, 'lambda', nlambda, nlambda_dim)
    if (status /= NF90_NOERR) call netcdf_error (status, dim='lambda')
    status = nf90_def_dim (ncid, 'egrid', negrid, negrid_dim)
    if (status /= NF90_NOERR) call netcdf_error (status, dim='egrid')
    status = nf90_def_dim (ncid, 'species', nspec, nspec_dim)
    if (status /= NF90_NOERR) call netcdf_error (status, dim='species')
    status = nf90_def_dim (ncid, 'sign', 2, nsign_dim)
    if (status /= NF90_NOERR) call netcdf_error (status, dim='sign')
    status = nf90_def_dim (ncid, 't', NF90_UNLIMITED, time_dim)
    if (status /= NF90_NOERR) call netcdf_error (status, dim='t')
    status = nf90_def_dim (ncid, 'char10', 10, char10_dim)
    if (status /= NF90_NOERR) call netcdf_error (status, dim='char10')
    status = nf90_def_dim (ncid, 'char200', 200, char200_dim)
    if (status /= NF90_NOERR) call netcdf_error (status, dim='char200')
    status = nf90_def_dim (ncid, 'nlines', num_input_lines, nlines_dim)
    if (status /= NF90_NOERR) call netcdf_error (status, dim='nlines')
    status = nf90_def_dim (ncid, 'ri', 2, ri_dim)
    if (status /= NF90_NOERR) call netcdf_error (status, dim='ri')
    status = nf90_def_dim (ncid, 'nheat', 7, nheat_dim)
    if (status /= NF90_NOERR) call netcdf_error (status, dim='nheat')
! MAB>
!  ** RN> if nesub=0 (=NF90_UNLIMITED?),
!         I get 'nc_unlimited size already in use' error **
    status = nf90_def_dim (ncid, 'esub', nesub, nesub_dim)
    if (status /= NF90_NOERR) call netcdf_error (status, dim='esub')
    if (write_vspectrum) then
       status = nf90_def_dim (ncid, 'pvpe', nvpe, nvpe_dim)
       if (status /= NF90_NOERR) call netcdf_error (status, dim='pvpe')
       status = nf90_def_dim (ncid, 'qvpa', nvpa, nvpa_dim)
       if (status /= NF90_NOERR) call netcdf_error (status, dim='qvpa')
    end if
! <MAB

!>JMT
    if (freq_out) then
       status = nf90_def_dim (ncid_freq, 'kykx_modes', nk_freq, nakyx_freq_dim)
       if (status /= NF90_NOERR) call netcdf_error (status, dim='kykx_modes')
       status = nf90_def_dim (ncid_freq, 'kykx_dim', 2, kykx_freq_dim)
       if (status /= NF90_NOERR) call netcdf_error (status, dim='kykx_dim')
       status = nf90_def_dim (ncid_freq, 'theta', 2*ntgrid+1, ntheta_freq_dim)
       if (status /= NF90_NOERR) call netcdf_error (status, dim='theta')
       status = nf90_def_dim (ncid_freq, 't', NF90_UNLIMITED, time_freq_dim)
       if (status /= NF90_NOERR) call netcdf_error (status, dim='t')
       status = nf90_def_dim (ncid_freq, 'ri', 2, ri_freq_dim)
       if (status /= NF90_NOERR) call netcdf_error (status, dim='ri')
    endif

    if (write_full_moments) then
       status = nf90_def_dim (ncid_fullmom, 'nt', NF90_UNLIMITED, time_fullmom_dim)
       if (status /= NF90_NOERR) call netcdf_error (status, dim='t')
       status = nf90_def_dim (ncid_fullmom, 'nkx', nakx, nkx_fullmom_dim)
       if (status /= NF90_NOERR) call netcdf_error (status, dim='kx')
       status = nf90_def_dim (ncid_fullmom, 'nky', naky, nky_fullmom_dim)
       if (status /= NF90_NOERR) call netcdf_error (status, dim='ky')
       status = nf90_def_dim (ncid_fullmom, 'ntheta', 2*ntgrid+1, ntheta_fullmom_dim)
       if (status /= NF90_NOERR) call netcdf_error (status, dim='theta')
       status = nf90_def_dim (ncid_fullmom, 'ri', 2, ri_fullmom_dim)
       if (status /= NF90_NOERR) call netcdf_error (status, dim='ri')
       status = nf90_def_dim (ncid_fullmom, 'nspec', nspec, nspec_fullmom_dim)
       if (status /= NF90_NOERR) call netcdf_error (status, dim='nspec')
    endif

!<JMT 

    ! added by EAB 03/05/04 for movies
    if (my_make_movie) then
       nx=yxf_lo%nx
       ny=yxf_lo%ny
       status = nf90_def_dim (ncid_movie, 'x', nx, nx_dim)
       if (status /= NF90_NOERR) call netcdf_error (status, dim='x')
       status = nf90_def_dim (ncid_movie, 'y', ny, ny_dim)
       if (status /= NF90_NOERR) call netcdf_error (status, dim='y')
       status = nf90_def_dim (ncid_movie, 'theta', 2*ntgrid+1, nth_dim)
       if (status /= NF90_NOERR) call netcdf_error (status, dim='theta')
!
! can only have one NF_UNLIMITED, so:
!
!       status = netcdf_def_dim(ncid_movie, 't', NF_UNLIMITED, time_movie_dim)
       status = nf90_def_dim (ncid_movie, 'tm', nmovie_tot, time_movie_dim)
       if (status /= NF90_NOERR) call netcdf_error (status, dim='tm')
    endif



# endif
  end subroutine define_dims

  subroutine nc_grids
    ! TT: called only for proc0
    use theta_grid, only: ntgrid, theta
    use kgrids, only: naky, nakx, akx, aky, nx, ny
    use agk_layouts, only: yxf_lo
    use species, only: nspec
    use le_grids, only: negrid, nlambda
    use nonlinear_terms, only: nonlin
# ifdef NETCDF
    use netcdf, only: NF90_NOERR, nf90_put_var
!    use agk_save, only: netcdf_error
    use netcdf_utils, only: netcdf_error
    use constants, only: pi, kind_id
    use agk_mem, only: alloc8, dealloc8

    integer :: status
    real :: nmesh

    real, dimension(:), allocatable :: x, y
    real, dimension(1:2,1:nk_freq) :: akyx_freq
    integer :: ik, it

    status = nf90_put_var (ncid, nttot_id, 2*ntgrid+1)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, nttot_id)
    status = nf90_put_var (ncid, naky_id, naky)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, naky_id)
    status = nf90_put_var (ncid, nakx_id, nakx)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, nakx_id)
    status = nf90_put_var (ncid, nspec_id, nspec)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, nspec_id)

    status = nf90_put_var (ncid, akx_id, akx)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, akx_id)
    status = nf90_put_var (ncid, aky_id, aky)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, aky_id)
    status = nf90_put_var (ncid, theta_id, theta)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, theta_id)

    if (nonlin) then
       nmesh = int(2*ntgrid+1,kind_id)*2*nlambda*negrid*nx*ny*nspec
    else
       nmesh = int(2*ntgrid+1,kind_id)*2*nlambda*negrid*nakx*naky*nspec
    end if

    status = nf90_put_var (ncid, nmesh_id, nmesh)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, nmesh_id)

    ! added by EAB 03/05/04 for movies
    if(my_make_movie) then

       allocate(x(yxf_lo%nx)); call alloc8(r1=x,v="x")
       allocate(y(yxf_lo%ny)); call alloc8(r1=y,v="y")
       do it = 1, yxf_lo%nx 
          x(it) = 2.0*pi/akx(2)*(-0.5+ real(it-1)/real(yxf_lo%nx))
       end do
       do ik = 1, yxf_lo%ny
          y(ik) = 2.0*pi/aky(2)*(-0.5+ real(ik-1)/real(yxf_lo%ny))
       end do
       
       status = nf90_put_var (ncid_movie, nx_id, yxf_lo%nx)
       if (status /= NF90_NOERR) &
            call netcdf_error (status, ncid_movie, nx_id)
       status = nf90_put_var (ncid_movie, ny_id, yxf_lo%ny)
       if (status /= NF90_NOERR) &
            call netcdf_error (status, ncid_movie, ny_id)
       status = nf90_put_var (ncid_movie, nth_id, 2*ntgrid+1)
       if (status /= NF90_NOERR) &
            call netcdf_error (status, ncid_movie, nth_id)
       status = nf90_put_var (ncid_movie, x_id, x)
       if (status /= NF90_NOERR) &
            call netcdf_error (status, ncid_movie, x_id)
       status = nf90_put_var (ncid_movie, y_id, y)
       if (status /= NF90_NOERR) &
            call netcdf_error (status, ncid_movie, y_id)
       status = nf90_put_var (ncid_movie, th_id, theta)
       if (status /= NF90_NOERR) &
            call netcdf_error (status, ncid_movie, th_id)
!       deallocate(x)
       call dealloc8(r1=x,v="x")
!       deallocate(y)
       call dealloc8(r1=y,v="y")
    endif

!>JMT
    if (freq_out) then
       do ik=1,nk_freq
          akyx_freq(1,ik) = aky(kyx_freq(ik,1))
          akyx_freq(2,ik) = akx(kyx_freq(ik,2))
       enddo
       status = nf90_put_var (ncid_freq, akyx_freq_id, akyx_freq)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid_freq, akyx_freq_id)
       status = nf90_put_var (ncid_freq, theta_freq_id, theta)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid_freq, theta_freq_id)
    endif
    
    if (write_full_moments) then
       status = nf90_put_var (ncid_fullmom, theta_fullmom_id, theta)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid_fullmom, theta_fullmom_id)
       status = nf90_put_var (ncid_fullmom, akx_fullmom_id, akx)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid_fullmom, akx_fullmom_id)
       status = nf90_put_var (ncid_fullmom, aky_fullmom_id, aky)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid_fullmom, aky_fullmom_id)
    endif
!<JMT
# endif
  end subroutine nc_grids

  subroutine netcdf_init (serial_io2)
    use mp, only: proc0
    logical, intent(in) :: serial_io2

# ifdef NETCDF
!!$    if (serial_io2) then
!!$       ! only proc0 will write
!!$       proc_write=proc0
!!$    else
!!$       ! all processors will write
!!$       proc_write=.true.
!!$    endif
    proc_write = proc0 .or. (.not.serial_io2)
# endif

  end subroutine netcdf_init

  subroutine nc_finish
    use mp, only: proc0
    use agk_mem, only: dealloc8
# ifdef NETCDF
    use netcdf, only: nf90_close
!    use agk_save, only: netcdf_error
    use netcdf_utils, only: netcdf_error
    integer :: status

    if (proc0) then
       call save_input
       status = nf90_close (ncid)
       if (status /= NF90_NOERR) call netcdf_error (status)
       if (my_make_movie) then
          status = nf90_close (ncid_movie)
          if (status /= NF90_NOERR) call netcdf_error (status)
       endif
!>JMT       
       if (freq_out) then
          status = nf90_close (ncid_freq)
          if (status /= NF90_NOERR) call netcdf_error (status)
          call dealloc8(i2=kyx_freq,v="kyx_freq")
       endif

       if (write_full_moments) then
          status = nf90_close(ncid_fullmom)
          if (status /= NF90_NOERR) call netcdf_error (status)
       endif
!<JMT
    end if
# endif
  end subroutine nc_finish

  subroutine save_input
    ! TT: called only for proc0
# ifdef NETCDF
    use file_utils, only: num_input_lines, get_input_unit
    use netcdf, only: nf90_put_var
    use netcdf_utils, only: netcdf_error

    character(200) line
    integer, dimension (2) :: nin_start, nin_count

    integer :: status, n, unit

    nin_start(1) = 1
    nin_start(2) = 1

    nin_count(2) = 1

    call get_input_unit (unit)
    rewind (unit=unit)
    do n = 1, num_input_lines
       read (unit=unit, fmt="(a)") line
       nin_count(1) = len(trim(line))
!       status = nf_put_vara_text (ncid, input_id, nin_start, nin_count, line)
       status = nf90_put_var (ncid, input_id, line, start=nin_start, count=nin_count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, input_id)
       nin_start(2) = nin_start(2) + 1
    end do
# endif
  end subroutine save_input

  subroutine define_vars (write_nl_flux, write_omega, write_avg_moments, &
       write_full_moments_r, write_vspectrum, &
       write_final_moments, write_hrate)

    ! TT: called only for proc0
    use mp, only: nproc
    use kgrids, only: naky, nakx
    use run_parameters, only: use_Phi, use_Apar, use_Bpar
# ifdef NETCDF
!    use agk_save, only: netcdf_real, netcdf_error
    use netcdf_utils, only: netcdf_real, netcdf_error
    use netcdf, only: NF90_CHAR, NF90_INT, NF90_GLOBAL
    use netcdf, only: nf90_def_var, nf90_put_att, nf90_inq_libvers, nf90_enddef, nf90_put_var
# endif
    logical, intent (in) :: write_nl_flux, write_omega, write_avg_moments
!    logical, intent (in) :: write_full_moments_r, write_vspectrum, write_kspectrum
    logical, intent (in) :: write_full_moments_r, write_vspectrum
    logical, intent (in) :: write_final_moments, write_hrate
# ifdef NETCDF
    character (5) :: ci
    character (20) :: datestamp, timestamp, timezone
    logical :: d_neo = .false.
    
    integer :: status

    fmode_dim(1) = nakx_dim
    fmode_dim(2) = naky_dim

    mode_dim (1) = nakx_dim
    mode_dim (2) = naky_dim
    mode_dim (3) = time_dim

    kx_dim (1) = nakx_dim
    kx_dim (2) = time_dim
    
    ky_dim (1) = naky_dim
    ky_dim (2) = time_dim
    
    om_dim (1) = ri_dim
    om_dim (2) = time_dim

    omega_dim (1) = ri_dim
    omega_dim (2) = nakx_dim
    omega_dim (3) = naky_dim
    omega_dim (4) = time_dim

    ! RN
    mom_dim(1) = ri_dim
    mom_dim(2) = nakx_dim
    mom_dim(3) = naky_dim
    mom_dim(4) = nspec_dim
    mom_dim(5) = time_dim
    ! RN

    phase_dim (1) = ri_dim
    phase_dim (2) = nakx_dim
    phase_dim (3) = naky_dim
    
    nin_dim(1) = char200_dim
    nin_dim(2) = nlines_dim
    
    flux_dim (1) = nspec_dim
    flux_dim (2) = time_dim

    fluxk_dim (1) = nakx_dim
    fluxk_dim (2) = naky_dim
    fluxk_dim (3) = nspec_dim
    fluxk_dim (4) = time_dim

    fluxx_dim (1) = nakx_dim
    fluxx_dim (2) = nspec_dim
    fluxx_dim (3) = time_dim

    heat_dim (1) = nspec_dim
    heat_dim (2) = nheat_dim
    heat_dim (3) = time_dim

    heatk_dim (1) = nakx_dim
    heatk_dim (2) = naky_dim
    heatk_dim (3) = nspec_dim
    heatk_dim (4) = nheat_dim
    heatk_dim (5) = time_dim

    field_dim (1) = ri_dim
    field_dim (2) = nttot_dim
    field_dim (3) = nakx_dim
    field_dim (4) = naky_dim
    field_dim (5) = time_dim
    
    final_field_dim (1) = ri_dim
    final_field_dim (2) = nttot_dim
    final_field_dim (3) = nakx_dim
    final_field_dim (4) = naky_dim

    final_mom_dim (1) = ri_dim
    final_mom_dim (2) = nttot_dim
    final_mom_dim (3) = nakx_dim
    final_mom_dim (4) = naky_dim
    final_mom_dim (5) = nspec_dim

    loop_mom_dim (1) = ri_dim
    loop_mom_dim (2) = nakx_dim
    loop_mom_dim (3) = nspec_dim
    loop_mom_dim (4) = time_dim

    loop_phi_dim (1) = ri_dim
    loop_phi_dim (2) = nakx_dim
    loop_phi_dim (3) = time_dim

    vxi_dim (1) = nlambda_dim
    vxi_dim (2) = nesub_dim
    vxi_dim (3) = nspec_dim
    vxi_dim (4) = time_dim

    vpavpe_dim (1) = nvpe_dim
    vpavpe_dim (2) = nvpa_dim
    vpavpe_dim (3) = nspec_dim
    vpavpe_dim (4) = time_dim

    status = nf90_put_att (ncid, NF90_GLOBAL, 'title', 'AstroGK Simulation Data')
!    if (status /= NF90_NOERR) call netcdf_error (status)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, NF90_GLOBAL, att='title')

    datestamp(:) = ' '
    timestamp(:) = ' '
    timezone(:) = ' '
    call date_and_time (datestamp, timestamp, timezone)

    status = nf90_def_var (ncid, 'code_info', NF90_CHAR, char10_dim, code_id)
    if (status /= NF90_NOERR) call netcdf_error (status, var='code_info')
    status = nf90_put_att (ncid, code_id, 'long_name', 'AstroGK')
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, code_id, att='long_name')

# define _GIT_HASH_ GIT_HASH
    status = nf90_put_att (ncid, code_id, 'revision', _GIT_HASH_)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, code_id, att=ci)
    
    ci = 'c1'
    status = nf90_put_att (ncid, code_id, trim(ci), 'Date: '//trim(datestamp))
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, code_id, att=ci)

    ci = 'c2'
    status = nf90_put_att (ncid, code_id, trim(ci), &
         'Time: '//trim(timestamp)//' '//trim(timezone))
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, code_id, att=ci)

    ci = 'c3'
    status = nf90_put_att (ncid, code_id, trim(ci), &
         'netCDF version '//trim(nf90_inq_libvers()))
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, code_id, att=ci)

    ci = 'c4'
    status = nf90_put_att (ncid, code_id, trim(ci), &
         'Units are determined with respect to reference temperature (T_ref),')
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, code_id, att=ci)

    ci = 'c5'
    status = nf90_put_att (ncid, code_id, trim(ci), &
         'reference charge (q_ref), reference mass (mass_ref),')
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, code_id, att=ci)

    ci = 'c6'
    status = nf90_put_att (ncid, code_id, trim(ci), &
         'reference field (B_ref), and reference length (a_ref)')
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, code_id, att=ci)

    ci = 'c7'
    status = nf90_put_att (ncid, code_id, trim(ci), &
         'from which one may construct rho_ref and vt_ref/a,')
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, code_id, att=ci)

    ci = 'c8'
    status = nf90_put_att (ncid, code_id, trim(ci), &
         'which are the basic units of perpendicular length and time.')
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, code_id, att=ci)

    status = nf90_def_var (ncid, 'nproc', NF90_INT, nproc_id)
    if (status /= NF90_NOERR) call netcdf_error (status, var='nproc')
    status = nf90_put_att (ncid, nproc_id, 'long_name', 'Number of processors')
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, nproc_id, att='long_name')

    status = nf90_def_var (ncid, 'nmesh', netcdf_real, nmesh_id)
    if (status /= NF90_NOERR) call netcdf_error (status, var='nmesh')
    status = nf90_put_att (ncid, nmesh_id, 'long_name', 'Number of meshpoints')
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, nmesh_id, att='long_name')

    status = nf90_def_var (ncid, 'nkx', NF90_INT, nakx_id)
    if (status /= NF90_NOERR) call netcdf_error (status, var='nkx')
    status = nf90_def_var (ncid, 'nky', NF90_INT, naky_id)
    if (status /= NF90_NOERR) call netcdf_error (status, var='nky')
    status = nf90_def_var (ncid, 'ntheta_tot', NF90_INT, nttot_id)
    if (status /= NF90_NOERR) call netcdf_error (status, var='ntheta_tot')
    status = nf90_def_var (ncid, 'nspecies', NF90_INT, nspec_id)
    if (status /= NF90_NOERR) call netcdf_error (status, var='nspecies')

    status = nf90_def_var (ncid, 't', netcdf_real, time_dim, time_id)
    if (status /= NF90_NOERR) call netcdf_error (status, var='t')
    status = nf90_put_att (ncid, time_id, 'long_name', 'Time')
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, time_id, att='long_name')
    status = nf90_put_att (ncid, time_id, 'units', 'L/vt')
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, time_id, att='units')

    status = nf90_def_var (ncid, 'charge', netcdf_real, nspec_dim, charge_id)
    if (status /= NF90_NOERR) call netcdf_error (status, var='charge')
    status = nf90_put_att (ncid, charge_id, 'long_name', 'Charge')
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, charge_id, att='long_name')
    status = nf90_put_att (ncid, charge_id, 'units', 'q')
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, charge_id, att='units')

    status = nf90_def_var (ncid, 'mass', netcdf_real, nspec_dim, mass_id)
    if (status /= NF90_NOERR) call netcdf_error (status, var='mass')
    status = nf90_put_att (ncid, mass_id, 'long_name', 'Atomic mass')
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, mass_id, att='long_name')
    status = nf90_put_att (ncid, mass_id, 'units', 'm')
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, mass_id, att='units')

    status = nf90_def_var (ncid, 'dens', netcdf_real, nspec_dim, dens_id)
    if (status /= NF90_NOERR) call netcdf_error (status, var='dens')
    status = nf90_put_att (ncid, dens_id, 'long_name', 'Density')
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, dens_id, att='long_name')
    status = nf90_put_att (ncid, dens_id, 'units', 'n_e')
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, dens_id, att='units')

    status = nf90_def_var (ncid, 'temp', netcdf_real, nspec_dim, temp_id)
    if (status /= NF90_NOERR) call netcdf_error (status, var='temp')
    status = nf90_put_att (ncid, temp_id, 'long_name', 'Temperature')
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, temp_id, att='long_name')
    status = nf90_put_att (ncid, temp_id, 'units', 'T')
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, temp_id, att='units')

    status = nf90_def_var (ncid, 'tprim', netcdf_real, nspec_dim, tprim_id)
    if (status /= NF90_NOERR) call netcdf_error (status, var='tprim')
    status = nf90_put_att (ncid, tprim_id, 'long_name', '-1/rho dT/drho')
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, tprim_id, att='long_name')

    status = nf90_def_var (ncid, 'fprim', netcdf_real, nspec_dim, fprim_id) 
    if (status /= NF90_NOERR) call netcdf_error (status, var='fprim')
    status = nf90_put_att (ncid, fprim_id, 'long_name', '-1/rho dn/drho')
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, fprim_id, att='long_name')

    status = nf90_def_var (ncid, 'uprim', netcdf_real, nspec_dim, uprim_id)
    if (status /= NF90_NOERR) call netcdf_error (status, var='uprim')
    status = nf90_put_att (ncid, uprim_id, 'long_name', '-1/v_t du_par/drho')
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, uprim_id, att='long_name')

    status = nf90_def_var (ncid, 'uprim2', netcdf_real, nspec_dim, uprim2_id)
    if (status /= NF90_NOERR) call netcdf_error (status, var='uprim2')

    status = nf90_def_var (ncid, 'nu', netcdf_real, nspec_dim, nu_id)
    if (status /= NF90_NOERR) call netcdf_error (status, var='nu')
    status = nf90_put_att (ncid, nu_id, 'long_name', 'Collisionality')
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, nu_id, att='long_name')
    status = nf90_put_att (ncid, nu_id, 'units', 'v_t/L')
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, nu_id, att='units')
    
    status = nf90_def_var (ncid, 'type_of_species', NF90_INT, nspec_dim, spec_type_id)
    if (status /= NF90_NOERR) call netcdf_error (status, var='type_of_species')

    status = nf90_def_var (ncid, 'kx', netcdf_real, nakx_dim, akx_id)
    if (status /= NF90_NOERR) call netcdf_error (status, var='kx')
    status = nf90_put_att (ncid, akx_id, 'long_name', 'kx rho')
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, akx_id, att='long_name')

    status = nf90_def_var (ncid, 'ky', netcdf_real, naky_dim, aky_id)
    if (status /= NF90_NOERR) call netcdf_error (status, var='ky')
    status = nf90_put_att (ncid, aky_id, 'long_name', 'ky rho')
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, aky_id, att='long_name')

    status = nf90_def_var (ncid, 'theta', netcdf_real, nttot_dim, theta_id)
    if (status /= NF90_NOERR) call netcdf_error (status, var='theta')

    status = nf90_def_var (ncid, 'gradpar', netcdf_real, gradpar_id)
    if (status /= NF90_NOERR) call netcdf_error (status, var='gradpar')
    status = nf90_def_var (ncid, 'jacob', netcdf_real, jacob_id)
    if (status /= NF90_NOERR) call netcdf_error (status, var='jacob')

    if (use_Phi) then
       status = nf90_def_var (ncid, 'phi2', netcdf_real, time_dim, phi2_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='phi2')
       status = nf90_put_att (ncid, phi2_id, 'long_name', '|Potential**2|')
       if (status /= NF90_NOERR) &
            call netcdf_error (status, ncid, phi2_id, att='long_name')
       status = nf90_put_att (ncid, phi2_id, 'units', '(T/q rho/L)**2')
       if (status /= NF90_NOERR) &
            call netcdf_error (status, ncid, phi2_id, att='units')

       status = nf90_def_var &
            (ncid, 'phi2_by_mode', netcdf_real, mode_dim, phi2_by_mode_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='phi2_by_mode')
       if (nakx > 1) then
          status = nf90_def_var &
               (ncid, 'phi2_by_kx', netcdf_real, kx_dim, phi2_by_kx_id)
          if (status /= NF90_NOERR) &
               call netcdf_error (status, var='phi2_by_kx')
       end if

       if (naky > 1) then
          status = nf90_def_var &
               (ncid, 'phi2_by_ky', netcdf_real, ky_dim, phi2_by_ky_id)
          if (status /= NF90_NOERR) &
               call netcdf_error (status, var='phi2_by_ky')
       end if

       status = nf90_def_var (ncid, 'phi0', netcdf_real, omega_dim, phi0_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='phi0')

       if (write_nl_flux) then
          status = nf90_def_var &
               (ncid, 'es_heat_par',  netcdf_real, flux_dim, es_heat_par_id)
          if (status /= NF90_NOERR) &
               call netcdf_error (status, var='es_heat_par')
          status = nf90_def_var &
               (ncid, 'es_heat_perp', netcdf_real, flux_dim, es_heat_perp_id)
          if (status /= NF90_NOERR) &
               call netcdf_error (status, var='es_heat_perp')
          status = nf90_def_var &
               (ncid, 'es_heat_flux', netcdf_real, flux_dim, es_heat_flux_id)
          if (status /= NF90_NOERR) &
               call netcdf_error (status, var='es_heat_flux')
          status = nf90_def_var &
               (ncid, 'es_mom_flux',  netcdf_real, flux_dim, es_mom_flux_id)
          if (status /= NF90_NOERR) &
               call netcdf_error (status, var='es_mom_flux')
          status = nf90_def_var &
               (ncid, 'es_part_flux', netcdf_real, flux_dim, es_part_flux_id)
          if (status /= NF90_NOERR) &
               call netcdf_error (status, var='es_part_flux')
          status = nf90_def_var &
               (ncid, 'es_heat_by_k', netcdf_real, fluxk_dim, es_heat_by_k_id)
          if (status /= NF90_NOERR) &
               call netcdf_error (status, var='es_heat_by_k')
          status = nf90_def_var &
               (ncid, 'es_mom_by_k',  netcdf_real, fluxk_dim, es_mom_by_k_id)
          if (status /= NF90_NOERR) &
               call netcdf_error (status, var='es_mom_by_k')
          status = nf90_def_var &
               (ncid, 'es_part_by_k', netcdf_real, fluxk_dim, es_part_by_k_id)
          if (status /= NF90_NOERR) &
               call netcdf_error (status, var='es_part_by_k')
       end if
       status = nf90_def_var &
            (ncid, 'phi', netcdf_real, final_field_dim, phi_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='phi')
       status = nf90_put_att &
            (ncid, phi_id, 'long_name', 'Electrostatic Potential')
       if (status /= NF90_NOERR) &
            call netcdf_error (status, ncid, phi_id, att='long_name')
       status = nf90_put_att (ncid, phi_id, 'idl_name', '!7U!6')
       if (status /= NF90_NOERR) &
            call netcdf_error (status, ncid, phi_id, att='idl_name')
       status = nf90_put_att (ncid, phi_id, 'units', 'T/q rho/L')
       if (status /= NF90_NOERR) &
            call netcdf_error (status, ncid, phi_id, att='units')

       if (write_phi_t) then
          status = nf90_def_var &
               (ncid, 'phi_t', netcdf_real, field_dim, phi_t_id)
          if (status /= NF90_NOERR) call netcdf_error (status, var='phi_t')
          status = nf90_put_att (ncid, phi_t_id, 'long_name', 'Electrostatic Potential over time')
          if (status /= NF90_NOERR) call netcdf_error (status, ncid, phi_t_id, att='long_name')
       end if
       !if (d_fields_per) then
          !status = nf90_def_var &
               !(ncid, 'phi_t', netcdf_real, field_dim, phi_t_id)
          !if (status /= NF90_NOERR) call netcdf_error (status, var='phi_t')
       !end if
    end if

    if (use_Apar) then
       status = nf90_def_var (ncid, 'apar2', netcdf_real, time_dim, apar2_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='apar2')
       status = nf90_def_var &
            (ncid, 'apar2_by_mode', netcdf_real, mode_dim, apar2_by_mode_id)
       if (status /= NF90_NOERR) &
            call netcdf_error (status, var='apar2_by_mode')
       if (nakx > 1) then
          status = nf90_def_var &
               (ncid, 'apar2_by_kx', netcdf_real, kx_dim, apar2_by_kx_id)
          if (status /= NF90_NOERR) &
               call netcdf_error (status, var='apar2_by_kx')
       end if
       if (naky > 1) then
          status = nf90_def_var &
               (ncid, 'apar2_by_ky', netcdf_real, ky_dim, apar2_by_ky_id)
          if (status /= NF90_NOERR) &
               call netcdf_error (status, var='apar2_by_ky')
       end if

       status = nf90_def_var (ncid, 'apar0', netcdf_real, omega_dim, apar0_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='apar0')
       if (write_nl_flux) then
          status = nf90_def_var &
               (ncid,'apar_heat_flux',netcdf_real, flux_dim, apar_heat_flux_id)
          if (status /= NF90_NOERR) &
               call netcdf_error (status, var='apar_heat_flux')
          status = nf90_def_var &
               (ncid, 'apar_heat_par', netcdf_real, flux_dim, apar_heat_par_id)
          if (status /= NF90_NOERR) call netcdf_error (status, var='apar_heat_par')
          status = nf90_def_var (ncid, 'apar_heat_perp', netcdf_real, flux_dim, apar_heat_perp_id)
          if (status /= NF90_NOERR) call netcdf_error (status, var='apar_heat_perp')
          status = nf90_def_var (ncid, 'apar_mom_flux',  netcdf_real, flux_dim, apar_mom_flux_id)
          if (status /= NF90_NOERR) call netcdf_error (status, var='apar_mom_flux')
          status = nf90_def_var (ncid, 'apar_part_flux', netcdf_real, flux_dim, apar_part_flux_id)
          if (status /= NF90_NOERR) call netcdf_error (status, var='apar_part_flux')
          status = nf90_def_var (ncid, 'apar_heat_by_k', netcdf_real, fluxk_dim, apar_heat_by_k_id)
          if (status /= NF90_NOERR) call netcdf_error (status, var='apar_heat_by_k')
          status = nf90_def_var (ncid, 'apar_mom_by_k',  netcdf_real, fluxk_dim, apar_mom_by_k_id)
          if (status /= NF90_NOERR) call netcdf_error (status, var='apar_mom_by_k')
          status = nf90_def_var (ncid, 'apar_part_by_k', netcdf_real, fluxk_dim, apar_part_by_k_id)
          if (status /= NF90_NOERR) call netcdf_error (status, var='apar_part_by_k')
       end if
       status = nf90_def_var (ncid, 'apar', netcdf_real, final_field_dim, apar_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='apar')

       !if (d_fields_per) then
        !  status = nf90_def_var (ncid, 'apar_t', netcdf_real, field_dim, apar_t_id)
         ! if (status /= NF90_NOERR) call netcdf_error (status, var='apar_t')
       !end if

       if (write_apar_t) then
          status = nf90_def_var (ncid, 'apar_t', netcdf_real, field_dim, apar_t_id)
          if (status /= NF90_NOERR) call netcdf_error (status, var='apar_t')
          status = nf90_put_att (ncid, apar_t_id, 'long_name', 'Parallel Magnetic Potential over Time')
          if (status /= NF90_NOERR) call netcdf_error (status, ncid, apar_t_id, att='long_name')
       end if
       status = nf90_put_att (ncid, apar2_by_mode_id, 'long_name', 'Apar squared')
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, apar2_by_mode_id, att='long_name')
       status = nf90_put_att (ncid, apar_id, 'long_name', 'Parallel Magnetic Potential')
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, apar_id, att='long_name')
       status = nf90_put_att (ncid, apar_id, 'idl_name', '!6A!9!D#!N!6')
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, apar_id, att='idl_name')
       status = nf90_put_att (ncid, apar2_id, 'long_name', 'Total A_par squared')
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, apar2_id, att='long_name')
    end if

    if (use_Bpar) then
       status = nf90_def_var (ncid, 'bpar2', netcdf_real, time_dim, bpar2_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='bpar2')
       status = nf90_def_var (ncid, 'bpar2_by_mode', netcdf_real, mode_dim, bpar2_by_mode_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='bpar2_by_mode')
       if (nakx > 1) then
          status = nf90_def_var (ncid, 'bpar2_by_kx', netcdf_real, kx_dim, bpar2_by_kx_id)
          if (status /= NF90_NOERR) call netcdf_error (status, var='bpar2_by_kx')
       end if
       if (naky > 1) then
          status = nf90_def_var (ncid, 'bpar2_by_ky', netcdf_real, ky_dim, bpar2_by_ky_id)
          if (status /= NF90_NOERR) call netcdf_error (status, var='bpar2_by_ky')
       end if
       status = nf90_def_var (ncid, 'bpar0', netcdf_real, omega_dim, bpar0_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='bpar0')
       if (write_nl_flux) then
          status = nf90_def_var (ncid, 'bpar_heat_flux', netcdf_real, flux_dim, bpar_heat_flux_id)
          if (status /= NF90_NOERR) call netcdf_error (status, var='bpar_heat_flux')
          status = nf90_def_var (ncid, 'bpar_heat_par', netcdf_real, flux_dim, bpar_heat_par_id)
          if (status /= NF90_NOERR) call netcdf_error (status, var='bpar_heat_par')
          status = nf90_def_var (ncid, 'bpar_heat_perp', netcdf_real, flux_dim, bpar_heat_perp_id)
          if (status /= NF90_NOERR) call netcdf_error (status, var='bpar_heat_perp')
          status = nf90_def_var (ncid, 'bpar_mom_flux', netcdf_real, flux_dim, bpar_mom_flux_id)
          if (status /= NF90_NOERR) call netcdf_error (status, var='bpar_mom_flux')
          status = nf90_def_var (ncid, 'bpar_part_flux', netcdf_real, flux_dim, bpar_part_flux_id)
          if (status /= NF90_NOERR) call netcdf_error (status, var='bpar_part_flux')
          status = nf90_def_var (ncid, 'bpar_heat_by_k', netcdf_real, fluxk_dim, bpar_heat_by_k_id)
          if (status /= NF90_NOERR) call netcdf_error (status, var='bpar_heat_by_k')
          status = nf90_def_var (ncid, 'bpar_mom_by_k', netcdf_real, fluxk_dim, bpar_mom_by_k_id)
          if (status /= NF90_NOERR) call netcdf_error (status, var='bpar_mom_by_k')
          status = nf90_def_var (ncid, 'bpar_part_by_k', netcdf_real, fluxk_dim, bpar_part_by_k_id)
          if (status /= NF90_NOERR) call netcdf_error (status, var='bpar_part_by_k')
       end if
       status = nf90_def_var (ncid, 'bpar', netcdf_real, final_field_dim, bpar_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='bpar')

       if (write_bpar_t) then
          status = nf90_def_var (ncid, 'bpar_t', netcdf_real, field_dim, bpar_t_id)
          if (status /= NF90_NOERR) call netcdf_error (status, var='bpar_t')
          status = nf90_put_att (ncid, bpar_t_id, 'long_name', 'delta B Parallel over time')
          if (status /= NF90_NOERR) call netcdf_error (status, ncid, bpar_t_id, att='long_name')
       end if
       !if (d_fields_per) then
        !  status = nf90_def_var (ncid, 'bpar_t', netcdf_real, field_dim, bpar_t_id)
         ! if (status /= NF90_NOERR) call netcdf_error (status, var='bpar_t')
       !end if

       status = nf90_put_att (ncid, bpar2_by_mode_id, 'long_name', 'A_perp squared')
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, bpar2_by_mode_id, att='long_name')
       status = nf90_put_att (ncid, bpar_id, 'long_name', 'delta B Parallel')
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, bpar_id, att='long_name')
       status = nf90_put_att (ncid, bpar_id, 'idl_name', '!6B!9!D#!N!6')
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, bpar_id, att='idl_name')
       status = nf90_put_att (ncid, bpar2_id, 'long_name', 'Total A_perp squared')
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, bpar2_id, att='long_name')
    end if

    status = nf90_def_var (ncid, 'phase', netcdf_real, phase_dim, phase_id)
    if (status /= NF90_NOERR) call netcdf_error (status, var='phase')
    status = nf90_put_att (ncid, phase_id, 'long_name', 'Normalizing phase')
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, phase_id, att='long_name')

!    status = nf90_def_var (ncid, 'phtot', netcdf_real, 3, mode_dim, phtot_id)
!    status = nf90_def_var (ncid, 'dmix',  netcdf_real, 3, mode_dim, dmix_id)
!    status = nf90_def_var (ncid, 'kperpnorm', netcdf_real, 3, mode_dim, kperpnorm_id)

    if (write_omega) then
       status = nf90_def_var (ncid, 'omega', netcdf_real, omega_dim, omega_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='omega')
       status = nf90_def_var (ncid, 'omegaavg', netcdf_real, omega_dim, omegaavg_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='omegaavg')
    end if

    if (write_nl_flux) then
       status = nf90_def_var (ncid, 'hflux_tot', netcdf_real, time_dim, hflux_tot_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='hflux_tot')
       status = nf90_def_var (ncid, 'vflux_tot', netcdf_real, time_dim, vflux_tot_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='vflux_tot')
       status = nf90_def_var (ncid, 'zflux_tot', netcdf_real, time_dim, zflux_tot_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='zflux_tot')
    end if

    if (d_neo) then
       status = nf90_def_var (ncid, 'qflux_neo_by_k', netcdf_real, fluxk_dim, qflux_neo_by_k_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='qflux_neo_by_k')
       status = nf90_def_var (ncid, 'pflux_neo_by_k', netcdf_real, fluxk_dim, pflux_neo_by_k_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='pflux_neo_by_k')
       status = nf90_def_var (ncid, 'sourcefac', netcdf_real, om_dim, sourcefac_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='sourcefac')
    end if

    if (write_final_moments) then
       status = nf90_def_var &
            (ncid, 'ntot', netcdf_real, final_mom_dim, ntot_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='ntot')
       status = nf90_def_var &
            (ncid, 'density', netcdf_real, final_mom_dim, density_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='density')
       status = nf90_def_var &
            (ncid, 'upar', netcdf_real, final_mom_dim, upar_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='upar')
       status = nf90_def_var &
            (ncid, 'tpar', netcdf_real, final_mom_dim, tpar_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='tpar')
       status = nf90_def_var &
            (ncid, 'tperp', netcdf_real, final_mom_dim, tperp_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='tperp')
    end if

    ! RN> moments at particle coordinate (small r)
    if (write_full_moments_r) then
       status = nf90_def_var (ncid, 'dens0', netcdf_real, mom_dim, dens0_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='dens0')
       status = nf90_def_var (ncid, 'ux0', netcdf_real, mom_dim, ux0_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='ux0')
       status = nf90_def_var (ncid, 'uy0', netcdf_real, mom_dim, uy0_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='uy0')
       status = nf90_def_var (ncid, 'uz0', netcdf_real, mom_dim, uz0_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='uz0')
       status = nf90_def_var (ncid, 'pxx0', netcdf_real, mom_dim, pxx0_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='pxx0')
       status = nf90_def_var (ncid, 'pyy0', netcdf_real, mom_dim, pyy0_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='pyy0')
       status = nf90_def_var (ncid, 'pzz0', netcdf_real, mom_dim, pzz0_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='pzz0')
       status = nf90_def_var (ncid, 'pxy0', netcdf_real, mom_dim, pxy0_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='pxy0')
       status = nf90_def_var (ncid, 'pyz0', netcdf_real, mom_dim, pyz0_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='pyz0')
       status = nf90_def_var (ncid, 'pzx0', netcdf_real, mom_dim, pzx0_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='pzx0')
    endif
    ! <RN

! MAB>
    if (write_vspectrum) then
       status = nf90_def_var (ncid, 'gvxi', netcdf_real, vxi_dim, gvxi_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='gvxi')
       status = nf90_def_var (ncid, 'gpq', netcdf_real, vpavpe_dim, gpq_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='gpq')
    end if
!    if (write_kspectrum) then
!       status = nf90_def_var (ncid, 'gkk', netcdf_real, kk_dim, gkk_id)
!       if (status /= NF90_NOERR) call netcdf_error (status, var='gkk')
!    end if

    if (write_avg_moments) then
       status = nf90_def_var (ncid, 'phi00', netcdf_real, loop_phi_dim, phi00_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='phi00')
       status = nf90_def_var (ncid, 'ntot00', netcdf_real, loop_mom_dim, ntot00_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='ntot00')
       status = nf90_def_var (ncid, 'density00', netcdf_real, loop_mom_dim, density00_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='density00')
       status = nf90_def_var (ncid, 'upar00', netcdf_real, loop_mom_dim, upar00_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='upar00')
       status = nf90_def_var (ncid, 'tpar00', netcdf_real, loop_mom_dim, tpar00_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='tpar00')
       status = nf90_def_var (ncid, 'tperp00', netcdf_real, loop_mom_dim, tperp00_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='tperp00')
       status = nf90_def_var (ncid, 'ntot2', netcdf_real, flux_dim, ntot2_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='ntot2')
       status = nf90_def_var (ncid, 'ntot2_by_mode', netcdf_real, fluxk_dim, ntot2_by_mode_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='ntot2_by_mode')
       status = nf90_def_var (ncid, 'ntot20', netcdf_real, flux_dim,  ntot20_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='ntot20')
       status = nf90_put_att (ncid, ntot20_id, 'long_name', 'Density**2 at theta=0')
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, ntot20_id, att='long_name')
       status = nf90_def_var (ncid, 'ntot20_by_mode', netcdf_real, fluxk_dim, ntot20_by_mode_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='ntot20_by_mode')
    end if

    if (write_hrate) then
       status = nf90_def_var (ncid, 'h_energy', netcdf_real, time_dim, h_energy_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='h_energy')
       status = nf90_def_var (ncid, 'h_energy_dot', netcdf_real, time_dim, h_energy_dot_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='h_energy_dot')
       status = nf90_def_var (ncid, 'h_antenna', netcdf_real, time_dim, h_antenna_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='h_antenna')
       status = nf90_def_var (ncid, 'h_eapar', netcdf_real, time_dim, h_eapar_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='h_eapar')
       status = nf90_def_var (ncid, 'h_ebpar', netcdf_real, time_dim, h_ebpar_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='h_ebpar')

       status = nf90_def_var (ncid, 'h_delfs2', netcdf_real, flux_dim, h_delfs2_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='h_delfs2')
       status = nf90_def_var (ncid, 'h_hs2', netcdf_real, flux_dim, h_hs2_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='h_hs2')
       status = nf90_def_var (ncid, 'h_phis2', netcdf_real, flux_dim, h_phis2_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='h_phis2')
       status = nf90_def_var (ncid, 'h_collisions', netcdf_real, flux_dim, h_collisions_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='h_collisions')
       status = nf90_def_var (ncid, 'h_gradients', netcdf_real, flux_dim, h_gradients_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='h_gradients')
       status = nf90_def_var (ncid, 'h_hypercoll', netcdf_real, flux_dim, h_hypercoll_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='h_hypercoll')
       status = nf90_def_var (ncid, 'h_imp_colls', netcdf_real, flux_dim, h_imp_colls_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='h_imp_colls')
       status = nf90_def_var (ncid, 'h_heating', netcdf_real, flux_dim, h_heating_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='h_heating')
       status = nf90_put_att (ncid, h_heating_id, 'long_name', 'Total heating by species')
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, h_heating_id, att='long_name')

       status = nf90_def_var (ncid, 'hk_energy', netcdf_real, mode_dim, hk_energy_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='hk_energy')
       status = nf90_def_var (ncid, 'hk_energy_dot', netcdf_real, mode_dim, hk_energy_dot_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='hk_energy_dot')
       status = nf90_def_var (ncid, 'hk_antenna', netcdf_real, mode_dim, hk_antenna_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='hk_antenna')
       status = nf90_def_var (ncid, 'hk_eapar', netcdf_real, mode_dim, hk_eapar_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='hk_eapar')
       status = nf90_def_var (ncid, 'hk_ebpar', netcdf_real, mode_dim, hk_ebpar_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='hk_ebpar')

       status = nf90_def_var (ncid, 'hk_delfs2', netcdf_real, fluxk_dim, hk_delfs2_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='hk_delf2')
       status = nf90_def_var (ncid, 'hk_hs2', netcdf_real, fluxk_dim, hk_hs2_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='hk_hs2')
       status = nf90_def_var (ncid, 'hk_phis2', netcdf_real, fluxk_dim, hk_phis2_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='hk_phis2')
       status = nf90_def_var (ncid, 'hk_collisions', netcdf_real, fluxk_dim, hk_collisions_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='hk_collisions')
       status = nf90_def_var (ncid, 'hk_gradients', netcdf_real, fluxk_dim, hk_gradients_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='hk_gradients')
       status = nf90_def_var (ncid, 'hk_imp_colls', netcdf_real, fluxk_dim, hk_imp_colls_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='hk_imp_colls')
       status = nf90_def_var (ncid, 'hk_hypercoll', netcdf_real, fluxk_dim, hk_hypercoll_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='hk_hypercoll')
       status = nf90_def_var (ncid, 'hk_heating', netcdf_real, fluxk_dim, hk_heating_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='hk_heating')
       status = nf90_put_att (ncid, hk_heating_id, 'long_name', 'Total heating by species and mode')
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, hk_heating_id, att='long_name')
    end if

!    status = nf90_put_att (ncid, phtot_id, 'long_name', 'Field amplitude')

    status = nf90_def_var (ncid, 'input_file', NF90_CHAR, nin_dim, input_id)
    if (status /= NF90_NOERR) call netcdf_error (status, var='input_file')
    status = nf90_put_att (ncid, input_id, 'long_name', 'Input file')
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, input_id, att='long_name')

    status = nf90_enddef (ncid)  ! out of definition mode
    if (status /= NF90_NOERR) call netcdf_error (status)

    status = nf90_put_var (ncid, nproc_id, nproc)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, nproc_id)

        ! added by EAB 03/05/04 for movies
    if(my_make_movie) then
       xmode_dim (1) = nx_dim
       xmode_dim (2) = ny_dim
       xmode_dim (3) = nth_dim
       xmode_dim (4) = time_movie_dim
       status = nf90_put_att (ncid_movie, NF90_GLOBAL, 'title', 'AstroGK Simulation x,y,theta Data for Movies')
       if (status /= NF90_NOERR) call netcdf_error (status, ncid_movie, NF90_GLOBAL, att='title')
       status = nf90_def_var (ncid_movie, 'nx', NF90_INT, nx_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='nx')
       status = nf90_def_var (ncid_movie, 'ny', NF90_INT, ny_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='ny')
       status = nf90_def_var (ncid_movie, 'ntheta', NF90_INT, nth_id)    
       if (status /= NF90_NOERR) call netcdf_error (status, var='ntheta')
       status = nf90_def_var (ncid_movie, 'tm', netcdf_real, time_movie_dim, time_movie_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='tm')
       status = nf90_put_att (ncid_movie, time_movie_id, 'long_name', 'Time')
       if (status /= NF90_NOERR) call netcdf_error (status, ncid_movie, time_movie_id, att='long_name')
       status = nf90_put_att (ncid_movie, time_movie_id, 'units', 'L/vt')
       if (status /= NF90_NOERR) call netcdf_error (status, ncid_movie, time_movie_id, att='units')
       status = nf90_def_var (ncid_movie, 'x', netcdf_real, nx_dim, x_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='x')
       status = nf90_put_att (ncid_movie, x_id, 'long_name', 'x / rho')
       if (status /= NF90_NOERR) call netcdf_error (status, ncid_movie, x_id, att='long_name')
       status = nf90_def_var (ncid_movie, 'y', netcdf_real, ny_dim, y_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='y')
       status = nf90_put_att (ncid_movie, y_id, 'long_name', 'y / rho')
       if (status /= NF90_NOERR) call netcdf_error (status, ncid_movie, y_id, att='long_name')
       status = nf90_def_var (ncid_movie, 'theta', netcdf_real, nth_dim, th_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='theta')
! >RN
       status = nf90_def_var (ncid_movie, 'density_by_xmode', netcdf_real, xmode_dim, density_by_xmode_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='density_by_xmode')
! <RN
       if(use_Phi) then
          status = nf90_def_var (ncid_movie, 'phi_by_xmode', netcdf_real, xmode_dim, phi_by_xmode_id)
          if (status /= NF90_NOERR) call netcdf_error (status, var='phi_by_xmode')
       end if
! >RN 2008/06/06
! apar is apar, not density. see above for density_by_xmode

! TEMPORARY BD
!       status = nf90_def_var (ncid_movie, 'density_by_xmode', netcdf_real, xmode_dim, apar_by_xmode_id)
!       if (status /= NF90_NOERR) call netcdf_error (status, var='density_by_xmode')
       if(use_Apar) then
          status = nf90_def_var (ncid_movie, 'apar_by_xmode', netcdf_real, xmode_dim, apar_by_xmode_id)
       end if
! <RN
       if(use_Bpar) then
          status = nf90_def_var (ncid_movie, 'bpar_by_xmode', netcdf_real, xmode_dim, bpar_by_xmode_id)
          if (status /= NF90_NOERR) call netcdf_error (status, var='bpar_by_xmode')
       end if
       status = nf90_enddef (ncid_movie)  ! out of definition mode
       if (status /= NF90_NOERR) call netcdf_error (status)
    endif

!>JMT
    if (freq_out) then
       freq_mode_dim(1) = ri_freq_dim
       freq_mode_dim(2) = ntheta_freq_dim
       freq_mode_dim(3) = nakyx_freq_dim
       freq_mode_dim(4) = time_freq_dim

       kykx_mode_dim(1) = kykx_freq_dim
       kykx_mode_dim(2) = nakyx_freq_dim

       status = nf90_put_att (ncid_freq, NF90_GLOBAL, 'title', 'AstroGK Simulation field data for Frequency')
       if (status /= NF90_NOERR) call netcdf_error (status, ncid_freq, NF90_GLOBAL, att='title')
       
       status = nf90_def_var (ncid_freq, 't', netcdf_real, time_freq_dim, time_freq_id)    
       if (status /= NF90_NOERR) call netcdf_error (status, var='t')
       status = nf90_put_att (ncid_freq, time_freq_id, 'long_name', 'Time')
       if (status /= NF90_NOERR) call netcdf_error (status, ncid_freq, time_freq_id, att='long_name')
       status = nf90_put_att (ncid_freq, time_freq_id, 'units', 'L/vt')
       if (status /= NF90_NOERR) call netcdf_error (status, ncid_freq, time_freq_id, att='units')

       status = nf90_def_var (ncid_freq, 'kykx', netcdf_real, kykx_mode_dim, akyx_freq_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='kykx')
       status = nf90_put_att (ncid_freq, akyx_freq_id, 'long_name', '(ky, kx) rho')
       if (status /= NF90_NOERR) call netcdf_error (status, ncid_freq, akyx_freq_id, att='long_name')

       status = nf90_def_var (ncid_freq, 'theta', netcdf_real, ntheta_freq_dim, theta_freq_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='theta')

       if (use_Phi) then
          status = nf90_def_var (ncid_freq, 'phi', netcdf_real, freq_mode_dim, phi_freq_id)
          if (status /= NF90_NOERR) call netcdf_error (status, var='phi')
       endif

       if (use_Apar) then
          status = nf90_def_var (ncid_freq, 'apar', netcdf_real, freq_mode_dim, apar_freq_id)
          if (status /= NF90_NOERR) call netcdf_error (status, var='apar')
       endif

       if (use_Bpar) then
          status = nf90_def_var (ncid_freq, 'bpar', netcdf_real, freq_mode_dim, bpar_freq_id)
          if (status /= NF90_NOERR) call netcdf_error (status, var='bpar')
       endif
       status = nf90_def_var (ncid_freq, 'epar', netcdf_real, freq_mode_dim, epar_freq_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='epar')

       status = nf90_enddef (ncid_freq)  ! out of definition mode
       if (status /= NF90_NOERR) call netcdf_error (status)
    endif
    
    ! JMT> moments at particle coordinate (small r)
    if (write_full_moments) then
       momf_dim(1) = ri_fullmom_dim
       momf_dim(2) = ntheta_fullmom_dim
       momf_dim(3) = nkx_fullmom_dim
       momf_dim(4) = nky_fullmom_dim
       momf_dim(5) = nspec_fullmom_dim
       momf_dim(6) = time_fullmom_dim
       
       
       status = nf90_put_att (ncid_fullmom, NF90_GLOBAL, 'title', 'AstroGK Simulation moment data')
       if (status /= NF90_NOERR) call netcdf_error (status, ncid_freq, NF90_GLOBAL, att='title')
       
       status = nf90_def_var (ncid_fullmom, 't', netcdf_real, time_fullmom_dim, time_fullmom_id)    
       if (status /= NF90_NOERR) call netcdf_error (status, var='t')
       status = nf90_put_att (ncid_fullmom, time_fullmom_id, 'long_name', 'Time')
       if (status /= NF90_NOERR) call netcdf_error (status, ncid_fullmom, time_fullmom_id, att='long_name')
       status = nf90_put_att (ncid_fullmom, time_fullmom_id, 'units', 'L/vt')
       if (status /= NF90_NOERR) call netcdf_error (status, ncid_fullmom, time_fullmom_id, att='units')


       status = nf90_def_var (ncid_fullmom, 'kx', netcdf_real, nkx_fullmom_dim, akx_fullmom_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='kx')
       status = nf90_put_att (ncid_fullmom, akx_fullmom_id, 'long_name', 'kx rho')
       if (status /= NF90_NOERR) call netcdf_error (status, ncid_fullmom, akx_fullmom_id, att='long_name')
       status = nf90_def_var (ncid_fullmom, 'ky', netcdf_real, nky_fullmom_dim, aky_fullmom_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='ky')
       status = nf90_put_att (ncid_fullmom, aky_fullmom_id, 'long_name', 'ky rho')
       if (status /= NF90_NOERR) call netcdf_error (status, ncid_fullmom, aky_fullmom_id, att='long_name')
       status = nf90_def_var (ncid_fullmom, 'theta', netcdf_real, ntheta_fullmom_dim, theta_fullmom_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='theta')
       status = nf90_put_att (ncid_fullmom, theta_fullmom_id, 'long_name', 'theta')
       if (status /= NF90_NOERR) call netcdf_error (status, ncid_fullmom, theta_fullmom_id, att='long_name')


       status = nf90_def_var (ncid_fullmom, 'densf0', netcdf_real, momf_dim, densf0_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='densf0')
       status = nf90_def_var (ncid_fullmom, 'uxf0', netcdf_real, momf_dim, uxf0_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='uxf0')
       status = nf90_def_var (ncid_fullmom, 'uyf0', netcdf_real, momf_dim, uyf0_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='uyf0')
       status = nf90_def_var (ncid_fullmom, 'uzf0', netcdf_real, momf_dim, uzf0_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='uzf0')
       status = nf90_def_var (ncid_fullmom, 'pxxf0', netcdf_real, momf_dim, pxxf0_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='pxxf0')
       status = nf90_def_var (ncid_fullmom, 'pyyf0', netcdf_real, momf_dim, pyyf0_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='pyyf0')
       status = nf90_def_var (ncid_fullmom, 'pzzf0', netcdf_real, momf_dim, pzzf0_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='pzzf0')
       status = nf90_def_var (ncid_fullmom, 'pxyf0', netcdf_real, momf_dim, pxyf0_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='pxyf0')
       status = nf90_def_var (ncid_fullmom, 'pyzf0', netcdf_real, momf_dim, pyzf0_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='pyzf0')
       status = nf90_def_var (ncid_fullmom, 'pzxf0', netcdf_real, momf_dim, pzxf0_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='pzxf0')
       status = nf90_def_var (ncid_fullmom, 'qzzzf0', netcdf_real, momf_dim, qzzzf0_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='qzzzf0')
       status = nf90_def_var (ncid_fullmom, 'qzppf0', netcdf_real, momf_dim, qzppf0_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='qzppf0')

       status = nf90_enddef (ncid_fullmom)  ! out of definition mode
       if (status /= NF90_NOERR) call netcdf_error (status)
    endif
    ! <JMT
    
!<JMT
# endif
  end subroutine define_vars

  subroutine nc_final_fields

    use convert, only: c2r
    use run_parameters, only: use_Phi, use_Apar, use_Bpar
    use fields_arrays, only: phi, apar, bpar
    use theta_grid, only: ntgrid
    use kgrids, only: naky, nakx
# ifdef NETCDF
    use netcdf, only: nf90_put_var
!    use agk_save, only: netcdf_error
    use netcdf_utils, only: netcdf_error
    real, dimension (2, 2*ntgrid+1, nakx, naky) :: ri3
    integer :: status

    if (use_Phi .and. proc_write) then
       call c2r (phi, ri3)
       status = nf90_put_var (ncid, phi_id, ri3)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, phi_id)
    end if

    if (use_Apar .and. proc_write) then
       call c2r (apar, ri3)
       status = nf90_put_var (ncid, apar_id, ri3)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, apar_id)
    end if

    if (use_Bpar .and. proc_write) then
       call c2r (bpar, ri3)
       status = nf90_put_var (ncid, bpar_id, ri3)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, bpar_id)
    end if
# endif
  end subroutine nc_final_fields

  subroutine nc_final_epar (epar)

    use convert, only: c2r
    use theta_grid, only: ntgrid
    use kgrids, only: naky, nakx
# ifdef NETCDF
    use netcdf, only: nf90_def_var, nf90_put_var
    use netcdf, only: nf90_redef, nf90_enddef
!    use agk_save, only: netcdf_error, netcdf_real
    use netcdf_utils, only: netcdf_error, netcdf_real
# endif
    complex, dimension (:,:,:), intent (in) :: epar
# ifdef NETCDF
    real, dimension (2, 2*ntgrid+1, nakx, naky) :: ri3
    integer :: status

    if (proc_write) then
       status = nf90_redef(ncid)
       if (status /= NF90_NOERR) call netcdf_error (status)
       status = nf90_def_var &
            (ncid, 'epar', netcdf_real, final_field_dim, epar_id)
       if (status /= NF90_NOERR) call netcdf_error (status, var='epar')
       status = nf90_enddef(ncid)
       if (status /= NF90_NOERR) call netcdf_error (status)
       call c2r (epar, ri3)
       status = nf90_put_var(ncid, epar_id, ri3)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, epar_id)
    end if
# endif
  end subroutine nc_final_epar

  subroutine nc_final_moments (ntot, density, upar, tpar, tperp)
    ! TT: called only for proc0
    use convert, only: c2r
    use theta_grid, only: ntgrid
    use kgrids, only: naky, nakx
    use species, only: nspec
# ifdef NETCDF
    use netcdf, only: nf90_put_var
!    use agk_save, only: netcdf_error
    use netcdf_utils, only: netcdf_error
# endif
    complex, dimension (:,:,:,:), intent (in) :: ntot, density, upar, tpar, tperp
# ifdef NETCDF
    real, dimension (2, 2*ntgrid+1, nakx, naky, nspec) :: ri4
    integer :: status

    call c2r (ntot, ri4)
    status = nf90_put_var (ncid, ntot_id, ri4)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, ntot_id)

    call c2r (density, ri4)
    status = nf90_put_var (ncid, density_id, ri4)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, density_id)

    call c2r (upar, ri4)
    status = nf90_put_var (ncid, upar_id, ri4)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, upar_id)

    call c2r (tpar, ri4)
    status = nf90_put_var (ncid, tpar_id, ri4)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, tpar_id)

    call c2r (tperp, ri4)
    status = nf90_put_var (ncid, tperp_id, ri4)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, tperp_id)
# endif
  end subroutine nc_final_moments

  subroutine nc_loop_moments (nout, ntot2, ntot2_by_mode, ntot20, &
       ntot20_by_mode, phi00, ntot00, density00, upar00, tpar00, tperp00)

    use convert, only: c2r

    use kgrids, only: naky, nakx
    use species, only: nspec
# ifdef NETCDF
    use netcdf, only: nf90_put_var
    use netcdf_utils, only: netcdf_error
# endif

    integer, intent (in) :: nout
    real, dimension (:), intent (in) :: ntot2, ntot20
    real, dimension (:,:,:), intent (in) :: ntot2_by_mode, ntot20_by_mode
    complex, dimension (:), intent (in) :: phi00
    complex, dimension (:,:), intent (in) :: ntot00, density00, upar00, tpar00, tperp00
# ifdef NETCDF
    real, dimension (2, nakx, nspec) :: ri2
    real, dimension (2, nakx) :: ri1
    integer, dimension (2) :: start, count
    integer, dimension (3) :: start3, count3
    integer, dimension (4) :: start00, count00, start4, count4
    integer :: status

    start00(1) = 1
    start00(2) = 1
    start00(3) = 1
    start00(4) = nout
    
    count00(1) = 2
    count00(2) = nakx
    count00(3) = nspec
    count00(4) = 1

    start3(1) = 1
    start3(2) = 1
    start3(3) = nout
    
    count3(1) = 2
    count3(2) = nakx
    count3(3) = 1

    start(1) = 1
    start(2) = nout
    
    count(1) = nspec
    count(2) = 1

    start4(1) = 1
    start4(2) = 1
    start4(3) = 1
    start4(4) = nout
    
    count4(1) = nakx
    count4(2) = naky
    count4(3) = nspec
    count4(4) = 1

    if (proc_write) then
       status = nf90_put_var (ncid, ntot2_id, ntot2, start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, ntot2_id)
       status = nf90_put_var (ncid, ntot2_by_mode_id, ntot2_by_mode, start=start4, count=count4)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, ntot2_by_mode_id)

       status = nf90_put_var (ncid, ntot20_id, ntot20, start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, ntot20_id)
       status = nf90_put_var (ncid, ntot20_by_mode_id, ntot20_by_mode, start=start4, count=count4)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, ntot20_by_mode_id)

       call c2r (phi00, ri1)
       status = nf90_put_var (ncid, phi00_id, ri1, start=start3, count=count3)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, phi00_id)

       call c2r (ntot00, ri2)
       status = nf90_put_var (ncid, ntot00_id, ri2, start=start00, count=count00)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, ntot00_id)

       call c2r (density00, ri2)
       status = nf90_put_var (ncid, density00_id, ri2, start=start00, count=count00)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, density00_id)

       call c2r (upar00, ri2)
       status = nf90_put_var (ncid, upar00_id, ri2, start=start00, count=count00)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, upar00_id)

       call c2r (tpar00, ri2)
       status = nf90_put_var (ncid, tpar00_id, ri2, start=start00, count=count00)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, tpar00_id)

       call c2r (tperp00, ri2)
       status = nf90_put_var (ncid, tperp00_id, ri2, start=start00, count=count00)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, tperp00_id)
    end if
# endif
  end subroutine nc_loop_moments

  subroutine nc_qflux (nout, qheat, qmheat, qbheat, &
       heat_par,  mheat_par,  bheat_par, &
       heat_perp, mheat_perp, bheat_perp, &
       heat_fluxes, mheat_fluxes, bheat_fluxes, hflux_tot)

    use species, only: nspec
    use kgrids, only: naky, nakx
    use run_parameters, only: use_Phi, use_Apar, use_Bpar
# ifdef NETCDF
    use netcdf, only: nf90_put_var
!    use agk_save, only: netcdf_error
    use netcdf_utils, only: netcdf_error
# endif

    integer, intent (in) :: nout
    real, dimension (:,:,:), intent (in) :: qheat, qmheat, qbheat
    real, dimension (:), intent (in) :: heat_par, mheat_par, bheat_par
    real, dimension (:), intent (in) :: heat_perp, mheat_perp, bheat_perp
    real, dimension (:), intent (in) :: heat_fluxes, mheat_fluxes, bheat_fluxes
    real, intent (in) :: hflux_tot
# ifdef NETCDF
    integer, dimension (2) :: start, count
    integer, dimension (4) :: start4, count4
    integer :: status

    start4(1) = 1
    start4(2) = 1
    start4(3) = 1
    start4(4) = nout
    
    count4(1) = nakx
    count4(2) = naky
    count4(3) = nspec
    count4(4) = 1

    start(1) = 1
    start(2) = nout
    
    count(1) = nspec
    count(2) = 1

    if (use_Phi .and. proc_write) then
       status = nf90_put_var (ncid, es_heat_flux_id, heat_fluxes, start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, es_heat_flux_id)
       status = nf90_put_var (ncid, es_heat_par_id, heat_par, start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, es_heat_par_id)
       status = nf90_put_var (ncid, es_heat_perp_id, heat_perp, start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, es_heat_perp_id)
       status = nf90_put_var (ncid, es_heat_by_k_id, qheat, start=start4, count=count4)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, es_heat_by_k_id)
    end if

    if (use_Apar .and. proc_write) then
       status = nf90_put_var (ncid, apar_heat_flux_id, mheat_fluxes, start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, apar_heat_flux_id)
       status = nf90_put_var (ncid, apar_heat_par_id, mheat_par, start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, apar_heat_par_id)
       status = nf90_put_var (ncid, apar_heat_perp_id, mheat_perp, start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, apar_heat_perp_id)
       status = nf90_put_var (ncid, apar_heat_by_k_id, qmheat, start=start4, count=count4)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, apar_heat_by_k_id)
    end if

    if (use_Bpar .and. proc_write) then
       status = nf90_put_var (ncid, bpar_heat_flux_id, bheat_fluxes, start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, bpar_heat_flux_id)
       status = nf90_put_var (ncid, bpar_heat_par_id, bheat_par, start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, bpar_heat_par_id)
       status = nf90_put_var (ncid, bpar_heat_perp_id, bheat_perp, start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, bpar_heat_perp_id)
       status = nf90_put_var (ncid, bpar_heat_by_k_id, qbheat, start=start4, count=count4)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, bpar_heat_by_k_id)
    end if

    if (proc_write) then
       status = nf90_put_var (ncid, hflux_tot_id, hflux_tot, start=(/nout/))
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, hflux_tot_id)
    end if
# endif
  end subroutine nc_qflux

  subroutine nc_pflux (nout, pflux, pmflux, pbflux, &
       part_fluxes, mpart_fluxes, bpart_fluxes, zflux_tot)

    use species, only: nspec
    use kgrids, only: naky, nakx
    use run_parameters, only: use_Phi, use_Apar, use_Bpar
# ifdef NETCDF
    use netcdf, only: nf90_put_var
!    use agk_save, only: netcdf_error
    use netcdf_utils, only: netcdf_error
# endif
    integer, intent (in) :: nout
    real, dimension (:,:,:), intent (in) :: pflux, pmflux, pbflux
    real, dimension(:), intent (in) :: part_fluxes, mpart_fluxes, bpart_fluxes
    real, intent (in) :: zflux_tot
# ifdef NETCDF
    integer, dimension (2) :: start, count
    integer, dimension (4) :: start4, count4
    integer :: status

    start4(1) = 1
    start4(2) = 1
    start4(3) = 1
    start4(4) = nout
    
    count4(1) = nakx
    count4(2) = naky
    count4(3) = nspec
    count4(4) = 1

    start(1) = 1
    start(2) = nout
    
    count(1) = nspec
    count(2) = 1

    if (use_Phi .and. proc_write) then
       status = nf90_put_var (ncid, es_part_flux_id, part_fluxes, start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, es_part_flux_id)
       status = nf90_put_var (ncid, es_part_by_k_id, pflux, start=start4, count=count4)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, es_part_by_k_id)
    end if

    if (use_Apar .and. proc_write) then
       status = nf90_put_var (ncid, apar_part_flux_id, mpart_fluxes, start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, apar_part_flux_id)
       status = nf90_put_var (ncid, apar_part_by_k_id, pmflux, start=start4, count=count4)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, apar_part_by_k_id)
    end if

    if (use_Bpar .and. proc_write) then
       status = nf90_put_var (ncid, bpar_part_flux_id, bpart_fluxes, start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, bpar_part_flux_id)
       status = nf90_put_var (ncid, bpar_part_by_k_id, pbflux, start=start4, count=count4)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, bpar_part_by_k_id)
    end if

    if (proc_write) then
       status = nf90_put_var (ncid, zflux_tot_id, zflux_tot, start=(/nout/))
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, zflux_tot_id)
    end if
# endif
  end subroutine nc_pflux

  subroutine nc_vflux (nout, vflux, vmflux, vbflux, &
       mom_fluxes, mmom_fluxes, bmom_fluxes, vflux_tot)

    use species, only: nspec
    use kgrids, only: naky, nakx
    use run_parameters, only: use_Phi, use_Apar, use_Bpar
# ifdef NETCDF
    use netcdf, only: nf90_put_var
!    use agk_save, only: netcdf_error
    use netcdf_utils, only: netcdf_error
# endif

    integer, intent (in) :: nout
    real, dimension (:,:,:), intent (in) :: vflux, vmflux, vbflux
    real, dimension(:), intent (in) :: mom_fluxes, mmom_fluxes, bmom_fluxes
    real, intent (in) :: vflux_tot
# ifdef NETCDF
    integer, dimension (2) :: start, count
    integer, dimension (4) :: start4, count4
    integer :: status

    start4(1) = 1
    start4(2) = 1
    start4(3) = 1
    start4(4) = nout
    
    count4(1) = nakx
    count4(2) = naky
    count4(3) = nspec
    count4(4) = 1

    start(1) = 1
    start(2) = nout
    
    count(1) = nspec
    count(2) = 1

    if (use_Phi .and. proc_write) then
       status = nf90_put_var (ncid, es_mom_flux_id, mom_fluxes, start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, es_mom_flux_id)
       status = nf90_put_var (ncid, es_mom_by_k_id, vflux, start=start4, count=count4)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, es_mom_by_k_id)
    end if

    if (use_Apar .and. proc_write) then
       status = nf90_put_var (ncid, apar_mom_flux_id, mmom_fluxes, start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, apar_mom_flux_id)
       status = nf90_put_var (ncid, apar_mom_by_k_id, vmflux, start=start4, count=count4)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, apar_mom_by_k_id)
    end if

    if (use_Bpar .and. proc_write) then
       status = nf90_put_var (ncid, bpar_mom_flux_id, bmom_fluxes, start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, bpar_mom_flux_id)
       status = nf90_put_var (ncid, bpar_mom_by_k_id, vbflux, start=start4, count=count4)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, bpar_mom_by_k_id)
    end if

    if (proc_write) then
       status = nf90_put_var (ncid, vflux_tot_id, vflux_tot, start=(/nout/))
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, vflux_tot_id)
    end if
# endif
  end subroutine nc_vflux
!------------------------------------------------------------------------------
!                              AstroGK, 2008
!------------------------------------------------------------------------------
! NetCDF Output for loop diagnostics
  subroutine nc_loop (nout, time, fluxfac, &
       phi0,   phi2,   phi2_by_mode, &! phiavg, &
       apar0,  apar2,  apar2_by_mode, &
       bpar0, bpar2, bpar2_by_mode, &
       h, hk, omega, omegaavg, phitot, write_omega, write_hrate) 
    ! TT: called only for proc0
    use agk_heating_index
    use run_parameters, only: use_Phi, use_Apar, use_Bpar
    use fields_arrays, only: phi, apar, bpar
    use theta_grid, only: ntgrid
    use kgrids, only: naky, nakx
    use species, only: nspec
    use convert, only: c2r
# ifdef NETCDF
    use netcdf, only: nf90_put_var, nf90_sync
    use netcdf_utils, only: netcdf_error
# endif
    implicit none
    !Passed
    integer, intent (in) :: nout
    real, intent (in) :: time, phi2, apar2, bpar2
    real, dimension (:), intent (in) :: fluxfac
    complex, dimension(:,:), intent (in) :: phi0, apar0, bpar0, omega, omegaavg
    real, dimension(:,:), intent (in) :: phi2_by_mode, apar2_by_mode, bpar2_by_mode, phitot
    real, dimension(:), intent (in) :: h
    real, dimension(:,:,:), intent (in) :: hk
    logical :: write_omega, write_hrate
# ifdef NETCDF
    !Local 
    real, dimension (nakx) :: field2_by_kx
    real, dimension (naky) :: field2_by_ky
    real, dimension (2, nakx, naky) :: ri2
    real, dimension (2, 2*ntgrid+1, nakx, naky) :: ri4
    integer, dimension (5) :: start5, count5
    complex, dimension (nakx, naky) :: tmp
    integer, dimension (4) :: start0, count0, start4, count4
    integer, dimension (3) :: start, count, starth, counth
    integer, dimension (2) :: startx, countx, starty, county, starts, counts
    integer :: status, it, ik

    status = nf90_put_var (ncid, time_id, time, start=(/nout/))
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, time_id)

    start(1) = 1
    start(2) = 1
    start(3) = nout

    count(1) = nakx
    count(2) = naky
    count(3) = 1

    start0(1) = 1
    start0(2) = 1
    start0(3) = 1
    start0(4) = nout

    count0(1) = 2
    count0(2) = nakx
    count0(3) = naky
    count0(4) = 1

    start4(1) = 1
    start4(2) = 1
    start4(3) = 1
    start4(4) = nout

    count4(1) = nakx
    count4(2) = naky
    count4(3) = nspec
    count4(4) = 1

    starty(1) = 1
    starty(2) = nout

    county(1) = naky
    county(2) = 1

    startx(1) = 1
    startx(2) = nout

    countx(1) = nakx
    countx(2) = 1

    starts(1) = 1
    starts(2) = nout

    counts(1) = nspec
    counts(2) = 1

    starth(1) = 1 
    starth(2) = 1
    starth(3) = nout

    counth(1) = nspec
    counth(2) = 7
    counth(3) = 1


    !Added by EGH ; for writing phi_t, the whole potential vs time

    start5(1) = 1
    start5(2) = 1
    start5(3) = 1
    start5(4) = 1
    start5(5) = nout
  
    count5(1) = 2
    count5(2) = 2*ntgrid+1
    count5(3) = nakx
    count5(4) = naky
    count5(5) = 1
  
    !End EGH

    if (use_Phi) then

      ! Write fields at the current timestep, if write_phi_over_time is true
      if(write_phi_t) then
          call c2r (phi, ri4)
         !ri_phi_t(:,:,:,:,1) = ri4(:,:,:,:)
          status = nf90_put_var (ncid, phi_t_id, ri4, start=start5, count=count5)
              if (status /= NF90_NOERR) call netcdf_error (status, ncid, phi_id)
      end if


       if (nakx > 1) then
          do it = 1, nakx
             field2_by_kx(it) = sum(phi2_by_mode(it,:)*fluxfac(:))
          end do
          status = nf90_put_var (ncid, phi2_by_kx_id, field2_by_kx, start=startx, count=countx)
          if (status /= NF90_NOERR) call netcdf_error (status, ncid, phi2_by_kx_id)
       end if

       if (naky > 1) then
          do ik = 1, naky
             field2_by_ky(ik) = sum(phi2_by_mode(:,ik)*fluxfac(ik))
          end do
          status = nf90_put_var (ncid, phi2_by_ky_id, field2_by_ky, start=starty, count=county)
          if (status /= NF90_NOERR) call netcdf_error (status, ncid, phi2_by_ky_id)
       end if

       call c2r (phi0, ri2)
       status = nf90_put_var (ncid, phi0_id, ri2, start=start0, count=count0)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, phi0_id)
!       call c2r (phiavg, ri2) 
!       status = nf90_put_var (ncid, phiavg_id, ri2, start=start0, count=count0)
       status = nf90_put_var (ncid, phi2_by_mode_id, phi2_by_mode, start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, phi2_by_mode_id)
       status = nf90_put_var (ncid, phi2_id, phi2, start=(/nout/))
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, phi2_id)
    end if

    if (use_Apar) then

       if(write_apar_t) then
           call c2r (apar, ri4)
                !ri_apar_t(:,:,:,:,1) = ri3(:,:,:,:)
          status = nf90_put_var (ncid, apar_t_id, ri4, start=start5, count=count5)
          if (status /= NF90_NOERR) call netcdf_error (status, ncid, apar_id)
        end if
       if (nakx > 1) then
          do it = 1, nakx
             field2_by_kx(it) = sum(apar2_by_mode(it,:)*fluxfac(:))
          end do
          status = nf90_put_var (ncid, apar2_by_kx_id, field2_by_kx, start=startx, count=countx)
          if (status /= NF90_NOERR) call netcdf_error (status, ncid, apar2_by_kx_id)
       end if

       if (naky > 1) then
          do ik = 1, naky
             field2_by_ky(ik) = sum(apar2_by_mode(:,ik)*fluxfac(ik))
          end do
          status = nf90_put_var (ncid, apar2_by_ky_id, field2_by_ky, start=starty, count=county)
          if (status /= NF90_NOERR) call netcdf_error (status, ncid, apar2_by_ky_id)
       end if

       call c2r (apar0, ri2) 
       status = nf90_put_var (ncid, apar0_id, ri2, start=start0, count=count0)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, apar0_id)
       status = nf90_put_var (ncid, apar2_by_mode_id, apar2_by_mode, start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, apar2_by_mode_id)
       status = nf90_put_var (ncid, apar2_id, apar2, start=(/nout/))
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, apar2_id)
    end if

    if (use_Bpar) then

       if(write_bpar_t) then
          call c2r (bpar, ri4)
                !ri_bpar_t(:,:,:,:,1) = ri3(:,:,:,:)
          status = nf90_put_var (ncid, bpar_t_id, ri4, start=start5, count=count5)
          if (status /= NF90_NOERR) call netcdf_error (status, ncid, bpar_id)
       end if
       if (nakx > 1) then
          do it = 1, nakx
             field2_by_kx(it) = sum(bpar2_by_mode(it,:)*fluxfac(:))
          end do
          status = nf90_put_var (ncid, bpar2_by_kx_id, field2_by_kx, start=startx, count=countx)
          if (status /= NF90_NOERR) call netcdf_error (status, ncid, bpar2_by_kx_id)
       end if

       if (naky > 1) then
          do ik = 1, naky
             field2_by_ky(ik) = sum(bpar2_by_mode(:,ik)*fluxfac(ik))
          end do
          status = nf90_put_var (ncid, bpar2_by_ky_id, field2_by_ky, start=starty, count=county)
          if (status /= NF90_NOERR) call netcdf_error (status, ncid, bpar2_by_ky_id)
       end if

       call c2r (bpar0, ri2) 
       status = nf90_put_var (ncid, bpar0_id, ri2, start=start0, count=count0)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, bpar0_id)
       status = nf90_put_var (ncid, bpar2_by_mode_id, bpar2_by_mode, start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, bpar2_by_mode_id)
       status = nf90_put_var (ncid, bpar2_id, bpar2, start=(/nout/))
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, bpar2_id)
    end if

    if (write_hrate) then
       status = nf90_put_var (ncid, h_energy_id, h(lenergy), start=(/nout/))
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, h_energy_id)
       status = nf90_put_var (ncid, h_energy_dot_id, h(lenergy_dot), start=(/nout/))
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, h_energy_dot_id)
       status = nf90_put_var (ncid, h_antenna_id, h(lantenna), start=(/nout/))
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, h_antenna_id)
       status = nf90_put_var (ncid, h_eapar_id, h(leapar), start=(/nout/))
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, h_eapar_id)
       status = nf90_put_var (ncid, h_ebpar_id, h(lebpar), start=(/nout/))
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, h_ebpar_id)

       status = nf90_put_var (ncid, h_delfs2_id, h(ldelfs2(1):ldelfs2(nspec)), start=starts, count=counts)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, h_delfs2_id)
       status = nf90_put_var (ncid, h_hs2_id, h(lhs2(1):lhs2(nspec)), start=starts, count=counts)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, h_hs2_id)
       status = nf90_put_var (ncid, h_phis2_id, h(lphis2(1):lphis2(nspec)), start=starts, count=counts)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, h_phis2_id)
       status = nf90_put_var (ncid, h_collisions_id, h(lcollisions(1):lcollisions(nspec)), start=starts, count=counts)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, h_collisions_id)
       status = nf90_put_var (ncid, h_gradients_id, h(lgradients(1):lgradients(nspec)), start=starts, count=counts)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, h_gradients_id)
       status = nf90_put_var (ncid, h_imp_colls_id, h(limp_colls(1):limp_colls(nspec)), start=starts, count=counts)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, h_imp_colls_id)
       status = nf90_put_var (ncid, h_hypercoll_id, h(lhypercoll(1):lhypercoll(nspec)), start=starts, count=counts)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, h_hypercoll_id)
       status = nf90_put_var (ncid, h_heating_id, h(lheating(1):lheating(nspec)), start=starts, count=counts)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, h_heating_id)

       status = nf90_put_var (ncid, hk_energy_id, hk(:,:,lenergy), start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, hk_energy_id)
       status = nf90_put_var (ncid, hk_energy_dot_id, hk(:,:,lenergy_dot), start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, hk_energy_dot_id)
       status = nf90_put_var (ncid, hk_antenna_id, hk(:,:,lantenna), start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, hk_antenna_id)
       status = nf90_put_var (ncid, hk_eapar_id, hk(:,:,leapar), start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, hk_eapar_id)
       status = nf90_put_var (ncid, hk_ebpar_id, hk(:,:,lebpar), start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, hk_ebpar_id)
       status = nf90_put_var (ncid, hk_collisions_id, hk(:,:,lcollisions(1):lcollisions(nspec)), start=start4, count=count4)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, hk_collisions_id)
       status = nf90_put_var (ncid, hk_gradients_id, hk(:,:,lgradients(1):lgradients(nspec)), start=start4, count=count4)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, hk_gradients_id)
       status = nf90_put_var (ncid, hk_heating_id, hk(:,:,lheating(1):lheating(nspec)), start=start4, count=count4)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, hk_heating_id)
       status = nf90_put_var (ncid, hk_hypercoll_id, hk(:,:,lhypercoll(1):lhypercoll(nspec)), start=start4, count=count4)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, hk_hypercoll_id)
       status = nf90_put_var (ncid, hk_imp_colls_id, hk(:,:,limp_colls(1):limp_colls(nspec)), start=start4, count=count4)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, hk_imp_colls_id)
       status = nf90_put_var (ncid, hk_hs2_id, hk(:,:,lhs2(1):lhs2(nspec)), start=start4, count=count4)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, hk_hs2_id)
       status = nf90_put_var (ncid, hk_phis2_id, hk(:,:,lphis2(1):lphis2(nspec)), start=start4, count=count4)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, hk_phis2_id)
       status = nf90_put_var (ncid, hk_delfs2_id, hk(:,:,ldelfs2(1):ldelfs2(nspec)), start=start4, count=count4)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, hk_delfs2_id)

    end if

    if (write_omega) then
       do it = 1, nakx
          tmp(it, :) = omega(it, :)
       end do
       
       call c2r (tmp, ri2)
       status = nf90_put_var (ncid, omega_id, ri2, start=start0, count=count0)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, omega_id)

       do it = 1, nakx
          tmp(it, :) = omegaavg(it, :)
       end do

       call c2r (tmp, ri2)
       status = nf90_put_var (ncid, omegaavg_id, ri2, start=start0, count=count0)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, omegaavg_id)
    end if

!    status = nf90_put_var (ncid, phtot_id, start=start, count=count, phitot)
    if (mod(nout, 10) == 0) then
       status = nf90_sync (ncid)
       if (status /= NF90_NOERR) call netcdf_error (status)
    end if
# endif
  end subroutine nc_loop

  ! RN> output full not guiding center moments 
  subroutine nc_loop_fullmom (nout, time, &
       & dens0, ux0, uy0, uz0, pxx0, pyy0, pzz0, pxy0, pyz0, pzx0)
    use kgrids, only: naky, nakx
    use species, only: nspec
    use convert, only: c2r
# ifdef NETCDF
    use netcdf, only: nf90_put_var, nf90_sync
    use netcdf_utils, only: netcdf_error
# endif
    implicit none

    integer, intent (in) :: nout
    real, intent (in) :: time
    complex, intent(in) :: dens0(:,:,:)
    complex, intent(in) :: ux0(:,:,:), uy0(:,:,:), uz0(:,:,:)
    complex, intent(in) :: pxx0(:,:,:),pyy0(:,:,:),pzz0(:,:,:)
    complex, intent(in) :: pxy0(:,:,:),pyz0(:,:,:),pzx0(:,:,:)
# ifdef NETCDF
    real, dimension (2, nakx, naky, nspec) :: ri3
    integer, dimension (5) :: startmom, countmom
    integer :: status

    status = nf90_put_var (ncid, time_id, time, start=(/nout/))
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, time_id)

    startmom(1) = 1;    countmom(1)=2
    startmom(2) = 1;    countmom(2)=nakx
    startmom(3) = 1;    countmom(3)=naky
    startmom(4) = 1;    countmom(4)=nspec
    startmom(5) = nout; countmom(5)=1

    call c2r (dens0, ri3)
    status = nf90_put_var (ncid, dens0_id, ri3, start=startmom, count=countmom)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, dens0_id)

    call c2r (ux0, ri3)
    status = nf90_put_var (ncid, ux0_id, ri3, start=startmom, count=countmom)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, ux0_id)
    call c2r (uy0, ri3)
    status = nf90_put_var (ncid, uy0_id, ri3, start=startmom, count=countmom)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, uy0_id)
    call c2r (uz0, ri3)
    status = nf90_put_var (ncid, uz0_id, ri3, start=startmom, count=countmom)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, uz0_id)

    ! pressures
    call c2r (pxx0, ri3)
    status = nf90_put_var (ncid, pxx0_id, ri3, start=startmom, count=countmom)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, pxx0_id)
    call c2r (pyy0, ri3)
    status = nf90_put_var (ncid, pyy0_id, ri3, start=startmom, count=countmom)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, pyy0_id)
    call c2r (pzz0, ri3)
    status = nf90_put_var (ncid, pzz0_id, ri3, start=startmom, count=countmom)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, pzz0_id)
    call c2r (pxy0, ri3)
    status = nf90_put_var (ncid, pxy0_id, ri3, start=startmom, count=countmom)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, pxy0_id)
    call c2r (pyz0, ri3)
    status = nf90_put_var (ncid, pyz0_id, ri3, start=startmom, count=countmom)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, pyz0_id)
    call c2r (pzx0, ri3)
    status = nf90_put_var (ncid, pzx0_id, ri3, start=startmom, count=countmom)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, pzx0_id)

    if (mod(nout, 10) == 0) then
       status = nf90_sync (ncid)
       if (status /= NF90_NOERR) call netcdf_error (status)
    end if
# endif
  end subroutine nc_loop_fullmom
  ! <RN

  ! JMT> output full not guiding center moments 
  subroutine nc_loop_fullmom2 (time, nout, &
       & dens0, ux0, uy0, uz0, pxx0, pyy0, pzz0, pxy0, pyz0, pzx0, qzzz0, qzpp0)
    use kgrids, only: naky, nakx
    use theta_grid, only: ntgrid
    use species, only: nspec
    use convert, only: c2r
# ifdef NETCDF
    use netcdf, only: nf90_put_var, nf90_sync, nf90_close
    use netcdf_utils, only: netcdf_error
# endif
    implicit none

    integer, intent (in) :: nout
    real, intent (in) :: time
    complex, intent(in) :: dens0(:,:,:,:)
    complex, intent(in) :: ux0(:,:,:,:), uy0(:,:,:,:), uz0(:,:,:,:)
    complex, intent(in) :: pxx0(:,:,:,:),pyy0(:,:,:,:),pzz0(:,:,:,:)
    complex, intent(in) :: pxy0(:,:,:,:),pyz0(:,:,:,:),pzx0(:,:,:,:)
    complex, intent(in) :: qzzz0(:,:,:,:),qzpp0(:,:,:,:)
# ifdef NETCDF
    real, dimension (2,  2*ntgrid+1, nakx, naky, nspec) :: ri4
    integer, dimension (6) :: startmom, countmom
    integer :: status

    status = nf90_put_var (ncid_fullmom, time_fullmom_id, time, start=(/nout/))
    if (status /= NF90_NOERR) call netcdf_error (status, ncid_fullmom, time_fullmom_id)

    startmom(1) = 1;    countmom(1)=2
    startmom(2) = 1;    countmom(2)=2*ntgrid+1
    startmom(3) = 1;    countmom(3)=nakx
    startmom(4) = 1;    countmom(4)=naky
    startmom(5) = 1;    countmom(5)=nspec
    startmom(6) = nout; countmom(6)=1

    call c2r (dens0, ri4)
    status = nf90_put_var (ncid_fullmom, densf0_id, ri4, start=startmom, count=countmom)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid_fullmom, densf0_id)

    call c2r (ux0, ri4)
    status = nf90_put_var (ncid_fullmom, uxf0_id, ri4, start=startmom, count=countmom)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid_fullmom, uxf0_id)
    call c2r (uy0, ri4)
    status = nf90_put_var (ncid_fullmom, uyf0_id, ri4, start=startmom, count=countmom)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid_fullmom, uyf0_id)
    call c2r (uz0, ri4)
    status = nf90_put_var (ncid_fullmom, uzf0_id, ri4, start=startmom, count=countmom)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid_fullmom, uzf0_id)

    ! pressures
    call c2r (pxx0, ri4)
    status = nf90_put_var (ncid_fullmom, pxxf0_id, ri4, start=startmom, count=countmom)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid_fullmom, pxxf0_id)
    call c2r (pyy0, ri4)
    status = nf90_put_var (ncid_fullmom, pyyf0_id, ri4, start=startmom, count=countmom)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid_fullmom, pyyf0_id)
    call c2r (pzz0, ri4)
    status = nf90_put_var (ncid_fullmom, pzzf0_id, ri4, start=startmom, count=countmom)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid_fullmom, pzzf0_id)
    call c2r (pxy0, ri4)
    status = nf90_put_var (ncid_fullmom, pxyf0_id, ri4, start=startmom, count=countmom)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid_fullmom, pxyf0_id)
    call c2r (pyz0, ri4)
    status = nf90_put_var (ncid_fullmom, pyzf0_id, ri4, start=startmom, count=countmom)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid_fullmom, pyzf0_id)
    call c2r (pzx0, ri4)
    status = nf90_put_var (ncid_fullmom, pzxf0_id, ri4, start=startmom, count=countmom)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid_fullmom, pzxf0_id)
    call c2r (qzzz0, ri4)
    status = nf90_put_var (ncid_fullmom, qzzzf0_id, ri4, start=startmom, count=countmom)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid_fullmom, qzzzf0_id)
    call c2r (qzpp0, ri4)
    status = nf90_put_var (ncid_fullmom, qzppf0_id, ri4, start=startmom, count=countmom)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid_fullmom, qzppf0_id)

    status = nf90_sync (ncid_fullmom)
    if (status /= NF90_NOERR) call netcdf_error (status)
    
# endif
  end subroutine nc_loop_fullmom2
  ! <JMT

!>JMT
  subroutine nc_loop_freq(nout, time)
    use convert, only: c2r
    use run_parameters, only: use_Phi, use_Apar, use_Bpar
    use theta_grid, only: ntgrid
    use kgrids, only: naky, nakx
    use fields_arrays, only: phi, apar, phinew, aparnew, bparnew
    use dist_fn, only: get_epar
    use agk_mem, only: alloc8, dealloc8
# ifdef NETCDF
    use netcdf, only: nf90_put_var, nf90_sync
    use netcdf_utils, only: netcdf_error
#endif
    integer, intent (in) :: nout
    real, intent (in) :: time
# ifdef NETCDF
    real, dimension(2, 2*ntgrid+1, nakx, naky) :: ri3
    real, dimension(2, 2*ntgrid+1, nk_freq) :: field
    integer :: status, i
    integer, dimension(4) :: start, count
    complex, dimension (:,:,:), allocatable :: epar
        
    status = nf90_put_var (ncid_freq, time_freq_id, time, start=(/nout/))
    if (status /= NF90_NOERR) call netcdf_error (status, ncid_freq, time_freq_id)
    
    start(1) = 1
    start(2) = 1
    start(3) = 1
    start(4) = nout
    
    count(1) = 2
    count(2) = 2*ntgrid+1
    count(3) = nk_freq
    count(4) = 1
    
    if (use_Phi) then
       call c2r (phinew, ri3)
       do i=1, nk_freq
          field(:,:,i) = ri3(:,:,kyx_freq(i,2),kyx_freq(i,1))
       enddo
       status = nf90_put_var (ncid_freq, phi_freq_id, field, start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid_freq, phi_freq_id)
    end if

    if (use_Apar) then
       call c2r (aparnew, ri3)
       do i=1, nk_freq
          field(:,:,i) = ri3(:,:,kyx_freq(i,2),kyx_freq(i,1))
       enddo
       status = nf90_put_var (ncid_freq, apar_freq_id, field, start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid_freq, apar_freq_id)
    end if

    if (use_Bpar) then
       call c2r (bparnew, ri3)
       do i=1, nk_freq
          field(:,:,i) = ri3(:,:,kyx_freq(i,2),kyx_freq(i,1))
       enddo
       status = nf90_put_var (ncid_freq, bpar_freq_id, field, start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid_freq, bpar_freq_id)
    end if
    
    allocate (epar(-ntgrid:ntgrid, nakx, naky)) ; call alloc8 (c3=epar, v="epar") 
    epar = 0.
    call get_epar (phi, apar, phinew, aparnew, epar)
    call c2r (epar, ri3)
    do i=1, nk_freq
       field(:,:,i) = ri3(:,:,kyx_freq(i,2),kyx_freq(i,1))
    enddo
    status = nf90_put_var (ncid_freq, epar_freq_id, field, start=start, count=count)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid_freq, epar_freq_id)

    call dealloc8 (c3=epar, v="epar")
 
    
    status = nf90_sync (ncid_freq)
    if (status /= NF90_NOERR) call netcdf_error (status)
# endif
  end subroutine nc_loop_freq
!<JMT

  ! added by EAB on 03/05/04
  subroutine nc_loop_movie (nout_movie, time, yxphi, yxapar, yxbpar, yxden)
    ! TT: called only for proc0
    use agk_layouts, only: yxf_lo
    use theta_grid, only: ntgrid
    use run_parameters, only: use_Phi, use_Apar, use_Bpar
# ifdef NETCDF
    use netcdf, only: nf90_put_var, nf90_sync
!    use agk_save, only: netcdf_error
    use netcdf_utils, only: netcdf_error
# endif
    integer, intent (in) :: nout_movie
    real, intent (in) :: time
    real, dimension (:,:,:), intent (in):: yxphi, yxapar, yxbpar 
    real, dimension (:,:,:), intent (in):: yxden
# ifdef NETCDF
    integer :: status
    integer, dimension (4) :: start, count

    status = nf90_put_var (ncid_movie, time_movie_id, time, start=(/nout_movie/))
    if (status /= NF90_NOERR) call netcdf_error (status, ncid_movie, time_movie_id)
    start(1) = 1
    start(2) = 1
    start(3) = 1
    start(4) = nout_movie
    count(1) = yxf_lo%nx
    count(2) = yxf_lo%ny
    count(3) = 2*ntgrid+1
    count(4) = 1

    status = nf90_put_var (ncid_movie, density_by_xmode_id, yxden, start=start, count=count)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid_movie, density_by_xmode_id)

    if (use_Phi) then
       status = nf90_put_var (ncid_movie, phi_by_xmode_id, yxphi, start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid_movie, phi_by_xmode_id)
    end if
    if (use_Apar) then
       status = nf90_put_var (ncid_movie, apar_by_xmode_id, yxapar, start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid_movie, apar_by_xmode_id)
    end if

    if (use_Bpar) then
       status = nf90_put_var (ncid_movie, bpar_by_xmode_id, yxbpar, start=start, count=count)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid_movie, bpar_by_xmode_id)
    end if

    if (mod(nout_movie, 10) == 0) then
       status = nf90_sync (ncid_movie)
       if (status /= NF90_NOERR) call netcdf_error (status)
    end if
# endif
  end subroutine nc_loop_movie

  ! MAB> output velocity spectra
!  subroutine nc_loop_vspec (nout, time, gvxi, gpq)
  subroutine nc_loop_vspec (nout, gvxi, gpq)
    use le_grids, only: nvpa, nvpe, nesub, nlambda
    use species, only: nspec
# ifdef NETCDF
    use netcdf, only: nf90_put_var, nf90_sync
    use netcdf_utils, only: netcdf_error
# endif
    implicit none

    integer, intent (in) :: nout
    real, dimension (:,:,:), intent (in) :: gvxi, gpq
# ifdef NETCDF
    integer :: status
    integer, dimension (4) :: startvxi, countvxi, startpq, countpq

!    status = nf90_put_var (ncid, time_id, time, start=(/nout/))
!    if (status /= NF90_NOERR) call netcdf_error (status, ncid, time_id)

    startvxi(1) = 1;    countvxi(1)=nlambda
    startvxi(2) = 1;    countvxi(2)=nesub
    startvxi(3) = 1;    countvxi(3)=nspec
    startvxi(4) = nout; countvxi(4)=1

    startpq(1) = 1;    countpq(1)=nvpe
    startpq(2) = 1;    countpq(2)=nvpa
    startpq(3) = 1;    countpq(3)=nspec
    startpq(4) = nout; countpq(4)=1

    if (proc_write) then
       status = nf90_put_var (ncid, gvxi_id, gvxi, start=startvxi, count=countvxi)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, gvxi_id)
       status = nf90_put_var (ncid, gpq_id, gpq, start=startpq, count=countpq)
       if (status /= NF90_NOERR) call netcdf_error (status, ncid, gpq_id)

       if (mod(nout, 10) == 0) then
          status = nf90_sync (ncid)
          if (status /= NF90_NOERR) call netcdf_error (status)
       end if
    end if
# endif
  end subroutine nc_loop_vspec
  ! <MAB

  subroutine nc_species
    ! TT: called only for proc0
    use species, only: spec
# ifdef NETCDF
    use netcdf, only: nf90_put_var
!    use agk_save, only: netcdf_error
    use netcdf_utils, only: netcdf_error

    integer :: status
    status = nf90_put_var (ncid, charge_id, spec%z)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, charge_id)
    status = nf90_put_var (ncid, mass_id, spec%mass)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, mass_id)
    status = nf90_put_var (ncid, dens_id, spec%dens)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, dens_id)
    status = nf90_put_var (ncid, temp_id, spec%temp)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, temp_id)
    status = nf90_put_var (ncid, tprim_id, spec%tprim)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, tprim_id)
    status = nf90_put_var (ncid, fprim_id, spec%fprim)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, fprim_id)
    status = nf90_put_var (ncid, uprim_id, spec%uprim)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, uprim_id)
    status = nf90_put_var (ncid, nu_id, spec%nu)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, nu_id)
    status = nf90_put_var (ncid, spec_type_id, spec%type)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, spec_type_id)
# endif
  end subroutine nc_species

  subroutine nc_geo
    ! TT: called only for proc0
    use theta_grid, only: gradpar, jacob
# ifdef NETCDF
    use netcdf, only: nf90_put_var
!    use agk_save, only: netcdf_error
    use netcdf_utils, only: netcdf_error
    integer :: status

    status = nf90_put_var (ncid, gradpar_id, gradpar)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, gradpar_id)
    status = nf90_put_var (ncid, jacob_id, jacob)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, jacob_id)
# endif
  end subroutine nc_geo

  subroutine nc_param
    use run_parameters, only: beta, zeff
# ifdef NETCDF
    use netcdf, only: nf90_put_var, nf90_def_var
    use netcdf, only: nf90_redef, nf90_enddef
    use netcdf_utils, only: netcdf_real
    use netcdf_utils, only: netcdf_error

    integer :: status
    integer :: beta_id, zeff_id

    status = nf90_redef (ncid)
    if (status /= NF90_NOERR) &
         & call netcdf_error (status, message='redef nc_param')
    
    status = nf90_def_var (ncid, 'beta', netcdf_real, beta_id)
    if (status /= NF90_NOERR) call netcdf_error (status, var='beta')
    status = nf90_def_var (ncid, 'zeff', netcdf_real, zeff_id)
    if (status /= NF90_NOERR) call netcdf_error (status, var='zeff')

    status = nf90_enddef (ncid)
    if (status /= NF90_NOERR) &
         & call netcdf_error (status, message='enddef nc_param')

    status = nf90_put_var (ncid, beta_id, beta)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, beta_id)
    status = nf90_put_var (ncid, zeff_id, zeff)
    if (status /= NF90_NOERR) call netcdf_error (status, ncid, zeff_id)
# endif
  end subroutine nc_param

end module agk_io
