module run_parameters
  implicit none

  public :: init_run_parameters

  public :: beta, zeff, tite, aant
  public :: fphi, fapar, fbpar
  public :: use_Phi, use_Apar, use_Bpar
  public :: nstep, delt_max
  public :: ieqzip
  public :: avail_cpu_time, margin_cpu_time
  public :: store_eq
  public :: hdf5_stop, hdf5_dble

  private

  real :: beta, zeff, tite
  real :: fphi, fapar, fbpar
  real :: delt, delt_max
  real :: avail_cpu_time, margin_cpu_time

  integer :: nstep
  logical :: use_Phi, use_Apar, use_Bpar
  ! following eqzip options are obsolete, use eqzip_option
  logical :: eqzip, secondary, tertiary, kill_full_eq_evol
  logical :: aant
  integer, allocatable :: ieqzip(:,:)

  integer :: eqzip_option_switch
  integer, parameter :: &
       eqzip_option_none = 1, &
       eqzip_option_secondary = 2, &
       eqzip_option_tertiary = 3, &
       eqzip_option_equilibrium = 4
  integer :: delt_option_switch
  integer, parameter :: delt_option_hand = 1, delt_option_auto = 2
  logical :: store_eq
  logical :: hdf5_stop, hdf5_dble

contains

  subroutine init_run_parameters
    use kgrids, only: init_kgrids, naky, nakx
    use agk_time, only: init_delt
    use agk_mem, only: alloc8

    implicit none
    logical, save :: initialized = .false.

    if (initialized) return
    initialized = .true.

    call read_parameters

    call init_kgrids
    call init_delt (delt)

    if(.not.allocated(ieqzip)) then
       allocate(ieqzip(nakx,naky)); call alloc8(i2=ieqzip,v="ieqzip")
    end if
    ieqzip(1:nakx,1:naky)=1
    select case (eqzip_option_switch)
    case (eqzip_option_secondary)
       ! suppress evolution of secondary mode
       ieqzip(1,2) = 0
    case (eqzip_option_tertiary)
       ! suppress evolution of tertiary mode
       ieqzip(2,1) = 0
       ieqzip(nakx,1) = 0
    case (eqzip_option_equilibrium)
       ! suppress evolution of 1D equilibrium (x dependent)
       ieqzip(1:nakx,1) = 0
    end select

  end subroutine init_run_parameters

  subroutine read_parameters
    use file_utils, only: input_unit, error_unit, input_unit_exist
    use mp, only: proc0, broadcast, mp_abort
    use agk_save, only: init_dt
    use text_options, only: text_option, get_option_value
    implicit none
    type (text_option), dimension (4), parameter :: eqzipopts = &
         (/ text_option('none', eqzip_option_none), &
            text_option('secondary', eqzip_option_secondary), &
            text_option('tertiary', eqzip_option_tertiary), &
            text_option('equilibrium', eqzip_option_equilibrium) /)
    character (len=20) :: eqzip_option
    type (text_option), dimension (3), parameter :: deltopts = &
         (/ text_option('default', delt_option_hand), &
            text_option('set_by_hand', delt_option_hand), &
            text_option('check_restart', delt_option_auto) /)
    character(20) :: delt_option
    
    integer :: ierr, istatus, in_file
    real :: delt_saved

    logical :: exist
    
    integer :: ireaderr1=0, ireaderr2=0
    logical :: abort_readerr=.true.

    namelist /parameters/ beta, zeff, tite, aant
    namelist /knobs/ delt, nstep, &
         & eqzip_option, &
         & eqzip, secondary, tertiary, kill_full_eq_evol, &
         & avail_cpu_time, margin_cpu_time, &
         & store_eq, hdf5_stop, hdf5_dble, &
         delt_option, use_Phi, use_Apar, use_Bpar

    if (proc0) then
       use_Phi = .true.    ;   fphi = 0.0
       use_Apar = .true.   ;   fapar = 0.0
       use_Bpar = .true.   ;   fbpar = 0.0

       aant = .true.   !  Use antenna to drive A_par if T; otherwise, drive Phi

       beta = 0.0
       zeff = 1.0
       tite = 1.0
       eqzip_option = 'none'
       eqzip = .false.
       secondary = .false.
       tertiary = .false.
       kill_full_eq_evol = .false.
       delt_option = 'default'
       avail_cpu_time=1.e10
       margin_cpu_time=300.
       store_eq = .false.
       hdf5_stop = .true.
       hdf5_dble = .true.

       in_file = input_unit_exist("parameters", exist)
       if (exist) read (unit=in_file, nml=parameters, iostat=ireaderr1)

       in_file = input_unit_exist("knobs", exist)
       if (exist) read (unit=in_file, nml=knobs, iostat=ireaderr2)

       if (use_Phi) fphi = 1.0
       if (use_Apar) fapar = 1.0
       if (use_Bpar) fbpar = 1.0

       ! RN 2009/2/6

       call get_option_value ( &
            eqzip_option, eqzipopts, eqzip_option_switch, error_unit(), &
            "eqzip_option in knobs")

       ! eqzip, secondary, tertiary, kill_full_eq_evol are obsolete
       ! but, is kept for backward compatibility
       ! use eqzip_option instead
       if (eqzip .and. eqzip_option_switch == eqzip_option_none) then
          if (kill_full_eq_evol) then
             eqzip_option_switch = eqzip_option_equilibrium
             write(error_unit(),*) 'eqzip_option is set to equilibrium'
          else 
             if (tertiary) then
                eqzip_option_switch = eqzip_option_tertiary
                write(error_unit(),*) 'eqzip_option is set to tertiary'
             else
                eqzip_option_switch = eqzip_option_secondary
                write(error_unit(),*) 'eqzip_option is set to secondary'
             end if
          end if
       else if (eqzip .and. eqzip_option_switch /= eqzip_option_none) then
          write(error_unit(),*) 'eqzip, secondary, tertiary, and kill_full_eq_evol are ignored'
          write(error_unit(),*) 'because eqzip_option exists'
       end if

       ierr = error_unit()
       call get_option_value &
            (delt_option, deltopts, delt_option_switch, ierr, &
            "delt_option in knobs")
    end if
    
    call broadcast (ireaderr1)
    call broadcast (ireaderr2)
    if (abort_readerr) then
       if (ireaderr1 > 0) then
          call mp_abort('Read error at parameters')
       else if (ireaderr1 < 0) then
          call mp_abort('End of file/record occurred at parameters')
       endif
       if (ireaderr2 > 0) then
          call mp_abort('Read error at knobs')
       else if (ireaderr2 < 0) then
          call mp_abort('End of file/record occurred at knobs')
       endif
    endif
    
    call broadcast (delt_option_switch)
    call broadcast (delt)
    call broadcast (beta)
    call broadcast (zeff)
    call broadcast (tite)
    call broadcast (aant)
    call broadcast (use_Phi)
    call broadcast (use_Apar)
    call broadcast (use_Bpar)
    call broadcast (fphi)
    call broadcast (fapar)
    call broadcast (fbpar)
    call broadcast (nstep)
    call broadcast (eqzip_option_switch)
    call broadcast (avail_cpu_time)
    call broadcast (margin_cpu_time)
    call broadcast (store_eq)
    call broadcast (hdf5_stop)
    call broadcast (hdf5_dble)

    delt_max = delt

    delt_saved = delt
    if (delt_option_switch == delt_option_auto) then
       !
       ! This should be done only for restart runs.
       ! Currently, delt is read from restart file whenever it exsits regardless
       ! of the initialization mode.
       !
       call init_dt (delt_saved, istatus)
       if (istatus == 0) delt  = delt_saved
    endif

  end subroutine read_parameters

end module run_parameters
