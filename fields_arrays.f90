module fields_arrays

  implicit none
  public

  complex, dimension (:,:,:), allocatable :: phi,    apar,    bpar
  complex, dimension (:,:,:), allocatable :: phi_ext, apar_ext
  complex, dimension (:,:,:), allocatable :: phinew, aparnew, bparnew
  complex, dimension (:,:,:), allocatable :: phi_eq, apar_eq, bpar_eq
  ! (-ntgrid:ntgrid,nakx,naky) replicated

  integer, save :: nidx

  complex, dimension (:,:), allocatable :: aminv

end module fields_arrays
